"use strict";
cc._RF.push(module, '2c5ff9R/FJPip0TZkafs6WJ', 'Home');
// Scripts/Home.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Slider2_1 = require("./Slider2");
var Config_1 = require("./Config");
var Leaderboard_1 = require("./Leaderboard");
var util_1 = require("./util");
var Popup_1 = require("./Popup");
var DailyBonus_1 = require("./popop/bonus/DailyBonus");
var AutoHide_1 = require("./AutoHide");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Home = /** @class */ (function (_super) {
    __extends(Home, _super);
    function Home() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.avatar = null;
        _this.playerName = null;
        _this.coin = null;
        _this.betSetting = null;
        _this.leaderboard = null;
        _this.music = null;
        _this.dailyBonus = null;
        _this.popup = null;
        _this.toast = null;
        _this.modal = null;
        _this.playTime = 0;
        _this.countTime = 0;
        _this.itvCountTime = null;
        _this.preloadSceneGame = function () {
            cc.director.preloadScene('game', function (completedCount, totalCount, item) {
                // console.log('====================================');
                // console.log('preload', completedCount, totalCount, item);
                // console.log('====================================');
            }, function (error) {
                console.log('====================================');
                console.log('loaded game');
                console.log('====================================');
            });
        };
        _this.onChallenge = function (playerId, photo, username, coin) { return __awaiter(_this, void 0, void 0, function () {
            var error_1, minCoin;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Api_1.default.coin <= Config_1.default.bankrupt) {
                            Api_1.default.preloadRewardedVideo();
                            this.popup.open(this.node, 4);
                            Api_1.default.showInterstitialAd();
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, Api_1.default.challenge(playerId)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 4];
                    case 4:
                        if (Math.random() < 0.7) {
                            Api_1.default.showInterstitialAd();
                        }
                        cc.audioEngine.stopMusic();
                        Config_1.default.battle = { photo: photo, username: username, coin: coin };
                        minCoin = Math.min(Math.max(coin, 100000), Api_1.default.coin);
                        Config_1.default.betValue = Math.floor(minCoin * 0.3);
                        cc.director.loadScene('game');
                        Api_1.default.logEvent(EventKeys_1.default.PLAY_WITH_FRIEND);
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    Home.prototype.initCountTime = function () {
        var _this = this;
        this.itvCountTime = setInterval(function () {
            _this.countTime += Config_1.default.stepOfCountTime / 1000;
            util_1.default.logTimeInGame(_this.countTime);
        }, Config_1.default.stepOfCountTime);
    };
    Home.prototype.clearLogTime = function () {
        this.itvCountTime && clearInterval(this.itvCountTime);
        this.itvCountTime = null;
        this.countTime = 0;
    };
    // init logic
    Home.prototype.start = function () {
        var _this = this;
        util_1.default.logTimeInGame(0);
        this.initCountTime();
        this.betSetting.onValueChange = this.onBetChange;
        Api_1.default.initAsync(function (bonus, day) {
            Language_1.default.getInstance().load(Api_1.default.locale);
            cc.systemEvent.emit('LANG_CHAN');
            _this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
            _this.playerName.string = Api_1.default.username;
            cc.assetManager.loadRemote(Api_1.default.photo, function (err, tex) {
                _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
                Config_1.default.userphoto = tex;
            });
            if (bonus) {
                Api_1.default.preloadRewardedVideo(function () {
                    Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_SHOW);
                    _this.dailyBonus.show(day, Api_1.default.dailyBonusClaimed);
                });
                cc.systemEvent.on('claim_daily_bonus', _this.onClaimDailyBonus, _this);
                cc.systemEvent.on('claim_daily_bonus_fail', _this.onClaimDailyBonusFail, _this);
            }
        }, this.leaderboard);
        cc.systemEvent.on('lb_battle', this.onChallenge, this);
        cc.systemEvent.on('lb_share', this.onShare, this);
        cc.audioEngine.setMusicVolume(0.3);
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        // this.scheduleOnce(this.preloadSceneGame, 1);
        setTimeout(function () {
            _this.preloadSceneGame();
        }, 1);
        Api_1.default.preloadInterstitialAd();
    };
    Home.prototype.onShare = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Api_1.default.shareAsync()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Home.prototype.onClaimDailyBonus = function (day, bonus, extra) {
        if (bonus.coin) {
            Api_1.default.coinIncrement(extra ? 2 * bonus.coin : bonus.coin);
            this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        }
        if (bonus.ticket) {
            Api_1.default.ticket += extra ? 2 * bonus.ticket : bonus.ticket;
        }
        Api_1.default.claimDailyBonus(day);
    };
    Home.prototype.onClaimDailyBonusFail = function () {
        this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
    };
    Home.prototype.openPlayPopupClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_NOW);
        if (Api_1.default.coin > Config_1.default.bankrupt) {
            this.popup.open(this.node, 1);
        }
        else {
            Api_1.default.preloadRewardedVideo();
            Api_1.default.showInterstitialAd();
            this.popup.open(this.node, 4);
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
        }
    };
    Home.prototype.onPlayClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_ACCEPT);
        Config_1.default.battle = null;
        cc.audioEngine.stopMusic();
        cc.director.loadScene('game');
    };
    Home.prototype.onShopClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.SHOP_GO);
        cc.director.loadScene('shop');
    };
    Home.prototype.onDailyBonusClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.DAILY_GIFT_CLICK);
        this.dailyBonus.show(Api_1.default.dailyBonusDay, Api_1.default.dailyBonusClaimed);
        Api_1.default.showInterstitialAd();
    };
    Home.prototype.onBetChange = function (value) {
        Config_1.default.betValue = value;
    };
    Home.prototype.onBotChange = function (sender, params) {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_MODE + '-' + params);
        if (sender.isChecked) {
            Config_1.default.totalPlayer = parseInt(params);
        }
    };
    Home.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        else {
            cc.audioEngine.stopMusic();
        }
    };
    Home.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.openWithText(null, msg);
        Api_1.default.coinIncrement(bonus);
        this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        this.popup.close(null, 4);
    };
    Home.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 2);
            _this.toast.openWithText(null, 'Mời bạn chơi không thành công');
        });
    };
    Home.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    __decorate([
        property(cc.Sprite)
    ], Home.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "coin", void 0);
    __decorate([
        property(Slider2_1.default)
    ], Home.prototype, "betSetting", void 0);
    __decorate([
        property(Leaderboard_1.default)
    ], Home.prototype, "leaderboard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Home.prototype, "music", void 0);
    __decorate([
        property(DailyBonus_1.default)
    ], Home.prototype, "dailyBonus", void 0);
    __decorate([
        property(Popup_1.default)
    ], Home.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], Home.prototype, "toast", void 0);
    __decorate([
        property(Modal_1.default)
    ], Home.prototype, "modal", void 0);
    Home = __decorate([
        ccclass
    ], Home);
    return Home;
}(cc.Component));
exports.default = Home;

cc._RF.pop();