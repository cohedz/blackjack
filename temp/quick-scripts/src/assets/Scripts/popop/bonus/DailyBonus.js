"use strict";
cc._RF.push(module, '2f9e8XOO6BHzap8qYPJCMDb', 'DailyBonus');
// Scripts/popop/bonus/DailyBonus.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Popup_1 = require("../../Popup");
var Config_1 = require("../../Config");
var DailyBonusItem_1 = require("./DailyBonusItem");
var Api_1 = require("../../Api");
var AutoHide_1 = require("../../AutoHide");
var util_1 = require("../../util");
var EventKeys_1 = require("../../EventKeys");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DailyBonus = /** @class */ (function (_super) {
    __extends(DailyBonus, _super);
    function DailyBonus() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.toast = null;
        _this.days = [];
        _this.effect = null;
        _this.current = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    DailyBonus.prototype.start = function () {
    };
    // update (dt) {}
    DailyBonus.prototype.show = function (day, claimed) {
        if (claimed === void 0) { claimed = false; }
        if (!claimed) {
            day++;
        }
        if (day > Config_1.default.dailyBonus.length) {
            day = 1;
        }
        this.current = day;
        this.popup.open(this.node, 2);
        for (var i = 0; i < this.days.length; i++) {
            var d = i + 1;
            var claimable = (d == day) && !claimed;
            if (d < day) {
                this.days[i].setClaimable(claimable, true);
            }
            else if (d == day) {
                this.days[i].setClaimable(claimable, claimed);
            }
            else {
                this.days[i].setClaimable(claimable, false);
            }
        }
    };
    DailyBonus.prototype.onClaim = function (sender, day) {
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        this.onClaimCompl(sender, date, false);
        cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], false);
        this.popup.close(this.node, 2);
    };
    DailyBonus.prototype.onClaimExtra = function (sender, day) {
        var _this = this;
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD);
        Api_1.default.showRewardedVideo(function () {
            _this.onClaimCompl(sender, date, false);
            cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], true);
            _this.popup.close(_this.node, 2);
        }, function () {
            cc.systemEvent.emit('claim_daily_bonus_fail');
            Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD_ERROR);
            _this.popup.close(_this.node, 2);
        });
    };
    DailyBonus.prototype.onClaimCompl = function (sender, day, double) {
        var gift = Config_1.default.dailyBonus[day - 1];
        if (!gift.coin) {
            return;
        }
        var e = cc.instantiate(this.effect);
        e.active = true;
        e.position = sender.target.position;
        e.scale = 1;
        e.parent = cc.director.getScene().getChildByName("Canvas");
        var coinBonus = double ? gift.coin * 2 : gift.coin;
        e.getComponent(cc.Label).string = util_1.default.numberFormat(coinBonus);
        e.runAction(cc.sequence(cc.moveBy(1.2, 0, 250), cc.removeSelf()));
    };
    __decorate([
        property(Popup_1.default)
    ], DailyBonus.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], DailyBonus.prototype, "toast", void 0);
    __decorate([
        property(DailyBonusItem_1.default)
    ], DailyBonus.prototype, "days", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonus.prototype, "effect", void 0);
    DailyBonus = __decorate([
        ccclass
    ], DailyBonus);
    return DailyBonus;
}(cc.Component));
exports.default = DailyBonus;

cc._RF.pop();