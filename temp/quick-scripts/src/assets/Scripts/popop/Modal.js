"use strict";
cc._RF.push(module, 'a10e5sRCWFPIIokEQkdNm2n', 'Modal');
// Scripts/popop/Modal.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Modal = /** @class */ (function (_super) {
    __extends(Modal, _super);
    function Modal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.title = null;
        _this.content = null;
        return _this;
    }
    Modal_1 = Modal;
    // LIFE-CYCLE CALLBACKS:
    Modal.prototype.onLoad = function () {
        Modal_1.instance = this;
    };
    Modal.prototype.start = function () {
    };
    // update (dt) {}
    Modal.prototype.show = function (text) {
        this.content.string = text;
        this.node.active = true;
        this.popup.active = true;
    };
    Modal.prototype.onClose = function () {
        this.node.active = false;
        this.popup.active = false;
    };
    var Modal_1;
    Modal.instance = null;
    __decorate([
        property(cc.Node)
    ], Modal.prototype, "popup", void 0);
    __decorate([
        property(cc.Label)
    ], Modal.prototype, "title", void 0);
    __decorate([
        property(cc.Label)
    ], Modal.prototype, "content", void 0);
    Modal = Modal_1 = __decorate([
        ccclass
    ], Modal);
    return Modal;
}(cc.Component));
exports.default = Modal;

cc._RF.pop();