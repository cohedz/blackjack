"use strict";
cc._RF.push(module, '1351esCO5tMRYPcn390B3zj', 'CommonCard');
// Scripts/CommonCard.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonCard = /** @class */ (function () {
    function CommonCard() {
        this.totalCards = 0;
        this.cards = [];
        this.combat = [];
        this.latest = null;
    }
    CommonCard.prototype.getPosition = function () {
        if (this.cards.length == 0)
            return cc.Vec2.ZERO;
        var sx = Math.random() * 100 - 50;
        var sy = Math.random() * 100 - 50;
        return cc.v2(sx, sy + 50);
    };
    CommonCard.prototype.reset = function () {
        this.totalCards = 0;
        this.cards = [];
        this.latest = null;
        if (this.combat.length > 0) {
            this.combat = [];
        }
    };
    CommonCard.prototype.push = function (cards) {
        this.totalCards += cards.count();
        this.cards.push(cards);
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        this.latest = cards;
    };
    CommonCard.prototype.isCombatOpen = function () {
        return this.combat.length > 0;
    };
    CommonCard.prototype.pushCombat = function (player, cards) {
        this.combat.push({ player: player, cards: cards });
    };
    CommonCard.prototype.hasCombat = function () {
        if (this.combat.length < 2)
            return false;
        var peek = this.combat[this.combat.length - 1];
        if (peek.cards.highest.rank == 15)
            return false;
        return true;
    };
    CommonCard.prototype.getCombat = function () {
        return this.combat[0].cards;
    };
    CommonCard.prototype.getCombatLength = function () {
        return this.combat.length;
    };
    CommonCard.prototype.getCombatWinner = function () {
        return this.combat[this.combat.length - 1].player;
    };
    CommonCard.prototype.getCombatVictim = function () {
        return this.combat[this.combat.length - 2].player;
    };
    CommonCard.prototype.resetCombat = function () {
        this.combat = [];
    };
    CommonCard.prototype.peek = function () {
        return this.cards[this.cards.length - 1];
    };
    CommonCard.prototype.length = function () {
        return this.cards.length;
    };
    CommonCard.prototype.isEmpty = function () {
        return this.cards.length == 0;
    };
    CommonCard.prototype.nextRound = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            for (var j = this.cards[i].count() - 1; j >= 0; --j) {
                var card = this.cards[i].at(j);
                card.hide();
                card.node.setScale(0.5);
            }
        }
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        if (this.combat.length > 0) {
            this.combat = [];
        }
        this.cards = [];
        this.latest = null;
    };
    CommonCard.prototype.overlapCard = function (cards) {
        for (var i = 0; i < cards.count(); i++) {
            cards.at(i).overlap();
        }
    };
    return CommonCard;
}());
exports.default = CommonCard;

cc._RF.pop();