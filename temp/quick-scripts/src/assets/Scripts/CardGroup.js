"use strict";
cc._RF.push(module, 'bece1QhfllOVa5CfNF0TmNb', 'CardGroup');
// Scripts/CardGroup.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup = /** @class */ (function () {
    function CardGroup(cards, kind) {
        this.cards = cards;
        this.kind = CardGroup.Single;
        this.kind = kind;
        if (kind != 0) {
            this.highest = Helper_1.default.findMaxCard(cards);
        }
    }
    CardGroup.prototype.getPosition = function () {
        if (this.cards.length % 2 != 0) {
            var idx = (this.cards.length - 1) / 2;
            return this.cards[idx].node.getPosition();
        }
        var middle = this.cards.length / 2;
        var c1 = this.cards[middle].node.getPosition();
        var c2 = this.cards[middle - 1].node.getPosition();
        return cc.v2((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
    };
    CardGroup.prototype.gt = function (o) {
        if (this.kind == o.kind && this.count() == o.count())
            return this.highest.gt(o.highest);
        if (o.highest.rank == 15 && o.count() == 1) {
            if (this.kind == CardGroup.MultiPair)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.highest.rank == 15 && o.count() == 2) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.kind == CardGroup.MultiPair && o.count() == 6) {
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
        }
        if (o.kind == CardGroup.House && o.count() == 4) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            else
                return false;
        }
        return false;
    };
    CardGroup.prototype.at = function (i) {
        return this.cards[i];
    };
    CardGroup.prototype.count = function () {
        return this.cards.length;
    };
    CardGroup.prototype.push = function (card) {
        this.cards.push(card);
    };
    CardGroup.prototype.remove = function (card) {
        var index = this.cards.indexOf(card, 0);
        this.cards.splice(index, 1);
    };
    CardGroup.prototype.contains = function (card) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            if (this.cards[i] == card)
                return true;
        }
        return false;
    };
    CardGroup.prototype.calculate = function () {
        if (this.cards.length == 1) {
            this.kind = CardGroup.Single;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isHouse(this.cards)) {
            this.kind = CardGroup.House;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isStraight(this.cards)) {
            this.kind = CardGroup.Straight;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isMultiPair(this.cards)) {
            this.kind = CardGroup.MultiPair;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else {
            this.kind = CardGroup.Ungrouped;
        }
    };
    CardGroup.prototype.sort = function () {
        Helper_1.default.sort(this.cards);
    };
    CardGroup.prototype.isInvalid = function () {
        return this.kind == CardGroup.Ungrouped;
    };
    CardGroup.prototype.dump = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            console.log(this.cards[i].toString());
        }
    };
    CardGroup.Ungrouped = 0;
    CardGroup.Single = 1;
    CardGroup.House = 2;
    CardGroup.Straight = 3;
    CardGroup.Boss = 4;
    CardGroup.MultiPair = 5;
    return CardGroup;
}());
exports.default = CardGroup;

cc._RF.pop();