"use strict";
cc._RF.push(module, 'fef6cR3M0xAfLpe1NdULNzR', 'EventKeys');
// Scripts/EventKeys.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventKeys = {
// PLAY_NOW: 'play_now',
// SHOP_GO: 'shop_go',
// VIP_GO: 'vip_go',
// DAILY_GIFT_CLICK: 'daily_gift_click',
// PLAY_MODE: 'play_mode',
// PLAY_ACCEPT: 'play_accept',
// QUIT_GAME: 'quit_game',
// WIN: 'win',
// LOSE: 'lose',
// PLAY_DURATION: 'play_duration',
// PLAY_TIME: 'play_time',
// INVITE_FRIEND: 'invite_friend',
// SPIN: 'spin',
// PLAY_WITH_FRIEND: 'play_with_friend',
// POPUP_ADREWARD_COIN_OPEN: '100k_popup',
// POPUP_ADREWARD_COIN_CLICK: '100k_clickpopup',
// POPUP_ADREWARD_COIN_ERROR: '100k_novideopopup',
// POPUP_ADREWARD_SPIN_OPEN: 'spin_popup',
// POPUP_ADREWARD_SPIN_CLICK: 'spin_click_popup',
// POPUP_ADREWARD_SPIN_ERROR: 'spin_novideo_popup',
// POPUP_DAILY_REWARD_SHOW: 'daily_reward_popup',
// POPUP_DAILY_REWARD_AD: 'daily_reward_click_video',
// POPUP_DAILY_REWARD_AD_ERROR: 'daily_reward_no_video',
};
exports.default = EventKeys;

cc._RF.pop();