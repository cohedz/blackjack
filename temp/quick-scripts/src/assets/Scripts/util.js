"use strict";
cc._RF.push(module, '5164dU8pUhEeacqN2jObHLU', 'util');
// Scripts/util.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Config_1 = require("./Config");
var util = /** @class */ (function () {
    function util() {
    }
    util.numberFormat = function (n) {
        var ii = 0;
        while ((n = n / 1000) >= 1) {
            ii++;
        }
        return (Math.round(n * 10 * 1000) / 10) + util.prefix[ii];
    };
    util.playAudio = function (audioClip) {
        if (Config_1.default.soundEnable)
            cc.audioEngine.playEffect(audioClip, false);
    };
    util.prefix = ["", "k", "M", "G", "T", "P", "E", "Z", "Y", "x10^27", "x10^30", "x10^33"]; // should be enough. Number.MAX_VALUE is about 10^308
    util.logTimeInGame = function (time) {
        if ([30, 60, 90, 120, 180, 270, 510].indexOf(time) > -1 || time === 0) {
            Api_1.default.logEvent(time + "s_sstime");
        }
    };
    return util;
}());
exports.default = util;

cc._RF.pop();