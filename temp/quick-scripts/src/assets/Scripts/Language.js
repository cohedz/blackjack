"use strict";
cc._RF.push(module, '3d3c6mK5nNMjYk5tCjZK4el', 'Language');
// Scripts/Language.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vi_VN_1 = require("../lang/vi_VN");
var th_TH_1 = require("../lang/th_TH");
var en_US_1 = require("../lang/en_US");
var tl_PH_1 = require("../lang/tl_PH");
var Language = /** @class */ (function () {
    function Language() {
        this.lang = en_US_1.default;
    }
    Language.getInstance = function () {
        if (!Language._instance) {
            Language._instance = new Language();
        }
        return Language._instance;
    };
    Language.prototype.load = function (lang) {
        if (lang == 'vi_VN') {
            this.lang = vi_VN_1.default;
        }
        else if (lang == 'th_TH') {
            this.lang = th_TH_1.default;
        }
        else if (lang == 'tl_PH') {
            this.lang = tl_PH_1.default;
        }
        else {
            this.lang = en_US_1.default;
        }
    };
    Language.prototype.get = function (key) {
        return this.lang[key];
    };
    Language._instance = null;
    return Language;
}());
exports.default = Language;

cc._RF.pop();