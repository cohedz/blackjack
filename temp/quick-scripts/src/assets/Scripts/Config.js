"use strict";
cc._RF.push(module, 'a4c56iZ0yNPbYQ1DV9FQmk8', 'Config');
// Scripts/Config.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.language = 'vi';
    Config.defaultCoin = 100000;
    Config.botCoin = 100000;
    Config.bankrupt = 1000;
    Config.bankrupt_bonus = 500000;
    Config.minBet = 3000;
    Config.maxBet = 100000;
    Config.betValue = 3000;
    Config.totalPlayer = 6;
    Config.userphoto = null;
    Config.battle = null;
    Config.intertital_ads = '717961958974985_757265555044625';
    Config.reward_video = '316445729595977_363105338263349';
    Config.gift_reward_video = '316445729595977_363105338263349';
    Config.soundEnable = true;
    Config.dailyBonus = [
        { coin: 100000 },
        { ticket: 3 },
        { coin: 500000 },
        { coin: 1000000 },
        { coin: 5000000 },
        { ticket: 5 },
        { coin: 10000000 }
    ];
    Config.stepOfCountTime = 30000;
    return Config;
}());
exports.default = Config;
// Ngày 1 : 100K ,
// Ngày 2 : 300K , 
// ngày 3 thêm 3 lượt SPIN x tiền , 
// ngày 4 : 1M , 
// ngày 5 thêm 5 lượt Spin x tiền , 
// ngày 6 5M , 
// ngày 7 : 10M

cc._RF.pop();