"use strict";
cc._RF.push(module, 'c8251pV8NZGpa2LG0Z5yRed', 'Game');
// Scripts/Game.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = require("./Player");
var Deck_1 = require("./Deck");
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var CommonCard_1 = require("./CommonCard");
var Toast_1 = require("./Toast");
var Api_1 = require("./Api");
var Notice_1 = require("./Notice");
var Config_1 = require("./Config");
var util_1 = require("./util");
var SpinWheel_1 = require("./SpinWheel");
var Popup_1 = require("./Popup");
var Bot_1 = require("./Bot");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var suiteAsset = {
    H: 'co',
    D: "ro",
    C: "chuon",
    S: "bích"
};
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Profile = /** @class */ (function () {
    function Profile() {
        this.avatar = null;
        this.username = '';
    }
    __decorate([
        property(cc.SpriteFrame)
    ], Profile.prototype, "avatar", void 0);
    __decorate([
        property(cc.String)
    ], Profile.prototype, "username", void 0);
    Profile = __decorate([
        ccclass('Profile')
    ], Profile);
    return Profile;
}());
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deck = null;
        _this.play = null;
        _this.seats = [];
        _this.fire = null;
        _this.fold = null;
        _this.sort = null;
        _this.resultEffects = [];
        _this.bigWin = null;
        _this.toast = null;
        _this.notice = null;
        _this.betText = null;
        _this.spin = null;
        _this.popup = null;
        _this.profileBots = [];
        _this.soundToggle = null;
        _this.audioFold = null;
        _this.audioShowCard = null;
        _this.audioGarbageCard = null;
        _this.audioFire = null;
        _this.audioLose = null;
        _this.audioWin = null;
        _this.audioSortCard = null;
        _this.audioQuitGame = null;
        _this.audioDealCard = null;
        _this.audioFireSingle = null;
        _this.modal = null;
        _this.players = [];
        _this.commonCards = new CommonCard_1.default();
        _this.state = 0;
        _this.turn = 0;
        _this.leaveGame = false;
        _this.owner = null;
        _this.myself = null;
        _this.betValue = 1000;
        _this.totalPlayer = 6;
        _this.startTime = 0;
        _this.playTime = 0;
        _this.addSeat = function (seat) {
            var profile = _this.getProfileBot();
            seat.show();
            seat.setAvatar(profile.avatar);
            seat.setUsername(profile.username);
            _this.players.push(seat);
        };
        _this.showAllPlayerCard = function () {
            _this.players.map(function (player) {
                player.showAllCard();
            });
        };
        _this.showResult = function () {
            // let leaderPoint = this.players.slice(-1)[0].point
            // let userPoint = this.players[0].point
            // let resultString = [`điểm cái: ${leaderPoint}`, `Điểm user: ${userPoint}`]
            // this.players.map(p => {
            //     if (p.isBot() && !p.isCai()) {
            //         resultString.push(`Điểm bot ${resultString.length - 1}: ${p.point}`)
            //     }
            // })
            var dealer = _this.players[0];
            var dealerAddCoin = 0;
            _this.players.map(function (player, index) {
                if (!index)
                    return;
                var playerCoin = _this.calCointEndGame(player);
                if (player.subUser) {
                    playerCoin += _this.calCointEndGame(player.subUser);
                }
                player.changeCoin(playerCoin);
                console.log('====================================');
                console.log('user ' + index + ' ' + playerCoin);
                console.log('====================================');
                dealerAddCoin -= playerCoin;
            });
            dealer.changeCoin(dealerAddCoin);
            console.log('====================================');
            console.log('nha cai ' + dealerAddCoin);
            console.log('====================================');
            _this.showAllPlayerCard();
            _this.delayReset(3);
            _this.state = Game_1.LATE;
            // this.owner = winner;
            // this.bigWin.active = true;
            // let total = 0;
            // for (let i = 0; i < this.players.length; i++) {
            //     let player = this.players[i];
            //     if (player != winner) {
            //         if (player.isBot()) {
            //             player.hideCardCounter();
            //             player.showHandCards();
            //             player.reorder();
            //         }
            //         let lost = 13 * this.betValue;
            //         lost = Math.min(player.coinVal, lost);
            //         player.subCoin(lost);
            //         total += lost;
            //     }
            // }
            // winner.addCoin(total);
            Api_1.default.updateCoin(_this.myself.coinVal);
            // this.onWin(total);
            // return this.toast.show(`Tổng cmn kết rồi\n ${resultString.join('\n')}`)
        };
        _this.calCointEndGame = function (player) {
            var dealer = _this.players[0];
            var playerCoin = 0;
            if (player.bonusType == 2) {
                playerCoin -= _this.betValue / 2;
            }
            else if (player.point > 21) {
                playerCoin -= _this.betValue;
                // player.subCoin(this.betValue)
            }
            else if (player.checkBlackJack()) {
                playerCoin += _this.betValue * 1.5;
            }
            else if (player.point > dealer.point) {
                playerCoin += _this.betValue;
            }
            else if (player.point === dealer.point) {
                return 0;
            }
            else if (dealer.point > 21) {
                playerCoin += _this.betValue;
            }
            else {
                playerCoin -= _this.betValue;
            }
            if (/1/.test(player.bonusType.toString())) {
                playerCoin *= 2;
            }
            return playerCoin;
        };
        _this.checkWin = function () {
            // console.log('====================================');
            // console.log(this.players);
            // console.log('====================================');
            return new Promise(function (resolve) {
                // this.players.
            });
        };
        return _this;
    }
    Game_1 = Game;
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        var _this = this;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        var As = cc.find('Canvas/cards/AS');
        cc.resources.load('cards', cc.SpriteAtlas, function (err, atlas) {
            var initCardsResouces = new Promise(function (resolve) {
                new Array(13).fill('').map(function (el, rank) {
                    ['S', 'C', 'D', 'H'].map(function (suite, idSuite) {
                        if (!rank && !suite)
                            return;
                        var r = rank + 1;
                        var newNode = cc.instantiate(As);
                        switch (r) {
                            case 1:
                                r = 'A';
                                break;
                            case 11:
                                r = 'J';
                                break;
                            case 12:
                                r = 'Q';
                                break;
                            case 13:
                                r = 'K';
                                break;
                        }
                        var assetSuiteName = suiteAsset[suite];
                        var rankAssetname = r;
                        if (idSuite < 2)
                            rankAssetname += '-1';
                        // console.log('====================================');
                        // console.log(rankAssetname, assetSuiteName,);
                        // console.log('====================================');
                        newNode.name = r + suite;
                        var suiteSprite = atlas.getSpriteFrame(assetSuiteName);
                        newNode.children[0].getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(rankAssetname);
                        newNode.children[1].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[2].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[3].getComponent(cc.Widget).target = newNode;
                        newNode.active = false;
                        // console.log('====================================');
                        // console.log(newNode);
                        // console.log('====================================');
                        As.parent.addChild(newNode);
                        if (rank === 12 && suite === 'H') {
                            setTimeout(function () {
                                resolve(1);
                            }, 500);
                        }
                    });
                });
            });
            Promise.all([initCardsResouces]).then(function (rs) {
                _this.initGame();
            }).catch(function (err) {
                console.log('====================================');
                console.log(err);
                console.log('====================================');
            });
        });
    };
    // start(){}
    Game.prototype.initGame = function () {
        var _this = this;
        this.startTime = Date.now();
        this.state = Game_1.WAIT;
        this.betValue = Config_1.default.betValue;
        this.totalPlayer = Config_1.default.totalPlayer;
        this.betText.string = Language_1.default.getInstance().get('NO') + ': ' + util_1.default.numberFormat(this.betValue);
        this.deck.node.active = false;
        this.sort.node.active = false;
        this.play.node.active = false;
        this.hideDashboard();
        this.notice.hide();
        this.spin.onSpinHide = this.onSpinHide.bind(this);
        this.spin.onSpinCompleted = this.onSpinCompleted.bind(this);
        if (Config_1.default.battle) {
            this.totalPlayer = 2;
        }
        var arr = /0|3/;
        if (this.totalPlayer === 4)
            arr = /0|1|3|5/;
        else if (this.totalPlayer === 6)
            arr = /0|1|2|3|4|5/;
        this.seats.map(function (seat, i) {
            seat.seat = _this.players.length;
            if (arr.test(i.toString()))
                _this.addSeat(seat);
            else {
                seat.hide();
            }
        });
        // for (let i = 0; i < this.totalPlayer; i++) {
        //     let seat = this.seats[i];
        //     seat.seat = i;
        //     if (i < this.totalPlayer) {
        //         let profile = this.getProfileBot();
        //         seat.show();
        //         seat.setAvatar(profile.avatar);
        //         seat.setUsername(profile.username);
        //         this.players.push(seat);
        //     } else {
        //         seat.hide();
        //     }
        // }
        this.myself = this.players[this.totalPlayer / 2];
        this.myself.setUsername(Api_1.default.username);
        this.myself.setCoin(Api_1.default.coin);
        this.myself.setTimeCallback(this.onPlayerTimeout, this, this.node);
        if (Config_1.default.userphoto) {
            this.myself.setAvatar(new cc.SpriteFrame(Config_1.default.userphoto));
        }
        if (Config_1.default.battle) {
            var bot = this.players[1];
            bot.setCoin(Config_1.default.battle.coin);
            bot.setAvatar(new cc.SpriteFrame(Config_1.default.battle.photo));
            bot.setUsername(Config_1.default.battle.username);
        }
        else {
            this.players.map(function (player) {
                if (player.isBot())
                    player.setCoin(Config_1.default.botCoin);
            });
        }
        this.node.runAction(cc.sequence(cc.delayTime(0.7), cc.callFunc(this.deal, this)));
        if (Config_1.default.soundEnable) {
            this.soundToggle.uncheck();
        }
        else {
            this.soundToggle.check();
        }
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.onDestroy = function () {
        // this.clearLogTime()
        // const duration = Date.now() - this.startTime;
        // Api.logEvent(EventKeys.PLAY_DURATION, Math.floor(duration / 1000));
    };
    // update(dt) {
    //     if (this.playTime < 30 && this.playTime + dt >= 30) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-30');
    //     } else if (this.playTime < 60 && this.playTime + dt >= 60) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-60');
    //     } else if (this.playTime < 90 && this.playTime + dt >= 90) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-90');
    //     } else if (this.playTime < 180 && this.playTime + dt >= 180) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-180');
    //     } else if (this.playTime < 360 && this.playTime + dt >= 360) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-360');
    //     } else if (this.playTime < 500 && this.playTime + dt >= 500) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-500');
    //     }
    //     this.playTime += dt;
    // }
    Game.prototype.onTouchStart = function (evt) {
        if (this.state != Game_1.PLAY)
            return;
        var selected = this.myself.touch(evt.getLocation());
        if (!selected)
            return;
        var prepareCards = this.myself.prepareCards;
        if (prepareCards.contains(selected)) {
            this.myself.unselectCard(selected);
            this.onCardSelected(selected, false);
        }
        else {
            this.myself.selectCard(selected);
            this.onCardSelected(selected, true);
        }
        if (this.turn != this.myself.seat) {
            return;
        }
        if (prepareCards.kind == CardGroup_1.default.Ungrouped) {
            this.allowFire(false);
            return;
        }
        if (this.commonCards.length() == 0) {
            this.allowFire(true);
            return;
        }
        if (prepareCards.gt(this.commonCards.peek())) {
            this.allowFire(true);
            return;
        }
        this.allowFire(false);
    };
    Game.prototype.onPlayerTimeout = function () {
        this.onStandClicked();
        // this.hideDashboard();
        // if (this.commonCards.length() == 0) {
        //     let cards = Helper.findMinCard(this.myself.cards);
        //     this.onFire(this.myself, new CardGroup([cards], CardGroup.Single));
        //     this.myself.reorder();
        // } else {
        //     return this.onFold(this.myself);
        // }
    };
    Game.prototype.onQuitClicked = function () {
        Api_1.default.logEvent(EventKeys_1.default.QUIT_GAME);
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            this.popup.open(this.node, 1);
        }
        else {
            util_1.default.playAudio(this.audioQuitGame);
            cc.director.loadScene('home');
            Api_1.default.showInterstitialAd();
        }
    };
    Game.prototype.onBackClicked = function () {
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            var coin = Math.max(0, Api_1.default.coin - this.betValue * 10);
            Api_1.default.updateCoin(coin);
        }
        util_1.default.playAudio(this.audioQuitGame);
        cc.director.loadScene('home');
        Api_1.default.showInterstitialAd();
    };
    Game.prototype.showHandCards = function () {
        util_1.default.playAudio(this.audioShowCard);
        // this.myself.showHandCards();
        this.state = Game_1.PLAY;
        // console.log('====================================');
        // console.log(this.myself.cards);
        // console.log('====================================');
        // if (Helper.isBigWin(this.myself.cards)) {
        //     this.node.runAction(cc.sequence(
        //         cc.delayTime(0.7),
        //         cc.callFunc(this.onBigWin, this, this.myself)
        //     ));
        //     return;
        // }
        // this.node.runAction(cc.sequence(
        //     cc.delayTime(0.7),
        //     cc.callFunc(this.myself.sortHandCards, this.myself)
        // ));
        this.players.map(function (player, index) {
            if (player.isBot())
                player.showCardCounter();
            else {
                player.showAllCard();
            }
        });
        var checkBJ = this.players.filter(function (player) {
            player.checkBlackJack();
        });
        // let isUserWin = checkBJ.find(player => player.isUser())
        // let isUserOrDealerWin = checkBJ.find(player => !player.isBot() || player.isCai())
        // if (isUserOrDealerWin) return this.showResult()
        // for (let i = this.getTotalPlayer() - 1; i > 0; --i) {
        //     this.players[i].showCardCounter();
        // }
        if (!this.owner) {
            this.owner = this.findPlayerHasFirstCard();
        }
        this.setCurrentTurn(this.owner, true);
        this.sort.node.active = true;
    };
    Game.prototype.findPlayerHasFirstCard = function () {
        // for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
        //     if (Helper.findCard(this.players[i].cards, 3, 1))
        //         return this.players[i];
        // }
        var userInRound = this.players.filter(function (player) { return player.isInRound() && !player.isCai() && player; });
        return userInRound[0];
    };
    Game.prototype.showDashboard = function (lead) {
        this.allowFold(!lead);
        if (this.commonCards.length() > 0) {
            var cards = Bot_1.default.suggest(this.commonCards, this.myself.cards);
            this.allowFire(!!cards);
        }
        else {
            this.allowFire(true);
        }
        this.fold.node.active = true;
        this.fire.node.active = true;
    };
    Game.prototype.setColor = function (btn, color) {
        btn.target.color = color;
        btn.target.children.forEach(function (child) {
            child.color = color;
        });
    };
    Game.prototype.hideDashboard = function (needShow) {
        if (needShow === void 0) { needShow = false; }
        var actionUser = cc.find('Canvas/actionUser');
        actionUser.active = needShow;
        if (needShow) {
            var showInsurr = this.players[0].cards[0].rank === 1;
            // showInsurr = true
            actionUser.children[4].active = showInsurr;
            var showSplit = !this.myself.isSplit && this.myself.cards[0].rank === this.myself.cards[1].rank;
            showSplit = true;
            if (!showSplit && showInsurr) {
                actionUser.children[4].x = 190.44;
            }
            actionUser.children[3].active = showSplit;
        }
    };
    Game.prototype.allowFold = function (allow) {
        this.fold.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fold, color);
    };
    Game.prototype.allowFire = function (allow) {
        // this.fire.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fire, color);
    };
    Game.prototype.onSortCardClicked = function () {
        util_1.default.playAudio(this.audioSortCard);
        this.myself.sortHandCards();
    };
    Game.prototype.onFireClicked = function () {
        this.onHit();
        // let cards = this.myself.prepareCards;
        // if (cards.count() == 0) { return console.log('empty'); }
        // if (this.commonCards.length() > 0 && !cards.gt(this.commonCards.peek())) {
        //     return console.log('invalid');
        // }
        // if (cards.isInvalid()) {
        //     return console.log('invalid');
        // }
        // this.hideDashboard();
        // this.onFire(this.myself, cards);
        // // resort hand card
        // this.myself.reorder();
    };
    Game.prototype.onFoldClicked = function () {
        // this.hideDashboard();
        // this.onFold(this.myself);
    };
    Game.prototype.deal = function () {
        var _this = this;
        Api_1.default.preloadRewardedVideo();
        if (Api_1.default.coin <= this.betValue) {
            this.popup.open(null, 3);
            Api_1.default.showInterstitialAd();
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
            return;
        }
        this.play.node.active = false;
        this.deck.node.active = true;
        this.deck.shuffle();
        this.commonCards.reset();
        for (var i = 1; i < this.players.length; i++) {
            var bot = this.players[i];
            if (bot.getCoin() <= this.betValue * 3) {
                var profile = this.getProfileBot();
                var rate = 0.5 + Math.random() * 0.5;
                bot.setCoin(Math.floor(this.myself.getCoin() * rate));
                bot.setAvatar(profile.avatar);
                bot.setUsername(profile.username);
            }
        }
        cc.find('Canvas/actionUser/insurr').x = 402.038;
        this.seats[6].hide();
        this.players.map(function (p) {
            p.reset();
            p.updatePoint('0');
            p.setBonusType(0);
        });
        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < this.getTotalPlayer(); j++) {
                var player = this.players[j];
                var card = this.deck.pick();
                player.push(card, 0.3 + (i * this.getTotalPlayer() + j) * Game_1.DEAL_SPEED);
            }
        }
        // this.deck.hide();
        this.node.runAction(cc.sequence(cc.delayTime(Game_1.DEAL_SPEED * this.totalPlayer * 2), cc.callFunc(this.showHandCards, this)));
        this.node.runAction(cc.sequence(cc.delayTime(0.4), cc.callFunc(function () { return util_1.default.playAudio(_this.audioDealCard); })));
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.showEffect = function (player, effect) {
        effect.active = true;
        effect.setPosition(player.node.getPosition());
    };
    Game.prototype.onFold = function (player) {
        util_1.default.playAudio(this.audioFold);
        if (this.commonCards.hasCombat()) {
            var count = this.commonCards.getCombatLength();
            var victim = this.commonCards.getCombatVictim();
            var winner = this.commonCards.getCombatWinner();
            var bigPig = this.commonCards.getCombat();
            var spotWin = Math.pow(2, count - 1) * Helper_1.default.calculateSpotWin(bigPig.cards) * this.betValue;
            var coin = Math.min(spotWin, victim.coinVal);
            winner.addCoin(coin);
            victim.subCoin(coin);
        }
        if (this.commonCards.isCombatOpen()) {
            this.commonCards.resetCombat();
        }
        player.setInRound(false);
        player.setActive(false);
        var players = this.getInRoundPlayers();
        if (players.length >= 2) {
            return this.nextTurn();
        }
        this.nextRound(players[0]);
    };
    Game.prototype.onFire = function (player, cards, isDown) {
        if (isDown === void 0) { isDown = false; }
        var audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        this.node.runAction(cc.sequence(cc.delayTime(0.17), cc.callFunc(function () { return util_1.default.playAudio(audioClip); })));
        player.setActive(false);
        var cardCount = cards.count();
        var pos = this.commonCards.getPosition();
        cards.sort();
        for (var i = cards.count() - 1; i >= 0; --i) {
            if (isDown)
                cards.at(i).show();
            var card = cards.at(i);
            card.node.zIndex = this.commonCards.totalCards + i;
            var move = cc.moveTo(0.3, cc.v2(pos.x + (i - cardCount / 2) * Game_1.CARD_SPACE, pos.y));
            card.node.runAction(move);
            var scale = cc.scaleTo(0.3, 0.6);
            card.node.runAction(scale);
        }
        this.commonCards.push(cards);
        if (cards.highest.rank == 15) {
            this.commonCards.pushCombat(player, cards);
        }
        else if (this.commonCards.isCombatOpen()) {
            this.commonCards.pushCombat(player, cards);
        }
        player.removeCards(cards);
        if (player.isBot()) {
            player.updateCardCounter();
        }
        if (cards.highest.rank == 15) {
            this.notice.showBigPig(cards.count(), 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (3 * 2)) {
            this.notice.show(Notice_1.default.THREE_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (4 * 2)) {
            this.notice.show(Notice_1.default.FOUR_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.House && cards.count() == 4) {
            this.notice.show(Notice_1.default.FOUR_CARD, 3);
        }
        if (player.cards.length > 0) {
            return this.nextTurn();
        }
        this.state = Game_1.LATE;
        this.sort.node.active = false;
        this.node.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(this.showResult, this, player)));
    };
    Game.prototype.onBigWin = function (sender, winner) {
        this.state = Game_1.LATE;
        this.owner = winner;
        this.bigWin.active = true;
        var total = 0;
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player != winner) {
                if (player.isBot()) {
                    player.hideCardCounter();
                    player.showHandCards();
                    player.reorder();
                }
                var lost = 13 * this.betValue;
                lost = Math.min(player.coinVal, lost);
                player.subCoin(lost);
                total += lost;
            }
        }
        winner.addCoin(total);
        Api_1.default.updateCoin(this.myself.coinVal);
        this.onWin(total);
    };
    // showResult(sender: cc.Node, winner: Player) {
    //     let players = this.players.filter((player) => (player != winner));
    //     players.sort((a, b) => { return a.cards.length - b.cards.length; });
    //     this.showEffect(winner, this.resultEffects[0]);
    //     let total = 0;
    //     let ranking = 1;
    //     if (winner == this.myself) ranking = 1;
    //     for (let i = 0; i < players.length; i++) {
    //         let player = players[i];
    //         if (player.isBot()) {
    //             player.hideCardCounter();
    //             player.showHandCards();
    //             player.reorder();
    //         }
    //         if (player == this.myself) ranking = i + 2;
    //         this.showEffect(player, this.resultEffects[i + 1]);
    //         let lost = Helper.calculate(player.cards) * this.betValue;
    //         lost = Math.min(player.coinVal, lost);
    //         player.subCoin(lost);
    //         total += lost;
    //     }
    //     winner.addCoin(total);
    //     Api.updateCoin(this.myself.coinVal);
    //     this.owner = winner;
    //     if (winner == this.myself) {
    //         this.onWin(total);
    //     } else {
    //         this.onLose(ranking);
    //     }
    // }
    Game.prototype.onLose = function (ranking) {
        Api_1.default.logEvent(EventKeys_1.default.LOSE + '_' + ranking);
        util_1.default.playAudio(this.audioLose);
        var rnd = Math.random();
        if (ranking == 2 && rnd <= 0.3) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 3 && rnd <= 0.2) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 4 && rnd <= 0.1) {
            Api_1.default.showInterstitialAd();
        }
        this.delayReset(3);
    };
    Game.prototype.onWin = function (won) {
        Api_1.default.logEvent(EventKeys_1.default.WIN);
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_OPEN);
        util_1.default.playAudio(this.audioWin);
        var rnd = Math.random();
        if (rnd <= 0.3 && Api_1.default.isInterstitialAdLoaded()) {
            Api_1.default.showInterstitialAd();
            this.delayReset(3);
        }
        else {
            this.spin.show(won);
        }
    };
    Game.prototype.onSpinHide = function () {
        this.delayReset(3);
    };
    Game.prototype.onSpinCompleted = function (result, won) {
        var bonus = this.spin.compute(result.id, won);
        if (bonus > 0) {
            this.myself.addCoin(bonus);
            Api_1.default.updateCoin(this.myself.coinVal);
        }
    };
    Game.prototype.delayReset = function (dt) {
        this.node.runAction(cc.sequence(cc.delayTime(dt), cc.callFunc(this.reset, this)));
    };
    Game.prototype.reset = function () {
        for (var i = this.resultEffects.length - 1; i >= 0; --i) {
            this.resultEffects[i].active = false;
        }
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].reset();
        }
        if (this.bigWin.active) {
            this.bigWin.active = false;
        }
        // this.deck.node.active = false;
        this.play.node.active = true;
    };
    Game.prototype.onCardSelected = function (card, selected) {
    };
    Game.prototype.nextTurn = function () {
        var next = this.getNextInRoundPlayer();
        this.setCurrentTurn(next, false);
    };
    Game.prototype.nextRound = function (lead) {
        this.commonCards.nextRound();
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].setInRound(true);
        }
        this.setCurrentTurn(lead, true);
        // util.playAudio(this.audioGarbageCard);
    };
    Game.prototype.onHitClicked = function () {
        var player = this.myself;
        this.takeCard(player);
        player.setActive(true);
        this.hideDashboard();
    };
    Game.prototype.onDoubleClicked = function () {
    };
    Game.prototype.onSplitClicked = function () {
    };
    Game.prototype.onStandClicked = function () {
        var player = this.myself;
        player.setActive(false);
        if (!player.inRound && player.subUser) {
            player = player.subUser;
        }
        player.setInRound(false);
        this.hideDashboard();
        this.nextTurn();
    };
    Game.prototype.setCurrentTurn = function (player, lead) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var canHit;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.players.map(function (p) { return p.setActive(false); });
                        if (!player) {
                            return [2 /*return*/, this.showResult()];
                        }
                        this.turn = player.seat;
                        player.setActive(true);
                        if (player.isUser()) {
                            return [2 /*return*/, Game_1.sleep(Game_1.WAIT_RE_SHOW_USER_ACTION).then(function () {
                                    _this.hideDashboard(true);
                                })];
                        }
                        canHit = player.point > this.players[0].point;
                        if (player.point < 18) {
                            canHit = true;
                        }
                        if (!canHit) {
                            canHit = this.players.find(function (p) { return player.point > p.point; }) != undefined;
                        }
                        if (player.point >= 18)
                            canHit = false;
                        if (((_a = player.cards) === null || _a === void 0 ? void 0 : _a.length) === 5)
                            canHit = false;
                        return [4 /*yield*/, Game_1.sleep(Game_1.WAIT_BOT)];
                    case 1:
                        _b.sent();
                        player.setInRound(false);
                        // setTimeout(() => {
                        if (canHit) {
                            this.takeCard(player);
                        }
                        else {
                            this.toast.show(player.username.string + ' stand' + player.point + " điểm");
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.getInRoundPlayers = function () {
        var players = [];
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            if (this.players[i].isInRound())
                players.push(this.players[i]);
        }
        return players;
    };
    Game.prototype.getNextInRoundPlayer = function () {
        var _this = this;
        // tim thang nao co ghe ngoi > turn trc va dang o trong van choi
        var nextTurn = this.players.find(function (player) {
            var _a;
            return (player.seat === _this.turn && ((_a = player === null || player === void 0 ? void 0 : player.subUser) === null || _a === void 0 ? void 0 : _a.inRound)) || (player.seat > _this.turn && player.isInRound());
        });
        if (!nextTurn && this.players[0].inRound)
            return this.players[0]; // neu khong con ai thi chuyen luot cho nha cai
        // let nextTurn = this.players[this.turn + 1]
        // if (!nextTurn && this.players[0].isInRound()) nextTurn = this.players[0]
        // if (!nextTurn.isInRound()) return
        return nextTurn;
        // for (let i = 1; i < this.getTotalPlayer(); i++) {
        //     let offset = this.turn + i;
        //     if (offset >= this.getTotalPlayer()) offset -= this.getTotalPlayer();
        //     if (this.players[offset].isInRound())
        //         return this.players[offset];
        // }
        // return null;
    };
    Game.prototype.takeCard = function (player, needNext) {
        if (needNext === void 0) { needNext = true; }
        return __awaiter(this, void 0, void 0, function () {
            var card, _player;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = this.deck.pick();
                        this.toast.show(player.username.string + ': ' + player.point + "đ bốc thêm" + card.rank);
                        if (!(card === null || card === void 0 ? void 0 : card.node)) {
                            this.state = Game_1.LATE;
                            this.sort.node.active = false;
                            // this.node.runAction(cc.sequence(
                            //     cc.delayTime(2),
                            //     cc.callFunc(this.showResult, this, player)
                            // ));
                            return [2 /*return*/];
                        }
                        player.push(card, 0);
                        if (!needNext)
                            return [2 /*return*/];
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        return [4 /*yield*/, Game_1.sleep(0)];
                    case 1:
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        _a.sent();
                        if (player.isBot()) {
                            // player.updateCardCounter();
                        }
                        else {
                            player.cards.map(function (el) { return el.show(); });
                        }
                        player.setActive(false);
                        _player = player;
                        if (!player.inRound && player.subUser) {
                            _player = player.subUser;
                        }
                        if (_player.point >= 21 || _player.cards.length === 5) {
                            _player.setInRound(false);
                            _player.point > 21 && this.toast.show(_player.username.string + (" Thua: " + _player.point + " \u0111i\u1EC3m"));
                            // bot stand
                            _player.setActive(false);
                            return [2 /*return*/, this.nextTurn()];
                        }
                        // console.log('====================================');
                        // console.log(player.cards.map(el => el.node.name));
                        // console.log('====================================');
                        // this.checkWin()
                        // this.nextTurn()
                        if (!player.bonusType) {
                            this.setCurrentTurn(player, false);
                        }
                        else {
                            player.setInRound(false);
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.execBot = function (sender, bot) {
        // alert(1)
        var cards = this.commonCards.isEmpty()
            ? Bot_1.default.random(bot.cards)
            : Bot_1.default.suggest(this.commonCards, bot.cards);
        if (!cards) {
            return this.onFold(bot);
        }
        this.onFire(this.players[this.turn], cards, true);
    };
    Game.prototype.getTotalPlayer = function () {
        return this.players.length;
    };
    Game.prototype.getProfileBot = function () {
        for (var i = 0; i < 10; i++) {
            var rnd = Math.floor(Math.random() * this.profileBots.length);
            if (!this.isUsage(this.profileBots[rnd])) {
                return this.profileBots[rnd];
            }
        }
        return this.profileBots[0];
    };
    Game.prototype.isUsage = function (user) {
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].username.string == user.username) {
                return true;
            }
        }
        return false;
    };
    Game.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
    };
    Game.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.show(msg);
        Api_1.default.coinIncrement(bonus);
        this.myself.setCoin(Api_1.default.coin);
        this.popup.close(null, 3);
        // update bet room
        this.betValue = Math.round(Api_1.default.coin * 0.3);
        this.betText.string = util_1.default.numberFormat(this.betValue);
    };
    Game.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.toast.show('Mời bạn chơi không thành công');
        });
    };
    Game.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    Game.prototype.onSetBonusClicked = function (e, bonus) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('====================================');
                        console.log(bonus);
                        console.log('====================================');
                        if (!(bonus == 3)) return [3 /*break*/, 4];
                        this.myself.split(this.seats[6]);
                        this.seats[6].show();
                        this.hideDashboard();
                        return [4 /*yield*/, Game_1.sleep(Game_1.DEAL_SPEED)
                            // this.setCurrentTurn(this.myself, false)
                            // this.onHitClicked()
                        ];
                    case 1:
                        _a.sent();
                        // this.setCurrentTurn(this.myself, false)
                        // this.onHitClicked()
                        this.takeCard(this.myself, false);
                        return [4 /*yield*/, Game_1.sleep(500)];
                    case 2:
                        _a.sent();
                        this.takeCard(this.myself.subUser, false);
                        return [4 /*yield*/, Game_1.sleep(1000)];
                    case 3:
                        _a.sent();
                        this.nextTurn();
                        return [3 /*break*/, 5];
                    case 4:
                        this.myself.setBonusType(bonus);
                        if (bonus == 1) { // double
                            this.onHitClicked();
                            if (this.myself.inRound) {
                                this.myself.inRound = false;
                            }
                            else {
                                this.myself.subUser.inRound = false;
                            }
                        }
                        else {
                            this.onStandClicked();
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    // onSetBonus = (bonus) => {
    //     console.log('====================================');
    //     console.log(bonus);
    //     console.log('====================================');
    //     // this.myself.setBonusType()
    // }
    // cheat
    Game.prototype.cheat_win = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.myself);
    };
    Game.prototype.cheat_lose = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.players[1]);
    };
    var Game_1;
    Game.sleep = function (wait) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve(1);
                    }, wait);
                })];
        });
    }); };
    Game.WAIT = 0; // wait for player
    Game.DEAL = 1; // deal card
    Game.PLAY = 2; // play
    Game.LATE = 3; // late
    // static readonly DEAL_SPEED: number = 0//1.66;
    // static readonly CARD_SPACE: number = 45;
    // static readonly WAIT_BOT: number = 0;
    // static readonly WAIT_RE_SHOW_USER_ACTION: number = 0;
    Game.DEAL_SPEED = 1.2; //1.66;
    Game.CARD_SPACE = 45;
    Game.WAIT_BOT = 2000;
    Game.WAIT_RE_SHOW_USER_ACTION = 1000;
    __decorate([
        property(Deck_1.default)
    ], Game.prototype, "deck", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "play", void 0);
    __decorate([
        property(Player_1.default)
    ], Game.prototype, "seats", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fire", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fold", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "sort", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "resultEffects", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "bigWin", void 0);
    __decorate([
        property(Toast_1.default)
    ], Game.prototype, "toast", void 0);
    __decorate([
        property(Notice_1.default)
    ], Game.prototype, "notice", void 0);
    __decorate([
        property(cc.Label)
    ], Game.prototype, "betText", void 0);
    __decorate([
        property(SpinWheel_1.default)
    ], Game.prototype, "spin", void 0);
    __decorate([
        property(Popup_1.default)
    ], Game.prototype, "popup", void 0);
    __decorate([
        property(Profile)
    ], Game.prototype, "profileBots", void 0);
    __decorate([
        property(cc.Toggle)
    ], Game.prototype, "soundToggle", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFold", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioShowCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioGarbageCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFire", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioSortCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioQuitGame", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioDealCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFireSingle", void 0);
    __decorate([
        property(Modal_1.default)
    ], Game.prototype, "modal", void 0);
    Game = Game_1 = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();