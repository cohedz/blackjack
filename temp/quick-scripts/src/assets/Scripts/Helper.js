"use strict";
cc._RF.push(module, '1879dJgJEVIpqZTqzYVoVpF', 'Helper');
// Scripts/Helper.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.isHouse = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (amount != 0 && cards.length != amount)
            return false;
        if (amount == 0 && (cards.length < 2 || cards.length > 4))
            return false;
        var cardMin = Helper.findMinCard(cards);
        var cardMax = Helper.findMaxCard(cards);
        return cardMin.rank == cardMax.rank;
    };
    Helper.isStraight = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (amount != 0 && cards.length != amount)
            return false;
        if (amount == 0 && cards.length < 3)
            return false;
        var cardMin = Helper.findMinCard(cards);
        for (var i = cards.length - 1; i > 0; --i) {
            if (cardMin.rank + i > 14)
                return false;
            var card = Helper.findCardByRank(cards, cardMin.rank + i);
            if (!card)
                return false;
        }
        return true;
    };
    Helper.isMultiPair = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (cards.length < 6 || cards.length % 2 == 1)
            return false;
        if (amount != 0 && cards.length != 2 * amount)
            return false;
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == 15)
                return false;
            var matched = Helper.findCardByRank(cards, cards[i].rank, cards[i]);
            if (!matched)
                return false;
        }
        return true;
    };
    Helper.isBigWin = function (cards) {
        var reduce = cards.reduce(function (accumulator, currentValue) {
            if (!accumulator[currentValue.rank]) {
                accumulator[currentValue.rank] = [];
            }
            accumulator[currentValue.rank].push(currentValue);
            return accumulator;
        }, {});
        if (!!reduce[15] && reduce[15].length == 4)
            return 1;
        if (Helper.isDragon(reduce))
            return 2;
        if (Helper.pairCount(reduce) == 6)
            return 3;
        return 0;
    };
    Helper.isDragon = function (reduce) {
        for (var i = 3; i <= 14; i++) {
            if (!reduce[i]) {
                return false;
            }
        }
        return true;
    };
    Helper.pairCount = function (reduce) {
        var count = 0;
        for (var v in reduce) {
            if (reduce[v].length >= 2)
                count++;
        }
        return count;
    };
    Helper.sort = function (cards) {
        cards.sort(function (a, b) {
            return a.gt(b) ? 1 : -1;
        });
    };
    ;
    // card utlis
    Helper.findCard = function (cards, rank, suit) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var card = cards[i];
            if (card.rank == rank && card.suit == suit)
                return card;
        }
        return null;
    };
    Helper.findMaxCard = function (cards, filter) {
        if (filter === void 0) { filter = null; }
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
            var c = cards[i];
            if (filter == null || c.lt(filter)) {
                if (card == null)
                    card = c;
                else if (c.gt(card))
                    card = c;
            }
        }
        return card;
    };
    Helper.findMinCard = function (cards, filter) {
        if (filter === void 0) { filter = null; }
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
            var c = cards[i];
            if (filter == null || c.gt(filter)) {
                if (card == null)
                    card = c;
                else if (c.lt(card))
                    card = c;
            }
        }
        return card;
    };
    Helper.findAllCardByRange = function (cards, max, min) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank > min && cards[i].rank <= max) {
                if (!Helper.findCardByRank(setCards, cards[i].rank)) {
                    setCards.push(cards[i]);
                }
            }
        }
        return setCards;
    };
    Helper.findAllCardByRank = function (cards, rank, limit) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
                if (setCards.length == limit)
                    return setCards;
            }
        }
        return setCards;
    };
    Helper.findCardByRank = function (cards, rank, filter) {
        if (filter === void 0) { filter = null; }
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank && cards[i] != filter)
                return cards[i];
        }
        return null;
    };
    // array utlis
    Helper.findIndex = function (arr, target) {
        for (var i = arr.length - 1; i >= 0; --i) {
            if (target == arr[i])
                return i;
        }
        return -1;
    };
    Helper.removeBy = function (arr, target) {
        var idx = Helper.findIndex(arr, target);
        Helper.removeAt(arr, idx);
    };
    Helper.removeAt = function (arr, idx) {
        arr.splice(idx, 1);
    };
    Helper.highlight = function (cards, handCards) {
        return [];
    };
    ;
    Helper.findStraightCard = function (cards, filter) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var cardStarted = cards[i];
            if (cardStarted.rank < 15 && cardStarted.gt(filter.highest)) {
                var straight = Helper.findAllCardByRange(cards, cardStarted.rank, cardStarted.rank - filter.count());
                if (straight.length == filter.count()) {
                    return straight;
                }
            }
        }
        return null;
    };
    Helper.findHouseCard = function (cards, filter, limit) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var cardStarted = cards[i];
            if (cardStarted.gt(filter)) {
                var straight = Helper.findAllCardByRank(cards, cardStarted.rank, limit);
                if (straight.length == limit) {
                    return straight;
                }
            }
        }
        return null;
    };
    Helper.findMultiPairCard = function (cards, filter, limit) {
        // let reduce = cards.reduce(function(accumulator: object, currentValue: Card) {
        //     if (!accumulator[currentValue.rank]) {
        //         accumulator[currentValue.rank] = [];
        //     }
        //     accumulator[currentValue.rank].push(currentValue);
        //     return accumulator;
        // }, {});
        // for (let i = 0; i < cards.length; i++) {
        //     let card = cards[i];
        //     if (card.gt(filter)) {
        //         let multiPairs = [];
        //         for(let j = 0; j < limit; j++) {
        //             let arr: Array<Card> = reduce[card.rank - j];
        //             if (arr.length == 1) break;
        //             multiPairs.push();
        //         }
        //     }
        // }
        return null;
    };
    // caculate
    Helper.calculate = function (cards) {
        var pigs = Helper.findAllCardByRank(cards, 15, 4);
        var spotWin = Helper.calculateSpotWin(pigs);
        return cards.length + spotWin;
    };
    Helper.calculateSpotWin = function (pigs) {
        var total = 0;
        for (var i = pigs.length - 1; i >= 0; --i) {
            total += pigs[i].suit + 2;
        }
        return total;
    };
    return Helper;
}());
exports.default = Helper;

cc._RF.pop();