"use strict";
cc._RF.push(module, '6cfdf+Ec8NOfKCqLqWb3Un0', 'Notice');
// Scripts/Notice.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Alert = /** @class */ (function (_super) {
    __extends(Alert, _super);
    function Alert() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
    }
    Alert_1 = Alert;
    // LIFE-CYCLE CALLBACKS:
    Alert.prototype.onLoad = function () {
        this.label = this.getComponent(cc.Label);
    };
    Alert.prototype.start = function () { };
    Alert.prototype.showBigPig = function (num, duration) {
        if (num == 2) {
            this.show(Alert_1.TWO_PIG, duration);
        }
        else if (num == 3) {
            this.show(Alert_1.THREE_PIG, duration);
        }
        else {
            this.show(Alert_1.ONE_PIG, duration);
        }
    };
    Alert.prototype.show = function (type, duration) {
        this.node.active = true;
        this.label.string = Language_1.default.getInstance().get(type);
        var action = cc.sequence(cc.delayTime(duration), cc.callFunc(this.hide, this));
        this.node.runAction(action);
    };
    Alert.prototype.hide = function () {
        this.node.active = false;
    };
    var Alert_1;
    // update (dt) {}
    Alert.ONE_PIG = '1BEST';
    Alert.TWO_PIG = '2BEST';
    Alert.THREE_PIG = '3BEST';
    Alert.THREE_PAIR = '3PAIRS';
    Alert.FOUR_CARD = 'FOURKIND';
    Alert.FOUR_PAIR = '4PAIRS';
    Alert = Alert_1 = __decorate([
        ccclass
    ], Alert);
    return Alert;
}(cc.Component));
exports.default = Alert;

cc._RF.pop();