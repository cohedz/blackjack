"use strict";
cc._RF.push(module, 'cce96i6QBJMl7PTDBXefIeo', 'Deck');
// Scripts/Deck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Card_1 = require("./Card");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Deck = /** @class */ (function (_super) {
    __extends(Deck, _super);
    function Deck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cards = Array();
        _this.offset = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Deck.prototype.onLoad = function () {
        for (var i = 0; i < 52; i++) {
            var card = this.node.children[i].getComponent(Card_1.default);
            var cardName = card.node.name;
            card.rank = this.getRankFromName(cardName);
            card.suit = this.getSuitFromName(cardName);
            this.cards.push(card);
        }
    };
    // start() { }
    // update (dt) {}
    Deck.prototype.shuffle = function () {
        var length = this.cards.length;
        var lastIndex = length - 1;
        var index = -1;
        while (++index < length) {
            var rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
            var value = this.cards[rand];
            this.cards[rand] = this.cards[index];
            this.cards[index] = value;
        }
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            card.reset();
        }
        this.offset = 0;
        // this.cheat();
    };
    // cheat() {
    //     let pos = 0;
    //     for (let i = this.cards.length - 1; i >= 0; --i) {
    //         let card = this.cards[i];
    //         if (card.rank == 9) {
    //             let tmp = this.cards[pos];
    //             this.cards[pos] = card;
    //             this.cards[i] = tmp;
    //             pos += 4;
    //         }
    //     }
    // }
    Deck.prototype.pick = function () {
        var card = this.cards[this.offset];
        ++this.offset;
        return card;
    };
    Deck.prototype.hide = function () {
        for (var i = this.offset; i < this.cards.length; i++) {
            this.cards[i].node.active = false;
        }
    };
    Deck.prototype.getRankFromName = function (cardName) {
        if (cardName.length == 3)
            return 10;
        switch (cardName[0]) {
            case 'J': return 11;
            case 'Q': return 12;
            case 'K': return 13;
            case 'A': return 1;
            // case '2': return 15;
            default: return parseInt(cardName[0]);
        }
    };
    Deck.prototype.getSuitFromName = function (cardName) {
        switch (cardName[cardName.length - 1]) {
            case 'S': return 1;
            case 'C': return 2;
            case 'D': return 3;
            case 'H': return 4;
            default: throw new Error(cardName);
        }
    };
    Deck = __decorate([
        ccclass
    ], Deck);
    return Deck;
}(cc.Component));
exports.default = Deck;

cc._RF.pop();