"use strict";
cc._RF.push(module, 'daacdf5d5lMhrNHqrN6niHA', 'SpinWheel');
// Scripts/SpinWheel.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var Popup_1 = require("./Popup");
var Toast_1 = require("./Toast");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SpinWheel = /** @class */ (function (_super) {
    __extends(SpinWheel, _super);
    function SpinWheel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.wheel = null;
        _this.ticket = null;
        _this.popup = null;
        _this.toast = null;
        _this.btnRun = null;
        _this.btnAd = null;
        _this.audioSpinWin = null;
        _this.audioSpinLose = null;
        _this.audioSpinRun = null;
        _this.modal = null;
        _this.won = 0;
        _this.isRunning = false;
        return _this;
    }
    SpinWheel_1 = SpinWheel;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    SpinWheel.prototype.start = function () {
    };
    // update (dt) {}
    SpinWheel.prototype.show = function (won) {
        var playNow = Api_1.default.ticket > 0;
        this.popup.open(this.node, 2);
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
        this.node.active = true;
        this.isRunning = false;
        this.btnAd.node.active = !playNow;
        this.btnRun.node.active = playNow;
        this.won = won;
    };
    SpinWheel.prototype.hide = function () {
        this.popup.close(this.node, 2);
    };
    SpinWheel.prototype.play = function () {
        if (this.isRunning || Api_1.default.ticket <= 0)
            return;
        Api_1.default.ticket--;
        this.updateTicket();
        this.spin();
        Api_1.default.logEvent(EventKeys_1.default.SPIN);
    };
    SpinWheel.prototype.spin = function () {
        util_1.default.playAudio(this.audioSpinRun);
        this.isRunning = true;
        var result = this.randomPrize();
        var action = cc.rotateBy(5, 360 * 10 + result.angle - 30 - (this.wheel.node.angle % 360));
        this.wheel.node.runAction(cc.sequence(action.easing(cc.easeCubicActionOut()), cc.callFunc(this.onCompleted, this, result)));
    };
    SpinWheel.prototype.showVideoAd = function () {
        var _this = this;
        if (this.isRunning)
            return;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.spin();
        }, function (msg) {
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_ERROR);
            _this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
        });
    };
    SpinWheel.prototype.onCompleted = function (sender, data) {
        var msg = Language_1.default.getInstance().get(data.msg);
        this.toast.show(msg);
        this.onSpinCompleted(data, this.won);
        if (data.id == 1) {
            this.node.runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(this.spin, this)));
        }
        else {
            this.node.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(this.hide, this)));
            util_1.default.playAudio(data.id == 0 ? this.audioSpinLose : this.audioSpinWin);
        }
        // if(Api.ticket == 0) {
        //     this.btnAd.node.active = true;
        //     this.btnRun.node.active = false;
        // }
    };
    SpinWheel.prototype.onDisable = function () {
        this.onSpinHide();
    };
    SpinWheel.prototype.compute = function (id, val) {
        switch (id) {
            case 2: return val;
            case 3: return val * 2;
            case 4: return val * 4;
            case 5: return val * 9;
            default: return 0;
        }
    };
    SpinWheel.prototype.onClose = function () {
        if (this.isRunning)
            return;
        Popup_1.default.instance.close(null, 2);
    };
    SpinWheel.prototype.randomPrize = function () {
        var rate = Math.random() * 100;
        var sample = SpinWheel_1.result_rate;
        for (var i = 0; i < sample.length; i++)
            if (sample[i].rate > rate)
                return sample[i];
        return sample[0];
    };
    SpinWheel.prototype.updateTicket = function () {
        Api_1.default.updateTicket();
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
    };
    var SpinWheel_1;
    //Tỷ lệ vòng quay : X10 ( 5% ) , X5 ( 10% ) , X3 ( 20% ) , THÊM LƯỢT ( 15% ) , MẤT LƯỢT ( 15% ) , X2 (35%)
    SpinWheel.result_rate = [
        { id: 0, angle: 4 * 60, rate: 15, msg: "LUCK" },
        { id: 1, angle: 5 * 60, rate: 30, msg: "TURN" },
        { id: 2, angle: 0 * 60, rate: 65, msg: "X2" },
        { id: 3, angle: 1 * 60, rate: 85, msg: "X3" },
        { id: 4, angle: 2 * 60, rate: 90, msg: "X5" },
        { id: 5, angle: 3 * 60, rate: 100, msg: "X10" },
    ];
    __decorate([
        property(cc.Sprite)
    ], SpinWheel.prototype, "wheel", void 0);
    __decorate([
        property(cc.Label)
    ], SpinWheel.prototype, "ticket", void 0);
    __decorate([
        property(Popup_1.default)
    ], SpinWheel.prototype, "popup", void 0);
    __decorate([
        property(Toast_1.default)
    ], SpinWheel.prototype, "toast", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnRun", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnAd", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinRun", void 0);
    __decorate([
        property(Modal_1.default)
    ], SpinWheel.prototype, "modal", void 0);
    SpinWheel = SpinWheel_1 = __decorate([
        ccclass
    ], SpinWheel);
    return SpinWheel;
}(cc.Component));
exports.default = SpinWheel;

cc._RF.pop();