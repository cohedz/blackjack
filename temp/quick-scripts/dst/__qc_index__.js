
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Api');
require('./assets/Scripts/AudioPlayer');
require('./assets/Scripts/AutoHide');
require('./assets/Scripts/Bot');
require('./assets/Scripts/Card');
require('./assets/Scripts/CardGroup');
require('./assets/Scripts/CommonCard');
require('./assets/Scripts/Config');
require('./assets/Scripts/Deck');
require('./assets/Scripts/EventKeys');
require('./assets/Scripts/Game');
require('./assets/Scripts/HandCard');
require('./assets/Scripts/Helper');
require('./assets/Scripts/Home');
require('./assets/Scripts/LBEntry');
require('./assets/Scripts/Language');
require('./assets/Scripts/Leaderboard');
require('./assets/Scripts/Notice');
require('./assets/Scripts/Player');
require('./assets/Scripts/Popup');
require('./assets/Scripts/Shop');
require('./assets/Scripts/Slider2');
require('./assets/Scripts/SpinWheel');
require('./assets/Scripts/Text2');
require('./assets/Scripts/Timer');
require('./assets/Scripts/Toast');
require('./assets/Scripts/image');
require('./assets/Scripts/popop/Modal');
require('./assets/Scripts/popop/bonus/DailyBonus');
require('./assets/Scripts/popop/bonus/DailyBonusItem');
require('./assets/Scripts/tween/TweenMove');
require('./assets/Scripts/util');
require('./assets/lang/en_US');
require('./assets/lang/en_US_tut');
require('./assets/lang/th_TH');
require('./assets/lang/th_TH_tut');
require('./assets/lang/tl_PH');
require('./assets/lang/tl_PH_tut');
require('./assets/lang/vi_VN');
require('./assets/lang/vi_VN_tut');
require('./assets/migration/use_reversed_rotateBy');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();