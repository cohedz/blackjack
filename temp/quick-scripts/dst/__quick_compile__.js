
(function () {
var scripts = [{"deps":{"./assets/Scripts/AutoHide":11,"./assets/Scripts/Bot":5,"./assets/Scripts/Card":13,"./assets/Scripts/CardGroup":12,"./assets/Scripts/CommonCard":9,"./assets/Scripts/Config":8,"./assets/Scripts/Deck":14,"./assets/Scripts/EventKeys":27,"./assets/Scripts/Game":40,"./assets/Scripts/HandCard":15,"./assets/Scripts/Helper":28,"./assets/Scripts/Home":1,"./assets/Scripts/LBEntry":17,"./assets/Scripts/Leaderboard":26,"./assets/Scripts/Language":16,"./assets/Scripts/Notice":22,"./assets/Scripts/Player":2,"./assets/Scripts/Popup":19,"./assets/Scripts/Slider2":18,"./assets/Scripts/Shop":20,"./assets/Scripts/SpinWheel":21,"./assets/Scripts/Text2":25,"./assets/Scripts/Timer":29,"./assets/Scripts/Toast":23,"./assets/Scripts/image":41,"./assets/Scripts/util":24,"./assets/Scripts/Api":4,"./assets/Scripts/popop/Modal":31,"./assets/Scripts/popop/bonus/DailyBonusItem":3,"./assets/Scripts/popop/bonus/DailyBonus":30,"./assets/Scripts/tween/TweenMove":6,"./assets/lang/en_US_tut":7,"./assets/lang/th_TH_tut":32,"./assets/lang/th_TH":33,"./assets/lang/tl_PH_tut":35,"./assets/lang/tl_PH":34,"./assets/lang/vi_VN_tut":36,"./assets/lang/vi_VN":37,"./assets/lang/en_US":38,"./assets/migration/use_reversed_rotateBy":10,"./assets/Scripts/AudioPlayer":39},"path":"preview-scripts/__qc_index__.js"},{"deps":{"./Api":4,"./Slider2":18,"./Config":8,"./Leaderboard":26,"./util":24,"./Popup":19,"./popop/bonus/DailyBonus":30,"./EventKeys":27,"./AutoHide":11,"./Language":16,"./popop/Modal":31},"path":"preview-scripts/assets/Scripts/Home.js"},{"deps":{"./Helper":28,"./Timer":29,"./CardGroup":12,"./tween/TweenMove":6,"./util":24,"./Config":8,"./Game":40},"path":"preview-scripts/assets/Scripts/Player.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/popop/bonus/DailyBonusItem.js"},{"deps":{"./Config":8,"./image":41},"path":"preview-scripts/assets/Scripts/Api.js"},{"deps":{"./Helper":28,"./CardGroup":12},"path":"preview-scripts/assets/Scripts/Bot.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/tween/TweenMove.js"},{"deps":{},"path":"preview-scripts/assets/lang/en_US_tut.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Config.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/CommonCard.js"},{"deps":{},"path":"preview-scripts/assets/migration/use_reversed_rotateBy.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/AutoHide.js"},{"deps":{"./Helper":28},"path":"preview-scripts/assets/Scripts/CardGroup.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Card.js"},{"deps":{"./Card":13},"path":"preview-scripts/assets/Scripts/Deck.js"},{"deps":{"./Card":13},"path":"preview-scripts/assets/Scripts/HandCard.js"},{"deps":{"../lang/vi_VN":37,"../lang/th_TH":33,"../lang/en_US":38,"../lang/tl_PH":34},"path":"preview-scripts/assets/Scripts/Language.js"},{"deps":{"./util":24,"./Api":4},"path":"preview-scripts/assets/Scripts/LBEntry.js"},{"deps":{"./util":24,"./Config":8},"path":"preview-scripts/assets/Scripts/Slider2.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Popup.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Shop.js"},{"deps":{"./util":24,"./Api":4,"./Popup":19,"./Toast":23,"./popop/Modal":31,"./EventKeys":27,"./Language":16},"path":"preview-scripts/assets/Scripts/SpinWheel.js"},{"deps":{"./Language":16},"path":"preview-scripts/assets/Scripts/Notice.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Toast.js"},{"deps":{"./Api":4,"./Config":8},"path":"preview-scripts/assets/Scripts/util.js"},{"deps":{"./Language":16},"path":"preview-scripts/assets/Scripts/Text2.js"},{"deps":{"./LBEntry":17},"path":"preview-scripts/assets/Scripts/Leaderboard.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/EventKeys.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Helper.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/Timer.js"},{"deps":{"../../Popup":19,"../../Config":8,"../../Api":4,"./DailyBonusItem":3,"../../AutoHide":11,"../../util":24,"../../EventKeys":27},"path":"preview-scripts/assets/Scripts/popop/bonus/DailyBonus.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/popop/Modal.js"},{"deps":{},"path":"preview-scripts/assets/lang/th_TH_tut.js"},{"deps":{"./th_TH_tut":32},"path":"preview-scripts/assets/lang/th_TH.js"},{"deps":{"./th_TH_tut":32},"path":"preview-scripts/assets/lang/tl_PH.js"},{"deps":{},"path":"preview-scripts/assets/lang/tl_PH_tut.js"},{"deps":{},"path":"preview-scripts/assets/lang/vi_VN_tut.js"},{"deps":{"./vi_VN_tut":36},"path":"preview-scripts/assets/lang/vi_VN.js"},{"deps":{"./en_US_tut":7},"path":"preview-scripts/assets/lang/en_US.js"},{"deps":{"./Config":8},"path":"preview-scripts/assets/Scripts/AudioPlayer.js"},{"deps":{"./Player":2,"./Helper":28,"./Deck":14,"./CardGroup":12,"./Toast":23,"./CommonCard":9,"./Notice":22,"./Api":4,"./util":24,"./Config":8,"./SpinWheel":21,"./Popup":19,"./Bot":5,"./popop/Modal":31,"./Language":16,"./EventKeys":27},"path":"preview-scripts/assets/Scripts/Game.js"},{"deps":{},"path":"preview-scripts/assets/Scripts/image.js"}];
var entries = ["preview-scripts/__qc_index__.js"];
var bundleScript = 'preview-scripts/__qc_bundle__.js';

/**
 * Notice: This file can not use ES6 (for IE 11)
 */
var modules = {};
var name2path = {};

// Will generated by module.js plugin
// var scripts = ${scripts};
// var entries = ${entries};
// var bundleScript = ${bundleScript};

if (typeof global === 'undefined') {
    window.global = window;
}

var isJSB = typeof jsb !== 'undefined';

function getXMLHttpRequest () {
    return window.XMLHttpRequest ? new window.XMLHttpRequest() : new ActiveXObject('MSXML2.XMLHTTP');
}

function downloadText(url, callback) {
    if (isJSB) {
        var result = jsb.fileUtils.getStringFromFile(url);
        callback(null, result);
        return;
    }

    var xhr = getXMLHttpRequest(),
        errInfo = 'Load text file failed: ' + url;
    xhr.open('GET', url, true);
    if (xhr.overrideMimeType) xhr.overrideMimeType('text\/plain; charset=utf-8');
    xhr.onload = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200 || xhr.status === 0) {
                callback(null, xhr.responseText);
            }
            else {
                callback({status:xhr.status, errorMessage:errInfo + ', status: ' + xhr.status});
            }
        }
        else {
            callback({status:xhr.status, errorMessage:errInfo + '(wrong readyState)'});
        }
    };
    xhr.onerror = function(){
        callback({status:xhr.status, errorMessage:errInfo + '(error)'});
    };
    xhr.ontimeout = function(){
        callback({status:xhr.status, errorMessage:errInfo + '(time out)'});
    };
    xhr.send(null);
};

function loadScript (src, cb) {
    if (typeof require !== 'undefined') {
        require(src);
        return cb();
    }

    // var timer = 'load ' + src;
    // console.time(timer);

    var scriptElement = document.createElement('script');

    function done() {
        // console.timeEnd(timer);
        // deallocation immediate whatever
        scriptElement.remove();
    }

    scriptElement.onload = function () {
        done();
        cb();
    };
    scriptElement.onerror = function () {
        done();
        var error = 'Failed to load ' + src;
        console.error(error);
        cb(new Error(error));
    };
    scriptElement.setAttribute('type','text/javascript');
    scriptElement.setAttribute('charset', 'utf-8');
    scriptElement.setAttribute('src', src);

    document.head.appendChild(scriptElement);
}

function loadScripts (srcs, cb) {
    var n = srcs.length;

    srcs.forEach(function (src) {
        loadScript(src, function () {
            n--;
            if (n === 0) {
                cb();
            }
        });
    })
}

function formatPath (path) {
    let destPath = window.__quick_compile_project__.destPath;
    if (destPath) {
        let prefix = 'preview-scripts';
        if (destPath[destPath.length - 1] === '/') {
            prefix += '/';
        }
        path = path.replace(prefix, destPath);
    }
    return path;
}

window.__quick_compile_project__ = {
    destPath: '',

    registerModule: function (path, module) {
        path = formatPath(path);
        modules[path].module = module;
    },

    registerModuleFunc: function (path, func) {
        path = formatPath(path);
        modules[path].func = func;

        var sections = path.split('/');
        var name = sections[sections.length - 1];
        name = name.replace(/\.(?:js|ts|json)$/i, '');
        name2path[name] = path;
    },

    require: function (request, path) {
        var m, requestScript;

        path = formatPath(path);
        if (path) {
            m = modules[path];
            if (!m) {
                console.warn('Can not find module for path : ' + path);
                return null;
            }
        }

        if (m) {
            let depIndex = m.deps[request];
            // dependence script was excluded
            if (depIndex === -1) {
                return null;
            }
            else {
                requestScript = scripts[ m.deps[request] ];
            }
        }
        
        let requestPath = '';
        if (!requestScript) {
            // search from name2path when request is a dynamic module name
            if (/^[\w- .]*$/.test(request)) {
                requestPath = name2path[request];
            }

            if (!requestPath) {
                if (CC_JSB) {
                    return require(request);
                }
                else {
                    console.warn('Can not find deps [' + request + '] for path : ' + path);
                    return null;
                }
            }
        }
        else {
            requestPath = formatPath(requestScript.path);
        }

        let requestModule = modules[requestPath];
        if (!requestModule) {
            console.warn('Can not find request module for path : ' + requestPath);
            return null;
        }

        if (!requestModule.module && requestModule.func) {
            requestModule.func();
        }

        if (!requestModule.module) {
            console.warn('Can not find requestModule.module for path : ' + path);
            return null;
        }

        return requestModule.module.exports;
    },

    run: function () {
        entries.forEach(function (entry) {
            entry = formatPath(entry);
            var module = modules[entry];
            if (!module.module) {
                module.func();
            }
        });
    },

    load: function (cb) {
        var self = this;

        var srcs = scripts.map(function (script) {
            var path = formatPath(script.path);
            modules[path] = script;

            if (script.mtime) {
                path += ("?mtime=" + script.mtime);
            }
            return path;
        });

        console.time && console.time('load __quick_compile_project__');
        // jsb can not analysis sourcemap, so keep separate files.
        if (bundleScript && !isJSB) {
            downloadText(formatPath(bundleScript), function (err, bundleSource) {
                console.timeEnd && console.timeEnd('load __quick_compile_project__');
                if (err) {
                    console.error(err);
                    return;
                }

                let evalTime = 'eval __quick_compile_project__ : ' + srcs.length + ' files';
                console.time && console.time(evalTime);
                var sources = bundleSource.split('\n//------QC-SOURCE-SPLIT------\n');
                for (var i = 0; i < sources.length; i++) {
                    if (sources[i]) {
                        window.eval(sources[i]);
                        // not sure why new Function cannot set breakpoints precisely
                        // new Function(sources[i])()
                    }
                }
                self.run();
                console.timeEnd && console.timeEnd(evalTime);
                cb();
            })
        }
        else {
            loadScripts(srcs, function () {
                self.run();
                console.timeEnd && console.timeEnd('load __quick_compile_project__');
                cb();
            });
        }
    }
};

// Polyfill for IE 11
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}
})();
    