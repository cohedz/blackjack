
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Api');
require('./assets/Scripts/AudioPlayer');
require('./assets/Scripts/AutoHide');
require('./assets/Scripts/Bot');
require('./assets/Scripts/Card');
require('./assets/Scripts/CardGroup');
require('./assets/Scripts/CommonCard');
require('./assets/Scripts/Config');
require('./assets/Scripts/Deck');
require('./assets/Scripts/EventKeys');
require('./assets/Scripts/Game');
require('./assets/Scripts/HandCard');
require('./assets/Scripts/Helper');
require('./assets/Scripts/Home');
require('./assets/Scripts/LBEntry');
require('./assets/Scripts/Language');
require('./assets/Scripts/Leaderboard');
require('./assets/Scripts/Notice');
require('./assets/Scripts/Player');
require('./assets/Scripts/Popup');
require('./assets/Scripts/Shop');
require('./assets/Scripts/Slider2');
require('./assets/Scripts/SpinWheel');
require('./assets/Scripts/Text2');
require('./assets/Scripts/Timer');
require('./assets/Scripts/Toast');
require('./assets/Scripts/image');
require('./assets/Scripts/popop/Modal');
require('./assets/Scripts/popop/bonus/DailyBonus');
require('./assets/Scripts/popop/bonus/DailyBonusItem');
require('./assets/Scripts/tween/TweenMove');
require('./assets/Scripts/util');
require('./assets/lang/en_US');
require('./assets/lang/en_US_tut');
require('./assets/lang/th_TH');
require('./assets/lang/th_TH_tut');
require('./assets/lang/tl_PH');
require('./assets/lang/tl_PH_tut');
require('./assets/lang/vi_VN');
require('./assets/lang/vi_VN_tut');
require('./assets/migration/use_reversed_rotateBy');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Home.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2c5ff9R/FJPip0TZkafs6WJ', 'Home');
// Scripts/Home.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Slider2_1 = require("./Slider2");
var Config_1 = require("./Config");
var Leaderboard_1 = require("./Leaderboard");
var util_1 = require("./util");
var Popup_1 = require("./Popup");
var DailyBonus_1 = require("./popop/bonus/DailyBonus");
var AutoHide_1 = require("./AutoHide");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Home = /** @class */ (function (_super) {
    __extends(Home, _super);
    function Home() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.avatar = null;
        _this.playerName = null;
        _this.coin = null;
        _this.betSetting = null;
        _this.leaderboard = null;
        _this.music = null;
        _this.dailyBonus = null;
        _this.popup = null;
        _this.toast = null;
        _this.modal = null;
        _this.playTime = 0;
        _this.countTime = 0;
        _this.itvCountTime = null;
        _this.preloadSceneGame = function () {
            cc.director.preloadScene('game', function (completedCount, totalCount, item) {
                // console.log('====================================');
                // console.log('preload', completedCount, totalCount, item);
                // console.log('====================================');
            }, function (error) {
                console.log('====================================');
                console.log('loaded game');
                console.log('====================================');
            });
        };
        _this.onChallenge = function (playerId, photo, username, coin) { return __awaiter(_this, void 0, void 0, function () {
            var error_1, minCoin;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Api_1.default.coin <= Config_1.default.bankrupt) {
                            Api_1.default.preloadRewardedVideo();
                            this.popup.open(this.node, 4);
                            Api_1.default.showInterstitialAd();
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, Api_1.default.challenge(playerId)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 4];
                    case 4:
                        if (Math.random() < 0.7) {
                            Api_1.default.showInterstitialAd();
                        }
                        cc.audioEngine.stopMusic();
                        Config_1.default.battle = { photo: photo, username: username, coin: coin };
                        minCoin = Math.min(Math.max(coin, 100000), Api_1.default.coin);
                        Config_1.default.betValue = Math.floor(minCoin * 0.3);
                        cc.director.loadScene('game');
                        Api_1.default.logEvent(EventKeys_1.default.PLAY_WITH_FRIEND);
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    Home.prototype.initCountTime = function () {
        var _this = this;
        this.itvCountTime = setInterval(function () {
            _this.countTime += Config_1.default.stepOfCountTime / 1000;
            util_1.default.logTimeInGame(_this.countTime);
        }, Config_1.default.stepOfCountTime);
    };
    Home.prototype.clearLogTime = function () {
        this.itvCountTime && clearInterval(this.itvCountTime);
        this.itvCountTime = null;
        this.countTime = 0;
    };
    // init logic
    Home.prototype.start = function () {
        var _this = this;
        util_1.default.logTimeInGame(0);
        this.initCountTime();
        this.betSetting.onValueChange = this.onBetChange;
        Api_1.default.initAsync(function (bonus, day) {
            Language_1.default.getInstance().load(Api_1.default.locale);
            cc.systemEvent.emit('LANG_CHAN');
            _this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
            _this.playerName.string = Api_1.default.username;
            cc.assetManager.loadRemote(Api_1.default.photo, function (err, tex) {
                _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
                Config_1.default.userphoto = tex;
            });
            if (bonus) {
                Api_1.default.preloadRewardedVideo(function () {
                    Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_SHOW);
                    _this.dailyBonus.show(day, Api_1.default.dailyBonusClaimed);
                });
                cc.systemEvent.on('claim_daily_bonus', _this.onClaimDailyBonus, _this);
                cc.systemEvent.on('claim_daily_bonus_fail', _this.onClaimDailyBonusFail, _this);
            }
        }, this.leaderboard);
        cc.systemEvent.on('lb_battle', this.onChallenge, this);
        cc.systemEvent.on('lb_share', this.onShare, this);
        cc.audioEngine.setMusicVolume(0.3);
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        // this.scheduleOnce(this.preloadSceneGame, 1);
        setTimeout(function () {
            _this.preloadSceneGame();
        }, 1);
        Api_1.default.preloadInterstitialAd();
    };
    Home.prototype.onShare = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Api_1.default.shareAsync()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Home.prototype.onClaimDailyBonus = function (day, bonus, extra) {
        if (bonus.coin) {
            Api_1.default.coinIncrement(extra ? 2 * bonus.coin : bonus.coin);
            this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        }
        if (bonus.ticket) {
            Api_1.default.ticket += extra ? 2 * bonus.ticket : bonus.ticket;
        }
        Api_1.default.claimDailyBonus(day);
    };
    Home.prototype.onClaimDailyBonusFail = function () {
        this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
    };
    Home.prototype.openPlayPopupClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_NOW);
        if (Api_1.default.coin > Config_1.default.bankrupt) {
            this.popup.open(this.node, 1);
        }
        else {
            Api_1.default.preloadRewardedVideo();
            Api_1.default.showInterstitialAd();
            this.popup.open(this.node, 4);
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
        }
    };
    Home.prototype.onPlayClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_ACCEPT);
        Config_1.default.battle = null;
        cc.audioEngine.stopMusic();
        cc.director.loadScene('game');
    };
    Home.prototype.onShopClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.SHOP_GO);
        cc.director.loadScene('shop');
    };
    Home.prototype.onDailyBonusClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.DAILY_GIFT_CLICK);
        this.dailyBonus.show(Api_1.default.dailyBonusDay, Api_1.default.dailyBonusClaimed);
        Api_1.default.showInterstitialAd();
    };
    Home.prototype.onBetChange = function (value) {
        Config_1.default.betValue = value;
    };
    Home.prototype.onBotChange = function (sender, params) {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_MODE + '-' + params);
        if (sender.isChecked) {
            Config_1.default.totalPlayer = parseInt(params);
        }
    };
    Home.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        else {
            cc.audioEngine.stopMusic();
        }
    };
    Home.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.openWithText(null, msg);
        Api_1.default.coinIncrement(bonus);
        this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        this.popup.close(null, 4);
    };
    Home.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 2);
            _this.toast.openWithText(null, 'Mời bạn chơi không thành công');
        });
    };
    Home.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    __decorate([
        property(cc.Sprite)
    ], Home.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "coin", void 0);
    __decorate([
        property(Slider2_1.default)
    ], Home.prototype, "betSetting", void 0);
    __decorate([
        property(Leaderboard_1.default)
    ], Home.prototype, "leaderboard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Home.prototype, "music", void 0);
    __decorate([
        property(DailyBonus_1.default)
    ], Home.prototype, "dailyBonus", void 0);
    __decorate([
        property(Popup_1.default)
    ], Home.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], Home.prototype, "toast", void 0);
    __decorate([
        property(Modal_1.default)
    ], Home.prototype, "modal", void 0);
    Home = __decorate([
        ccclass
    ], Home);
    return Home;
}(cc.Component));
exports.default = Home;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0hvbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkJBQXdCO0FBQ3hCLHFDQUFnQztBQUNoQyxtQ0FBOEI7QUFDOUIsNkNBQXdDO0FBQ3hDLCtCQUEwQjtBQUMxQixpQ0FBNEI7QUFDNUIsdURBQWtEO0FBQ2xELHVDQUFrQztBQUNsQyx1Q0FBa0M7QUFDbEMseUNBQW9DO0FBQ3BDLHVDQUFrQztBQUU1QixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFrQyx3QkFBWTtJQUE5QztRQUFBLHFFQXlOQztRQXZORyxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBRXpCLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBRTVCLFVBQUksR0FBYSxJQUFJLENBQUM7UUFFdEIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFFM0IsaUJBQVcsR0FBZ0IsSUFBSSxDQUFDO1FBRWhDLFdBQUssR0FBaUIsSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUN0QixrQkFBWSxHQUFHLElBQUksQ0FBQztRQXlEcEIsc0JBQWdCLEdBQUc7WUFDZixFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQzNCLFVBQUMsY0FBYyxFQUFFLFVBQVUsRUFBRSxJQUFJO2dCQUM3Qix1REFBdUQ7Z0JBQ3ZELDREQUE0RDtnQkFDNUQsdURBQXVEO1lBQzNELENBQUMsRUFDRCxVQUFDLEtBQUs7Z0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDeEQsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLENBQUE7UUFNRCxpQkFBVyxHQUFHLFVBQU8sUUFBZ0IsRUFBRSxLQUFtQixFQUFFLFFBQWdCLEVBQUUsSUFBWTs7Ozs7d0JBQ3RGLElBQUksYUFBRyxDQUFDLElBQUksSUFBSSxnQkFBTSxDQUFDLFFBQVEsRUFBRTs0QkFDN0IsYUFBRyxDQUFDLG9CQUFvQixFQUFFLENBQUM7NEJBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7NEJBQzlCLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDOzRCQUN6QixzQkFBTzt5QkFDVjs7Ozt3QkFHRyxxQkFBTSxhQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzs7Ozt3QkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFLLENBQUMsQ0FBQTs7O3dCQUVwQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLEVBQUU7NEJBQ3JCLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO3lCQUM1Qjt3QkFFRCxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO3dCQUMzQixnQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssT0FBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7d0JBQ3RDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUFFLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDekQsZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7d0JBQzVDLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUM5QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs7OzthQUM1QyxDQUFBOztJQStGTCxDQUFDO0lBL0xHLDRCQUFhLEdBQWI7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxTQUFTLElBQUksZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFBO1lBQy9DLGNBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3RDLENBQUMsRUFBRSxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFFRCwyQkFBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFlBQVksSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO0lBQ3RCLENBQUM7SUFFRCxhQUFhO0lBQ2Isb0JBQUssR0FBTDtRQUFBLGlCQXVDQztRQXJDRyxjQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUVwQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2pELGFBQUcsQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFjLEVBQUUsR0FBVztZQUN0QyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxhQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsYUFBRyxDQUFDLFFBQVEsQ0FBQztZQUN0QyxFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBZSxhQUFHLENBQUMsS0FBSyxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7Z0JBQ3pELEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbEQsZ0JBQU0sQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQzNCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsYUFBRyxDQUFDLG9CQUFvQixDQUFDO29CQUNyQixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsdUJBQXVCLENBQUMsQ0FBQztvQkFDaEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUMsQ0FBQztnQkFDSCxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxLQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLENBQUM7Z0JBQ3JFLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLHdCQUF3QixFQUFFLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxLQUFJLENBQUMsQ0FBQzthQUNqRjtRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFckIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFbEQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkMsSUFBSSxnQkFBTSxDQUFDLFdBQVcsRUFBRTtZQUNwQixFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzlDO1FBRUQsK0NBQStDO1FBQy9DLFVBQVUsQ0FBQztZQUNQLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO1FBQzNCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBRWhDLENBQUM7SUFpQkssc0JBQU8sR0FBYjs7Ozs0QkFDSSxxQkFBTSxhQUFHLENBQUMsVUFBVSxFQUFFLEVBQUE7O3dCQUF0QixTQUFzQixDQUFDOzs7OztLQUMxQjtJQTBCRCxnQ0FBaUIsR0FBakIsVUFBa0IsR0FBVyxFQUFFLEtBQXVDLEVBQUUsS0FBYztRQUNsRixJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDWixhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsRDtRQUNELElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLGFBQUcsQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztTQUN6RDtRQUNELGFBQUcsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELG9DQUFxQixHQUFyQjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELGlDQUFrQixHQUFsQjtRQUNJLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqQyxJQUFJLGFBQUcsQ0FBQyxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNqQzthQUFNO1lBQ0gsYUFBRyxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDM0IsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7SUFFRCwwQkFBVyxHQUFYO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BDLGdCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNyQixFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCwwQkFBVyxHQUFYO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFHLENBQUMsYUFBYSxFQUFFLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9ELGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCwwQkFBVyxHQUFYLFVBQVksS0FBYTtRQUNyQixnQkFBTSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVELDBCQUFXLEdBQVgsVUFBWSxNQUFpQixFQUFFLE1BQWM7UUFDekMsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFDakQsSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ2xCLGdCQUFNLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFRCw0QkFBYSxHQUFiLFVBQWMsTUFBaUIsRUFBRSxJQUFhO1FBQzFDLGdCQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUV2QyxJQUFJLGdCQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3BCLEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDOUM7YUFBTTtZQUNILEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBRUQsbUNBQW9CLEdBQXBCLFVBQXFCLEtBQWE7UUFDOUIsSUFBTSxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEVBQUUsY0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNuQyxhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsMkJBQVksR0FBWjtRQUFBLGlCQVFDO1FBUEcsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3RDLGFBQUcsQ0FBQyxNQUFNLENBQUM7WUFDUCxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUU7WUFDQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLCtCQUErQixDQUFDLENBQUM7UUFDbkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUJBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEcsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDbEQsYUFBRyxDQUFDLGlCQUFpQixDQUFDO1lBQ2xCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBRTtZQUNDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM3QyxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUF0TkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt3Q0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzRDQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0NBQ0c7SUFFdEI7UUFEQyxRQUFRLENBQUMsaUJBQU8sQ0FBQzs0Q0FDUztJQUUzQjtRQURDLFFBQVEsQ0FBQyxxQkFBVyxDQUFDOzZDQUNVO0lBRWhDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzt1Q0FDTjtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzRDQUNTO0lBRTlCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt1Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDO3VDQUNJO0lBRXZCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt1Q0FDSTtJQXBCSCxJQUFJO1FBRHhCLE9BQU87T0FDYSxJQUFJLENBeU54QjtJQUFELFdBQUM7Q0F6TkQsQUF5TkMsQ0F6TmlDLEVBQUUsQ0FBQyxTQUFTLEdBeU43QztrQkF6Tm9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IFNsaWRlcjIgZnJvbSBcIi4vU2xpZGVyMlwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCBMZWFkZXJib2FyZCBmcm9tIFwiLi9MZWFkZXJib2FyZFwiO1xuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IFBvcHVwIGZyb20gXCIuL1BvcHVwXCI7XG5pbXBvcnQgRGFpbHlCb251cyBmcm9tIFwiLi9wb3BvcC9ib251cy9EYWlseUJvbnVzXCI7XG5pbXBvcnQgQXV0b0hpZGUgZnJvbSBcIi4vQXV0b0hpZGVcIjtcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi9wb3BvcC9Nb2RhbFwiO1xuaW1wb3J0IEV2ZW50S2V5cyBmcm9tIFwiLi9FdmVudEtleXNcIjtcbmltcG9ydCBMYW5ndWFnZSBmcm9tIFwiLi9MYW5ndWFnZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSG9tZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBsYXllck5hbWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgY29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShTbGlkZXIyKVxuICAgIGJldFNldHRpbmc6IFNsaWRlcjIgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShMZWFkZXJib2FyZClcbiAgICBsZWFkZXJib2FyZDogTGVhZGVyYm9hcmQgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIG11c2ljOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShEYWlseUJvbnVzKVxuICAgIGRhaWx5Qm9udXM6IERhaWx5Qm9udXMgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShBdXRvSGlkZSlcbiAgICB0b2FzdDogQXV0b0hpZGUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShNb2RhbClcbiAgICBtb2RhbDogTW9kYWwgPSBudWxsO1xuXG4gICAgcGxheVRpbWU6IG51bWJlciA9IDA7XG4gICAgY291bnRUaW1lOiBudW1iZXIgPSAwO1xuICAgIGl0dkNvdW50VGltZSA9IG51bGw7XG5cbiAgICBpbml0Q291bnRUaW1lKCkge1xuICAgICAgICB0aGlzLml0dkNvdW50VGltZSA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY291bnRUaW1lICs9IENvbmZpZy5zdGVwT2ZDb3VudFRpbWUgLyAxMDAwXG4gICAgICAgICAgICB1dGlsLmxvZ1RpbWVJbkdhbWUodGhpcy5jb3VudFRpbWUpXG4gICAgICAgIH0sIENvbmZpZy5zdGVwT2ZDb3VudFRpbWUpXG4gICAgfVxuXG4gICAgY2xlYXJMb2dUaW1lKCkge1xuICAgICAgICB0aGlzLml0dkNvdW50VGltZSAmJiBjbGVhckludGVydmFsKHRoaXMuaXR2Q291bnRUaW1lKTtcbiAgICAgICAgdGhpcy5pdHZDb3VudFRpbWUgPSBudWxsO1xuICAgICAgICB0aGlzLmNvdW50VGltZSA9IDBcbiAgICB9XG5cbiAgICAvLyBpbml0IGxvZ2ljXG4gICAgc3RhcnQoKSB7XG5cbiAgICAgICAgdXRpbC5sb2dUaW1lSW5HYW1lKDApO1xuICAgICAgICB0aGlzLmluaXRDb3VudFRpbWUoKVxuXG4gICAgICAgIHRoaXMuYmV0U2V0dGluZy5vblZhbHVlQ2hhbmdlID0gdGhpcy5vbkJldENoYW5nZTtcbiAgICAgICAgQXBpLmluaXRBc3luYygoYm9udXM6IGJvb2xlYW4sIGRheTogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmxvYWQoQXBpLmxvY2FsZSk7XG4gICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KCdMQU5HX0NIQU4nKTtcbiAgICAgICAgICAgIHRoaXMuY29pbi5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdChBcGkuY29pbik7XG4gICAgICAgICAgICB0aGlzLnBsYXllck5hbWUuc3RyaW5nID0gQXBpLnVzZXJuYW1lO1xuICAgICAgICAgICAgY2MuYXNzZXRNYW5hZ2VyLmxvYWRSZW1vdGU8Y2MuVGV4dHVyZTJEPihBcGkucGhvdG8sIChlcnIsIHRleCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuYXZhdGFyLnNwcml0ZUZyYW1lID0gbmV3IGNjLlNwcml0ZUZyYW1lKHRleCk7XG4gICAgICAgICAgICAgICAgQ29uZmlnLnVzZXJwaG90byA9IHRleDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKGJvbnVzKSB7XG4gICAgICAgICAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9EQUlMWV9SRVdBUkRfU0hPVyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGFpbHlCb251cy5zaG93KGRheSwgQXBpLmRhaWx5Qm9udXNDbGFpbWVkKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignY2xhaW1fZGFpbHlfYm9udXMnLCB0aGlzLm9uQ2xhaW1EYWlseUJvbnVzLCB0aGlzKTtcbiAgICAgICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignY2xhaW1fZGFpbHlfYm9udXNfZmFpbCcsIHRoaXMub25DbGFpbURhaWx5Qm9udXNGYWlsLCB0aGlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgdGhpcy5sZWFkZXJib2FyZCk7XG5cbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oJ2xiX2JhdHRsZScsIHRoaXMub25DaGFsbGVuZ2UsIHRoaXMpO1xuICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignbGJfc2hhcmUnLCB0aGlzLm9uU2hhcmUsIHRoaXMpO1xuXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnNldE11c2ljVm9sdW1lKDAuMyk7XG4gICAgICAgIGlmIChDb25maWcuc291bmRFbmFibGUpIHtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlNdXNpYyh0aGlzLm11c2ljLCB0cnVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHRoaXMuc2NoZWR1bGVPbmNlKHRoaXMucHJlbG9hZFNjZW5lR2FtZSwgMSk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5wcmVsb2FkU2NlbmVHYW1lKClcbiAgICAgICAgfSwgMSk7XG4gICAgICAgIEFwaS5wcmVsb2FkSW50ZXJzdGl0aWFsQWQoKTtcblxuICAgIH1cblxuICAgIHByZWxvYWRTY2VuZUdhbWUgPSAoKSA9PiB7XG4gICAgICAgIGNjLmRpcmVjdG9yLnByZWxvYWRTY2VuZSgnZ2FtZScsXG4gICAgICAgICAgICAoY29tcGxldGVkQ291bnQsIHRvdGFsQ291bnQsIGl0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ3ByZWxvYWQnLCBjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCwgaXRlbSk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbG9hZGVkIGdhbWUnKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgYXN5bmMgb25TaGFyZSgpIHtcbiAgICAgICAgYXdhaXQgQXBpLnNoYXJlQXN5bmMoKTtcbiAgICB9XG5cbiAgICBvbkNoYWxsZW5nZSA9IGFzeW5jIChwbGF5ZXJJZDogc3RyaW5nLCBwaG90bzogY2MuVGV4dHVyZTJELCB1c2VybmFtZTogc3RyaW5nLCBjb2luOiBudW1iZXIpID0+IHtcbiAgICAgICAgaWYgKEFwaS5jb2luIDw9IENvbmZpZy5iYW5rcnVwdCkge1xuICAgICAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCk7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLm9wZW4odGhpcy5ub2RlLCA0KTtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCBBcGkuY2hhbGxlbmdlKHBsYXllcklkKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHsgY29uc29sZS5sb2coZXJyb3IpIH1cblxuICAgICAgICBpZiAoTWF0aC5yYW5kb20oKSA8IDAuNykge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc3RvcE11c2ljKCk7XG4gICAgICAgIENvbmZpZy5iYXR0bGUgPSB7IHBob3RvLCB1c2VybmFtZSwgY29pbiB9O1xuICAgICAgICBsZXQgbWluQ29pbiA9IE1hdGgubWluKE1hdGgubWF4KGNvaW4sIDEwMDAwMCksIEFwaS5jb2luKTtcbiAgICAgICAgQ29uZmlnLmJldFZhbHVlID0gTWF0aC5mbG9vcihtaW5Db2luICogMC4zKTtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdnYW1lJyk7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9XSVRIX0ZSSUVORCk7XG4gICAgfVxuXG4gICAgb25DbGFpbURhaWx5Qm9udXMoZGF5OiBudW1iZXIsIGJvbnVzOiB7IGNvaW46IG51bWJlciwgdGlja2V0OiBudW1iZXIgfSwgZXh0cmE6IGJvb2xlYW4pIHtcbiAgICAgICAgaWYgKGJvbnVzLmNvaW4pIHtcbiAgICAgICAgICAgIEFwaS5jb2luSW5jcmVtZW50KGV4dHJhID8gMiAqIGJvbnVzLmNvaW4gOiBib251cy5jb2luKTtcbiAgICAgICAgICAgIHRoaXMuY29pbi5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdChBcGkuY29pbik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGJvbnVzLnRpY2tldCkge1xuICAgICAgICAgICAgQXBpLnRpY2tldCArPSBleHRyYSA/IDIgKiBib251cy50aWNrZXQgOiBib251cy50aWNrZXQ7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLmNsYWltRGFpbHlCb251cyhkYXkpO1xuICAgIH1cblxuICAgIG9uQ2xhaW1EYWlseUJvbnVzRmFpbCgpIHtcbiAgICAgICAgdGhpcy5tb2RhbC5zaG93KExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdOT1ZJREVPJykpO1xuICAgIH1cblxuICAgIG9wZW5QbGF5UG9wdXBDbGljaygpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QTEFZX05PVyk7XG4gICAgICAgIGlmIChBcGkuY29pbiA+IENvbmZpZy5iYW5rcnVwdCkge1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBBcGkucHJlbG9hZFJld2FyZGVkVmlkZW8oKTtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgICAgIHRoaXMucG9wdXAub3Blbih0aGlzLm5vZGUsIDQpO1xuICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9DT0lOX09QRU4pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25QbGF5Q2xpY2soKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9BQ0NFUFQpO1xuICAgICAgICBDb25maWcuYmF0dGxlID0gbnVsbDtcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc3RvcE11c2ljKCk7XG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnZ2FtZScpO1xuICAgIH1cblxuICAgIG9uU2hvcENsaWNrKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlNIT1BfR08pO1xuICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoJ3Nob3AnKTtcbiAgICB9XG5cbiAgICBvbkRhaWx5Qm9udXNDbGljaygpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5EQUlMWV9HSUZUX0NMSUNLKTtcbiAgICAgICAgdGhpcy5kYWlseUJvbnVzLnNob3coQXBpLmRhaWx5Qm9udXNEYXksIEFwaS5kYWlseUJvbnVzQ2xhaW1lZCk7XG4gICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICB9XG5cbiAgICBvbkJldENoYW5nZSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIENvbmZpZy5iZXRWYWx1ZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIG9uQm90Q2hhbmdlKHNlbmRlcjogY2MuVG9nZ2xlLCBwYXJhbXM6IHN0cmluZykge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfTU9ERSArICctJyArIHBhcmFtcyk7XG4gICAgICAgIGlmIChzZW5kZXIuaXNDaGVja2VkKSB7XG4gICAgICAgICAgICBDb25maWcudG90YWxQbGF5ZXIgPSBwYXJzZUludChwYXJhbXMpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25Tb3VuZFRvZ2dsZShzZW5kZXI6IGNjLlRvZ2dsZSwgaXNPbjogYm9vbGVhbikge1xuICAgICAgICBDb25maWcuc291bmRFbmFibGUgPSAhc2VuZGVyLmlzQ2hlY2tlZDtcblxuICAgICAgICBpZiAoQ29uZmlnLnNvdW5kRW5hYmxlKSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5TXVzaWModGhpcy5tdXNpYywgdHJ1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wTXVzaWMoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNsYWltQmFua3J1cHRjeU1vbmV5KGJvbnVzOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgbXNnID0gY2MuanMuZm9ybWF0U3RyKExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdNT05FWTEnKSwgdXRpbC5udW1iZXJGb3JtYXQoYm9udXMpKTtcbiAgICAgICAgdGhpcy50b2FzdC5vcGVuV2l0aFRleHQobnVsbCwgbXNnKTtcbiAgICAgICAgQXBpLmNvaW5JbmNyZW1lbnQoYm9udXMpO1xuICAgICAgICB0aGlzLmNvaW4uc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLmNvaW4pO1xuICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDQpO1xuICAgIH1cblxuICAgIGludml0ZUZpcmVuZCgpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5JTlZJVEVfRlJJRU5EKTtcbiAgICAgICAgQXBpLmludml0ZSgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsYWltQmFua3J1cHRjeU1vbmV5KENvbmZpZy5iYW5rcnVwdF9ib251cyk7XG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMucG9wdXAuY2xvc2UobnVsbCwgMik7XG4gICAgICAgICAgICB0aGlzLnRvYXN0Lm9wZW5XaXRoVGV4dChudWxsLCAnTeG7nWkgYuG6oW4gY2jGoWkga2jDtG5nIHRow6BuaCBjw7RuZycpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhZFJld2FyZCgpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9DT0lOX0NMSUNLKTtcbiAgICAgICAgQXBpLnNob3dSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xhaW1CYW5rcnVwdGN5TW9uZXkoQ29uZmlnLmJhbmtydXB0X2JvbnVzKTtcbiAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShBcGkucmFuZG9tQm9udXMoKSk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fRVJST1IpO1xuICAgICAgICB9KVxuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Player.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4cea4WuKU1GD7EAlqpK5ThR', 'Player');
// Scripts/Player.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var Timer_1 = require("./Timer");
var TweenMove_1 = require("./tween/TweenMove");
var util_1 = require("./util");
var Config_1 = require("./Config");
var Game_1 = require("./Game");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fold = null;
        _this.timer = null;
        _this.coin = null;
        _this.username = null;
        _this.marker = null;
        _this.avatar = null;
        _this.effect = null;
        _this.pointLbl = null;
        _this.seat = 0;
        _this.cards = [];
        _this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        _this.inRound = true;
        _this.coinVal = 1;
        _this.cardMargin = 0;
        _this.cardRootPosition = null;
        _this.cardDirection = null;
        _this.labelCardCounter = null;
        _this.numOfAce = 0;
        _this.point = 0;
        _this.bonusType = 0; // 0 nothing,  1 double, 2 insurr
        _this.isSplit = 0;
        _this.subUser = null;
        _this.isSubUser = false;
        _this.setBonusType = function (bonusType) {
            try {
                var self = _this;
                var txtBonusUser = 'Canvas/user/bonus';
                if (!_this.inRound) {
                    self = _this.subUser;
                    txtBonusUser = 'Canvas/subUser/bonus';
                }
                self.bonusType = bonusType;
                var bonusLbl = cc.find(txtBonusUser);
                bonusLbl.active = !!bonusType;
                var str = bonusType == 1 ? 'x2' : '/2';
                bonusLbl.getComponent(cc.Label).string = str;
            }
            catch (error) {
            }
        };
        _this.showAllCard = function () {
            _this.cards.map(function (card) {
                card.show();
            });
        };
        _this.changeCoin = function (coin) {
            var color = cc.color(0xf9, 0xd2, 0x1e, 255), addString = '+';
            if (coin < 0) {
                color = cc.color(255, 0, 0, 255), addString = '-';
            }
            _this.effect.getComponent(cc.Label).string = addString + util_1.default.numberFormat(Math.abs(coin));
            _this.effect.node.color = color;
            _this.effect.play();
            _this.setCoin(_this.coinVal + coin);
        };
        _this.split = function (subUser) {
            _this.subUser = subUser;
            _this.subUser.isSubUser = true;
            if (_this.isSplit)
                return;
            _this.isSplit = 1;
            if (_this.cards[1].rank === 1)
                _this.numOfAce--;
            _this.cards.map(function (card, index) {
                if (index === 1) {
                    return _this.subUser.push(card, 0);
                }
                card.node.zIndex = index === 1 ? -1 : index;
                var pos = _this.getCardRootPosition();
                pos.x += index < 2 ? 0 : index * _this.cardMargin;
                var move = cc.sequence(cc.delayTime(Game_1.default.DEAL_SPEED), cc.moveTo(0.2, pos));
                card.node.runAction(move);
            });
            setTimeout(function () {
                _this.cards = _this.cards.filter(function (card, index) { return index !== 1; });
            }, 0);
            _this.updateCardCounter();
            var point = _this.getPoint();
            _this.updatePoint(point.toString());
        };
        _this.showCard2 = function () {
            // if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            //     console.log('====================================');
            //     console.log(123);
            //     console.log('====================================');
            //     this.cards[1].show()
            // }
        };
        _this.getPoint = function () {
            var point = _this.cards.reduce(function (point, current) {
                if (current.rank === 1) {
                    return point;
                }
                else if (current.rank > 10) {
                    return point + 10;
                }
                else {
                    return point + current.rank;
                }
            }, 0);
            if (_this.numOfAce > 0) {
                if (point > 11) {
                    point += _this.numOfAce * 1;
                }
                else {
                    point += _this.numOfAce * 11;
                }
            }
            _this.point = point;
            return point;
            // if (point > 21) {
            //     let txtThua = `$user thua`
            //     if (this.isCai()) {
            //         return alert('cai thua: ' + point + ' diem')
            //     } else if (this.isBot()) {
            //         alert('bot thua: ' + point + ' diem')
            //     } else {
            //         alert('user thua: ' + point + ' diem')
            //     }
            // }
        };
        _this.checkBlackJack = function () {
            var _a, _b;
            var rank1 = (_a = _this.cards[0]) === null || _a === void 0 ? void 0 : _a.rank;
            var rank2 = (_b = _this.cards[1]) === null || _b === void 0 ? void 0 : _b.rank;
            if (rank1 === 1 || rank2 === 1) {
                if (rank1 > 9 || rank2 > 9) {
                    _this.setInRound(false);
                    _this.updatePoint("Black Jack!");
                    return true;
                }
            }
            return false;
        };
        return _this;
    }
    Player_1 = Player;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Player.prototype.start = function () {
        this.fold.active = false;
        this.timer.hide();
        this.marker.active = false;
        // this.cardMargin = this.seat == 0 ? Player.CARD_MARGIN :
        //     0.5 * Player.CARD_MARGIN;
        this.cardMargin = 0.5 * Player_1.CARD_MARGIN;
        this.cardDirection = this.seat == 1 ? -1 : 1;
        this.labelCardCounter = this.marker.getComponentInChildren(cc.Label);
    };
    // update (dt) {}
    Player.prototype.show = function () {
        this.node.active = true;
    };
    Player.prototype.hide = function () {
        this.node.active = false;
    };
    Player.prototype.isBot = function () {
        return this.seat !== Config_1.default.totalPlayer / 2;
        return this.seat > 0;
    };
    Player.prototype.isUser = function () {
        return this.isSubUser || this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.isCai = function () {
        return this.seat === 0;
        return this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.setInRound = function (inRound) {
        this.fold.active = !inRound;
        this.inRound = inRound;
    };
    Player.prototype.isInRound = function () {
        return this.inRound;
    };
    Player.prototype.addCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '+' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(0xf9, 0xd2, 0x1e, 255);
        this.effect.play();
        this.setCoin(this.coinVal + val);
    };
    Player.prototype.subCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '-' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(255, 0, 0, 255);
        this.effect.play();
        this.setCoin(this.coinVal - val);
    };
    Player.prototype.setCoin = function (val) {
        this.coinVal = val;
        this.coin.string = util_1.default.numberFormat(val);
    };
    Player.prototype.getCoin = function () {
        return this.coinVal;
    };
    Player.prototype.setUsername = function (val) {
        this.username.string = val;
    };
    Player.prototype.setAvatar = function (sprite) {
        this.avatar.spriteFrame = sprite;
    };
    Player.prototype.reset = function () {
        this.marker.active = false;
        this.fold.active = false;
        this.cards = [];
        this.isSubUser = false;
        this.inRound = true;
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        this.timer.hide();
        this.numOfAce = 0;
        this.point = 0;
        this.bonusType = 0;
        this.isSplit = 0;
        if (this.subUser) {
            this.subUser.reset();
            this.subUser = null;
        }
    };
    Player.prototype.push = function (card, delay) {
        var _this = this;
        if (!this.inRound && this.subUser)
            return this.subUser.push(card, delay);
        card.node.zIndex = this.cards.length;
        this.cards.push(card);
        if (card.rank === 1)
            this.numOfAce += 1;
        var point = this.getPoint();
        var fun = function () {
            _this.updatePoint(point == 21 && _this.cards.length === 2 ? 'BlackJack' : point.toString());
            if (point > 20)
                _this.inRound = false;
            _this.cards[0].show();
            if (_this.isUser()) {
                card.show();
            }
            else {
                _this.updateCardCounter();
            }
        };
        if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            fun = function () {
                _this.cards[1].show();
                _this.updatePoint(null);
            };
        }
        // this.updatePoint()
        var pos = this.getCardRootPosition();
        var scale = cc.sequence(cc.delayTime(delay), cc.scaleTo(0.2, 0.5));
        card.node.runAction(scale);
        // if (this.isBot()) {
        //     if (this.cards.length < 3) {
        //         pos.x += (this.cards.length - 1) * this.cardMargin;
        //     } else {
        //         pos.x += 2 * this.cardMargin;
        //     }
        // } else {
        pos.x += (this.cards.length - 1) * this.cardMargin;
        // }
        var move = cc.sequence(cc.delayTime(delay), cc.moveTo(0.2, pos), cc.callFunc(fun));
        card.node.runAction(move);
    };
    Player.prototype.touch = function (pos) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            if (card.node.getBoundingBoxToWorld().contains(pos)) {
                return card;
            }
        }
        return null;
    };
    Player.prototype.selectCard = function (card) {
        // this.prepareCards.push(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y + 30);
    };
    Player.prototype.unselectCard = function (card) {
        // this.prepareCards.remove(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y);
    };
    Player.prototype.removeCards = function (cards) {
        for (var i = cards.count() - 1; i >= 0; --i) {
            Helper_1.default.removeBy(this.cards, cards.at(i));
        }
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
    };
    Player.prototype.setActive = function (on) {
        if (on) {
            this.timer.show(Player_1.TIME);
        }
        else {
            this.timer.hide();
        }
    };
    Player.prototype.setTimeCallback = function (selector, selectorTarget, data) {
        this.timer.onCompleted(selector, selectorTarget, data);
    };
    Player.prototype.showHandCards = function () {
        for (var i = 0; i < this.cards.length; i++) {
            this.cards[i].show();
        }
    };
    Player.prototype.reorder = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var pos = this.getCardRootPosition();
            pos.x += i * this.cardMargin * this.cardDirection;
            var card = this.cards[i];
            card.node.setPosition(pos);
            card.node.zIndex = this.seat == 1 ? this.cards.length - i : i;
        }
    };
    Player.prototype.sortHandCards = function () {
        Helper_1.default.sort(this.cards);
        this.reorder();
    };
    Player.prototype.showCardCounter = function () {
        this.marker.active = true;
        this.updateCardCounter();
    };
    Player.prototype.hideCardCounter = function () {
        this.marker.active = false;
    };
    Player.prototype.updateCardCounter = function () {
        try {
            this.labelCardCounter.string = this.cards.length.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.updatePoint = function (label) {
        if (Number(this.pointLbl.string) === NaN)
            return;
        this.point = this.getPoint();
        try {
            this.pointLbl.string = label || this.point.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.getCardRootPosition = function () {
        if (!this.cardRootPosition) {
            this.cardRootPosition = cc.v2(this.marker.position.x * this.node.scaleX + this.node.position.x, this.marker.position.y * this.node.scaleY + this.node.position.y);
        }
        return cc.v2(this.cardRootPosition);
    };
    var Player_1;
    Player.CARD_MARGIN = 60;
    Player.TIME = 30;
    __decorate([
        property(cc.Node)
    ], Player.prototype, "fold", void 0);
    __decorate([
        property(Timer_1.default)
    ], Player.prototype, "timer", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "coin", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "username", void 0);
    __decorate([
        property(cc.Node)
    ], Player.prototype, "marker", void 0);
    __decorate([
        property(cc.Sprite)
    ], Player.prototype, "avatar", void 0);
    __decorate([
        property(TweenMove_1.default)
    ], Player.prototype, "effect", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "pointLbl", void 0);
    Player = Player_1 = __decorate([
        ccclass
    ], Player);
    return Player;
}(cc.Component));
exports.default = Player;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1BsYXllci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBOEI7QUFFOUIseUNBQW9DO0FBQ3BDLGlDQUE0QjtBQUM1QiwrQ0FBMEM7QUFDMUMsK0JBQTBCO0FBQzFCLG1DQUE4QjtBQUM5QiwrQkFBMEI7QUFHMUIsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFFN0YsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBb0MsMEJBQVk7SUFBaEQ7UUFBQSxxRUFtWkM7UUFqWkcsVUFBSSxHQUFZLElBQUksQ0FBQztRQUVyQixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLFVBQUksR0FBYSxJQUFJLENBQUM7UUFFdEIsY0FBUSxHQUFhLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRTFCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFDakIsV0FBSyxHQUFnQixFQUFFLENBQUM7UUFDeEIsa0JBQVksR0FBYyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakUsYUFBTyxHQUFZLElBQUksQ0FBQztRQUN4QixhQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLHNCQUFnQixHQUFZLElBQUksQ0FBQztRQUNqQyxtQkFBYSxHQUFXLElBQUksQ0FBQztRQUM3QixzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFDbEMsY0FBUSxHQUFXLENBQUMsQ0FBQTtRQUNwQixXQUFLLEdBQVcsQ0FBQyxDQUFBO1FBRWpCLGVBQVMsR0FBVyxDQUFDLENBQUEsQ0FBQyxpQ0FBaUM7UUFDdkQsYUFBTyxHQUFXLENBQUMsQ0FBQTtRQUVuQixhQUFPLEdBQVcsSUFBSSxDQUFBO1FBQ3RCLGVBQVMsR0FBWSxLQUFLLENBQUE7UUFnQjFCLGtCQUFZLEdBQUcsVUFBQyxTQUFpQjtZQUU3QixJQUFJO2dCQUNBLElBQUksSUFBSSxHQUFHLEtBQUksQ0FBQTtnQkFDZixJQUFJLFlBQVksR0FBRyxtQkFBbUIsQ0FBQTtnQkFDdEMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2YsSUFBSSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUE7b0JBQ25CLFlBQVksR0FBRyxzQkFBc0IsQ0FBQTtpQkFDeEM7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7Z0JBRTFCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3JDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQTtnQkFDN0IsSUFBSSxHQUFHLEdBQUcsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7Z0JBQ3RDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7YUFDL0M7WUFBQyxPQUFPLEtBQUssRUFBRTthQUVmO1FBQ0wsQ0FBQyxDQUFBO1FBUUQsaUJBQVcsR0FBRztZQUNWLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDZixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7WUFDZixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQTtRQTZCRCxnQkFBVSxHQUFHLFVBQUMsSUFBWTtZQUN0QixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDN0QsSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFO2dCQUNWLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLFNBQVMsR0FBRyxHQUFHLENBQUE7YUFDcEQ7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxRixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBQzlCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQTtRQW1ERCxXQUFLLEdBQUcsVUFBQyxPQUFlO1lBRXBCLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO1lBQ3RCLEtBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQTtZQUU3QixJQUFJLEtBQUksQ0FBQyxPQUFPO2dCQUFFLE9BQU07WUFDeEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUE7WUFDaEIsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDO2dCQUFFLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUM3QyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUN2QixJQUFJLEtBQUssS0FBSyxDQUFDLEVBQUU7b0JBQ2IsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUE7aUJBQ3BDO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7Z0JBQzNDLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUNyQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQ2pELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxjQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDM0UsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUE7WUFDRixVQUFVLENBQUM7Z0JBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxLQUFLLEtBQUssQ0FBQyxFQUFYLENBQVcsQ0FBQyxDQUFBO1lBQ2hFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVOLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO1lBQ3hCLElBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO1FBRXRDLENBQUMsQ0FBQTtRQW9ERCxlQUFTLEdBQUc7WUFDUixrRUFBa0U7WUFDbEUsMkRBQTJEO1lBQzNELHdCQUF3QjtZQUN4QiwyREFBMkQ7WUFDM0QsMkJBQTJCO1lBQzNCLElBQUk7UUFDUixDQUFDLENBQUE7UUE0RkQsY0FBUSxHQUFHO1lBQ1AsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQyxLQUFLLEVBQUUsT0FBTztnQkFDekMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtvQkFDcEIsT0FBTyxLQUFLLENBQUE7aUJBQ2Y7cUJBQU0sSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLEVBQUUsRUFBRTtvQkFDMUIsT0FBTyxLQUFLLEdBQUcsRUFBRSxDQUFBO2lCQUNwQjtxQkFBTTtvQkFDSCxPQUFPLEtBQUssR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFBO2lCQUM5QjtZQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVOLElBQUksS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQUU7Z0JBQ25CLElBQUksS0FBSyxHQUFHLEVBQUUsRUFBRTtvQkFDWixLQUFLLElBQUksS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUE7aUJBQzdCO3FCQUFNO29CQUNILEtBQUssSUFBSSxLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtpQkFDOUI7YUFDSjtZQUNELEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBQ2xCLE9BQU8sS0FBSyxDQUFBO1lBQ1osb0JBQW9CO1lBQ3BCLGlDQUFpQztZQUNqQywwQkFBMEI7WUFDMUIsdURBQXVEO1lBQ3ZELGlDQUFpQztZQUNqQyxnREFBZ0Q7WUFDaEQsZUFBZTtZQUNmLGlEQUFpRDtZQUNqRCxRQUFRO1lBQ1IsSUFBSTtRQUNSLENBQUMsQ0FBQTtRQUVELG9CQUFjLEdBQUc7O1lBQ2IsSUFBSSxLQUFLLFNBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsMENBQUUsSUFBSSxDQUFBO1lBQy9CLElBQUksS0FBSyxTQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLDBDQUFFLElBQUksQ0FBQTtZQUUvQixJQUFJLEtBQUssS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFDNUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7b0JBQ3hCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7b0JBQ3RCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUE7b0JBQy9CLE9BQU8sSUFBSSxDQUFBO2lCQUNkO2FBQ0o7WUFDRCxPQUFPLEtBQUssQ0FBQTtRQUNoQixDQUFDLENBQUE7O0lBWUwsQ0FBQztlQW5ab0IsTUFBTTtJQW1DdkIsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZixzQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLDBEQUEwRDtRQUMxRCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBTSxDQUFDLFdBQVcsQ0FBQztRQUMzQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHNCQUFzQixDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBc0JELGlCQUFpQjtJQUVqQixxQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFRRCxxQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLGdCQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUM1QyxPQUFPLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCx1QkFBTSxHQUFOO1FBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztRQUN2QixPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCwyQkFBVSxHQUFWLFVBQVcsT0FBZ0I7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7SUFDM0IsQ0FBQztJQUVELDBCQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQWNELHdCQUFPLEdBQVAsVUFBUSxHQUFXO1FBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsd0JBQU8sR0FBUCxVQUFRLEdBQVc7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCx3QkFBTyxHQUFQLFVBQVEsR0FBVztRQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELHdCQUFPLEdBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELDRCQUFXLEdBQVgsVUFBWSxHQUFXO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztJQUMvQixDQUFDO0lBRUQsMEJBQVMsR0FBVCxVQUFVLE1BQXNCO1FBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztJQUNyQyxDQUFDO0lBRUQsc0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO1FBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUE7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7U0FDdEI7SUFDTCxDQUFDO0lBOEJELHFCQUFJLEdBQUosVUFBSyxJQUFVLEVBQUUsS0FBYTtRQUE5QixpQkFnREM7UUEvQ0csSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUV4RSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQztZQUFFLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFBO1FBRXZDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtRQUUzQixJQUFJLEdBQUcsR0FBRztZQUNOLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFJLEVBQUUsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7WUFDekYsSUFBSSxLQUFLLEdBQUcsRUFBRTtnQkFBRSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTtZQUNwQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO1lBQ3BCLElBQUksS0FBSSxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTthQUFFO2lCQUM3QjtnQkFDRCxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTthQUMzQjtRQUNMLENBQUMsQ0FBQTtRQUVELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUMxRCxHQUFHLEdBQUc7Z0JBQ0YsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtnQkFDcEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUMxQixDQUFDLENBQUE7U0FDSjtRQUdELHFCQUFxQjtRQUNyQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUdyQyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixzQkFBc0I7UUFFdEIsbUNBQW1DO1FBQ25DLDhEQUE4RDtRQUM5RCxlQUFlO1FBQ2Ysd0NBQXdDO1FBQ3hDLFFBQVE7UUFHUixXQUFXO1FBQ1gsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbkQsSUFBSTtRQUVKLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQVdELHNCQUFLLEdBQUwsVUFBTSxHQUFZO1FBQ2QsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDakQsT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELDJCQUFVLEdBQVYsVUFBVyxJQUFVO1FBQ2pCLGdDQUFnQztRQUNoQyxpQ0FBaUM7UUFDakMseUVBQXlFO0lBQzdFLENBQUM7SUFFRCw2QkFBWSxHQUFaLFVBQWEsSUFBVTtRQUNuQixrQ0FBa0M7UUFDbEMsaUNBQWlDO1FBQ2pDLG9FQUFvRTtJQUN4RSxDQUFDO0lBRUQsNEJBQVcsR0FBWCxVQUFZLEtBQWdCO1FBQ3hCLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3pDLGdCQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDBCQUFTLEdBQVQsVUFBVSxFQUFXO1FBQ2pCLElBQUksRUFBRSxFQUFFO1lBQ0osSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hDO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQztJQUVELGdDQUFlLEdBQWYsVUFBZ0IsUUFBa0IsRUFBRSxjQUFtQixFQUFFLElBQVM7UUFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQsOEJBQWEsR0FBYjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELHdCQUFPLEdBQVA7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQzdDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQ3JDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNsRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNqRTtJQUNMLENBQUM7SUFFRCw4QkFBYSxHQUFiO1FBQ0ksZ0JBQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsZ0NBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsZ0NBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQsa0NBQWlCLEdBQWpCO1FBQ0ksSUFBSTtZQUNBLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDL0Q7UUFBQyxPQUFPLEtBQUssRUFBRTtTQUVmO0lBQ0wsQ0FBQztJQUVELDRCQUFXLEdBQVgsVUFBWSxLQUFhO1FBQ3JCLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRztZQUFFLE9BQU07UUFDaEQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7UUFDNUIsSUFBSTtZQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFBO1NBQ3hEO1FBQUMsT0FBTyxLQUFLLEVBQUU7U0FFZjtJQUNMLENBQUM7SUFnRE8sb0NBQW1CLEdBQTNCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUMxRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekU7UUFDRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDeEMsQ0FBQzs7SUFFZSxrQkFBVyxHQUFXLEVBQUUsQ0FBQztJQUN6QixXQUFJLEdBQVcsRUFBRSxDQUFDO0lBaFpsQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dDQUNHO0lBRXJCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt5Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3dDQUNHO0lBRXRCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NENBQ087SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDSztJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzBDQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLG1CQUFTLENBQUM7MENBQ0s7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0Q0FDTztJQWhCVCxNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBbVoxQjtJQUFELGFBQUM7Q0FuWkQsQUFtWkMsQ0FuWm1DLEVBQUUsQ0FBQyxTQUFTLEdBbVovQztrQkFuWm9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuaW1wb3J0IENhcmQgZnJvbSBcIi4vQ2FyZFwiO1xuaW1wb3J0IENhcmRHcm91cCBmcm9tIFwiLi9DYXJkR3JvdXBcIjtcbmltcG9ydCBUaW1lciBmcm9tIFwiLi9UaW1lclwiO1xuaW1wb3J0IFR3ZWVuTW92ZSBmcm9tIFwiLi90d2Vlbi9Ud2Vlbk1vdmVcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuL3V0aWxcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4vQ29uZmlnXCI7XG5pbXBvcnQgR2FtZSBmcm9tIFwiLi9HYW1lXCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuXG4vLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGxheWVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBmb2xkOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoVGltZXIpXG4gICAgdGltZXI6IFRpbWVyID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgY29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1c2VybmFtZTogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIG1hcmtlcjogY2MuTm9kZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KFR3ZWVuTW92ZSlcbiAgICBlZmZlY3Q6IFR3ZWVuTW92ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBvaW50TGJsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBzZWF0OiBudW1iZXIgPSAwO1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPiA9IFtdO1xuICAgIHByZXBhcmVDYXJkczogQ2FyZEdyb3VwID0gbmV3IENhcmRHcm91cChbXSwgQ2FyZEdyb3VwLlVuZ3JvdXBlZCk7XG4gICAgaW5Sb3VuZDogYm9vbGVhbiA9IHRydWU7XG4gICAgY29pblZhbDogbnVtYmVyID0gMTtcbiAgICBjYXJkTWFyZ2luOiBudW1iZXIgPSAwO1xuICAgIGNhcmRSb290UG9zaXRpb246IGNjLlZlYzIgPSBudWxsO1xuICAgIGNhcmREaXJlY3Rpb246IG51bWJlciA9IG51bGw7XG4gICAgbGFiZWxDYXJkQ291bnRlcjogY2MuTGFiZWwgPSBudWxsO1xuICAgIG51bU9mQWNlOiBudW1iZXIgPSAwXG4gICAgcG9pbnQ6IG51bWJlciA9IDBcblxuICAgIGJvbnVzVHlwZTogbnVtYmVyID0gMCAvLyAwIG5vdGhpbmcsICAxIGRvdWJsZSwgMiBpbnN1cnJcbiAgICBpc1NwbGl0OiBudW1iZXIgPSAwXG5cbiAgICBzdWJVc2VyOiBQbGF5ZXIgPSBudWxsXG4gICAgaXNTdWJVc2VyOiBib29sZWFuID0gZmFsc2VcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMuZm9sZC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy50aW1lci5oaWRlKCk7XG4gICAgICAgIHRoaXMubWFya2VyLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAvLyB0aGlzLmNhcmRNYXJnaW4gPSB0aGlzLnNlYXQgPT0gMCA/IFBsYXllci5DQVJEX01BUkdJTiA6XG4gICAgICAgIC8vICAgICAwLjUgKiBQbGF5ZXIuQ0FSRF9NQVJHSU47XG4gICAgICAgIHRoaXMuY2FyZE1hcmdpbiA9IDAuNSAqIFBsYXllci5DQVJEX01BUkdJTjtcbiAgICAgICAgdGhpcy5jYXJkRGlyZWN0aW9uID0gdGhpcy5zZWF0ID09IDEgPyAtMSA6IDE7XG4gICAgICAgIHRoaXMubGFiZWxDYXJkQ291bnRlciA9IHRoaXMubWFya2VyLmdldENvbXBvbmVudEluQ2hpbGRyZW4oY2MuTGFiZWwpO1xuICAgIH1cblxuICAgIHNldEJvbnVzVHlwZSA9IChib251c1R5cGU6IG51bWJlcikgPT4ge1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXNcbiAgICAgICAgICAgIGxldCB0eHRCb251c1VzZXIgPSAnQ2FudmFzL3VzZXIvYm9udXMnXG4gICAgICAgICAgICBpZiAoIXRoaXMuaW5Sb3VuZCkge1xuICAgICAgICAgICAgICAgIHNlbGYgPSB0aGlzLnN1YlVzZXJcbiAgICAgICAgICAgICAgICB0eHRCb251c1VzZXIgPSAnQ2FudmFzL3N1YlVzZXIvYm9udXMnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZWxmLmJvbnVzVHlwZSA9IGJvbnVzVHlwZVxuXG4gICAgICAgICAgICBsZXQgYm9udXNMYmwgPSBjYy5maW5kKHR4dEJvbnVzVXNlcik7XG4gICAgICAgICAgICBib251c0xibC5hY3RpdmUgPSAhIWJvbnVzVHlwZVxuICAgICAgICAgICAgbGV0IHN0ciA9IGJvbnVzVHlwZSA9PSAxID8gJ3gyJyA6ICcvMidcbiAgICAgICAgICAgIGJvbnVzTGJsLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc3RyXG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaG93KCkge1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzaG93QWxsQ2FyZCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5jYXJkcy5tYXAoY2FyZCA9PiB7XG4gICAgICAgICAgICBjYXJkLnNob3coKVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBpc0JvdCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhdCAhPT0gQ29uZmlnLnRvdGFsUGxheWVyIC8gMjtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhdCA+IDA7XG4gICAgfVxuXG4gICAgaXNVc2VyKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1N1YlVzZXIgfHwgdGhpcy5zZWF0ID09PSBDb25maWcudG90YWxQbGF5ZXIgLyAyO1xuICAgIH1cblxuICAgIGlzQ2FpKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWF0ID09PSAwO1xuICAgICAgICByZXR1cm4gdGhpcy5zZWF0ID09PSBDb25maWcudG90YWxQbGF5ZXIgLyAyO1xuICAgIH1cblxuICAgIHNldEluUm91bmQoaW5Sb3VuZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmZvbGQuYWN0aXZlID0gIWluUm91bmQ7XG4gICAgICAgIHRoaXMuaW5Sb3VuZCA9IGluUm91bmQ7XG4gICAgfVxuXG4gICAgaXNJblJvdW5kKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pblJvdW5kO1xuICAgIH1cblxuICAgIGNoYW5nZUNvaW4gPSAoY29pbjogbnVtYmVyKSA9PiB7XG4gICAgICAgIGxldCBjb2xvciA9IGNjLmNvbG9yKDB4ZjksIDB4ZDIsIDB4MWUsIDI1NSksIGFkZFN0cmluZyA9ICcrJztcbiAgICAgICAgaWYgKGNvaW4gPCAwKSB7XG4gICAgICAgICAgICBjb2xvciA9IGNjLmNvbG9yKDI1NSwgMCwgMCwgMjU1KSwgYWRkU3RyaW5nID0gJy0nXG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmVmZmVjdC5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IGFkZFN0cmluZyArIHV0aWwubnVtYmVyRm9ybWF0KE1hdGguYWJzKGNvaW4pKTtcbiAgICAgICAgdGhpcy5lZmZlY3Qubm9kZS5jb2xvciA9IGNvbG9yXG4gICAgICAgIHRoaXMuZWZmZWN0LnBsYXkoKTtcbiAgICAgICAgdGhpcy5zZXRDb2luKHRoaXMuY29pblZhbCArIGNvaW4pO1xuICAgIH1cblxuICAgIGFkZENvaW4odmFsOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5lZmZlY3QuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSAnKycgKyB1dGlsLm51bWJlckZvcm1hdCh2YWwpO1xuICAgICAgICB0aGlzLmVmZmVjdC5ub2RlLmNvbG9yID0gY2MuY29sb3IoMHhmOSwgMHhkMiwgMHgxZSwgMjU1KTtcbiAgICAgICAgdGhpcy5lZmZlY3QucGxheSgpO1xuICAgICAgICB0aGlzLnNldENvaW4odGhpcy5jb2luVmFsICsgdmFsKTtcbiAgICB9XG5cbiAgICBzdWJDb2luKHZhbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuZWZmZWN0LmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gJy0nICsgdXRpbC5udW1iZXJGb3JtYXQodmFsKTtcbiAgICAgICAgdGhpcy5lZmZlY3Qubm9kZS5jb2xvciA9IGNjLmNvbG9yKDI1NSwgMCwgMCwgMjU1KTtcbiAgICAgICAgdGhpcy5lZmZlY3QucGxheSgpO1xuICAgICAgICB0aGlzLnNldENvaW4odGhpcy5jb2luVmFsIC0gdmFsKTtcbiAgICB9XG5cbiAgICBzZXRDb2luKHZhbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuY29pblZhbCA9IHZhbDtcbiAgICAgICAgdGhpcy5jb2luLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KHZhbCk7XG4gICAgfVxuXG4gICAgZ2V0Q29pbigpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb2luVmFsO1xuICAgIH1cblxuICAgIHNldFVzZXJuYW1lKHZhbDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudXNlcm5hbWUuc3RyaW5nID0gdmFsO1xuICAgIH1cblxuICAgIHNldEF2YXRhcihzcHJpdGU6IGNjLlNwcml0ZUZyYW1lKSB7XG4gICAgICAgIHRoaXMuYXZhdGFyLnNwcml0ZUZyYW1lID0gc3ByaXRlO1xuICAgIH1cblxuICAgIHJlc2V0KCkge1xuICAgICAgICB0aGlzLm1hcmtlci5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5mb2xkLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmNhcmRzID0gW107XG4gICAgICAgIHRoaXMuaXNTdWJVc2VyID0gZmFsc2VcbiAgICAgICAgdGhpcy5pblJvdW5kID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5wcmVwYXJlQ2FyZHMgPSBuZXcgQ2FyZEdyb3VwKFtdLCBDYXJkR3JvdXAuVW5ncm91cGVkKTtcbiAgICAgICAgdGhpcy50aW1lci5oaWRlKCk7XG4gICAgICAgIHRoaXMubnVtT2ZBY2UgPSAwO1xuICAgICAgICB0aGlzLnBvaW50ID0gMDtcbiAgICAgICAgdGhpcy5ib251c1R5cGUgPSAwXG4gICAgICAgIHRoaXMuaXNTcGxpdCA9IDBcbiAgICAgICAgaWYgKHRoaXMuc3ViVXNlcikge1xuICAgICAgICAgICAgdGhpcy5zdWJVc2VyLnJlc2V0KClcbiAgICAgICAgICAgIHRoaXMuc3ViVXNlciA9IG51bGxcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNwbGl0ID0gKHN1YlVzZXI6IFBsYXllcikgPT4ge1xuXG4gICAgICAgIHRoaXMuc3ViVXNlciA9IHN1YlVzZXJcbiAgICAgICAgdGhpcy5zdWJVc2VyLmlzU3ViVXNlciA9IHRydWVcblxuICAgICAgICBpZiAodGhpcy5pc1NwbGl0KSByZXR1cm5cbiAgICAgICAgdGhpcy5pc1NwbGl0ID0gMVxuICAgICAgICBpZiAodGhpcy5jYXJkc1sxXS5yYW5rID09PSAxKSB0aGlzLm51bU9mQWNlLS1cbiAgICAgICAgdGhpcy5jYXJkcy5tYXAoKGNhcmQsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBpZiAoaW5kZXggPT09IDEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zdWJVc2VyLnB1c2goY2FyZCwgMClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSBpbmRleCA9PT0gMSA/IC0xIDogaW5kZXhcbiAgICAgICAgICAgIGxldCBwb3MgPSB0aGlzLmdldENhcmRSb290UG9zaXRpb24oKTtcbiAgICAgICAgICAgIHBvcy54ICs9IGluZGV4IDwgMiA/IDAgOiBpbmRleCAqIHRoaXMuY2FyZE1hcmdpbjtcbiAgICAgICAgICAgIGxldCBtb3ZlID0gY2Muc2VxdWVuY2UoY2MuZGVsYXlUaW1lKEdhbWUuREVBTF9TUEVFRCksIGNjLm1vdmVUbygwLjIsIHBvcykpO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnJ1bkFjdGlvbihtb3ZlKTtcbiAgICAgICAgfSlcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNhcmRzID0gdGhpcy5jYXJkcy5maWx0ZXIoKGNhcmQsIGluZGV4KSA9PiBpbmRleCAhPT0gMSlcbiAgICAgICAgfSwgMCk7XG5cbiAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpXG4gICAgICAgIGxldCBwb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KHBvaW50LnRvU3RyaW5nKCkpXG5cbiAgICB9XG5cbiAgICBwdXNoKGNhcmQ6IENhcmQsIGRlbGF5OiBudW1iZXIpIHtcbiAgICAgICAgaWYgKCF0aGlzLmluUm91bmQgJiYgdGhpcy5zdWJVc2VyKSByZXR1cm4gdGhpcy5zdWJVc2VyLnB1c2goY2FyZCwgZGVsYXkpXG5cbiAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZCk7XG4gICAgICAgIGlmIChjYXJkLnJhbmsgPT09IDEpIHRoaXMubnVtT2ZBY2UgKz0gMVxuXG4gICAgICAgIGxldCBwb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuXG4gICAgICAgIGxldCBmdW4gPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KHBvaW50ID09IDIxICYmIHRoaXMuY2FyZHMubGVuZ3RoID09PSAyID8gJ0JsYWNrSmFjaycgOiBwb2ludC50b1N0cmluZygpKVxuICAgICAgICAgICAgaWYgKHBvaW50ID4gMjApIHRoaXMuaW5Sb3VuZCA9IGZhbHNlXG4gICAgICAgICAgICB0aGlzLmNhcmRzWzBdLnNob3coKVxuICAgICAgICAgICAgaWYgKHRoaXMuaXNVc2VyKCkpIHsgY2FyZC5zaG93KCkgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0JvdCgpICYmICF0aGlzLmlzQ2FpKCkgJiYgdGhpcy5jYXJkcy5sZW5ndGggPT09IDIpIHtcbiAgICAgICAgICAgIGZ1biA9ICgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmNhcmRzWzFdLnNob3coKVxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUG9pbnQobnVsbClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLy8gdGhpcy51cGRhdGVQb2ludCgpXG4gICAgICAgIGxldCBwb3MgPSB0aGlzLmdldENhcmRSb290UG9zaXRpb24oKTtcblxuXG4gICAgICAgIGxldCBzY2FsZSA9IGNjLnNlcXVlbmNlKGNjLmRlbGF5VGltZShkZWxheSksIGNjLnNjYWxlVG8oMC4yLCAwLjUpKTtcbiAgICAgICAgY2FyZC5ub2RlLnJ1bkFjdGlvbihzY2FsZSk7XG4gICAgICAgIC8vIGlmICh0aGlzLmlzQm90KCkpIHtcblxuICAgICAgICAvLyAgICAgaWYgKHRoaXMuY2FyZHMubGVuZ3RoIDwgMykge1xuICAgICAgICAvLyAgICAgICAgIHBvcy54ICs9ICh0aGlzLmNhcmRzLmxlbmd0aCAtIDEpICogdGhpcy5jYXJkTWFyZ2luO1xuICAgICAgICAvLyAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gICAgICAgICBwb3MueCArPSAyICogdGhpcy5jYXJkTWFyZ2luO1xuICAgICAgICAvLyAgICAgfVxuXG5cbiAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgcG9zLnggKz0gKHRoaXMuY2FyZHMubGVuZ3RoIC0gMSkgKiB0aGlzLmNhcmRNYXJnaW47XG4gICAgICAgIC8vIH1cblxuICAgICAgICBsZXQgbW92ZSA9IGNjLnNlcXVlbmNlKGNjLmRlbGF5VGltZShkZWxheSksIGNjLm1vdmVUbygwLjIsIHBvcyksIGNjLmNhbGxGdW5jKGZ1bikpO1xuICAgICAgICBjYXJkLm5vZGUucnVuQWN0aW9uKG1vdmUpO1xuICAgIH1cblxuICAgIHNob3dDYXJkMiA9ICgpID0+IHtcbiAgICAgICAgLy8gaWYgKHRoaXMuaXNCb3QoKSAmJiAhdGhpcy5pc0NhaSgpICYmIHRoaXMuY2FyZHMubGVuZ3RoID09PSAyKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZygxMjMpO1xuICAgICAgICAvLyAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAvLyAgICAgdGhpcy5jYXJkc1sxXS5zaG93KClcbiAgICAgICAgLy8gfVxuICAgIH1cblxuICAgIHRvdWNoKHBvczogY2MuVmVjMik6IENhcmQge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmNhcmRzW2ldO1xuICAgICAgICAgICAgaWYgKGNhcmQubm9kZS5nZXRCb3VuZGluZ0JveFRvV29ybGQoKS5jb250YWlucyhwb3MpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNhcmQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc2VsZWN0Q2FyZChjYXJkOiBDYXJkKSB7XG4gICAgICAgIC8vIHRoaXMucHJlcGFyZUNhcmRzLnB1c2goY2FyZCk7XG4gICAgICAgIC8vIHRoaXMucHJlcGFyZUNhcmRzLmNhbGN1bGF0ZSgpO1xuICAgICAgICAvLyBjYXJkLnNldFBvc2l0aW9uWSh0aGlzLm1hcmtlci5wb3NpdGlvbi55ICsgdGhpcy5ub2RlLnBvc2l0aW9uLnkgKyAzMCk7XG4gICAgfVxuXG4gICAgdW5zZWxlY3RDYXJkKGNhcmQ6IENhcmQpIHtcbiAgICAgICAgLy8gdGhpcy5wcmVwYXJlQ2FyZHMucmVtb3ZlKGNhcmQpO1xuICAgICAgICAvLyB0aGlzLnByZXBhcmVDYXJkcy5jYWxjdWxhdGUoKTtcbiAgICAgICAgLy8gY2FyZC5zZXRQb3NpdGlvblkodGhpcy5tYXJrZXIucG9zaXRpb24ueSArIHRoaXMubm9kZS5wb3NpdGlvbi55KTtcbiAgICB9XG5cbiAgICByZW1vdmVDYXJkcyhjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5jb3VudCgpIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGhlbHBlci5yZW1vdmVCeSh0aGlzLmNhcmRzLCBjYXJkcy5hdChpKSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnByZXBhcmVDYXJkcyA9IG5ldyBDYXJkR3JvdXAoW10sIENhcmRHcm91cC5Vbmdyb3VwZWQpO1xuICAgIH1cblxuICAgIHNldEFjdGl2ZShvbjogYm9vbGVhbikge1xuICAgICAgICBpZiAob24pIHtcbiAgICAgICAgICAgIHRoaXMudGltZXIuc2hvdyhQbGF5ZXIuVElNRSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnRpbWVyLmhpZGUoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNldFRpbWVDYWxsYmFjayhzZWxlY3RvcjogRnVuY3Rpb24sIHNlbGVjdG9yVGFyZ2V0OiBhbnksIGRhdGE6IGFueSkge1xuICAgICAgICB0aGlzLnRpbWVyLm9uQ29tcGxldGVkKHNlbGVjdG9yLCBzZWxlY3RvclRhcmdldCwgZGF0YSk7XG4gICAgfVxuXG4gICAgc2hvd0hhbmRDYXJkcygpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmNhcmRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLmNhcmRzW2ldLnNob3coKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlb3JkZXIoKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBsZXQgcG9zID0gdGhpcy5nZXRDYXJkUm9vdFBvc2l0aW9uKCk7XG4gICAgICAgICAgICBwb3MueCArPSBpICogdGhpcy5jYXJkTWFyZ2luICogdGhpcy5jYXJkRGlyZWN0aW9uO1xuICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmNhcmRzW2ldO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnNldFBvc2l0aW9uKHBvcyk7XG4gICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gdGhpcy5zZWF0ID09IDEgPyB0aGlzLmNhcmRzLmxlbmd0aCAtIGkgOiBpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc29ydEhhbmRDYXJkcygpIHtcbiAgICAgICAgaGVscGVyLnNvcnQodGhpcy5jYXJkcyk7XG4gICAgICAgIHRoaXMucmVvcmRlcigpO1xuICAgIH1cblxuICAgIHNob3dDYXJkQ291bnRlcigpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpO1xuICAgIH1cblxuICAgIGhpZGVDYXJkQ291bnRlcigpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgdXBkYXRlQ2FyZENvdW50ZXIoKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmxhYmVsQ2FyZENvdW50ZXIuc3RyaW5nID0gdGhpcy5jYXJkcy5sZW5ndGgudG9TdHJpbmcoKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgdXBkYXRlUG9pbnQobGFiZWw6IHN0cmluZykge1xuICAgICAgICBpZiAoTnVtYmVyKHRoaXMucG9pbnRMYmwuc3RyaW5nKSA9PT0gTmFOKSByZXR1cm5cbiAgICAgICAgdGhpcy5wb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy5wb2ludExibC5zdHJpbmcgPSBsYWJlbCB8fCB0aGlzLnBvaW50LnRvU3RyaW5nKClcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0UG9pbnQgPSAoKSA9PiB7XG4gICAgICAgIHZhciBwb2ludCA9IHRoaXMuY2FyZHMucmVkdWNlKChwb2ludCwgY3VycmVudCkgPT4ge1xuICAgICAgICAgICAgaWYgKGN1cnJlbnQucmFuayA9PT0gMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludFxuICAgICAgICAgICAgfSBlbHNlIGlmIChjdXJyZW50LnJhbmsgPiAxMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludCArIDEwXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludCArIGN1cnJlbnQucmFua1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCAwKTtcblxuICAgICAgICBpZiAodGhpcy5udW1PZkFjZSA+IDApIHtcbiAgICAgICAgICAgIGlmIChwb2ludCA+IDExKSB7XG4gICAgICAgICAgICAgICAgcG9pbnQgKz0gdGhpcy5udW1PZkFjZSAqIDFcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcG9pbnQgKz0gdGhpcy5udW1PZkFjZSAqIDExXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5wb2ludCA9IHBvaW50XG4gICAgICAgIHJldHVybiBwb2ludFxuICAgICAgICAvLyBpZiAocG9pbnQgPiAyMSkge1xuICAgICAgICAvLyAgICAgbGV0IHR4dFRodWEgPSBgJHVzZXIgdGh1YWBcbiAgICAgICAgLy8gICAgIGlmICh0aGlzLmlzQ2FpKCkpIHtcbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gYWxlcnQoJ2NhaSB0aHVhOiAnICsgcG9pbnQgKyAnIGRpZW0nKVxuICAgICAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLmlzQm90KCkpIHtcbiAgICAgICAgLy8gICAgICAgICBhbGVydCgnYm90IHRodWE6ICcgKyBwb2ludCArICcgZGllbScpXG4gICAgICAgIC8vICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgICAgIGFsZXJ0KCd1c2VyIHRodWE6ICcgKyBwb2ludCArICcgZGllbScpXG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vIH1cbiAgICB9XG5cbiAgICBjaGVja0JsYWNrSmFjayA9ICgpID0+IHtcbiAgICAgICAgbGV0IHJhbmsxID0gdGhpcy5jYXJkc1swXT8ucmFua1xuICAgICAgICBsZXQgcmFuazIgPSB0aGlzLmNhcmRzWzFdPy5yYW5rXG5cbiAgICAgICAgaWYgKHJhbmsxID09PSAxIHx8IHJhbmsyID09PSAxKSB7XG4gICAgICAgICAgICBpZiAocmFuazEgPiA5IHx8IHJhbmsyID4gOSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KFwiQmxhY2sgSmFjayFcIilcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0Q2FyZFJvb3RQb3NpdGlvbigpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNhcmRSb290UG9zaXRpb24pIHtcbiAgICAgICAgICAgIHRoaXMuY2FyZFJvb3RQb3NpdGlvbiA9IGNjLnYyKHRoaXMubWFya2VyLnBvc2l0aW9uLnggKiB0aGlzLm5vZGUuc2NhbGVYICsgdGhpcy5ub2RlLnBvc2l0aW9uLngsXG4gICAgICAgICAgICAgICAgdGhpcy5tYXJrZXIucG9zaXRpb24ueSAqIHRoaXMubm9kZS5zY2FsZVkgKyB0aGlzLm5vZGUucG9zaXRpb24ueSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNjLnYyKHRoaXMuY2FyZFJvb3RQb3NpdGlvbik7XG4gICAgfVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IENBUkRfTUFSR0lOOiBudW1iZXIgPSA2MDtcbiAgICBzdGF0aWMgcmVhZG9ubHkgVElNRTogbnVtYmVyID0gMzA7XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/popop/bonus/DailyBonusItem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cd2a0cL565MMb3PI2MSL01H', 'DailyBonusItem');
// Scripts/popop/bonus/DailyBonusItem.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DailyBonusItem = /** @class */ (function (_super) {
    __extends(DailyBonusItem, _super);
    function DailyBonusItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = null;
        _this.claimed = null;
        _this.extra = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    DailyBonusItem.prototype.start = function () {
    };
    // update (dt) {}
    DailyBonusItem.prototype.setClaimable = function (claimable, isClaimed) {
        this.claimed.active = isClaimed;
        this.getComponent(cc.Button).interactable = claimable;
        this.extra.active = claimable;
    };
    __decorate([
        property(cc.Label)
    ], DailyBonusItem.prototype, "value", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonusItem.prototype, "claimed", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonusItem.prototype, "extra", void 0);
    DailyBonusItem = __decorate([
        ccclass
    ], DailyBonusItem);
    return DailyBonusItem;
}(cc.Component));
exports.default = DailyBonusItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3BvcG9wL2JvbnVzL0RhaWx5Qm9udXNJdGVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBMkJDO1FBeEJHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFHdkIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixXQUFLLEdBQVksSUFBSSxDQUFDOztJQWtCMUIsQ0FBQztJQWhCRyx3QkFBd0I7SUFFeEIsZUFBZTtJQUVmLDhCQUFLLEdBQUw7SUFFQSxDQUFDO0lBRUQsaUJBQWlCO0lBRWpCLHFDQUFZLEdBQVosVUFBYSxTQUFrQixFQUFFLFNBQWtCO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1FBQ3RELElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztJQUNsQyxDQUFDO0lBdEJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aURBQ0k7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNJO0lBVEwsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTJCbEM7SUFBRCxxQkFBQztDQTNCRCxBQTJCQyxDQTNCMkMsRUFBRSxDQUFDLFNBQVMsR0EyQnZEO2tCQTNCb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERhaWx5Qm9udXNJdGVtIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB2YWx1ZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgY2xhaW1lZDogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBleHRyYTogY2MuTm9kZSA9IG51bGw7XG5cbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQgKCkge1xuXG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHNldENsYWltYWJsZShjbGFpbWFibGU6IGJvb2xlYW4sIGlzQ2xhaW1lZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmNsYWltZWQuYWN0aXZlID0gaXNDbGFpbWVkO1xuICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5CdXR0b24pLmludGVyYWN0YWJsZSA9IGNsYWltYWJsZTtcbiAgICAgICAgdGhpcy5leHRyYS5hY3RpdmUgPSBjbGFpbWFibGU7XG4gICAgfVxuXG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Api.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cffd1wdHiNPIaFpHSP+5t/F', 'Api');
// Scripts/Api.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var image_1 = require("./image");
var Api = /** @class */ (function () {
    function Api() {
    }
    Api.initAsync = function (callback, leaderboard) {
        if (Api.initialized) {
            Api.loadLeaderboard(leaderboard);
            return callback(false, Api.dailyBonusDay); // daily_bonus_claimed_at, daily_bonus_day
        }
        if (!Api.initialized && window.hasOwnProperty('FBInstant')) {
            Api.initialized = true;
            Api.photo = FBInstant.player.getPhoto();
            Api.username = FBInstant.player.getName();
            Api.playerId = FBInstant.player.getID();
            Api.locale = FBInstant.getLocale();
            Api.getPlayerData(callback);
            Api.loadLeaderboard(leaderboard);
            Api.subscribeBot();
            Api.createShortcut();
        }
        else {
            Api.ticket = 10;
            Api.dailyBonusDay = 0;
            // setTimeout(() => {
            Api.locale = 'vi_VN';
            // Api.locale = 'th_TH';
            // callback(true, Api.dailyBonusDay);
            Api.loadLeaderboard(leaderboard);
            // }, 5000);
        }
    };
    Api.getPlayerData = function (callback) {
        FBInstant.player.getDataAsync(['coin', 'ticket', 'daily_bonus_claimed_at', 'daily_bonus_day']).then(function (data) {
            cc.log('data is loaded');
            Api.coin = data['coin'];
            if (Api.coin == undefined || Api.coin == null) {
                Api.coin = Config_1.default.defaultCoin;
                Api.isDirty = true;
            }
            Api.ticket = data['ticket'] || 0;
            Api.dailyBonusDay = data['daily_bonus_day'] || 0;
            Api.dailyBonusClaimed = !Api.dailyBonusClaimable(data['daily_bonus_claimed_at']);
            callback(!Api.dailyBonusClaimed, Api.dailyBonusDay);
            if (Api.isDirty) {
                Api.flush();
            }
        });
    };
    Api.subscribeBot = function () {
        FBInstant.player.canSubscribeBotAsync().then(function (can_subscribe) {
            cc.log('subscribeBotAsync', can_subscribe);
            Api.canSubscribeBot = can_subscribe.valueOf();
            // Then when you want to ask the player to subscribe
            if (Api.canSubscribeBot) {
                FBInstant.player.subscribeBotAsync().then(
                // Player is subscribed to the bot
                ).catch(function (e) {
                    // Handle subscription failure
                    cc.log('subscribeBotAsync');
                });
            }
        }).catch(function (e) {
            cc.log('subscribeBotAsync', e);
        });
    };
    Api.createShortcut = function () {
        FBInstant.canCreateShortcutAsync()
            .then(function (canCreateShortcut) {
            if (canCreateShortcut) {
                FBInstant.createShortcutAsync()
                    .then(function () {
                    // Shortcut created
                })
                    .catch(function () {
                    // Shortcut not created
                });
            }
        });
    };
    Api.loadLeaderboard = function (leaderboard) {
        fetch(Api.lb_api + "/" + Api.lb_id).then(function (response) { return response.json(); }).then(function (res) {
            var _a;
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            ((_a = res === null || res === void 0 ? void 0 : res.entries) === null || _a === void 0 ? void 0 : _a.length) && res.entries.map(function (e, i) {
                leaderboard.render(i + 1, e.username, e.score, e.photo, e.user_id);
            });
            // for (let i = 0; i < res.entries.length; i++) {
            //     const e = res.entries[i];
            //     leaderboard.render(
            //         i + 1,
            //         e.username,
            //         e.score,
            //         e.photo,
            //         e.user_id,
            //     );
            // }
            leaderboard.onLoadComplete();
        }).catch(function (error) { return console.error(error); });
    };
    Api.preloadRewardedVideo = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (!window.hasOwnProperty('FBInstant')) {
        if (callback)
            callback();
        return;
        // }
        if (Api.preloadedRewardedVideo || !Config_1.default.reward_video) {
            return;
        }
        FBInstant.getRewardedVideoAsync(Config_1.default.reward_video).then(function (rewarded) {
            // Load the Ad asynchronously
            Api.preloadedRewardedVideo = rewarded;
            return Api.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            cc.log('Rewarded video preloaded');
            if (callback)
                callback();
        }).catch(function (e) {
            Api.preloadedRewardedVideo = null;
            console.error(e.message);
            if (callback)
                callback();
        });
    };
    Api.showRewardedVideo = function (success, error) {
        // if (!window.hasOwnProperty('FBInstant')) {
        //     return error('rrr');
        // }
        // if (!Config.reward_video) {
        success();
        // }
        // if (!Api.preloadedRewardedVideo) {
        //     console.error('ad not load yet');
        //     return error('ad not load yet');
        // }
        // Api.preloadedRewardedVideo.showAsync().then(function () {
        //     success();
        //     Api.preloadedRewardedVideo = null;
        //     cc.log('Rewarded video watched successfully');
        // }).catch(function (e) {
        //     error(e.message);
        //     Api.preloadedRewardedVideo = null;
        //     console.error(e.message);
        // });
    };
    Api.preloadInterstitialAd = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (window.hasOwnProperty('FBInstant') && !Api.preloadedInterstitial) {
        //     FBInstant.getInterstitialAdAsync(Config.intertital_ads).then(function(interstitial) {
        //         // Load the Ad asynchronously
        //         Api.preloadedInterstitial = interstitial;
        //         return Api.preloadedInterstitial.loadAsync();
        //     }).then(function() {
        //         cc.log('Interstitial preloaded');
        if (callback)
            callback();
        // }).catch(function(err){
        //     cc.error('Interstitial failed to preload');
        //     cc.error(err);
        // });
        // }
    };
    Api.showInterstitialAd = function () {
        // if (window.hasOwnProperty('FBInstant')) {
        //     if (Api.preloadedInterstitial) {
        //         Api.preloadedInterstitial.showAsync().then(function () {
        //             // Perform post-ad success operation
        //             cc.log('Interstitial ad finished successfully');
        //             Api.preloadedInterstitial = null;
        //         }).catch(function (e) {
        //             cc.error(e.message);
        //             Api.preloadedInterstitial = null;
        //         });
        //     }
        // }
    };
    Api.isInterstitialAdLoaded = function () {
        if (!window.hasOwnProperty('FBInstant')) {
            return false;
        }
        return !!Api.preloadedInterstitial;
    };
    Api.challenge = function (playerId) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FBInstant.context.createAsync(playerId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, FBInstant.updateAsync({
                                action: 'CUSTOM',
                                template: 'play_turn',
                                cta: 'Chơi ngay',
                                image: image_1.default,
                                text: Api.username + ' đang thách đấu bạn. Nhấn chơi ngay!',
                                strategy: 'IMMEDIATE'
                            })];
                }
            });
        });
    };
    Api.shareAsync = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                if (window.hasOwnProperty('FBInstant')) {
                    return [2 /*return*/, FBInstant.shareAsync({
                            intent: 'SHARE',
                            image: image_1.default,
                            text: 'Tiến liên miền nam 2020!',
                        })];
                }
                return [2 /*return*/, Promise.resolve()];
            });
        });
    };
    Api.invite = function (success, error) {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.context
                .chooseAsync()
                .then(function () {
                success(FBInstant.context.getID());
                FBInstant.updateAsync({
                    action: 'CUSTOM',
                    template: 'play_turn',
                    cta: 'Chơi ngay',
                    image: image_1.default,
                    text: Api.username + ' mời bạn cùng chơi game tiến liên miền nam',
                    strategy: 'IMMEDIATE'
                });
            }).catch(error);
        }
        else {
            success('');
        }
    };
    Api.coinIncrement = function (val) {
        Api.updateCoin(Api.coin + val);
    };
    Api.updateCoin = function (coin) {
        Api.coin = coin;
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function () { return cc.log('Score saved'); }).catch(function (err) { return console.error(err); });
    };
    Api.updateTicket = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ ticket: Api.ticket }).then(function () {
                cc.log('ticket is set');
            });
        }
    };
    Api.claimDailyBonus = function (day) {
        Api.dailyBonusClaimed = true;
        if (window.hasOwnProperty('FBInstant')) {
            var claimed_at = new Date();
            FBInstant.player.setDataAsync({
                coin: Api.coin,
                ticket: Api.ticket,
                'daily_bonus_claimed_at': claimed_at.toString(),
                'daily_bonus_day': day
            }).then(function () {
                cc.log('data is set');
            }, function (reason) {
                cc.log('data is not set', reason);
            });
        }
        Api.dailyBonusDay++;
    };
    Api.dailyBonusClaimable = function (claimedAt) {
        if (!claimedAt)
            return true;
        if (typeof claimedAt == 'string') {
            claimedAt = new Date(claimedAt);
        }
        var now = new Date();
        if (now.getFullYear() != claimedAt.getFullYear())
            return true;
        if (now.getMonth() != claimedAt.getMonth())
            return true;
        if (now.getDate() != claimedAt.getDate())
            return true;
        return false;
    };
    Api.flush = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin, ticket: Api.ticket }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function (res) { return res.json(); }).then(function (res) { return cc.log(res); }).catch(function (err) { return console.error(err); });
    };
    Api.logEvent = function (eventName, valueToSum, parameters) {
        if (!eventName)
            return;
        // if (window.hasOwnProperty('FBInstant')) {
        try {
            // window.firebase.analytics().logEvent(eventName, parameters);
            console.log('====================================');
            console.log(eventName, parameters);
            console.log('====================================');
        }
        catch (error) {
            console.log('====================================');
            console.log('analytic error', error);
            console.log('====================================');
        }
        // FBInstant.logEvent(eventName, valueToSum, parameters);
        // }
    };
    Api.setScoreAsync = function (score) {
        // if (!window.hasOwnProperty('FBInstant')) return;
        var data = { score: score, userId: Api.playerId, name: Api.username, photo: Api.photo };
        return fetch(Api.lb_api + "/" + Api.lb_id, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    };
    Api.randomBonus = function () {
        var r = Math.random();
        if (r < 0.05)
            return 400000;
        if (r < 0.2)
            return 300000;
        if (r < 0.5)
            return 200000;
        return 100000;
    };
    Api.initialized = false;
    Api.coin = 10000;
    Api.ticket = 0;
    Api.username = 'DM';
    Api.playerId = 'DM';
    Api.locale = 'vi_VN';
    Api.photo = 'https://i.imgur.com/FUOxs43.jpg';
    Api.isDirty = false;
    Api.preloadedRewardedVideo = null;
    Api.preloadedInterstitial = null;
    Api.canSubscribeBot = false;
    Api.dailyBonusDay = 0;
    Api.dailyBonusClaimed = false;
    Api.lb_api = 'https://leaderboard.adpia.com.vn/instant'; //'https://lb.dozo.vn/api'
    Api.lb_id = 'a330e5054804f7fb73bd1f99'; //'5f714415630b9b9ff8146f15'
    Api.ID_APP = '440201687124598';
    return Api;
}());
exports.default = Api;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1DQUE4QjtBQUM5QixpQ0FBbUM7QUFFbkM7SUFBQTtJQStWQSxDQUFDO0lBN1VpQixhQUFTLEdBQXZCLFVBQXdCLFFBQStDLEVBQUUsV0FBd0I7UUFDN0YsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFO1lBQ2pCLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsT0FBTyxRQUFRLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLDBDQUEwQztTQUN4RjtRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDeEQsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDdkIsR0FBRyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMxQyxHQUFHLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDeEMsR0FBRyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFbkMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM1QixHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNuQixHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDeEI7YUFBTTtZQUNILEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ2hCLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLHFCQUFxQjtZQUNyQixHQUFHLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNyQix3QkFBd0I7WUFDeEIscUNBQXFDO1lBQ3JDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsWUFBWTtTQUNmO0lBQ0wsQ0FBQztJQUVjLGlCQUFhLEdBQTVCLFVBQTZCLFFBQStDO1FBQ3hFLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSx3QkFBd0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSTtZQUM5RyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDekIsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEIsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLFNBQVMsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDM0MsR0FBRyxDQUFDLElBQUksR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDOUIsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDdEI7WUFDRCxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakQsR0FBRyxDQUFDLGlCQUFpQixHQUFHLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7WUFDakYsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNwRCxJQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ2IsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFYyxnQkFBWSxHQUEzQjtRQUNJLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxhQUFhO1lBQ2hFLEVBQUUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFDM0MsR0FBRyxDQUFDLGVBQWUsR0FBRyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDOUMsb0RBQW9EO1lBQ3BELElBQUksR0FBRyxDQUFDLGVBQWUsRUFBRTtnQkFDckIsU0FBUyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLElBQUk7Z0JBQ3JDLGtDQUFrQztpQkFDckMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUNmLDhCQUE4QjtvQkFDOUIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNoQyxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRWMsa0JBQWMsR0FBN0I7UUFDSSxTQUFTLENBQUMsc0JBQXNCLEVBQUU7YUFDN0IsSUFBSSxDQUFDLFVBQVUsaUJBQWlCO1lBQzdCLElBQUksaUJBQWlCLEVBQUU7Z0JBQ25CLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRTtxQkFDMUIsSUFBSSxDQUFDO29CQUNGLG1CQUFtQjtnQkFDdkIsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQztvQkFDSCx1QkFBdUI7Z0JBQzNCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFYSxtQkFBZSxHQUE3QixVQUE4QixXQUF3QjtRQUNsRCxLQUFLLENBQUksR0FBRyxDQUFDLE1BQU0sU0FBSSxHQUFHLENBQUMsS0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFmLENBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7O1lBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUVwRCxPQUFBLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxPQUFPLDBDQUFFLE1BQU0sS0FBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN6QyxXQUFXLENBQUMsTUFBTSxDQUNkLENBQUMsR0FBRyxDQUFDLEVBQ0wsQ0FBQyxDQUFDLFFBQVEsRUFDVixDQUFDLENBQUMsS0FBSyxFQUNQLENBQUMsQ0FBQyxLQUFLLEVBQ1AsQ0FBQyxDQUFDLE9BQU8sQ0FDWixDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUE7WUFFRixpREFBaUQ7WUFDakQsZ0NBQWdDO1lBQ2hDLDBCQUEwQjtZQUMxQixpQkFBaUI7WUFDakIsc0JBQXNCO1lBQ3RCLG1CQUFtQjtZQUNuQixtQkFBbUI7WUFDbkIscUJBQXFCO1lBQ3JCLFNBQVM7WUFDVCxJQUFJO1lBQ0osV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRWEsd0JBQW9CLEdBQWxDLFVBQW1DLFFBQTJCO1FBQTNCLHlCQUFBLEVBQUEsZUFBMkI7UUFDMUQsNkNBQTZDO1FBQzdDLElBQUksUUFBUTtZQUFFLFFBQVEsRUFBRSxDQUFDO1FBQ3pCLE9BQU87UUFDUCxJQUFJO1FBQ0osSUFBSSxHQUFHLENBQUMsc0JBQXNCLElBQUksQ0FBQyxnQkFBTSxDQUFDLFlBQVksRUFBRTtZQUNwRCxPQUFPO1NBQ1Y7UUFDRCxTQUFTLENBQUMscUJBQXFCLENBQUMsZ0JBQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRO1lBQ3hFLDZCQUE2QjtZQUM3QixHQUFHLENBQUMsc0JBQXNCLEdBQUcsUUFBUSxDQUFDO1lBQ3RDLE9BQU8sR0FBRyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2xELENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNKLEVBQUUsQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUNuQyxJQUFJLFFBQVE7Z0JBQUUsUUFBUSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNQLEdBQUcsQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7WUFDbEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekIsSUFBSSxRQUFRO2dCQUFFLFFBQVEsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVhLHFCQUFpQixHQUEvQixVQUFnQyxPQUFtQixFQUFFLEtBQTRCO1FBQzdFLDZDQUE2QztRQUM3QywyQkFBMkI7UUFDM0IsSUFBSTtRQUNKLDhCQUE4QjtRQUM5QixPQUFPLEVBQUUsQ0FBQztRQUNWLElBQUk7UUFDSixxQ0FBcUM7UUFDckMsd0NBQXdDO1FBQ3hDLHVDQUF1QztRQUN2QyxJQUFJO1FBQ0osNERBQTREO1FBQzVELGlCQUFpQjtRQUNqQix5Q0FBeUM7UUFDekMscURBQXFEO1FBQ3JELDBCQUEwQjtRQUMxQix3QkFBd0I7UUFDeEIseUNBQXlDO1FBQ3pDLGdDQUFnQztRQUNoQyxNQUFNO0lBQ1YsQ0FBQztJQUVhLHlCQUFxQixHQUFuQyxVQUFvQyxRQUEyQjtRQUEzQix5QkFBQSxFQUFBLGVBQTJCO1FBQzNELDBFQUEwRTtRQUMxRSw0RkFBNEY7UUFDNUYsd0NBQXdDO1FBQ3hDLG9EQUFvRDtRQUNwRCx3REFBd0Q7UUFDeEQsMkJBQTJCO1FBQzNCLDRDQUE0QztRQUM1QyxJQUFJLFFBQVE7WUFBRSxRQUFRLEVBQUUsQ0FBQztRQUN6QiwwQkFBMEI7UUFDMUIsa0RBQWtEO1FBQ2xELHFCQUFxQjtRQUNyQixNQUFNO1FBQ04sSUFBSTtJQUNSLENBQUM7SUFFYSxzQkFBa0IsR0FBaEM7UUFDSSw0Q0FBNEM7UUFDNUMsdUNBQXVDO1FBQ3ZDLG1FQUFtRTtRQUNuRSxtREFBbUQ7UUFDbkQsK0RBQStEO1FBQy9ELGdEQUFnRDtRQUNoRCxrQ0FBa0M7UUFDbEMsbUNBQW1DO1FBQ25DLGdEQUFnRDtRQUNoRCxjQUFjO1FBQ2QsUUFBUTtRQUNSLElBQUk7SUFDUixDQUFDO0lBRWEsMEJBQXNCLEdBQXBDO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDckMsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUM7SUFDdkMsQ0FBQztJQUVtQixhQUFTLEdBQTdCLFVBQThCLFFBQWdCO3VDQUFHLE9BQU87Ozs0QkFDcEQscUJBQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUE3QyxTQUE2QyxDQUFDO3dCQUM5QyxzQkFBTyxTQUFTLENBQUMsV0FBVyxDQUFDO2dDQUN6QixNQUFNLEVBQUUsUUFBUTtnQ0FDaEIsUUFBUSxFQUFFLFdBQVc7Z0NBQ3JCLEdBQUcsRUFBRSxXQUFXO2dDQUNoQixLQUFLLEVBQUUsZUFBWTtnQ0FDbkIsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEdBQUcsc0NBQXNDO2dDQUMzRCxRQUFRLEVBQUUsV0FBVzs2QkFDeEIsQ0FBQyxFQUFDOzs7O0tBQ047SUFFbUIsY0FBVSxHQUE5Qjt1Q0FBa0MsT0FBTzs7Z0JBQ3JDLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtvQkFDcEMsc0JBQU8sU0FBUyxDQUFDLFVBQVUsQ0FBQzs0QkFDeEIsTUFBTSxFQUFFLE9BQU87NEJBQ2YsS0FBSyxFQUFFLGVBQVk7NEJBQ25CLElBQUksRUFBRSwwQkFBMEI7eUJBQ25DLENBQUMsRUFBQztpQkFDTjtnQkFDRCxzQkFBTyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUM7OztLQUM1QjtJQUVhLFVBQU0sR0FBcEIsVUFBcUIsT0FBOEIsRUFBRSxLQUE0QjtRQUM3RSxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxDQUFDLE9BQU87aUJBQ1osV0FBVyxFQUFFO2lCQUNiLElBQUksQ0FBQztnQkFDRixPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNuQyxTQUFTLENBQUMsV0FBVyxDQUFDO29CQUNsQixNQUFNLEVBQUUsUUFBUTtvQkFDaEIsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLEdBQUcsRUFBRSxXQUFXO29CQUNoQixLQUFLLEVBQUUsZUFBWTtvQkFDbkIsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEdBQUcsNENBQTRDO29CQUNqRSxRQUFRLEVBQUUsV0FBVztpQkFDeEIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFYSxpQkFBYSxHQUEzQixVQUE0QixHQUFXO1FBQ25DLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRWEsY0FBVSxHQUF4QixVQUF5QixJQUFZO1FBQ2pDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25ELEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUNuRyxDQUFDO0lBRWEsZ0JBQVksR0FBMUI7UUFDSSxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2RCxFQUFFLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRWEsbUJBQWUsR0FBN0IsVUFBOEIsR0FBVztRQUNyQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxJQUFJLFVBQVUsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1lBQzVCLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUMxQixJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUk7Z0JBQ2QsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNO2dCQUNsQix3QkFBd0IsRUFBRSxVQUFVLENBQUMsUUFBUSxFQUFFO2dCQUMvQyxpQkFBaUIsRUFBRSxHQUFHO2FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ0osRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUMxQixDQUFDLEVBQUUsVUFBVSxNQUFNO2dCQUNmLEVBQUUsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRWMsdUJBQW1CLEdBQWxDLFVBQW1DLFNBQXdCO1FBQ3ZELElBQUksQ0FBQyxTQUFTO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDNUIsSUFBSSxPQUFPLFNBQVMsSUFBSSxRQUFRLEVBQUU7WUFDOUIsU0FBUyxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsSUFBTSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN2QixJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDOUQsSUFBSSxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ3hELElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUN0RCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRWEsU0FBSyxHQUFuQjtRQUNJLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZFLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBVixDQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFYLENBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBRWEsWUFBUSxHQUF0QixVQUF1QixTQUFpQixFQUFFLFVBQW1CLEVBQUUsVUFBbUI7UUFDOUUsSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFNO1FBQ3RCLDRDQUE0QztRQUM1QyxJQUFJO1lBQ0EsK0RBQStEO1lBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7U0FDdkQ7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztTQUN2RDtRQUNELHlEQUF5RDtRQUN6RCxJQUFJO0lBQ1IsQ0FBQztJQUVjLGlCQUFhLEdBQTVCLFVBQTZCLEtBQUs7UUFDOUIsbURBQW1EO1FBRW5ELElBQU0sSUFBSSxHQUFHLEVBQUUsS0FBSyxPQUFBLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUNsRixPQUFPLEtBQUssQ0FBSSxHQUFHLENBQUMsTUFBTSxTQUFJLEdBQUcsQ0FBQyxLQUFPLEVBQ3JDO1lBQ0ksTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUU7WUFDL0MsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1NBQzdCLENBQ0osQ0FBQTtJQUNMLENBQUM7SUFFYSxlQUFXLEdBQXpCO1FBQ0ksSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxHQUFHLElBQUk7WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsR0FBRyxHQUFHO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFDM0IsSUFBSSxDQUFDLEdBQUcsR0FBRztZQUFFLE9BQU8sTUFBTSxDQUFDO1FBQzNCLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUE3Vk0sZUFBVyxHQUFZLEtBQUssQ0FBQztJQUM3QixRQUFJLEdBQVcsS0FBSyxDQUFDO0lBQ3JCLFVBQU0sR0FBVyxDQUFDLENBQUM7SUFDbkIsWUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixZQUFRLEdBQVcsSUFBSSxDQUFDO0lBQ3hCLFVBQU0sR0FBVyxPQUFPLENBQUM7SUFDekIsU0FBSyxHQUFXLGlDQUFpQyxDQUFDO0lBQ2xELFdBQU8sR0FBWSxLQUFLLENBQUM7SUFDekIsMEJBQXNCLEdBQXlCLElBQUksQ0FBQztJQUNwRCx5QkFBcUIsR0FBeUIsSUFBSSxDQUFDO0lBQ25ELG1CQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLGlCQUFhLEdBQVcsQ0FBQyxDQUFDO0lBQzFCLHFCQUFpQixHQUFZLEtBQUssQ0FBQztJQUNuQyxVQUFNLEdBQUcsMENBQTBDLENBQUEsQ0FBQSwwQkFBMEI7SUFDN0UsU0FBSyxHQUFHLDBCQUEwQixDQUFBLENBQUEsNEJBQTRCO0lBQzlELFVBQU0sR0FBRyxpQkFBaUIsQ0FBQTtJQStVckMsVUFBQztDQS9WRCxBQStWQyxJQUFBO2tCQS9Wb0IsR0FBRyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMZWFkZXJib2FyZCBmcm9tIFwiLi9MZWFkZXJib2FyZFwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCBpbWFnZV9iYXNlNjQgZnJvbSAnLi9pbWFnZSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFwaSB7XG4gICAgc3RhdGljIGluaXRpYWxpemVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgc3RhdGljIGNvaW46IG51bWJlciA9IDEwMDAwO1xuICAgIHN0YXRpYyB0aWNrZXQ6IG51bWJlciA9IDA7XG4gICAgc3RhdGljIHVzZXJuYW1lOiBzdHJpbmcgPSAnRE0nO1xuICAgIHN0YXRpYyBwbGF5ZXJJZDogc3RyaW5nID0gJ0RNJztcbiAgICBzdGF0aWMgbG9jYWxlOiBzdHJpbmcgPSAndmlfVk4nO1xuICAgIHN0YXRpYyBwaG90bzogc3RyaW5nID0gJ2h0dHBzOi8vaS5pbWd1ci5jb20vRlVPeHM0My5qcGcnO1xuICAgIHN0YXRpYyBpc0RpcnR5OiBib29sZWFuID0gZmFsc2U7XG4gICAgc3RhdGljIHByZWxvYWRlZFJld2FyZGVkVmlkZW86IEZCSW5zdGFudC5BZEluc3RhbmNlID0gbnVsbDtcbiAgICBzdGF0aWMgcHJlbG9hZGVkSW50ZXJzdGl0aWFsOiBGQkluc3RhbnQuQWRJbnN0YW5jZSA9IG51bGw7XG4gICAgc3RhdGljIGNhblN1YnNjcmliZUJvdCA9IGZhbHNlO1xuICAgIHN0YXRpYyBkYWlseUJvbnVzRGF5OiBudW1iZXIgPSAwO1xuICAgIHN0YXRpYyBkYWlseUJvbnVzQ2xhaW1lZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHN0YXRpYyBsYl9hcGkgPSAnaHR0cHM6Ly9sZWFkZXJib2FyZC5hZHBpYS5jb20udm4vaW5zdGFudCcvLydodHRwczovL2xiLmRvem8udm4vYXBpJ1xuICAgIHN0YXRpYyBsYl9pZCA9ICdhMzMwZTUwNTQ4MDRmN2ZiNzNiZDFmOTknLy8nNWY3MTQ0MTU2MzBiOWI5ZmY4MTQ2ZjE1J1xuICAgIHN0YXRpYyBJRF9BUFAgPSAnNDQwMjAxNjg3MTI0NTk4J1xuXG4gICAgcHVibGljIHN0YXRpYyBpbml0QXN5bmMoY2FsbGJhY2s6IChib251czogYm9vbGVhbiwgZGF5OiBudW1iZXIpID0+IHZvaWQsIGxlYWRlcmJvYXJkOiBMZWFkZXJib2FyZCkge1xuICAgICAgICBpZiAoQXBpLmluaXRpYWxpemVkKSB7XG4gICAgICAgICAgICBBcGkubG9hZExlYWRlcmJvYXJkKGxlYWRlcmJvYXJkKTtcbiAgICAgICAgICAgIHJldHVybiBjYWxsYmFjayhmYWxzZSwgQXBpLmRhaWx5Qm9udXNEYXkpOyAvLyBkYWlseV9ib251c19jbGFpbWVkX2F0LCBkYWlseV9ib251c19kYXlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghQXBpLmluaXRpYWxpemVkICYmIHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEFwaS5pbml0aWFsaXplZCA9IHRydWU7XG4gICAgICAgICAgICBBcGkucGhvdG8gPSBGQkluc3RhbnQucGxheWVyLmdldFBob3RvKCk7XG4gICAgICAgICAgICBBcGkudXNlcm5hbWUgPSBGQkluc3RhbnQucGxheWVyLmdldE5hbWUoKTtcbiAgICAgICAgICAgIEFwaS5wbGF5ZXJJZCA9IEZCSW5zdGFudC5wbGF5ZXIuZ2V0SUQoKTtcbiAgICAgICAgICAgIEFwaS5sb2NhbGUgPSBGQkluc3RhbnQuZ2V0TG9jYWxlKCk7XG5cbiAgICAgICAgICAgIEFwaS5nZXRQbGF5ZXJEYXRhKGNhbGxiYWNrKTtcbiAgICAgICAgICAgIEFwaS5sb2FkTGVhZGVyYm9hcmQobGVhZGVyYm9hcmQpO1xuICAgICAgICAgICAgQXBpLnN1YnNjcmliZUJvdCgpO1xuICAgICAgICAgICAgQXBpLmNyZWF0ZVNob3J0Y3V0KCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBBcGkudGlja2V0ID0gMTA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0RheSA9IDA7XG4gICAgICAgICAgICAvLyBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIEFwaS5sb2NhbGUgPSAndmlfVk4nO1xuICAgICAgICAgICAgLy8gQXBpLmxvY2FsZSA9ICd0aF9USCc7XG4gICAgICAgICAgICAvLyBjYWxsYmFjayh0cnVlLCBBcGkuZGFpbHlCb251c0RheSk7XG4gICAgICAgICAgICBBcGkubG9hZExlYWRlcmJvYXJkKGxlYWRlcmJvYXJkKTtcbiAgICAgICAgICAgIC8vIH0sIDUwMDApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgZ2V0UGxheWVyRGF0YShjYWxsYmFjazogKGJvbnVzOiBib29sZWFuLCBkYXk6IG51bWJlcikgPT4gdm9pZCkge1xuICAgICAgICBGQkluc3RhbnQucGxheWVyLmdldERhdGFBc3luYyhbJ2NvaW4nLCAndGlja2V0JywgJ2RhaWx5X2JvbnVzX2NsYWltZWRfYXQnLCAnZGFpbHlfYm9udXNfZGF5J10pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGNjLmxvZygnZGF0YSBpcyBsb2FkZWQnKTtcbiAgICAgICAgICAgIEFwaS5jb2luID0gZGF0YVsnY29pbiddO1xuICAgICAgICAgICAgaWYgKEFwaS5jb2luID09IHVuZGVmaW5lZCB8fCBBcGkuY29pbiA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgQXBpLmNvaW4gPSBDb25maWcuZGVmYXVsdENvaW47XG4gICAgICAgICAgICAgICAgQXBpLmlzRGlydHkgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgQXBpLnRpY2tldCA9IGRhdGFbJ3RpY2tldCddIHx8IDA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0RheSA9IGRhdGFbJ2RhaWx5X2JvbnVzX2RheSddIHx8IDA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0NsYWltZWQgPSAhQXBpLmRhaWx5Qm9udXNDbGFpbWFibGUoZGF0YVsnZGFpbHlfYm9udXNfY2xhaW1lZF9hdCddKTtcbiAgICAgICAgICAgIGNhbGxiYWNrKCFBcGkuZGFpbHlCb251c0NsYWltZWQsIEFwaS5kYWlseUJvbnVzRGF5KTtcbiAgICAgICAgICAgIGlmIChBcGkuaXNEaXJ0eSkge1xuICAgICAgICAgICAgICAgIEFwaS5mbHVzaCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBzdWJzY3JpYmVCb3QoKSB7XG4gICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuY2FuU3Vic2NyaWJlQm90QXN5bmMoKS50aGVuKGZ1bmN0aW9uIChjYW5fc3Vic2NyaWJlKSB7XG4gICAgICAgICAgICBjYy5sb2coJ3N1YnNjcmliZUJvdEFzeW5jJywgY2FuX3N1YnNjcmliZSk7XG4gICAgICAgICAgICBBcGkuY2FuU3Vic2NyaWJlQm90ID0gY2FuX3N1YnNjcmliZS52YWx1ZU9mKCk7XG4gICAgICAgICAgICAvLyBUaGVuIHdoZW4geW91IHdhbnQgdG8gYXNrIHRoZSBwbGF5ZXIgdG8gc3Vic2NyaWJlXG4gICAgICAgICAgICBpZiAoQXBpLmNhblN1YnNjcmliZUJvdCkge1xuICAgICAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc3Vic2NyaWJlQm90QXN5bmMoKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAvLyBQbGF5ZXIgaXMgc3Vic2NyaWJlZCB0byB0aGUgYm90XG4gICAgICAgICAgICAgICAgKS5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBIYW5kbGUgc3Vic2NyaXB0aW9uIGZhaWx1cmVcbiAgICAgICAgICAgICAgICAgICAgY2MubG9nKCdzdWJzY3JpYmVCb3RBc3luYycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xuICAgICAgICAgICAgY2MubG9nKCdzdWJzY3JpYmVCb3RBc3luYycsIGUpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBjcmVhdGVTaG9ydGN1dCgpIHtcbiAgICAgICAgRkJJbnN0YW50LmNhbkNyZWF0ZVNob3J0Y3V0QXN5bmMoKVxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGNhbkNyZWF0ZVNob3J0Y3V0KSB7XG4gICAgICAgICAgICAgICAgaWYgKGNhbkNyZWF0ZVNob3J0Y3V0KSB7XG4gICAgICAgICAgICAgICAgICAgIEZCSW5zdGFudC5jcmVhdGVTaG9ydGN1dEFzeW5jKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTaG9ydGN1dCBjcmVhdGVkXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTaG9ydGN1dCBub3QgY3JlYXRlZFxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBsb2FkTGVhZGVyYm9hcmQobGVhZGVyYm9hcmQ6IExlYWRlcmJvYXJkKSB7XG4gICAgICAgIGZldGNoKGAke0FwaS5sYl9hcGl9LyR7QXBpLmxiX2lkfWApLnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKS50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuXG4gICAgICAgICAgICByZXM/LmVudHJpZXM/Lmxlbmd0aCAmJiByZXMuZW50cmllcy5tYXAoKGUsIGkpID0+IHtcbiAgICAgICAgICAgICAgICBsZWFkZXJib2FyZC5yZW5kZXIoXG4gICAgICAgICAgICAgICAgICAgIGkgKyAxLFxuICAgICAgICAgICAgICAgICAgICBlLnVzZXJuYW1lLFxuICAgICAgICAgICAgICAgICAgICBlLnNjb3JlLFxuICAgICAgICAgICAgICAgICAgICBlLnBob3RvLFxuICAgICAgICAgICAgICAgICAgICBlLnVzZXJfaWQsXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgIC8vIGZvciAobGV0IGkgPSAwOyBpIDwgcmVzLmVudHJpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIC8vICAgICBjb25zdCBlID0gcmVzLmVudHJpZXNbaV07XG4gICAgICAgICAgICAvLyAgICAgbGVhZGVyYm9hcmQucmVuZGVyKFxuICAgICAgICAgICAgLy8gICAgICAgICBpICsgMSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS51c2VybmFtZSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS5zY29yZSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS5waG90byxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS51c2VyX2lkLFxuICAgICAgICAgICAgLy8gICAgICk7XG4gICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICBsZWFkZXJib2FyZC5vbkxvYWRDb21wbGV0ZSgpO1xuICAgICAgICB9KS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBwcmVsb2FkUmV3YXJkZWRWaWRlbyhjYWxsYmFjazogKCkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICAgICAgLy8gaWYgKCF3aW5kb3cuaGFzT3duUHJvcGVydHkoJ0ZCSW5zdGFudCcpKSB7XG4gICAgICAgIGlmIChjYWxsYmFjaykgY2FsbGJhY2soKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgICAvLyB9XG4gICAgICAgIGlmIChBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyB8fCAhQ29uZmlnLnJld2FyZF92aWRlbykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIEZCSW5zdGFudC5nZXRSZXdhcmRlZFZpZGVvQXN5bmMoQ29uZmlnLnJld2FyZF92aWRlbykudGhlbihmdW5jdGlvbiAocmV3YXJkZWQpIHtcbiAgICAgICAgICAgIC8vIExvYWQgdGhlIEFkIGFzeW5jaHJvbm91c2x5XG4gICAgICAgICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IHJld2FyZGVkO1xuICAgICAgICAgICAgcmV0dXJuIEFwaS5wcmVsb2FkZWRSZXdhcmRlZFZpZGVvLmxvYWRBc3luYygpO1xuICAgICAgICB9KS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIGNjLmxvZygnUmV3YXJkZWQgdmlkZW8gcHJlbG9hZGVkJyk7XG4gICAgICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIH0pLmNhdGNoKChlKSA9PiB7XG4gICAgICAgICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IG51bGw7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUubWVzc2FnZSk7XG4gICAgICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2hvd1Jld2FyZGVkVmlkZW8oc3VjY2VzczogKCkgPT4gdm9pZCwgZXJyb3I6IChtc2c6IHN0cmluZykgPT4gdm9pZCkge1xuICAgICAgICAvLyBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgLy8gICAgIHJldHVybiBlcnJvcigncnJyJyk7XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gaWYgKCFDb25maWcucmV3YXJkX3ZpZGVvKSB7XG4gICAgICAgIHN1Y2Nlc3MoKTtcbiAgICAgICAgLy8gfVxuICAgICAgICAvLyBpZiAoIUFwaS5wcmVsb2FkZWRSZXdhcmRlZFZpZGVvKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmVycm9yKCdhZCBub3QgbG9hZCB5ZXQnKTtcbiAgICAgICAgLy8gICAgIHJldHVybiBlcnJvcignYWQgbm90IGxvYWQgeWV0Jyk7XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gQXBpLnByZWxvYWRlZFJld2FyZGVkVmlkZW8uc2hvd0FzeW5jKCkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vICAgICBzdWNjZXNzKCk7XG4gICAgICAgIC8vICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IG51bGw7XG4gICAgICAgIC8vICAgICBjYy5sb2coJ1Jld2FyZGVkIHZpZGVvIHdhdGNoZWQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIC8vIH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIC8vICAgICBlcnJvcihlLm1lc3NhZ2UpO1xuICAgICAgICAvLyAgICAgQXBpLnByZWxvYWRlZFJld2FyZGVkVmlkZW8gPSBudWxsO1xuICAgICAgICAvLyAgICAgY29uc29sZS5lcnJvcihlLm1lc3NhZ2UpO1xuICAgICAgICAvLyB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHByZWxvYWRJbnRlcnN0aXRpYWxBZChjYWxsYmFjazogKCkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICAgICAgLy8gaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykgJiYgIUFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgIEZCSW5zdGFudC5nZXRJbnRlcnN0aXRpYWxBZEFzeW5jKENvbmZpZy5pbnRlcnRpdGFsX2FkcykudGhlbihmdW5jdGlvbihpbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgICAgICAvLyBMb2FkIHRoZSBBZCBhc3luY2hyb25vdXNseVxuICAgICAgICAvLyAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBpbnRlcnN0aXRpYWw7XG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwubG9hZEFzeW5jKCk7XG4gICAgICAgIC8vICAgICB9KS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyAgICAgICAgIGNjLmxvZygnSW50ZXJzdGl0aWFsIHByZWxvYWRlZCcpO1xuICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIC8vIH0pLmNhdGNoKGZ1bmN0aW9uKGVycil7XG4gICAgICAgIC8vICAgICBjYy5lcnJvcignSW50ZXJzdGl0aWFsIGZhaWxlZCB0byBwcmVsb2FkJyk7XG4gICAgICAgIC8vICAgICBjYy5lcnJvcihlcnIpO1xuICAgICAgICAvLyB9KTtcbiAgICAgICAgLy8gfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2hvd0ludGVyc3RpdGlhbEFkKCkge1xuICAgICAgICAvLyBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAvLyAgICAgaWYgKEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgICAgICBBcGkucHJlbG9hZGVkSW50ZXJzdGl0aWFsLnNob3dBc3luYygpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAvLyAgICAgICAgICAgICAvLyBQZXJmb3JtIHBvc3QtYWQgc3VjY2VzcyBvcGVyYXRpb25cbiAgICAgICAgLy8gICAgICAgICAgICAgY2MubG9nKCdJbnRlcnN0aXRpYWwgYWQgZmluaXNoZWQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIC8vICAgICAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBudWxsO1xuICAgICAgICAvLyAgICAgICAgIH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIGNjLmVycm9yKGUubWVzc2FnZSk7XG4gICAgICAgIC8vICAgICAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBudWxsO1xuICAgICAgICAvLyAgICAgICAgIH0pO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBpc0ludGVyc3RpdGlhbEFkTG9hZGVkKCk6IGJvb2xlYW4ge1xuICAgICAgICBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gISFBcGkucHJlbG9hZGVkSW50ZXJzdGl0aWFsO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgY2hhbGxlbmdlKHBsYXllcklkOiBzdHJpbmcpOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgYXdhaXQgRkJJbnN0YW50LmNvbnRleHQuY3JlYXRlQXN5bmMocGxheWVySWQpO1xuICAgICAgICByZXR1cm4gRkJJbnN0YW50LnVwZGF0ZUFzeW5jKHtcbiAgICAgICAgICAgIGFjdGlvbjogJ0NVU1RPTScsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJ3BsYXlfdHVybicsXG4gICAgICAgICAgICBjdGE6ICdDaMahaSBuZ2F5JyxcbiAgICAgICAgICAgIGltYWdlOiBpbWFnZV9iYXNlNjQsXG4gICAgICAgICAgICB0ZXh0OiBBcGkudXNlcm5hbWUgKyAnIMSRYW5nIHRow6FjaCDEkeG6pXUgYuG6oW4uIE5o4bqlbiBjaMahaSBuZ2F5IScsXG4gICAgICAgICAgICBzdHJhdGVneTogJ0lNTUVESUFURSdcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBzaGFyZUFzeW5jKCk6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgcmV0dXJuIEZCSW5zdGFudC5zaGFyZUFzeW5jKHtcbiAgICAgICAgICAgICAgICBpbnRlbnQ6ICdTSEFSRScsXG4gICAgICAgICAgICAgICAgaW1hZ2U6IGltYWdlX2Jhc2U2NCxcbiAgICAgICAgICAgICAgICB0ZXh0OiAnVGnhur9uIGxpw6puIG1p4buBbiBuYW0gMjAyMCEnLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgaW52aXRlKHN1Y2Nlc3M6IChtc2c6IHN0cmluZykgPT4gdm9pZCwgZXJyb3I6IChyZWFzb246IGFueSkgPT4gdm9pZCkge1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgRkJJbnN0YW50LmNvbnRleHRcbiAgICAgICAgICAgICAgICAuY2hvb3NlQXN5bmMoKVxuICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhGQkluc3RhbnQuY29udGV4dC5nZXRJRCgpKTtcbiAgICAgICAgICAgICAgICAgICAgRkJJbnN0YW50LnVwZGF0ZUFzeW5jKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ0NVU1RPTScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogJ3BsYXlfdHVybicsXG4gICAgICAgICAgICAgICAgICAgICAgICBjdGE6ICdDaMahaSBuZ2F5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlOiBpbWFnZV9iYXNlNjQsXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBBcGkudXNlcm5hbWUgKyAnIG3hu51pIGLhuqFuIGPDuW5nIGNoxqFpIGdhbWUgdGnhur9uIGxpw6puIG1p4buBbiBuYW0nLFxuICAgICAgICAgICAgICAgICAgICAgICAgc3RyYXRlZ3k6ICdJTU1FRElBVEUnXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pLmNhdGNoKGVycm9yKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHN1Y2Nlc3MoJycpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBjb2luSW5jcmVtZW50KHZhbDogbnVtYmVyKSB7XG4gICAgICAgIEFwaS51cGRhdGVDb2luKEFwaS5jb2luICsgdmFsKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHVwZGF0ZUNvaW4oY29pbjogbnVtYmVyKSB7XG4gICAgICAgIEFwaS5jb2luID0gY29pbjtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgY29pbjogQXBpLmNvaW4gfSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY2MubG9nKCdkYXRhIGlzIHNldCcpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLnNldFNjb3JlQXN5bmMoQXBpLmNvaW4pLnRoZW4oKCkgPT4gY2MubG9nKCdTY29yZSBzYXZlZCcpKS5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHVwZGF0ZVRpY2tldCgpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgdGlja2V0OiBBcGkudGlja2V0IH0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNjLmxvZygndGlja2V0IGlzIHNldCcpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGNsYWltRGFpbHlCb251cyhkYXk6IG51bWJlcikge1xuICAgICAgICBBcGkuZGFpbHlCb251c0NsYWltZWQgPSB0cnVlO1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgbGV0IGNsYWltZWRfYXQgPSBuZXcgRGF0ZSgpO1xuICAgICAgICAgICAgRkJJbnN0YW50LnBsYXllci5zZXREYXRhQXN5bmMoe1xuICAgICAgICAgICAgICAgIGNvaW46IEFwaS5jb2luLFxuICAgICAgICAgICAgICAgIHRpY2tldDogQXBpLnRpY2tldCxcbiAgICAgICAgICAgICAgICAnZGFpbHlfYm9udXNfY2xhaW1lZF9hdCc6IGNsYWltZWRfYXQudG9TdHJpbmcoKSxcbiAgICAgICAgICAgICAgICAnZGFpbHlfYm9udXNfZGF5JzogZGF5XG4gICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjYy5sb2coJ2RhdGEgaXMgc2V0Jyk7XG4gICAgICAgICAgICB9LCBmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgICAgICAgICAgY2MubG9nKCdkYXRhIGlzIG5vdCBzZXQnLCByZWFzb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLmRhaWx5Qm9udXNEYXkrKztcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBkYWlseUJvbnVzQ2xhaW1hYmxlKGNsYWltZWRBdDogc3RyaW5nIHwgRGF0ZSkge1xuICAgICAgICBpZiAoIWNsYWltZWRBdCkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmICh0eXBlb2YgY2xhaW1lZEF0ID09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICBjbGFpbWVkQXQgPSBuZXcgRGF0ZShjbGFpbWVkQXQpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IG5vdyA9IG5ldyBEYXRlKCk7XG4gICAgICAgIGlmIChub3cuZ2V0RnVsbFllYXIoKSAhPSBjbGFpbWVkQXQuZ2V0RnVsbFllYXIoKSkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmIChub3cuZ2V0TW9udGgoKSAhPSBjbGFpbWVkQXQuZ2V0TW9udGgoKSkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmIChub3cuZ2V0RGF0ZSgpICE9IGNsYWltZWRBdC5nZXREYXRlKCkpIHJldHVybiB0cnVlO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBmbHVzaCgpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgY29pbjogQXBpLmNvaW4sIHRpY2tldDogQXBpLnRpY2tldCB9KS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjYy5sb2coJ2RhdGEgaXMgc2V0Jyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBBcGkuc2V0U2NvcmVBc3luYyhBcGkuY29pbikudGhlbihyZXMgPT4gcmVzLmpzb24oKSkudGhlbihyZXMgPT4gY2MubG9nKHJlcykpLmNhdGNoKGVyciA9PiBjb25zb2xlLmVycm9yKGVycikpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgbG9nRXZlbnQoZXZlbnROYW1lOiBzdHJpbmcsIHZhbHVlVG9TdW0/OiBudW1iZXIsIHBhcmFtZXRlcnM/OiBPYmplY3QpIHtcbiAgICAgICAgaWYgKCFldmVudE5hbWUpIHJldHVyblxuICAgICAgICAvLyBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gd2luZG93LmZpcmViYXNlLmFuYWx5dGljcygpLmxvZ0V2ZW50KGV2ZW50TmFtZSwgcGFyYW1ldGVycyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhldmVudE5hbWUsIHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2FuYWx5dGljIGVycm9yJywgZXJyb3IpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICB9XG4gICAgICAgIC8vIEZCSW5zdGFudC5sb2dFdmVudChldmVudE5hbWUsIHZhbHVlVG9TdW0sIHBhcmFtZXRlcnMpO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgc2V0U2NvcmVBc3luYyhzY29yZSkge1xuICAgICAgICAvLyBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHJldHVybjtcblxuICAgICAgICBjb25zdCBkYXRhID0geyBzY29yZSwgdXNlcklkOiBBcGkucGxheWVySWQsIG5hbWU6IEFwaS51c2VybmFtZSwgcGhvdG86IEFwaS5waG90byB9XG4gICAgICAgIHJldHVybiBmZXRjaChgJHtBcGkubGJfYXBpfS8ke0FwaS5sYl9pZH1gLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyB9LFxuICAgICAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGRhdGEpXG4gICAgICAgICAgICB9XG4gICAgICAgIClcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHJhbmRvbUJvbnVzKCkge1xuICAgICAgICBjb25zdCByID0gTWF0aC5yYW5kb20oKTtcbiAgICAgICAgaWYgKHIgPCAwLjA1KSByZXR1cm4gNDAwMDAwO1xuICAgICAgICBpZiAociA8IDAuMikgcmV0dXJuIDMwMDAwMDtcbiAgICAgICAgaWYgKHIgPCAwLjUpIHJldHVybiAyMDAwMDA7XG4gICAgICAgIHJldHVybiAxMDAwMDA7XG4gICAgfVxufSJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Bot.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2fab9HUN9BE+7ULtb/jaU1f', 'Bot');
// Scripts/Bot.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardGroup_1 = require("./CardGroup");
var Helper_1 = require("./Helper");
var Bot = /** @class */ (function () {
    function Bot() {
    }
    Bot.random = function (handCards) {
        var cards = null;
        var firstCard = Helper_1.default.findCard(handCards, 3, 1);
        if (firstCard) {
            var straight = Bot.findAllCardByRank(handCards, 3);
            if (straight.length >= 3) {
                return new CardGroup_1.default(straight, CardGroup_1.default.House);
            }
            return new CardGroup_1.default([firstCard], CardGroup_1.default.Single);
        }
        cards = Bot.findStraightCard(handCards);
        if (cards) {
            return new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        cards = Bot.findHouseCard(handCards);
        if (cards && !(cards[0].rank == 15 && cards.length < handCards.length)) {
            return new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        var card = Helper_1.default.findMinCard(handCards);
        return new CardGroup_1.default([card], CardGroup_1.default.Single);
    };
    Bot.suggest = function (commonCards, handCards) {
        var common = commonCards.peek();
        if (common.highest.rank == 15 && common.count() == 3) {
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 2) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 4 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var pair = Helper_1.default.findHouseCard(handCards, common.highest, 2);
            if (pair) {
                return new CardGroup_1.default(pair, CardGroup_1.default.House);
            }
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 1) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 3 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var card = Helper_1.default.findMinCard(handCards, common.highest);
            if (card) {
                return new CardGroup_1.default([card], CardGroup_1.default.Single);
            }
            return null;
        }
        if (common.kind == CardGroup_1.default.Single) {
            var cards = Helper_1.default.findMinCard(handCards, common.highest);
            return cards == null ? null : new CardGroup_1.default([cards], CardGroup_1.default.Single);
        }
        if (common.kind == CardGroup_1.default.House) {
            var cards = Helper_1.default.findHouseCard(handCards, common.highest, common.count());
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        if (common.kind == CardGroup_1.default.Straight) {
            var cards = Helper_1.default.findStraightCard(handCards, common);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        if (common.kind == CardGroup_1.default.MultiPair) {
            var cards = Helper_1.default.findMultiPairCard(handCards, common.highest, common.count() / 2);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
        }
        return null;
    };
    Bot.findStraightCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            if (cardStarted.rank < 13) {
                var straight = Bot._findStraightCard(cards, cardStarted);
                if (straight.length >= 3) {
                    return straight;
                }
            }
        }
        return null;
    };
    Bot._findStraightCard = function (cards, started) {
        var straight = [started];
        for (var c = started.rank + 1; c < 15; c++) {
            var card = Helper_1.default.findCardByRank(cards, c);
            if (card == null) {
                break;
            }
            else {
                straight.push(card);
            }
        }
        return straight;
    };
    Bot.findHouseCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            var straight = Bot.findAllCardByRank(cards, cardStarted.rank);
            if (straight.length >= 2) {
                return straight;
            }
        }
        return null;
    };
    Bot.findAllCardByRank = function (cards, rank) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
            }
        }
        return setCards;
    };
    return Bot;
}());
exports.default = Bot;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0JvdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLHlDQUFvQztBQUNwQyxtQ0FBOEI7QUFHOUI7SUFBQTtJQXFJQSxDQUFDO0lBcElVLFVBQU0sR0FBYixVQUFjLFNBQXNCO1FBQ2hDLElBQUksS0FBSyxHQUFXLElBQUksQ0FBQztRQUN6QixJQUFJLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2pELElBQUksU0FBUyxFQUFFO1lBQ1gsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixPQUFPLElBQUksbUJBQVMsQ0FBQyxRQUFRLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuRDtZQUNELE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN2RDtRQUVELEtBQUssR0FBRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEMsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNuRDtRQUVELEtBQUssR0FBRyxHQUFHLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNwRSxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoRDtRQUVELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sV0FBTyxHQUFkLFVBQWUsV0FBdUIsRUFBRSxTQUFzQjtRQUMxRCxJQUFJLE1BQU0sR0FBRyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFaEMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNsRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNsRCxJQUFJLEtBQUssR0FBRyxnQkFBTSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksS0FBSyxFQUFFO2dCQUNQLE9BQU8sSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRSxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BEO1lBQ0QsSUFBSSxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwRCxJQUFJLElBQUksRUFBRTtnQkFDTixPQUFPLElBQUksbUJBQVMsQ0FBQyxJQUFJLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMvQztZQUNELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksSUFBSSxFQUFFO2dCQUNOLE9BQU8sSUFBSSxtQkFBUyxDQUFDLElBQUksRUFBRSxtQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQy9DO1lBQ0QsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDbEQsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUM3RCxJQUFJLEtBQUssRUFBRTtnQkFDUCxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwRDtZQUNELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDcEQsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sT0FBTyxJQUFJLG1CQUFTLENBQUMsSUFBSSxFQUFFLG1CQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDL0M7WUFDRCxJQUFJLElBQUksR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pELElBQUksSUFBSSxFQUFFO2dCQUNOLE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNsRDtZQUNELE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDakMsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxtQkFBUyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMxRTtRQUVELElBQUksTUFBTSxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLEtBQUssR0FBRyxnQkFBTSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUM1RSxPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRSxtQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZFO1FBRUQsSUFBSSxNQUFNLENBQUMsSUFBSSxJQUFJLG1CQUFTLENBQUMsUUFBUSxFQUFFO1lBQ25DLElBQUksS0FBSyxHQUFHLGdCQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLG1CQUFTLENBQUMsS0FBSyxFQUFFLG1CQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUU7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxTQUFTLEVBQUU7WUFDcEMsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEYsT0FBTyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMzRTtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxvQkFBZ0IsR0FBdkIsVUFBd0IsS0FBYTtRQUNqQyxnQkFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxXQUFXLENBQUMsSUFBSSxHQUFHLEVBQUUsRUFBRTtnQkFDdkIsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDekQsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtvQkFDdEIsT0FBTyxRQUFRLENBQUM7aUJBQ25CO2FBQ0o7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxxQkFBaUIsR0FBeEIsVUFBeUIsS0FBYSxFQUFFLE9BQWE7UUFDakQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtnQkFBRSxNQUFNO2FBQUU7aUJBQ3ZCO2dCQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFBRTtTQUNoQztRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFTSxpQkFBYSxHQUFwQixVQUFxQixLQUFhO1FBQzlCLGdCQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RCxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixPQUFPLFFBQVEsQ0FBQzthQUNuQjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLHFCQUFpQixHQUF4QixVQUF5QixLQUFhLEVBQUUsSUFBWTtRQUNoRCxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsS0FBSyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3hDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDM0I7U0FDSjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDTCxVQUFDO0FBQUQsQ0FySUEsQUFxSUMsSUFBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcbmltcG9ydCBDYXJkR3JvdXAgZnJvbSBcIi4vQ2FyZEdyb3VwXCI7XG5pbXBvcnQgSGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuaW1wb3J0IENvbW1vbkNhcmQgZnJvbSBcIi4vQ29tbW9uQ2FyZFwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCb3Qge1xuICAgIHN0YXRpYyByYW5kb20oaGFuZENhcmRzOiBBcnJheTxDYXJkPik6IENhcmRHcm91cCB7XG4gICAgICAgIGxldCBjYXJkczogQ2FyZFtdID0gbnVsbDtcbiAgICAgICAgbGV0IGZpcnN0Q2FyZCA9IEhlbHBlci5maW5kQ2FyZChoYW5kQ2FyZHMsIDMsIDEpO1xuICAgICAgICBpZiAoZmlyc3RDYXJkKSB7XG4gICAgICAgICAgICBsZXQgc3RyYWlnaHQgPSBCb3QuZmluZEFsbENhcmRCeVJhbmsoaGFuZENhcmRzLCAzKTtcbiAgICAgICAgICAgIGlmIChzdHJhaWdodC5sZW5ndGggPj0gMykge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKHN0cmFpZ2h0LCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoW2ZpcnN0Q2FyZF0sIENhcmRHcm91cC5TaW5nbGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FyZHMgPSBCb3QuZmluZFN0cmFpZ2h0Q2FyZChoYW5kQ2FyZHMpO1xuICAgICAgICBpZiAoY2FyZHMpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKGNhcmRzLCBDYXJkR3JvdXAuU3RyYWlnaHQpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FyZHMgPSBCb3QuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMpO1xuICAgICAgICBpZiAoY2FyZHMgJiYgIShjYXJkc1swXS5yYW5rID09IDE1ICYmIGNhcmRzLmxlbmd0aCA8IGhhbmRDYXJkcy5sZW5ndGgpKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChjYXJkcywgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBjYXJkID0gSGVscGVyLmZpbmRNaW5DYXJkKGhhbmRDYXJkcyk7XG4gICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKFtjYXJkXSwgQ2FyZEdyb3VwLlNpbmdsZSk7XG4gICAgfVxuXG4gICAgc3RhdGljIHN1Z2dlc3QoY29tbW9uQ2FyZHM6IENvbW1vbkNhcmQsIGhhbmRDYXJkczogQXJyYXk8Q2FyZD4pOiBDYXJkR3JvdXAge1xuICAgICAgICBsZXQgY29tbW9uID0gY29tbW9uQ2FyZHMucGVlaygpO1xuXG4gICAgICAgIGlmIChjb21tb24uaGlnaGVzdC5yYW5rID09IDE1ICYmIGNvbW1vbi5jb3VudCgpID09IDMpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgY29tbW9uLmNvdW50KCkgPT0gMikge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgbnVsbCwgNCAqIDIpO1xuICAgICAgICAgICAgaWYgKGNhcmRzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5NdWx0aVBhaXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGZvdXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIG51bGwsIDQpO1xuICAgICAgICAgICAgaWYgKGZvdXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChmb3VyLCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IHBhaXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIGNvbW1vbi5oaWdoZXN0LCAyKTtcbiAgICAgICAgICAgIGlmIChwYWlyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAocGFpciwgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgY29tbW9uLmNvdW50KCkgPT0gMSkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgbnVsbCwgMyAqIDIpO1xuICAgICAgICAgICAgaWYgKGNhcmRzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5NdWx0aVBhaXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGZvdXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIG51bGwsIDQpO1xuICAgICAgICAgICAgaWYgKGZvdXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChmb3VyLCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGNhcmQgPSBIZWxwZXIuZmluZE1pbkNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCk7XG4gICAgICAgICAgICBpZiAoY2FyZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKFtjYXJkXSwgQ2FyZEdyb3VwLlNpbmdsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb21tb24ua2luZCA9PSBDYXJkR3JvdXAuU2luZ2xlKSB7XG4gICAgICAgICAgICBsZXQgY2FyZHMgPSBIZWxwZXIuZmluZE1pbkNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCk7XG4gICAgICAgICAgICByZXR1cm4gY2FyZHMgPT0gbnVsbCA/IG51bGwgOiBuZXcgQ2FyZEdyb3VwKFtjYXJkc10sIENhcmRHcm91cC5TaW5nbGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5raW5kID09IENhcmRHcm91cC5Ib3VzZSkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRIb3VzZUNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCwgY29tbW9uLmNvdW50KCkpO1xuICAgICAgICAgICAgcmV0dXJuIGNhcmRzID09IG51bGwgPyBudWxsIDogbmV3IENhcmRHcm91cChjYXJkcywgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb21tb24ua2luZCA9PSBDYXJkR3JvdXAuU3RyYWlnaHQpIHtcbiAgICAgICAgICAgIGxldCBjYXJkcyA9IEhlbHBlci5maW5kU3RyYWlnaHRDYXJkKGhhbmRDYXJkcywgY29tbW9uKTtcbiAgICAgICAgICAgIHJldHVybiBjYXJkcyA9PSBudWxsID8gbnVsbCA6IG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5TdHJhaWdodCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoY29tbW9uLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpcikge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgY29tbW9uLmhpZ2hlc3QsIGNvbW1vbi5jb3VudCgpIC8gMik7XG4gICAgICAgICAgICByZXR1cm4gY2FyZHMgPT0gbnVsbCA/IG51bGwgOiBuZXcgQ2FyZEdyb3VwKGNhcmRzLCBDYXJkR3JvdXAuTXVsdGlQYWlyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHN0YXRpYyBmaW5kU3RyYWlnaHRDYXJkKGNhcmRzOiBDYXJkW10pOiBBcnJheTxDYXJkPiB7XG4gICAgICAgIEhlbHBlci5zb3J0KGNhcmRzKTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYXJkcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGNhcmRTdGFydGVkID0gY2FyZHNbaV07XG4gICAgICAgICAgICBpZiAoY2FyZFN0YXJ0ZWQucmFuayA8IDEzKSB7XG4gICAgICAgICAgICAgICAgbGV0IHN0cmFpZ2h0ID0gQm90Ll9maW5kU3RyYWlnaHRDYXJkKGNhcmRzLCBjYXJkU3RhcnRlZCk7XG4gICAgICAgICAgICAgICAgaWYgKHN0cmFpZ2h0Lmxlbmd0aCA+PSAzKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc3RhdGljIF9maW5kU3RyYWlnaHRDYXJkKGNhcmRzOiBDYXJkW10sIHN0YXJ0ZWQ6IENhcmQpOiBBcnJheTxDYXJkPiB7XG4gICAgICAgIGxldCBzdHJhaWdodCA9IFtzdGFydGVkXTtcbiAgICAgICAgZm9yIChsZXQgYyA9IHN0YXJ0ZWQucmFuayArIDE7IGMgPCAxNTsgYysrKSB7XG4gICAgICAgICAgICBsZXQgY2FyZCA9IEhlbHBlci5maW5kQ2FyZEJ5UmFuayhjYXJkcywgYyk7XG4gICAgICAgICAgICBpZiAoY2FyZCA9PSBudWxsKSB7IGJyZWFrOyB9XG4gICAgICAgICAgICBlbHNlIHsgc3RyYWlnaHQucHVzaChjYXJkKTsgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZEhvdXNlQ2FyZChjYXJkczogQ2FyZFtdKTogQXJyYXk8Q2FyZD4ge1xuICAgICAgICBIZWxwZXIuc29ydChjYXJkcyk7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2FyZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBjYXJkU3RhcnRlZCA9IGNhcmRzW2ldO1xuICAgICAgICAgICAgbGV0IHN0cmFpZ2h0ID0gQm90LmZpbmRBbGxDYXJkQnlSYW5rKGNhcmRzLCBjYXJkU3RhcnRlZC5yYW5rKTtcbiAgICAgICAgICAgIGlmIChzdHJhaWdodC5sZW5ndGggPj0gMikge1xuICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZEFsbENhcmRCeVJhbmsoY2FyZHM6IENhcmRbXSwgcmFuazogbnVtYmVyKTogQ2FyZFtdIHtcbiAgICAgICAgbGV0IHNldENhcmRzID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGNhcmRzW2ldLnJhbmsgPT0gcmFuaykge1xuICAgICAgICAgICAgICAgIHNldENhcmRzLnB1c2goY2FyZHNbaV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzZXRDYXJkcztcbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/tween/TweenMove.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '21238R/+BlIzaVSvLlZw08P', 'TweenMove');
// Scripts/tween/TweenMove.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var TweenMove = /** @class */ (function (_super) {
    __extends(TweenMove, _super);
    function TweenMove() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.duration = 2;
        _this.delayHide = 1;
        _this.distance = 180;
        _this.from = cc.Vec2.ZERO;
        _this.time = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    TweenMove.prototype.onLoad = function () {
        this.from = this.node.getPosition();
    };
    TweenMove.prototype.start = function () {
    };
    TweenMove.prototype.update = function (dt) {
        this.time += dt;
        if (this.time < this.duration) {
            var y = this.from.y + this.distance * (this.time / this.duration);
            this.node.setPosition(this.from.x, y);
        }
        if (this.time >= this.duration + this.delayHide) {
            this.node.active = false;
        }
    };
    TweenMove.prototype.play = function () {
        this.time = 0;
        this.node.active = true;
    };
    TweenMove = __decorate([
        ccclass
    ], TweenMove);
    return TweenMove;
}(cc.Component));
exports.default = TweenMove;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3R3ZWVuL1R3ZWVuTW92ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUF1Qyw2QkFBWTtJQUFuRDtRQUFBLHFFQWdDQztRQS9CVyxjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFDdEIsY0FBUSxHQUFXLEdBQUcsQ0FBQztRQUN2QixVQUFJLEdBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDN0IsVUFBSSxHQUFXLENBQUMsQ0FBQzs7SUEyQjdCLENBQUM7SUExQkcsd0JBQXdCO0lBRXhCLDBCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDeEMsQ0FBQztJQUVELHlCQUFLLEdBQUw7SUFFQSxDQUFDO0lBRUQsMEJBQU0sR0FBTixVQUFRLEVBQUU7UUFDTixJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUMzQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDekM7UUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFRCx3QkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQztJQS9CZ0IsU0FBUztRQUQ3QixPQUFPO09BQ2EsU0FBUyxDQWdDN0I7SUFBRCxnQkFBQztDQWhDRCxBQWdDQyxDQWhDc0MsRUFBRSxDQUFDLFNBQVMsR0FnQ2xEO2tCQWhDb0IsU0FBUyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFR3ZWVuTW92ZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgcHJpdmF0ZSBkdXJhdGlvbjogbnVtYmVyID0gMjtcbiAgICBwcml2YXRlIGRlbGF5SGlkZTogbnVtYmVyID0gMTtcbiAgICBwcml2YXRlIGRpc3RhbmNlOiBudW1iZXIgPSAxODA7XG4gICAgcHJpdmF0ZSBmcm9tOiBjYy5WZWMyID0gY2MuVmVjMi5aRVJPO1xuICAgIHByaXZhdGUgdGltZTogbnVtYmVyID0gMDtcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIG9uTG9hZCAoKSB7XG4gICAgICAgIHRoaXMuZnJvbSA9IHRoaXMubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgIH1cblxuICAgIHN0YXJ0ICgpIHtcblxuICAgIH1cblxuICAgIHVwZGF0ZSAoZHQpIHtcbiAgICAgICAgdGhpcy50aW1lICs9IGR0O1xuICAgICAgICBpZiAodGhpcy50aW1lIDwgdGhpcy5kdXJhdGlvbikge1xuICAgICAgICAgICAgbGV0IHkgPSB0aGlzLmZyb20ueSArIHRoaXMuZGlzdGFuY2UgKiAodGhpcy50aW1lIC8gdGhpcy5kdXJhdGlvbik7XG4gICAgICAgICAgICB0aGlzLm5vZGUuc2V0UG9zaXRpb24odGhpcy5mcm9tLngsIHkpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMudGltZSA+PSB0aGlzLmR1cmF0aW9uICsgdGhpcy5kZWxheUhpZGUpIHtcbiAgICAgICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHBsYXkoKSB7XG4gICAgICAgIHRoaXMudGltZSA9IDA7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/en_US_tut.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b99d6dWx5hBq5cWjzQLp+PI', 'en_US_tut');
// lang/en_US_tut.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tutor = "The game is for four players. A standard 52 card deck is used; there are no Jokers and no wild cards. It is possible for two or three to play. It can also be played by more than four players, using two 52 card packs shuffled together.\n\nThe game is normally dealt and played clockwise, but can be played anticlockwise instead if the players agree in advance to do so.\n\nThe ranking of the cards is: Two (highest), Ace, King, Queen, Jack, Ten, Nine, Eight, Seven, Six, Five, Four, Three (lowest).\n\nWithin each rank there is also an order of suits: Hearts (highest), Diamonds, Clubs, Spades (lowest).\n\nSo the 3 of Spades is the lowest card in the pack, and the 2 of Hearts is the highest. Rank is more important than suit, so for example the 8 beats the 7.\n\nThe Deal\n\nFor the first game, the dealer is chosen at random; subsequently the loser of each game has to deal the next. When there are four players, 13 cards are dealt to each player.\n\nIf there are fewer than four players, 13 cards are still dealt to each player, and there will be some cards left undealt - these are not used in the game. An alternative with three players is, by prior agreement, to deal 17 cards each. When there are only two players, only 13 cards each should be dealt - if all the cards were dealt the players would be able to work out each other's hands, which would spoil the game. When there are more than four players, you can agree in advance either to deal 13 cards each from the double deck, or deal as many cards as possible equally to the players.\n\nThe Play\n\nIn the first game only, the player with the 3 of Spades begins play. If no one has the 3 (in the three or two player game) whoever holds the lowest card begins. The player must begin by playing this lowest card, either on its own or as part of a combination.\n\nIn subsequent games, the winner of the previous game plays first, and can start with any combination.\n\nEach player in turn must now either beat the previously played card or combination, by playing a card or combination that beats it, or pass and not play any cards. The played card(s) are placed in a heap face up in the centre of the table. The play goes around the table as many times as necessary until someone plays a card or combination that no one else beats. When this happens, all the played cards are set aside, and the person whose play was unbeaten starts again by playing any legal card or combination face up to the centre of the table.\n\nIf you pass you are locked out of the play until someone makes a play that no one beats. Only when the cards are set aside and a new card or combination is led are you entitled to play again.\n\nExample (with three players): the player to your right plays a single three, you hold an ace but decide to pass, the player to your left plays a nine and the player to right plays a king. You cannot now beat the king with your ace, because you have already passed. If the third player passes too, and your right hand opponent now leads a queen, you can now play your ace if you want to.\n\nThe legal plays in the game are as follows:\nSingle card The lowest single card is the 3 and the highest is the 2.\nPair Two cards of the same rank - such as 7-7 or Q-Q.\nTriple Three cards of the same rank - such as 5-5-5\nFour of a kind Four cards of the same rank - such as 9-9-9-9.\nSequence Three or more cards of consecutive rank (the suits can be mixed) - such as 4-5-6 or J-Q-K-A. Sequences cannot \"turn the corner\" between two and three - A-2-3 is not a valid sequence because 2 is high and 3 is low.\nDouble Sequence Three or more pairs of consecutive rank - such as 3-3-4-4-5-5 or 6-6-7-7-8-8-9-9.\n\nIn general, a combination can only be beaten by a higher combination of the same type and same number of cards. So if a single card is led, only single cards can be played; if a pair is led only pairs can be played; a three card sequence can only be beaten by a higher three card sequence; and so on. You cannot for example beat a pair with a triple, or a four card sequence with a five card sequence.\n\nTo decide which of two combinations of the same type is higher you just look at the highest card in the combination. For example 7-7 beats 7-7 because the heart beats the diamond. In the same way 8-9-10 beats 8-9-10 because it is the highest cards (the tens) that are compared.\n\nThere are just four exceptions to the rule that a combination can only be beaten by a combination of the same type:\n\nA four of a kind can beat any single two (but not any other single card, such as an ace or king). A four of a kind can be beaten by a higher four of a kind.\n\nA sequence of three pairs (such as 7-7-8-8-9-9) can beat any single two (but not any other single card). A sequence of three pairs can be beaten by a higher sequence of three pairs.\n\nA sequence of four pairs (such as 5-5-6-6-7-7-8-8) can beat a pair of twos (but not any other pair). A sequence of four pairs can be beaten by a higher sequence of four pairs.\n\nA sequence of five pairs (such as 8-8-9-9-10-10-J-J-Q-Q) can beat a set of three twos (but not any other three of a kind). A sequence of five pairs can be beaten by a higher sequence of five pairs.\n\nThese combinations that can beat single twos or sets of twos are sometimes known as bombs or two-bombs, and can be played even by a player who has previously passed.\n\nNote that these exceptions only apply to beating twos, not other cards. For example, if someone plays an ace you cannot beat it with your four of a kind, but if the ace has been beaten by a two, then your four of a kind can be used to beat the two";
exports.default = tutor;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL2VuX1VTX3R1dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sS0FBSyxHQUFHLDAvS0FvRDBPLENBQUE7QUFFeFAsa0JBQWUsS0FBSyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgdHV0b3IgPSBgVGhlIGdhbWUgaXMgZm9yIGZvdXIgcGxheWVycy4gQSBzdGFuZGFyZCA1MiBjYXJkIGRlY2sgaXMgdXNlZDsgdGhlcmUgYXJlIG5vIEpva2VycyBhbmQgbm8gd2lsZCBjYXJkcy4gSXQgaXMgcG9zc2libGUgZm9yIHR3byBvciB0aHJlZSB0byBwbGF5LiBJdCBjYW4gYWxzbyBiZSBwbGF5ZWQgYnkgbW9yZSB0aGFuIGZvdXIgcGxheWVycywgdXNpbmcgdHdvIDUyIGNhcmQgcGFja3Mgc2h1ZmZsZWQgdG9nZXRoZXIuXG5cblRoZSBnYW1lIGlzIG5vcm1hbGx5IGRlYWx0IGFuZCBwbGF5ZWQgY2xvY2t3aXNlLCBidXQgY2FuIGJlIHBsYXllZCBhbnRpY2xvY2t3aXNlIGluc3RlYWQgaWYgdGhlIHBsYXllcnMgYWdyZWUgaW4gYWR2YW5jZSB0byBkbyBzby5cblxuVGhlIHJhbmtpbmcgb2YgdGhlIGNhcmRzIGlzOiBUd28gKGhpZ2hlc3QpLCBBY2UsIEtpbmcsIFF1ZWVuLCBKYWNrLCBUZW4sIE5pbmUsIEVpZ2h0LCBTZXZlbiwgU2l4LCBGaXZlLCBGb3VyLCBUaHJlZSAobG93ZXN0KS5cblxuV2l0aGluIGVhY2ggcmFuayB0aGVyZSBpcyBhbHNvIGFuIG9yZGVyIG9mIHN1aXRzOiBIZWFydHMgKGhpZ2hlc3QpLCBEaWFtb25kcywgQ2x1YnMsIFNwYWRlcyAobG93ZXN0KS5cblxuU28gdGhlIDMgb2YgU3BhZGVzIGlzIHRoZSBsb3dlc3QgY2FyZCBpbiB0aGUgcGFjaywgYW5kIHRoZSAyIG9mIEhlYXJ0cyBpcyB0aGUgaGlnaGVzdC4gUmFuayBpcyBtb3JlIGltcG9ydGFudCB0aGFuIHN1aXQsIHNvIGZvciBleGFtcGxlIHRoZSA4IGJlYXRzIHRoZSA3LlxuXG5UaGUgRGVhbFxuXG5Gb3IgdGhlIGZpcnN0IGdhbWUsIHRoZSBkZWFsZXIgaXMgY2hvc2VuIGF0IHJhbmRvbTsgc3Vic2VxdWVudGx5IHRoZSBsb3NlciBvZiBlYWNoIGdhbWUgaGFzIHRvIGRlYWwgdGhlIG5leHQuIFdoZW4gdGhlcmUgYXJlIGZvdXIgcGxheWVycywgMTMgY2FyZHMgYXJlIGRlYWx0IHRvIGVhY2ggcGxheWVyLlxuXG5JZiB0aGVyZSBhcmUgZmV3ZXIgdGhhbiBmb3VyIHBsYXllcnMsIDEzIGNhcmRzIGFyZSBzdGlsbCBkZWFsdCB0byBlYWNoIHBsYXllciwgYW5kIHRoZXJlIHdpbGwgYmUgc29tZSBjYXJkcyBsZWZ0IHVuZGVhbHQgLSB0aGVzZSBhcmUgbm90IHVzZWQgaW4gdGhlIGdhbWUuIEFuIGFsdGVybmF0aXZlIHdpdGggdGhyZWUgcGxheWVycyBpcywgYnkgcHJpb3IgYWdyZWVtZW50LCB0byBkZWFsIDE3IGNhcmRzIGVhY2guIFdoZW4gdGhlcmUgYXJlIG9ubHkgdHdvIHBsYXllcnMsIG9ubHkgMTMgY2FyZHMgZWFjaCBzaG91bGQgYmUgZGVhbHQgLSBpZiBhbGwgdGhlIGNhcmRzIHdlcmUgZGVhbHQgdGhlIHBsYXllcnMgd291bGQgYmUgYWJsZSB0byB3b3JrIG91dCBlYWNoIG90aGVyJ3MgaGFuZHMsIHdoaWNoIHdvdWxkIHNwb2lsIHRoZSBnYW1lLiBXaGVuIHRoZXJlIGFyZSBtb3JlIHRoYW4gZm91ciBwbGF5ZXJzLCB5b3UgY2FuIGFncmVlIGluIGFkdmFuY2UgZWl0aGVyIHRvIGRlYWwgMTMgY2FyZHMgZWFjaCBmcm9tIHRoZSBkb3VibGUgZGVjaywgb3IgZGVhbCBhcyBtYW55IGNhcmRzIGFzIHBvc3NpYmxlIGVxdWFsbHkgdG8gdGhlIHBsYXllcnMuXG5cblRoZSBQbGF5XG5cbkluIHRoZSBmaXJzdCBnYW1lIG9ubHksIHRoZSBwbGF5ZXIgd2l0aCB0aGUgMyBvZiBTcGFkZXMgYmVnaW5zIHBsYXkuIElmIG5vIG9uZSBoYXMgdGhlIDMgKGluIHRoZSB0aHJlZSBvciB0d28gcGxheWVyIGdhbWUpIHdob2V2ZXIgaG9sZHMgdGhlIGxvd2VzdCBjYXJkIGJlZ2lucy4gVGhlIHBsYXllciBtdXN0IGJlZ2luIGJ5IHBsYXlpbmcgdGhpcyBsb3dlc3QgY2FyZCwgZWl0aGVyIG9uIGl0cyBvd24gb3IgYXMgcGFydCBvZiBhIGNvbWJpbmF0aW9uLlxuXG5JbiBzdWJzZXF1ZW50IGdhbWVzLCB0aGUgd2lubmVyIG9mIHRoZSBwcmV2aW91cyBnYW1lIHBsYXlzIGZpcnN0LCBhbmQgY2FuIHN0YXJ0IHdpdGggYW55IGNvbWJpbmF0aW9uLlxuXG5FYWNoIHBsYXllciBpbiB0dXJuIG11c3Qgbm93IGVpdGhlciBiZWF0IHRoZSBwcmV2aW91c2x5IHBsYXllZCBjYXJkIG9yIGNvbWJpbmF0aW9uLCBieSBwbGF5aW5nIGEgY2FyZCBvciBjb21iaW5hdGlvbiB0aGF0IGJlYXRzIGl0LCBvciBwYXNzIGFuZCBub3QgcGxheSBhbnkgY2FyZHMuIFRoZSBwbGF5ZWQgY2FyZChzKSBhcmUgcGxhY2VkIGluIGEgaGVhcCBmYWNlIHVwIGluIHRoZSBjZW50cmUgb2YgdGhlIHRhYmxlLiBUaGUgcGxheSBnb2VzIGFyb3VuZCB0aGUgdGFibGUgYXMgbWFueSB0aW1lcyBhcyBuZWNlc3NhcnkgdW50aWwgc29tZW9uZSBwbGF5cyBhIGNhcmQgb3IgY29tYmluYXRpb24gdGhhdCBubyBvbmUgZWxzZSBiZWF0cy4gV2hlbiB0aGlzIGhhcHBlbnMsIGFsbCB0aGUgcGxheWVkIGNhcmRzIGFyZSBzZXQgYXNpZGUsIGFuZCB0aGUgcGVyc29uIHdob3NlIHBsYXkgd2FzIHVuYmVhdGVuIHN0YXJ0cyBhZ2FpbiBieSBwbGF5aW5nIGFueSBsZWdhbCBjYXJkIG9yIGNvbWJpbmF0aW9uIGZhY2UgdXAgdG8gdGhlIGNlbnRyZSBvZiB0aGUgdGFibGUuXG5cbklmIHlvdSBwYXNzIHlvdSBhcmUgbG9ja2VkIG91dCBvZiB0aGUgcGxheSB1bnRpbCBzb21lb25lIG1ha2VzIGEgcGxheSB0aGF0IG5vIG9uZSBiZWF0cy4gT25seSB3aGVuIHRoZSBjYXJkcyBhcmUgc2V0IGFzaWRlIGFuZCBhIG5ldyBjYXJkIG9yIGNvbWJpbmF0aW9uIGlzIGxlZCBhcmUgeW91IGVudGl0bGVkIHRvIHBsYXkgYWdhaW4uXG5cbkV4YW1wbGUgKHdpdGggdGhyZWUgcGxheWVycyk6IHRoZSBwbGF5ZXIgdG8geW91ciByaWdodCBwbGF5cyBhIHNpbmdsZSB0aHJlZSwgeW91IGhvbGQgYW4gYWNlIGJ1dCBkZWNpZGUgdG8gcGFzcywgdGhlIHBsYXllciB0byB5b3VyIGxlZnQgcGxheXMgYSBuaW5lIGFuZCB0aGUgcGxheWVyIHRvIHJpZ2h0IHBsYXlzIGEga2luZy4gWW91IGNhbm5vdCBub3cgYmVhdCB0aGUga2luZyB3aXRoIHlvdXIgYWNlLCBiZWNhdXNlIHlvdSBoYXZlIGFscmVhZHkgcGFzc2VkLiBJZiB0aGUgdGhpcmQgcGxheWVyIHBhc3NlcyB0b28sIGFuZCB5b3VyIHJpZ2h0IGhhbmQgb3Bwb25lbnQgbm93IGxlYWRzIGEgcXVlZW4sIHlvdSBjYW4gbm93IHBsYXkgeW91ciBhY2UgaWYgeW91IHdhbnQgdG8uXG5cblRoZSBsZWdhbCBwbGF5cyBpbiB0aGUgZ2FtZSBhcmUgYXMgZm9sbG93czpcblNpbmdsZSBjYXJkIFRoZSBsb3dlc3Qgc2luZ2xlIGNhcmQgaXMgdGhlIDMgYW5kIHRoZSBoaWdoZXN0IGlzIHRoZSAyLlxuUGFpciBUd28gY2FyZHMgb2YgdGhlIHNhbWUgcmFuayAtIHN1Y2ggYXMgNy03IG9yIFEtUS5cblRyaXBsZSBUaHJlZSBjYXJkcyBvZiB0aGUgc2FtZSByYW5rIC0gc3VjaCBhcyA1LTUtNVxuRm91ciBvZiBhIGtpbmQgRm91ciBjYXJkcyBvZiB0aGUgc2FtZSByYW5rIC0gc3VjaCBhcyA5LTktOS05LlxuU2VxdWVuY2UgVGhyZWUgb3IgbW9yZSBjYXJkcyBvZiBjb25zZWN1dGl2ZSByYW5rICh0aGUgc3VpdHMgY2FuIGJlIG1peGVkKSAtIHN1Y2ggYXMgNC01LTYgb3IgSi1RLUstQS4gU2VxdWVuY2VzIGNhbm5vdCBcInR1cm4gdGhlIGNvcm5lclwiIGJldHdlZW4gdHdvIGFuZCB0aHJlZSAtIEEtMi0zIGlzIG5vdCBhIHZhbGlkIHNlcXVlbmNlIGJlY2F1c2UgMiBpcyBoaWdoIGFuZCAzIGlzIGxvdy5cbkRvdWJsZSBTZXF1ZW5jZSBUaHJlZSBvciBtb3JlIHBhaXJzIG9mIGNvbnNlY3V0aXZlIHJhbmsgLSBzdWNoIGFzIDMtMy00LTQtNS01IG9yIDYtNi03LTctOC04LTktOS5cblxuSW4gZ2VuZXJhbCwgYSBjb21iaW5hdGlvbiBjYW4gb25seSBiZSBiZWF0ZW4gYnkgYSBoaWdoZXIgY29tYmluYXRpb24gb2YgdGhlIHNhbWUgdHlwZSBhbmQgc2FtZSBudW1iZXIgb2YgY2FyZHMuIFNvIGlmIGEgc2luZ2xlIGNhcmQgaXMgbGVkLCBvbmx5IHNpbmdsZSBjYXJkcyBjYW4gYmUgcGxheWVkOyBpZiBhIHBhaXIgaXMgbGVkIG9ubHkgcGFpcnMgY2FuIGJlIHBsYXllZDsgYSB0aHJlZSBjYXJkIHNlcXVlbmNlIGNhbiBvbmx5IGJlIGJlYXRlbiBieSBhIGhpZ2hlciB0aHJlZSBjYXJkIHNlcXVlbmNlOyBhbmQgc28gb24uIFlvdSBjYW5ub3QgZm9yIGV4YW1wbGUgYmVhdCBhIHBhaXIgd2l0aCBhIHRyaXBsZSwgb3IgYSBmb3VyIGNhcmQgc2VxdWVuY2Ugd2l0aCBhIGZpdmUgY2FyZCBzZXF1ZW5jZS5cblxuVG8gZGVjaWRlIHdoaWNoIG9mIHR3byBjb21iaW5hdGlvbnMgb2YgdGhlIHNhbWUgdHlwZSBpcyBoaWdoZXIgeW91IGp1c3QgbG9vayBhdCB0aGUgaGlnaGVzdCBjYXJkIGluIHRoZSBjb21iaW5hdGlvbi4gRm9yIGV4YW1wbGUgNy03IGJlYXRzIDctNyBiZWNhdXNlIHRoZSBoZWFydCBiZWF0cyB0aGUgZGlhbW9uZC4gSW4gdGhlIHNhbWUgd2F5IDgtOS0xMCBiZWF0cyA4LTktMTAgYmVjYXVzZSBpdCBpcyB0aGUgaGlnaGVzdCBjYXJkcyAodGhlIHRlbnMpIHRoYXQgYXJlIGNvbXBhcmVkLlxuXG5UaGVyZSBhcmUganVzdCBmb3VyIGV4Y2VwdGlvbnMgdG8gdGhlIHJ1bGUgdGhhdCBhIGNvbWJpbmF0aW9uIGNhbiBvbmx5IGJlIGJlYXRlbiBieSBhIGNvbWJpbmF0aW9uIG9mIHRoZSBzYW1lIHR5cGU6XG5cbkEgZm91ciBvZiBhIGtpbmQgY2FuIGJlYXQgYW55IHNpbmdsZSB0d28gKGJ1dCBub3QgYW55IG90aGVyIHNpbmdsZSBjYXJkLCBzdWNoIGFzIGFuIGFjZSBvciBraW5nKS4gQSBmb3VyIG9mIGEga2luZCBjYW4gYmUgYmVhdGVuIGJ5IGEgaGlnaGVyIGZvdXIgb2YgYSBraW5kLlxuXG5BIHNlcXVlbmNlIG9mIHRocmVlIHBhaXJzIChzdWNoIGFzIDctNy04LTgtOS05KSBjYW4gYmVhdCBhbnkgc2luZ2xlIHR3byAoYnV0IG5vdCBhbnkgb3RoZXIgc2luZ2xlIGNhcmQpLiBBIHNlcXVlbmNlIG9mIHRocmVlIHBhaXJzIGNhbiBiZSBiZWF0ZW4gYnkgYSBoaWdoZXIgc2VxdWVuY2Ugb2YgdGhyZWUgcGFpcnMuXG5cbkEgc2VxdWVuY2Ugb2YgZm91ciBwYWlycyAoc3VjaCBhcyA1LTUtNi02LTctNy04LTgpIGNhbiBiZWF0IGEgcGFpciBvZiB0d29zIChidXQgbm90IGFueSBvdGhlciBwYWlyKS4gQSBzZXF1ZW5jZSBvZiBmb3VyIHBhaXJzIGNhbiBiZSBiZWF0ZW4gYnkgYSBoaWdoZXIgc2VxdWVuY2Ugb2YgZm91ciBwYWlycy5cblxuQSBzZXF1ZW5jZSBvZiBmaXZlIHBhaXJzIChzdWNoIGFzIDgtOC05LTktMTAtMTAtSi1KLVEtUSkgY2FuIGJlYXQgYSBzZXQgb2YgdGhyZWUgdHdvcyAoYnV0IG5vdCBhbnkgb3RoZXIgdGhyZWUgb2YgYSBraW5kKS4gQSBzZXF1ZW5jZSBvZiBmaXZlIHBhaXJzIGNhbiBiZSBiZWF0ZW4gYnkgYSBoaWdoZXIgc2VxdWVuY2Ugb2YgZml2ZSBwYWlycy5cblxuVGhlc2UgY29tYmluYXRpb25zIHRoYXQgY2FuIGJlYXQgc2luZ2xlIHR3b3Mgb3Igc2V0cyBvZiB0d29zIGFyZSBzb21ldGltZXMga25vd24gYXMgYm9tYnMgb3IgdHdvLWJvbWJzLCBhbmQgY2FuIGJlIHBsYXllZCBldmVuIGJ5IGEgcGxheWVyIHdobyBoYXMgcHJldmlvdXNseSBwYXNzZWQuXG5cbk5vdGUgdGhhdCB0aGVzZSBleGNlcHRpb25zIG9ubHkgYXBwbHkgdG8gYmVhdGluZyB0d29zLCBub3Qgb3RoZXIgY2FyZHMuIEZvciBleGFtcGxlLCBpZiBzb21lb25lIHBsYXlzIGFuIGFjZSB5b3UgY2Fubm90IGJlYXQgaXQgd2l0aCB5b3VyIGZvdXIgb2YgYSBraW5kLCBidXQgaWYgdGhlIGFjZSBoYXMgYmVlbiBiZWF0ZW4gYnkgYSB0d28sIHRoZW4geW91ciBmb3VyIG9mIGEga2luZCBjYW4gYmUgdXNlZCB0byBiZWF0IHRoZSB0d29gXG5cbmV4cG9ydCBkZWZhdWx0IHR1dG9yO1xuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Config.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a4c56iZ0yNPbYQ1DV9FQmk8', 'Config');
// Scripts/Config.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.language = 'vi';
    Config.defaultCoin = 100000;
    Config.botCoin = 100000;
    Config.bankrupt = 1000;
    Config.bankrupt_bonus = 500000;
    Config.minBet = 3000;
    Config.maxBet = 100000;
    Config.betValue = 3000;
    Config.totalPlayer = 6;
    Config.userphoto = null;
    Config.battle = null;
    Config.intertital_ads = '717961958974985_757265555044625';
    Config.reward_video = '316445729595977_363105338263349';
    Config.gift_reward_video = '316445729595977_363105338263349';
    Config.soundEnable = true;
    Config.dailyBonus = [
        { coin: 100000 },
        { ticket: 3 },
        { coin: 500000 },
        { coin: 1000000 },
        { coin: 5000000 },
        { ticket: 5 },
        { coin: 10000000 }
    ];
    Config.stepOfCountTime = 30000;
    return Config;
}());
exports.default = Config;
// Ngày 1 : 100K ,
// Ngày 2 : 300K , 
// ngày 3 thêm 3 lượt SPIN x tiền , 
// ngày 4 : 1M , 
// ngày 5 thêm 5 lượt Spin x tiền , 
// ngày 6 5M , 
// ngày 7 : 10M

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7SUEwQkEsQ0FBQztJQXpCaUIsZUFBUSxHQUFXLElBQUksQ0FBQztJQUNmLGtCQUFXLEdBQVcsTUFBTSxDQUFDO0lBQzdCLGNBQU8sR0FBVyxNQUFNLENBQUM7SUFDekIsZUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixxQkFBYyxHQUFXLE1BQU0sQ0FBQztJQUNoQyxhQUFNLEdBQVcsSUFBSSxDQUFDO0lBQ3RCLGFBQU0sR0FBVyxNQUFNLENBQUM7SUFDakMsZUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixrQkFBVyxHQUFXLENBQUMsQ0FBQztJQUN4QixnQkFBUyxHQUFpQixJQUFJLENBQUM7SUFDL0IsYUFBTSxHQUE0RCxJQUFJLENBQUM7SUFDOUQscUJBQWMsR0FBVyxpQ0FBaUMsQ0FBQztJQUMzRCxtQkFBWSxHQUFXLGlDQUFpQyxDQUFDO0lBQ3pELHdCQUFpQixHQUFXLGlDQUFpQyxDQUFDO0lBQ3ZFLGtCQUFXLEdBQUcsSUFBSSxDQUFDO0lBQ1YsaUJBQVUsR0FBRztRQUNoQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7UUFDaEIsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO1FBQ2IsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO1FBQ2hCLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRTtRQUNqQixFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7UUFDakIsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO1FBQ2IsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFO0tBQ3JCLENBQUM7SUFDWSxzQkFBZSxHQUFHLEtBQUssQ0FBQztJQUMxQyxhQUFDO0NBMUJELEFBMEJDLElBQUE7a0JBMUJvQixNQUFNO0FBNEIzQixrQkFBa0I7QUFDbEIsbUJBQW1CO0FBQ25CLG9DQUFvQztBQUNwQyxpQkFBaUI7QUFDakIsb0NBQW9DO0FBQ3BDLGVBQWU7QUFDZixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29uZmlnIHtcbiAgICBwdWJsaWMgc3RhdGljIGxhbmd1YWdlOiBzdHJpbmcgPSAndmknO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdENvaW46IG51bWJlciA9IDEwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IGJvdENvaW46IG51bWJlciA9IDEwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IGJhbmtydXB0OiBudW1iZXIgPSAxMDAwO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgYmFua3J1cHRfYm9udXM6IG51bWJlciA9IDUwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IG1pbkJldDogbnVtYmVyID0gMzAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IG1heEJldDogbnVtYmVyID0gMTAwMDAwO1xuICAgIHB1YmxpYyBzdGF0aWMgYmV0VmFsdWU6IG51bWJlciA9IDMwMDA7XG4gICAgcHVibGljIHN0YXRpYyB0b3RhbFBsYXllcjogbnVtYmVyID0gNjtcbiAgICBwdWJsaWMgc3RhdGljIHVzZXJwaG90bzogY2MuVGV4dHVyZTJEID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGJhdHRsZTogeyBwaG90bzogY2MuVGV4dHVyZTJELCB1c2VybmFtZTogc3RyaW5nLCBjb2luOiBudW1iZXIgfSA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyByZWFkb25seSBpbnRlcnRpdGFsX2Fkczogc3RyaW5nID0gJzcxNzk2MTk1ODk3NDk4NV83NTcyNjU1NTUwNDQ2MjUnO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgcmV3YXJkX3ZpZGVvOiBzdHJpbmcgPSAnMzE2NDQ1NzI5NTk1OTc3XzM2MzEwNTMzODI2MzM0OSc7XG4gICAgcHVibGljIHN0YXRpYyByZWFkb25seSBnaWZ0X3Jld2FyZF92aWRlbzogc3RyaW5nID0gJzMxNjQ0NTcyOTU5NTk3N18zNjMxMDUzMzgyNjMzNDknO1xuICAgIHB1YmxpYyBzdGF0aWMgc291bmRFbmFibGUgPSB0cnVlO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgZGFpbHlCb251cyA9IFtcbiAgICAgICAgeyBjb2luOiAxMDAwMDAgfSxcbiAgICAgICAgeyB0aWNrZXQ6IDMgfSxcbiAgICAgICAgeyBjb2luOiA1MDAwMDAgfSxcbiAgICAgICAgeyBjb2luOiAxMDAwMDAwIH0sXG4gICAgICAgIHsgY29pbjogNTAwMDAwMCB9LFxuICAgICAgICB7IHRpY2tldDogNSB9LFxuICAgICAgICB7IGNvaW46IDEwMDAwMDAwIH1cbiAgICBdO1xuICAgIHB1YmxpYyBzdGF0aWMgc3RlcE9mQ291bnRUaW1lID0gMzAwMDA7XG59XG5cbi8vIE5nw6B5IDEgOiAxMDBLICxcbi8vIE5nw6B5IDIgOiAzMDBLICwgXG4vLyBuZ8OgeSAzIHRow6ptIDMgbMaw4bujdCBTUElOIHggdGnhu4FuICwgXG4vLyBuZ8OgeSA0IDogMU0gLCBcbi8vIG5nw6B5IDUgdGjDqm0gNSBsxrDhu6N0IFNwaW4geCB0aeG7gW4gLCBcbi8vIG5nw6B5IDYgNU0gLCBcbi8vIG5nw6B5IDcgOiAxME0iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/CommonCard.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1351esCO5tMRYPcn390B3zj', 'CommonCard');
// Scripts/CommonCard.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonCard = /** @class */ (function () {
    function CommonCard() {
        this.totalCards = 0;
        this.cards = [];
        this.combat = [];
        this.latest = null;
    }
    CommonCard.prototype.getPosition = function () {
        if (this.cards.length == 0)
            return cc.Vec2.ZERO;
        var sx = Math.random() * 100 - 50;
        var sy = Math.random() * 100 - 50;
        return cc.v2(sx, sy + 50);
    };
    CommonCard.prototype.reset = function () {
        this.totalCards = 0;
        this.cards = [];
        this.latest = null;
        if (this.combat.length > 0) {
            this.combat = [];
        }
    };
    CommonCard.prototype.push = function (cards) {
        this.totalCards += cards.count();
        this.cards.push(cards);
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        this.latest = cards;
    };
    CommonCard.prototype.isCombatOpen = function () {
        return this.combat.length > 0;
    };
    CommonCard.prototype.pushCombat = function (player, cards) {
        this.combat.push({ player: player, cards: cards });
    };
    CommonCard.prototype.hasCombat = function () {
        if (this.combat.length < 2)
            return false;
        var peek = this.combat[this.combat.length - 1];
        if (peek.cards.highest.rank == 15)
            return false;
        return true;
    };
    CommonCard.prototype.getCombat = function () {
        return this.combat[0].cards;
    };
    CommonCard.prototype.getCombatLength = function () {
        return this.combat.length;
    };
    CommonCard.prototype.getCombatWinner = function () {
        return this.combat[this.combat.length - 1].player;
    };
    CommonCard.prototype.getCombatVictim = function () {
        return this.combat[this.combat.length - 2].player;
    };
    CommonCard.prototype.resetCombat = function () {
        this.combat = [];
    };
    CommonCard.prototype.peek = function () {
        return this.cards[this.cards.length - 1];
    };
    CommonCard.prototype.length = function () {
        return this.cards.length;
    };
    CommonCard.prototype.isEmpty = function () {
        return this.cards.length == 0;
    };
    CommonCard.prototype.nextRound = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            for (var j = this.cards[i].count() - 1; j >= 0; --j) {
                var card = this.cards[i].at(j);
                card.hide();
                card.node.setScale(0.5);
            }
        }
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        if (this.combat.length > 0) {
            this.combat = [];
        }
        this.cards = [];
        this.latest = null;
    };
    CommonCard.prototype.overlapCard = function (cards) {
        for (var i = 0; i < cards.count(); i++) {
            cards.at(i).overlap();
        }
    };
    return CommonCard;
}());
exports.default = CommonCard;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NvbW1vbkNhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRQTtJQUFBO1FBQ0ksZUFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixVQUFLLEdBQXFCLEVBQUUsQ0FBQztRQUM3QixXQUFNLEdBQW9CLEVBQUUsQ0FBQztRQUVyQixXQUFNLEdBQWMsSUFBSSxDQUFDO0lBb0dyQyxDQUFDO0lBbEdHLGdDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDdEIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUV4QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUVsQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsMEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztJQUVELHlCQUFJLEdBQUosVUFBSyxLQUFnQjtRQUNqQixJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxpQ0FBWSxHQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELCtCQUFVLEdBQVYsVUFBVyxNQUFjLEVBQUUsS0FBZ0I7UUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLFFBQUEsRUFBRSxLQUFLLE9BQUEsRUFBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUM7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUN6QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUNoRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELG9DQUFlLEdBQWY7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzlCLENBQUM7SUFFRCxvQ0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUN0RCxDQUFDO0lBRUQsb0NBQWUsR0FBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7SUFDdEQsQ0FBQztJQUVELGdDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQseUJBQUksR0FBSjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsMkJBQU0sR0FBTjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDN0IsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQzNCO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNqQztRQUNELElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELGdDQUFXLEdBQVgsVUFBWSxLQUFnQjtRQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDO0lBQ0wsaUJBQUM7QUFBRCxDQXpHQSxBQXlHQyxJQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENhcmRHcm91cCBmcm9tIFwiLi9DYXJkR3JvdXBcIjtcbmltcG9ydCBQbGF5ZXIgZnJvbSBcIi4vUGxheWVyXCI7XG5cbmludGVyZmFjZSBDYXJkUGxheSB7XG4gICAgcGxheWVyOiBQbGF5ZXI7XG4gICAgY2FyZHM6IENhcmRHcm91cDtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29tbW9uQ2FyZCB7XG4gICAgdG90YWxDYXJkczogbnVtYmVyID0gMDtcbiAgICBjYXJkczogQXJyYXk8Q2FyZEdyb3VwPiA9IFtdO1xuICAgIGNvbWJhdDogQXJyYXk8Q2FyZFBsYXk+ID0gW107XG5cbiAgICBwcml2YXRlIGxhdGVzdDogQ2FyZEdyb3VwID0gbnVsbDtcblxuICAgIGdldFBvc2l0aW9uKCk6IGNjLlZlYzIge1xuICAgICAgICBpZiAodGhpcy5jYXJkcy5sZW5ndGggPT0gMClcbiAgICAgICAgICAgIHJldHVybiBjYy5WZWMyLlpFUk87XG5cbiAgICAgICAgbGV0IHN4ID0gTWF0aC5yYW5kb20oKSAqIDEwMCAtIDUwO1xuICAgICAgICBsZXQgc3kgPSBNYXRoLnJhbmRvbSgpICogMTAwIC0gNTA7XG5cbiAgICAgICAgcmV0dXJuIGNjLnYyKHN4LCBzeSArIDUwKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgdGhpcy50b3RhbENhcmRzID0gMDtcbiAgICAgICAgdGhpcy5jYXJkcyA9IFtdO1xuICAgICAgICB0aGlzLmxhdGVzdCA9IG51bGw7XG4gICAgICAgIGlmICh0aGlzLmNvbWJhdC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLmNvbWJhdCA9IFtdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVzaChjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIHRoaXMudG90YWxDYXJkcyArPSBjYXJkcy5jb3VudCgpO1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZHMpO1xuICAgICAgICBpZiAodGhpcy5sYXRlc3QpIHtcbiAgICAgICAgICAgIHRoaXMub3ZlcmxhcENhcmQodGhpcy5sYXRlc3QpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGF0ZXN0ID0gY2FyZHM7XG4gICAgfVxuXG4gICAgaXNDb21iYXRPcGVuKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXQubGVuZ3RoID4gMDtcbiAgICB9XG5cbiAgICBwdXNoQ29tYmF0KHBsYXllcjogUGxheWVyLCBjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIHRoaXMuY29tYmF0LnB1c2goe3BsYXllciwgY2FyZHN9KTtcbiAgICB9XG5cbiAgICBoYXNDb21iYXQoKTogYm9vbGVhbiB7XG4gICAgICAgIGlmICh0aGlzLmNvbWJhdC5sZW5ndGggPCAyKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIGxldCBwZWVrID0gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMV07XG4gICAgICAgIGlmIChwZWVrLmNhcmRzLmhpZ2hlc3QucmFuayA9PSAxNSkgcmV0dXJuIGZhbHNlO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBnZXRDb21iYXQoKTogQ2FyZEdyb3VwIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29tYmF0WzBdLmNhcmRzO1xuICAgIH1cblxuICAgIGdldENvbWJhdExlbmd0aCgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXQubGVuZ3RoO1xuICAgIH1cblxuICAgIGdldENvbWJhdFdpbm5lcigpOiBQbGF5ZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMV0ucGxheWVyO1xuICAgIH1cblxuICAgIGdldENvbWJhdFZpY3RpbSgpOiBQbGF5ZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMl0ucGxheWVyO1xuICAgIH1cblxuICAgIHJlc2V0Q29tYmF0KCkge1xuICAgICAgICB0aGlzLmNvbWJhdCA9IFtdO1xuICAgIH1cblxuICAgIHBlZWsoKTogQ2FyZEdyb3VwIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHNbdGhpcy5jYXJkcy5sZW5ndGggLSAxXTtcbiAgICB9XG5cbiAgICBsZW5ndGgoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgIH1cblxuICAgIGlzRW1wdHkoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmNhcmRzLmxlbmd0aCA9PSAwO1xuICAgIH1cblxuICAgIG5leHRSb3VuZCgpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGogPSB0aGlzLmNhcmRzW2ldLmNvdW50KCkgLSAxOyBqID49IDA7IC0taikge1xuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1tpXS5hdChqKTtcbiAgICAgICAgICAgICAgICBjYXJkLmhpZGUoKTtcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUuc2V0U2NhbGUoMC41KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5sYXRlc3QpIHtcbiAgICAgICAgICAgIHRoaXMub3ZlcmxhcENhcmQodGhpcy5sYXRlc3QpO1xuICAgICAgICB9XG4gICAgICAgIGlmKHRoaXMuY29tYmF0Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuY29tYmF0ID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jYXJkcyA9IFtdO1xuICAgICAgICB0aGlzLmxhdGVzdCA9IG51bGw7XG4gICAgfVxuXG4gICAgb3ZlcmxhcENhcmQoY2FyZHM6IENhcmRHcm91cCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNhcmRzLmNvdW50KCk7IGkrKykge1xuICAgICAgICAgICAgY2FyZHMuYXQoaSkub3ZlcmxhcCgpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/migration/use_reversed_rotateBy.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dd02fL4CqJITr6/oDI+73WZ', 'use_reversed_rotateBy');
// migration/use_reversed_rotateBy.js

"use strict";

/*
 * This script is automatically generated by Cocos Creator and is only used for projects compatible with v2.1.0/v2.1.1/v2.3.0/v2.3.1/v2.3.2 versions.
 * You do not need to manually add this script in any other project.
 * If you don't use cc.Action in your project, you can delete this script directly.
 * If your project is hosted in VCS such as git, submit this script together.
 *
 * 此脚本由 Cocos Creator 自动生成，仅用于兼容 v2.1.0/v2.1.1/v2.3.0/v2.3.1/v2.3.2 版本的工程，
 * 你无需在任何其它项目中手动添加此脚本。
 * 如果你的项目中没用到 Action，可直接删除该脚本。
 * 如果你的项目有托管于 git 等版本库，请将此脚本一并上传。
 */
cc.RotateBy._reverse = true;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9taWdyYXRpb24vdXNlX3JldmVyc2VkX3JvdGF0ZUJ5LmpzIl0sIm5hbWVzIjpbImNjIiwiUm90YXRlQnkiLCJfcmV2ZXJzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsUUFBSCxDQUFZQyxRQUFaLEdBQXVCLElBQXZCIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogVGhpcyBzY3JpcHQgaXMgYXV0b21hdGljYWxseSBnZW5lcmF0ZWQgYnkgQ29jb3MgQ3JlYXRvciBhbmQgaXMgb25seSB1c2VkIGZvciBwcm9qZWN0cyBjb21wYXRpYmxlIHdpdGggdjIuMS4wL3YyLjEuMS92Mi4zLjAvdjIuMy4xL3YyLjMuMiB2ZXJzaW9ucy5cbiAqIFlvdSBkbyBub3QgbmVlZCB0byBtYW51YWxseSBhZGQgdGhpcyBzY3JpcHQgaW4gYW55IG90aGVyIHByb2plY3QuXG4gKiBJZiB5b3UgZG9uJ3QgdXNlIGNjLkFjdGlvbiBpbiB5b3VyIHByb2plY3QsIHlvdSBjYW4gZGVsZXRlIHRoaXMgc2NyaXB0IGRpcmVjdGx5LlxuICogSWYgeW91ciBwcm9qZWN0IGlzIGhvc3RlZCBpbiBWQ1Mgc3VjaCBhcyBnaXQsIHN1Ym1pdCB0aGlzIHNjcmlwdCB0b2dldGhlci5cbiAqXG4gKiDmraTohJrmnKznlLEgQ29jb3MgQ3JlYXRvciDoh6rliqjnlJ/miJDvvIzku4XnlKjkuo7lhbzlrrkgdjIuMS4wL3YyLjEuMS92Mi4zLjAvdjIuMy4xL3YyLjMuMiDniYjmnKznmoTlt6XnqIvvvIxcbiAqIOS9oOaXoOmcgOWcqOS7u+S9leWFtuWug+mhueebruS4reaJi+WKqOa3u+WKoOatpOiEmuacrOOAglxuICog5aaC5p6c5L2g55qE6aG555uu5Lit5rKh55So5YiwIEFjdGlvbu+8jOWPr+ebtOaOpeWIoOmZpOivpeiEmuacrOOAglxuICog5aaC5p6c5L2g55qE6aG555uu5pyJ5omY566h5LqOIGdpdCDnrYnniYjmnKzlupPvvIzor7flsIbmraTohJrmnKzkuIDlubbkuIrkvKDjgIJcbiAqL1xuXG5jYy5Sb3RhdGVCeS5fcmV2ZXJzZSA9IHRydWU7XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/AutoHide.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1ab56S/omxH+qPXcysDsNwA', 'AutoHide');
// Scripts/AutoHide.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AutoHide = /** @class */ (function (_super) {
    __extends(AutoHide, _super);
    function AutoHide() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.duration = 3;
        _this.elapsed = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    AutoHide.prototype.start = function () {
    };
    AutoHide.prototype.update = function (dt) {
        this.elapsed += dt;
        if (this.elapsed >= this.duration) {
            this.node.active = false;
        }
    };
    AutoHide.prototype.show = function (sender) {
        this.label.string = 'TÍnh năng sắp ra mắt...';
        this.elapsed = 0;
        this.node.active = true;
    };
    AutoHide.prototype.openWithText = function (sender, string) {
        this.label.string = string;
        this.elapsed = 0;
        this.node.active = true;
    };
    __decorate([
        property(cc.Label)
    ], AutoHide.prototype, "label", void 0);
    __decorate([
        property
    ], AutoHide.prototype, "duration", void 0);
    AutoHide = __decorate([
        ccclass
    ], AutoHide);
    return AutoHide;
}(cc.Component));
exports.default = AutoHide;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0F1dG9IaWRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBaUNDO1FBL0JHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFFdkIsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUNyQixhQUFPLEdBQVcsQ0FBQyxDQUFDOztJQTRCeEIsQ0FBQztJQTFCRyx3QkFBd0I7SUFFeEIsZUFBZTtJQUVmLHdCQUFLLEdBQUw7SUFFQSxDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFRLEVBQUU7UUFDTixJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDNUI7SUFDTCxDQUFDO0lBRUQsdUJBQUksR0FBSixVQUFLLE1BQWU7UUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCwrQkFBWSxHQUFaLFVBQWEsTUFBZSxFQUFFLE1BQWM7UUFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBOUJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7MkNBQ0k7SUFFdkI7UUFEQyxRQUFROzhDQUNZO0lBSkosUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWlDNUI7SUFBRCxlQUFDO0NBakNELEFBaUNDLENBakNxQyxFQUFFLENBQUMsU0FBUyxHQWlDakQ7a0JBakNvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQXV0b0hpZGUgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBsYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eVxuICAgIGR1cmF0aW9uOiBudW1iZXIgPSAzO1xuICAgIGVsYXBzZWQ6IG51bWJlciA9IDA7XG5cbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQgKCkge1xuXG4gICAgfVxuXG4gICAgdXBkYXRlIChkdCkge1xuICAgICAgICB0aGlzLmVsYXBzZWQgKz0gZHQ7XG4gICAgICAgIGlmICh0aGlzLmVsYXBzZWQgPj0gdGhpcy5kdXJhdGlvbikge1xuICAgICAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2hvdyhzZW5kZXI6IGNjLk5vZGUpIHtcbiAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSAnVMONbmggbsSDbmcgc+G6r3AgcmEgbeG6r3QuLi4nO1xuICAgICAgICB0aGlzLmVsYXBzZWQgPSAwO1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBvcGVuV2l0aFRleHQoc2VuZGVyOiBjYy5Ob2RlLCBzdHJpbmc6IHN0cmluZykge1xuICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9IHN0cmluZztcbiAgICAgICAgdGhpcy5lbGFwc2VkID0gMDtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/CardGroup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bece1QhfllOVa5CfNF0TmNb', 'CardGroup');
// Scripts/CardGroup.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup = /** @class */ (function () {
    function CardGroup(cards, kind) {
        this.cards = cards;
        this.kind = CardGroup.Single;
        this.kind = kind;
        if (kind != 0) {
            this.highest = Helper_1.default.findMaxCard(cards);
        }
    }
    CardGroup.prototype.getPosition = function () {
        if (this.cards.length % 2 != 0) {
            var idx = (this.cards.length - 1) / 2;
            return this.cards[idx].node.getPosition();
        }
        var middle = this.cards.length / 2;
        var c1 = this.cards[middle].node.getPosition();
        var c2 = this.cards[middle - 1].node.getPosition();
        return cc.v2((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
    };
    CardGroup.prototype.gt = function (o) {
        if (this.kind == o.kind && this.count() == o.count())
            return this.highest.gt(o.highest);
        if (o.highest.rank == 15 && o.count() == 1) {
            if (this.kind == CardGroup.MultiPair)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.highest.rank == 15 && o.count() == 2) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.kind == CardGroup.MultiPair && o.count() == 6) {
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
        }
        if (o.kind == CardGroup.House && o.count() == 4) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            else
                return false;
        }
        return false;
    };
    CardGroup.prototype.at = function (i) {
        return this.cards[i];
    };
    CardGroup.prototype.count = function () {
        return this.cards.length;
    };
    CardGroup.prototype.push = function (card) {
        this.cards.push(card);
    };
    CardGroup.prototype.remove = function (card) {
        var index = this.cards.indexOf(card, 0);
        this.cards.splice(index, 1);
    };
    CardGroup.prototype.contains = function (card) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            if (this.cards[i] == card)
                return true;
        }
        return false;
    };
    CardGroup.prototype.calculate = function () {
        if (this.cards.length == 1) {
            this.kind = CardGroup.Single;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isHouse(this.cards)) {
            this.kind = CardGroup.House;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isStraight(this.cards)) {
            this.kind = CardGroup.Straight;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isMultiPair(this.cards)) {
            this.kind = CardGroup.MultiPair;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else {
            this.kind = CardGroup.Ungrouped;
        }
    };
    CardGroup.prototype.sort = function () {
        Helper_1.default.sort(this.cards);
    };
    CardGroup.prototype.isInvalid = function () {
        return this.kind == CardGroup.Ungrouped;
    };
    CardGroup.prototype.dump = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            console.log(this.cards[i].toString());
        }
    };
    CardGroup.Ungrouped = 0;
    CardGroup.Single = 1;
    CardGroup.House = 2;
    CardGroup.Straight = 3;
    CardGroup.Boss = 4;
    CardGroup.MultiPair = 5;
    return CardGroup;
}());
exports.default = CardGroup;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NhcmRHcm91cC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1DQUE4QjtBQUU5QjtJQUtJLG1CQUFZLEtBQWtCLEVBQUUsSUFBWTtRQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLE9BQU8sR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM1QztJQUNMLENBQUM7SUFFRCwrQkFBVyxHQUFYO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzVCLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0M7UUFFRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDbkMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0MsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25ELE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxzQkFBRSxHQUFGLFVBQUcsQ0FBWTtRQUNYLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQ2hELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXRDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxTQUFTO2dCQUNoQyxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDakQsT0FBTyxJQUFJLENBQUM7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ3hDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNyRCxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDakQsT0FBTyxJQUFJLENBQUM7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNqRCxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDckQsT0FBTyxJQUFJLENBQUM7U0FDbkI7UUFFRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQzdDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNyRCxPQUFPLElBQUksQ0FBQzs7Z0JBRVosT0FBTyxLQUFLLENBQUM7U0FDcEI7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsc0JBQUUsR0FBRixVQUFHLENBQVM7UUFDUixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzdCLENBQUM7SUFFRCx3QkFBSSxHQUFKLFVBQUssSUFBVTtRQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQkFBTSxHQUFOLFVBQU8sSUFBVTtRQUNiLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELDRCQUFRLEdBQVIsVUFBUyxJQUFVO1FBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSTtnQkFBRSxPQUFPLElBQUksQ0FBQztTQUMxQztRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDbkM7SUFDTCxDQUFDO0lBRUQsd0JBQUksR0FBSjtRQUNJLGdCQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxDQUFDO0lBQzVDLENBQUM7SUFFRCx3QkFBSSxHQUFKO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFZSxtQkFBUyxHQUFXLENBQUMsQ0FBQztJQUN0QixnQkFBTSxHQUFXLENBQUMsQ0FBQztJQUNuQixlQUFLLEdBQVcsQ0FBQyxDQUFDO0lBQ2xCLGtCQUFRLEdBQVcsQ0FBQyxDQUFDO0lBQ3JCLGNBQUksR0FBVyxDQUFDLENBQUM7SUFDakIsbUJBQVMsR0FBVyxDQUFDLENBQUM7SUFDMUMsZ0JBQUM7Q0E3SEQsQUE2SEMsSUFBQTtrQkE3SG9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2FyZCBmcm9tIFwiLi9DYXJkXCI7XG5pbXBvcnQgSGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXJkR3JvdXAge1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPjtcbiAgICBoaWdoZXN0OiBDYXJkO1xuICAgIGtpbmQ6IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNhcmRzOiBBcnJheTxDYXJkPiwga2luZDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuY2FyZHMgPSBjYXJkcztcbiAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLlNpbmdsZTtcbiAgICAgICAgdGhpcy5raW5kID0ga2luZDtcbiAgICAgICAgaWYgKGtpbmQgIT0gMCkge1xuICAgICAgICAgICAgdGhpcy5oaWdoZXN0ID0gSGVscGVyLmZpbmRNYXhDYXJkKGNhcmRzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldFBvc2l0aW9uKCk6IGNjLlZlYzIge1xuICAgICAgICBpZiAodGhpcy5jYXJkcy5sZW5ndGggJSAyICE9IDApIHtcbiAgICAgICAgICAgIGxldCBpZHggPSAodGhpcy5jYXJkcy5sZW5ndGggLSAxKSAvIDI7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYXJkc1tpZHhdLm5vZGUuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBtaWRkbGUgPSB0aGlzLmNhcmRzLmxlbmd0aCAvIDI7XG4gICAgICAgIGxldCBjMSA9IHRoaXMuY2FyZHNbbWlkZGxlXS5ub2RlLmdldFBvc2l0aW9uKCk7XG4gICAgICAgIGxldCBjMiA9IHRoaXMuY2FyZHNbbWlkZGxlIC0gMV0ubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgICAgICByZXR1cm4gY2MudjIoKGMxLnggKyBjMi54KSAvIDIsIChjMS55ICsgYzIueSkgLyAyKTtcbiAgICB9XG5cbiAgICBndChvOiBDYXJkR3JvdXApOiBib29sZWFuIHtcbiAgICAgICAgaWYgKHRoaXMua2luZCA9PSBvLmtpbmQgJiYgdGhpcy5jb3VudCgpID09IG8uY291bnQoKSlcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmhpZ2hlc3QuZ3Qoby5oaWdoZXN0KTtcblxuICAgICAgICBpZiAoby5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgby5jb3VudCgpID09IDEpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpcilcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIGlmICh0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLkhvdXNlICYmIHRoaXMuY291bnQoKSA9PSA0KVxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG8uaGlnaGVzdC5yYW5rID09IDE1ICYmIG8uY291bnQoKSA9PSAyKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5NdWx0aVBhaXIgJiYgdGhpcy5jb3VudCgpID09IDgpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiB0aGlzLmNvdW50KCkgPT0gNClcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpciAmJiBvLmNvdW50KCkgPT0gNikge1xuICAgICAgICAgICAgaWYgKHRoaXMua2luZCA9PSBDYXJkR3JvdXAuSG91c2UgJiYgdGhpcy5jb3VudCgpID09IDQpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5NdWx0aVBhaXIgJiYgdGhpcy5jb3VudCgpID09IDgpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoby5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiBvLmNvdW50KCkgPT0gNCkge1xuICAgICAgICAgICAgaWYgKHRoaXMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIHRoaXMuY291bnQoKSA9PSA4KVxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBhdChpOiBudW1iZXIpOiBDYXJkIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHNbaV07XG4gICAgfVxuXG4gICAgY291bnQoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgIH1cblxuICAgIHB1c2goY2FyZDogQ2FyZCkge1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZCk7XG4gICAgfVxuXG4gICAgcmVtb3ZlKGNhcmQ6IENhcmQpIHtcbiAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5jYXJkcy5pbmRleE9mKGNhcmQsIDApO1xuICAgICAgICB0aGlzLmNhcmRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgfVxuXG4gICAgY29udGFpbnMoY2FyZDogQ2FyZCk6IGJvb2xlYW4ge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuY2FyZHNbaV0gPT0gY2FyZCkgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGNhbGN1bGF0ZSgpIHtcbiAgICAgICAgaWYgKHRoaXMuY2FyZHMubGVuZ3RoID09IDEpIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5TaW5nbGU7XG4gICAgICAgICAgICB0aGlzLmhpZ2hlc3QgPSBIZWxwZXIuZmluZE1heENhcmQodGhpcy5jYXJkcyk7XG4gICAgICAgIH0gZWxzZSBpZiAoSGVscGVyLmlzSG91c2UodGhpcy5jYXJkcykpIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5Ib3VzZTtcbiAgICAgICAgICAgIHRoaXMuaGlnaGVzdCA9IEhlbHBlci5maW5kTWF4Q2FyZCh0aGlzLmNhcmRzKTtcbiAgICAgICAgfSBlbHNlIGlmIChIZWxwZXIuaXNTdHJhaWdodCh0aGlzLmNhcmRzKSkge1xuICAgICAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLlN0cmFpZ2h0O1xuICAgICAgICAgICAgdGhpcy5oaWdoZXN0ID0gSGVscGVyLmZpbmRNYXhDYXJkKHRoaXMuY2FyZHMpO1xuICAgICAgICB9IGVsc2UgaWYgKEhlbHBlci5pc011bHRpUGFpcih0aGlzLmNhcmRzKSkge1xuICAgICAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLk11bHRpUGFpcjtcbiAgICAgICAgICAgIHRoaXMuaGlnaGVzdCA9IEhlbHBlci5maW5kTWF4Q2FyZCh0aGlzLmNhcmRzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5Vbmdyb3VwZWQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzb3J0KCkge1xuICAgICAgICBIZWxwZXIuc29ydCh0aGlzLmNhcmRzKTtcbiAgICB9XG5cbiAgICBpc0ludmFsaWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLlVuZ3JvdXBlZDtcbiAgICB9XG5cbiAgICBkdW1wKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5jYXJkc1tpXS50b1N0cmluZygpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHN0YXRpYyByZWFkb25seSBVbmdyb3VwZWQ6IG51bWJlciA9IDA7XG4gICAgc3RhdGljIHJlYWRvbmx5IFNpbmdsZTogbnVtYmVyID0gMTtcbiAgICBzdGF0aWMgcmVhZG9ubHkgSG91c2U6IG51bWJlciA9IDI7XG4gICAgc3RhdGljIHJlYWRvbmx5IFN0cmFpZ2h0OiBudW1iZXIgPSAzO1xuICAgIHN0YXRpYyByZWFkb25seSBCb3NzOiBudW1iZXIgPSA0O1xuICAgIHN0YXRpYyByZWFkb25seSBNdWx0aVBhaXI6IG51bWJlciA9IDU7XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Card.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5e609fdf41BH6QiJ7pgTFU1', 'Card');
// Scripts/Card.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Card = /** @class */ (function (_super) {
    __extends(Card, _super);
    function Card() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.back = null;
        _this.front = null;
        _this.rank = 1; // A: 1, J: 11, Q: 12, K: 13
        _this.suit = 1; // 1: spade, 2: club, 3: diamond, 4: heart
        _this.overlayer = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Card.prototype.onLoad = function () {
        this.front = this.getComponent(cc.Sprite).spriteFrame;
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.overlayer = this.node.children[this.node.children.length - 1];
    };
    Card.prototype.start = function () {
        this.overlayer.active = false;
    };
    Card.prototype.reset = function () {
        this.node.active = true;
        this.node.setScale(1);
        this.overlayer.active = false;
        this.node.setPosition(cc.Vec2.ZERO);
        this.hide();
    };
    Card.prototype.show = function () {
        this.getComponent(cc.Sprite).spriteFrame = this.front;
        this.setChildrenActive(true);
    };
    Card.prototype.hide = function () {
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.setChildrenActive(false);
    };
    Card.prototype.setChildrenActive = function (active) {
        for (var i = 0; i < this.node.children.length - 1; i++) {
            this.node.children[i].active = active;
        }
    };
    Card.prototype.overlap = function () {
        this.overlayer.active = true;
    };
    Card.prototype.setPositionY = function (newPosY) {
        this.node.setPosition(this.node.position.x, newPosY);
    };
    Card.prototype.compare = function (o) {
        if (o == null)
            return 1;
        if (this.rank > o.rank)
            return 1;
        if (this.rank < o.rank)
            return -1;
        if (this.suit > o.suit)
            return 1;
        if (this.suit < o.suit)
            return -1;
        return 0;
    };
    Card.prototype.gt = function (o) {
        return this.compare(o) > 0;
    };
    Card.prototype.lt = function (o) {
        return this.compare(o) < 0;
    };
    // update (dt) {}
    Card.prototype.toString = function () {
        return 'Rank: ' + this.rank + ' Suit: ' + this.suit;
    };
    __decorate([
        property(cc.SpriteFrame)
    ], Card.prototype, "back", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Card.prototype, "front", void 0);
    Card = __decorate([
        ccclass
    ], Card);
    return Card;
}(cc.Component));
exports.default = Card;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQixrRkFBa0Y7QUFDbEYseUZBQXlGO0FBQ3pGLG1CQUFtQjtBQUNuQiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBQ25HLDhCQUE4QjtBQUM5Qiw0RkFBNEY7QUFDNUYsbUdBQW1HOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFN0YsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBa0Msd0JBQVk7SUFBOUM7UUFBQSxxRUEwRUM7UUF4RUcsVUFBSSxHQUFtQixJQUFJLENBQUM7UUFFNUIsV0FBSyxHQUFtQixJQUFJLENBQUM7UUFDN0IsVUFBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLDRCQUE0QjtRQUM5QyxVQUFJLEdBQVcsQ0FBQyxDQUFDLENBQUMsMENBQTBDO1FBQzVELGVBQVMsR0FBWSxJQUFJLENBQUM7O0lBbUU5QixDQUFDO0lBbEVHLHdCQUF3QjtJQUV4QixxQkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUM7UUFDdEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELG9CQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUVELG9CQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxtQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxtQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFTyxnQ0FBaUIsR0FBekIsVUFBMEIsTUFBZTtRQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1NBQ3pDO0lBQ0wsQ0FBQztJQUVELHNCQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQUVELDJCQUFZLEdBQVosVUFBYSxPQUFlO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsc0JBQU8sR0FBUCxVQUFRLENBQU87UUFDWCxJQUFJLENBQUMsSUFBSSxJQUFJO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEIsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDakMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJO1lBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUNsQyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUk7WUFBRSxPQUFPLENBQUMsQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUk7WUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELGlCQUFFLEdBQUYsVUFBRyxDQUFPO1FBQ04sT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsaUJBQUUsR0FBRixVQUFHLENBQU87UUFDTixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxpQkFBaUI7SUFFakIsdUJBQVEsR0FBUjtRQUNJLE9BQU8sUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDeEQsQ0FBQztJQXZFRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3NDQUNHO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7dUNBQ0k7SUFKWixJQUFJO1FBRHhCLE9BQU87T0FDYSxJQUFJLENBMEV4QjtJQUFELFdBQUM7Q0ExRUQsQUEwRUMsQ0ExRWlDLEVBQUUsQ0FBQyxTQUFTLEdBMEU3QztrQkExRW9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FyZCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxuICAgIGJhY2s6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXG4gICAgZnJvbnQ6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcbiAgICByYW5rOiBudW1iZXIgPSAxOyAvLyBBOiAxLCBKOiAxMSwgUTogMTIsIEs6IDEzXG4gICAgc3VpdDogbnVtYmVyID0gMTsgLy8gMTogc3BhZGUsIDI6IGNsdWIsIDM6IGRpYW1vbmQsIDQ6IGhlYXJ0XG4gICAgb3ZlcmxheWVyOiBjYy5Ob2RlID0gbnVsbDtcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy5mcm9udCA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWU7XG4gICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmJhY2s7XG4gICAgICAgIHRoaXMub3ZlcmxheWVyID0gdGhpcy5ub2RlLmNoaWxkcmVuW3RoaXMubm9kZS5jaGlsZHJlbi5sZW5ndGggLSAxXTtcbiAgICB9XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy5vdmVybGF5ZXIuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcmVzZXQoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLm5vZGUuc2V0U2NhbGUoMSk7XG4gICAgICAgIHRoaXMub3ZlcmxheWVyLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLm5vZGUuc2V0UG9zaXRpb24oY2MuVmVjMi5aRVJPKTtcbiAgICAgICAgdGhpcy5oaWRlKCk7XG4gICAgfVxuXG4gICAgc2hvdygpIHtcbiAgICAgICAgdGhpcy5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5zcHJpdGVGcmFtZSA9IHRoaXMuZnJvbnQ7XG4gICAgICAgIHRoaXMuc2V0Q2hpbGRyZW5BY3RpdmUodHJ1ZSk7XG4gICAgfVxuXG4gICAgaGlkZSgpIHtcbiAgICAgICAgdGhpcy5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5zcHJpdGVGcmFtZSA9IHRoaXMuYmFjaztcbiAgICAgICAgdGhpcy5zZXRDaGlsZHJlbkFjdGl2ZShmYWxzZSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRDaGlsZHJlbkFjdGl2ZShhY3RpdmU6IGJvb2xlYW4pIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm5vZGUuY2hpbGRyZW4ubGVuZ3RoIC0gMTsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUuY2hpbGRyZW5baV0uYWN0aXZlID0gYWN0aXZlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb3ZlcmxhcCgpIHtcbiAgICAgICAgdGhpcy5vdmVybGF5ZXIuYWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzZXRQb3NpdGlvblkobmV3UG9zWTogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIHRoaXMubm9kZS5zZXRQb3NpdGlvbih0aGlzLm5vZGUucG9zaXRpb24ueCwgbmV3UG9zWSk7XG4gICAgfVxuXG4gICAgY29tcGFyZShvOiBDYXJkKSB7XG4gICAgICAgIGlmIChvID09IG51bGwpIHJldHVybiAxO1xuICAgICAgICBpZiAodGhpcy5yYW5rID4gby5yYW5rKSByZXR1cm4gMTtcbiAgICAgICAgaWYgKHRoaXMucmFuayA8IG8ucmFuaykgcmV0dXJuIC0xO1xuICAgICAgICBpZiAodGhpcy5zdWl0ID4gby5zdWl0KSByZXR1cm4gMTtcbiAgICAgICAgaWYgKHRoaXMuc3VpdCA8IG8uc3VpdCkgcmV0dXJuIC0xO1xuICAgICAgICByZXR1cm4gMDtcbiAgICB9XG5cbiAgICBndChvOiBDYXJkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbXBhcmUobykgPiAwO1xuICAgIH1cblxuICAgIGx0KG86IENhcmQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29tcGFyZShvKSA8IDA7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHRvU3RyaW5nKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiAnUmFuazogJyArIHRoaXMucmFuayArICcgU3VpdDogJyArIHRoaXMuc3VpdDtcbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Deck.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cce96i6QBJMl7PTDBXefIeo', 'Deck');
// Scripts/Deck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Card_1 = require("./Card");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Deck = /** @class */ (function (_super) {
    __extends(Deck, _super);
    function Deck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cards = Array();
        _this.offset = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Deck.prototype.onLoad = function () {
        for (var i = 0; i < 52; i++) {
            var card = this.node.children[i].getComponent(Card_1.default);
            var cardName = card.node.name;
            card.rank = this.getRankFromName(cardName);
            card.suit = this.getSuitFromName(cardName);
            this.cards.push(card);
        }
    };
    // start() { }
    // update (dt) {}
    Deck.prototype.shuffle = function () {
        var length = this.cards.length;
        var lastIndex = length - 1;
        var index = -1;
        while (++index < length) {
            var rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
            var value = this.cards[rand];
            this.cards[rand] = this.cards[index];
            this.cards[index] = value;
        }
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            card.reset();
        }
        this.offset = 0;
        // this.cheat();
    };
    // cheat() {
    //     let pos = 0;
    //     for (let i = this.cards.length - 1; i >= 0; --i) {
    //         let card = this.cards[i];
    //         if (card.rank == 9) {
    //             let tmp = this.cards[pos];
    //             this.cards[pos] = card;
    //             this.cards[i] = tmp;
    //             pos += 4;
    //         }
    //     }
    // }
    Deck.prototype.pick = function () {
        var card = this.cards[this.offset];
        ++this.offset;
        return card;
    };
    Deck.prototype.hide = function () {
        for (var i = this.offset; i < this.cards.length; i++) {
            this.cards[i].node.active = false;
        }
    };
    Deck.prototype.getRankFromName = function (cardName) {
        if (cardName.length == 3)
            return 10;
        switch (cardName[0]) {
            case 'J': return 11;
            case 'Q': return 12;
            case 'K': return 13;
            case 'A': return 1;
            // case '2': return 15;
            default: return parseInt(cardName[0]);
        }
    };
    Deck.prototype.getSuitFromName = function (cardName) {
        switch (cardName[cardName.length - 1]) {
            case 'S': return 1;
            case 'C': return 2;
            case 'D': return 3;
            case 'H': return 4;
            default: throw new Error(cardName);
        }
    };
    Deck = __decorate([
        ccclass
    ], Deck);
    return Deck;
}(cc.Component));
exports.default = Deck;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0RlY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0JBQTBCO0FBRTFCLG9CQUFvQjtBQUNwQixrRkFBa0Y7QUFDbEYseUZBQXlGO0FBQ3pGLG1CQUFtQjtBQUNuQiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBQ25HLDhCQUE4QjtBQUM5Qiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBRTdGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWtDLHdCQUFZO0lBQTlDO1FBQUEscUVBb0ZDO1FBbkZHLFdBQUssR0FBZ0IsS0FBSyxFQUFFLENBQUM7UUFDN0IsWUFBTSxHQUFXLENBQUMsQ0FBQzs7SUFrRnZCLENBQUM7SUFqRkcsd0JBQXdCO0lBRXhCLHFCQUFNLEdBQU47UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxjQUFJLENBQUMsQ0FBQztZQUNwRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQztJQUVELGNBQWM7SUFFZCxpQkFBaUI7SUFFakIsc0JBQU8sR0FBUDtRQUNJLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFBO1FBQ2hDLElBQU0sU0FBUyxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUE7UUFDNUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDZCxPQUFPLEVBQUUsS0FBSyxHQUFHLE1BQU0sRUFBRTtZQUNyQixJQUFNLElBQUksR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxTQUFTLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDeEUsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUE7U0FDNUI7UUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQzdDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsZ0JBQWdCO0lBQ3BCLENBQUM7SUFFRCxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLHlEQUF5RDtJQUN6RCxvQ0FBb0M7SUFDcEMsZ0NBQWdDO0lBQ2hDLHlDQUF5QztJQUN6QyxzQ0FBc0M7SUFDdEMsbUNBQW1DO0lBQ25DLHdCQUF3QjtJQUN4QixZQUFZO0lBQ1osUUFBUTtJQUNSLElBQUk7SUFFSixtQkFBSSxHQUFKO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELG1CQUFJLEdBQUo7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBRU8sOEJBQWUsR0FBdkIsVUFBd0IsUUFBZ0I7UUFDcEMsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDcEIsT0FBTyxFQUFFLENBQUM7UUFDZCxRQUFRLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNqQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDcEIsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNwQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2QixPQUFPLENBQUMsQ0FBQyxPQUFPLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFTyw4QkFBZSxHQUF2QixVQUF3QixRQUFnQjtRQUNwQyxRQUFRLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ25DLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkIsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25CLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN0QztJQUNMLENBQUM7SUFuRmdCLElBQUk7UUFEeEIsT0FBTztPQUNhLElBQUksQ0FvRnhCO0lBQUQsV0FBQztDQXBGRCxBQW9GQyxDQXBGaUMsRUFBRSxDQUFDLFNBQVMsR0FvRjdDO2tCQXBGb0IsSUFBSSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlY2sgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPiA9IEFycmF5KCk7XG4gICAgb2Zmc2V0OiBudW1iZXIgPSAwO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDUyOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5ub2RlLmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChDYXJkKTtcbiAgICAgICAgICAgIGxldCBjYXJkTmFtZSA9IGNhcmQubm9kZS5uYW1lO1xuICAgICAgICAgICAgY2FyZC5yYW5rID0gdGhpcy5nZXRSYW5rRnJvbU5hbWUoY2FyZE5hbWUpO1xuICAgICAgICAgICAgY2FyZC5zdWl0ID0gdGhpcy5nZXRTdWl0RnJvbU5hbWUoY2FyZE5hbWUpO1xuICAgICAgICAgICAgdGhpcy5jYXJkcy5wdXNoKGNhcmQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gc3RhcnQoKSB7IH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaHVmZmxlKCk6IHZvaWQge1xuICAgICAgICBjb25zdCBsZW5ndGggPSB0aGlzLmNhcmRzLmxlbmd0aFxuICAgICAgICBjb25zdCBsYXN0SW5kZXggPSBsZW5ndGggLSAxXG4gICAgICAgIGxldCBpbmRleCA9IC0xXG4gICAgICAgIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgICAgICAgICBjb25zdCByYW5kID0gaW5kZXggKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobGFzdEluZGV4IC0gaW5kZXggKyAxKSlcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5jYXJkc1tyYW5kXVxuICAgICAgICAgICAgdGhpcy5jYXJkc1tyYW5kXSA9IHRoaXMuY2FyZHNbaW5kZXhdXG4gICAgICAgICAgICB0aGlzLmNhcmRzW2luZGV4XSA9IHZhbHVlXG4gICAgICAgIH1cbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1tpXTtcbiAgICAgICAgICAgIGNhcmQucmVzZXQoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLm9mZnNldCA9IDA7XG4gICAgICAgIC8vIHRoaXMuY2hlYXQoKTtcbiAgICB9XG5cbiAgICAvLyBjaGVhdCgpIHtcbiAgICAvLyAgICAgbGV0IHBvcyA9IDA7XG4gICAgLy8gICAgIGZvciAobGV0IGkgPSB0aGlzLmNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgLy8gICAgICAgICBsZXQgY2FyZCA9IHRoaXMuY2FyZHNbaV07XG4gICAgLy8gICAgICAgICBpZiAoY2FyZC5yYW5rID09IDkpIHtcbiAgICAvLyAgICAgICAgICAgICBsZXQgdG1wID0gdGhpcy5jYXJkc1twb3NdO1xuICAgIC8vICAgICAgICAgICAgIHRoaXMuY2FyZHNbcG9zXSA9IGNhcmQ7XG4gICAgLy8gICAgICAgICAgICAgdGhpcy5jYXJkc1tpXSA9IHRtcDtcbiAgICAvLyAgICAgICAgICAgICBwb3MgKz0gNDtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgfVxuICAgIC8vIH1cblxuICAgIHBpY2soKTogQ2FyZCB7XG4gICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1t0aGlzLm9mZnNldF07XG4gICAgICAgICsrdGhpcy5vZmZzZXQ7XG4gICAgICAgIHJldHVybiBjYXJkO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLm9mZnNldDsgaSA8IHRoaXMuY2FyZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMuY2FyZHNbaV0ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0UmFua0Zyb21OYW1lKGNhcmROYW1lOiBzdHJpbmcpOiBudW1iZXIge1xuICAgICAgICBpZiAoY2FyZE5hbWUubGVuZ3RoID09IDMpXG4gICAgICAgICAgICByZXR1cm4gMTA7XG4gICAgICAgIHN3aXRjaCAoY2FyZE5hbWVbMF0pIHtcbiAgICAgICAgICAgIGNhc2UgJ0onOiByZXR1cm4gMTE7XG4gICAgICAgICAgICBjYXNlICdRJzogcmV0dXJuIDEyO1xuICAgICAgICAgICAgY2FzZSAnSyc6IHJldHVybiAxMztcbiAgICAgICAgICAgIGNhc2UgJ0EnOiByZXR1cm4gMTtcbiAgICAgICAgICAgIC8vIGNhc2UgJzInOiByZXR1cm4gMTU7XG4gICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gcGFyc2VJbnQoY2FyZE5hbWVbMF0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRTdWl0RnJvbU5hbWUoY2FyZE5hbWU6IHN0cmluZykge1xuICAgICAgICBzd2l0Y2ggKGNhcmROYW1lW2NhcmROYW1lLmxlbmd0aCAtIDFdKSB7XG4gICAgICAgICAgICBjYXNlICdTJzogcmV0dXJuIDE7XG4gICAgICAgICAgICBjYXNlICdDJzogcmV0dXJuIDI7XG4gICAgICAgICAgICBjYXNlICdEJzogcmV0dXJuIDM7XG4gICAgICAgICAgICBjYXNlICdIJzogcmV0dXJuIDQ7XG4gICAgICAgICAgICBkZWZhdWx0OiB0aHJvdyBuZXcgRXJyb3IoY2FyZE5hbWUpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/HandCard.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3f5f4vN8GVIHYlDzS8EcLN6', 'HandCard');
// Scripts/HandCard.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Card_1 = require("./Card");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var HandCard = /** @class */ (function (_super) {
    __extends(HandCard, _super);
    function HandCard() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cards = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    HandCard.prototype.start = function () {
    };
    __decorate([
        property(Card_1.default)
    ], HandCard.prototype, "cards", void 0);
    HandCard = __decorate([
        ccclass
    ], HandCard);
    return HandCard;
}(cc.Component));
exports.default = HandCard;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0hhbmRDYXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLCtCQUEwQjtBQUUxQixvQkFBb0I7QUFDcEIsa0ZBQWtGO0FBQ2xGLHlGQUF5RjtBQUN6RixtQkFBbUI7QUFDbkIsNEZBQTRGO0FBQzVGLG1HQUFtRztBQUNuRyw4QkFBOEI7QUFDOUIsNEZBQTRGO0FBQzVGLG1HQUFtRztBQUU3RixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFzQyw0QkFBWTtJQUFsRDtRQUFBLHFFQWFDO1FBWEcsV0FBSyxHQUFXLElBQUksQ0FBQzs7UUFVckIsaUJBQWlCO0lBQ3JCLENBQUM7SUFURyx3QkFBd0I7SUFFeEIsZUFBZTtJQUVmLHdCQUFLLEdBQUw7SUFFQSxDQUFDO0lBUkQ7UUFEQyxRQUFRLENBQUMsY0FBSSxDQUFDOzJDQUNNO0lBRkosUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWE1QjtJQUFELGVBQUM7Q0FiRCxBQWFDLENBYnFDLEVBQUUsQ0FBQyxTQUFTLEdBYWpEO2tCQWJvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENhcmQgZnJvbSBcIi4vQ2FyZFwiO1xuXG4vLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEhhbmRDYXJkIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoQ2FyZClcbiAgICBjYXJkczogQ2FyZFtdID0gbnVsbDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Language.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3d3c6mK5nNMjYk5tCjZK4el', 'Language');
// Scripts/Language.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vi_VN_1 = require("../lang/vi_VN");
var th_TH_1 = require("../lang/th_TH");
var en_US_1 = require("../lang/en_US");
var tl_PH_1 = require("../lang/tl_PH");
var Language = /** @class */ (function () {
    function Language() {
        this.lang = en_US_1.default;
    }
    Language.getInstance = function () {
        if (!Language._instance) {
            Language._instance = new Language();
        }
        return Language._instance;
    };
    Language.prototype.load = function (lang) {
        if (lang == 'vi_VN') {
            this.lang = vi_VN_1.default;
        }
        else if (lang == 'th_TH') {
            this.lang = th_TH_1.default;
        }
        else if (lang == 'tl_PH') {
            this.lang = tl_PH_1.default;
        }
        else {
            this.lang = en_US_1.default;
        }
    };
    Language.prototype.get = function (key) {
        return this.lang[key];
    };
    Language._instance = null;
    return Language;
}());
exports.default = Language;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0xhbmd1YWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQStCO0FBQy9CLHVDQUErQjtBQUMvQix1Q0FBK0I7QUFDL0IsdUNBQStCO0FBRS9CO0lBQUE7UUFHWSxTQUFJLEdBQVEsZUFBRSxDQUFDO0lBeUIzQixDQUFDO0lBdkJpQixvQkFBVyxHQUF6QjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFO1lBQ3JCLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztTQUN2QztRQUNELE9BQU8sUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUM5QixDQUFDO0lBRU0sdUJBQUksR0FBWCxVQUFZLElBQVk7UUFDcEIsSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLGVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFTSxzQkFBRyxHQUFWLFVBQVcsR0FBVztRQUNsQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQXpCYyxrQkFBUyxHQUFhLElBQUksQ0FBQztJQTJCOUMsZUFBQztDQTVCRCxBQTRCQyxJQUFBO2tCQTVCb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB2aSBmcm9tICcuLi9sYW5nL3ZpX1ZOJztcbmltcG9ydCB0aCBmcm9tICcuLi9sYW5nL3RoX1RIJztcbmltcG9ydCBlbiBmcm9tICcuLi9sYW5nL2VuX1VTJztcbmltcG9ydCBwaCBmcm9tICcuLi9sYW5nL3RsX1BIJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGFuZ3VhZ2Uge1xuICAgIHByaXZhdGUgc3RhdGljIF9pbnN0YW5jZTogTGFuZ3VhZ2UgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBsYW5nOiBhbnkgPSBlbjtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0SW5zdGFuY2UoKSB7XG4gICAgICAgIGlmICghTGFuZ3VhZ2UuX2luc3RhbmNlKSB7XG4gICAgICAgICAgICBMYW5ndWFnZS5faW5zdGFuY2UgPSBuZXcgTGFuZ3VhZ2UoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTGFuZ3VhZ2UuX2luc3RhbmNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBsb2FkKGxhbmc6IHN0cmluZykge1xuICAgICAgICBpZiAobGFuZyA9PSAndmlfVk4nKSB7XG4gICAgICAgICAgICB0aGlzLmxhbmcgPSB2aTtcbiAgICAgICAgfSBlbHNlIGlmIChsYW5nID09ICd0aF9USCcpIHtcbiAgICAgICAgICAgIHRoaXMubGFuZyA9IHRoO1xuICAgICAgICB9IGVsc2UgaWYgKGxhbmcgPT0gJ3RsX1BIJykge1xuICAgICAgICAgICAgdGhpcy5sYW5nID0gcGg7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmxhbmcgPSBlbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBnZXQoa2V5OiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubGFuZ1trZXldO1xuICAgIH1cblxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/LBEntry.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '48e8adoRHBEI57tbH867RGK', 'LBEntry');
// Scripts/LBEntry.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LBEntry = /** @class */ (function (_super) {
    __extends(LBEntry, _super);
    function LBEntry() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rank = null;
        _this.medal = null;
        _this.avatar = null;
        _this.playerName = null;
        _this.playerCoin = null;
        _this.spriteMedals = [];
        _this.playerId = null;
        _this.photo = null;
        _this.coin = 0;
        _this.click_key = 'lb_battle';
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    LBEntry.prototype.start = function () {
    };
    LBEntry.prototype.render = function (rank, name, coin, avatar, playerId) {
        var _this = this;
        this.playerId = playerId;
        this.playerName.string = name;
        this.playerCoin.string = util_1.default.numberFormat(coin);
        this.coin = coin;
        cc.assetManager.loadRemote(avatar, function (err, tex) {
            _this.photo = tex;
            _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
        });
        if (this.spriteMedals.length >= rank) {
            this.rank.node.active = false;
            this.medal.spriteFrame = this.spriteMedals[rank - 1];
        }
        else {
            this.rank.string = util_1.default.numberFormat(rank);
            this.medal.node.active = false;
        }
        if (Api_1.default.playerId == playerId) {
            this.click_key = 'lb_share';
        }
    };
    LBEntry.prototype.onClick = function () {
        cc.systemEvent.emit(this.click_key, this.playerId, this.photo, this.playerName.string, this.coin);
    };
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "rank", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "medal", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerCoin", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], LBEntry.prototype, "spriteMedals", void 0);
    LBEntry = __decorate([
        ccclass
    ], LBEntry);
    return LBEntry;
}(cc.Component));
exports.default = LBEntry;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0xCRW50cnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsK0JBQTBCO0FBQzFCLDZCQUF3QjtBQUVsQixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFxQywyQkFBWTtJQUFqRDtRQUFBLHFFQW9EQztRQWxERyxVQUFJLEdBQWEsSUFBSSxDQUFDO1FBRXRCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFFeEIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixrQkFBWSxHQUFxQixFQUFFLENBQUM7UUFFNUIsY0FBUSxHQUFXLElBQUksQ0FBQztRQUN4QixXQUFLLEdBQWlCLElBQUksQ0FBQztRQUMzQixVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBQ2pCLGVBQVMsR0FBVyxXQUFXLENBQUM7O0lBbUM1QyxDQUFDO0lBbENHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYsdUJBQUssR0FBTDtJQUVBLENBQUM7SUFFRCx3QkFBTSxHQUFOLFVBQU8sSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsTUFBYyxFQUFFLFFBQWdCO1FBQWpGLGlCQXFCQztRQXBCRyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBZSxNQUFNLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRztZQUN0RCxLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztZQUNqQixLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTtZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxHQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDbEM7UUFFRCxJQUFJLGFBQUcsQ0FBQyxRQUFRLElBQUksUUFBUSxFQUFFO1lBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1NBQy9CO0lBQ0wsQ0FBQztJQUVELHlCQUFPLEdBQVA7UUFDSSxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEcsQ0FBQztJQWpERDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3lDQUNHO0lBRXRCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MENBQ0k7SUFFeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzsyQ0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOytDQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7K0NBQ1M7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztpREFDVztJQVpuQixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBb0QzQjtJQUFELGNBQUM7Q0FwREQsQUFvREMsQ0FwRG9DLEVBQUUsQ0FBQyxTQUFTLEdBb0RoRDtrQkFwRG9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IEFwaSBmcm9tIFwiLi9BcGlcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMQkVudHJ5IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgcmFuazogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgbWVkYWw6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBsYXllck5hbWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgcGxheWVyQ29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcbiAgICBzcHJpdGVNZWRhbHM6IGNjLlNwcml0ZUZyYW1lW10gPSBbXTtcblxuICAgIHByaXZhdGUgcGxheWVySWQ6IHN0cmluZyA9IG51bGw7XG4gICAgcHJpdmF0ZSBwaG90bzogY2MuVGV4dHVyZTJEID0gbnVsbDtcbiAgICBwcml2YXRlIGNvaW46IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSBjbGlja19rZXk6IHN0cmluZyA9ICdsYl9iYXR0bGUnO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICByZW5kZXIocmFuazogbnVtYmVyLCBuYW1lOiBzdHJpbmcsIGNvaW46IG51bWJlciwgYXZhdGFyOiBzdHJpbmcsIHBsYXllcklkOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5wbGF5ZXJJZCA9IHBsYXllcklkO1xuICAgICAgICB0aGlzLnBsYXllck5hbWUuc3RyaW5nID0gbmFtZTtcbiAgICAgICAgdGhpcy5wbGF5ZXJDb2luLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KGNvaW4pO1xuICAgICAgICB0aGlzLmNvaW4gPSBjb2luO1xuICAgICAgICBjYy5hc3NldE1hbmFnZXIubG9hZFJlbW90ZTxjYy5UZXh0dXJlMkQ+KGF2YXRhciwgKGVyciwgdGV4KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBob3RvID0gdGV4O1xuICAgICAgICAgICAgdGhpcy5hdmF0YXIuc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUodGV4KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHRoaXMuc3ByaXRlTWVkYWxzLmxlbmd0aCA+PSByYW5rKSB7XG4gICAgICAgICAgICB0aGlzLnJhbmsubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubWVkYWwuc3ByaXRlRnJhbWUgPSB0aGlzLnNwcml0ZU1lZGFsc1tyYW5rLTFdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5yYW5rLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KHJhbmspO1xuICAgICAgICAgICAgdGhpcy5tZWRhbC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKEFwaS5wbGF5ZXJJZCA9PSBwbGF5ZXJJZCkge1xuICAgICAgICAgICAgdGhpcy5jbGlja19rZXkgPSAnbGJfc2hhcmUnO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25DbGljaygpIHtcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCh0aGlzLmNsaWNrX2tleSwgdGhpcy5wbGF5ZXJJZCwgdGhpcy5waG90bywgdGhpcy5wbGF5ZXJOYW1lLnN0cmluZywgdGhpcy5jb2luKTtcbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Slider2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '60e38Bi/OJDvpmRhqGFGQN+', 'Slider2');
// Scripts/Slider2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var util_1 = require("./util");
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Slider2 = /** @class */ (function (_super) {
    __extends(Slider2, _super);
    function Slider2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maxValue = null;
        _this.minValue = null;
        _this.value = null;
        _this.fill = null;
        _this.slider = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    Slider2.prototype.onLoad = function () {
        var sliderEventHandler = new cc.Component.EventHandler();
        sliderEventHandler.target = this.node;
        sliderEventHandler.component = "Slider2";
        sliderEventHandler.handler = "_onValueChange";
        this.slider = this.getComponent(cc.Slider);
        this.slider.slideEvents.push(sliderEventHandler);
    };
    Slider2.prototype.start = function () {
        this.minValue.string = util_1.default.numberFormat(Config_1.default.minBet);
        this.maxValue.string = util_1.default.numberFormat(Config_1.default.maxBet);
        this._onValueChange(this.slider, null);
    };
    Slider2.prototype._onValueChange = function (sender, params) {
        var val = this.getValue();
        this.onValueChange(val);
        this.value.string = util_1.default.numberFormat(val);
        var size = this.fill.node.getContentSize();
        size.width = this.node.getContentSize().width * this.slider.progress;
        this.fill.node.setContentSize(size);
    };
    Slider2.prototype.getValue = function () {
        var val = Config_1.default.minBet + (Config_1.default.maxBet - Config_1.default.minBet) * this.slider.progress;
        return Math.round(val / 1000) * 1000;
    };
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "maxValue", void 0);
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "minValue", void 0);
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "value", void 0);
    __decorate([
        property(cc.Sprite)
    ], Slider2.prototype, "fill", void 0);
    Slider2 = __decorate([
        ccclass
    ], Slider2);
    return Slider2;
}(cc.Component));
exports.default = Slider2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1NsaWRlcjIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBQzlCLCtCQUEwQjtBQUUxQixvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjtBQUU1RSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFxQywyQkFBWTtJQUFqRDtRQUFBLHFFQThDQztRQTVDRyxjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRTFCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFFMUIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQWMsSUFBSSxDQUFDO1FBRXZCLFlBQU0sR0FBYyxJQUFJLENBQUM7O1FBbUN6QixpQkFBaUI7SUFDckIsQ0FBQztJQWpDRyx3QkFBd0I7SUFFeEIsd0JBQU0sR0FBTjtRQUNJLElBQUksa0JBQWtCLEdBQUcsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3pELGtCQUFrQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RDLGtCQUFrQixDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7UUFDeEMsa0JBQWtCLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDO1FBRTlDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsZ0NBQWMsR0FBZCxVQUFlLE1BQU0sRUFBRSxNQUFNO1FBQ3pCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNyRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELDBCQUFRLEdBQVI7UUFDSSxJQUFJLEdBQUcsR0FBRyxnQkFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLGdCQUFNLENBQUMsTUFBTSxHQUFHLGdCQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDakYsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUM7SUFDekMsQ0FBQztJQXpDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNPO0lBRTFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NkNBQ087SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzswQ0FDSTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lDQUNHO0lBUk4sT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQThDM0I7SUFBRCxjQUFDO0NBOUNELEFBOENDLENBOUNvQyxFQUFFLENBQUMsU0FBUyxHQThDaEQ7a0JBOUNvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuL3V0aWxcIjtcblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTbGlkZXIyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbWF4VmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbWluVmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgdmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICAgIGZpbGw6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBzbGlkZXI6IGNjLlNsaWRlciA9IG51bGw7XG4gICAgb25WYWx1ZUNoYW5nZTogKHY6IG51bWJlcikgPT4gdm9pZDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB2YXIgc2xpZGVyRXZlbnRIYW5kbGVyID0gbmV3IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIoKTtcbiAgICAgICAgc2xpZGVyRXZlbnRIYW5kbGVyLnRhcmdldCA9IHRoaXMubm9kZTtcbiAgICAgICAgc2xpZGVyRXZlbnRIYW5kbGVyLmNvbXBvbmVudCA9IFwiU2xpZGVyMlwiXG4gICAgICAgIHNsaWRlckV2ZW50SGFuZGxlci5oYW5kbGVyID0gXCJfb25WYWx1ZUNoYW5nZVwiO1xuXG4gICAgICAgIHRoaXMuc2xpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuU2xpZGVyKTtcbiAgICAgICAgdGhpcy5zbGlkZXIuc2xpZGVFdmVudHMucHVzaChzbGlkZXJFdmVudEhhbmRsZXIpO1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLm1pblZhbHVlLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KENvbmZpZy5taW5CZXQpO1xuICAgICAgICB0aGlzLm1heFZhbHVlLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KENvbmZpZy5tYXhCZXQpO1xuICAgICAgICB0aGlzLl9vblZhbHVlQ2hhbmdlKHRoaXMuc2xpZGVyLCBudWxsKTtcbiAgICB9XG5cbiAgICBfb25WYWx1ZUNoYW5nZShzZW5kZXIsIHBhcmFtcykge1xuICAgICAgICBsZXQgdmFsID0gdGhpcy5nZXRWYWx1ZSgpO1xuICAgICAgICB0aGlzLm9uVmFsdWVDaGFuZ2UodmFsKTtcbiAgICAgICAgdGhpcy52YWx1ZS5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdCh2YWwpO1xuICAgICAgICBsZXQgc2l6ZSA9IHRoaXMuZmlsbC5ub2RlLmdldENvbnRlbnRTaXplKCk7XG4gICAgICAgIHNpemUud2lkdGggPSB0aGlzLm5vZGUuZ2V0Q29udGVudFNpemUoKS53aWR0aCAqIHRoaXMuc2xpZGVyLnByb2dyZXNzO1xuICAgICAgICB0aGlzLmZpbGwubm9kZS5zZXRDb250ZW50U2l6ZShzaXplKTtcbiAgICB9XG5cbiAgICBnZXRWYWx1ZSgpOiBudW1iZXIge1xuICAgICAgICBsZXQgdmFsID0gQ29uZmlnLm1pbkJldCArIChDb25maWcubWF4QmV0IC0gQ29uZmlnLm1pbkJldCkgKiB0aGlzLnNsaWRlci5wcm9ncmVzcztcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodmFsIC8gMTAwMCkgKiAxMDAwO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Popup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '756eeJpotFA972j694PfnLH', 'Popup');
// Scripts/Popup.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Popup = /** @class */ (function (_super) {
    __extends(Popup, _super);
    function Popup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popups = [];
        return _this;
    }
    Popup_1 = Popup;
    // LIFE-CYCLE CALLBACKS:
    Popup.prototype.onLoad = function () {
    };
    Popup.prototype.start = function () {
        Popup_1.instance = this;
    };
    // update (dt) {}
    Popup.prototype.close = function (sender, id) {
        this.node.active = false;
        this.popups.forEach(function (p) {
            p.active = false;
        });
    };
    Popup.prototype.open = function (sender, id) {
        this.node.active = true;
        if (id) {
            this.popups[id - 1].active = true;
        }
    };
    var Popup_1;
    Popup.instance = null;
    __decorate([
        property(cc.Node)
    ], Popup.prototype, "popups", void 0);
    Popup = Popup_1 = __decorate([
        ccclass
    ], Popup);
    return Popup;
}(cc.Component));
exports.default = Popup;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1BvcHVwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsa0ZBQWtGO0FBQ2xGLHlGQUF5RjtBQUN6RixtQkFBbUI7QUFDbkIsNEZBQTRGO0FBQzVGLG1HQUFtRztBQUNuRyw4QkFBOEI7QUFDOUIsNEZBQTRGO0FBQzVGLG1HQUFtRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTdGLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQW1DLHlCQUFZO0lBQS9DO1FBQUEscUVBK0JDO1FBN0JHLFlBQU0sR0FBYyxFQUFFLENBQUM7O0lBNkIzQixDQUFDO2NBL0JvQixLQUFLO0lBSXRCLHdCQUF3QjtJQUV4QixzQkFBTSxHQUFOO0lBRUEsQ0FBQztJQUVELHFCQUFLLEdBQUw7UUFDSSxPQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQsaUJBQWlCO0lBRWpCLHFCQUFLLEdBQUwsVUFBTSxNQUFlLEVBQUUsRUFBVTtRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDO1lBQ2pCLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELG9CQUFJLEdBQUosVUFBSyxNQUFlLEVBQUUsRUFBVTtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBRyxFQUFFLEVBQUU7WUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQzs7SUFFYSxjQUFRLEdBQVUsSUFBSSxDQUFDO0lBNUJyQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lDQUNLO0lBRk4sS0FBSztRQUR6QixPQUFPO09BQ2EsS0FBSyxDQStCekI7SUFBRCxZQUFDO0NBL0JELEFBK0JDLENBL0JrQyxFQUFFLENBQUMsU0FBUyxHQStCOUM7a0JBL0JvQixLQUFLIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1cCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcG9wdXBzOiBjYy5Ob2RlW10gPSBbXTtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkICgpIHtcbiAgICAgICAgXG4gICAgfVxuXG4gICAgc3RhcnQgKCkge1xuICAgICAgICBQb3B1cC5pbnN0YW5jZSA9IHRoaXM7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIGNsb3NlKHNlbmRlcjogY2MuTm9kZSwgaWQ6IG51bWJlcikge1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMucG9wdXBzLmZvckVhY2gocCA9PiB7XG4gICAgICAgICAgICBwLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIG9wZW4oc2VuZGVyOiBjYy5Ob2RlLCBpZDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBpZihpZCkge1xuICAgICAgICAgICAgdGhpcy5wb3B1cHNbaWQgLSAxXS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBpbnN0YW5jZTogUG9wdXAgPSBudWxsO1xufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Shop.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '22b59b5sv5O3KRVI8Z7dJWa', 'Shop');
// Scripts/Shop.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Shop = /** @class */ (function (_super) {
    __extends(Shop, _super);
    function Shop() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Shop.prototype.start = function () {
    };
    // update (dt) {}
    Shop.prototype.backClick = function () {
        cc.director.loadScene('home');
    };
    Shop.prototype.buyClick = function (value) {
    };
    Shop = __decorate([
        ccclass
    ], Shop);
    return Shop;
}(cc.Component));
exports.default = Shop;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1Nob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQixrRkFBa0Y7QUFDbEYseUZBQXlGO0FBQ3pGLG1CQUFtQjtBQUNuQiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBQ25HLDhCQUE4QjtBQUM5Qiw0RkFBNEY7QUFDNUYsbUdBQW1HOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFN0YsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBa0Msd0JBQVk7SUFBOUM7O0lBa0JBLENBQUM7SUFqQkcsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZixvQkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELGlCQUFpQjtJQUVqQix3QkFBUyxHQUFUO1FBQ0ksRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELHVCQUFRLEdBQVIsVUFBUyxLQUFhO0lBRXRCLENBQUM7SUFqQmdCLElBQUk7UUFEeEIsT0FBTztPQUNhLElBQUksQ0FrQnhCO0lBQUQsV0FBQztDQWxCRCxBQWtCQyxDQWxCaUMsRUFBRSxDQUFDLFNBQVMsR0FrQjdDO2tCQWxCb0IsSUFBSSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2hvcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0ICgpIHtcblxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBiYWNrQ2xpY2soKSB7XG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnaG9tZScpO1xuICAgIH1cblxuICAgIGJ1eUNsaWNrKHZhbHVlOiBudW1iZXIpIHtcblxuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/SpinWheel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'daacdf5d5lMhrNHqrN6niHA', 'SpinWheel');
// Scripts/SpinWheel.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var Popup_1 = require("./Popup");
var Toast_1 = require("./Toast");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SpinWheel = /** @class */ (function (_super) {
    __extends(SpinWheel, _super);
    function SpinWheel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.wheel = null;
        _this.ticket = null;
        _this.popup = null;
        _this.toast = null;
        _this.btnRun = null;
        _this.btnAd = null;
        _this.audioSpinWin = null;
        _this.audioSpinLose = null;
        _this.audioSpinRun = null;
        _this.modal = null;
        _this.won = 0;
        _this.isRunning = false;
        return _this;
    }
    SpinWheel_1 = SpinWheel;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    SpinWheel.prototype.start = function () {
    };
    // update (dt) {}
    SpinWheel.prototype.show = function (won) {
        var playNow = Api_1.default.ticket > 0;
        this.popup.open(this.node, 2);
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
        this.node.active = true;
        this.isRunning = false;
        this.btnAd.node.active = !playNow;
        this.btnRun.node.active = playNow;
        this.won = won;
    };
    SpinWheel.prototype.hide = function () {
        this.popup.close(this.node, 2);
    };
    SpinWheel.prototype.play = function () {
        if (this.isRunning || Api_1.default.ticket <= 0)
            return;
        Api_1.default.ticket--;
        this.updateTicket();
        this.spin();
        Api_1.default.logEvent(EventKeys_1.default.SPIN);
    };
    SpinWheel.prototype.spin = function () {
        util_1.default.playAudio(this.audioSpinRun);
        this.isRunning = true;
        var result = this.randomPrize();
        var action = cc.rotateBy(5, 360 * 10 + result.angle - 30 - (this.wheel.node.angle % 360));
        this.wheel.node.runAction(cc.sequence(action.easing(cc.easeCubicActionOut()), cc.callFunc(this.onCompleted, this, result)));
    };
    SpinWheel.prototype.showVideoAd = function () {
        var _this = this;
        if (this.isRunning)
            return;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.spin();
        }, function (msg) {
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_ERROR);
            _this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
        });
    };
    SpinWheel.prototype.onCompleted = function (sender, data) {
        var msg = Language_1.default.getInstance().get(data.msg);
        this.toast.show(msg);
        this.onSpinCompleted(data, this.won);
        if (data.id == 1) {
            this.node.runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(this.spin, this)));
        }
        else {
            this.node.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(this.hide, this)));
            util_1.default.playAudio(data.id == 0 ? this.audioSpinLose : this.audioSpinWin);
        }
        // if(Api.ticket == 0) {
        //     this.btnAd.node.active = true;
        //     this.btnRun.node.active = false;
        // }
    };
    SpinWheel.prototype.onDisable = function () {
        this.onSpinHide();
    };
    SpinWheel.prototype.compute = function (id, val) {
        switch (id) {
            case 2: return val;
            case 3: return val * 2;
            case 4: return val * 4;
            case 5: return val * 9;
            default: return 0;
        }
    };
    SpinWheel.prototype.onClose = function () {
        if (this.isRunning)
            return;
        Popup_1.default.instance.close(null, 2);
    };
    SpinWheel.prototype.randomPrize = function () {
        var rate = Math.random() * 100;
        var sample = SpinWheel_1.result_rate;
        for (var i = 0; i < sample.length; i++)
            if (sample[i].rate > rate)
                return sample[i];
        return sample[0];
    };
    SpinWheel.prototype.updateTicket = function () {
        Api_1.default.updateTicket();
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
    };
    var SpinWheel_1;
    //Tỷ lệ vòng quay : X10 ( 5% ) , X5 ( 10% ) , X3 ( 20% ) , THÊM LƯỢT ( 15% ) , MẤT LƯỢT ( 15% ) , X2 (35%)
    SpinWheel.result_rate = [
        { id: 0, angle: 4 * 60, rate: 15, msg: "LUCK" },
        { id: 1, angle: 5 * 60, rate: 30, msg: "TURN" },
        { id: 2, angle: 0 * 60, rate: 65, msg: "X2" },
        { id: 3, angle: 1 * 60, rate: 85, msg: "X3" },
        { id: 4, angle: 2 * 60, rate: 90, msg: "X5" },
        { id: 5, angle: 3 * 60, rate: 100, msg: "X10" },
    ];
    __decorate([
        property(cc.Sprite)
    ], SpinWheel.prototype, "wheel", void 0);
    __decorate([
        property(cc.Label)
    ], SpinWheel.prototype, "ticket", void 0);
    __decorate([
        property(Popup_1.default)
    ], SpinWheel.prototype, "popup", void 0);
    __decorate([
        property(Toast_1.default)
    ], SpinWheel.prototype, "toast", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnRun", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnAd", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinRun", void 0);
    __decorate([
        property(Modal_1.default)
    ], SpinWheel.prototype, "modal", void 0);
    SpinWheel = SpinWheel_1 = __decorate([
        ccclass
    ], SpinWheel);
    return SpinWheel;
}(cc.Component));
exports.default = SpinWheel;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1NwaW5XaGVlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVuRywrQkFBMEI7QUFDMUIsNkJBQXdCO0FBQ3hCLGlDQUE0QjtBQUM1QixpQ0FBNEI7QUFDNUIsdUNBQWtDO0FBQ2xDLHlDQUFvQztBQUNwQyx1Q0FBa0M7QUFFNUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBdUMsNkJBQVk7SUFBbkQ7UUFBQSxxRUFxSkM7UUFuSkcsV0FBSyxHQUFjLElBQUksQ0FBQztRQUV4QixZQUFNLEdBQWEsSUFBSSxDQUFDO1FBRXhCLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFVLElBQUksQ0FBQztRQUVwQixZQUFNLEdBQWMsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFHeEIsa0JBQVksR0FBaUIsSUFBSSxDQUFDO1FBRWxDLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxrQkFBWSxHQUFpQixJQUFJLENBQUM7UUFFbEMsV0FBSyxHQUFVLElBQUksQ0FBQztRQUtaLFNBQUcsR0FBVyxDQUFDLENBQUM7UUFDaEIsZUFBUyxHQUFZLEtBQUssQ0FBQzs7SUEwSHZDLENBQUM7a0JBckpvQixTQUFTO0lBNEIxQix3QkFBd0I7SUFFeEIsZUFBZTtJQUVmLHlCQUFLLEdBQUw7SUFDQSxDQUFDO0lBRUQsaUJBQWlCO0lBRWpCLHdCQUFJLEdBQUosVUFBSyxHQUFXO1FBQ1osSUFBTSxPQUFPLEdBQUcsYUFBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7SUFDbkIsQ0FBQztJQUVELHdCQUFJLEdBQUo7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCx3QkFBSSxHQUFKO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLGFBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQztZQUFFLE9BQU87UUFFOUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVaLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsd0JBQUksR0FBSjtRQUNJLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRWxDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQ2pDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLENBQUMsRUFDdEMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FDOUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtCQUFXLEdBQVg7UUFBQSxpQkFTQztRQVJHLElBQUksSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQzNCLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2xELGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQyxFQUFFLFVBQUMsR0FBRztZQUNILGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ2xELEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDM0QsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sK0JBQVcsR0FBbkIsVUFBb0IsTUFBZSxFQUFFLElBQVM7UUFDMUMsSUFBTSxHQUFHLEdBQUcsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFHLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFDakIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUMvQixDQUFDLENBQUE7U0FDTDthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFDakIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUMvQixDQUFDLENBQUM7WUFDSCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDekU7UUFDRCx3QkFBd0I7UUFDeEIscUNBQXFDO1FBQ3JDLHVDQUF1QztRQUN2QyxJQUFJO0lBQ1IsQ0FBQztJQUVELDZCQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7SUFDckIsQ0FBQztJQUVELDJCQUFPLEdBQVAsVUFBUSxFQUFVLEVBQUUsR0FBVztRQUMzQixRQUFPLEVBQUUsRUFBRTtZQUNQLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDbkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDckI7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQzNCLGVBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU8sK0JBQVcsR0FBbkI7UUFDSSxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBQ2pDLElBQUksTUFBTSxHQUFHLFdBQVMsQ0FBQyxXQUFXLENBQUM7UUFDbkMsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO1lBQzlCLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJO2dCQUFFLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hELE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFTyxnQ0FBWSxHQUFwQjtRQUNJLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2RCxDQUFDOztJQUVELDBHQUEwRztJQUNsRixxQkFBVyxHQUFHO1FBQ2xDLEVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUM7UUFDNUMsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUMsRUFBRSxFQUFFLElBQUksRUFBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBQztRQUM1QyxFQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFDO1FBQzFDLEVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUM7UUFDMUMsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUMsRUFBRSxFQUFFLElBQUksRUFBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBQztRQUMxQyxFQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFDO0tBQzlDLENBQUE7SUFsSkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs0Q0FDSTtJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNLO0lBRXhCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzs0Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7NENBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2Q0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzRDQUNJO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUMsQ0FBQzttREFDRztJQUVsQztRQURDLFFBQVEsQ0FBQyxFQUFDLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFDLENBQUM7b0RBQ0k7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBQyxDQUFDO21EQUNHO0lBRWxDO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzs0Q0FDSTtJQXJCSCxTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBcUo3QjtJQUFELGdCQUFDO0NBckpELEFBcUpDLENBckpzQyxFQUFFLENBQUMsU0FBUyxHQXFKbEQ7a0JBckpvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IEFwaSBmcm9tIFwiLi9BcGlcIjtcbmltcG9ydCBQb3B1cCBmcm9tIFwiLi9Qb3B1cFwiO1xuaW1wb3J0IFRvYXN0IGZyb20gXCIuL1RvYXN0XCI7XG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4vcG9wb3AvTW9kYWxcIjtcbmltcG9ydCBFdmVudEtleXMgZnJvbSBcIi4vRXZlbnRLZXlzXCI7XG5pbXBvcnQgTGFuZ3VhZ2UgZnJvbSBcIi4vTGFuZ3VhZ2VcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTcGluV2hlZWwgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgd2hlZWw6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHRpY2tldDogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShUb2FzdClcbiAgICB0b2FzdDogVG9hc3QgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgYnRuUnVuOiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgYnRuQWQ6IGNjLkJ1dHRvbiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6IGNjLkF1ZGlvQ2xpcH0pXG4gICAgYXVkaW9TcGluV2luOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7dHlwZTogY2MuQXVkaW9DbGlwfSlcbiAgICBhdWRpb1NwaW5Mb3NlOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7dHlwZTogY2MuQXVkaW9DbGlwfSlcbiAgICBhdWRpb1NwaW5SdW46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KE1vZGFsKVxuICAgIG1vZGFsOiBNb2RhbCA9IG51bGw7XG5cbiAgICBvblNwaW5IaWRlOiAoKSA9PiB2b2lkO1xuICAgIG9uU3BpbkNvbXBsZXRlZDogKHJlc3VsdDoge2lkOiBudW1iZXIsIGFuZ2xlOiBudW1iZXIsIG1zZzogc3RyaW5nfSwgd29uOiBudW1iZXIpID0+IHZvaWQ7XG5cbiAgICBwcml2YXRlIHdvbjogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIGlzUnVubmluZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHNob3cod29uOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgcGxheU5vdyA9IEFwaS50aWNrZXQgPiAwO1xuICAgICAgICB0aGlzLnBvcHVwLm9wZW4odGhpcy5ub2RlLCAyKTtcbiAgICAgICAgdGhpcy50aWNrZXQuc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLnRpY2tldCk7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmlzUnVubmluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLmJ0bkFkLm5vZGUuYWN0aXZlID0gIXBsYXlOb3c7XG4gICAgICAgIHRoaXMuYnRuUnVuLm5vZGUuYWN0aXZlID0gcGxheU5vdztcbiAgICAgICAgdGhpcy53b24gPSB3b247XG4gICAgfVxuXG4gICAgaGlkZSgpIHtcbiAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgIH1cblxuICAgIHBsYXkoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzUnVubmluZyB8fCBBcGkudGlja2V0IDw9IDApIHJldHVybjtcblxuICAgICAgICBBcGkudGlja2V0LS07XG4gICAgICAgIHRoaXMudXBkYXRlVGlja2V0KCk7XG5cbiAgICAgICAgdGhpcy5zcGluKCk7XG5cbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5TUElOKTtcbiAgICB9XG5cbiAgICBzcGluKCkge1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvU3BpblJ1bik7XG5cbiAgICAgICAgdGhpcy5pc1J1bm5pbmcgPSB0cnVlO1xuICAgICAgICBsZXQgcmVzdWx0ID0gdGhpcy5yYW5kb21Qcml6ZSgpO1xuICAgICAgICBsZXQgYWN0aW9uID0gY2Mucm90YXRlQnkoNSwgMzYwICogMTAgKyByZXN1bHQuYW5nbGUgLSAzMCAtICh0aGlzLndoZWVsLm5vZGUuYW5nbGUgJSAzNjApKTtcbiAgICAgICAgdGhpcy53aGVlbC5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGFjdGlvbi5lYXNpbmcoY2MuZWFzZUN1YmljQWN0aW9uT3V0KCkpLFxuICAgICAgICAgICAgY2MuY2FsbEZ1bmModGhpcy5vbkNvbXBsZXRlZCwgdGhpcywgcmVzdWx0KVxuICAgICAgICApKTtcbiAgICB9XG5cbiAgICBzaG93VmlkZW9BZCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNSdW5uaW5nKSByZXR1cm47XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUE9QVVBfQURSRVdBUkRfU1BJTl9DTElDSyk7XG4gICAgICAgIEFwaS5zaG93UmV3YXJkZWRWaWRlbygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNwaW4oKTtcbiAgICAgICAgfSwgKG1zZykgPT4ge1xuICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9TUElOX0VSUk9SKTtcbiAgICAgICAgICAgIHRoaXMubW9kYWwuc2hvdyhMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCgnTk9WSURFTycpKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uQ29tcGxldGVkKHNlbmRlcjogY2MuTm9kZSwgZGF0YTogYW55KSB7XG4gICAgICAgIGNvbnN0IG1zZyA9IExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KGRhdGEubXNnKTtcbiAgICAgICAgdGhpcy50b2FzdC5zaG93KG1zZyk7XG4gICAgICAgIHRoaXMub25TcGluQ29tcGxldGVkKGRhdGEsIHRoaXMud29uKTtcbiAgICAgICAgaWYoZGF0YS5pZCA9PSAxKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjMpLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuc3BpbiwgdGhpcylcbiAgICAgICAgICAgICkpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjUpLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgICAgICkpO1xuICAgICAgICAgICAgdXRpbC5wbGF5QXVkaW8oZGF0YS5pZCA9PSAwID8gdGhpcy5hdWRpb1NwaW5Mb3NlIDogdGhpcy5hdWRpb1NwaW5XaW4pO1xuICAgICAgICB9XG4gICAgICAgIC8vIGlmKEFwaS50aWNrZXQgPT0gMCkge1xuICAgICAgICAvLyAgICAgdGhpcy5idG5BZC5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIC8vICAgICB0aGlzLmJ0blJ1bi5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgb25EaXNhYmxlKCkge1xuICAgICAgICB0aGlzLm9uU3BpbkhpZGUoKVxuICAgIH1cblxuICAgIGNvbXB1dGUoaWQ6IG51bWJlciwgdmFsOiBudW1iZXIpIHtcbiAgICAgICAgc3dpdGNoKGlkKSB7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiB2YWw7XG4gICAgICAgICAgICBjYXNlIDM6IHJldHVybiB2YWwgKiAyO1xuICAgICAgICAgICAgY2FzZSA0OiByZXR1cm4gdmFsICogNDtcbiAgICAgICAgICAgIGNhc2UgNTogcmV0dXJuIHZhbCAqIDk7XG4gICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ2xvc2UoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzUnVubmluZykgcmV0dXJuO1xuICAgICAgICBQb3B1cC5pbnN0YW5jZS5jbG9zZShudWxsLCAyKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJhbmRvbVByaXplKCkge1xuICAgICAgICBjb25zdCByYXRlID0gTWF0aC5yYW5kb20oKSAqIDEwMDtcbiAgICAgICAgbGV0IHNhbXBsZSA9IFNwaW5XaGVlbC5yZXN1bHRfcmF0ZTtcbiAgICAgICAgZm9yKGxldCBpPTA7IGk8IHNhbXBsZS5sZW5ndGg7IGkrKylcbiAgICAgICAgICAgIGlmIChzYW1wbGVbaV0ucmF0ZSA+IHJhdGUpIHJldHVybiBzYW1wbGVbaV07XG4gICAgICAgIHJldHVybiBzYW1wbGVbMF07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1cGRhdGVUaWNrZXQoKSB7XG4gICAgICAgIEFwaS51cGRhdGVUaWNrZXQoKTtcbiAgICAgICAgdGhpcy50aWNrZXQuc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLnRpY2tldCk7XG4gICAgfVxuXG4gICAgLy9U4bu3IGzhu4cgdsOybmcgcXVheSA6IFgxMCAoIDUlICkgLCBYNSAoIDEwJSApICwgWDMgKCAyMCUgKSAsIFRIw4pNIEzGr+G7olQgKCAxNSUgKSAsIE3huqRUIEzGr+G7olQgKCAxNSUgKSAsIFgyICgzNSUpXG4gICAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgcmVzdWx0X3JhdGUgPSBbXG4gICAgICAgIHtpZDogMCwgYW5nbGU6IDQqNjAsIHJhdGU6ICAxNSwgbXNnOiBcIkxVQ0tcIn0sXG4gICAgICAgIHtpZDogMSwgYW5nbGU6IDUqNjAsIHJhdGU6ICAzMCwgbXNnOiBcIlRVUk5cIn0sXG4gICAgICAgIHtpZDogMiwgYW5nbGU6IDAqNjAsIHJhdGU6ICA2NSwgbXNnOiBcIlgyXCJ9LFxuICAgICAgICB7aWQ6IDMsIGFuZ2xlOiAxKjYwLCByYXRlOiAgODUsIG1zZzogXCJYM1wifSxcbiAgICAgICAge2lkOiA0LCBhbmdsZTogMio2MCwgcmF0ZTogIDkwLCBtc2c6IFwiWDVcIn0sXG4gICAgICAgIHtpZDogNSwgYW5nbGU6IDMqNjAsIHJhdGU6IDEwMCwgbXNnOiBcIlgxMFwifSxcbiAgICBdXG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Notice.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6cfdf+Ec8NOfKCqLqWb3Un0', 'Notice');
// Scripts/Notice.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Alert = /** @class */ (function (_super) {
    __extends(Alert, _super);
    function Alert() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
    }
    Alert_1 = Alert;
    // LIFE-CYCLE CALLBACKS:
    Alert.prototype.onLoad = function () {
        this.label = this.getComponent(cc.Label);
    };
    Alert.prototype.start = function () { };
    Alert.prototype.showBigPig = function (num, duration) {
        if (num == 2) {
            this.show(Alert_1.TWO_PIG, duration);
        }
        else if (num == 3) {
            this.show(Alert_1.THREE_PIG, duration);
        }
        else {
            this.show(Alert_1.ONE_PIG, duration);
        }
    };
    Alert.prototype.show = function (type, duration) {
        this.node.active = true;
        this.label.string = Language_1.default.getInstance().get(type);
        var action = cc.sequence(cc.delayTime(duration), cc.callFunc(this.hide, this));
        this.node.runAction(action);
    };
    Alert.prototype.hide = function () {
        this.node.active = false;
    };
    var Alert_1;
    // update (dt) {}
    Alert.ONE_PIG = '1BEST';
    Alert.TWO_PIG = '2BEST';
    Alert.THREE_PIG = '3BEST';
    Alert.THREE_PAIR = '3PAIRS';
    Alert.FOUR_CARD = 'FOURKIND';
    Alert.FOUR_PAIR = '4PAIRS';
    Alert = Alert_1 = __decorate([
        ccclass
    ], Alert);
    return Alert;
}(cc.Component));
exports.default = Alert;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL05vdGljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRix1Q0FBa0M7QUFFNUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBbUMseUJBQVk7SUFBL0M7UUFBQSxxRUEwQ0M7UUF6Q1csV0FBSyxHQUFhLElBQUksQ0FBQzs7SUF5Q25DLENBQUM7Y0ExQ29CLEtBQUs7SUFFdEIsd0JBQXdCO0lBRXhCLHNCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxxQkFBSyxHQUFMLGNBQVcsQ0FBQztJQUVaLDBCQUFVLEdBQVYsVUFBVyxHQUFXLEVBQUUsUUFBZ0I7UUFDcEMsSUFBRyxHQUFHLElBQUksQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ3RDO2FBQU0sSUFBRyxHQUFHLElBQUksQ0FBQyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBSyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQztJQUVELG9CQUFJLEdBQUosVUFBSyxJQUFZLEVBQUUsUUFBZ0I7UUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQ3BCLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQ3RCLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDL0IsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxvQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7O0lBRUQsaUJBQWlCO0lBRUQsYUFBTyxHQUFXLE9BQU8sQ0FBQztJQUMxQixhQUFPLEdBQVcsT0FBTyxDQUFDO0lBQzFCLGVBQVMsR0FBVyxPQUFPLENBQUM7SUFDNUIsZ0JBQVUsR0FBVyxRQUFRLENBQUM7SUFDOUIsZUFBUyxHQUFXLFVBQVUsQ0FBQztJQUMvQixlQUFTLEdBQVcsUUFBUSxDQUFDO0lBekM1QixLQUFLO1FBRHpCLE9BQU87T0FDYSxLQUFLLENBMEN6QjtJQUFELFlBQUM7Q0ExQ0QsQUEwQ0MsQ0ExQ2tDLEVBQUUsQ0FBQyxTQUFTLEdBMEM5QztrQkExQ29CLEtBQUsiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IExhbmd1YWdlIGZyb20gXCIuL0xhbmd1YWdlXCI7XG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWxlcnQgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIHByaXZhdGUgbGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIG9uTG9hZCAoKSB7XG4gICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmdldENvbXBvbmVudChjYy5MYWJlbCk7XG4gICAgfVxuXG4gICAgc3RhcnQgKCkgeyB9XG5cbiAgICBzaG93QmlnUGlnKG51bTogbnVtYmVyLCBkdXJhdGlvbjogbnVtYmVyKSB7XG4gICAgICAgIGlmKG51bSA9PSAyKSB7XG4gICAgICAgICAgICB0aGlzLnNob3coQWxlcnQuVFdPX1BJRywgZHVyYXRpb24pO1xuICAgICAgICB9IGVsc2UgaWYobnVtID09IDMpIHtcbiAgICAgICAgICAgIHRoaXMuc2hvdyhBbGVydC5USFJFRV9QSUcsIGR1cmF0aW9uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc2hvdyhBbGVydC5PTkVfUElHLCBkdXJhdGlvbik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzaG93KHR5cGU6IHN0cmluZywgZHVyYXRpb246IG51bWJlcikge1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCh0eXBlKTtcbiAgICAgICAgbGV0IGFjdGlvbiA9IGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MuZGVsYXlUaW1lKGR1cmF0aW9uKSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihhY3Rpb24pO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IE9ORV9QSUc6IHN0cmluZyA9ICcxQkVTVCc7XG4gICAgc3RhdGljIHJlYWRvbmx5IFRXT19QSUc6IHN0cmluZyA9ICcyQkVTVCc7XG4gICAgc3RhdGljIHJlYWRvbmx5IFRIUkVFX1BJRzogc3RyaW5nID0gJzNCRVNUJztcbiAgICBzdGF0aWMgcmVhZG9ubHkgVEhSRUVfUEFJUjogc3RyaW5nID0gJzNQQUlSUyc7XG4gICAgc3RhdGljIHJlYWRvbmx5IEZPVVJfQ0FSRDogc3RyaW5nID0gJ0ZPVVJLSU5EJztcbiAgICBzdGF0aWMgcmVhZG9ubHkgRk9VUl9QQUlSOiBzdHJpbmcgPSAnNFBBSVJTJztcbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Toast.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9f74bKQm+BNarPR7bnDfejb', 'Toast');
// Scripts/Toast.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Toast = /** @class */ (function (_super) {
    __extends(Toast, _super);
    function Toast() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Toast.prototype.start = function () {
    };
    // update (dt) {}
    Toast.prototype.show = function (text) {
        this.node.stopAllActions();
        this.label.string = text;
        this.node.active = true;
        var widget = this.getComponent(cc.Widget);
        widget.top = -this.node.height;
        widget.updateAlignment();
        this.node.runAction(cc.sequence(cc.moveBy(0.5, 0, -this.node.height), cc.delayTime(0.7), cc.moveBy(0.5, 0, this.node.height), cc.callFunc(this.hide, this)));
    };
    Toast.prototype.hide = function () {
        this.node.active = false;
    };
    __decorate([
        property(cc.Label)
    ], Toast.prototype, "label", void 0);
    Toast = __decorate([
        ccclass
    ], Toast);
    return Toast;
}(cc.Component));
exports.default = Toast;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1RvYXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsa0ZBQWtGO0FBQ2xGLHlGQUF5RjtBQUN6RixtQkFBbUI7QUFDbkIsNEZBQTRGO0FBQzVGLG1HQUFtRztBQUNuRyw4QkFBOEI7QUFDOUIsNEZBQTRGO0FBQzVGLG1HQUFtRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTdGLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQW1DLHlCQUFZO0lBQS9DO1FBQUEscUVBZ0NDO1FBN0JHLFdBQUssR0FBYSxJQUFJLENBQUM7O0lBNkIzQixDQUFDO0lBM0JHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYscUJBQUssR0FBTDtJQUNBLENBQUM7SUFFRCxpQkFBaUI7SUFFakIsb0JBQUksR0FBSixVQUFLLElBQVk7UUFDYixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQy9CLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUMzQixFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNwQyxFQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUNqQixFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDbkMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUMvQixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0JBQUksR0FBSjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBNUJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7d0NBQ0k7SUFITixLQUFLO1FBRHpCLE9BQU87T0FDYSxLQUFLLENBZ0N6QjtJQUFELFlBQUM7Q0FoQ0QsQUFnQ0MsQ0FoQ2tDLEVBQUUsQ0FBQyxTQUFTLEdBZ0M5QztrQkFoQ29CLEtBQUsiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRvYXN0IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBsYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0ICgpIHtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxuXG4gICAgc2hvdyh0ZXh0OiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5ub2RlLnN0b3BBbGxBY3Rpb25zKCk7XG4gICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gdGV4dDtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIGxldCB3aWRnZXQgPSB0aGlzLmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgICAgICB3aWRnZXQudG9wID0gLXRoaXMubm9kZS5oZWlnaHQ7XG4gICAgICAgIHdpZGdldC51cGRhdGVBbGlnbm1lbnQoKTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLm1vdmVCeSgwLjUsIDAsIC10aGlzLm5vZGUuaGVpZ2h0KSxcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjcpLFxuICAgICAgICAgICAgY2MubW92ZUJ5KDAuNSwgMCwgdGhpcy5ub2RlLmhlaWdodCksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLmhpZGUsIHRoaXMpXG4gICAgICAgICkpO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/util.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5164dU8pUhEeacqN2jObHLU', 'util');
// Scripts/util.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Config_1 = require("./Config");
var util = /** @class */ (function () {
    function util() {
    }
    util.numberFormat = function (n) {
        var ii = 0;
        while ((n = n / 1000) >= 1) {
            ii++;
        }
        return (Math.round(n * 10 * 1000) / 10) + util.prefix[ii];
    };
    util.playAudio = function (audioClip) {
        if (Config_1.default.soundEnable)
            cc.audioEngine.playEffect(audioClip, false);
    };
    util.prefix = ["", "k", "M", "G", "T", "P", "E", "Z", "Y", "x10^27", "x10^30", "x10^33"]; // should be enough. Number.MAX_VALUE is about 10^308
    util.logTimeInGame = function (time) {
        if ([30, 60, 90, 120, 180, 270, 510].indexOf(time) > -1 || time === 0) {
            Api_1.default.logEvent(time + "s_sstime");
        }
    };
    return util;
}());
exports.default = util;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3V0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw2QkFBd0I7QUFDeEIsbUNBQThCO0FBRTlCO0lBQUE7SUFtQkEsQ0FBQztJQWhCaUIsaUJBQVksR0FBMUIsVUFBMkIsQ0FBUztRQUNoQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFBRSxFQUFFLEVBQUUsQ0FBQztTQUFFO1FBQ3JDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRWEsY0FBUyxHQUF2QixVQUF3QixTQUF1QjtRQUMzQyxJQUFJLGdCQUFNLENBQUMsV0FBVztZQUNsQixFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQVhlLFdBQU0sR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxxREFBcUQ7SUFhNUksa0JBQWEsR0FBRyxVQUFDLElBQVk7UUFDdkMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxFQUFFO1lBQ25FLGFBQUcsQ0FBQyxRQUFRLENBQUksSUFBSSxhQUFVLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUMsQ0FBQTtJQUNMLFdBQUM7Q0FuQkQsQUFtQkMsSUFBQTtrQkFuQm9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgdXRpbCB7XG4gICAgc3RhdGljIHJlYWRvbmx5IHByZWZpeCA9IFtcIlwiLCBcImtcIiwgXCJNXCIsIFwiR1wiLCBcIlRcIiwgXCJQXCIsIFwiRVwiLCBcIlpcIiwgXCJZXCIsIFwieDEwXjI3XCIsIFwieDEwXjMwXCIsIFwieDEwXjMzXCJdOyAvLyBzaG91bGQgYmUgZW5vdWdoLiBOdW1iZXIuTUFYX1ZBTFVFIGlzIGFib3V0IDEwXjMwOFxuXG4gICAgcHVibGljIHN0YXRpYyBudW1iZXJGb3JtYXQobjogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IGlpID0gMDtcbiAgICAgICAgd2hpbGUgKChuID0gbiAvIDEwMDApID49IDEpIHsgaWkrKzsgfVxuICAgICAgICByZXR1cm4gKE1hdGgucm91bmQobiAqIDEwICogMTAwMCkgLyAxMCkgKyB1dGlsLnByZWZpeFtpaV07XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBwbGF5QXVkaW8oYXVkaW9DbGlwOiBjYy5BdWRpb0NsaXApIHtcbiAgICAgICAgaWYgKENvbmZpZy5zb3VuZEVuYWJsZSlcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoYXVkaW9DbGlwLCBmYWxzZSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBsb2dUaW1lSW5HYW1lID0gKHRpbWU6IG51bWJlcikgPT4ge1xuICAgICAgICBpZiAoWzMwLCA2MCwgOTAsIDEyMCwgMTgwLCAyNzAsIDUxMF0uaW5kZXhPZih0aW1lKSA+IC0xIHx8IHRpbWUgPT09IDApIHtcbiAgICAgICAgICAgIEFwaS5sb2dFdmVudChgJHt0aW1lfXNfc3N0aW1lYCk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Text2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '33666ScFZtOSIs1HT5qmapm', 'Text2');
// Scripts/Text2.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Text2 = /** @class */ (function (_super) {
    __extends(Text2, _super);
    function Text2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.key = '';
        _this.label = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Text2.prototype.start = function () {
        this.label = this.getComponent(cc.Label);
        if (!this.label) {
            this.label = this.getComponent(cc.RichText);
        }
        if (this.label) {
            this.label.string = Language_1.default.getInstance().get(this.key);
        }
        cc.systemEvent.on('LANG_CHAN', this.onLanguageChange, this);
    };
    // update (dt) {}
    Text2.prototype.onLanguageChange = function () {
        console.log('onLanguageChange', this, this.label);
        if (this.label) {
            this.label.string = Language_1.default.getInstance().get(this.key);
        }
    };
    Text2.prototype.onDestroy = function () {
        cc.systemEvent.off('LANG_CHAN');
    };
    __decorate([
        property()
    ], Text2.prototype, "key", void 0);
    Text2 = __decorate([
        ccclass
    ], Text2);
    return Text2;
}(cc.Component));
exports.default = Text2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1RleHQyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLHVDQUFrQztBQUU1QixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFtQyx5QkFBWTtJQUEvQztRQUFBLHFFQStCQztRQTdCRyxTQUFHLEdBQVcsRUFBRSxDQUFDO1FBQ2pCLFdBQUssR0FBMkIsSUFBSSxDQUFDOztJQTRCekMsQ0FBQztJQTFCRyx3QkFBd0I7SUFFeEIscUJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDYixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzVEO1FBRUQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQsaUJBQWlCO0lBRWpCLGdDQUFnQixHQUFoQjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNqRCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDNUQ7SUFDTCxDQUFDO0lBRUQseUJBQVMsR0FBVDtRQUNJLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUE1QkQ7UUFEQyxRQUFRLEVBQUU7c0NBQ007SUFGQSxLQUFLO1FBRHpCLE9BQU87T0FDYSxLQUFLLENBK0J6QjtJQUFELFlBQUM7Q0EvQkQsQUErQkMsQ0EvQmtDLEVBQUUsQ0FBQyxTQUFTLEdBK0I5QztrQkEvQm9CLEtBQUsiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IExhbmd1YWdlIGZyb20gXCIuL0xhbmd1YWdlXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXh0MiBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KClcbiAgICBrZXk6IHN0cmluZyA9ICcnO1xuICAgIGxhYmVsOiBjYy5MYWJlbCB8IGNjLlJpY2hUZXh0ID0gbnVsbDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmdldENvbXBvbmVudChjYy5MYWJlbCk7XG4gICAgICAgIGlmICghdGhpcy5sYWJlbCkge1xuICAgICAgICAgICAgdGhpcy5sYWJlbCA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLlJpY2hUZXh0KTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5sYWJlbCkge1xuICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCh0aGlzLmtleSk7XG4gICAgICAgIH1cblxuICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignTEFOR19DSEFOJywgdGhpcy5vbkxhbmd1YWdlQ2hhbmdlLCB0aGlzKTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxuXG4gICAgb25MYW5ndWFnZUNoYW5nZSgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ29uTGFuZ3VhZ2VDaGFuZ2UnLCB0aGlzLCB0aGlzLmxhYmVsKVxuICAgICAgICBpZiAodGhpcy5sYWJlbCkge1xuICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCh0aGlzLmtleSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkRlc3Ryb3koKSB7XG4gICAgICAgIGNjLnN5c3RlbUV2ZW50Lm9mZignTEFOR19DSEFOJyk7XG4gICAgfVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Leaderboard.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'baa5a1+8bxE/rfm6Qsnw+yf', 'Leaderboard');
// Scripts/Leaderboard.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var LBEntry_1 = require("./LBEntry");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Leaderboard = /** @class */ (function (_super) {
    __extends(Leaderboard, _super);
    function Leaderboard() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.template = null;
        _this.container = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Leaderboard.prototype.start = function () { };
    // update (dt) {}
    Leaderboard.prototype.render = function (rank, name, coin, avatar, playerId) {
        var entry = cc.instantiate(this.template);
        entry.getComponent(LBEntry_1.default).render(rank, name, coin, avatar, playerId);
        this.container.addChild(entry);
        entry.active = true;
    };
    Leaderboard.prototype.onLoadComplete = function () { };
    __decorate([
        property(cc.Node)
    ], Leaderboard.prototype, "template", void 0);
    __decorate([
        property(cc.Node)
    ], Leaderboard.prototype, "container", void 0);
    Leaderboard = __decorate([
        ccclass
    ], Leaderboard);
    return Leaderboard;
}(cc.Component));
exports.default = Leaderboard;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0xlYWRlcmJvYXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLHFDQUFnQztBQUUxQixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUF5QywrQkFBWTtJQUFyRDtRQUFBLHFFQXdCQztRQXRCRyxjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBWSxJQUFJLENBQUM7O0lBbUI5QixDQUFDO0lBakJHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYsMkJBQUssR0FBTCxjQUFVLENBQUM7SUFFWCxpQkFBaUI7SUFFakIsNEJBQU0sR0FBTixVQUFPLElBQVksRUFBRSxJQUFZLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxRQUFnQjtRQUM3RSxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxQyxLQUFLLENBQUMsWUFBWSxDQUFDLGlCQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxvQ0FBYyxHQUFkLGNBQWtCLENBQUM7SUFwQm5CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDUTtJQUxULFdBQVc7UUFEL0IsT0FBTztPQUNhLFdBQVcsQ0F3Qi9CO0lBQUQsa0JBQUM7Q0F4QkQsQUF3QkMsQ0F4QndDLEVBQUUsQ0FBQyxTQUFTLEdBd0JwRDtrQkF4Qm9CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IExCRW50cnkgZnJvbSBcIi4vTEJFbnRyeVwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIExlYWRlcmJvYXJkIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICB0ZW1wbGF0ZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBjb250YWluZXI6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0ICgpIHt9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxuXG4gICAgcmVuZGVyKHJhbms6IG51bWJlciwgbmFtZTogc3RyaW5nLCBjb2luOiBudW1iZXIsIGF2YXRhcjogc3RyaW5nLCBwbGF5ZXJJZDogc3RyaW5nKSB7XG4gICAgICAgIGxldCBlbnRyeSA9IGNjLmluc3RhbnRpYXRlKHRoaXMudGVtcGxhdGUpO1xuICAgICAgICBlbnRyeS5nZXRDb21wb25lbnQoTEJFbnRyeSkucmVuZGVyKHJhbmssIG5hbWUsIGNvaW4sIGF2YXRhciwgcGxheWVySWQpO1xuICAgICAgICB0aGlzLmNvbnRhaW5lci5hZGRDaGlsZChlbnRyeSk7XG4gICAgICAgIGVudHJ5LmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgb25Mb2FkQ29tcGxldGUoKSB7fVxuXG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/EventKeys.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fef6cR3M0xAfLpe1NdULNzR', 'EventKeys');
// Scripts/EventKeys.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventKeys = {
// PLAY_NOW: 'play_now',
// SHOP_GO: 'shop_go',
// VIP_GO: 'vip_go',
// DAILY_GIFT_CLICK: 'daily_gift_click',
// PLAY_MODE: 'play_mode',
// PLAY_ACCEPT: 'play_accept',
// QUIT_GAME: 'quit_game',
// WIN: 'win',
// LOSE: 'lose',
// PLAY_DURATION: 'play_duration',
// PLAY_TIME: 'play_time',
// INVITE_FRIEND: 'invite_friend',
// SPIN: 'spin',
// PLAY_WITH_FRIEND: 'play_with_friend',
// POPUP_ADREWARD_COIN_OPEN: '100k_popup',
// POPUP_ADREWARD_COIN_CLICK: '100k_clickpopup',
// POPUP_ADREWARD_COIN_ERROR: '100k_novideopopup',
// POPUP_ADREWARD_SPIN_OPEN: 'spin_popup',
// POPUP_ADREWARD_SPIN_CLICK: 'spin_click_popup',
// POPUP_ADREWARD_SPIN_ERROR: 'spin_novideo_popup',
// POPUP_DAILY_REWARD_SHOW: 'daily_reward_popup',
// POPUP_DAILY_REWARD_AD: 'daily_reward_click_video',
// POPUP_DAILY_REWARD_AD_ERROR: 'daily_reward_no_video',
};
exports.default = EventKeys;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0V2ZW50S2V5cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sU0FBUyxHQUFHO0FBQ2Qsd0JBQXdCO0FBQ3hCLHNCQUFzQjtBQUN0QixvQkFBb0I7QUFDcEIsd0NBQXdDO0FBQ3hDLDBCQUEwQjtBQUMxQiw4QkFBOEI7QUFDOUIsMEJBQTBCO0FBQzFCLGNBQWM7QUFDZCxnQkFBZ0I7QUFDaEIsa0NBQWtDO0FBQ2xDLDBCQUEwQjtBQUMxQixrQ0FBa0M7QUFDbEMsZ0JBQWdCO0FBQ2hCLHdDQUF3QztBQUN4QywwQ0FBMEM7QUFDMUMsZ0RBQWdEO0FBQ2hELGtEQUFrRDtBQUNsRCwwQ0FBMEM7QUFDMUMsaURBQWlEO0FBQ2pELG1EQUFtRDtBQUNuRCxpREFBaUQ7QUFDakQscURBQXFEO0FBQ3JELHdEQUF3RDtDQUMzRCxDQUFBO0FBRUQsa0JBQWUsU0FBUyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgRXZlbnRLZXlzID0ge1xuICAgIC8vIFBMQVlfTk9XOiAncGxheV9ub3cnLFxuICAgIC8vIFNIT1BfR086ICdzaG9wX2dvJyxcbiAgICAvLyBWSVBfR086ICd2aXBfZ28nLFxuICAgIC8vIERBSUxZX0dJRlRfQ0xJQ0s6ICdkYWlseV9naWZ0X2NsaWNrJyxcbiAgICAvLyBQTEFZX01PREU6ICdwbGF5X21vZGUnLFxuICAgIC8vIFBMQVlfQUNDRVBUOiAncGxheV9hY2NlcHQnLFxuICAgIC8vIFFVSVRfR0FNRTogJ3F1aXRfZ2FtZScsXG4gICAgLy8gV0lOOiAnd2luJyxcbiAgICAvLyBMT1NFOiAnbG9zZScsXG4gICAgLy8gUExBWV9EVVJBVElPTjogJ3BsYXlfZHVyYXRpb24nLFxuICAgIC8vIFBMQVlfVElNRTogJ3BsYXlfdGltZScsXG4gICAgLy8gSU5WSVRFX0ZSSUVORDogJ2ludml0ZV9mcmllbmQnLFxuICAgIC8vIFNQSU46ICdzcGluJyxcbiAgICAvLyBQTEFZX1dJVEhfRlJJRU5EOiAncGxheV93aXRoX2ZyaWVuZCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9PUEVOOiAnMTAwa19wb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9DTElDSzogJzEwMGtfY2xpY2twb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9FUlJPUjogJzEwMGtfbm92aWRlb3BvcHVwJyxcbiAgICAvLyBQT1BVUF9BRFJFV0FSRF9TUElOX09QRU46ICdzcGluX3BvcHVwJyxcbiAgICAvLyBQT1BVUF9BRFJFV0FSRF9TUElOX0NMSUNLOiAnc3Bpbl9jbGlja19wb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfU1BJTl9FUlJPUjogJ3NwaW5fbm92aWRlb19wb3B1cCcsXG4gICAgLy8gUE9QVVBfREFJTFlfUkVXQVJEX1NIT1c6ICdkYWlseV9yZXdhcmRfcG9wdXAnLFxuICAgIC8vIFBPUFVQX0RBSUxZX1JFV0FSRF9BRDogJ2RhaWx5X3Jld2FyZF9jbGlja192aWRlbycsXG4gICAgLy8gUE9QVVBfREFJTFlfUkVXQVJEX0FEX0VSUk9SOiAnZGFpbHlfcmV3YXJkX25vX3ZpZGVvJyxcbn1cblxuZXhwb3J0IGRlZmF1bHQgRXZlbnRLZXlzOyJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Helper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1879dJgJEVIpqZTqzYVoVpF', 'Helper');
// Scripts/Helper.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.isHouse = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (amount != 0 && cards.length != amount)
            return false;
        if (amount == 0 && (cards.length < 2 || cards.length > 4))
            return false;
        var cardMin = Helper.findMinCard(cards);
        var cardMax = Helper.findMaxCard(cards);
        return cardMin.rank == cardMax.rank;
    };
    Helper.isStraight = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (amount != 0 && cards.length != amount)
            return false;
        if (amount == 0 && cards.length < 3)
            return false;
        var cardMin = Helper.findMinCard(cards);
        for (var i = cards.length - 1; i > 0; --i) {
            if (cardMin.rank + i > 14)
                return false;
            var card = Helper.findCardByRank(cards, cardMin.rank + i);
            if (!card)
                return false;
        }
        return true;
    };
    Helper.isMultiPair = function (cards, amount) {
        if (amount === void 0) { amount = 0; }
        if (cards.length < 6 || cards.length % 2 == 1)
            return false;
        if (amount != 0 && cards.length != 2 * amount)
            return false;
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == 15)
                return false;
            var matched = Helper.findCardByRank(cards, cards[i].rank, cards[i]);
            if (!matched)
                return false;
        }
        return true;
    };
    Helper.isBigWin = function (cards) {
        var reduce = cards.reduce(function (accumulator, currentValue) {
            if (!accumulator[currentValue.rank]) {
                accumulator[currentValue.rank] = [];
            }
            accumulator[currentValue.rank].push(currentValue);
            return accumulator;
        }, {});
        if (!!reduce[15] && reduce[15].length == 4)
            return 1;
        if (Helper.isDragon(reduce))
            return 2;
        if (Helper.pairCount(reduce) == 6)
            return 3;
        return 0;
    };
    Helper.isDragon = function (reduce) {
        for (var i = 3; i <= 14; i++) {
            if (!reduce[i]) {
                return false;
            }
        }
        return true;
    };
    Helper.pairCount = function (reduce) {
        var count = 0;
        for (var v in reduce) {
            if (reduce[v].length >= 2)
                count++;
        }
        return count;
    };
    Helper.sort = function (cards) {
        cards.sort(function (a, b) {
            return a.gt(b) ? 1 : -1;
        });
    };
    ;
    // card utlis
    Helper.findCard = function (cards, rank, suit) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var card = cards[i];
            if (card.rank == rank && card.suit == suit)
                return card;
        }
        return null;
    };
    Helper.findMaxCard = function (cards, filter) {
        if (filter === void 0) { filter = null; }
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
            var c = cards[i];
            if (filter == null || c.lt(filter)) {
                if (card == null)
                    card = c;
                else if (c.gt(card))
                    card = c;
            }
        }
        return card;
    };
    Helper.findMinCard = function (cards, filter) {
        if (filter === void 0) { filter = null; }
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
            var c = cards[i];
            if (filter == null || c.gt(filter)) {
                if (card == null)
                    card = c;
                else if (c.lt(card))
                    card = c;
            }
        }
        return card;
    };
    Helper.findAllCardByRange = function (cards, max, min) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank > min && cards[i].rank <= max) {
                if (!Helper.findCardByRank(setCards, cards[i].rank)) {
                    setCards.push(cards[i]);
                }
            }
        }
        return setCards;
    };
    Helper.findAllCardByRank = function (cards, rank, limit) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
                if (setCards.length == limit)
                    return setCards;
            }
        }
        return setCards;
    };
    Helper.findCardByRank = function (cards, rank, filter) {
        if (filter === void 0) { filter = null; }
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank && cards[i] != filter)
                return cards[i];
        }
        return null;
    };
    // array utlis
    Helper.findIndex = function (arr, target) {
        for (var i = arr.length - 1; i >= 0; --i) {
            if (target == arr[i])
                return i;
        }
        return -1;
    };
    Helper.removeBy = function (arr, target) {
        var idx = Helper.findIndex(arr, target);
        Helper.removeAt(arr, idx);
    };
    Helper.removeAt = function (arr, idx) {
        arr.splice(idx, 1);
    };
    Helper.highlight = function (cards, handCards) {
        return [];
    };
    ;
    Helper.findStraightCard = function (cards, filter) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var cardStarted = cards[i];
            if (cardStarted.rank < 15 && cardStarted.gt(filter.highest)) {
                var straight = Helper.findAllCardByRange(cards, cardStarted.rank, cardStarted.rank - filter.count());
                if (straight.length == filter.count()) {
                    return straight;
                }
            }
        }
        return null;
    };
    Helper.findHouseCard = function (cards, filter, limit) {
        for (var i = cards.length - 1; i >= 0; --i) {
            var cardStarted = cards[i];
            if (cardStarted.gt(filter)) {
                var straight = Helper.findAllCardByRank(cards, cardStarted.rank, limit);
                if (straight.length == limit) {
                    return straight;
                }
            }
        }
        return null;
    };
    Helper.findMultiPairCard = function (cards, filter, limit) {
        // let reduce = cards.reduce(function(accumulator: object, currentValue: Card) {
        //     if (!accumulator[currentValue.rank]) {
        //         accumulator[currentValue.rank] = [];
        //     }
        //     accumulator[currentValue.rank].push(currentValue);
        //     return accumulator;
        // }, {});
        // for (let i = 0; i < cards.length; i++) {
        //     let card = cards[i];
        //     if (card.gt(filter)) {
        //         let multiPairs = [];
        //         for(let j = 0; j < limit; j++) {
        //             let arr: Array<Card> = reduce[card.rank - j];
        //             if (arr.length == 1) break;
        //             multiPairs.push();
        //         }
        //     }
        // }
        return null;
    };
    // caculate
    Helper.calculate = function (cards) {
        var pigs = Helper.findAllCardByRank(cards, 15, 4);
        var spotWin = Helper.calculateSpotWin(pigs);
        return cards.length + spotWin;
    };
    Helper.calculateSpotWin = function (pigs) {
        var total = 0;
        for (var i = pigs.length - 1; i >= 0; --i) {
            total += pigs[i].suit + 2;
        }
        return total;
    };
    return Helper;
}());
exports.default = Helper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0hlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUdBO0lBQUE7SUE0T0EsQ0FBQztJQTFPVSxjQUFPLEdBQWQsVUFBZSxLQUFhLEVBQUUsTUFBa0I7UUFBbEIsdUJBQUEsRUFBQSxVQUFrQjtRQUM1QyxJQUFJLE1BQU0sSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxNQUFNO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFFeEQsSUFBSSxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUV4RSxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hDLElBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFeEMsT0FBTyxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDeEMsQ0FBQztJQUVNLGlCQUFVLEdBQWpCLFVBQWtCLEtBQWEsRUFBRSxNQUFrQjtRQUFsQix1QkFBQSxFQUFBLFVBQWtCO1FBQy9DLElBQUksTUFBTSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLE1BQU07WUFBRSxPQUFPLEtBQUssQ0FBQztRQUV4RCxJQUFJLE1BQU0sSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFFbEQsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUV4QyxLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxFQUFFO2dCQUFFLE9BQU8sS0FBSyxDQUFDO1lBRXhDLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLElBQUk7Z0JBQUUsT0FBTyxLQUFLLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sa0JBQVcsR0FBbEIsVUFBbUIsS0FBYSxFQUFFLE1BQWtCO1FBQWxCLHVCQUFBLEVBQUEsVUFBa0I7UUFDaEQsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFFNUQsSUFBSSxNQUFNLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxHQUFHLE1BQU07WUFBRSxPQUFPLEtBQUssQ0FBQztRQUU1RCxLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQUUsT0FBTyxLQUFLLENBQUM7WUFDdEMsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsT0FBTztnQkFBRSxPQUFPLEtBQUssQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxlQUFRLEdBQWYsVUFBZ0IsS0FBYTtRQUN6QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsV0FBbUIsRUFBRSxZQUFrQjtZQUN2RSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDakMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDdkM7WUFDRCxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRCxPQUFPLFdBQVcsQ0FBQztRQUN2QixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFUCxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDckQsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3RDLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFFNUMsT0FBTyxDQUFDLENBQUM7SUFDYixDQUFDO0lBRU0sZUFBUSxHQUFmLFVBQWdCLE1BQWM7UUFDMUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNaLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sZ0JBQVMsR0FBaEIsVUFBaUIsTUFBYztRQUMzQixJQUFJLEtBQUssR0FBVyxDQUFDLENBQUM7UUFDdEIsS0FBSyxJQUFJLENBQUMsSUFBSSxNQUFNLEVBQUU7WUFDbEIsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUM7Z0JBQUUsS0FBSyxFQUFFLENBQUM7U0FDdEM7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU0sV0FBSSxHQUFYLFVBQVksS0FBYTtRQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUM7WUFDckIsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUFBLENBQUM7SUFFRixhQUFhO0lBQ04sZUFBUSxHQUFmLFVBQWdCLEtBQWEsRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUNyRCxLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJO2dCQUFHLE9BQU8sSUFBSSxDQUFDO1NBQzVEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLGtCQUFXLEdBQWxCLFVBQW1CLEtBQWEsRUFBRSxNQUFtQjtRQUFuQix1QkFBQSxFQUFBLGFBQW1CO1FBQ2pELElBQUksSUFBSSxHQUFTLElBQUksQ0FBQztRQUN0QixLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksTUFBTSxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoQyxJQUFJLElBQUksSUFBSSxJQUFJO29CQUNaLElBQUksR0FBRyxDQUFDLENBQUM7cUJBQ1IsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDZixJQUFJLEdBQUcsQ0FBQyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sa0JBQVcsR0FBbEIsVUFBbUIsS0FBYSxFQUFFLE1BQW1CO1FBQW5CLHVCQUFBLEVBQUEsYUFBbUI7UUFDakQsSUFBSSxJQUFJLEdBQVMsSUFBSSxDQUFDO1FBQ3RCLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN4QyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakIsSUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2hDLElBQUksSUFBSSxJQUFJLElBQUk7b0JBQ1osSUFBSSxHQUFHLENBQUMsQ0FBQztxQkFDUixJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUNmLElBQUksR0FBRyxDQUFDLENBQUM7YUFDaEI7U0FFSjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSx5QkFBa0IsR0FBekIsVUFBMEIsS0FBYSxFQUFFLEdBQVcsRUFBRSxHQUFXO1FBQzdELElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNsQixLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEdBQUcsRUFBRTtnQkFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDakQsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDM0I7YUFDSjtTQUNKO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVNLHdCQUFpQixHQUF4QixVQUF5QixLQUFhLEVBQUUsSUFBWSxFQUFFLEtBQWE7UUFDL0QsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN4QyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUN2QixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksS0FBSztvQkFDeEIsT0FBTyxRQUFRLENBQUM7YUFDdkI7U0FDSjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFTSxxQkFBYyxHQUFyQixVQUFzQixLQUFhLEVBQUUsSUFBWSxFQUFFLE1BQW1CO1FBQW5CLHVCQUFBLEVBQUEsYUFBbUI7UUFDbEUsS0FBSyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3hDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU07Z0JBQUUsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEU7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsY0FBYztJQUNQLGdCQUFTLEdBQWhCLFVBQWlCLEdBQWtCLEVBQUUsTUFBTTtRQUN2QyxLQUFLLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDdEMsSUFBSSxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFBRSxPQUFPLENBQUMsQ0FBQztTQUNsQztRQUNELE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDZCxDQUFDO0lBRU0sZUFBUSxHQUFmLFVBQWdCLEdBQWtCLEVBQUUsTUFBTTtRQUN0QyxJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN4QyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRU0sZUFBUSxHQUFmLFVBQWdCLEdBQWtCLEVBQUUsR0FBVztRQUMzQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBRU0sZ0JBQVMsR0FBaEIsVUFBaUIsS0FBYSxFQUFFLFNBQWlCO1FBQzdDLE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUFBLENBQUM7SUFFSyx1QkFBZ0IsR0FBdkIsVUFBd0IsS0FBYSxFQUFFLE1BQWlCO1FBQ3BELEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN4QyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxXQUFXLENBQUMsSUFBSSxHQUFHLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDekQsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ3JHLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBQ25DLE9BQU8sUUFBUSxDQUFDO2lCQUNuQjthQUNKO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sb0JBQWEsR0FBcEIsVUFBcUIsS0FBYSxFQUFFLE1BQVksRUFBRSxLQUFhO1FBQzNELEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN4QyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUN4QixJQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3hFLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7b0JBQzFCLE9BQU8sUUFBUSxDQUFDO2lCQUNuQjthQUNKO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sd0JBQWlCLEdBQXhCLFVBQXlCLEtBQWEsRUFBRSxNQUFZLEVBQUUsS0FBYTtRQUMvRCxnRkFBZ0Y7UUFDaEYsNkNBQTZDO1FBQzdDLCtDQUErQztRQUMvQyxRQUFRO1FBQ1IseURBQXlEO1FBQ3pELDBCQUEwQjtRQUMxQixVQUFVO1FBRVYsMkNBQTJDO1FBQzNDLDJCQUEyQjtRQUMzQiw2QkFBNkI7UUFDN0IsK0JBQStCO1FBQy9CLDJDQUEyQztRQUMzQyw0REFBNEQ7UUFDNUQsMENBQTBDO1FBQzFDLGlDQUFpQztRQUNqQyxZQUFZO1FBQ1osUUFBUTtRQUNSLElBQUk7UUFDSixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsV0FBVztJQUNKLGdCQUFTLEdBQWhCLFVBQWlCLEtBQWE7UUFDMUIsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7SUFDbEMsQ0FBQztJQUVNLHVCQUFnQixHQUF2QixVQUF3QixJQUFZO1FBQ2hDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN2QyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7U0FDN0I7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBQ0wsYUFBQztBQUFELENBNU9BLEFBNE9DLElBQUEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2FyZCBmcm9tIFwiLi9DYXJkXCI7XG5pbXBvcnQgQ2FyZEdyb3VwIGZyb20gXCIuL0NhcmRHcm91cFwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBIZWxwZXIge1xuXG4gICAgc3RhdGljIGlzSG91c2UoY2FyZHM6IENhcmRbXSwgYW1vdW50OiBudW1iZXIgPSAwKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChhbW91bnQgIT0gMCAmJiBjYXJkcy5sZW5ndGggIT0gYW1vdW50KSByZXR1cm4gZmFsc2U7XG5cbiAgICAgICAgaWYgKGFtb3VudCA9PSAwICYmIChjYXJkcy5sZW5ndGggPCAyIHx8IGNhcmRzLmxlbmd0aCA+IDQpKSByZXR1cm4gZmFsc2U7XG5cbiAgICAgICAgbGV0IGNhcmRNaW4gPSBIZWxwZXIuZmluZE1pbkNhcmQoY2FyZHMpO1xuICAgICAgICBsZXQgY2FyZE1heCA9IEhlbHBlci5maW5kTWF4Q2FyZChjYXJkcyk7XG5cbiAgICAgICAgcmV0dXJuIGNhcmRNaW4ucmFuayA9PSBjYXJkTWF4LnJhbms7XG4gICAgfVxuXG4gICAgc3RhdGljIGlzU3RyYWlnaHQoY2FyZHM6IENhcmRbXSwgYW1vdW50OiBudW1iZXIgPSAwKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChhbW91bnQgIT0gMCAmJiBjYXJkcy5sZW5ndGggIT0gYW1vdW50KSByZXR1cm4gZmFsc2U7XG5cbiAgICAgICAgaWYgKGFtb3VudCA9PSAwICYmIGNhcmRzLmxlbmd0aCA8IDMpIHJldHVybiBmYWxzZTtcblxuICAgICAgICBsZXQgY2FyZE1pbiA9IEhlbHBlci5maW5kTWluQ2FyZChjYXJkcyk7XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmxlbmd0aCAtIDE7IGkgPiAwOyAtLWkpIHtcbiAgICAgICAgICAgIGlmIChjYXJkTWluLnJhbmsgKyBpID4gMTQpIHJldHVybiBmYWxzZTtcblxuICAgICAgICAgICAgbGV0IGNhcmQgPSBIZWxwZXIuZmluZENhcmRCeVJhbmsoY2FyZHMsIGNhcmRNaW4ucmFuayArIGkpO1xuICAgICAgICAgICAgaWYgKCFjYXJkKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBzdGF0aWMgaXNNdWx0aVBhaXIoY2FyZHM6IENhcmRbXSwgYW1vdW50OiBudW1iZXIgPSAwKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChjYXJkcy5sZW5ndGggPCA2IHx8IGNhcmRzLmxlbmd0aCAlIDIgPT0gMSkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgIGlmIChhbW91bnQgIT0gMCAmJiBjYXJkcy5sZW5ndGggIT0gMiAqIGFtb3VudCkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGNhcmRzW2ldLnJhbmsgPT0gMTUpIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIGxldCBtYXRjaGVkID0gSGVscGVyLmZpbmRDYXJkQnlSYW5rKGNhcmRzLCBjYXJkc1tpXS5yYW5rLCBjYXJkc1tpXSk7XG4gICAgICAgICAgICBpZiAoIW1hdGNoZWQpIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc0JpZ1dpbihjYXJkczogQ2FyZFtdKSB7XG4gICAgICAgIGxldCByZWR1Y2UgPSBjYXJkcy5yZWR1Y2UoZnVuY3Rpb24gKGFjY3VtdWxhdG9yOiBvYmplY3QsIGN1cnJlbnRWYWx1ZTogQ2FyZCkge1xuICAgICAgICAgICAgaWYgKCFhY2N1bXVsYXRvcltjdXJyZW50VmFsdWUucmFua10pIHtcbiAgICAgICAgICAgICAgICBhY2N1bXVsYXRvcltjdXJyZW50VmFsdWUucmFua10gPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFjY3VtdWxhdG9yW2N1cnJlbnRWYWx1ZS5yYW5rXS5wdXNoKGN1cnJlbnRWYWx1ZSk7XG4gICAgICAgICAgICByZXR1cm4gYWNjdW11bGF0b3I7XG4gICAgICAgIH0sIHt9KTtcblxuICAgICAgICBpZiAoISFyZWR1Y2VbMTVdICYmIHJlZHVjZVsxNV0ubGVuZ3RoID09IDQpIHJldHVybiAxO1xuICAgICAgICBpZiAoSGVscGVyLmlzRHJhZ29uKHJlZHVjZSkpIHJldHVybiAyO1xuICAgICAgICBpZiAoSGVscGVyLnBhaXJDb3VudChyZWR1Y2UpID09IDYpIHJldHVybiAzO1xuXG4gICAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc0RyYWdvbihyZWR1Y2U6IG9iamVjdCk6IGJvb2xlYW4ge1xuICAgICAgICBmb3IgKGxldCBpID0gMzsgaSA8PSAxNDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoIXJlZHVjZVtpXSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcGFpckNvdW50KHJlZHVjZTogb2JqZWN0KTogbnVtYmVyIHtcbiAgICAgICAgbGV0IGNvdW50OiBudW1iZXIgPSAwO1xuICAgICAgICBmb3IgKGxldCB2IGluIHJlZHVjZSkge1xuICAgICAgICAgICAgaWYgKHJlZHVjZVt2XS5sZW5ndGggPj0gMikgY291bnQrKztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY291bnQ7XG4gICAgfVxuXG4gICAgc3RhdGljIHNvcnQoY2FyZHM6IENhcmRbXSkge1xuICAgICAgICBjYXJkcy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgICAgICByZXR1cm4gYS5ndChiKSA/IDEgOiAtMTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIC8vIGNhcmQgdXRsaXNcbiAgICBzdGF0aWMgZmluZENhcmQoY2FyZHM6IENhcmRbXSwgcmFuazogbnVtYmVyLCBzdWl0OiBudW1iZXIpOiBDYXJkIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBsZXQgY2FyZCA9IGNhcmRzW2ldO1xuICAgICAgICAgICAgaWYgKGNhcmQucmFuayA9PSByYW5rICYmIGNhcmQuc3VpdCA9PSBzdWl0KSAgcmV0dXJuIGNhcmQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc3RhdGljIGZpbmRNYXhDYXJkKGNhcmRzOiBDYXJkW10sIGZpbHRlcjogQ2FyZCA9IG51bGwpOiBDYXJkIHtcbiAgICAgICAgbGV0IGNhcmQ6IENhcmQgPSBudWxsO1xuICAgICAgICBmb3IgKGxldCBpID0gY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGxldCBjID0gY2FyZHNbaV07XG4gICAgICAgICAgICBpZiAoZmlsdGVyID09IG51bGwgfHwgYy5sdChmaWx0ZXIpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGNhcmQgPT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgY2FyZCA9IGM7XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoYy5ndChjYXJkKSlcbiAgICAgICAgICAgICAgICAgICAgY2FyZCA9IGM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNhcmQ7XG4gICAgfVxuXG4gICAgc3RhdGljIGZpbmRNaW5DYXJkKGNhcmRzOiBDYXJkW10sIGZpbHRlcjogQ2FyZCA9IG51bGwpOiBDYXJkIHtcbiAgICAgICAgbGV0IGNhcmQ6IENhcmQgPSBudWxsO1xuICAgICAgICBmb3IgKGxldCBpID0gY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGxldCBjID0gY2FyZHNbaV07XG4gICAgICAgICAgICBpZiAoZmlsdGVyID09IG51bGwgfHwgYy5ndChmaWx0ZXIpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGNhcmQgPT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgY2FyZCA9IGM7XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoYy5sdChjYXJkKSlcbiAgICAgICAgICAgICAgICAgICAgY2FyZCA9IGM7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY2FyZDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZEFsbENhcmRCeVJhbmdlKGNhcmRzOiBDYXJkW10sIG1heDogbnVtYmVyLCBtaW46IG51bWJlcikge1xuICAgICAgICBsZXQgc2V0Q2FyZHMgPSBbXTtcbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBpZiAoY2FyZHNbaV0ucmFuayA+IG1pbiAmJiBjYXJkc1tpXS5yYW5rIDw9IG1heCkge1xuICAgICAgICAgICAgICAgIGlmICghSGVscGVyLmZpbmRDYXJkQnlSYW5rKHNldENhcmRzLCBjYXJkc1tpXS5yYW5rKSkge1xuICAgICAgICAgICAgICAgICAgICBzZXRDYXJkcy5wdXNoKGNhcmRzW2ldKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNldENhcmRzO1xuICAgIH1cblxuICAgIHN0YXRpYyBmaW5kQWxsQ2FyZEJ5UmFuayhjYXJkczogQ2FyZFtdLCByYW5rOiBudW1iZXIsIGxpbWl0OiBudW1iZXIpOiBDYXJkW10ge1xuICAgICAgICBsZXQgc2V0Q2FyZHMgPSBbXTtcbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBpZiAoY2FyZHNbaV0ucmFuayA9PSByYW5rKSB7XG4gICAgICAgICAgICAgICAgc2V0Q2FyZHMucHVzaChjYXJkc1tpXSk7XG4gICAgICAgICAgICAgICAgaWYgKHNldENhcmRzLmxlbmd0aCA9PSBsaW1pdClcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNldENhcmRzO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzZXRDYXJkcztcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZENhcmRCeVJhbmsoY2FyZHM6IENhcmRbXSwgcmFuazogbnVtYmVyLCBmaWx0ZXI6IENhcmQgPSBudWxsKTogQ2FyZCB7XG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGNhcmRzW2ldLnJhbmsgPT0gcmFuayAmJiBjYXJkc1tpXSAhPSBmaWx0ZXIpIHJldHVybiBjYXJkc1tpXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICAvLyBhcnJheSB1dGxpc1xuICAgIHN0YXRpYyBmaW5kSW5kZXgoYXJyOiBBcnJheTxvYmplY3Q+LCB0YXJnZXQpOiBudW1iZXIge1xuICAgICAgICBmb3IgKGxldCBpID0gYXJyLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBpZiAodGFyZ2V0ID09IGFycltpXSkgcmV0dXJuIGk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH1cblxuICAgIHN0YXRpYyByZW1vdmVCeShhcnI6IEFycmF5PG9iamVjdD4sIHRhcmdldCkge1xuICAgICAgICBsZXQgaWR4ID0gSGVscGVyLmZpbmRJbmRleChhcnIsIHRhcmdldCk7XG4gICAgICAgIEhlbHBlci5yZW1vdmVBdChhcnIsIGlkeCk7XG4gICAgfVxuXG4gICAgc3RhdGljIHJlbW92ZUF0KGFycjogQXJyYXk8b2JqZWN0PiwgaWR4OiBudW1iZXIpIHtcbiAgICAgICAgYXJyLnNwbGljZShpZHgsIDEpO1xuICAgIH1cblxuICAgIHN0YXRpYyBoaWdobGlnaHQoY2FyZHM6IENhcmRbXSwgaGFuZENhcmRzOiBDYXJkW10pIHtcbiAgICAgICAgcmV0dXJuIFtdO1xuICAgIH07XG5cbiAgICBzdGF0aWMgZmluZFN0cmFpZ2h0Q2FyZChjYXJkczogQ2FyZFtdLCBmaWx0ZXI6IENhcmRHcm91cCk6IEFycmF5PENhcmQ+IHtcbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBsZXQgY2FyZFN0YXJ0ZWQgPSBjYXJkc1tpXTtcbiAgICAgICAgICAgIGlmIChjYXJkU3RhcnRlZC5yYW5rIDwgMTUgJiYgY2FyZFN0YXJ0ZWQuZ3QoZmlsdGVyLmhpZ2hlc3QpKSB7XG4gICAgICAgICAgICAgICAgbGV0IHN0cmFpZ2h0ID0gSGVscGVyLmZpbmRBbGxDYXJkQnlSYW5nZShjYXJkcywgY2FyZFN0YXJ0ZWQucmFuaywgY2FyZFN0YXJ0ZWQucmFuayAtIGZpbHRlci5jb3VudCgpKTtcbiAgICAgICAgICAgICAgICBpZiAoc3RyYWlnaHQubGVuZ3RoID09IGZpbHRlci5jb3VudCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc3RhdGljIGZpbmRIb3VzZUNhcmQoY2FyZHM6IENhcmRbXSwgZmlsdGVyOiBDYXJkLCBsaW1pdDogbnVtYmVyKTogQXJyYXk8Q2FyZD4ge1xuICAgICAgICBmb3IgKGxldCBpID0gY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGxldCBjYXJkU3RhcnRlZCA9IGNhcmRzW2ldO1xuICAgICAgICAgICAgaWYgKGNhcmRTdGFydGVkLmd0KGZpbHRlcikpIHtcbiAgICAgICAgICAgICAgICBsZXQgc3RyYWlnaHQgPSBIZWxwZXIuZmluZEFsbENhcmRCeVJhbmsoY2FyZHMsIGNhcmRTdGFydGVkLnJhbmssIGxpbWl0KTtcbiAgICAgICAgICAgICAgICBpZiAoc3RyYWlnaHQubGVuZ3RoID09IGxpbWl0KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc3RhdGljIGZpbmRNdWx0aVBhaXJDYXJkKGNhcmRzOiBDYXJkW10sIGZpbHRlcjogQ2FyZCwgbGltaXQ6IG51bWJlcik6IEFycmF5PENhcmQ+IHtcbiAgICAgICAgLy8gbGV0IHJlZHVjZSA9IGNhcmRzLnJlZHVjZShmdW5jdGlvbihhY2N1bXVsYXRvcjogb2JqZWN0LCBjdXJyZW50VmFsdWU6IENhcmQpIHtcbiAgICAgICAgLy8gICAgIGlmICghYWNjdW11bGF0b3JbY3VycmVudFZhbHVlLnJhbmtdKSB7XG4gICAgICAgIC8vICAgICAgICAgYWNjdW11bGF0b3JbY3VycmVudFZhbHVlLnJhbmtdID0gW107XG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vICAgICBhY2N1bXVsYXRvcltjdXJyZW50VmFsdWUucmFua10ucHVzaChjdXJyZW50VmFsdWUpO1xuICAgICAgICAvLyAgICAgcmV0dXJuIGFjY3VtdWxhdG9yO1xuICAgICAgICAvLyB9LCB7fSk7XG5cbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBjYXJkcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IGNhcmQgPSBjYXJkc1tpXTtcbiAgICAgICAgLy8gICAgIGlmIChjYXJkLmd0KGZpbHRlcikpIHtcbiAgICAgICAgLy8gICAgICAgICBsZXQgbXVsdGlQYWlycyA9IFtdO1xuICAgICAgICAvLyAgICAgICAgIGZvcihsZXQgaiA9IDA7IGogPCBsaW1pdDsgaisrKSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIGxldCBhcnI6IEFycmF5PENhcmQ+ID0gcmVkdWNlW2NhcmQucmFuayAtIGpdO1xuICAgICAgICAvLyAgICAgICAgICAgICBpZiAoYXJyLmxlbmd0aCA9PSAxKSBicmVhaztcbiAgICAgICAgLy8gICAgICAgICAgICAgbXVsdGlQYWlycy5wdXNoKCk7XG4gICAgICAgIC8vICAgICAgICAgfVxuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIC8vIGNhY3VsYXRlXG4gICAgc3RhdGljIGNhbGN1bGF0ZShjYXJkczogQ2FyZFtdKTogbnVtYmVyIHtcbiAgICAgICAgbGV0IHBpZ3MgPSBIZWxwZXIuZmluZEFsbENhcmRCeVJhbmsoY2FyZHMsIDE1LCA0KTtcbiAgICAgICAgbGV0IHNwb3RXaW4gPSBIZWxwZXIuY2FsY3VsYXRlU3BvdFdpbihwaWdzKTtcbiAgICAgICAgcmV0dXJuIGNhcmRzLmxlbmd0aCArIHNwb3RXaW47XG4gICAgfVxuXG4gICAgc3RhdGljIGNhbGN1bGF0ZVNwb3RXaW4ocGlnczogQ2FyZFtdKTogbnVtYmVyIHtcbiAgICAgICAgbGV0IHRvdGFsID0gMDtcbiAgICAgICAgZm9yIChsZXQgaSA9IHBpZ3MubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIHRvdGFsICs9IHBpZ3NbaV0uc3VpdCArIDI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRvdGFsO1xuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Timer.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd1e7cbJml9KPo0i8Y/KcUS7', 'Timer');
// Scripts/Timer.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Timer = /** @class */ (function (_super) {
    __extends(Timer, _super);
    function Timer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sprite = null;
        _this.timer = 0;
        _this.duration = 0;
        _this.completed = null;
        _this._selectorTarget = null;
        _this.target = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Timer.prototype.onLoad = function () {
        this.sprite = this.getComponent(cc.Sprite);
    };
    Timer.prototype.start = function () {
    };
    Timer.prototype.update = function (dt) {
        this.timer += dt;
        if (this.timer < this.duration) {
            this.sprite.fillRange = this.timer / this.duration;
            return;
        }
        this.node.active = false;
        if (this.completed != null) {
            this.completed.call(this._selectorTarget, this.target);
        }
    };
    Timer.prototype.onCompleted = function (selector, selectorTarget, target) {
        this.completed = selector;
        this.target = target;
        this._selectorTarget = selectorTarget;
    };
    Timer.prototype.show = function (time) {
        this.duration = time;
        this.timer = 0;
        this.node.active = true;
    };
    Timer.prototype.hide = function () {
        this.node.active = false;
    };
    Timer = __decorate([
        ccclass
    ], Timer);
    return Timer;
}(cc.Component));
exports.default = Timer;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1RpbWVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsa0ZBQWtGO0FBQ2xGLHlGQUF5RjtBQUN6RixtQkFBbUI7QUFDbkIsNEZBQTRGO0FBQzVGLG1HQUFtRztBQUNuRyw4QkFBOEI7QUFDOUIsNEZBQTRGO0FBQzVGLG1HQUFtRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTdGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW1DLHlCQUFZO0lBQS9DO1FBQUEscUVBNkNDO1FBNUNHLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFDekIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFDM0IscUJBQWUsR0FBUSxJQUFJLENBQUM7UUFDNUIsWUFBTSxHQUFRLElBQUksQ0FBQzs7SUF1Q3ZCLENBQUM7SUF0Q0csd0JBQXdCO0lBRXhCLHNCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxxQkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELHNCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDakIsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ25ELE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzFEO0lBQ0wsQ0FBQztJQUVELDJCQUFXLEdBQVgsVUFBWSxRQUFrQixFQUFFLGNBQW1CLEVBQUUsTUFBVztRQUM1RCxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsZUFBZSxHQUFFLGNBQWMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsb0JBQUksR0FBSixVQUFLLElBQVk7UUFDYixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBRUQsb0JBQUksR0FBSjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBNUNnQixLQUFLO1FBRHpCLE9BQU87T0FDYSxLQUFLLENBNkN6QjtJQUFELFlBQUM7Q0E3Q0QsQUE2Q0MsQ0E3Q2tDLEVBQUUsQ0FBQyxTQUFTLEdBNkM5QztrQkE3Q29CLEtBQUsiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGltZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIHNwcml0ZTogY2MuU3ByaXRlID0gbnVsbDtcbiAgICB0aW1lcjogbnVtYmVyID0gMDtcbiAgICBkdXJhdGlvbjogbnVtYmVyID0gMDtcbiAgICBjb21wbGV0ZWQ6IEZ1bmN0aW9uID0gbnVsbDtcbiAgICBfc2VsZWN0b3JUYXJnZXQ6IGFueSA9IG51bGw7XG4gICAgdGFyZ2V0OiBhbnkgPSBudWxsO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB0aGlzLnNwcml0ZSA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSk7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG5cbiAgICB9XG5cbiAgICB1cGRhdGUoZHQpIHtcbiAgICAgICAgdGhpcy50aW1lciArPSBkdDtcbiAgICAgICAgaWYgKHRoaXMudGltZXIgPCB0aGlzLmR1cmF0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLnNwcml0ZS5maWxsUmFuZ2UgPSB0aGlzLnRpbWVyIC8gdGhpcy5kdXJhdGlvbjtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgaWYgKHRoaXMuY29tcGxldGVkICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMuY29tcGxldGVkLmNhbGwodGhpcy5fc2VsZWN0b3JUYXJnZXQsIHRoaXMudGFyZ2V0KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ29tcGxldGVkKHNlbGVjdG9yOiBGdW5jdGlvbiwgc2VsZWN0b3JUYXJnZXQ6IGFueSwgdGFyZ2V0OiBhbnkpIHtcbiAgICAgICAgdGhpcy5jb21wbGV0ZWQgPSBzZWxlY3RvcjtcbiAgICAgICAgdGhpcy50YXJnZXQgPSB0YXJnZXQ7XG4gICAgICAgIHRoaXMuX3NlbGVjdG9yVGFyZ2V0PSBzZWxlY3RvclRhcmdldDtcbiAgICB9XG5cbiAgICBzaG93KHRpbWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLmR1cmF0aW9uID0gdGltZTtcbiAgICAgICAgdGhpcy50aW1lciA9IDA7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/popop/bonus/DailyBonus.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2f9e8XOO6BHzap8qYPJCMDb', 'DailyBonus');
// Scripts/popop/bonus/DailyBonus.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Popup_1 = require("../../Popup");
var Config_1 = require("../../Config");
var DailyBonusItem_1 = require("./DailyBonusItem");
var Api_1 = require("../../Api");
var AutoHide_1 = require("../../AutoHide");
var util_1 = require("../../util");
var EventKeys_1 = require("../../EventKeys");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DailyBonus = /** @class */ (function (_super) {
    __extends(DailyBonus, _super);
    function DailyBonus() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.toast = null;
        _this.days = [];
        _this.effect = null;
        _this.current = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    DailyBonus.prototype.start = function () {
    };
    // update (dt) {}
    DailyBonus.prototype.show = function (day, claimed) {
        if (claimed === void 0) { claimed = false; }
        if (!claimed) {
            day++;
        }
        if (day > Config_1.default.dailyBonus.length) {
            day = 1;
        }
        this.current = day;
        this.popup.open(this.node, 2);
        for (var i = 0; i < this.days.length; i++) {
            var d = i + 1;
            var claimable = (d == day) && !claimed;
            if (d < day) {
                this.days[i].setClaimable(claimable, true);
            }
            else if (d == day) {
                this.days[i].setClaimable(claimable, claimed);
            }
            else {
                this.days[i].setClaimable(claimable, false);
            }
        }
    };
    DailyBonus.prototype.onClaim = function (sender, day) {
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        this.onClaimCompl(sender, date, false);
        cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], false);
        this.popup.close(this.node, 2);
    };
    DailyBonus.prototype.onClaimExtra = function (sender, day) {
        var _this = this;
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD);
        Api_1.default.showRewardedVideo(function () {
            _this.onClaimCompl(sender, date, false);
            cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], true);
            _this.popup.close(_this.node, 2);
        }, function () {
            cc.systemEvent.emit('claim_daily_bonus_fail');
            Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD_ERROR);
            _this.popup.close(_this.node, 2);
        });
    };
    DailyBonus.prototype.onClaimCompl = function (sender, day, double) {
        var gift = Config_1.default.dailyBonus[day - 1];
        if (!gift.coin) {
            return;
        }
        var e = cc.instantiate(this.effect);
        e.active = true;
        e.position = sender.target.position;
        e.scale = 1;
        e.parent = cc.director.getScene().getChildByName("Canvas");
        var coinBonus = double ? gift.coin * 2 : gift.coin;
        e.getComponent(cc.Label).string = util_1.default.numberFormat(coinBonus);
        e.runAction(cc.sequence(cc.moveBy(1.2, 0, 250), cc.removeSelf()));
    };
    __decorate([
        property(Popup_1.default)
    ], DailyBonus.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], DailyBonus.prototype, "toast", void 0);
    __decorate([
        property(DailyBonusItem_1.default)
    ], DailyBonus.prototype, "days", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonus.prototype, "effect", void 0);
    DailyBonus = __decorate([
        ccclass
    ], DailyBonus);
    return DailyBonus;
}(cc.Component));
exports.default = DailyBonus;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3BvcG9wL2JvbnVzL0RhaWx5Qm9udXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYscUNBQWdDO0FBQ2hDLHVDQUFrQztBQUNsQyxtREFBOEM7QUFDOUMsaUNBQTRCO0FBQzVCLDJDQUFzQztBQUN0QyxtQ0FBOEI7QUFDOUIsNkNBQXdDO0FBRWxDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXdDLDhCQUFZO0lBQXBEO1FBQUEscUVBZ0ZDO1FBOUVHLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQXFCLEVBQUUsQ0FBQztRQUU1QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBQ3ZCLGFBQU8sR0FBVyxDQUFDLENBQUM7O0lBdUV4QixDQUFDO0lBdEVHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYsMEJBQUssR0FBTDtJQUVBLENBQUM7SUFFRCxpQkFBaUI7SUFFakIseUJBQUksR0FBSixVQUFLLEdBQVcsRUFBRSxPQUF3QjtRQUF4Qix3QkFBQSxFQUFBLGVBQXdCO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBQyxHQUFHLEVBQUUsQ0FBQztTQUFDO1FBQ3RCLElBQUksR0FBRyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtZQUNoQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFNLENBQUMsR0FBRyxDQUFDLEdBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBTSxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUNULElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM5QztpQkFBTSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNqRDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDL0M7U0FDSjtJQUNMLENBQUM7SUFFRCw0QkFBTyxHQUFQLFVBQVEsTUFBZSxFQUFFLEdBQVc7UUFDaEMsSUFBSSxhQUFHLENBQUMsaUJBQWlCO1lBQUUsT0FBTztRQUNsQyxJQUFNLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNoRCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDdkMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLGdCQUFNLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsTUFBZSxFQUFFLEdBQVc7UUFBekMsaUJBYUM7UUFaRyxJQUFJLGFBQUcsQ0FBQyxpQkFBaUI7WUFBRSxPQUFPO1FBQ2xDLElBQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2hELGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlDLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdkMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLGdCQUFNLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRixLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsRUFBRTtZQUNDLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDOUMsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDcEQsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsTUFBTSxFQUFFLEdBQVcsRUFBRSxNQUFlO1FBQzdDLElBQU0sSUFBSSxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNaLE9BQU07U0FDVDtRQUNELElBQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUNuQixFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQ3RCLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FDbEIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQTdFRDtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7NkNBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsa0JBQVEsQ0FBQzs2Q0FDSTtJQUV2QjtRQURDLFFBQVEsQ0FBQyx3QkFBYyxDQUFDOzRDQUNHO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ0s7SUFSTixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBZ0Y5QjtJQUFELGlCQUFDO0NBaEZELEFBZ0ZDLENBaEZ1QyxFQUFFLENBQUMsU0FBUyxHQWdGbkQ7a0JBaEZvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBQb3B1cCBmcm9tIFwiLi4vLi4vUG9wdXBcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4uLy4uL0NvbmZpZ1wiO1xuaW1wb3J0IERhaWx5Qm9udXNJdGVtIGZyb20gXCIuL0RhaWx5Qm9udXNJdGVtXCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuLi8uLi9BcGlcIjtcbmltcG9ydCBBdXRvSGlkZSBmcm9tIFwiLi4vLi4vQXV0b0hpZGVcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuLi8uLi91dGlsXCI7XG5pbXBvcnQgRXZlbnRLZXlzIGZyb20gXCIuLi8uLi9FdmVudEtleXNcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERhaWx5Qm9udXMgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShBdXRvSGlkZSlcbiAgICB0b2FzdDogQXV0b0hpZGUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShEYWlseUJvbnVzSXRlbSlcbiAgICBkYXlzOiBEYWlseUJvbnVzSXRlbVtdID0gW107XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBjdXJyZW50OiBudW1iZXIgPSAwO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaG93KGRheTogbnVtYmVyLCBjbGFpbWVkOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgaWYgKCFjbGFpbWVkKSB7ZGF5Kys7fVxuICAgICAgICBpZiAoZGF5ID4gQ29uZmlnLmRhaWx5Qm9udXMubGVuZ3RoKSB7XG4gICAgICAgICAgICBkYXkgPSAxO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY3VycmVudCA9IGRheTtcbiAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMik7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5kYXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBkID0gaSsxO1xuICAgICAgICAgICAgY29uc3QgY2xhaW1hYmxlID0gKGQgPT0gZGF5KSAmJiAhY2xhaW1lZDtcbiAgICAgICAgICAgIGlmIChkIDwgZGF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzW2ldLnNldENsYWltYWJsZShjbGFpbWFibGUsIHRydWUpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChkID09IGRheSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZGF5c1tpXS5zZXRDbGFpbWFibGUoY2xhaW1hYmxlLCBjbGFpbWVkKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzW2ldLnNldENsYWltYWJsZShjbGFpbWFibGUsIGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ2xhaW0oc2VuZGVyOiBjYy5Ob2RlLCBkYXk6IHN0cmluZykge1xuICAgICAgICBpZiAoQXBpLmRhaWx5Qm9udXNDbGFpbWVkKSByZXR1cm47XG4gICAgICAgIGNvbnN0IGRhdGUgPSBkYXkgPyBwYXJzZUludChkYXkpIDogdGhpcy5jdXJyZW50O1xuICAgICAgICB0aGlzLm9uQ2xhaW1Db21wbChzZW5kZXIsIGRhdGUsIGZhbHNlKTtcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCgnY2xhaW1fZGFpbHlfYm9udXMnLCBkYXRlLCBDb25maWcuZGFpbHlCb251c1tkYXRlIC0gMV0sIGZhbHNlKTtcbiAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgIH1cblxuICAgIG9uQ2xhaW1FeHRyYShzZW5kZXI6IGNjLk5vZGUsIGRheTogc3RyaW5nKSB7XG4gICAgICAgIGlmIChBcGkuZGFpbHlCb251c0NsYWltZWQpIHJldHVybjtcbiAgICAgICAgY29uc3QgZGF0ZSA9IGRheSA/IHBhcnNlSW50KGRheSkgOiB0aGlzLmN1cnJlbnQ7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUE9QVVBfREFJTFlfUkVXQVJEX0FEKTtcbiAgICAgICAgQXBpLnNob3dSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25DbGFpbUNvbXBsKHNlbmRlciwgZGF0ZSwgZmFsc2UpO1xuICAgICAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCgnY2xhaW1fZGFpbHlfYm9udXMnLCBkYXRlLCBDb25maWcuZGFpbHlCb251c1tkYXRlIC0gMV0sIHRydWUpO1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KCdjbGFpbV9kYWlseV9ib251c19mYWlsJyk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0RBSUxZX1JFV0FSRF9BRF9FUlJPUik7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKHRoaXMubm9kZSwgMik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIG9uQ2xhaW1Db21wbChzZW5kZXIsIGRheTogbnVtYmVyLCBkb3VibGU6IGJvb2xlYW4pIHtcbiAgICAgICAgY29uc3QgZ2lmdCA9IENvbmZpZy5kYWlseUJvbnVzW2RheSAtIDFdO1xuICAgICAgICBpZiAoIWdpZnQuY29pbikge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuZWZmZWN0KTtcbiAgICAgICAgZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBlLnBvc2l0aW9uID0gc2VuZGVyLnRhcmdldC5wb3NpdGlvbjtcbiAgICAgICAgZS5zY2FsZSA9IDE7XG4gICAgICAgIGUucGFyZW50ID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKTtcbiAgICAgICAgY29uc3QgY29pbkJvbnVzID0gZG91YmxlID8gZ2lmdC5jb2luICogMiA6IGdpZnQuY29pbjtcbiAgICAgICAgZS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KGNvaW5Cb251cyk7XG4gICAgICAgIGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MubW92ZUJ5KDEuMiwgMCwgMjUwKSxcbiAgICAgICAgICAgIGNjLnJlbW92ZVNlbGYoKVxuICAgICAgICApKVxuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/popop/Modal.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a10e5sRCWFPIIokEQkdNm2n', 'Modal');
// Scripts/popop/Modal.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Modal = /** @class */ (function (_super) {
    __extends(Modal, _super);
    function Modal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.title = null;
        _this.content = null;
        return _this;
    }
    Modal_1 = Modal;
    // LIFE-CYCLE CALLBACKS:
    Modal.prototype.onLoad = function () {
        Modal_1.instance = this;
    };
    Modal.prototype.start = function () {
    };
    // update (dt) {}
    Modal.prototype.show = function (text) {
        this.content.string = text;
        this.node.active = true;
        this.popup.active = true;
    };
    Modal.prototype.onClose = function () {
        this.node.active = false;
        this.popup.active = false;
    };
    var Modal_1;
    Modal.instance = null;
    __decorate([
        property(cc.Node)
    ], Modal.prototype, "popup", void 0);
    __decorate([
        property(cc.Label)
    ], Modal.prototype, "title", void 0);
    __decorate([
        property(cc.Label)
    ], Modal.prototype, "content", void 0);
    Modal = Modal_1 = __decorate([
        ccclass
    ], Modal);
    return Modal;
}(cc.Component));
exports.default = Modal;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3BvcG9wL01vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW1DLHlCQUFZO0lBQS9DO1FBQUEscUVBaUNDO1FBL0JHLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUd2QixhQUFPLEdBQWEsSUFBSSxDQUFDOztJQXlCN0IsQ0FBQztjQWpDb0IsS0FBSztJQVV0Qix3QkFBd0I7SUFFeEIsc0JBQU0sR0FBTjtRQUNJLE9BQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFFRCxxQkFBSyxHQUFMO0lBQ0EsQ0FBQztJQUVELGlCQUFpQjtJQUVqQixvQkFBSSxHQUFKLFVBQUssSUFBWTtRQUNiLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCx1QkFBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDOztJQUVhLGNBQVEsR0FBVSxJQUFJLENBQUM7SUE5QnJDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0NBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt3Q0FDSTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzBDQUNNO0lBUlIsS0FBSztRQUR6QixPQUFPO09BQ2EsS0FBSyxDQWlDekI7SUFBRCxZQUFDO0NBakNELEFBaUNDLENBakNrQyxFQUFFLENBQUMsU0FBUyxHQWlDOUM7a0JBakNvQixLQUFLIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNb2RhbCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcG9wdXA6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHRpdGxlOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgY29udGVudDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICBvbkxvYWQgKCkge1xuICAgICAgICBNb2RhbC5pbnN0YW5jZSA9IHRoaXM7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHNob3codGV4dDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuY29udGVudC5zdHJpbmcgPSB0ZXh0O1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5wb3B1cC5hY3RpdmUgPSB0cnVlO1xuICAgIH1cblxuICAgIG9uQ2xvc2UoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5wb3B1cC5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGluc3RhbmNlOiBNb2RhbCA9IG51bGw7XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/th_TH_tut.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c9f3eJaDlJCYblINDTqM1DA', 'th_TH_tut');
// lang/th_TH_tut.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tutor = "\u0E40\u0E01\u0E21\u0E19\u0E35\u0E49\u0E21\u0E35\u0E44\u0E27\u0E49\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E35\u0E48\u0E04\u0E19 \u0E43\u0E0A\u0E49\u0E2A\u0E33\u0E23\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E21\u0E32\u0E15\u0E23\u0E10\u0E32\u0E19 52 \u0E43\u0E1A \u0E44\u0E21\u0E48\u0E21\u0E35\u0E42\u0E08\u0E4A\u0E01\u0E40\u0E01\u0E2D\u0E23\u0E4C\u0E41\u0E25\u0E30\u0E44\u0E21\u0E48\u0E21\u0E35\u0E44\u0E27\u0E25\u0E14\u0E4C\u0E01\u0E32\u0E23\u0E4C\u0E14 \u0E40\u0E1B\u0E47\u0E19\u0E44\u0E1B\u0E44\u0E14\u0E49\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E2D\u0E07\u0E2B\u0E23\u0E37\u0E2D\u0E2A\u0E32\u0E21\u0E04\u0E23\u0E31\u0E49\u0E07 \u0E19\u0E2D\u0E01\u0E08\u0E32\u0E01\u0E19\u0E35\u0E49\u0E22\u0E31\u0E07\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E44\u0E14\u0E49\u0E42\u0E14\u0E22\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E21\u0E32\u0E01\u0E01\u0E27\u0E48\u0E32\u0E2A\u0E35\u0E48\u0E04\u0E19\u0E42\u0E14\u0E22\u0E43\u0E0A\u0E49\u0E0B\u0E2D\u0E07\u0E01\u0E32\u0E23\u0E4C\u0E14 52 \u0E43\u0E1A\u0E2A\u0E2D\u0E07\u0E0A\u0E38\u0E14\u0E2A\u0E31\u0E1A\u0E01\u0E31\u0E19\n\n\u0E42\u0E14\u0E22\u0E1B\u0E01\u0E15\u0E34\u0E40\u0E01\u0E21\u0E08\u0E30\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48\u0E41\u0E25\u0E30\u0E40\u0E25\u0E48\u0E19\u0E15\u0E32\u0E21\u0E40\u0E02\u0E47\u0E21\u0E19\u0E32\u0E2C\u0E34\u0E01\u0E32 \u0E41\u0E15\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E17\u0E27\u0E19\u0E40\u0E02\u0E47\u0E21\u0E19\u0E32\u0E2C\u0E34\u0E01\u0E32\u0E41\u0E17\u0E19\u0E44\u0E14\u0E49\u0E2B\u0E32\u0E01\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E15\u0E01\u0E25\u0E07\u0E25\u0E48\u0E27\u0E07\u0E2B\u0E19\u0E49\u0E32\u0E17\u0E35\u0E48\u0E08\u0E30\u0E17\u0E33\u0E40\u0E0A\u0E48\u0E19\u0E19\u0E31\u0E49\u0E19\n\n\u0E01\u0E32\u0E23\u0E08\u0E31\u0E14\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E04\u0E37\u0E2D\u0E2A\u0E2D\u0E07 (\u0E2A\u0E39\u0E07\u0E2A\u0E38\u0E14), \u0E40\u0E2D\u0E0B, \u0E04\u0E34\u0E07, \u0E04\u0E27\u0E35\u0E19, \u0E41\u0E08\u0E47\u0E04, \u0E2A\u0E34\u0E1A, \u0E40\u0E01\u0E49\u0E32, \u0E41\u0E1B\u0E14, \u0E40\u0E08\u0E47\u0E14, \u0E2B\u0E01, \u0E2B\u0E49\u0E32, \u0E2A\u0E35\u0E48, \u0E2A\u0E32\u0E21 (\u0E15\u0E48\u0E33\u0E2A\u0E38\u0E14)\n\n\u0E20\u0E32\u0E22\u0E43\u0E19\u0E41\u0E15\u0E48\u0E25\u0E30\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E22\u0E31\u0E07\u0E21\u0E35\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E0A\u0E38\u0E14: Hearts (\u0E2A\u0E39\u0E07\u0E2A\u0E38\u0E14), Diamonds, Clubs, Spades (\u0E15\u0E48\u0E33\u0E2A\u0E38\u0E14)\n\n\u0E14\u0E31\u0E07\u0E19\u0E31\u0E49\u0E19\u0E44\u0E1E\u0E48 3 \u0E42\u0E1E\u0E14\u0E33\u0E08\u0E36\u0E07\u0E40\u0E1B\u0E47\u0E19\u0E44\u0E1E\u0E48\u0E17\u0E35\u0E48\u0E15\u0E48\u0E33\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E43\u0E19\u0E41\u0E1E\u0E47\u0E04\u0E41\u0E25\u0E30\u0E44\u0E1E\u0E48 2 \u0E43\u0E1A\u0E02\u0E2D\u0E07 Hearts \u0E19\u0E31\u0E49\u0E19\u0E2A\u0E39\u0E07\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14 \u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E21\u0E35\u0E04\u0E27\u0E32\u0E21\u0E2A\u0E33\u0E04\u0E31\u0E0D\u0E21\u0E32\u0E01\u0E01\u0E27\u0E48\u0E32\u0E04\u0E27\u0E32\u0E21\u0E40\u0E2B\u0E21\u0E32\u0E30\u0E2A\u0E21\u0E15\u0E31\u0E27\u0E2D\u0E22\u0E48\u0E32\u0E07\u0E40\u0E0A\u0E48\u0E19 8 \u0E0A\u0E19\u0E30 7\n\n\u0E02\u0E49\u0E2D\u0E15\u0E01\u0E25\u0E07\n\n\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E40\u0E01\u0E21\u0E41\u0E23\u0E01\u0E40\u0E08\u0E49\u0E32\u0E21\u0E37\u0E2D\u0E08\u0E30\u0E16\u0E39\u0E01\u0E40\u0E25\u0E37\u0E2D\u0E01\u0E41\u0E1A\u0E1A\u0E2A\u0E38\u0E48\u0E21 \u0E08\u0E32\u0E01\u0E19\u0E31\u0E49\u0E19\u0E1C\u0E39\u0E49\u0E41\u0E1E\u0E49\u0E02\u0E2D\u0E07\u0E41\u0E15\u0E48\u0E25\u0E30\u0E40\u0E01\u0E21\u0E08\u0E30\u0E15\u0E49\u0E2D\u0E07\u0E08\u0E31\u0E14\u0E01\u0E32\u0E23\u0E15\u0E48\u0E2D\u0E44\u0E1B \u0E40\u0E21\u0E37\u0E48\u0E2D\u0E21\u0E35\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E35\u0E48\u0E04\u0E19\u0E44\u0E1E\u0E48 13 \u0E43\u0E1A\u0E08\u0E30\u0E16\u0E39\u0E01\u0E41\u0E08\u0E01\u0E43\u0E2B\u0E49\u0E01\u0E31\u0E1A\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E41\u0E15\u0E48\u0E25\u0E30\u0E04\u0E19\n\n\u0E2B\u0E32\u0E01\u0E21\u0E35\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E19\u0E49\u0E2D\u0E22\u0E01\u0E27\u0E48\u0E32\u0E2A\u0E35\u0E48\u0E04\u0E19\u0E08\u0E30\u0E22\u0E31\u0E07\u0E04\u0E07\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48 13 \u0E43\u0E1A\u0E43\u0E2B\u0E49\u0E01\u0E31\u0E1A\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E41\u0E15\u0E48\u0E25\u0E30\u0E04\u0E19\u0E41\u0E25\u0E30\u0E08\u0E30\u0E21\u0E35\u0E44\u0E1E\u0E48\u0E40\u0E2B\u0E25\u0E37\u0E2D\u0E2D\u0E22\u0E39\u0E48\u0E0B\u0E36\u0E48\u0E07\u0E44\u0E21\u0E48\u0E44\u0E14\u0E49\u0E43\u0E0A\u0E49\u0E43\u0E19\u0E40\u0E01\u0E21 \u0E2D\u0E35\u0E01\u0E17\u0E32\u0E07\u0E40\u0E25\u0E37\u0E2D\u0E01\u0E2B\u0E19\u0E36\u0E48\u0E07\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E32\u0E21\u0E04\u0E19\u0E04\u0E37\u0E2D\u0E15\u0E32\u0E21\u0E02\u0E49\u0E2D\u0E15\u0E01\u0E25\u0E07\u0E01\u0E48\u0E2D\u0E19\u0E17\u0E35\u0E48\u0E08\u0E30\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48 17 \u0E43\u0E1A\u0E15\u0E48\u0E2D\u0E04\u0E19 \u0E40\u0E21\u0E37\u0E48\u0E2D\u0E21\u0E35\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E40\u0E1E\u0E35\u0E22\u0E07\u0E2A\u0E2D\u0E07\u0E04\u0E19\u0E04\u0E27\u0E23\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48\u0E43\u0E2B\u0E49\u0E04\u0E19\u0E25\u0E30 13 \u0E43\u0E1A\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19 - \u0E2B\u0E32\u0E01\u0E44\u0E1E\u0E48\u0E17\u0E31\u0E49\u0E07\u0E2B\u0E21\u0E14\u0E44\u0E14\u0E49\u0E23\u0E31\u0E1A\u0E01\u0E32\u0E23\u0E41\u0E08\u0E01\u0E41\u0E08\u0E07\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E08\u0E30\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E43\u0E0A\u0E49\u0E21\u0E37\u0E2D\u0E02\u0E2D\u0E07\u0E01\u0E31\u0E19\u0E41\u0E25\u0E30\u0E01\u0E31\u0E19\u0E44\u0E14\u0E49\u0E0B\u0E36\u0E48\u0E07\u0E08\u0E30\u0E17\u0E33\u0E43\u0E2B\u0E49\u0E40\u0E01\u0E21\u0E40\u0E2A\u0E35\u0E22 \u0E40\u0E21\u0E37\u0E48\u0E2D\u0E21\u0E35\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E21\u0E32\u0E01\u0E01\u0E27\u0E48\u0E32\u0E2A\u0E35\u0E48\u0E04\u0E19\u0E04\u0E38\u0E13\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E15\u0E01\u0E25\u0E07\u0E25\u0E48\u0E27\u0E07\u0E2B\u0E19\u0E49\u0E32\u0E44\u0E14\u0E49\u0E27\u0E48\u0E32\u0E08\u0E30\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48 13 \u0E43\u0E1A\u0E08\u0E32\u0E01\u0E44\u0E1E\u0E48\u0E2A\u0E2D\u0E07\u0E2A\u0E33\u0E23\u0E31\u0E1A\u0E2B\u0E23\u0E37\u0E2D\u0E41\u0E08\u0E01\u0E44\u0E1E\u0E48\u0E43\u0E2B\u0E49\u0E21\u0E32\u0E01\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E40\u0E17\u0E48\u0E32\u0E17\u0E35\u0E48\u0E08\u0E30\u0E17\u0E33\u0E44\u0E14\u0E49\u0E43\u0E2B\u0E49\u0E01\u0E31\u0E1A\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\n\n\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\n\n\u0E43\u0E19\u0E40\u0E01\u0E21\u0E41\u0E23\u0E01\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E17\u0E35\u0E48\u0E21\u0E35 3 Spades \u0E08\u0E30\u0E40\u0E23\u0E34\u0E48\u0E21\u0E40\u0E25\u0E48\u0E19 \u0E2B\u0E32\u0E01\u0E44\u0E21\u0E48\u0E21\u0E35\u0E43\u0E04\u0E23\u0E21\u0E35\u0E44\u0E1E\u0E48 3 \u0E43\u0E1A (\u0E43\u0E19\u0E40\u0E01\u0E21\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E32\u0E21\u0E2B\u0E23\u0E37\u0E2D\u0E2A\u0E2D\u0E07\u0E04\u0E19) \u0E43\u0E04\u0E23\u0E01\u0E47\u0E15\u0E32\u0E21\u0E17\u0E35\u0E48\u0E16\u0E37\u0E2D\u0E44\u0E1E\u0E48\u0E15\u0E48\u0E33\u0E2A\u0E38\u0E14\u0E08\u0E30\u0E40\u0E23\u0E34\u0E48\u0E21\u0E15\u0E49\u0E19 \u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E08\u0E30\u0E15\u0E49\u0E2D\u0E07\u0E40\u0E23\u0E34\u0E48\u0E21\u0E15\u0E49\u0E19\u0E14\u0E49\u0E27\u0E22\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E15\u0E48\u0E33\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E19\u0E35\u0E49\u0E44\u0E21\u0E48\u0E27\u0E48\u0E32\u0E08\u0E30\u0E14\u0E49\u0E27\u0E22\u0E15\u0E31\u0E27\u0E40\u0E2D\u0E07\u0E2B\u0E23\u0E37\u0E2D\u0E40\u0E1B\u0E47\u0E19\u0E2A\u0E48\u0E27\u0E19\u0E2B\u0E19\u0E36\u0E48\u0E07\u0E02\u0E2D\u0E07\u0E01\u0E32\u0E23\u0E23\u0E27\u0E21\u0E01\u0E31\u0E19\n\n\u0E43\u0E19\u0E40\u0E01\u0E21\u0E15\u0E48\u0E2D \u0E46 \u0E44\u0E1B\u0E1C\u0E39\u0E49\u0E0A\u0E19\u0E30\u0E02\u0E2D\u0E07\u0E40\u0E01\u0E21\u0E01\u0E48\u0E2D\u0E19\u0E2B\u0E19\u0E49\u0E32\u0E08\u0E30\u0E44\u0E14\u0E49\u0E40\u0E25\u0E48\u0E19\u0E01\u0E48\u0E2D\u0E19\u0E41\u0E25\u0E30\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E23\u0E34\u0E48\u0E21\u0E15\u0E49\u0E19\u0E14\u0E49\u0E27\u0E22\u0E0A\u0E38\u0E14\u0E04\u0E48\u0E32\u0E1C\u0E2A\u0E21\u0E43\u0E14\u0E01\u0E47\u0E44\u0E14\u0E49\n\n\u0E43\u0E19\u0E17\u0E32\u0E07\u0E01\u0E25\u0E31\u0E1A\u0E01\u0E31\u0E19\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E41\u0E15\u0E48\u0E25\u0E30\u0E04\u0E19\u0E08\u0E30\u0E15\u0E49\u0E2D\u0E07\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E1E\u0E48\u0E17\u0E35\u0E48\u0E40\u0E25\u0E48\u0E19\u0E01\u0E48\u0E2D\u0E19\u0E2B\u0E19\u0E49\u0E32\u0E19\u0E35\u0E49\u0E2B\u0E23\u0E37\u0E2D\u0E44\u0E1E\u0E48\u0E1C\u0E2A\u0E21\u0E01\u0E31\u0E19\u0E42\u0E14\u0E22\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E2B\u0E23\u0E37\u0E2D\u0E44\u0E1E\u0E48\u0E23\u0E27\u0E21\u0E01\u0E31\u0E19\u0E17\u0E35\u0E48\u0E0A\u0E19\u0E30\u0E2B\u0E23\u0E37\u0E2D\u0E2A\u0E48\u0E07\u0E1C\u0E48\u0E32\u0E19\u0E41\u0E25\u0E30\u0E44\u0E21\u0E48\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E43\u0E14 \u0E46 \u0E01\u0E32\u0E23\u0E4C\u0E14\u0E17\u0E35\u0E48\u0E40\u0E25\u0E48\u0E19\u0E08\u0E30\u0E16\u0E39\u0E01\u0E27\u0E32\u0E07\u0E43\u0E19\u0E01\u0E2D\u0E07\u0E42\u0E14\u0E22\u0E2B\u0E07\u0E32\u0E22\u0E2B\u0E19\u0E49\u0E32\u0E02\u0E36\u0E49\u0E19\u0E15\u0E23\u0E07\u0E01\u0E25\u0E32\u0E07\u0E42\u0E15\u0E4A\u0E30 \u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E08\u0E30\u0E14\u0E33\u0E40\u0E19\u0E34\u0E19\u0E44\u0E1B\u0E23\u0E2D\u0E1A \u0E46 \u0E42\u0E15\u0E4A\u0E30\u0E2B\u0E25\u0E32\u0E22 \u0E46 \u0E04\u0E23\u0E31\u0E49\u0E07\u0E40\u0E17\u0E48\u0E32\u0E17\u0E35\u0E48\u0E08\u0E33\u0E40\u0E1B\u0E47\u0E19\u0E08\u0E19\u0E01\u0E27\u0E48\u0E32\u0E08\u0E30\u0E21\u0E35\u0E04\u0E19\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E2B\u0E23\u0E37\u0E2D\u0E0A\u0E38\u0E14\u0E17\u0E35\u0E48\u0E44\u0E21\u0E48\u0E21\u0E35\u0E43\u0E04\u0E23\u0E0A\u0E19\u0E30 \u0E40\u0E21\u0E37\u0E48\u0E2D\u0E40\u0E01\u0E34\u0E14\u0E40\u0E2B\u0E15\u0E38\u0E01\u0E32\u0E23\u0E13\u0E4C\u0E40\u0E0A\u0E48\u0E19\u0E19\u0E35\u0E49\u0E44\u0E1E\u0E48\u0E17\u0E35\u0E48\u0E40\u0E25\u0E48\u0E19\u0E17\u0E31\u0E49\u0E07\u0E2B\u0E21\u0E14\u0E08\u0E30\u0E16\u0E39\u0E01\u0E27\u0E32\u0E07\u0E17\u0E34\u0E49\u0E07\u0E44\u0E27\u0E49\u0E41\u0E25\u0E30\u0E1C\u0E39\u0E49\u0E17\u0E35\u0E48\u0E40\u0E25\u0E48\u0E19\u0E44\u0E14\u0E49\u0E44\u0E21\u0E48\u0E41\u0E1E\u0E49\u0E43\u0E04\u0E23\u0E08\u0E30\u0E40\u0E23\u0E34\u0E48\u0E21\u0E2D\u0E35\u0E01\u0E04\u0E23\u0E31\u0E49\u0E07\u0E42\u0E14\u0E22\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E15\u0E32\u0E21\u0E01\u0E0E\u0E2B\u0E21\u0E32\u0E22\u0E2B\u0E23\u0E37\u0E2D\u0E44\u0E1E\u0E48\u0E1C\u0E2A\u0E21\u0E43\u0E14 \u0E46 \u0E42\u0E14\u0E22\u0E2B\u0E31\u0E19\u0E2B\u0E19\u0E49\u0E32\u0E40\u0E02\u0E49\u0E32\u0E2B\u0E32\u0E01\u0E25\u0E32\u0E07\u0E42\u0E15\u0E4A\u0E30\n\n\u0E2B\u0E32\u0E01\u0E04\u0E38\u0E13\u0E1C\u0E48\u0E32\u0E19\u0E44\u0E1B\u0E04\u0E38\u0E13\u0E08\u0E30\u0E16\u0E39\u0E01\u0E25\u0E47\u0E2D\u0E01\u0E44\u0E21\u0E48\u0E43\u0E2B\u0E49\u0E40\u0E25\u0E48\u0E19\u0E08\u0E19\u0E01\u0E27\u0E48\u0E32\u0E08\u0E30\u0E21\u0E35\u0E04\u0E19\u0E40\u0E25\u0E48\u0E19\u0E42\u0E14\u0E22\u0E17\u0E35\u0E48\u0E44\u0E21\u0E48\u0E21\u0E35\u0E43\u0E04\u0E23\u0E0A\u0E19\u0E30 \u0E04\u0E38\u0E13\u0E21\u0E35\u0E2A\u0E34\u0E17\u0E18\u0E34\u0E4C\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E43\u0E2B\u0E21\u0E48\u0E2D\u0E35\u0E01\u0E04\u0E23\u0E31\u0E49\u0E07\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19\n\n\u0E15\u0E31\u0E27\u0E2D\u0E22\u0E48\u0E32\u0E07 (\u0E21\u0E35\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E32\u0E21\u0E04\u0E19): \u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E17\u0E32\u0E07\u0E02\u0E27\u0E32\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E40\u0E25\u0E48\u0E19\u0E2A\u0E32\u0E21\u0E04\u0E19\u0E40\u0E14\u0E35\u0E22\u0E27\u0E04\u0E38\u0E13\u0E16\u0E37\u0E2D\u0E40\u0E2D\u0E0B \u0E41\u0E15\u0E48\u0E15\u0E31\u0E14\u0E2A\u0E34\u0E19\u0E43\u0E08\u0E17\u0E35\u0E48\u0E08\u0E30\u0E2A\u0E48\u0E07\u0E1C\u0E48\u0E32\u0E19\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E17\u0E32\u0E07\u0E0B\u0E49\u0E32\u0E22\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E40\u0E25\u0E48\u0E19\u0E40\u0E01\u0E49\u0E32\u0E04\u0E19\u0E41\u0E25\u0E30\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E17\u0E32\u0E07\u0E02\u0E27\u0E32\u0E40\u0E25\u0E48\u0E19\u0E40\u0E1B\u0E47\u0E19\u0E23\u0E32\u0E0A\u0E32 \u0E15\u0E2D\u0E19\u0E19\u0E35\u0E49\u0E04\u0E38\u0E13\u0E44\u0E21\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E23\u0E32\u0E0A\u0E32\u0E14\u0E49\u0E27\u0E22\u0E40\u0E2D\u0E0B\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E44\u0E14\u0E49\u0E40\u0E1E\u0E23\u0E32\u0E30\u0E04\u0E38\u0E13\u0E1C\u0E48\u0E32\u0E19\u0E44\u0E1B\u0E41\u0E25\u0E49\u0E27 \u0E2B\u0E32\u0E01\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E04\u0E19\u0E17\u0E35\u0E48\u0E2A\u0E32\u0E21\u0E1C\u0E48\u0E32\u0E19\u0E44\u0E1B\u0E14\u0E49\u0E27\u0E22\u0E41\u0E25\u0E30\u0E1D\u0E48\u0E32\u0E22\u0E15\u0E23\u0E07\u0E02\u0E49\u0E32\u0E21\u0E21\u0E37\u0E2D\u0E02\u0E27\u0E32\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E08\u0E30\u0E40\u0E1B\u0E47\u0E19\u0E23\u0E32\u0E0A\u0E34\u0E19\u0E35\u0E15\u0E2D\u0E19\u0E19\u0E35\u0E49\u0E04\u0E38\u0E13\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E40\u0E2D\u0E0B\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E44\u0E14\u0E49\u0E2B\u0E32\u0E01\u0E15\u0E49\u0E2D\u0E07\u0E01\u0E32\u0E23\n\n\u0E01\u0E32\u0E23\u0E40\u0E25\u0E48\u0E19\u0E15\u0E32\u0E21\u0E01\u0E0E\u0E2B\u0E21\u0E32\u0E22\u0E43\u0E19\u0E40\u0E01\u0E21\u0E21\u0E35\u0E14\u0E31\u0E07\u0E19\u0E35\u0E49:\n\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E17\u0E35\u0E48\u0E15\u0E48\u0E33\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E04\u0E37\u0E2D\u0E44\u0E1E\u0E48 3 \u0E43\u0E1A\u0E41\u0E25\u0E30\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E04\u0E37\u0E2D\u0E44\u0E1E\u0E48 2 \u0E43\u0E1A\n\u0E08\u0E31\u0E1A\u0E04\u0E39\u0E48\u0E44\u0E1E\u0E48\u0E2A\u0E2D\u0E07\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E21\u0E35\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19\u0E40\u0E0A\u0E48\u0E19 7-7 \u0E2B\u0E23\u0E37\u0E2D Q-Q\n\u0E44\u0E1E\u0E48\u0E2A\u0E32\u0E21\u0E43\u0E1A\u0E43\u0E19\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19 - \u0E40\u0E0A\u0E48\u0E19 5-5-5\n\u0E44\u0E1E\u0E48\u0E2A\u0E35\u0E48\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E21\u0E35\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19 - \u0E40\u0E0A\u0E48\u0E19 9-9-9-9\n\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E2A\u0E32\u0E21\u0E43\u0E1A\u0E02\u0E36\u0E49\u0E19\u0E44\u0E1B\u0E17\u0E35\u0E48\u0E21\u0E35\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E15\u0E48\u0E2D\u0E40\u0E19\u0E37\u0E48\u0E2D\u0E07\u0E01\u0E31\u0E19 (\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E1C\u0E2A\u0E21\u0E0A\u0E38\u0E14\u0E44\u0E14\u0E49) \u0E40\u0E0A\u0E48\u0E19 4-5-6 \u0E2B\u0E23\u0E37\u0E2D J-Q-K-A \u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E21\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16 \"\u0E1E\u0E25\u0E34\u0E01\u0E21\u0E38\u0E21\" \u0E23\u0E30\u0E2B\u0E27\u0E48\u0E32\u0E07\u0E2A\u0E2D\u0E07\u0E16\u0E36\u0E07\u0E2A\u0E32\u0E21 - A-2-3 \u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E17\u0E35\u0E48\u0E16\u0E39\u0E01\u0E15\u0E49\u0E2D\u0E07\u0E40\u0E19\u0E37\u0E48\u0E2D\u0E07\u0E08\u0E32\u0E01 2 \u0E2A\u0E39\u0E07\u0E41\u0E25\u0E30 3 \u0E15\u0E48\u0E33\n\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E04\u0E39\u0E48 3 \u0E04\u0E39\u0E48\u0E02\u0E36\u0E49\u0E19\u0E44\u0E1B\u0E02\u0E2D\u0E07\u0E2D\u0E31\u0E19\u0E14\u0E31\u0E1A\u0E17\u0E35\u0E48\u0E15\u0E48\u0E2D\u0E40\u0E19\u0E37\u0E48\u0E2D\u0E07\u0E01\u0E31\u0E19\u0E40\u0E0A\u0E48\u0E19 3-3-4-4-5-5 \u0E2B\u0E23\u0E37\u0E2D 6-6-7-7-8-8-9-9\n\n\u0E42\u0E14\u0E22\u0E17\u0E31\u0E48\u0E27\u0E44\u0E1B\u0E01\u0E32\u0E23\u0E1C\u0E2A\u0E21\u0E08\u0E30\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E14\u0E49\u0E42\u0E14\u0E22\u0E01\u0E32\u0E23\u0E1C\u0E2A\u0E21\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E02\u0E2D\u0E07\u0E44\u0E1E\u0E48\u0E1B\u0E23\u0E30\u0E40\u0E20\u0E17\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19\u0E41\u0E25\u0E30\u0E08\u0E33\u0E19\u0E27\u0E19\u0E44\u0E1E\u0E48\u0E40\u0E17\u0E48\u0E32\u0E01\u0E31\u0E19 \u0E14\u0E31\u0E07\u0E19\u0E31\u0E49\u0E19\u0E2B\u0E32\u0E01\u0E19\u0E33\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E08\u0E30\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E44\u0E14\u0E49\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19 \u0E16\u0E49\u0E32\u0E04\u0E39\u0E48\u0E19\u0E33\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19\u0E17\u0E35\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E44\u0E14\u0E49; \u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E2A\u0E32\u0E21\u0E43\u0E1A\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E14\u0E49\u0E42\u0E14\u0E22\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E2A\u0E32\u0E21\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19 \u0E41\u0E25\u0E30\u0E2D\u0E37\u0E48\u0E19 \u0E46 \u0E15\u0E31\u0E27\u0E2D\u0E22\u0E48\u0E32\u0E07\u0E40\u0E0A\u0E48\u0E19\u0E04\u0E38\u0E13\u0E44\u0E21\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E04\u0E39\u0E48\u0E14\u0E49\u0E27\u0E22\u0E44\u0E1E\u0E48\u0E2A\u0E32\u0E21\u0E43\u0E1A\u0E2B\u0E23\u0E37\u0E2D\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E2A\u0E35\u0E48\u0E43\u0E1A\u0E17\u0E35\u0E48\u0E21\u0E35\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E44\u0E1E\u0E48\u0E2B\u0E49\u0E32\u0E43\u0E1A\n\n\u0E43\u0E19\u0E01\u0E32\u0E23\u0E15\u0E31\u0E14\u0E2A\u0E34\u0E19\u0E43\u0E08\u0E27\u0E48\u0E32\u0E0A\u0E38\u0E14\u0E04\u0E48\u0E32\u0E1C\u0E2A\u0E21\u0E1B\u0E23\u0E30\u0E40\u0E20\u0E17\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19\u0E43\u0E14\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E04\u0E38\u0E13\u0E40\u0E1E\u0E35\u0E22\u0E07\u0E41\u0E04\u0E48\u0E14\u0E39\u0E44\u0E1E\u0E48\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E17\u0E35\u0E48\u0E2A\u0E38\u0E14\u0E43\u0E19\u0E0A\u0E38\u0E14\u0E04\u0E48\u0E32\u0E1C\u0E2A\u0E21 \u0E15\u0E31\u0E27\u0E2D\u0E22\u0E48\u0E32\u0E07\u0E40\u0E0A\u0E48\u0E19 7-7 \u0E40\u0E15\u0E49\u0E19 7-7 \u0E40\u0E1E\u0E23\u0E32\u0E30\u0E2B\u0E31\u0E27\u0E43\u0E08\u0E40\u0E15\u0E49\u0E19\u0E40\u0E1E\u0E0A\u0E23 \u0E43\u0E19\u0E17\u0E33\u0E19\u0E2D\u0E07\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19 8-9-10 \u0E40\u0E15\u0E49\u0E19 8-9-10 \u0E40\u0E1E\u0E23\u0E32\u0E30\u0E40\u0E1B\u0E47\u0E19\u0E44\u0E1E\u0E48\u0E2A\u0E39\u0E07\u0E2A\u0E38\u0E14 (\u0E2B\u0E25\u0E31\u0E01\u0E2A\u0E34\u0E1A) \u0E17\u0E35\u0E48\u0E19\u0E33\u0E21\u0E32\u0E40\u0E1B\u0E23\u0E35\u0E22\u0E1A\u0E40\u0E17\u0E35\u0E22\u0E1A\u0E01\u0E31\u0E19\n\n\u0E21\u0E35\u0E02\u0E49\u0E2D\u0E22\u0E01\u0E40\u0E27\u0E49\u0E19\u0E40\u0E1E\u0E35\u0E22\u0E07\u0E2A\u0E35\u0E48\u0E02\u0E49\u0E2D\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E01\u0E0E\u0E17\u0E35\u0E48\u0E27\u0E48\u0E32\u0E0A\u0E38\u0E14\u0E04\u0E48\u0E32\u0E1C\u0E2A\u0E21\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E14\u0E49\u0E42\u0E14\u0E22\u0E01\u0E32\u0E23\u0E23\u0E27\u0E21\u0E01\u0E31\u0E19\u0E02\u0E2D\u0E07\u0E1B\u0E23\u0E30\u0E40\u0E20\u0E17\u0E40\u0E14\u0E35\u0E22\u0E27\u0E01\u0E31\u0E19\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19:\n\n\u0E44\u0E1E\u0E48\u0E2A\u0E35\u0E48\u0E43\u0E1A\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E1E\u0E48\u0E2A\u0E2D\u0E07\u0E43\u0E1A\u0E43\u0E14\u0E01\u0E47\u0E44\u0E14\u0E49 (\u0E41\u0E15\u0E48\u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E2D\u0E37\u0E48\u0E19 \u0E46 \u0E40\u0E0A\u0E48\u0E19\u0E40\u0E2D\u0E0B\u0E2B\u0E23\u0E37\u0E2D\u0E23\u0E32\u0E0A\u0E32) \u0E2A\u0E35\u0E48\u0E0A\u0E19\u0E34\u0E14\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E14\u0E49\u0E14\u0E49\u0E27\u0E22\u0E2A\u0E35\u0E48\u0E0A\u0E19\u0E34\u0E14\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\n\n\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2A\u0E32\u0E21\u0E04\u0E39\u0E48 (\u0E40\u0E0A\u0E48\u0E19 7-7-8-8-9-9) \u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E44\u0E1E\u0E48\u0E2A\u0E2D\u0E07\u0E43\u0E1A\u0E43\u0E14\u0E01\u0E47\u0E44\u0E14\u0E49 (\u0E41\u0E15\u0E48\u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E44\u0E1E\u0E48\u0E43\u0E1A\u0E40\u0E14\u0E35\u0E22\u0E27\u0E2D\u0E37\u0E48\u0E19 \u0E46 ) \u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2A\u0E32\u0E21\u0E04\u0E39\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E42\u0E14\u0E22\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E02\u0E2D\u0E07\u0E2A\u0E32\u0E21\u0E04\u0E39\u0E48\n\n\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2A\u0E35\u0E48\u0E04\u0E39\u0E48 (\u0E40\u0E0A\u0E48\u0E19 5-5-6-6-7-7-8-8) \u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E04\u0E39\u0E48\u0E02\u0E2D\u0E07\u0E2A\u0E2D\u0E07\u0E04\u0E39\u0E48\u0E44\u0E14\u0E49 (\u0E41\u0E15\u0E48\u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E04\u0E39\u0E48\u0E2D\u0E37\u0E48\u0E19 \u0E46 ) \u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2A\u0E35\u0E48\u0E04\u0E39\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E42\u0E14\u0E22\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E02\u0E2D\u0E07\u0E2A\u0E35\u0E48\u0E04\u0E39\u0E48\n\n\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2B\u0E49\u0E32\u0E04\u0E39\u0E48 (\u0E40\u0E0A\u0E48\u0E19 8-8-9-9-10-10-J-J-Q-Q) \u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E40\u0E0B\u0E15\u0E02\u0E2D\u0E07\u0E2A\u0E32\u0E21\u0E04\u0E39\u0E48\u0E44\u0E14\u0E49 (\u0E41\u0E15\u0E48\u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E2D\u0E35\u0E01\u0E2A\u0E32\u0E21\u0E04\u0E39\u0E48) \u0E25\u0E33\u0E14\u0E31\u0E1A\u0E02\u0E2D\u0E07\u0E2B\u0E49\u0E32\u0E04\u0E39\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E42\u0E14\u0E22\u0E25\u0E33\u0E14\u0E31\u0E1A\u0E17\u0E35\u0E48\u0E2A\u0E39\u0E07\u0E01\u0E27\u0E48\u0E32\u0E02\u0E2D\u0E07\u0E2B\u0E49\u0E32\u0E04\u0E39\u0E48\n\n\u0E0A\u0E38\u0E14\u0E04\u0E48\u0E32\u0E1C\u0E2A\u0E21\u0E40\u0E2B\u0E25\u0E48\u0E32\u0E19\u0E35\u0E49\u0E17\u0E35\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E04\u0E39\u0E48\u0E40\u0E14\u0E35\u0E48\u0E22\u0E27\u0E2B\u0E23\u0E37\u0E2D\u0E0A\u0E38\u0E14\u0E2A\u0E2D\u0E07\u0E0A\u0E38\u0E14\u0E1A\u0E32\u0E07\u0E04\u0E23\u0E31\u0E49\u0E07\u0E40\u0E23\u0E35\u0E22\u0E01\u0E27\u0E48\u0E32\u0E23\u0E30\u0E40\u0E1A\u0E34\u0E14\u0E2B\u0E23\u0E37\u0E2D\u0E23\u0E30\u0E40\u0E1A\u0E34\u0E14\u0E2A\u0E2D\u0E07\u0E25\u0E39\u0E01\u0E41\u0E25\u0E30\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E25\u0E48\u0E19\u0E44\u0E14\u0E49\u0E41\u0E21\u0E49\u0E01\u0E23\u0E30\u0E17\u0E31\u0E48\u0E07\u0E1C\u0E39\u0E49\u0E40\u0E25\u0E48\u0E19\u0E17\u0E35\u0E48\u0E1C\u0E48\u0E32\u0E19\u0E21\u0E32\u0E01\u0E48\u0E2D\u0E19\u0E2B\u0E19\u0E49\u0E32\u0E19\u0E35\u0E49\n\n\u0E42\u0E1B\u0E23\u0E14\u0E17\u0E23\u0E32\u0E1A\u0E27\u0E48\u0E32\u0E02\u0E49\u0E2D\u0E22\u0E01\u0E40\u0E27\u0E49\u0E19\u0E40\u0E2B\u0E25\u0E48\u0E32\u0E19\u0E35\u0E49\u0E43\u0E0A\u0E49\u0E01\u0E31\u0E1A\u0E01\u0E32\u0E23\u0E15\u0E35\u0E2A\u0E2D\u0E07\u0E43\u0E1A\u0E40\u0E17\u0E48\u0E32\u0E19\u0E31\u0E49\u0E19\u0E44\u0E21\u0E48\u0E43\u0E0A\u0E48\u0E44\u0E1E\u0E48\u0E2D\u0E37\u0E48\u0E19 \u0E46 \u0E15\u0E31\u0E27\u0E2D\u0E22\u0E48\u0E32\u0E07\u0E40\u0E0A\u0E48\u0E19\u0E16\u0E49\u0E32\u0E21\u0E35\u0E04\u0E19\u0E40\u0E25\u0E48\u0E19\u0E40\u0E2D\u0E0B\u0E04\u0E38\u0E13\u0E08\u0E30\u0E44\u0E21\u0E48\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E21\u0E31\u0E19\u0E14\u0E49\u0E27\u0E22\u0E40\u0E2D\u0E0B\u0E17\u0E31\u0E49\u0E07\u0E2A\u0E35\u0E48\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E44\u0E14\u0E49 \u0E41\u0E15\u0E48\u0E16\u0E49\u0E32\u0E40\u0E2D\u0E0B\u0E16\u0E39\u0E01\u0E2A\u0E2D\u0E07\u0E04\u0E19\u0E2B\u0E19\u0E36\u0E48\u0E07\u0E04\u0E19\u0E17\u0E31\u0E49\u0E07\u0E2A\u0E35\u0E48\u0E02\u0E2D\u0E07\u0E04\u0E38\u0E13\u0E01\u0E47\u0E2A\u0E32\u0E21\u0E32\u0E23\u0E16\u0E43\u0E0A\u0E49\u0E40\u0E1E\u0E37\u0E48\u0E2D\u0E40\u0E2D\u0E32\u0E0A\u0E19\u0E30\u0E17\u0E31\u0E49\u0E07\u0E2A\u0E2D\u0E07\u0E44\u0E14\u0E49";
exports.default = tutor;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RoX1RIX3R1dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sS0FBSyxHQUFHLHd2d0JBb0RxTSxDQUFBO0FBRW5OLGtCQUFlLEtBQUssQ0FBQyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHR1dG9yID0gYOC5gOC4geC4oeC4meC4teC5ieC4oeC4teC5hOC4p+C5ieC4quC4s+C4q+C4o+C4seC4muC4nOC4ueC5ieC5gOC4peC5iOC4meC4quC4teC5iOC4hOC4mSDguYPguIrguYnguKrguLPguKPguLHguJrguYTguJ7guYjguKHguLLguJXguKPguJDguLLguJkgNTIg4LmD4LiaIOC5hOC4oeC5iOC4oeC4teC5guC4iOC5iuC4geC5gOC4geC4reC4o+C5jOC5geC4peC4sOC5hOC4oeC5iOC4oeC4teC5hOC4p+C4peC4lOC5jOC4geC4suC4o+C5jOC4lCDguYDguJvguYfguJnguYTguJvguYTguJTguYnguKrguLPguKvguKPguLHguJrguIHguLLguKPguYDguKXguYjguJnguKrguK3guIfguKvguKPguLfguK3guKrguLLguKHguITguKPguLHguYnguIcg4LiZ4Lit4LiB4LiI4Liy4LiB4LiZ4Li14LmJ4Lii4Lix4LiH4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lil4LmI4LiZ4LmE4LiU4LmJ4LmC4LiU4Lii4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4Lih4Liy4LiB4LiB4Lin4LmI4Liy4Liq4Li14LmI4LiE4LiZ4LmC4LiU4Lii4LmD4LiK4LmJ4LiL4Lit4LiH4LiB4Liy4Lij4LmM4LiUIDUyIOC5g+C4muC4quC4reC4h+C4iuC4uOC4lOC4quC4seC4muC4geC4seC4mVxuXG7guYLguJTguKLguJvguIHguJXguLTguYDguIHguKHguIjguLDguYHguIjguIHguYTguJ7guYjguYHguKXguLDguYDguKXguYjguJnguJXguLLguKHguYDguILguYfguKHguJnguLLguKzguLTguIHguLIg4LmB4LiV4LmI4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lil4LmI4LiZ4LiX4Lin4LiZ4LmA4LiC4LmH4Lih4LiZ4Liy4Lis4Li04LiB4Liy4LmB4LiX4LiZ4LmE4LiU4LmJ4Lir4Liy4LiB4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4LiV4LiB4Lil4LiH4Lil4LmI4Lin4LiH4Lir4LiZ4LmJ4Liy4LiX4Li14LmI4LiI4Liw4LiX4Liz4LmA4LiK4LmI4LiZ4LiZ4Lix4LmJ4LiZXG5cbuC4geC4suC4o+C4iOC4seC4lOC4reC4seC4meC4lOC4seC4muC5hOC4nuC5iOC4hOC4t+C4reC4quC4reC4hyAo4Liq4Li54LiH4Liq4Li44LiUKSwg4LmA4Lit4LiLLCDguITguLTguIcsIOC4hOC4p+C4teC4mSwg4LmB4LiI4LmH4LiELCDguKrguLTguJosIOC5gOC4geC5ieC4siwg4LmB4Lib4LiULCDguYDguIjguYfguJQsIOC4q+C4gSwg4Lir4LmJ4LiyLCDguKrguLXguYgsIOC4quC4suC4oSAo4LiV4LmI4Liz4Liq4Li44LiUKVxuXG7guKDguLLguKLguYPguJnguYHguJXguYjguKXguLDguK3guLHguJnguJTguLHguJrguKLguLHguIfguKHguLXguKXguLPguJTguLHguJrguILguK3guIfguIrguLjguJQ6IEhlYXJ0cyAo4Liq4Li54LiH4Liq4Li44LiUKSwgRGlhbW9uZHMsIENsdWJzLCBTcGFkZXMgKOC4leC5iOC4s+C4quC4uOC4lClcblxu4LiU4Lix4LiH4LiZ4Lix4LmJ4LiZ4LmE4Lie4LmIIDMg4LmC4Lie4LiU4Liz4LiI4Li24LiH4LmA4Lib4LmH4LiZ4LmE4Lie4LmI4LiX4Li14LmI4LiV4LmI4Liz4LiX4Li14LmI4Liq4Li44LiU4LmD4LiZ4LmB4Lie4LmH4LiE4LmB4Lil4Liw4LmE4Lie4LmIIDIg4LmD4Lia4LiC4Lit4LiHIEhlYXJ0cyDguJnguLHguYnguJnguKrguLnguIfguJfguLXguYjguKrguLjguJQg4Lit4Lix4LiZ4LiU4Lix4Lia4Lih4Li14LiE4Lin4Liy4Lih4Liq4Liz4LiE4Lix4LiN4Lih4Liy4LiB4LiB4Lin4LmI4Liy4LiE4Lin4Liy4Lih4LmA4Lir4Lih4Liy4Liw4Liq4Lih4LiV4Lix4Lin4Lit4Lii4LmI4Liy4LiH4LmA4LiK4LmI4LiZIDgg4LiK4LiZ4LiwIDdcblxu4LiC4LmJ4Lit4LiV4LiB4Lil4LiHXG5cbuC4quC4s+C4q+C4o+C4seC4muC5gOC4geC4oeC5geC4o+C4geC5gOC4iOC5ieC4suC4oeC4t+C4reC4iOC4sOC4luC4ueC4geC5gOC4peC4t+C4reC4geC5geC4muC4muC4quC4uOC5iOC4oSDguIjguLLguIHguJnguLHguYnguJnguJzguLnguYnguYHguJ7guYnguILguK3guIfguYHguJXguYjguKXguLDguYDguIHguKHguIjguLDguJXguYnguK3guIfguIjguLHguJTguIHguLLguKPguJXguYjguK3guYTguJsg4LmA4Lih4Li34LmI4Lit4Lih4Li14Lic4Li54LmJ4LmA4Lil4LmI4LiZ4Liq4Li14LmI4LiE4LiZ4LmE4Lie4LmIIDEzIOC5g+C4muC4iOC4sOC4luC4ueC4geC5geC4iOC4geC5g+C4q+C5ieC4geC4seC4muC4nOC4ueC5ieC5gOC4peC5iOC4meC5geC4leC5iOC4peC4sOC4hOC4mVxuXG7guKvguLLguIHguKHguLXguJzguLnguYnguYDguKXguYjguJnguJnguYnguK3guKLguIHguKfguYjguLLguKrguLXguYjguITguJnguIjguLDguKLguLHguIfguITguIfguYHguIjguIHguYTguJ7guYggMTMg4LmD4Lia4LmD4Lir4LmJ4LiB4Lix4Lia4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4LmB4LiV4LmI4Lil4Liw4LiE4LiZ4LmB4Lil4Liw4LiI4Liw4Lih4Li14LmE4Lie4LmI4LmA4Lir4Lil4Li34Lit4Lit4Lii4Li54LmI4LiL4Li24LmI4LiH4LmE4Lih4LmI4LmE4LiU4LmJ4LmD4LiK4LmJ4LmD4LiZ4LmA4LiB4LihIOC4reC4teC4geC4l+C4suC4h+C5gOC4peC4t+C4reC4geC4q+C4meC4tuC5iOC4h+C4quC4s+C4q+C4o+C4seC4muC4nOC4ueC5ieC5gOC4peC5iOC4meC4quC4suC4oeC4hOC4meC4hOC4t+C4reC4leC4suC4oeC4guC5ieC4reC4leC4geC4peC4h+C4geC5iOC4reC4meC4l+C4teC5iOC4iOC4sOC5geC4iOC4geC5hOC4nuC5iCAxNyDguYPguJrguJXguYjguK3guITguJkg4LmA4Lih4Li34LmI4Lit4Lih4Li14Lic4Li54LmJ4LmA4Lil4LmI4LiZ4LmA4Lie4Li14Lii4LiH4Liq4Lit4LiH4LiE4LiZ4LiE4Lin4Lij4LmB4LiI4LiB4LmE4Lie4LmI4LmD4Lir4LmJ4LiE4LiZ4Lil4LiwIDEzIOC5g+C4muC5gOC4l+C5iOC4suC4meC4seC5ieC4mSAtIOC4q+C4suC4geC5hOC4nuC5iOC4l+C4seC5ieC4h+C4q+C4oeC4lOC5hOC4lOC5ieC4o+C4seC4muC4geC4suC4o+C5geC4iOC4geC5geC4iOC4h+C4nOC4ueC5ieC5gOC4peC5iOC4meC4iOC4sOC4quC4suC4oeC4suC4o+C4luC5g+C4iuC5ieC4oeC4t+C4reC4guC4reC4h+C4geC4seC4meC5geC4peC4sOC4geC4seC4meC5hOC4lOC5ieC4i+C4tuC5iOC4h+C4iOC4sOC4l+C4s+C5g+C4q+C5ieC5gOC4geC4oeC5gOC4quC4teC4oiDguYDguKHguLfguYjguK3guKHguLXguJzguLnguYnguYDguKXguYjguJnguKHguLLguIHguIHguKfguYjguLLguKrguLXguYjguITguJnguITguLjguJPguKrguLLguKHguLLguKPguJbguJXguIHguKXguIfguKXguYjguKfguIfguKvguJnguYnguLLguYTguJTguYnguKfguYjguLLguIjguLDguYHguIjguIHguYTguJ7guYggMTMg4LmD4Lia4LiI4Liy4LiB4LmE4Lie4LmI4Liq4Lit4LiH4Liq4Liz4Lij4Lix4Lia4Lir4Lij4Li34Lit4LmB4LiI4LiB4LmE4Lie4LmI4LmD4Lir4LmJ4Lih4Liy4LiB4LiX4Li14LmI4Liq4Li44LiU4LmA4LiX4LmI4Liy4LiX4Li14LmI4LiI4Liw4LiX4Liz4LmE4LiU4LmJ4LmD4Lir4LmJ4LiB4Lix4Lia4Lic4Li54LmJ4LmA4Lil4LmI4LiZXG5cbuC4geC4suC4o+C5gOC4peC5iOC4mVxuXG7guYPguJnguYDguIHguKHguYHguKPguIHguYDguJfguYjguLLguJnguLHguYnguJnguJzguLnguYnguYDguKXguYjguJnguJfguLXguYjguKHguLUgMyBTcGFkZXMg4LiI4Liw4LmA4Lij4Li04LmI4Lih4LmA4Lil4LmI4LiZIOC4q+C4suC4geC5hOC4oeC5iOC4oeC4teC5g+C4hOC4o+C4oeC4teC5hOC4nuC5iCAzIOC5g+C4miAo4LmD4LiZ4LmA4LiB4Lih4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4Liq4Liy4Lih4Lir4Lij4Li34Lit4Liq4Lit4LiH4LiE4LiZKSDguYPguITguKPguIHguYfguJXguLLguKHguJfguLXguYjguJbguLfguK3guYTguJ7guYjguJXguYjguLPguKrguLjguJTguIjguLDguYDguKPguLTguYjguKHguJXguYnguJkg4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4LiI4Liw4LiV4LmJ4Lit4LiH4LmA4Lij4Li04LmI4Lih4LiV4LmJ4LiZ4LiU4LmJ4Lin4Lii4LiB4Liy4Lij4LmA4Lil4LmI4LiZ4LmE4Lie4LmI4LmD4Lia4LiX4Li14LmI4LiV4LmI4Liz4LiX4Li14LmI4Liq4Li44LiU4LiZ4Li14LmJ4LmE4Lih4LmI4Lin4LmI4Liy4LiI4Liw4LiU4LmJ4Lin4Lii4LiV4Lix4Lin4LmA4Lit4LiH4Lir4Lij4Li34Lit4LmA4Lib4LmH4LiZ4Liq4LmI4Lin4LiZ4Lir4LiZ4Li24LmI4LiH4LiC4Lit4LiH4LiB4Liy4Lij4Lij4Lin4Lih4LiB4Lix4LiZXG5cbuC5g+C4meC5gOC4geC4oeC4leC5iOC4rSDguYYg4LmE4Lib4Lic4Li54LmJ4LiK4LiZ4Liw4LiC4Lit4LiH4LmA4LiB4Lih4LiB4LmI4Lit4LiZ4Lir4LiZ4LmJ4Liy4LiI4Liw4LmE4LiU4LmJ4LmA4Lil4LmI4LiZ4LiB4LmI4Lit4LiZ4LmB4Lil4Liw4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lij4Li04LmI4Lih4LiV4LmJ4LiZ4LiU4LmJ4Lin4Lii4LiK4Li44LiU4LiE4LmI4Liy4Lic4Liq4Lih4LmD4LiU4LiB4LmH4LmE4LiU4LmJXG5cbuC5g+C4meC4l+C4suC4h+C4geC4peC4seC4muC4geC4seC4meC4nOC4ueC5ieC5gOC4peC5iOC4meC5geC4leC5iOC4peC4sOC4hOC4meC4iOC4sOC4leC5ieC4reC4h+C5gOC4reC4suC4iuC4meC4sOC5hOC4nuC5iOC4l+C4teC5iOC5gOC4peC5iOC4meC4geC5iOC4reC4meC4q+C4meC5ieC4suC4meC4teC5ieC4q+C4o+C4t+C4reC5hOC4nuC5iOC4nOC4quC4oeC4geC4seC4meC5guC4lOC4ouC4geC4suC4o+C5gOC4peC5iOC4meC5hOC4nuC5iOC4q+C4o+C4t+C4reC5hOC4nuC5iOC4o+C4p+C4oeC4geC4seC4meC4l+C4teC5iOC4iuC4meC4sOC4q+C4o+C4t+C4reC4quC5iOC4h+C4nOC5iOC4suC4meC5geC4peC4sOC5hOC4oeC5iOC5gOC4peC5iOC4meC5hOC4nuC5iOC5g+C4lCDguYYg4LiB4Liy4Lij4LmM4LiU4LiX4Li14LmI4LmA4Lil4LmI4LiZ4LiI4Liw4LiW4Li54LiB4Lin4Liy4LiH4LmD4LiZ4LiB4Lit4LiH4LmC4LiU4Lii4Lir4LiH4Liy4Lii4Lir4LiZ4LmJ4Liy4LiC4Li24LmJ4LiZ4LiV4Lij4LiH4LiB4Lil4Liy4LiH4LmC4LiV4LmK4LiwIOC4geC4suC4o+C5gOC4peC5iOC4meC4iOC4sOC4lOC4s+C5gOC4meC4tOC4meC5hOC4m+C4o+C4reC4miDguYYg4LmC4LiV4LmK4Liw4Lir4Lil4Liy4LiiIOC5hiDguITguKPguLHguYnguIfguYDguJfguYjguLLguJfguLXguYjguIjguLPguYDguJvguYfguJnguIjguJnguIHguKfguYjguLLguIjguLDguKHguLXguITguJnguYDguKXguYjguJnguYTguJ7guYjguKvguKPguLfguK3guIrguLjguJTguJfguLXguYjguYTguKHguYjguKHguLXguYPguITguKPguIrguJnguLAg4LmA4Lih4Li34LmI4Lit4LmA4LiB4Li04LiU4LmA4Lir4LiV4Li44LiB4Liy4Lij4LiT4LmM4LmA4LiK4LmI4LiZ4LiZ4Li14LmJ4LmE4Lie4LmI4LiX4Li14LmI4LmA4Lil4LmI4LiZ4LiX4Lix4LmJ4LiH4Lir4Lih4LiU4LiI4Liw4LiW4Li54LiB4Lin4Liy4LiH4LiX4Li04LmJ4LiH4LmE4Lin4LmJ4LmB4Lil4Liw4Lic4Li54LmJ4LiX4Li14LmI4LmA4Lil4LmI4LiZ4LmE4LiU4LmJ4LmE4Lih4LmI4LmB4Lie4LmJ4LmD4LiE4Lij4LiI4Liw4LmA4Lij4Li04LmI4Lih4Lit4Li14LiB4LiE4Lij4Lix4LmJ4LiH4LmC4LiU4Lii4LiB4Liy4Lij4LmA4Lil4LmI4LiZ4LmE4Lie4LmI4LiV4Liy4Lih4LiB4LiO4Lir4Lih4Liy4Lii4Lir4Lij4Li34Lit4LmE4Lie4LmI4Lic4Liq4Lih4LmD4LiUIOC5hiDguYLguJTguKLguKvguLHguJnguKvguJnguYnguLLguYDguILguYnguLLguKvguLLguIHguKXguLLguIfguYLguJXguYrguLBcblxu4Lir4Liy4LiB4LiE4Li44LiT4Lic4LmI4Liy4LiZ4LmE4Lib4LiE4Li44LiT4LiI4Liw4LiW4Li54LiB4Lil4LmH4Lit4LiB4LmE4Lih4LmI4LmD4Lir4LmJ4LmA4Lil4LmI4LiZ4LiI4LiZ4LiB4Lin4LmI4Liy4LiI4Liw4Lih4Li14LiE4LiZ4LmA4Lil4LmI4LiZ4LmC4LiU4Lii4LiX4Li14LmI4LmE4Lih4LmI4Lih4Li14LmD4LiE4Lij4LiK4LiZ4LiwIOC4hOC4uOC4k+C4oeC4teC4quC4tOC4l+C4mOC4tOC5jOC5gOC4peC5iOC4meC5hOC4nuC5iOC5g+C4q+C4oeC5iOC4reC4teC4geC4hOC4o+C4seC5ieC4h+C5gOC4l+C5iOC4suC4meC4seC5ieC4mVxuXG7guJXguLHguKfguK3guKLguYjguLLguIcgKOC4oeC4teC4nOC4ueC5ieC5gOC4peC5iOC4meC4quC4suC4oeC4hOC4mSk6IOC4nOC4ueC5ieC5gOC4peC5iOC4meC4l+C4suC4h+C4guC4p+C4suC4guC4reC4h+C4hOC4uOC4k+C5gOC4peC5iOC4meC4quC4suC4oeC4hOC4meC5gOC4lOC4teC4ouC4p+C4hOC4uOC4k+C4luC4t+C4reC5gOC4reC4iyDguYHguJXguYjguJXguLHguJTguKrguLTguJnguYPguIjguJfguLXguYjguIjguLDguKrguYjguIfguJzguYjguLLguJnguJzguLnguYnguYDguKXguYjguJnguJfguLLguIfguIvguYnguLLguKLguILguK3guIfguITguLjguJPguYDguKXguYjguJnguYDguIHguYnguLLguITguJnguYHguKXguLDguJzguLnguYnguYDguKXguYjguJnguJfguLLguIfguILguKfguLLguYDguKXguYjguJnguYDguJvguYfguJnguKPguLLguIrguLIg4LiV4Lit4LiZ4LiZ4Li14LmJ4LiE4Li44LiT4LmE4Lih4LmI4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lit4Liy4LiK4LiZ4Liw4Lij4Liy4LiK4Liy4LiU4LmJ4Lin4Lii4LmA4Lit4LiL4LiC4Lit4LiH4LiE4Li44LiT4LmE4LiU4LmJ4LmA4Lie4Lij4Liy4Liw4LiE4Li44LiT4Lic4LmI4Liy4LiZ4LmE4Lib4LmB4Lil4LmJ4LinIOC4q+C4suC4geC4nOC4ueC5ieC5gOC4peC5iOC4meC4hOC4meC4l+C4teC5iOC4quC4suC4oeC4nOC5iOC4suC4meC5hOC4m+C4lOC5ieC4p+C4ouC5geC4peC4sOC4neC5iOC4suC4ouC4leC4o+C4h+C4guC5ieC4suC4oeC4oeC4t+C4reC4guC4p+C4suC4guC4reC4h+C4hOC4uOC4k+C4iOC4sOC5gOC4m+C5h+C4meC4o+C4suC4iuC4tOC4meC4teC4leC4reC4meC4meC4teC5ieC4hOC4uOC4k+C4quC4suC4oeC4suC4o+C4luC5gOC4peC5iOC4meC5gOC4reC4i+C4guC4reC4h+C4hOC4uOC4k+C5hOC4lOC5ieC4q+C4suC4geC4leC5ieC4reC4h+C4geC4suC4o1xuXG7guIHguLLguKPguYDguKXguYjguJnguJXguLLguKHguIHguI7guKvguKHguLLguKLguYPguJnguYDguIHguKHguKHguLXguJTguLHguIfguJnguLXguYk6XG7guYTguJ7guYjguYPguJrguYDguJTguLXguKLguKfguYTguJ7guYjguYPguJrguYDguJTguLXguKLguKfguJfguLXguYjguJXguYjguLPguJfguLXguYjguKrguLjguJTguITguLfguK3guYTguJ7guYggMyDguYPguJrguYHguKXguLDguYPguJrguJfguLXguYjguKrguLnguIfguJfguLXguYjguKrguLjguJTguITguLfguK3guYTguJ7guYggMiDguYPguJpcbuC4iOC4seC4muC4hOC4ueC5iOC5hOC4nuC5iOC4quC4reC4h+C5g+C4muC4l+C4teC5iOC4oeC4teC4reC4seC4meC4lOC4seC4muC5gOC4lOC4teC4ouC4p+C4geC4seC4meC5gOC4iuC5iOC4mSA3LTcg4Lir4Lij4Li34LitIFEtUVxu4LmE4Lie4LmI4Liq4Liy4Lih4LmD4Lia4LmD4LiZ4Lit4Lix4LiZ4LiU4Lix4Lia4LmA4LiU4Li14Lii4Lin4LiB4Lix4LiZIC0g4LmA4LiK4LmI4LiZIDUtNS01XG7guYTguJ7guYjguKrguLXguYjguYPguJrguJfguLXguYjguKHguLXguK3guLHguJnguJTguLHguJrguYDguJTguLXguKLguKfguIHguLHguJkgLSDguYDguIrguYjguJkgOS05LTktOVxu4Lil4Liz4LiU4Lix4Lia4LmE4Lie4LmI4Liq4Liy4Lih4LmD4Lia4LiC4Li24LmJ4LiZ4LmE4Lib4LiX4Li14LmI4Lih4Li14Lil4Liz4LiU4Lix4Lia4LiV4LmI4Lit4LmA4LiZ4Li34LmI4Lit4LiH4LiB4Lix4LiZICjguKrguLLguKHguLLguKPguJbguJzguKrguKHguIrguLjguJTguYTguJTguYkpIOC5gOC4iuC5iOC4mSA0LTUtNiDguKvguKPguLfguK0gSi1RLUstQSDguKXguLPguJTguLHguJrguYTguKHguYjguKrguLLguKHguLLguKPguJYgXCLguJ7guKXguLTguIHguKHguLjguKFcIiDguKPguLDguKvguKfguYjguLLguIfguKrguK3guIfguJbguLbguIfguKrguLLguKEgLSBBLTItMyDguYTguKHguYjguYPguIrguYjguKXguLPguJTguLHguJrguJfguLXguYjguJbguLnguIHguJXguYnguK3guIfguYDguJnguLfguYjguK3guIfguIjguLLguIEgMiDguKrguLnguIfguYHguKXguLAgMyDguJXguYjguLNcbuC4peC4s+C4lOC4seC4muC4hOC4ueC5iCAzIOC4hOC4ueC5iOC4guC4tuC5ieC4meC5hOC4m+C4guC4reC4h+C4reC4seC4meC4lOC4seC4muC4l+C4teC5iOC4leC5iOC4reC5gOC4meC4t+C5iOC4reC4h+C4geC4seC4meC5gOC4iuC5iOC4mSAzLTMtNC00LTUtNSDguKvguKPguLfguK0gNi02LTctNy04LTgtOS05XG5cbuC5guC4lOC4ouC4l+C4seC5iOC4p+C5hOC4m+C4geC4suC4o+C4nOC4quC4oeC4iOC4sOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC5hOC4lOC5ieC5guC4lOC4ouC4geC4suC4o+C4nOC4quC4oeC4l+C4teC5iOC4quC4ueC4h+C4geC4p+C5iOC4suC4guC4reC4h+C5hOC4nuC5iOC4m+C4o+C4sOC5gOC4oOC4l+C5gOC4lOC4teC4ouC4p+C4geC4seC4meC5geC4peC4sOC4iOC4s+C4meC4p+C4meC5hOC4nuC5iOC5gOC4l+C5iOC4suC4geC4seC4mSDguJTguLHguIfguJnguLHguYnguJnguKvguLLguIHguJnguLPguYTguJ7guYjguYPguJrguYDguJTguLXguKLguKfguIjguLDguKrguLLguKHguLLguKPguJbguYDguKXguYjguJnguYTguJ7guYjguYPguJrguYDguJTguLXguKLguKfguYTguJTguYnguYDguJfguYjguLLguJnguLHguYnguJkg4LiW4LmJ4Liy4LiE4Li54LmI4LiZ4Liz4LmA4LiX4LmI4Liy4LiZ4Lix4LmJ4LiZ4LiX4Li14LmI4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lil4LmI4LiZ4LmE4LiU4LmJOyDguKXguLPguJTguLHguJrguYTguJ7guYjguKrguLLguKHguYPguJrguKrguLLguKHguLLguKPguJbguYDguK3guLLguIrguJnguLDguYTguJTguYnguYLguJTguKLguKXguLPguJTguLHguJrguYTguJ7guYjguKrguLLguKHguYPguJrguJfguLXguYjguKrguLnguIfguIHguKfguYjguLLguYDguJfguYjguLLguJnguLHguYnguJkg4LmB4Lil4Liw4Lit4Li34LmI4LiZIOC5hiDguJXguLHguKfguK3guKLguYjguLLguIfguYDguIrguYjguJnguITguLjguJPguYTguKHguYjguKrguLLguKHguLLguKPguJbguYDguK3guLLguIrguJnguLDguITguLnguYjguJTguYnguKfguKLguYTguJ7guYjguKrguLLguKHguYPguJrguKvguKPguLfguK3guKXguLPguJTguLHguJrguYTguJ7guYjguKrguLXguYjguYPguJrguJfguLXguYjguKHguLXguKXguLPguJTguLHguJrguYTguJ7guYjguKvguYnguLLguYPguJpcblxu4LmD4LiZ4LiB4Liy4Lij4LiV4Lix4LiU4Liq4Li04LiZ4LmD4LiI4Lin4LmI4Liy4LiK4Li44LiU4LiE4LmI4Liy4Lic4Liq4Lih4Lib4Lij4Liw4LmA4Lig4LiX4LmA4LiU4Li14Lii4Lin4LiB4Lix4LiZ4LmD4LiU4Liq4Li54LiH4LiB4Lin4LmI4Liy4LiE4Li44LiT4LmA4Lie4Li14Lii4LiH4LmB4LiE4LmI4LiU4Li54LmE4Lie4LmI4LiX4Li14LmI4Liq4Li54LiH4LiX4Li14LmI4Liq4Li44LiU4LmD4LiZ4LiK4Li44LiU4LiE4LmI4Liy4Lic4Liq4LihIOC4leC4seC4p+C4reC4ouC5iOC4suC4h+C5gOC4iuC5iOC4mSA3LTcg4LmA4LiV4LmJ4LiZIDctNyDguYDguJ7guKPguLLguLDguKvguLHguKfguYPguIjguYDguJXguYnguJnguYDguJ7guIrguKMg4LmD4LiZ4LiX4Liz4LiZ4Lit4LiH4LmA4LiU4Li14Lii4Lin4LiB4Lix4LiZIDgtOS0xMCDguYDguJXguYnguJkgOC05LTEwIOC5gOC4nuC4o+C4suC4sOC5gOC4m+C5h+C4meC5hOC4nuC5iOC4quC4ueC4h+C4quC4uOC4lCAo4Lir4Lil4Lix4LiB4Liq4Li04LiaKSDguJfguLXguYjguJnguLPguKHguLLguYDguJvguKPguLXguKLguJrguYDguJfguLXguKLguJrguIHguLHguJlcblxu4Lih4Li14LiC4LmJ4Lit4Lii4LiB4LmA4Lin4LmJ4LiZ4LmA4Lie4Li14Lii4LiH4Liq4Li14LmI4LiC4LmJ4Lit4Liq4Liz4Lir4Lij4Lix4Lia4LiB4LiO4LiX4Li14LmI4Lin4LmI4Liy4LiK4Li44LiU4LiE4LmI4Liy4Lic4Liq4Lih4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lit4Liy4LiK4LiZ4Liw4LmE4LiU4LmJ4LmC4LiU4Lii4LiB4Liy4Lij4Lij4Lin4Lih4LiB4Lix4LiZ4LiC4Lit4LiH4Lib4Lij4Liw4LmA4Lig4LiX4LmA4LiU4Li14Lii4Lin4LiB4Lix4LiZ4LmA4LiX4LmI4Liy4LiZ4Lix4LmJ4LiZOlxuXG7guYTguJ7guYjguKrguLXguYjguYPguJrguKrguLLguKHguLLguKPguJbguYDguK3guLLguIrguJnguLDguYTguJ7guYjguKrguK3guIfguYPguJrguYPguJTguIHguYfguYTguJTguYkgKOC5geC4leC5iOC5hOC4oeC5iOC5g+C4iuC5iOC5hOC4nuC5iOC5g+C4muC5gOC4lOC4teC4ouC4p+C4reC4t+C5iOC4mSDguYYg4LmA4LiK4LmI4LiZ4LmA4Lit4LiL4Lir4Lij4Li34Lit4Lij4Liy4LiK4LiyKSDguKrguLXguYjguIrguJnguLTguJTguKrguLLguKHguLLguKPguJbguYDguK3guLLguIrguJnguLDguYTguJTguYnguJTguYnguKfguKLguKrguLXguYjguIrguJnguLTguJTguJfguLXguYjguKrguLnguIfguIHguKfguYjguLJcblxu4Lil4Liz4LiU4Lix4Lia4LiC4Lit4LiH4Liq4Liy4Lih4LiE4Li54LmIICjguYDguIrguYjguJkgNy03LTgtOC05LTkpIOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC5hOC4nuC5iOC4quC4reC4h+C5g+C4muC5g+C4lOC4geC5h+C5hOC4lOC5iSAo4LmB4LiV4LmI4LmE4Lih4LmI4LmD4LiK4LmI4LmE4Lie4LmI4LmD4Lia4LmA4LiU4Li14Lii4Lin4Lit4Li34LmI4LiZIOC5hiApIOC4peC4s+C4lOC4seC4muC4guC4reC4h+C4quC4suC4oeC4hOC4ueC5iOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC5guC4lOC4ouC4peC4s+C4lOC4seC4muC4l+C4teC5iOC4quC4ueC4h+C4geC4p+C5iOC4suC4guC4reC4h+C4quC4suC4oeC4hOC4ueC5iFxuXG7guKXguLPguJTguLHguJrguILguK3guIfguKrguLXguYjguITguLnguYggKOC5gOC4iuC5iOC4mSA1LTUtNi02LTctNy04LTgpIOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC4hOC4ueC5iOC4guC4reC4h+C4quC4reC4h+C4hOC4ueC5iOC5hOC4lOC5iSAo4LmB4LiV4LmI4LmE4Lih4LmI4LmD4LiK4LmI4LiE4Li54LmI4Lit4Li34LmI4LiZIOC5hiApIOC4peC4s+C4lOC4seC4muC4guC4reC4h+C4quC4teC5iOC4hOC4ueC5iOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC5guC4lOC4ouC4peC4s+C4lOC4seC4muC4l+C4teC5iOC4quC4ueC4h+C4geC4p+C5iOC4suC4guC4reC4h+C4quC4teC5iOC4hOC4ueC5iFxuXG7guKXguLPguJTguLHguJrguILguK3guIfguKvguYnguLLguITguLnguYggKOC5gOC4iuC5iOC4mSA4LTgtOS05LTEwLTEwLUotSi1RLVEpIOC4quC4suC4oeC4suC4o+C4luC5gOC4reC4suC4iuC4meC4sOC5gOC4i+C4leC4guC4reC4h+C4quC4suC4oeC4hOC4ueC5iOC5hOC4lOC5iSAo4LmB4LiV4LmI4LmE4Lih4LmI4LmD4LiK4LmI4Lit4Li14LiB4Liq4Liy4Lih4LiE4Li54LmIKSDguKXguLPguJTguLHguJrguILguK3guIfguKvguYnguLLguITguLnguYjguKrguLLguKHguLLguKPguJbguYDguK3guLLguIrguJnguLDguYLguJTguKLguKXguLPguJTguLHguJrguJfguLXguYjguKrguLnguIfguIHguKfguYjguLLguILguK3guIfguKvguYnguLLguITguLnguYhcblxu4LiK4Li44LiU4LiE4LmI4Liy4Lic4Liq4Lih4LmA4Lir4Lil4LmI4Liy4LiZ4Li14LmJ4LiX4Li14LmI4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lit4Liy4LiK4LiZ4Liw4LiE4Li54LmI4LmA4LiU4Li14LmI4Lii4Lin4Lir4Lij4Li34Lit4LiK4Li44LiU4Liq4Lit4LiH4LiK4Li44LiU4Lia4Liy4LiH4LiE4Lij4Lix4LmJ4LiH4LmA4Lij4Li14Lii4LiB4Lin4LmI4Liy4Lij4Liw4LmA4Lia4Li04LiU4Lir4Lij4Li34Lit4Lij4Liw4LmA4Lia4Li04LiU4Liq4Lit4LiH4Lil4Li54LiB4LmB4Lil4Liw4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lil4LmI4LiZ4LmE4LiU4LmJ4LmB4Lih4LmJ4LiB4Lij4Liw4LiX4Lix4LmI4LiH4Lic4Li54LmJ4LmA4Lil4LmI4LiZ4LiX4Li14LmI4Lic4LmI4Liy4LiZ4Lih4Liy4LiB4LmI4Lit4LiZ4Lir4LiZ4LmJ4Liy4LiZ4Li14LmJXG5cbuC5guC4m+C4o+C4lOC4l+C4o+C4suC4muC4p+C5iOC4suC4guC5ieC4reC4ouC4geC5gOC4p+C5ieC4meC5gOC4q+C4peC5iOC4suC4meC4teC5ieC5g+C4iuC5ieC4geC4seC4muC4geC4suC4o+C4leC4teC4quC4reC4h+C5g+C4muC5gOC4l+C5iOC4suC4meC4seC5ieC4meC5hOC4oeC5iOC5g+C4iuC5iOC5hOC4nuC5iOC4reC4t+C5iOC4mSDguYYg4LiV4Lix4Lin4Lit4Lii4LmI4Liy4LiH4LmA4LiK4LmI4LiZ4LiW4LmJ4Liy4Lih4Li14LiE4LiZ4LmA4Lil4LmI4LiZ4LmA4Lit4LiL4LiE4Li44LiT4LiI4Liw4LmE4Lih4LmI4Liq4Liy4Lih4Liy4Lij4LiW4LmA4Lit4Liy4LiK4LiZ4Liw4Lih4Lix4LiZ4LiU4LmJ4Lin4Lii4LmA4Lit4LiL4LiX4Lix4LmJ4LiH4Liq4Li14LmI4LiC4Lit4LiH4LiE4Li44LiT4LmE4LiU4LmJIOC5geC4leC5iOC4luC5ieC4suC5gOC4reC4i+C4luC4ueC4geC4quC4reC4h+C4hOC4meC4q+C4meC4tuC5iOC4h+C4hOC4meC4l+C4seC5ieC4h+C4quC4teC5iOC4guC4reC4h+C4hOC4uOC4k+C4geC5h+C4quC4suC4oeC4suC4o+C4luC5g+C4iuC5ieC5gOC4nuC4t+C5iOC4reC5gOC4reC4suC4iuC4meC4sOC4l+C4seC5ieC4h+C4quC4reC4h+C5hOC4lOC5iWBcblxuZXhwb3J0IGRlZmF1bHQgdHV0b3I7XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/th_TH.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3b07by6DGRJu75lIR9EK7nZ', 'th_TH');
// lang/th_TH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'เล่นเลย',
    'WIN': 'ที่ 1',
    'LOSE2': 'ครั้งที่ 2',
    'LOSE3': 'วันที่ 3',
    'LOSE4': 'อันดับ 4',
    'PLAYER': 'จำนวนผู้เล่น',
    'BETNO': 'เดิมพัน',
    'DAILY': 'รางวัลรายวัน',
    'D1': 'วันที่ 1',
    'D2': 'วันที่ 2',
    'D3': 'วันที่ 3',
    'D4': 'วันที่ 4',
    'D5': 'วันที่ 5',
    'D6': 'วันที่ 6',
    'D7': 'วันที่ 7',
    '3TURNS': '3 รอบ',
    '5TURNS': '5 รอบ',
    'TURNS': 'รอบ',
    'RECEIVED': 'ได้รับ',
    'LEADER': 'ลีดเดอร์บอร์ด',
    'NOVIDEO': 'ไม่สามารถเล่นวิดีโอได้ในขณะนี้',
    'BET': 'เดิมพัน',
    'NO': 'จำนวน',
    'PASS': 'ผ่าน',
    'HIT': 'ตี',
    'ARRANGE': 'จัด',
    'QUITGAME': 'ออกจากเกม',
    'QUITGAMEP': 'คุณต้องการออกจากเกมหรือไม่? หากคุณออกจากเกมคุณจะเสียสิบเท่าของระดับการเดิมพัน',
    'QUIT': 'เลิก',
    '3PAIRS': '3 คู่ติดต่อกัน',
    '4PAIRS': '4 คู่ติดต่อกัน',
    'FOURKIND': 'สี่ชนิด',
    'FLUSH': 'สเตรทฟลัช',
    '1BEST': 'ดีที่สุด',
    '2BEST': '2 ดีที่สุด',
    '3BEST': '3 ดีที่สุด',
    'INSTRUCT': 'คำแนะนำ',
    'NOMONEY': 'คุณหมดเงินคลิกเพื่อรับเงินเพิ่ม',
    'RECEI2': 'ได้รับ',
    'SPINNOW': 'หมุน',
    'SPIN': 'หมุน',
    '1TURN': 'อีก 1 เทิร์น',
    'LUCKYSPIN': 'หมุนโชคดี',
    'LUCK': 'มีโชคในภายหลัง',
    'TURN': 'อีกหนึ่งเทิร์น',
    'X2': 'X2 เงิน',
    'X3': 'X3 เงิน',
    'X5': 'X5 เงิน',
    'X10': 'X10 เงิน',
    'MISS': 'นางสาว',
    'MONEY1': 'ยินดีด้วย! คุณมีเงิน %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RoX1RILnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQStCO0FBRS9CLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFNBQVM7SUFDcEIsS0FBSyxFQUFFLE9BQU87SUFDZCxPQUFPLEVBQUUsWUFBWTtJQUNyQixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsVUFBVTtJQUNuQixRQUFRLEVBQUUsY0FBYztJQUN4QixPQUFPLEVBQUUsU0FBUztJQUNsQixPQUFPLEVBQUUsY0FBYztJQUN2QixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixRQUFRLEVBQUUsT0FBTztJQUNqQixRQUFRLEVBQUUsT0FBTztJQUNqQixPQUFPLEVBQUUsS0FBSztJQUNkLFVBQVUsRUFBRSxRQUFRO0lBQ3BCLFFBQVEsRUFBRSxlQUFlO0lBQ3pCLFNBQVMsRUFBRSxnQ0FBZ0M7SUFDM0MsS0FBSyxFQUFFLFNBQVM7SUFDaEIsSUFBSSxFQUFFLE9BQU87SUFDYixNQUFNLEVBQUUsTUFBTTtJQUNkLEtBQUssRUFBRSxJQUFJO0lBQ1gsU0FBUyxFQUFFLEtBQUs7SUFDaEIsVUFBVSxFQUFFLFdBQVc7SUFDdkIsV0FBVyxFQUFFLCtFQUErRTtJQUM1RixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsUUFBUSxFQUFFLGdCQUFnQjtJQUMxQixVQUFVLEVBQUUsU0FBUztJQUNyQixPQUFPLEVBQUUsV0FBVztJQUNwQixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsWUFBWTtJQUNyQixPQUFPLEVBQUUsWUFBWTtJQUNyQixVQUFVLEVBQUUsU0FBUztJQUNyQixTQUFTLEVBQUUsaUNBQWlDO0lBQzVDLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLFNBQVMsRUFBRSxNQUFNO0lBQ2pCLE1BQU0sRUFBRSxNQUFNO0lBQ2QsT0FBTyxFQUFFLGNBQWM7SUFDdkIsV0FBVyxFQUFFLFdBQVc7SUFDeEIsTUFBTSxFQUFFLGdCQUFnQjtJQUN4QixNQUFNLEVBQUUsZ0JBQWdCO0lBQ3hCLElBQUksRUFBRSxTQUFTO0lBQ2YsSUFBSSxFQUFFLFNBQVM7SUFDZixJQUFJLEVBQUUsU0FBUztJQUNmLEtBQUssRUFBRSxVQUFVO0lBQ2pCLE1BQU0sRUFBRSxRQUFRO0lBQ2hCLFFBQVEsRUFBRSx5QkFBeUI7SUFDbkMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL3RoX1RIX3R1dCdcblxuZXhwb3J0IGRlZmF1bHQge1xuICAgICdQTEFZTk9XJzogJ+C5gOC4peC5iOC4meC5gOC4peC4oicsXG4gICAgJ1dJTic6ICfguJfguLXguYggMScsXG4gICAgJ0xPU0UyJzogJ+C4hOC4o+C4seC5ieC4h+C4l+C4teC5iCAyJyxcbiAgICAnTE9TRTMnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDMnLFxuICAgICdMT1NFNCc6ICfguK3guLHguJnguJTguLHguJogNCcsXG4gICAgJ1BMQVlFUic6ICfguIjguLPguJnguKfguJnguJzguLnguYnguYDguKXguYjguJknLFxuICAgICdCRVROTyc6ICfguYDguJTguLTguKHguJ7guLHguJknLFxuICAgICdEQUlMWSc6ICfguKPguLLguIfguKfguLHguKXguKPguLLguKLguKfguLHguJknLFxuICAgICdEMSc6ICfguKfguLHguJnguJfguLXguYggMScsXG4gICAgJ0QyJzogJ+C4p+C4seC4meC4l+C4teC5iCAyJyxcbiAgICAnRDMnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDMnLFxuICAgICdENCc6ICfguKfguLHguJnguJfguLXguYggNCcsXG4gICAgJ0Q1JzogJ+C4p+C4seC4meC4l+C4teC5iCA1JyxcbiAgICAnRDYnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDYnLFxuICAgICdENyc6ICfguKfguLHguJnguJfguLXguYggNycsXG4gICAgJzNUVVJOUyc6ICczIOC4o+C4reC4micsXG4gICAgJzVUVVJOUyc6ICc1IOC4o+C4reC4micsXG4gICAgJ1RVUk5TJzogJ+C4o+C4reC4micsXG4gICAgJ1JFQ0VJVkVEJzogJ+C5hOC4lOC5ieC4o+C4seC4micsXG4gICAgJ0xFQURFUic6ICfguKXguLXguJTguYDguJTguK3guKPguYzguJrguK3guKPguYzguJQnLFxuICAgICdOT1ZJREVPJzogJ+C5hOC4oeC5iOC4quC4suC4oeC4suC4o+C4luC5gOC4peC5iOC4meC4p+C4tOC4lOC4teC5guC4reC5hOC4lOC5ieC5g+C4meC4guC4k+C4sOC4meC4teC5iScsXG4gICAgJ0JFVCc6ICfguYDguJTguLTguKHguJ7guLHguJknLFxuICAgICdOTyc6ICfguIjguLPguJnguKfguJknLFxuICAgICdQQVNTJzogJ+C4nOC5iOC4suC4mScsXG4gICAgJ0hJVCc6ICfguJXguLUnLFxuICAgICdBUlJBTkdFJzogJ+C4iOC4seC4lCcsXG4gICAgJ1FVSVRHQU1FJzogJ+C4reC4reC4geC4iOC4suC4geC5gOC4geC4oScsXG4gICAgJ1FVSVRHQU1FUCc6ICfguITguLjguJPguJXguYnguK3guIfguIHguLLguKPguK3guK3guIHguIjguLLguIHguYDguIHguKHguKvguKPguLfguK3guYTguKHguYg/IOC4q+C4suC4geC4hOC4uOC4k+C4reC4reC4geC4iOC4suC4geC5gOC4geC4oeC4hOC4uOC4k+C4iOC4sOC5gOC4quC4teC4ouC4quC4tOC4muC5gOC4l+C5iOC4suC4guC4reC4h+C4o+C4sOC4lOC4seC4muC4geC4suC4o+C5gOC4lOC4tOC4oeC4nuC4seC4mScsXG4gICAgJ1FVSVQnOiAn4LmA4Lil4Li04LiBJyxcbiAgICAnM1BBSVJTJzogJzMg4LiE4Li54LmI4LiV4Li04LiU4LiV4LmI4Lit4LiB4Lix4LiZJyxcbiAgICAnNFBBSVJTJzogJzQg4LiE4Li54LmI4LiV4Li04LiU4LiV4LmI4Lit4LiB4Lix4LiZJyxcbiAgICAnRk9VUktJTkQnOiAn4Liq4Li14LmI4LiK4LiZ4Li04LiUJyxcbiAgICAnRkxVU0gnOiAn4Liq4LmA4LiV4Lij4LiX4Lif4Lil4Lix4LiKJyxcbiAgICAnMUJFU1QnOiAn4LiU4Li14LiX4Li14LmI4Liq4Li44LiUJyxcbiAgICAnMkJFU1QnOiAnMiDguJTguLXguJfguLXguYjguKrguLjguJQnLFxuICAgICczQkVTVCc6ICczIOC4lOC4teC4l+C4teC5iOC4quC4uOC4lCcsXG4gICAgJ0lOU1RSVUNUJzogJ+C4hOC4s+C5geC4meC4sOC4meC4sycsXG4gICAgJ05PTU9ORVknOiAn4LiE4Li44LiT4Lir4Lih4LiU4LmA4LiH4Li04LiZ4LiE4Lil4Li04LiB4LmA4Lie4Li34LmI4Lit4Lij4Lix4Lia4LmA4LiH4Li04LiZ4LmA4Lie4Li04LmI4LihJyxcbiAgICAnUkVDRUkyJzogJ+C5hOC4lOC5ieC4o+C4seC4micsXG4gICAgJ1NQSU5OT1cnOiAn4Lir4Lih4Li44LiZJyxcbiAgICAnU1BJTic6ICfguKvguKHguLjguJknLFxuICAgICcxVFVSTic6ICfguK3guLXguIEgMSDguYDguJfguLTguKPguYzguJknLFxuICAgICdMVUNLWVNQSU4nOiAn4Lir4Lih4Li44LiZ4LmC4LiK4LiE4LiU4Li1JyxcbiAgICAnTFVDSyc6ICfguKHguLXguYLguIrguITguYPguJnguKDguLLguKLguKvguKXguLHguIcnLFxuICAgICdUVVJOJzogJ+C4reC4teC4geC4q+C4meC4tuC5iOC4h+C5gOC4l+C4tOC4o+C5jOC4mScsXG4gICAgJ1gyJzogJ1gyIOC5gOC4h+C4tOC4mScsXG4gICAgJ1gzJzogJ1gzIOC5gOC4h+C4tOC4mScsXG4gICAgJ1g1JzogJ1g1IOC5gOC4h+C4tOC4mScsXG4gICAgJ1gxMCc6ICdYMTAg4LmA4LiH4Li04LiZJyxcbiAgICAnTUlTUyc6ICfguJnguLLguIfguKrguLLguKcnLFxuICAgICdNT05FWTEnOiAn4Lii4Li04LiZ4LiU4Li14LiU4LmJ4Lin4LiiISDguITguLjguJPguKHguLXguYDguIfguLTguJkgJXMnLFxuICAgICdUVVQnOiB0dXRvclxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/tl_PH.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '95e70d0JFBKabdpj8E+Dcll', 'tl_PH');
// lang/tl_PH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'Maglaro',
    'WIN': 'Ika-1',
    'LOSE2': 'Ika-2',
    'LOSE3': 'Ika-3',
    'LOSE4': 'Ika-4',
    'PLAYER': 'bilang ng mga manlalaro',
    'BETNO': 'Taya',
    'DAILY': 'Daily reward',
    'D1': 'Araw 1',
    'D2': 'Araw 2',
    'D3': 'Araw 3',
    'D4': 'Araw 4',
    'D5': 'Araw 5',
    'D6': 'Araw 6',
    'D7': 'Araw 7',
    '3TURNS': '3 liko',
    '5TURNS': '5 liko',
    'TURNS': 'liko',
    'RECEIVED': 'Habol',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Hindi maaaring i-play ang video ngayon',
    'BET': 'Taya',
    'NO': 'Numero',
    'PASS': 'Pumasa',
    'HIT': 'Hit',
    'ARRANGE': 'Ayusin',
    'QUITGAME': 'Tumigil sa laro',
    'QUITGAMEP': 'Kung huminto ka sa laro mawawala sa iyo ng sampung beses bilang antas ng pusta',
    'QUIT': 'Umalis na',
    '3PAIRS': '3 magkasunod na pares',
    '4PAIRS': '4 magkasunod na pares',
    'FOURKIND': 'Apat ng isang uri',
    'FLUSH': 'Straight Flush',
    '1BEST': 'pinakamahusay',
    '2BEST': '2 pinakamahusay',
    '3BEST': '3 pinakamahusay',
    'INSTRUCT': 'Panuto',
    'NOMONEY': 'Naubusan ka ng pera, mag-click upang makatanggap ng mas maraming pera',
    'RECEI2': 'Habol',
    'SPINNOW': 'Paikutin',
    'SPIN': 'Paikutin',
    '1TURN': '1 pa naman',
    'LUCKYSPIN': 'masuwerteng paikutin',
    'LUCK': 'Mag swerte ka mamaya',
    'TURN': 'Isa pa',
    'X2': 'X2 pera',
    'X3': 'X3 pera',
    'X5': 'X5 pera',
    'X10': 'X10 pera',
    'MISS': 'Miss',
    'MONEY1': 'Pagbati! Mayroon kang %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RsX1BILnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQWdDO0FBRWhDLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFNBQVM7SUFDcEIsS0FBSyxFQUFFLE9BQU87SUFDZCxPQUFPLEVBQUUsT0FBTztJQUNoQixPQUFPLEVBQUUsT0FBTztJQUNoQixPQUFPLEVBQUUsT0FBTztJQUNoQixRQUFRLEVBQUUseUJBQXlCO0lBQ25DLE9BQU8sRUFBRyxNQUFNO0lBQ2hCLE9BQU8sRUFBRSxjQUFjO0lBQ3ZCLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsUUFBUSxFQUFFLFFBQVE7SUFDbEIsUUFBUSxFQUFFLFFBQVE7SUFDbEIsT0FBTyxFQUFFLE1BQU07SUFDZixVQUFVLEVBQUUsT0FBTztJQUNuQixRQUFRLEVBQUUsYUFBYTtJQUN2QixTQUFTLEVBQUUsd0NBQXdDO0lBQ25ELEtBQUssRUFBRSxNQUFNO0lBQ2IsSUFBSSxFQUFFLFFBQVE7SUFDZCxNQUFNLEVBQUUsUUFBUTtJQUNoQixLQUFLLEVBQUUsS0FBSztJQUNaLFNBQVMsRUFBRSxRQUFRO0lBQ25CLFVBQVUsRUFBRSxpQkFBaUI7SUFDN0IsV0FBVyxFQUFFLGdGQUFnRjtJQUM3RixNQUFNLEVBQUUsV0FBVztJQUNuQixRQUFRLEVBQUUsdUJBQXVCO0lBQ2pDLFFBQVEsRUFBRSx1QkFBdUI7SUFDakMsVUFBVSxFQUFFLG1CQUFtQjtJQUMvQixPQUFPLEVBQUUsZ0JBQWdCO0lBQ3pCLE9BQU8sRUFBRSxlQUFlO0lBQ3hCLE9BQU8sRUFBRSxpQkFBaUI7SUFDMUIsT0FBTyxFQUFFLGlCQUFpQjtJQUMxQixVQUFVLEVBQUUsUUFBUTtJQUNwQixTQUFTLEVBQUUsdUVBQXVFO0lBQ2xGLFFBQVEsRUFBRSxPQUFPO0lBQ2pCLFNBQVMsRUFBRSxVQUFVO0lBQ3JCLE1BQU0sRUFBRSxVQUFVO0lBQ2xCLE9BQU8sRUFBRSxZQUFZO0lBQ3JCLFdBQVcsRUFBRSxzQkFBc0I7SUFDbkMsTUFBTSxFQUFFLHNCQUFzQjtJQUM5QixNQUFNLEVBQUUsUUFBUTtJQUNoQixJQUFJLEVBQUUsU0FBUztJQUNmLElBQUksRUFBRSxTQUFTO0lBQ2YsSUFBSSxFQUFFLFNBQVM7SUFDZixLQUFLLEVBQUUsVUFBVTtJQUNqQixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSwwQkFBMEI7SUFDcEMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL3RoX1RIX3R1dCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdNYWdsYXJvJyxcbiAgICAnV0lOJzogJ0lrYS0xJyxcbiAgICAnTE9TRTInOiAnSWthLTInLFxuICAgICdMT1NFMyc6ICdJa2EtMycsXG4gICAgJ0xPU0U0JzogJ0lrYS00JyxcbiAgICAnUExBWUVSJzogJ2JpbGFuZyBuZyBtZ2EgbWFubGFsYXJvJyxcbiAgICAnQkVUTk8nOiAgJ1RheWEnLFxuICAgICdEQUlMWSc6ICdEYWlseSByZXdhcmQnLFxuICAgICdEMSc6ICdBcmF3IDEnLFxuICAgICdEMic6ICdBcmF3IDInLFxuICAgICdEMyc6ICdBcmF3IDMnLFxuICAgICdENCc6ICdBcmF3IDQnLFxuICAgICdENSc6ICdBcmF3IDUnLFxuICAgICdENic6ICdBcmF3IDYnLFxuICAgICdENyc6ICdBcmF3IDcnLFxuICAgICczVFVSTlMnOiAnMyBsaWtvJyxcbiAgICAnNVRVUk5TJzogJzUgbGlrbycsXG4gICAgJ1RVUk5TJzogJ2xpa28nLFxuICAgICdSRUNFSVZFRCc6ICdIYWJvbCcsXG4gICAgJ0xFQURFUic6ICdMZWFkZXJib2FyZCcsXG4gICAgJ05PVklERU8nOiAnSGluZGkgbWFhYXJpbmcgaS1wbGF5IGFuZyB2aWRlbyBuZ2F5b24nLFxuICAgICdCRVQnOiAnVGF5YScsXG4gICAgJ05PJzogJ051bWVybycsXG4gICAgJ1BBU1MnOiAnUHVtYXNhJyxcbiAgICAnSElUJzogJ0hpdCcsXG4gICAgJ0FSUkFOR0UnOiAnQXl1c2luJyxcbiAgICAnUVVJVEdBTUUnOiAnVHVtaWdpbCBzYSBsYXJvJyxcbiAgICAnUVVJVEdBTUVQJzogJ0t1bmcgaHVtaW50byBrYSBzYSBsYXJvIG1hd2F3YWxhIHNhIGl5byBuZyBzYW1wdW5nIGJlc2VzIGJpbGFuZyBhbnRhcyBuZyBwdXN0YScsXG4gICAgJ1FVSVQnOiAnVW1hbGlzIG5hJyxcbiAgICAnM1BBSVJTJzogJzMgbWFna2FzdW5vZCBuYSBwYXJlcycsXG4gICAgJzRQQUlSUyc6ICc0IG1hZ2thc3Vub2QgbmEgcGFyZXMnLFxuICAgICdGT1VSS0lORCc6ICdBcGF0IG5nIGlzYW5nIHVyaScsXG4gICAgJ0ZMVVNIJzogJ1N0cmFpZ2h0IEZsdXNoJyxcbiAgICAnMUJFU1QnOiAncGluYWthbWFodXNheScsXG4gICAgJzJCRVNUJzogJzIgcGluYWthbWFodXNheScsXG4gICAgJzNCRVNUJzogJzMgcGluYWthbWFodXNheScsXG4gICAgJ0lOU1RSVUNUJzogJ1BhbnV0bycsXG4gICAgJ05PTU9ORVknOiAnTmF1YnVzYW4ga2EgbmcgcGVyYSwgbWFnLWNsaWNrIHVwYW5nIG1ha2F0YW5nZ2FwIG5nIG1hcyBtYXJhbWluZyBwZXJhJyxcbiAgICAnUkVDRUkyJzogJ0hhYm9sJyxcbiAgICAnU1BJTk5PVyc6ICdQYWlrdXRpbicsXG4gICAgJ1NQSU4nOiAnUGFpa3V0aW4nLFxuICAgICcxVFVSTic6ICcxIHBhIG5hbWFuJyxcbiAgICAnTFVDS1lTUElOJzogJ21hc3V3ZXJ0ZW5nIHBhaWt1dGluJyxcbiAgICAnTFVDSyc6ICdNYWcgc3dlcnRlIGthIG1hbWF5YScsXG4gICAgJ1RVUk4nOiAnSXNhIHBhJyxcbiAgICAnWDInOiAnWDIgcGVyYScsXG4gICAgJ1gzJzogJ1gzIHBlcmEnLFxuICAgICdYNSc6ICdYNSBwZXJhJyxcbiAgICAnWDEwJzogJ1gxMCBwZXJhJyxcbiAgICAnTUlTUyc6ICdNaXNzJyxcbiAgICAnTU9ORVkxJzogJ1BhZ2JhdGkhIE1heXJvb24ga2FuZyAlcycsXG4gICAgJ1RVVCc6IHR1dG9yXG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/tl_PH_tut.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '659f1eXcpxLYqcIXEkkIEA6', 'tl_PH_tut');
// lang/tl_PH_tut.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tutor = "Para sa unang laro, ang dealer ay napili nang sapalaran; pagkatapos ay ang natalo ng bawat laro ay kailangang harapin ang susunod. Kapag mayroong apat na manlalaro, 13 cards ang ibabahagi sa bawat manlalaro.\n\nKung may mas kaunti sa apat na manlalaro, 13 cards pa rin ang nakikitungo sa bawat manlalaro, at magkakaroon ng ilang mga kard na natitira na hindi na ginagamit - hindi ito ginagamit sa laro. Ang isang kahalili sa tatlong mga manlalaro ay, sa pamamagitan ng naunang kasunduan, upang makitungo sa bawat 17 card. Kapag mayroong dalawang manlalaro lamang, 13 card lamang ang bawat isa ay dapat na harapin - kung ang lahat ng mga kard ay naaksyunan ang mga manlalaro ay maaaring mag-ehersisyo ang mga kamay ng bawat isa, na makakasira sa laro. Kapag mayroong higit sa apat na manlalaro, maaari kang sumang-ayon nang maaga alinman sa pakikitungo sa 13 mga kard bawat isa mula sa dobleng deck, o pakikitungo sa maraming mga card hangga't maaari pantay sa mga manlalaro.\n\nAng laro\n\nSa unang laro lamang, ang manlalaro na may 3 ng Spades ay nagsisimulang maglaro. Kung walang sinuman ang mayroong 3 (sa tatlo o dalawang laro ng manlalaro) sinumang may hawak ng pinakamababang card ay nagsisimula. Dapat magsimula ang manlalaro sa pamamagitan ng paglalaro ng pinakamababang card na ito, alinman sa sarili o bilang bahagi ng isang kumbinasyon.\n\nSa kasunod na mga laro, ang nagwagi ng nakaraang laro ay naglalaro muna, at maaaring magsimula sa anumang kumbinasyon.\n\nAng bawat manlalaro naman ay dapat na talunin ang dati nang nilalaro na card o kombinasyon, sa pamamagitan ng paglalaro ng isang card o kombinasyon na pumapalo dito, o pumasa at hindi naglalaro ng anumang mga card. Ang (mga) tinugtog na kard ay inilalagay sa isang magbunton ng mukha sa gitna ng mesa. Ang paglalaro ay paikot-ikot sa talahanayan nang maraming beses hangga't kinakailangan hanggang sa ang isang tao ay maglaro ng isang kard o kumbinasyon na walang ibang natalo. Kapag nangyari ito, ang lahat ng mga pinatugtog na kard ay itinabi, at ang taong ang walang patalo na laro ay nagsisimula muli sa pamamagitan ng paglalaro ng anumang ligal na kard o kombinasyon na nakaharap sa gitna ng mesa.\n\nKung pumasa ka ay naka-lock sa labas ng laro hanggang sa may gumawa ng isang dula na walang nakakatalo. Kapag ang mga kard ay itinabi at ang isang bagong kard o kombinasyon ay pinangungunahan may karapatan kang maglaro muli.\n\nHalimbawa (na may tatlong mga manlalaro): ang manlalaro sa kanan ay gumaganap ng solong tatlo, may hawak kang alas ngunit nagpasya na pumasa, ang manlalaro sa kaliwa ay gumaganap ng siyam at ang manlalaro sa kanan ay gumaganap ng isang hari. Hindi mo matalo ngayon ang hari sa iyong ace, dahil nakapasa ka na. Kung ang pangatlong manlalaro ay pumasa din, at ang iyong kalaban sa kanang kamay ngayon ay humantong sa isang reyna, maaari mo na ngayong i-play ang iyong ace kung nais mo.\n\nAng mga ligal na dula sa laro ay ang mga sumusunod:\nSingle card Ang pinakamababang solong card ay ang 3 at ang pinakamataas ay ang 2.\nIpares ang Dalawang kard ng parehong ranggo - tulad ng 7-7 o Q-Q.\nTatlong Tatlong kard ng magkaparehong ranggo - tulad ng 5-5-5\nApat ng isang uri Apat na kard ng parehong ranggo - tulad ng 9-9-9-9.\nPagkakasunud-sunod Tatlo o higit pang mga kard ng magkakasunod na ranggo (ang mga suit ay maaaring ihalo) - tulad ng 4-5-6 o J-Q-K-A. Ang mga pagkakasunud-sunod ay hindi maaaring \"i-on ang sulok\" sa pagitan ng dalawa at tatlo - Ang A-2-3 ay hindi isang wastong pagkakasunud-sunod dahil ang 2 ay mataas at 3 ay mababa.\nDouble Sequence Tatlo o higit pang mga pares ng magkakasunod na ranggo - tulad ng 3-3-4-4-5-5 o 6-6-7-7-8-8-9-9.\n\nSa pangkalahatan, ang isang kumbinasyon ay maaari lamang matalo ng isang mas mataas na kumbinasyon ng parehong uri at parehong bilang ng mga kard. Kaya't kung ang isang solong card ay hahantong, iisang card lamang ang maaaring i-play; kung ang isang pares ay pinangunahan pares lamang ang maaaring i-play; ang isang pagkakasunud-sunod ng tatlong card ay maaari lamang matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong card; at iba pa. Hindi mo maaaring halimbawa talunin ang isang pares gamit ang isang triple, o isang pagkakasunud-sunod ng apat na card na may isang pagkakasunud-sunod ng limang card.\n\nUpang magpasya kung alin sa dalawang mga kumbinasyon ng parehong uri ang mas mataas ay titingnan mo lamang ang pinakamataas na card na pinagsama. Halimbawa 7-7 beats 7-7 dahil pinapalo ng puso ang brilyante. Sa parehong paraan 8-9-10 beats 8-9-10 sapagkat ito ang pinakamataas na card (ang sampu) na inihambing.\n\nMayroong apat na mga pagbubukod sa panuntunan na ang isang kumbinasyon ay maaari lamang matalo ng isang kumbinasyon ng parehong uri:\n\nAng isang apat na uri ay maaaring matalo ang anumang solong dalawa (ngunit hindi sa anumang iba pang solong card, tulad ng isang alas o hari). Ang isang apat na uri ay maaaring matalo ng mas mataas na apat na uri.\n\nAng isang pagkakasunud-sunod ng tatlong pares (tulad ng 7-7-8-8-9-9) ay maaaring matalo ang anumang solong dalawa (ngunit hindi anumang iba pang solong card). Ang isang pagkakasunud-sunod ng tatlong mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong mga pares.\n\nAng isang pagkakasunud-sunod ng apat na pares (tulad ng 5-5-6-6-7-7-8-8) ay maaaring matalo ang isang pares ng dalawa (ngunit hindi sa anumang iba pang pares). Ang isang pagkakasunud-sunod ng apat na mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng apat na mga pares.\n\nAng isang pagkakasunud-sunod ng limang pares (tulad ng 8-8-9-9-10-10-J-J-Q-Q) ay maaaring matalo ang isang hanay ng tatlong dalawa (ngunit hindi sa anumang iba pang tatlong uri). Ang isang pagkakasunud-sunod ng limang mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng limang mga pares.\n\nAng mga kumbinasyong ito na maaaring matalo ang mga nag-iisang twos o mga hanay ng dalawa ay paminsan-minsang kilala bilang mga bomba o dalawang-bomba, at maaaring i-play kahit ng isang manlalaro na dati nang lumipas.\n\nTandaan na nalalapat lamang ang mga pagbubukod na ito sa pagkatalo sa dalawa, hindi sa iba pang mga kard. Halimbawa, kung ang isang tao ay naglalaro ng alas ay hindi mo ito matatalo sa iyong apat na uri, ngunit kung ang alas ay pinalo ng dalawa, kung gayon ang iyong apat na uri ay maaaring magamit upang talunin ang dalawa";
exports.default = tutor;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RsX1BIX3R1dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sS0FBSyxHQUFHLGt3TUF3Q3NULENBQUE7QUFFcFUsa0JBQWUsS0FBSyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgdHV0b3IgPSBgUGFyYSBzYSB1bmFuZyBsYXJvLCBhbmcgZGVhbGVyIGF5IG5hcGlsaSBuYW5nIHNhcGFsYXJhbjsgcGFna2F0YXBvcyBheSBhbmcgbmF0YWxvIG5nIGJhd2F0IGxhcm8gYXkga2FpbGFuZ2FuZyBoYXJhcGluIGFuZyBzdXN1bm9kLiBLYXBhZyBtYXlyb29uZyBhcGF0IG5hIG1hbmxhbGFybywgMTMgY2FyZHMgYW5nIGliYWJhaGFnaSBzYSBiYXdhdCBtYW5sYWxhcm8uXG5cbkt1bmcgbWF5IG1hcyBrYXVudGkgc2EgYXBhdCBuYSBtYW5sYWxhcm8sIDEzIGNhcmRzIHBhIHJpbiBhbmcgbmFraWtpdHVuZ28gc2EgYmF3YXQgbWFubGFsYXJvLCBhdCBtYWdrYWthcm9vbiBuZyBpbGFuZyBtZ2Ega2FyZCBuYSBuYXRpdGlyYSBuYSBoaW5kaSBuYSBnaW5hZ2FtaXQgLSBoaW5kaSBpdG8gZ2luYWdhbWl0IHNhIGxhcm8uIEFuZyBpc2FuZyBrYWhhbGlsaSBzYSB0YXRsb25nIG1nYSBtYW5sYWxhcm8gYXksIHNhIHBhbWFtYWdpdGFuIG5nIG5hdW5hbmcga2FzdW5kdWFuLCB1cGFuZyBtYWtpdHVuZ28gc2EgYmF3YXQgMTcgY2FyZC4gS2FwYWcgbWF5cm9vbmcgZGFsYXdhbmcgbWFubGFsYXJvIGxhbWFuZywgMTMgY2FyZCBsYW1hbmcgYW5nIGJhd2F0IGlzYSBheSBkYXBhdCBuYSBoYXJhcGluIC0ga3VuZyBhbmcgbGFoYXQgbmcgbWdhIGthcmQgYXkgbmFha3N5dW5hbiBhbmcgbWdhIG1hbmxhbGFybyBheSBtYWFhcmluZyBtYWctZWhlcnNpc3lvIGFuZyBtZ2Ega2FtYXkgbmcgYmF3YXQgaXNhLCBuYSBtYWtha2FzaXJhIHNhIGxhcm8uIEthcGFnIG1heXJvb25nIGhpZ2l0IHNhIGFwYXQgbmEgbWFubGFsYXJvLCBtYWFhcmkga2FuZyBzdW1hbmctYXlvbiBuYW5nIG1hYWdhIGFsaW5tYW4gc2EgcGFraWtpdHVuZ28gc2EgMTMgbWdhIGthcmQgYmF3YXQgaXNhIG11bGEgc2EgZG9ibGVuZyBkZWNrLCBvIHBha2lraXR1bmdvIHNhIG1hcmFtaW5nIG1nYSBjYXJkIGhhbmdnYSd0IG1hYWFyaSBwYW50YXkgc2EgbWdhIG1hbmxhbGFyby5cblxuQW5nIGxhcm9cblxuU2EgdW5hbmcgbGFybyBsYW1hbmcsIGFuZyBtYW5sYWxhcm8gbmEgbWF5IDMgbmcgU3BhZGVzIGF5IG5hZ3Npc2ltdWxhbmcgbWFnbGFyby4gS3VuZyB3YWxhbmcgc2ludW1hbiBhbmcgbWF5cm9vbmcgMyAoc2EgdGF0bG8gbyBkYWxhd2FuZyBsYXJvIG5nIG1hbmxhbGFybykgc2ludW1hbmcgbWF5IGhhd2FrIG5nIHBpbmFrYW1hYmFiYW5nIGNhcmQgYXkgbmFnc2lzaW11bGEuIERhcGF0IG1hZ3NpbXVsYSBhbmcgbWFubGFsYXJvIHNhIHBhbWFtYWdpdGFuIG5nIHBhZ2xhbGFybyBuZyBwaW5ha2FtYWJhYmFuZyBjYXJkIG5hIGl0bywgYWxpbm1hbiBzYSBzYXJpbGkgbyBiaWxhbmcgYmFoYWdpIG5nIGlzYW5nIGt1bWJpbmFzeW9uLlxuXG5TYSBrYXN1bm9kIG5hIG1nYSBsYXJvLCBhbmcgbmFnd2FnaSBuZyBuYWthcmFhbmcgbGFybyBheSBuYWdsYWxhcm8gbXVuYSwgYXQgbWFhYXJpbmcgbWFnc2ltdWxhIHNhIGFudW1hbmcga3VtYmluYXN5b24uXG5cbkFuZyBiYXdhdCBtYW5sYWxhcm8gbmFtYW4gYXkgZGFwYXQgbmEgdGFsdW5pbiBhbmcgZGF0aSBuYW5nIG5pbGFsYXJvIG5hIGNhcmQgbyBrb21iaW5hc3lvbiwgc2EgcGFtYW1hZ2l0YW4gbmcgcGFnbGFsYXJvIG5nIGlzYW5nIGNhcmQgbyBrb21iaW5hc3lvbiBuYSBwdW1hcGFsbyBkaXRvLCBvIHB1bWFzYSBhdCBoaW5kaSBuYWdsYWxhcm8gbmcgYW51bWFuZyBtZ2EgY2FyZC4gQW5nIChtZ2EpIHRpbnVndG9nIG5hIGthcmQgYXkgaW5pbGFsYWdheSBzYSBpc2FuZyBtYWdidW50b24gbmcgbXVraGEgc2EgZ2l0bmEgbmcgbWVzYS4gQW5nIHBhZ2xhbGFybyBheSBwYWlrb3QtaWtvdCBzYSB0YWxhaGFuYXlhbiBuYW5nIG1hcmFtaW5nIGJlc2VzIGhhbmdnYSd0IGtpbmFrYWlsYW5nYW4gaGFuZ2dhbmcgc2EgYW5nIGlzYW5nIHRhbyBheSBtYWdsYXJvIG5nIGlzYW5nIGthcmQgbyBrdW1iaW5hc3lvbiBuYSB3YWxhbmcgaWJhbmcgbmF0YWxvLiBLYXBhZyBuYW5neWFyaSBpdG8sIGFuZyBsYWhhdCBuZyBtZ2EgcGluYXR1Z3RvZyBuYSBrYXJkIGF5IGl0aW5hYmksIGF0IGFuZyB0YW9uZyBhbmcgd2FsYW5nIHBhdGFsbyBuYSBsYXJvIGF5IG5hZ3Npc2ltdWxhIG11bGkgc2EgcGFtYW1hZ2l0YW4gbmcgcGFnbGFsYXJvIG5nIGFudW1hbmcgbGlnYWwgbmEga2FyZCBvIGtvbWJpbmFzeW9uIG5hIG5ha2FoYXJhcCBzYSBnaXRuYSBuZyBtZXNhLlxuXG5LdW5nIHB1bWFzYSBrYSBheSBuYWthLWxvY2sgc2EgbGFiYXMgbmcgbGFybyBoYW5nZ2FuZyBzYSBtYXkgZ3VtYXdhIG5nIGlzYW5nIGR1bGEgbmEgd2FsYW5nIG5ha2FrYXRhbG8uIEthcGFnIGFuZyBtZ2Ega2FyZCBheSBpdGluYWJpIGF0IGFuZyBpc2FuZyBiYWdvbmcga2FyZCBvIGtvbWJpbmFzeW9uIGF5IHBpbmFuZ3VuZ3VuYWhhbiBtYXkga2FyYXBhdGFuIGthbmcgbWFnbGFybyBtdWxpLlxuXG5IYWxpbWJhd2EgKG5hIG1heSB0YXRsb25nIG1nYSBtYW5sYWxhcm8pOiBhbmcgbWFubGFsYXJvIHNhIGthbmFuIGF5IGd1bWFnYW5hcCBuZyBzb2xvbmcgdGF0bG8sIG1heSBoYXdhayBrYW5nIGFsYXMgbmd1bml0IG5hZ3Bhc3lhIG5hIHB1bWFzYSwgYW5nIG1hbmxhbGFybyBzYSBrYWxpd2EgYXkgZ3VtYWdhbmFwIG5nIHNpeWFtIGF0IGFuZyBtYW5sYWxhcm8gc2Ega2FuYW4gYXkgZ3VtYWdhbmFwIG5nIGlzYW5nIGhhcmkuIEhpbmRpIG1vIG1hdGFsbyBuZ2F5b24gYW5nIGhhcmkgc2EgaXlvbmcgYWNlLCBkYWhpbCBuYWthcGFzYSBrYSBuYS4gS3VuZyBhbmcgcGFuZ2F0bG9uZyBtYW5sYWxhcm8gYXkgcHVtYXNhIGRpbiwgYXQgYW5nIGl5b25nIGthbGFiYW4gc2Ega2FuYW5nIGthbWF5IG5nYXlvbiBheSBodW1hbnRvbmcgc2EgaXNhbmcgcmV5bmEsIG1hYWFyaSBtbyBuYSBuZ2F5b25nIGktcGxheSBhbmcgaXlvbmcgYWNlIGt1bmcgbmFpcyBtby5cblxuQW5nIG1nYSBsaWdhbCBuYSBkdWxhIHNhIGxhcm8gYXkgYW5nIG1nYSBzdW11c3Vub2Q6XG5TaW5nbGUgY2FyZCBBbmcgcGluYWthbWFiYWJhbmcgc29sb25nIGNhcmQgYXkgYW5nIDMgYXQgYW5nIHBpbmFrYW1hdGFhcyBheSBhbmcgMi5cbklwYXJlcyBhbmcgRGFsYXdhbmcga2FyZCBuZyBwYXJlaG9uZyByYW5nZ28gLSB0dWxhZCBuZyA3LTcgbyBRLVEuXG5UYXRsb25nIFRhdGxvbmcga2FyZCBuZyBtYWdrYXBhcmVob25nIHJhbmdnbyAtIHR1bGFkIG5nIDUtNS01XG5BcGF0IG5nIGlzYW5nIHVyaSBBcGF0IG5hIGthcmQgbmcgcGFyZWhvbmcgcmFuZ2dvIC0gdHVsYWQgbmcgOS05LTktOS5cblBhZ2tha2FzdW51ZC1zdW5vZCBUYXRsbyBvIGhpZ2l0IHBhbmcgbWdhIGthcmQgbmcgbWFna2FrYXN1bm9kIG5hIHJhbmdnbyAoYW5nIG1nYSBzdWl0IGF5IG1hYWFyaW5nIGloYWxvKSAtIHR1bGFkIG5nIDQtNS02IG8gSi1RLUstQS4gQW5nIG1nYSBwYWdrYWthc3VudWQtc3Vub2QgYXkgaGluZGkgbWFhYXJpbmcgXCJpLW9uIGFuZyBzdWxva1wiIHNhIHBhZ2l0YW4gbmcgZGFsYXdhIGF0IHRhdGxvIC0gQW5nIEEtMi0zIGF5IGhpbmRpIGlzYW5nIHdhc3RvbmcgcGFna2FrYXN1bnVkLXN1bm9kIGRhaGlsIGFuZyAyIGF5IG1hdGFhcyBhdCAzIGF5IG1hYmFiYS5cbkRvdWJsZSBTZXF1ZW5jZSBUYXRsbyBvIGhpZ2l0IHBhbmcgbWdhIHBhcmVzIG5nIG1hZ2tha2FzdW5vZCBuYSByYW5nZ28gLSB0dWxhZCBuZyAzLTMtNC00LTUtNSBvIDYtNi03LTctOC04LTktOS5cblxuU2EgcGFuZ2thbGFoYXRhbiwgYW5nIGlzYW5nIGt1bWJpbmFzeW9uIGF5IG1hYWFyaSBsYW1hbmcgbWF0YWxvIG5nIGlzYW5nIG1hcyBtYXRhYXMgbmEga3VtYmluYXN5b24gbmcgcGFyZWhvbmcgdXJpIGF0IHBhcmVob25nIGJpbGFuZyBuZyBtZ2Ega2FyZC4gS2F5YSd0IGt1bmcgYW5nIGlzYW5nIHNvbG9uZyBjYXJkIGF5IGhhaGFudG9uZywgaWlzYW5nIGNhcmQgbGFtYW5nIGFuZyBtYWFhcmluZyBpLXBsYXk7IGt1bmcgYW5nIGlzYW5nIHBhcmVzIGF5IHBpbmFuZ3VuYWhhbiBwYXJlcyBsYW1hbmcgYW5nIG1hYWFyaW5nIGktcGxheTsgYW5nIGlzYW5nIHBhZ2tha2FzdW51ZC1zdW5vZCBuZyB0YXRsb25nIGNhcmQgYXkgbWFhYXJpIGxhbWFuZyBtYXRhbG8gbmcgaXNhbmcgbWFzIG1hdGFhcyBuYSBwYWdrYWthc3VudWQtc3Vub2QgbmcgdGF0bG9uZyBjYXJkOyBhdCBpYmEgcGEuIEhpbmRpIG1vIG1hYWFyaW5nIGhhbGltYmF3YSB0YWx1bmluIGFuZyBpc2FuZyBwYXJlcyBnYW1pdCBhbmcgaXNhbmcgdHJpcGxlLCBvIGlzYW5nIHBhZ2tha2FzdW51ZC1zdW5vZCBuZyBhcGF0IG5hIGNhcmQgbmEgbWF5IGlzYW5nIHBhZ2tha2FzdW51ZC1zdW5vZCBuZyBsaW1hbmcgY2FyZC5cblxuVXBhbmcgbWFncGFzeWEga3VuZyBhbGluIHNhIGRhbGF3YW5nIG1nYSBrdW1iaW5hc3lvbiBuZyBwYXJlaG9uZyB1cmkgYW5nIG1hcyBtYXRhYXMgYXkgdGl0aW5nbmFuIG1vIGxhbWFuZyBhbmcgcGluYWthbWF0YWFzIG5hIGNhcmQgbmEgcGluYWdzYW1hLiBIYWxpbWJhd2EgNy03IGJlYXRzIDctNyBkYWhpbCBwaW5hcGFsbyBuZyBwdXNvIGFuZyBicmlseWFudGUuIFNhIHBhcmVob25nIHBhcmFhbiA4LTktMTAgYmVhdHMgOC05LTEwIHNhcGFna2F0IGl0byBhbmcgcGluYWthbWF0YWFzIG5hIGNhcmQgKGFuZyBzYW1wdSkgbmEgaW5paGFtYmluZy5cblxuTWF5cm9vbmcgYXBhdCBuYSBtZ2EgcGFnYnVidWtvZCBzYSBwYW51bnR1bmFuIG5hIGFuZyBpc2FuZyBrdW1iaW5hc3lvbiBheSBtYWFhcmkgbGFtYW5nIG1hdGFsbyBuZyBpc2FuZyBrdW1iaW5hc3lvbiBuZyBwYXJlaG9uZyB1cmk6XG5cbkFuZyBpc2FuZyBhcGF0IG5hIHVyaSBheSBtYWFhcmluZyBtYXRhbG8gYW5nIGFudW1hbmcgc29sb25nIGRhbGF3YSAobmd1bml0IGhpbmRpIHNhIGFudW1hbmcgaWJhIHBhbmcgc29sb25nIGNhcmQsIHR1bGFkIG5nIGlzYW5nIGFsYXMgbyBoYXJpKS4gQW5nIGlzYW5nIGFwYXQgbmEgdXJpIGF5IG1hYWFyaW5nIG1hdGFsbyBuZyBtYXMgbWF0YWFzIG5hIGFwYXQgbmEgdXJpLlxuXG5BbmcgaXNhbmcgcGFna2FrYXN1bnVkLXN1bm9kIG5nIHRhdGxvbmcgcGFyZXMgKHR1bGFkIG5nIDctNy04LTgtOS05KSBheSBtYWFhcmluZyBtYXRhbG8gYW5nIGFudW1hbmcgc29sb25nIGRhbGF3YSAobmd1bml0IGhpbmRpIGFudW1hbmcgaWJhIHBhbmcgc29sb25nIGNhcmQpLiBBbmcgaXNhbmcgcGFna2FrYXN1bnVkLXN1bm9kIG5nIHRhdGxvbmcgbWdhIHBhcmVzIGF5IG1hYWFyaW5nIG1hdGFsbyBuZyBpc2FuZyBtYXMgbWF0YWFzIG5hIHBhZ2tha2FzdW51ZC1zdW5vZCBuZyB0YXRsb25nIG1nYSBwYXJlcy5cblxuQW5nIGlzYW5nIHBhZ2tha2FzdW51ZC1zdW5vZCBuZyBhcGF0IG5hIHBhcmVzICh0dWxhZCBuZyA1LTUtNi02LTctNy04LTgpIGF5IG1hYWFyaW5nIG1hdGFsbyBhbmcgaXNhbmcgcGFyZXMgbmcgZGFsYXdhIChuZ3VuaXQgaGluZGkgc2EgYW51bWFuZyBpYmEgcGFuZyBwYXJlcykuIEFuZyBpc2FuZyBwYWdrYWthc3VudWQtc3Vub2QgbmcgYXBhdCBuYSBtZ2EgcGFyZXMgYXkgbWFhYXJpbmcgbWF0YWxvIG5nIGlzYW5nIG1hcyBtYXRhYXMgbmEgcGFna2FrYXN1bnVkLXN1bm9kIG5nIGFwYXQgbmEgbWdhIHBhcmVzLlxuXG5BbmcgaXNhbmcgcGFna2FrYXN1bnVkLXN1bm9kIG5nIGxpbWFuZyBwYXJlcyAodHVsYWQgbmcgOC04LTktOS0xMC0xMC1KLUotUS1RKSBheSBtYWFhcmluZyBtYXRhbG8gYW5nIGlzYW5nIGhhbmF5IG5nIHRhdGxvbmcgZGFsYXdhIChuZ3VuaXQgaGluZGkgc2EgYW51bWFuZyBpYmEgcGFuZyB0YXRsb25nIHVyaSkuIEFuZyBpc2FuZyBwYWdrYWthc3VudWQtc3Vub2QgbmcgbGltYW5nIG1nYSBwYXJlcyBheSBtYWFhcmluZyBtYXRhbG8gbmcgaXNhbmcgbWFzIG1hdGFhcyBuYSBwYWdrYWthc3VudWQtc3Vub2QgbmcgbGltYW5nIG1nYSBwYXJlcy5cblxuQW5nIG1nYSBrdW1iaW5hc3lvbmcgaXRvIG5hIG1hYWFyaW5nIG1hdGFsbyBhbmcgbWdhIG5hZy1paXNhbmcgdHdvcyBvIG1nYSBoYW5heSBuZyBkYWxhd2EgYXkgcGFtaW5zYW4tbWluc2FuZyBraWxhbGEgYmlsYW5nIG1nYSBib21iYSBvIGRhbGF3YW5nLWJvbWJhLCBhdCBtYWFhcmluZyBpLXBsYXkga2FoaXQgbmcgaXNhbmcgbWFubGFsYXJvIG5hIGRhdGkgbmFuZyBsdW1pcGFzLlxuXG5UYW5kYWFuIG5hIG5hbGFsYXBhdCBsYW1hbmcgYW5nIG1nYSBwYWdidWJ1a29kIG5hIGl0byBzYSBwYWdrYXRhbG8gc2EgZGFsYXdhLCBoaW5kaSBzYSBpYmEgcGFuZyBtZ2Ega2FyZC4gSGFsaW1iYXdhLCBrdW5nIGFuZyBpc2FuZyB0YW8gYXkgbmFnbGFsYXJvIG5nIGFsYXMgYXkgaGluZGkgbW8gaXRvIG1hdGF0YWxvIHNhIGl5b25nIGFwYXQgbmEgdXJpLCBuZ3VuaXQga3VuZyBhbmcgYWxhcyBheSBwaW5hbG8gbmcgZGFsYXdhLCBrdW5nIGdheW9uIGFuZyBpeW9uZyBhcGF0IG5hIHVyaSBheSBtYWFhcmluZyBtYWdhbWl0IHVwYW5nIHRhbHVuaW4gYW5nIGRhbGF3YWBcblxuZXhwb3J0IGRlZmF1bHQgdHV0b3I7XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/vi_VN_tut.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '77101c4onlIip2iwXQKzaFb', 'vi_VN_tut');
// lang/vi_VN_tut.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tutor = "Ti\u1EBFn L\u00EAn Mi\u1EC1n Nam s\u1EED d\u1EE5ng b\u1ED9 b\u00E0i T\u00E2y 52 l\u00E1.\n\nGi\u00E1 tr\u1ECB qu\u00E2n b\u00E0i s\u1EBD ph\u1EE5 thu\u1ED9c v\u00E0o s\u1ED1: 2(heo) > A(x\u00EC) > K(gi\u00E0) > Q(\u0111\u1EA7m) > J(b\u1ED3i) > 10 > 9 > .... > 3.\n\nN\u1EBFu hai qu\u00E2n b\u00E0i c\u00F3 c\u00F9ng s\u1ED1 th\u00EC s\u1EBD so s\u00E1nh theo ch\u1EA5t : \u2665 > \u2666 > \u2663 > \u2660.\nV\u00ED d\u1EE5 : 10 \u2665 > 10 \u2666\n\nChia b\u00E0i: M\u1ED7i ng\u01B0\u1EDDi \u0111\u01B0\u1EE3c chia 13 l\u00E1 b\u00E0i.\n\nC\u00E1ch x\u1EBFp b\u00E0i: \nB\u00E0i r\u00E1c 1 l\u00E1 b\u00E0i l\u1EBB: b\u00E0i l\u1EBB c\u00F3 th\u1EC3 b\u1ECB \u0111\u00E8 b\u1EDFi 1 l\u00E1 b\u00E0i l\u1EBB c\u00F3 gi\u00E1 tr\u1ECB l\u1EDBn h\u01A1n\nVd : 4 \u2666 > 3 \u2660\n\nB\u1ED9 \u0111\u00F4i 2 l\u00E1 b\u00E0i gi\u1ED1ng nhau: 1 \u0111\u00F4i c\u00F3 th\u1EC3 \u0111\u00E8 \u0111\u01B0\u1EE3c 1 \u0111\u00F4i b\u00E9 h\u01A1n \nVd : J \u2665 + J \u2660 > J \u2666 + J \u2663\n\nT\u01B0\u01A1ng t\u1EF1 v\u1EDBi b\u1ED9 3 l\u00E1 b\u00E0i gi\u1ED1ng nhau ( s\u00E1m c\u00F4 )\n\nS\u1EA3nh : c\u00F3 \u00EDt nh\u1EA5t 3 l\u00E1 b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB t\u0103ng d\u1EA7n: 3 + 4 + 5 + 6 + 7 + 8 ......\n1 S\u1EA3nh c\u00F3 th\u1EC3 b\u1ECB \u0111\u00E8 b\u1EDFi 1 s\u1EA3nh c\u00F3 c\u00F9ng s\u1ED1 l\u00E1, nh\u01B0ng gi\u00E1 tr\u1ECB c\u1EE7a s\u1ED1 \u0111\u00F3 l\u1EDBn h\u01A1n\nVd : S\u1EA3nh 3 4 5 < 4 5 6 , ho\u1EB7c 7 8 9 < J Q K \n\nN\u1EBFu 2 s\u1EA3nh c\u00F3 c\u00F9ng s\u1ED1 l\u00E1 v\u00E0 gi\u00E1 tr\u1ECB , x\u00E9t ch\u1EA5t c\u1EE7a qu\u00E2n l\u1EDBn nh\u1EA5t c\u1EE7a s\u1EA3nh \u0111\u00F3 \u0111\u1EC3 t\u00ECm ra s\u1EA3nh l\u1EDBn h\u01A1n \nVd: J Q K(\u2663) < J Q K(\u2666)\n\n3 \u0111\u00F4i th\u00F4ng: 3 \u0111\u00F4i b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB t\u0103ng d\u1EA7n 4 4 + 5 5 + 6 6 \n\nT\u1EE9 qu\u00FD: 4 l\u00E1 b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB gi\u1ED1ng nhau 5 5 5 5, J J J J, Q Q Q Q \n              4 \u0111\u00F4i th\u00F4ng: 4 \u0111\u00F4i b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB t\u0103ng d\u1EA7n 4 4 + 5 5 + 6 6 + 7 7 \n              5 \u0111\u00F4i th\u00F4ng: 5 \u0111\u00F4i b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB t\u0103ng d\u1EA7n 4 4 + 5 5 + 6 6 + 7 7 + 8 8\n             6 \u0111\u00F4i th\u00F4ng: 6 \u0111\u00F4i b\u00E0i c\u00F3 gi\u00E1 tr\u1ECB t\u0103ng d\u1EA7n 4 4 + 5 5 + 6 6 + 7 7 + 8 8 + 9 9\n\n\u0110\u1ED9 m\u1EA1nh c\u1EE7a h\u00E0ng \u0111\u01B0\u1EE3c t\u00EDnh nh\u01B0 sau: 6 \u0111\u00F4i th\u00F4ng > 5 \u0111\u00F4i th\u00F4ng > 4 \u0111\u00F4i th\u00F4ng > 4 \u0111\u00F4i th\u00F4ng nh\u1ECF h\u01A1n > t\u1EE9 qu\u00FD > 3 \u0111\u00F4i th\u00F4ng\n\n\u0110\u00E1nh b\u00E0i:\n\nQuy\u1EC1n \u0111\u00E1nh tr\u01B0\u1EDBc: V\u00E1n \u0111\u1EA7u ti\u00EAn, quy\u1EC1n \u0111\u00E1nh tr\u01B0\u1EDBc s\u1EBD thu\u1ED9c v\u1EC1 ng\u01B0\u1EDDi s\u1EDF h\u1EEFu 3 \u2660.\n\nT\u1EEB v\u00E1n ti\u1EBFp theo ng\u01B0\u1EDDi nh\u1EA5t \u1EDF v\u00E1n tr\u01B0\u1EDBc \u0111\u00F3 s\u1EBD \u0111\u01B0\u1EE3c quy\u1EC1n \u0111\u00E1nh tr\u01B0\u1EDBc .\n\nV\u00F2ng \u0111\u00E1nh: Theo ng\u01B0\u1EE3c chi\u1EC1u kim \u0111\u1ED3ng h\u1ED3, m\u1ED7i ng\u01B0\u1EDDi \u0111\u01B0\u1EE3c ra 1 l\u00E1 b\u00E0i ho\u1EB7c 1 b\u1ED9 nhi\u1EC1u l\u00E1. Ng\u01B0\u1EDDi ra sau ph\u1EA3i \u0111\u00E1nh b\u00E0i c\u00F3 c\u00F9ng lo\u1EA1i v\u00E0 cao h\u01A1n ng\u01B0\u1EDDi \u0111\u00E1nh tr\u01B0\u1EDBc, tr\u1EEB tr\u01B0\u1EDDng h\u1EE3p ch\u1EB7t b\u00E0i. Lo\u1EA1i l\u00E0 c\u00F9ng b\u00E0i l\u1EBB, \u0111\u00F4i, b\u1ED9 ba,s\u1EA3nh.\n\nH\u1EBFt v\u00F2ng \u0111\u00E1nh: trong v\u00F2ng \u0111\u00E1nh, n\u1EBFu c\u00F3  1 ng\u01B0\u1EDDi b\u1ECF l\u01B0\u1EE3t th\u00EC coi nh\u01B0 b\u1ECF c\u1EA3 v\u00F2ng. N\u1EBFu kh\u00F4ng c\u00F2n ai ch\u1EB7n \u0111\u01B0\u1EE3c ti\u1EBFp th\u00EC ng\u01B0\u1EDDi \u0111\u00E1nh cu\u1ED1i c\u00F9ng \u0111\u01B0\u1EE3c ra b\u00E0i b\u1EAFt \u0111\u1EA7u v\u00F2ng m\u1EDBi .\n\nK\u1EBFt th\u00FAc: \n\nKhi ng\u01B0\u1EDDi \u0111\u1EA7u ti\u00EAn \u0111\u00E1nh h\u1EBFt b\u00E0i, ng\u01B0\u1EDDi \u0111\u00F3 s\u1EBD \u0111\u01B0\u1EE3c t\u00EDnh Nh\u1EA5t\n\nNh\u1EEFng ng\u01B0\u1EDDi c\u00F2n l\u1EA1i ti\u1EBFp t\u1EE5c \u0111\u00E1nh b\u00E0i \u0111\u1EC3 ch\u1ECDn ra th\u1EE9 t\u1EF1 v\u1EC1 Nh\u00EC , Ba , B\u00E9t\n\nCh\u1EB7t 2 (Heo) :\n3 \u0111\u00F4i th\u00F4ng ch\u1EB7t \u0111\u01B0\u1EE3c 2(heo) v\u00E0 3 \u0111\u00F4i th\u00F4ng nh\u1ECF h\u01A1n ( theo v\u00F2ng ch\u01A1i ) \n\nT\u1EE9 qu\u00FD ch\u1EB7t \u0111\u01B0\u1EE3c heo, \u0111\u00F4i heo, 3 \u0111\u00F4i th\u00F4ng v\u00E0 t\u1EE9 qu\u00FD nh\u1ECF h\u01A1n ( theo v\u00F2ng ch\u01A1i ) \n\n4 \u0111\u00F4i th\u00F4ng ch\u1EB7t \u0111\u01B0\u1EE3c heo, \u0111\u00F4i heo, 3 \u0111\u00F4i th\u00F4ng , t\u1EE9 qu\u00FD v\u00E0 4 \u0111\u00F4i th\u00F4ng nh\u1ECF h\u01A1n ( kh\u00F4ng c\u1EA7n theo v\u00F2ng ch\u01A1i ) \n\n\" Ch\u1EB7t ch\u1ED3ng \" cu\u1ED1i c\u00F9ng l\u00E0 t\u1ED5ng k\u1EBFt t\u1EA5t c\u1EA3 c\u00E1c h\u00E0nh vi \"ch\u1EB7t\" tr\u01B0\u1EDBc \u0111\u00F3 . Ng\u01B0\u1EDDi b\u1ECB ch\u1EB7t sau c\u00F9ng s\u1EBD ch\u1ECBu thi\u1EC7t h\u1EA1i ti\u1EC1n ch\u1EB7t .\n\nT\u1EDBi tr\u1EAFng: ki\u1EC3u th\u1EAFng ngay l\u1EADp t\u1EE9c kh\u00F4ng c\u1EA7n \u0111\u00E1nh khi ng\u01B0\u1EDDi ch\u01A1i th\u1ECFa m\u00E3n b\u1ED9 b\u00E0i sau :\n\nT\u1EA1i v\u00E1n \u0111\u1EA7u: T\u1EE9 qu\u00FD 3 ho\u1EB7c 3 \u0111\u00F4i th\u00F4ng c\u00F3 3 \u2660\n\nC\u00E1c v\u00E1n sau: T\u1EE9 qu\u00FD 2 , 5 \u0111\u00F4i th\u00F4ng , 6 \u0111\u00F4i th\u00F4ng , 6 \u0111\u00F4i b\u1EA5t k\u1EF3 , S\u1EA3nh R\u1ED3ng ( s\u1EA3nh n\u1ED1i t\u1EEB 3 -> A ) , 12 / 13 l\u00E1 b\u00E0i c\u00F9ng m\u00E0u ( \u2663\u2660 ho\u1EB7c \u2665\u2666 ) \n\nTh\u1ED1i b\u00E0i: \n\nNg\u01B0\u1EDDi ch\u01A1i b\u1ECB t\u00EDnh Th\u1ED1i khi ng\u01B0\u1EDDi \u0111\u00F3 v\u1EC1 B\u00E9t v\u00E0 tr\u00EAn tay h\u1ECD c\u00F2n c\u00E1c l\u00E1 b\u00E0i sau\nTh\u1ED1i c\u00F3 2(heo)\nTh\u1ED1i 3 \u0111\u00F4i th\u00F4ng\nTh\u1ED1i 4 \u0111\u00F4i th\u00F4ng\nTh\u1ED1i t\u1EE9 qu\u00FD\n\nNg\u01B0\u1EDDi ch\u01A1i b\u1ECB Ch\u00E1y b\u00E0i khi trong v\u00E1n ch\u01A1i c\u00F3 ng\u01B0\u1EDDi v\u1EC1 Nh\u1EA5t h\u1EBFt b\u00E0i , nh\u01B0ng ng\u01B0\u1EDDi ch\u01A1i \u0111\u00F3 v\u1EABn ch\u01B0a \u0111\u00E1nh l\u00E1 b\u00E0i n\u00E0o (c\u00F2n \u0111\u1EE7 13 l\u00E1).";
exports.default = tutor;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3ZpX1ZOX3R1dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sS0FBSyxHQUFHLGtxTUF5RW9ILENBQUE7QUFFbEksa0JBQWUsS0FBSyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgdHV0b3IgPSBgVGnhur9uIEzDqm4gTWnhu4FuIE5hbSBz4butIGThu6VuZyBi4buZIGLDoGkgVMOieSA1MiBsw6EuXG5cbkdpw6EgdHLhu4sgcXXDom4gYsOgaSBz4bq9IHBo4bulIHRodeG7mWMgdsOgbyBz4buROiAyKGhlbykgPiBBKHjDrCkgPiBLKGdpw6ApID4gUSjEkeG6p20pID4gSihi4buTaSkgPiAxMCA+IDkgPiAuLi4uID4gMy5cblxuTuG6v3UgaGFpIHF1w6JuIGLDoGkgY8OzIGPDuW5nIHPhu5EgdGjDrCBz4bq9IHNvIHPDoW5oIHRoZW8gY2jhuqV0IDog4pmlID4g4pmmID4g4pmjID4g4pmgLlxuVsOtIGThu6UgOiAxMCDimaUgPiAxMCDimaZcblxuQ2hpYSBiw6BpOiBN4buXaSBuZ8aw4budaSDEkcaw4bujYyBjaGlhIDEzIGzDoSBiw6BpLlxuXG5Dw6FjaCB44bq/cCBiw6BpOiBcbkLDoGkgcsOhYyAxIGzDoSBiw6BpIGzhurs6IGLDoGkgbOG6uyBjw7MgdGjhu4MgYuG7iyDEkcOoIGLhu59pIDEgbMOhIGLDoGkgbOG6uyBjw7MgZ2nDoSB0cuG7iyBs4bubbiBoxqFuXG5WZCA6IDQg4pmmID4gMyDimaBcblxuQuG7mSDEkcO0aSAyIGzDoSBiw6BpIGdp4buRbmcgbmhhdTogMSDEkcO0aSBjw7MgdGjhu4MgxJHDqCDEkcaw4bujYyAxIMSRw7RpIGLDqSBoxqFuIFxuVmQgOiBKIOKZpSArIEog4pmgID4gSiDimaYgKyBKIOKZo1xuXG5UxrDGoW5nIHThu7EgduG7m2kgYuG7mSAzIGzDoSBiw6BpIGdp4buRbmcgbmhhdSAoIHPDoW0gY8O0IClcblxuU+G6o25oIDogY8OzIMOtdCBuaOG6pXQgMyBsw6EgYsOgaSBjw7MgZ2nDoSB0cuG7iyB0xINuZyBk4bqnbjogMyArIDQgKyA1ICsgNiArIDcgKyA4IC4uLi4uLlxuMSBT4bqjbmggY8OzIHRo4buDIGLhu4sgxJHDqCBi4bufaSAxIHPhuqNuaCBjw7MgY8O5bmcgc+G7kSBsw6EsIG5oxrBuZyBnacOhIHRy4buLIGPhu6dhIHPhu5EgxJHDsyBs4bubbiBoxqFuXG5WZCA6IFPhuqNuaCAzIDQgNSA8IDQgNSA2ICwgaG/hurdjIDcgOCA5IDwgSiBRIEsgXG5cbk7hur91IDIgc+G6o25oIGPDsyBjw7luZyBz4buRIGzDoSB2w6AgZ2nDoSB0cuG7iyAsIHjDqXQgY2jhuqV0IGPhu6dhIHF1w6JuIGzhu5tuIG5o4bqldCBj4bunYSBz4bqjbmggxJHDsyDEkeG7gyB0w6xtIHJhIHPhuqNuaCBs4bubbiBoxqFuIFxuVmQ6IEogUSBLKOKZoykgPCBKIFEgSyjimaYpXG5cbjMgxJHDtGkgdGjDtG5nOiAzIMSRw7RpIGLDoGkgY8OzIGdpw6EgdHLhu4sgdMSDbmcgZOG6p24gNCA0ICsgNSA1ICsgNiA2IFxuXG5U4bupIHF1w706IDQgbMOhIGLDoGkgY8OzIGdpw6EgdHLhu4sgZ2nhu5FuZyBuaGF1IDUgNSA1IDUsIEogSiBKIEosIFEgUSBRIFEgXG4gICAgICAgICAgICAgIDQgxJHDtGkgdGjDtG5nOiA0IMSRw7RpIGLDoGkgY8OzIGdpw6EgdHLhu4sgdMSDbmcgZOG6p24gNCA0ICsgNSA1ICsgNiA2ICsgNyA3IFxuICAgICAgICAgICAgICA1IMSRw7RpIHRow7RuZzogNSDEkcO0aSBiw6BpIGPDsyBnacOhIHRy4buLIHTEg25nIGThuqduIDQgNCArIDUgNSArIDYgNiArIDcgNyArIDggOFxuICAgICAgICAgICAgIDYgxJHDtGkgdGjDtG5nOiA2IMSRw7RpIGLDoGkgY8OzIGdpw6EgdHLhu4sgdMSDbmcgZOG6p24gNCA0ICsgNSA1ICsgNiA2ICsgNyA3ICsgOCA4ICsgOSA5XG5cbsSQ4buZIG3huqFuaCBj4bunYSBow6BuZyDEkcaw4bujYyB0w61uaCBuaMawIHNhdTogNiDEkcO0aSB0aMO0bmcgPiA1IMSRw7RpIHRow7RuZyA+IDQgxJHDtGkgdGjDtG5nID4gNCDEkcO0aSB0aMO0bmcgbmjhu48gaMahbiA+IHThu6kgcXXDvSA+IDMgxJHDtGkgdGjDtG5nXG5cbsSQw6FuaCBiw6BpOlxuXG5RdXnhu4FuIMSRw6FuaCB0csaw4bubYzogVsOhbiDEkeG6p3UgdGnDqm4sIHF1eeG7gW4gxJHDoW5oIHRyxrDhu5tjIHPhur0gdGh14buZYyB24buBIG5nxrDhu51pIHPhu58gaOG7r3UgMyDimaAuXG5cblThu6sgdsOhbiB0aeG6v3AgdGhlbyBuZ8aw4budaSBuaOG6pXQg4bufIHbDoW4gdHLGsOG7m2MgxJHDsyBz4bq9IMSRxrDhu6NjIHF1eeG7gW4gxJHDoW5oIHRyxrDhu5tjIC5cblxuVsOybmcgxJHDoW5oOiBUaGVvIG5nxrDhu6NjIGNoaeG7gXUga2ltIMSR4buTbmcgaOG7kywgbeG7l2kgbmfGsOG7nWkgxJHGsOG7o2MgcmEgMSBsw6EgYsOgaSBob+G6t2MgMSBi4buZIG5oaeG7gXUgbMOhLiBOZ8aw4budaSByYSBzYXUgcGjhuqNpIMSRw6FuaCBiw6BpIGPDsyBjw7luZyBsb+G6oWkgdsOgIGNhbyBoxqFuIG5nxrDhu51pIMSRw6FuaCB0csaw4bubYywgdHLhu6sgdHLGsOG7nW5nIGjhu6NwIGNo4bq3dCBiw6BpLiBMb+G6oWkgbMOgIGPDuW5nIGLDoGkgbOG6uywgxJHDtGksIGLhu5kgYmEsc+G6o25oLlxuXG5I4bq/dCB2w7JuZyDEkcOhbmg6IHRyb25nIHbDsm5nIMSRw6FuaCwgbuG6v3UgY8OzICAxIG5nxrDhu51pIGLhu48gbMaw4bujdCB0aMOsIGNvaSBuaMawIGLhu48gY+G6oyB2w7JuZy4gTuG6v3Uga2jDtG5nIGPDsm4gYWkgY2jhurduIMSRxrDhu6NjIHRp4bq/cCB0aMOsIG5nxrDhu51pIMSRw6FuaCBjdeG7kWkgY8O5bmcgxJHGsOG7o2MgcmEgYsOgaSBi4bqvdCDEkeG6p3UgdsOybmcgbeG7m2kgLlxuXG5L4bq/dCB0aMO6YzogXG5cbktoaSBuZ8aw4budaSDEkeG6p3UgdGnDqm4gxJHDoW5oIGjhur90IGLDoGksIG5nxrDhu51pIMSRw7Mgc+G6vSDEkcaw4bujYyB0w61uaCBOaOG6pXRcblxuTmjhu69uZyBuZ8aw4budaSBjw7JuIGzhuqFpIHRp4bq/cCB04bulYyDEkcOhbmggYsOgaSDEkeG7gyBjaOG7jW4gcmEgdGjhu6kgdOG7sSB24buBIE5ow6wgLCBCYSAsIELDqXRcblxuQ2jhurd0IDIgKEhlbykgOlxuMyDEkcO0aSB0aMO0bmcgY2jhurd0IMSRxrDhu6NjIDIoaGVvKSB2w6AgMyDEkcO0aSB0aMO0bmcgbmjhu48gaMahbiAoIHRoZW8gdsOybmcgY2jGoWkgKSBcblxuVOG7qSBxdcO9IGNo4bq3dCDEkcaw4bujYyBoZW8sIMSRw7RpIGhlbywgMyDEkcO0aSB0aMO0bmcgdsOgIHThu6kgcXXDvSBuaOG7jyBoxqFuICggdGhlbyB2w7JuZyBjaMahaSApIFxuXG40IMSRw7RpIHRow7RuZyBjaOG6t3QgxJHGsOG7o2MgaGVvLCDEkcO0aSBoZW8sIDMgxJHDtGkgdGjDtG5nICwgdOG7qSBxdcO9IHbDoCA0IMSRw7RpIHRow7RuZyBuaOG7jyBoxqFuICgga2jDtG5nIGPhuqduIHRoZW8gdsOybmcgY2jGoWkgKSBcblxuXCIgQ2jhurd0IGNo4buTbmcgXCIgY3Xhu5FpIGPDuW5nIGzDoCB04buVbmcga+G6v3QgdOG6pXQgY+G6oyBjw6FjIGjDoG5oIHZpIFwiY2jhurd0XCIgdHLGsOG7m2MgxJHDsyAuIE5nxrDhu51pIGLhu4sgY2jhurd0IHNhdSBjw7luZyBz4bq9IGNo4buLdSB0aGnhu4d0IGjhuqFpIHRp4buBbiBjaOG6t3QgLlxuXG5U4bubaSB0cuG6r25nOiBraeG7g3UgdGjhuq9uZyBuZ2F5IGzhuq1wIHThu6ljIGtow7RuZyBj4bqnbiDEkcOhbmgga2hpIG5nxrDhu51pIGNoxqFpIHRo4buPYSBtw6NuIGLhu5kgYsOgaSBzYXUgOlxuXG5U4bqhaSB2w6FuIMSR4bqndTogVOG7qSBxdcO9IDMgaG/hurdjIDMgxJHDtGkgdGjDtG5nIGPDsyAzIOKZoFxuXG5Dw6FjIHbDoW4gc2F1OiBU4bupIHF1w70gMiAsIDUgxJHDtGkgdGjDtG5nICwgNiDEkcO0aSB0aMO0bmcgLCA2IMSRw7RpIGLhuqV0IGvhu7MgLCBT4bqjbmggUuG7k25nICggc+G6o25oIG7hu5FpIHThu6sgMyAtPiBBICkgLCAxMiAvIDEzIGzDoSBiw6BpIGPDuW5nIG3DoHUgKCDimaPimaAgaG/hurdjIOKZpeKZpiApIFxuXG5UaOG7kWkgYsOgaTogXG5cbk5nxrDhu51pIGNoxqFpIGLhu4sgdMOtbmggVGjhu5FpIGtoaSBuZ8aw4budaSDEkcOzIHbhu4EgQsOpdCB2w6AgdHLDqm4gdGF5IGjhu40gY8OybiBjw6FjIGzDoSBiw6BpIHNhdVxuVGjhu5FpIGPDsyAyKGhlbylcblRo4buRaSAzIMSRw7RpIHRow7RuZ1xuVGjhu5FpIDQgxJHDtGkgdGjDtG5nXG5UaOG7kWkgdOG7qSBxdcO9XG5cbk5nxrDhu51pIGNoxqFpIGLhu4sgQ2jDoXkgYsOgaSBraGkgdHJvbmcgdsOhbiBjaMahaSBjw7MgbmfGsOG7nWkgduG7gSBOaOG6pXQgaOG6v3QgYsOgaSAsIG5oxrBuZyBuZ8aw4budaSBjaMahaSDEkcOzIHbhuqtuIGNoxrBhIMSRw6FuaCBsw6EgYsOgaSBuw6BvIChjw7JuIMSR4bunIDEzIGzDoSkuYFxuXG5leHBvcnQgZGVmYXVsdCB0dXRvcjtcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/vi_VN.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '92027qqs+hEwbWfts3Mohj3', 'vi_VN');
// lang/vi_VN.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vi_VN_tut_1 = require("./vi_VN_tut");
exports.default = {
    'PLAYNOW': 'Chơi ngay',
    'WIN': 'Nhất',
    'LOSE2': 'Nhì',
    'LOSE3': 'Ba',
    'LOSE4': 'Bét',
    'PLAYER': 'Số người chơi',
    'BETNO': 'Mức cược',
    'DAILY': 'Thưởng hàng ngày',
    'D1': 'Ngày 1',
    'D2': 'Ngày 2',
    'D3': 'Ngày 3',
    'D4': 'Ngày 4',
    'D5': 'Ngày 5',
    'D6': 'Ngày 6',
    'D7': 'Ngày 7',
    '3TURNS': '3 lượt',
    '5TURNS': '5 lượt',
    'TURNS': '5 lượt',
    'RECEIVED': 'Đã nhận',
    'LEADER': 'Bảng xếp hạng',
    'NOVIDEO': 'Video hiện không khả dụng',
    'BET': 'Cược',
    'NO': 'Bàn',
    'PASS': 'Bỏ lượt',
    'HIT': 'Đánh',
    'ARRANGE': 'Xếp bài',
    'QUITGAME': 'Thoát game',
    'QUITGAMEP': 'Bạn có chắc muốn thoát game? Nếu thoát game bạn sẽ bị trừ 10 lần mức cược',
    'QUIT': 'Thoát',
    '3PAIRS': '3 đôi thông',
    '4PAIRS': '4 đôi thông',
    'FOURKIND': 'Tứ quý',
    'FLUSH': 'Tới trắng',
    '1BEST': 'Heo',
    '2BEST': 'Đôi heo',
    '3BEST': '3 heo',
    'INSTRUCT': 'Hướng dẫn',
    'NOMONEY': 'Bạn đã hết tiền, nhận tài trợ từ nhà cái',
    'RECEI2': 'Nhận',
    'SPINNOW': 'Quay ngay',
    'SPIN': 'Quay ',
    '1TURN': 'Thêm lượt',
    'LUCKYSPIN': 'Vòng quay may mắn',
    'LUCK': 'Chúc bạn may mắn lần sau',
    'TURN': 'Bạn được cộng thêm lượt',
    'X2': 'Nhân đôi tiền thưởng',
    'X3': 'Nhân ba tiền thưởng',
    'X5': 'Nhân năm tiền thưởng',
    'X10': 'Nhân mười tiền thưởng',
    'MISS': 'Mất lượt',
    'MONEY1': 'Chúc mừng bạn đã nhận được %s từ nhà cái',
    'TUT': vi_VN_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3ZpX1ZOLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQStCO0FBRS9CLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFdBQVc7SUFDdEIsS0FBSyxFQUFFLE1BQU07SUFDYixPQUFPLEVBQUUsS0FBSztJQUNkLE9BQU8sRUFBRSxJQUFJO0lBQ2IsT0FBTyxFQUFFLEtBQUs7SUFDZCxRQUFRLEVBQUUsZUFBZTtJQUN6QixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsa0JBQWtCO0lBQzNCLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsUUFBUSxFQUFFLFFBQVE7SUFDbEIsUUFBUSxFQUFFLFFBQVE7SUFDbEIsT0FBTyxFQUFFLFFBQVE7SUFDakIsVUFBVSxFQUFFLFNBQVM7SUFDckIsUUFBUSxFQUFFLGVBQWU7SUFDekIsU0FBUyxFQUFFLDJCQUEyQjtJQUN0QyxLQUFLLEVBQUUsTUFBTTtJQUNiLElBQUksRUFBRSxLQUFLO0lBQ1gsTUFBTSxFQUFFLFNBQVM7SUFDakIsS0FBSyxFQUFFLE1BQU07SUFDYixTQUFTLEVBQUUsU0FBUztJQUNwQixVQUFVLEVBQUUsWUFBWTtJQUN4QixXQUFXLEVBQUUsMkVBQTJFO0lBQ3hGLE1BQU0sRUFBRSxPQUFPO0lBQ2YsUUFBUSxFQUFFLGFBQWE7SUFDdkIsUUFBUSxFQUFFLGFBQWE7SUFDdkIsVUFBVSxFQUFFLFFBQVE7SUFDcEIsT0FBTyxFQUFFLFdBQVc7SUFDcEIsT0FBTyxFQUFFLEtBQUs7SUFDZCxPQUFPLEVBQUUsU0FBUztJQUNsQixPQUFPLEVBQUUsT0FBTztJQUNoQixVQUFVLEVBQUUsV0FBVztJQUN2QixTQUFTLEVBQUUsMENBQTBDO0lBQ3JELFFBQVEsRUFBRSxNQUFNO0lBQ2hCLFNBQVMsRUFBRSxXQUFXO0lBQ3RCLE1BQU0sRUFBRSxPQUFPO0lBQ2YsT0FBTyxFQUFFLFdBQVc7SUFDcEIsV0FBVyxFQUFFLG1CQUFtQjtJQUNoQyxNQUFNLEVBQUUsMEJBQTBCO0lBQ2xDLE1BQU0sRUFBRSx5QkFBeUI7SUFDakMsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QixJQUFJLEVBQUUscUJBQXFCO0lBQzNCLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsS0FBSyxFQUFFLHVCQUF1QjtJQUM5QixNQUFNLEVBQUUsVUFBVTtJQUNsQixRQUFRLEVBQUUsMENBQTBDO0lBQ3BELEtBQUssRUFBRSxtQkFBSztDQUNmLENBQUEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgdHV0b3IgZnJvbSAnLi92aV9WTl90dXQnXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdDaMahaSBuZ2F5JyxcbiAgICAnV0lOJzogJ05o4bqldCcsXG4gICAgJ0xPU0UyJzogJ05ow6wnLFxuICAgICdMT1NFMyc6ICdCYScsXG4gICAgJ0xPU0U0JzogJ0LDqXQnLFxuICAgICdQTEFZRVInOiAnU+G7kSBuZ8aw4budaSBjaMahaScsXG4gICAgJ0JFVE5PJzogJ03hu6ljIGPGsOG7o2MnLFxuICAgICdEQUlMWSc6ICdUaMaw4bufbmcgaMOgbmcgbmfDoHknLFxuICAgICdEMSc6ICdOZ8OgeSAxJyxcbiAgICAnRDInOiAnTmfDoHkgMicsXG4gICAgJ0QzJzogJ05nw6B5IDMnLFxuICAgICdENCc6ICdOZ8OgeSA0JyxcbiAgICAnRDUnOiAnTmfDoHkgNScsXG4gICAgJ0Q2JzogJ05nw6B5IDYnLFxuICAgICdENyc6ICdOZ8OgeSA3JyxcbiAgICAnM1RVUk5TJzogJzMgbMaw4bujdCcsXG4gICAgJzVUVVJOUyc6ICc1IGzGsOG7o3QnLFxuICAgICdUVVJOUyc6ICc1IGzGsOG7o3QnLFxuICAgICdSRUNFSVZFRCc6ICfEkMOjIG5o4bqtbicsXG4gICAgJ0xFQURFUic6ICdC4bqjbmcgeOG6v3AgaOG6oW5nJyxcbiAgICAnTk9WSURFTyc6ICdWaWRlbyBoaeG7h24ga2jDtG5nIGto4bqjIGThu6VuZycsXG4gICAgJ0JFVCc6ICdDxrDhu6NjJyxcbiAgICAnTk8nOiAnQsOgbicsXG4gICAgJ1BBU1MnOiAnQuG7jyBsxrDhu6N0JyxcbiAgICAnSElUJzogJ8SQw6FuaCcsXG4gICAgJ0FSUkFOR0UnOiAnWOG6v3AgYsOgaScsXG4gICAgJ1FVSVRHQU1FJzogJ1Rob8OhdCBnYW1lJyxcbiAgICAnUVVJVEdBTUVQJzogJ0LhuqFuIGPDsyBjaOG6r2MgbXXhu5FuIHRob8OhdCBnYW1lPyBO4bq/dSB0aG/DoXQgZ2FtZSBi4bqhbiBz4bq9IGLhu4sgdHLhu6sgMTAgbOG6p24gbeG7qWMgY8aw4bujYycsXG4gICAgJ1FVSVQnOiAnVGhvw6F0JyxcbiAgICAnM1BBSVJTJzogJzMgxJHDtGkgdGjDtG5nJyxcbiAgICAnNFBBSVJTJzogJzQgxJHDtGkgdGjDtG5nJyxcbiAgICAnRk9VUktJTkQnOiAnVOG7qSBxdcO9JyxcbiAgICAnRkxVU0gnOiAnVOG7m2kgdHLhuq9uZycsXG4gICAgJzFCRVNUJzogJ0hlbycsXG4gICAgJzJCRVNUJzogJ8SQw7RpIGhlbycsXG4gICAgJzNCRVNUJzogJzMgaGVvJyxcbiAgICAnSU5TVFJVQ1QnOiAnSMaw4bubbmcgZOG6q24nLFxuICAgICdOT01PTkVZJzogJ0LhuqFuIMSRw6MgaOG6v3QgdGnhu4FuLCBuaOG6rW4gdMOgaSB0cuG7oyB04burIG5ow6AgY8OhaScsXG4gICAgJ1JFQ0VJMic6ICdOaOG6rW4nLFxuICAgICdTUElOTk9XJzogJ1F1YXkgbmdheScsXG4gICAgJ1NQSU4nOiAnUXVheSAnLFxuICAgICcxVFVSTic6ICdUaMOqbSBsxrDhu6N0JyxcbiAgICAnTFVDS1lTUElOJzogJ1bDsm5nIHF1YXkgbWF5IG3huq9uJyxcbiAgICAnTFVDSyc6ICdDaMO6YyBi4bqhbiBtYXkgbeG6r24gbOG6p24gc2F1JyxcbiAgICAnVFVSTic6ICdC4bqhbiDEkcaw4bujYyBj4buZbmcgdGjDqm0gbMaw4bujdCcsXG4gICAgJ1gyJzogJ05ow6JuIMSRw7RpIHRp4buBbiB0aMaw4bufbmcnLFxuICAgICdYMyc6ICdOaMOibiBiYSB0aeG7gW4gdGjGsOG7n25nJyxcbiAgICAnWDUnOiAnTmjDom4gbsSDbSB0aeG7gW4gdGjGsOG7n25nJyxcbiAgICAnWDEwJzogJ05ow6JuIG3GsOG7nWkgdGnhu4FuIHRoxrDhu59uZycsXG4gICAgJ01JU1MnOiAnTeG6pXQgbMaw4bujdCcsXG4gICAgJ01PTkVZMSc6ICdDaMO6YyBt4burbmcgYuG6oW4gxJHDoyBuaOG6rW4gxJHGsOG7o2MgJXMgdOG7qyBuaMOgIGPDoWknLFxuICAgICdUVVQnOiB0dXRvclxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/en_US.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0d583jnHB5GtZrm4qYwUeQu', 'en_US');
// lang/en_US.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var en_US_tut_1 = require("./en_US_tut");
exports.default = {
    'PLAYNOW': 'Play now',
    'WIN': '1st',
    'LOSE2': '2nd',
    'LOSE3': '3rd',
    'LOSE4': '4th',
    'PLAYER': 'Number of players',
    'BETNO': 'Bet',
    'DAILY': 'Daily reward',
    'D1': 'Day 1',
    'D2': 'Day 2',
    'D3': 'Day 3',
    'D4': 'Day 4',
    'D5': 'Day 5',
    'D6': 'Day 6',
    'D7': 'Day 7',
    '3TURNS': '3 turns',
    '5TURNS': '5 turns',
    'TURNS': 'turns',
    'RECEIVED': 'Received',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Video cannot be played now',
    'BET': 'Bet',
    'NO': 'No',
    'PASS': 'Pass',
    'HIT': 'Hit',
    'ARRANGE': 'Arrange',
    'QUITGAME': 'Quit game',
    'QUITGAMEP': 'Do you want to quit game? If you quit game you\'ll lose ten times as bet level',
    'QUIT': 'Quit',
    '3PAIRS': '3 consecutive pairs',
    '4PAIRS': '4 consecutive pairs',
    'FOURKIND': 'Four of a kind',
    'FLUSH': 'Straight Flush',
    '1BEST': 'best',
    '2BEST': '2 best',
    '3BEST': '3 best',
    'INSTRUCT': 'Instruction',
    'NOMONEY': 'You\'ve run out of money, click to receive more money',
    'RECEI2': 'Claim',
    'SPINNOW': 'Spin',
    'SPIN': 'Spin',
    '1TURN': '1 more turn',
    'LUCKYSPIN': 'Lucky Spin',
    'LUCK': 'Have a luck later',
    'TURN': 'One more turn',
    'X2': 'X2 money',
    'X3': 'X3 money',
    'X5': 'X5 money',
    'X10': 'X10 money',
    'MISS': 'Miss',
    'MONEY1': 'Congratulation! You\'ve got %s',
    'TUT': en_US_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL2VuX1VTLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQWdDO0FBRWhDLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFVBQVU7SUFDckIsS0FBSyxFQUFFLEtBQUs7SUFDWixPQUFPLEVBQUUsS0FBSztJQUNkLE9BQU8sRUFBRSxLQUFLO0lBQ2QsT0FBTyxFQUFFLEtBQUs7SUFDZCxRQUFRLEVBQUUsbUJBQW1CO0lBQzdCLE9BQU8sRUFBRSxLQUFLO0lBQ2QsT0FBTyxFQUFFLGNBQWM7SUFDdkIsSUFBSSxFQUFFLE9BQU87SUFDYixJQUFJLEVBQUUsT0FBTztJQUNiLElBQUksRUFBRSxPQUFPO0lBQ2IsSUFBSSxFQUFFLE9BQU87SUFDYixJQUFJLEVBQUUsT0FBTztJQUNiLElBQUksRUFBRSxPQUFPO0lBQ2IsSUFBSSxFQUFFLE9BQU87SUFDYixRQUFRLEVBQUUsU0FBUztJQUNuQixRQUFRLEVBQUUsU0FBUztJQUNuQixPQUFPLEVBQUUsT0FBTztJQUNoQixVQUFVLEVBQUUsVUFBVTtJQUN0QixRQUFRLEVBQUUsYUFBYTtJQUN2QixTQUFTLEVBQUUsNEJBQTRCO0lBQ3ZDLEtBQUssRUFBRSxLQUFLO0lBQ1osSUFBSSxFQUFFLElBQUk7SUFDVixNQUFNLEVBQUUsTUFBTTtJQUNkLEtBQUssRUFBRSxLQUFLO0lBQ1osU0FBUyxFQUFFLFNBQVM7SUFDcEIsVUFBVSxFQUFFLFdBQVc7SUFDdkIsV0FBVyxFQUFFLGdGQUFnRjtJQUM3RixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxxQkFBcUI7SUFDL0IsUUFBUSxFQUFFLHFCQUFxQjtJQUMvQixVQUFVLEVBQUUsZ0JBQWdCO0lBQzVCLE9BQU8sRUFBRSxnQkFBZ0I7SUFDekIsT0FBTyxFQUFFLE1BQU07SUFDZixPQUFPLEVBQUUsUUFBUTtJQUNqQixPQUFPLEVBQUUsUUFBUTtJQUNqQixVQUFVLEVBQUUsYUFBYTtJQUN6QixTQUFTLEVBQUUsdURBQXVEO0lBQ2xFLFFBQVEsRUFBRSxPQUFPO0lBQ2pCLFNBQVMsRUFBRSxNQUFNO0lBQ2pCLE1BQU0sRUFBRSxNQUFNO0lBQ2QsT0FBTyxFQUFFLGFBQWE7SUFDdEIsV0FBVyxFQUFFLFlBQVk7SUFDekIsTUFBTSxFQUFFLG1CQUFtQjtJQUMzQixNQUFNLEVBQUUsZUFBZTtJQUN2QixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixLQUFLLEVBQUUsV0FBVztJQUNsQixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxnQ0FBZ0M7SUFDMUMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL2VuX1VTX3R1dCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdQbGF5IG5vdycsXG4gICAgJ1dJTic6ICcxc3QnLFxuICAgICdMT1NFMic6ICcybmQnLFxuICAgICdMT1NFMyc6ICczcmQnLFxuICAgICdMT1NFNCc6ICc0dGgnLFxuICAgICdQTEFZRVInOiAnTnVtYmVyIG9mIHBsYXllcnMnLFxuICAgICdCRVROTyc6ICdCZXQnLFxuICAgICdEQUlMWSc6ICdEYWlseSByZXdhcmQnLFxuICAgICdEMSc6ICdEYXkgMScsXG4gICAgJ0QyJzogJ0RheSAyJyxcbiAgICAnRDMnOiAnRGF5IDMnLFxuICAgICdENCc6ICdEYXkgNCcsXG4gICAgJ0Q1JzogJ0RheSA1JyxcbiAgICAnRDYnOiAnRGF5IDYnLFxuICAgICdENyc6ICdEYXkgNycsXG4gICAgJzNUVVJOUyc6ICczIHR1cm5zJyxcbiAgICAnNVRVUk5TJzogJzUgdHVybnMnLFxuICAgICdUVVJOUyc6ICd0dXJucycsXG4gICAgJ1JFQ0VJVkVEJzogJ1JlY2VpdmVkJyxcbiAgICAnTEVBREVSJzogJ0xlYWRlcmJvYXJkJyxcbiAgICAnTk9WSURFTyc6ICdWaWRlbyBjYW5ub3QgYmUgcGxheWVkIG5vdycsXG4gICAgJ0JFVCc6ICdCZXQnLFxuICAgICdOTyc6ICdObycsXG4gICAgJ1BBU1MnOiAnUGFzcycsXG4gICAgJ0hJVCc6ICdIaXQnLFxuICAgICdBUlJBTkdFJzogJ0FycmFuZ2UnLFxuICAgICdRVUlUR0FNRSc6ICdRdWl0IGdhbWUnLFxuICAgICdRVUlUR0FNRVAnOiAnRG8geW91IHdhbnQgdG8gcXVpdCBnYW1lPyBJZiB5b3UgcXVpdCBnYW1lIHlvdVxcJ2xsIGxvc2UgdGVuIHRpbWVzIGFzIGJldCBsZXZlbCcsXG4gICAgJ1FVSVQnOiAnUXVpdCcsXG4gICAgJzNQQUlSUyc6ICczIGNvbnNlY3V0aXZlIHBhaXJzJyxcbiAgICAnNFBBSVJTJzogJzQgY29uc2VjdXRpdmUgcGFpcnMnLFxuICAgICdGT1VSS0lORCc6ICdGb3VyIG9mIGEga2luZCcsXG4gICAgJ0ZMVVNIJzogJ1N0cmFpZ2h0IEZsdXNoJyxcbiAgICAnMUJFU1QnOiAnYmVzdCcsXG4gICAgJzJCRVNUJzogJzIgYmVzdCcsXG4gICAgJzNCRVNUJzogJzMgYmVzdCcsXG4gICAgJ0lOU1RSVUNUJzogJ0luc3RydWN0aW9uJyxcbiAgICAnTk9NT05FWSc6ICdZb3VcXCd2ZSBydW4gb3V0IG9mIG1vbmV5LCBjbGljayB0byByZWNlaXZlIG1vcmUgbW9uZXknLFxuICAgICdSRUNFSTInOiAnQ2xhaW0nLFxuICAgICdTUElOTk9XJzogJ1NwaW4nLFxuICAgICdTUElOJzogJ1NwaW4nLFxuICAgICcxVFVSTic6ICcxIG1vcmUgdHVybicsXG4gICAgJ0xVQ0tZU1BJTic6ICdMdWNreSBTcGluJyxcbiAgICAnTFVDSyc6ICdIYXZlIGEgbHVjayBsYXRlcicsXG4gICAgJ1RVUk4nOiAnT25lIG1vcmUgdHVybicsXG4gICAgJ1gyJzogJ1gyIG1vbmV5JyxcbiAgICAnWDMnOiAnWDMgbW9uZXknLFxuICAgICdYNSc6ICdYNSBtb25leScsXG4gICAgJ1gxMCc6ICdYMTAgbW9uZXknLFxuICAgICdNSVNTJzogJ01pc3MnLFxuICAgICdNT05FWTEnOiAnQ29uZ3JhdHVsYXRpb24hIFlvdVxcJ3ZlIGdvdCAlcycsXG4gICAgJ1RVVCc6IHR1dG9yXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/AudioPlayer.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a8fc4W4Bk5LCI1G39gq+6eK', 'AudioPlayer');
// Scripts/AudioPlayer.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AudioPlayer = /** @class */ (function (_super) {
    __extends(AudioPlayer, _super);
    function AudioPlayer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioClip = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    AudioPlayer.prototype.start = function () {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "AudioPlayer"; // This is the code file name
        clickEventHandler.handler = "play";
        var button = this.node.getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    };
    // update (dt) {}
    AudioPlayer.prototype.play = function () {
        if (Config_1.default.soundEnable)
            cc.audioEngine.play(this.audioClip, false, 1);
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], AudioPlayer.prototype, "audioClip", void 0);
    AudioPlayer = __decorate([
        ccclass
    ], AudioPlayer);
    return AudioPlayer;
}(cc.Component));
exports.default = AudioPlayer;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0F1ZGlvUGxheWVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLG1DQUE4QjtBQUV4QixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUF5QywrQkFBWTtJQUFyRDtRQUFBLHFFQTBCQztRQXhCRyxlQUFTLEdBQWlCLElBQUksQ0FBQzs7SUF3Qm5DLENBQUM7SUF0Qkcsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZiwyQkFBSyxHQUFMO1FBQ0ksSUFBSSxpQkFBaUIsR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDeEQsaUJBQWlCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQywyRUFBMkU7UUFDakgsaUJBQWlCLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFBLDZCQUE2QjtRQUN6RSxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRW5DLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxpQkFBaUI7SUFFakIsMEJBQUksR0FBSjtRQUNJLElBQUcsZ0JBQU0sQ0FBQyxXQUFXO1lBQ2pCLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFyQkQ7UUFEQyxRQUFRLENBQUMsRUFBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBQyxDQUFDO2tEQUNBO0lBRmQsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQTBCL0I7SUFBRCxrQkFBQztDQTFCRCxBQTBCQyxDQTFCd0MsRUFBRSxDQUFDLFNBQVMsR0EwQnBEO2tCQTFCb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgQ29uZmlnIGZyb20gXCIuL0NvbmZpZ1wiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEF1ZGlvUGxheWVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoe3R5cGU6IGNjLkF1ZGlvQ2xpcH0pXG4gICAgYXVkaW9DbGlwOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdmFyIGNsaWNrRXZlbnRIYW5kbGVyID0gbmV3IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIoKTtcbiAgICAgICAgY2xpY2tFdmVudEhhbmRsZXIudGFyZ2V0ID0gdGhpcy5ub2RlOyAvLyBUaGlzIG5vZGUgaXMgdGhlIG5vZGUgdG8gd2hpY2ggeW91ciBldmVudCBoYW5kbGVyIGNvZGUgY29tcG9uZW50IGJlbG9uZ3NcbiAgICAgICAgY2xpY2tFdmVudEhhbmRsZXIuY29tcG9uZW50ID0gXCJBdWRpb1BsYXllclwiOy8vIFRoaXMgaXMgdGhlIGNvZGUgZmlsZSBuYW1lXG4gICAgICAgIGNsaWNrRXZlbnRIYW5kbGVyLmhhbmRsZXIgPSBcInBsYXlcIjtcblxuICAgICAgICB2YXIgYnV0dG9uID0gdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5CdXR0b24pO1xuICAgICAgICBidXR0b24uY2xpY2tFdmVudHMucHVzaChjbGlja0V2ZW50SGFuZGxlcik7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHBsYXkoKSB7XG4gICAgICAgIGlmKENvbmZpZy5zb3VuZEVuYWJsZSlcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5hdWRpb0NsaXAsIGZhbHNlLCAxKTtcbiAgICB9XG5cblxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c8251pV8NZGpa2LG0Z5yRed', 'Game');
// Scripts/Game.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = require("./Player");
var Deck_1 = require("./Deck");
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var CommonCard_1 = require("./CommonCard");
var Toast_1 = require("./Toast");
var Api_1 = require("./Api");
var Notice_1 = require("./Notice");
var Config_1 = require("./Config");
var util_1 = require("./util");
var SpinWheel_1 = require("./SpinWheel");
var Popup_1 = require("./Popup");
var Bot_1 = require("./Bot");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var suiteAsset = {
    H: 'co',
    D: "ro",
    C: "chuon",
    S: "bích"
};
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Profile = /** @class */ (function () {
    function Profile() {
        this.avatar = null;
        this.username = '';
    }
    __decorate([
        property(cc.SpriteFrame)
    ], Profile.prototype, "avatar", void 0);
    __decorate([
        property(cc.String)
    ], Profile.prototype, "username", void 0);
    Profile = __decorate([
        ccclass('Profile')
    ], Profile);
    return Profile;
}());
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deck = null;
        _this.play = null;
        _this.seats = [];
        _this.fire = null;
        _this.fold = null;
        _this.sort = null;
        _this.resultEffects = [];
        _this.bigWin = null;
        _this.toast = null;
        _this.notice = null;
        _this.betText = null;
        _this.spin = null;
        _this.popup = null;
        _this.profileBots = [];
        _this.soundToggle = null;
        _this.audioFold = null;
        _this.audioShowCard = null;
        _this.audioGarbageCard = null;
        _this.audioFire = null;
        _this.audioLose = null;
        _this.audioWin = null;
        _this.audioSortCard = null;
        _this.audioQuitGame = null;
        _this.audioDealCard = null;
        _this.audioFireSingle = null;
        _this.modal = null;
        _this.players = [];
        _this.commonCards = new CommonCard_1.default();
        _this.state = 0;
        _this.turn = 0;
        _this.leaveGame = false;
        _this.owner = null;
        _this.myself = null;
        _this.betValue = 1000;
        _this.totalPlayer = 6;
        _this.startTime = 0;
        _this.playTime = 0;
        _this.addSeat = function (seat) {
            var profile = _this.getProfileBot();
            seat.show();
            seat.setAvatar(profile.avatar);
            seat.setUsername(profile.username);
            _this.players.push(seat);
        };
        _this.showAllPlayerCard = function () {
            _this.players.map(function (player) {
                player.showAllCard();
            });
        };
        _this.showResult = function () {
            // let leaderPoint = this.players.slice(-1)[0].point
            // let userPoint = this.players[0].point
            // let resultString = [`điểm cái: ${leaderPoint}`, `Điểm user: ${userPoint}`]
            // this.players.map(p => {
            //     if (p.isBot() && !p.isCai()) {
            //         resultString.push(`Điểm bot ${resultString.length - 1}: ${p.point}`)
            //     }
            // })
            var dealer = _this.players[0];
            var dealerAddCoin = 0;
            _this.players.map(function (player, index) {
                if (!index)
                    return;
                var playerCoin = _this.calCointEndGame(player);
                if (player.subUser) {
                    playerCoin += _this.calCointEndGame(player.subUser);
                }
                player.changeCoin(playerCoin);
                console.log('====================================');
                console.log('user ' + index + ' ' + playerCoin);
                console.log('====================================');
                dealerAddCoin -= playerCoin;
            });
            dealer.changeCoin(dealerAddCoin);
            console.log('====================================');
            console.log('nha cai ' + dealerAddCoin);
            console.log('====================================');
            _this.showAllPlayerCard();
            _this.delayReset(3);
            _this.state = Game_1.LATE;
            // this.owner = winner;
            // this.bigWin.active = true;
            // let total = 0;
            // for (let i = 0; i < this.players.length; i++) {
            //     let player = this.players[i];
            //     if (player != winner) {
            //         if (player.isBot()) {
            //             player.hideCardCounter();
            //             player.showHandCards();
            //             player.reorder();
            //         }
            //         let lost = 13 * this.betValue;
            //         lost = Math.min(player.coinVal, lost);
            //         player.subCoin(lost);
            //         total += lost;
            //     }
            // }
            // winner.addCoin(total);
            Api_1.default.updateCoin(_this.myself.coinVal);
            // this.onWin(total);
            // return this.toast.show(`Tổng cmn kết rồi\n ${resultString.join('\n')}`)
        };
        _this.calCointEndGame = function (player) {
            var dealer = _this.players[0];
            var playerCoin = 0;
            if (player.bonusType == 2) {
                playerCoin -= _this.betValue / 2;
            }
            else if (player.point > 21) {
                playerCoin -= _this.betValue;
                // player.subCoin(this.betValue)
            }
            else if (player.checkBlackJack()) {
                playerCoin += _this.betValue * 1.5;
            }
            else if (player.point > dealer.point) {
                playerCoin += _this.betValue;
            }
            else if (player.point === dealer.point) {
                return 0;
            }
            else if (dealer.point > 21) {
                playerCoin += _this.betValue;
            }
            else {
                playerCoin -= _this.betValue;
            }
            if (/1/.test(player.bonusType.toString())) {
                playerCoin *= 2;
            }
            return playerCoin;
        };
        _this.checkWin = function () {
            // console.log('====================================');
            // console.log(this.players);
            // console.log('====================================');
            return new Promise(function (resolve) {
                // this.players.
            });
        };
        return _this;
    }
    Game_1 = Game;
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        var _this = this;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        var As = cc.find('Canvas/cards/AS');
        cc.resources.load('cards', cc.SpriteAtlas, function (err, atlas) {
            var initCardsResouces = new Promise(function (resolve) {
                new Array(13).fill('').map(function (el, rank) {
                    ['S', 'C', 'D', 'H'].map(function (suite, idSuite) {
                        if (!rank && !suite)
                            return;
                        var r = rank + 1;
                        var newNode = cc.instantiate(As);
                        switch (r) {
                            case 1:
                                r = 'A';
                                break;
                            case 11:
                                r = 'J';
                                break;
                            case 12:
                                r = 'Q';
                                break;
                            case 13:
                                r = 'K';
                                break;
                        }
                        var assetSuiteName = suiteAsset[suite];
                        var rankAssetname = r;
                        if (idSuite < 2)
                            rankAssetname += '-1';
                        // console.log('====================================');
                        // console.log(rankAssetname, assetSuiteName,);
                        // console.log('====================================');
                        newNode.name = r + suite;
                        var suiteSprite = atlas.getSpriteFrame(assetSuiteName);
                        newNode.children[0].getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(rankAssetname);
                        newNode.children[1].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[2].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[3].getComponent(cc.Widget).target = newNode;
                        newNode.active = false;
                        // console.log('====================================');
                        // console.log(newNode);
                        // console.log('====================================');
                        As.parent.addChild(newNode);
                        if (rank === 12 && suite === 'H') {
                            setTimeout(function () {
                                resolve(1);
                            }, 500);
                        }
                    });
                });
            });
            Promise.all([initCardsResouces]).then(function (rs) {
                _this.initGame();
            }).catch(function (err) {
                console.log('====================================');
                console.log(err);
                console.log('====================================');
            });
        });
    };
    // start(){}
    Game.prototype.initGame = function () {
        var _this = this;
        this.startTime = Date.now();
        this.state = Game_1.WAIT;
        this.betValue = Config_1.default.betValue;
        this.totalPlayer = Config_1.default.totalPlayer;
        this.betText.string = Language_1.default.getInstance().get('NO') + ': ' + util_1.default.numberFormat(this.betValue);
        this.deck.node.active = false;
        this.sort.node.active = false;
        this.play.node.active = false;
        this.hideDashboard();
        this.notice.hide();
        this.spin.onSpinHide = this.onSpinHide.bind(this);
        this.spin.onSpinCompleted = this.onSpinCompleted.bind(this);
        if (Config_1.default.battle) {
            this.totalPlayer = 2;
        }
        var arr = /0|3/;
        if (this.totalPlayer === 4)
            arr = /0|1|3|5/;
        else if (this.totalPlayer === 6)
            arr = /0|1|2|3|4|5/;
        this.seats.map(function (seat, i) {
            seat.seat = _this.players.length;
            if (arr.test(i.toString()))
                _this.addSeat(seat);
            else {
                seat.hide();
            }
        });
        // for (let i = 0; i < this.totalPlayer; i++) {
        //     let seat = this.seats[i];
        //     seat.seat = i;
        //     if (i < this.totalPlayer) {
        //         let profile = this.getProfileBot();
        //         seat.show();
        //         seat.setAvatar(profile.avatar);
        //         seat.setUsername(profile.username);
        //         this.players.push(seat);
        //     } else {
        //         seat.hide();
        //     }
        // }
        this.myself = this.players[this.totalPlayer / 2];
        this.myself.setUsername(Api_1.default.username);
        this.myself.setCoin(Api_1.default.coin);
        this.myself.setTimeCallback(this.onPlayerTimeout, this, this.node);
        if (Config_1.default.userphoto) {
            this.myself.setAvatar(new cc.SpriteFrame(Config_1.default.userphoto));
        }
        if (Config_1.default.battle) {
            var bot = this.players[1];
            bot.setCoin(Config_1.default.battle.coin);
            bot.setAvatar(new cc.SpriteFrame(Config_1.default.battle.photo));
            bot.setUsername(Config_1.default.battle.username);
        }
        else {
            this.players.map(function (player) {
                if (player.isBot())
                    player.setCoin(Config_1.default.botCoin);
            });
        }
        this.node.runAction(cc.sequence(cc.delayTime(0.7), cc.callFunc(this.deal, this)));
        if (Config_1.default.soundEnable) {
            this.soundToggle.uncheck();
        }
        else {
            this.soundToggle.check();
        }
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.onDestroy = function () {
        // this.clearLogTime()
        // const duration = Date.now() - this.startTime;
        // Api.logEvent(EventKeys.PLAY_DURATION, Math.floor(duration / 1000));
    };
    // update(dt) {
    //     if (this.playTime < 30 && this.playTime + dt >= 30) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-30');
    //     } else if (this.playTime < 60 && this.playTime + dt >= 60) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-60');
    //     } else if (this.playTime < 90 && this.playTime + dt >= 90) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-90');
    //     } else if (this.playTime < 180 && this.playTime + dt >= 180) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-180');
    //     } else if (this.playTime < 360 && this.playTime + dt >= 360) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-360');
    //     } else if (this.playTime < 500 && this.playTime + dt >= 500) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-500');
    //     }
    //     this.playTime += dt;
    // }
    Game.prototype.onTouchStart = function (evt) {
        if (this.state != Game_1.PLAY)
            return;
        var selected = this.myself.touch(evt.getLocation());
        if (!selected)
            return;
        var prepareCards = this.myself.prepareCards;
        if (prepareCards.contains(selected)) {
            this.myself.unselectCard(selected);
            this.onCardSelected(selected, false);
        }
        else {
            this.myself.selectCard(selected);
            this.onCardSelected(selected, true);
        }
        if (this.turn != this.myself.seat) {
            return;
        }
        if (prepareCards.kind == CardGroup_1.default.Ungrouped) {
            this.allowFire(false);
            return;
        }
        if (this.commonCards.length() == 0) {
            this.allowFire(true);
            return;
        }
        if (prepareCards.gt(this.commonCards.peek())) {
            this.allowFire(true);
            return;
        }
        this.allowFire(false);
    };
    Game.prototype.onPlayerTimeout = function () {
        this.onStandClicked();
        // this.hideDashboard();
        // if (this.commonCards.length() == 0) {
        //     let cards = Helper.findMinCard(this.myself.cards);
        //     this.onFire(this.myself, new CardGroup([cards], CardGroup.Single));
        //     this.myself.reorder();
        // } else {
        //     return this.onFold(this.myself);
        // }
    };
    Game.prototype.onQuitClicked = function () {
        Api_1.default.logEvent(EventKeys_1.default.QUIT_GAME);
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            this.popup.open(this.node, 1);
        }
        else {
            util_1.default.playAudio(this.audioQuitGame);
            cc.director.loadScene('home');
            Api_1.default.showInterstitialAd();
        }
    };
    Game.prototype.onBackClicked = function () {
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            var coin = Math.max(0, Api_1.default.coin - this.betValue * 10);
            Api_1.default.updateCoin(coin);
        }
        util_1.default.playAudio(this.audioQuitGame);
        cc.director.loadScene('home');
        Api_1.default.showInterstitialAd();
    };
    Game.prototype.showHandCards = function () {
        util_1.default.playAudio(this.audioShowCard);
        // this.myself.showHandCards();
        this.state = Game_1.PLAY;
        // console.log('====================================');
        // console.log(this.myself.cards);
        // console.log('====================================');
        // if (Helper.isBigWin(this.myself.cards)) {
        //     this.node.runAction(cc.sequence(
        //         cc.delayTime(0.7),
        //         cc.callFunc(this.onBigWin, this, this.myself)
        //     ));
        //     return;
        // }
        // this.node.runAction(cc.sequence(
        //     cc.delayTime(0.7),
        //     cc.callFunc(this.myself.sortHandCards, this.myself)
        // ));
        this.players.map(function (player, index) {
            if (player.isBot())
                player.showCardCounter();
            else {
                player.showAllCard();
            }
        });
        var checkBJ = this.players.filter(function (player) {
            player.checkBlackJack();
        });
        // let isUserWin = checkBJ.find(player => player.isUser())
        // let isUserOrDealerWin = checkBJ.find(player => !player.isBot() || player.isCai())
        // if (isUserOrDealerWin) return this.showResult()
        // for (let i = this.getTotalPlayer() - 1; i > 0; --i) {
        //     this.players[i].showCardCounter();
        // }
        if (!this.owner) {
            this.owner = this.findPlayerHasFirstCard();
        }
        this.setCurrentTurn(this.owner, true);
        this.sort.node.active = true;
    };
    Game.prototype.findPlayerHasFirstCard = function () {
        // for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
        //     if (Helper.findCard(this.players[i].cards, 3, 1))
        //         return this.players[i];
        // }
        var userInRound = this.players.filter(function (player) { return player.isInRound() && !player.isCai() && player; });
        return userInRound[0];
    };
    Game.prototype.showDashboard = function (lead) {
        this.allowFold(!lead);
        if (this.commonCards.length() > 0) {
            var cards = Bot_1.default.suggest(this.commonCards, this.myself.cards);
            this.allowFire(!!cards);
        }
        else {
            this.allowFire(true);
        }
        this.fold.node.active = true;
        this.fire.node.active = true;
    };
    Game.prototype.setColor = function (btn, color) {
        btn.target.color = color;
        btn.target.children.forEach(function (child) {
            child.color = color;
        });
    };
    Game.prototype.hideDashboard = function (needShow) {
        if (needShow === void 0) { needShow = false; }
        var actionUser = cc.find('Canvas/actionUser');
        actionUser.active = needShow;
        if (needShow) {
            var showInsurr = this.players[0].cards[0].rank === 1;
            // showInsurr = true
            actionUser.children[4].active = showInsurr;
            var showSplit = !this.myself.isSplit && this.myself.cards[0].rank === this.myself.cards[1].rank;
            showSplit = true;
            if (!showSplit && showInsurr) {
                actionUser.children[4].x = 190.44;
            }
            actionUser.children[3].active = showSplit;
        }
    };
    Game.prototype.allowFold = function (allow) {
        this.fold.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fold, color);
    };
    Game.prototype.allowFire = function (allow) {
        // this.fire.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fire, color);
    };
    Game.prototype.onSortCardClicked = function () {
        util_1.default.playAudio(this.audioSortCard);
        this.myself.sortHandCards();
    };
    Game.prototype.onFireClicked = function () {
        this.onHit();
        // let cards = this.myself.prepareCards;
        // if (cards.count() == 0) { return console.log('empty'); }
        // if (this.commonCards.length() > 0 && !cards.gt(this.commonCards.peek())) {
        //     return console.log('invalid');
        // }
        // if (cards.isInvalid()) {
        //     return console.log('invalid');
        // }
        // this.hideDashboard();
        // this.onFire(this.myself, cards);
        // // resort hand card
        // this.myself.reorder();
    };
    Game.prototype.onFoldClicked = function () {
        // this.hideDashboard();
        // this.onFold(this.myself);
    };
    Game.prototype.deal = function () {
        var _this = this;
        Api_1.default.preloadRewardedVideo();
        if (Api_1.default.coin <= this.betValue) {
            this.popup.open(null, 3);
            Api_1.default.showInterstitialAd();
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
            return;
        }
        this.play.node.active = false;
        this.deck.node.active = true;
        this.deck.shuffle();
        this.commonCards.reset();
        for (var i = 1; i < this.players.length; i++) {
            var bot = this.players[i];
            if (bot.getCoin() <= this.betValue * 3) {
                var profile = this.getProfileBot();
                var rate = 0.5 + Math.random() * 0.5;
                bot.setCoin(Math.floor(this.myself.getCoin() * rate));
                bot.setAvatar(profile.avatar);
                bot.setUsername(profile.username);
            }
        }
        cc.find('Canvas/actionUser/insurr').x = 402.038;
        this.seats[6].hide();
        this.players.map(function (p) {
            p.reset();
            p.updatePoint('0');
            p.setBonusType(0);
        });
        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < this.getTotalPlayer(); j++) {
                var player = this.players[j];
                var card = this.deck.pick();
                player.push(card, 0.3 + (i * this.getTotalPlayer() + j) * Game_1.DEAL_SPEED);
            }
        }
        // this.deck.hide();
        this.node.runAction(cc.sequence(cc.delayTime(Game_1.DEAL_SPEED * this.totalPlayer * 2), cc.callFunc(this.showHandCards, this)));
        this.node.runAction(cc.sequence(cc.delayTime(0.4), cc.callFunc(function () { return util_1.default.playAudio(_this.audioDealCard); })));
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.showEffect = function (player, effect) {
        effect.active = true;
        effect.setPosition(player.node.getPosition());
    };
    Game.prototype.onFold = function (player) {
        util_1.default.playAudio(this.audioFold);
        if (this.commonCards.hasCombat()) {
            var count = this.commonCards.getCombatLength();
            var victim = this.commonCards.getCombatVictim();
            var winner = this.commonCards.getCombatWinner();
            var bigPig = this.commonCards.getCombat();
            var spotWin = Math.pow(2, count - 1) * Helper_1.default.calculateSpotWin(bigPig.cards) * this.betValue;
            var coin = Math.min(spotWin, victim.coinVal);
            winner.addCoin(coin);
            victim.subCoin(coin);
        }
        if (this.commonCards.isCombatOpen()) {
            this.commonCards.resetCombat();
        }
        player.setInRound(false);
        player.setActive(false);
        var players = this.getInRoundPlayers();
        if (players.length >= 2) {
            return this.nextTurn();
        }
        this.nextRound(players[0]);
    };
    Game.prototype.onFire = function (player, cards, isDown) {
        if (isDown === void 0) { isDown = false; }
        var audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        this.node.runAction(cc.sequence(cc.delayTime(0.17), cc.callFunc(function () { return util_1.default.playAudio(audioClip); })));
        player.setActive(false);
        var cardCount = cards.count();
        var pos = this.commonCards.getPosition();
        cards.sort();
        for (var i = cards.count() - 1; i >= 0; --i) {
            if (isDown)
                cards.at(i).show();
            var card = cards.at(i);
            card.node.zIndex = this.commonCards.totalCards + i;
            var move = cc.moveTo(0.3, cc.v2(pos.x + (i - cardCount / 2) * Game_1.CARD_SPACE, pos.y));
            card.node.runAction(move);
            var scale = cc.scaleTo(0.3, 0.6);
            card.node.runAction(scale);
        }
        this.commonCards.push(cards);
        if (cards.highest.rank == 15) {
            this.commonCards.pushCombat(player, cards);
        }
        else if (this.commonCards.isCombatOpen()) {
            this.commonCards.pushCombat(player, cards);
        }
        player.removeCards(cards);
        if (player.isBot()) {
            player.updateCardCounter();
        }
        if (cards.highest.rank == 15) {
            this.notice.showBigPig(cards.count(), 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (3 * 2)) {
            this.notice.show(Notice_1.default.THREE_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (4 * 2)) {
            this.notice.show(Notice_1.default.FOUR_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.House && cards.count() == 4) {
            this.notice.show(Notice_1.default.FOUR_CARD, 3);
        }
        if (player.cards.length > 0) {
            return this.nextTurn();
        }
        this.state = Game_1.LATE;
        this.sort.node.active = false;
        this.node.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(this.showResult, this, player)));
    };
    Game.prototype.onBigWin = function (sender, winner) {
        this.state = Game_1.LATE;
        this.owner = winner;
        this.bigWin.active = true;
        var total = 0;
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player != winner) {
                if (player.isBot()) {
                    player.hideCardCounter();
                    player.showHandCards();
                    player.reorder();
                }
                var lost = 13 * this.betValue;
                lost = Math.min(player.coinVal, lost);
                player.subCoin(lost);
                total += lost;
            }
        }
        winner.addCoin(total);
        Api_1.default.updateCoin(this.myself.coinVal);
        this.onWin(total);
    };
    // showResult(sender: cc.Node, winner: Player) {
    //     let players = this.players.filter((player) => (player != winner));
    //     players.sort((a, b) => { return a.cards.length - b.cards.length; });
    //     this.showEffect(winner, this.resultEffects[0]);
    //     let total = 0;
    //     let ranking = 1;
    //     if (winner == this.myself) ranking = 1;
    //     for (let i = 0; i < players.length; i++) {
    //         let player = players[i];
    //         if (player.isBot()) {
    //             player.hideCardCounter();
    //             player.showHandCards();
    //             player.reorder();
    //         }
    //         if (player == this.myself) ranking = i + 2;
    //         this.showEffect(player, this.resultEffects[i + 1]);
    //         let lost = Helper.calculate(player.cards) * this.betValue;
    //         lost = Math.min(player.coinVal, lost);
    //         player.subCoin(lost);
    //         total += lost;
    //     }
    //     winner.addCoin(total);
    //     Api.updateCoin(this.myself.coinVal);
    //     this.owner = winner;
    //     if (winner == this.myself) {
    //         this.onWin(total);
    //     } else {
    //         this.onLose(ranking);
    //     }
    // }
    Game.prototype.onLose = function (ranking) {
        Api_1.default.logEvent(EventKeys_1.default.LOSE + '_' + ranking);
        util_1.default.playAudio(this.audioLose);
        var rnd = Math.random();
        if (ranking == 2 && rnd <= 0.3) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 3 && rnd <= 0.2) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 4 && rnd <= 0.1) {
            Api_1.default.showInterstitialAd();
        }
        this.delayReset(3);
    };
    Game.prototype.onWin = function (won) {
        Api_1.default.logEvent(EventKeys_1.default.WIN);
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_OPEN);
        util_1.default.playAudio(this.audioWin);
        var rnd = Math.random();
        if (rnd <= 0.3 && Api_1.default.isInterstitialAdLoaded()) {
            Api_1.default.showInterstitialAd();
            this.delayReset(3);
        }
        else {
            this.spin.show(won);
        }
    };
    Game.prototype.onSpinHide = function () {
        this.delayReset(3);
    };
    Game.prototype.onSpinCompleted = function (result, won) {
        var bonus = this.spin.compute(result.id, won);
        if (bonus > 0) {
            this.myself.addCoin(bonus);
            Api_1.default.updateCoin(this.myself.coinVal);
        }
    };
    Game.prototype.delayReset = function (dt) {
        this.node.runAction(cc.sequence(cc.delayTime(dt), cc.callFunc(this.reset, this)));
    };
    Game.prototype.reset = function () {
        for (var i = this.resultEffects.length - 1; i >= 0; --i) {
            this.resultEffects[i].active = false;
        }
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].reset();
        }
        if (this.bigWin.active) {
            this.bigWin.active = false;
        }
        // this.deck.node.active = false;
        this.play.node.active = true;
    };
    Game.prototype.onCardSelected = function (card, selected) {
    };
    Game.prototype.nextTurn = function () {
        var next = this.getNextInRoundPlayer();
        this.setCurrentTurn(next, false);
    };
    Game.prototype.nextRound = function (lead) {
        this.commonCards.nextRound();
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].setInRound(true);
        }
        this.setCurrentTurn(lead, true);
        // util.playAudio(this.audioGarbageCard);
    };
    Game.prototype.onHitClicked = function () {
        var player = this.myself;
        this.takeCard(player);
        player.setActive(true);
        this.hideDashboard();
    };
    Game.prototype.onDoubleClicked = function () {
    };
    Game.prototype.onSplitClicked = function () {
    };
    Game.prototype.onStandClicked = function () {
        var player = this.myself;
        player.setActive(false);
        if (!player.inRound && player.subUser) {
            player = player.subUser;
        }
        player.setInRound(false);
        this.hideDashboard();
        this.nextTurn();
    };
    Game.prototype.setCurrentTurn = function (player, lead) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var canHit;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.players.map(function (p) { return p.setActive(false); });
                        if (!player) {
                            return [2 /*return*/, this.showResult()];
                        }
                        this.turn = player.seat;
                        player.setActive(true);
                        if (player.isUser()) {
                            return [2 /*return*/, Game_1.sleep(Game_1.WAIT_RE_SHOW_USER_ACTION).then(function () {
                                    _this.hideDashboard(true);
                                })];
                        }
                        canHit = player.point > this.players[0].point;
                        if (player.point < 18) {
                            canHit = true;
                        }
                        if (!canHit) {
                            canHit = this.players.find(function (p) { return player.point > p.point; }) != undefined;
                        }
                        if (player.point >= 18)
                            canHit = false;
                        if (((_a = player.cards) === null || _a === void 0 ? void 0 : _a.length) === 5)
                            canHit = false;
                        return [4 /*yield*/, Game_1.sleep(Game_1.WAIT_BOT)];
                    case 1:
                        _b.sent();
                        player.setInRound(false);
                        // setTimeout(() => {
                        if (canHit) {
                            this.takeCard(player);
                        }
                        else {
                            this.toast.show(player.username.string + ' stand' + player.point + " điểm");
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.getInRoundPlayers = function () {
        var players = [];
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            if (this.players[i].isInRound())
                players.push(this.players[i]);
        }
        return players;
    };
    Game.prototype.getNextInRoundPlayer = function () {
        var _this = this;
        // tim thang nao co ghe ngoi > turn trc va dang o trong van choi
        var nextTurn = this.players.find(function (player) {
            var _a;
            return (player.seat === _this.turn && ((_a = player === null || player === void 0 ? void 0 : player.subUser) === null || _a === void 0 ? void 0 : _a.inRound)) || (player.seat > _this.turn && player.isInRound());
        });
        if (!nextTurn && this.players[0].inRound)
            return this.players[0]; // neu khong con ai thi chuyen luot cho nha cai
        // let nextTurn = this.players[this.turn + 1]
        // if (!nextTurn && this.players[0].isInRound()) nextTurn = this.players[0]
        // if (!nextTurn.isInRound()) return
        return nextTurn;
        // for (let i = 1; i < this.getTotalPlayer(); i++) {
        //     let offset = this.turn + i;
        //     if (offset >= this.getTotalPlayer()) offset -= this.getTotalPlayer();
        //     if (this.players[offset].isInRound())
        //         return this.players[offset];
        // }
        // return null;
    };
    Game.prototype.takeCard = function (player, needNext) {
        if (needNext === void 0) { needNext = true; }
        return __awaiter(this, void 0, void 0, function () {
            var card, _player;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = this.deck.pick();
                        this.toast.show(player.username.string + ': ' + player.point + "đ bốc thêm" + card.rank);
                        if (!(card === null || card === void 0 ? void 0 : card.node)) {
                            this.state = Game_1.LATE;
                            this.sort.node.active = false;
                            // this.node.runAction(cc.sequence(
                            //     cc.delayTime(2),
                            //     cc.callFunc(this.showResult, this, player)
                            // ));
                            return [2 /*return*/];
                        }
                        player.push(card, 0);
                        if (!needNext)
                            return [2 /*return*/];
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        return [4 /*yield*/, Game_1.sleep(0)];
                    case 1:
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        _a.sent();
                        if (player.isBot()) {
                            // player.updateCardCounter();
                        }
                        else {
                            player.cards.map(function (el) { return el.show(); });
                        }
                        player.setActive(false);
                        _player = player;
                        if (!player.inRound && player.subUser) {
                            _player = player.subUser;
                        }
                        if (_player.point >= 21 || _player.cards.length === 5) {
                            _player.setInRound(false);
                            _player.point > 21 && this.toast.show(_player.username.string + (" Thua: " + _player.point + " \u0111i\u1EC3m"));
                            // bot stand
                            _player.setActive(false);
                            return [2 /*return*/, this.nextTurn()];
                        }
                        // console.log('====================================');
                        // console.log(player.cards.map(el => el.node.name));
                        // console.log('====================================');
                        // this.checkWin()
                        // this.nextTurn()
                        if (!player.bonusType) {
                            this.setCurrentTurn(player, false);
                        }
                        else {
                            player.setInRound(false);
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.execBot = function (sender, bot) {
        // alert(1)
        var cards = this.commonCards.isEmpty()
            ? Bot_1.default.random(bot.cards)
            : Bot_1.default.suggest(this.commonCards, bot.cards);
        if (!cards) {
            return this.onFold(bot);
        }
        this.onFire(this.players[this.turn], cards, true);
    };
    Game.prototype.getTotalPlayer = function () {
        return this.players.length;
    };
    Game.prototype.getProfileBot = function () {
        for (var i = 0; i < 10; i++) {
            var rnd = Math.floor(Math.random() * this.profileBots.length);
            if (!this.isUsage(this.profileBots[rnd])) {
                return this.profileBots[rnd];
            }
        }
        return this.profileBots[0];
    };
    Game.prototype.isUsage = function (user) {
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].username.string == user.username) {
                return true;
            }
        }
        return false;
    };
    Game.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
    };
    Game.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.show(msg);
        Api_1.default.coinIncrement(bonus);
        this.myself.setCoin(Api_1.default.coin);
        this.popup.close(null, 3);
        // update bet room
        this.betValue = Math.round(Api_1.default.coin * 0.3);
        this.betText.string = util_1.default.numberFormat(this.betValue);
    };
    Game.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.toast.show('Mời bạn chơi không thành công');
        });
    };
    Game.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    Game.prototype.onSetBonusClicked = function (e, bonus) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('====================================');
                        console.log(bonus);
                        console.log('====================================');
                        if (!(bonus == 3)) return [3 /*break*/, 4];
                        this.myself.split(this.seats[6]);
                        this.seats[6].show();
                        this.hideDashboard();
                        return [4 /*yield*/, Game_1.sleep(Game_1.DEAL_SPEED)
                            // this.setCurrentTurn(this.myself, false)
                            // this.onHitClicked()
                        ];
                    case 1:
                        _a.sent();
                        // this.setCurrentTurn(this.myself, false)
                        // this.onHitClicked()
                        this.takeCard(this.myself, false);
                        return [4 /*yield*/, Game_1.sleep(500)];
                    case 2:
                        _a.sent();
                        this.takeCard(this.myself.subUser, false);
                        return [4 /*yield*/, Game_1.sleep(1000)];
                    case 3:
                        _a.sent();
                        this.nextTurn();
                        return [3 /*break*/, 5];
                    case 4:
                        this.myself.setBonusType(bonus);
                        if (bonus == 1) { // double
                            this.onHitClicked();
                            if (this.myself.inRound) {
                                this.myself.inRound = false;
                            }
                            else {
                                this.myself.subUser.inRound = false;
                            }
                        }
                        else {
                            this.onStandClicked();
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    // onSetBonus = (bonus) => {
    //     console.log('====================================');
    //     console.log(bonus);
    //     console.log('====================================');
    //     // this.myself.setBonusType()
    // }
    // cheat
    Game.prototype.cheat_win = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.myself);
    };
    Game.prototype.cheat_lose = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.players[1]);
    };
    var Game_1;
    Game.sleep = function (wait) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve(1);
                    }, wait);
                })];
        });
    }); };
    Game.WAIT = 0; // wait for player
    Game.DEAL = 1; // deal card
    Game.PLAY = 2; // play
    Game.LATE = 3; // late
    // static readonly DEAL_SPEED: number = 0//1.66;
    // static readonly CARD_SPACE: number = 45;
    // static readonly WAIT_BOT: number = 0;
    // static readonly WAIT_RE_SHOW_USER_ACTION: number = 0;
    Game.DEAL_SPEED = 1.2; //1.66;
    Game.CARD_SPACE = 45;
    Game.WAIT_BOT = 2000;
    Game.WAIT_RE_SHOW_USER_ACTION = 1000;
    __decorate([
        property(Deck_1.default)
    ], Game.prototype, "deck", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "play", void 0);
    __decorate([
        property(Player_1.default)
    ], Game.prototype, "seats", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fire", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fold", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "sort", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "resultEffects", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "bigWin", void 0);
    __decorate([
        property(Toast_1.default)
    ], Game.prototype, "toast", void 0);
    __decorate([
        property(Notice_1.default)
    ], Game.prototype, "notice", void 0);
    __decorate([
        property(cc.Label)
    ], Game.prototype, "betText", void 0);
    __decorate([
        property(SpinWheel_1.default)
    ], Game.prototype, "spin", void 0);
    __decorate([
        property(Popup_1.default)
    ], Game.prototype, "popup", void 0);
    __decorate([
        property(Profile)
    ], Game.prototype, "profileBots", void 0);
    __decorate([
        property(cc.Toggle)
    ], Game.prototype, "soundToggle", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFold", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioShowCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioGarbageCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFire", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioSortCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioQuitGame", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioDealCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFireSingle", void 0);
    __decorate([
        property(Modal_1.default)
    ], Game.prototype, "modal", void 0);
    Game = Game_1 = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0dhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBQzlCLCtCQUEwQjtBQUUxQixtQ0FBOEI7QUFDOUIseUNBQW9DO0FBQ3BDLDJDQUFzQztBQUN0QyxpQ0FBNEI7QUFDNUIsNkJBQXdCO0FBQ3hCLG1DQUE4QjtBQUM5QixtQ0FBOEI7QUFDOUIsK0JBQTBCO0FBQzFCLHlDQUFvQztBQUNwQyxpQ0FBNEI7QUFDNUIsNkJBQXdCO0FBQ3hCLHVDQUFrQztBQUNsQyx5Q0FBb0M7QUFDcEMsdUNBQWtDO0FBRWxDLElBQUksVUFBVSxHQUFHO0lBQ2IsQ0FBQyxFQUFFLElBQUk7SUFDUCxDQUFDLEVBQUUsSUFBSTtJQUNQLENBQUMsRUFBRSxPQUFPO0lBQ1YsQ0FBQyxFQUFFLE1BQU07Q0FDWixDQUFBO0FBRUQsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFFN0YsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBQTtRQUVJLFdBQU0sR0FBbUIsSUFBSSxDQUFDO1FBRTlCLGFBQVEsR0FBVyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUhHO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7MkNBQ0s7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2Q0FDRTtJQUpwQixPQUFPO1FBRFosT0FBTyxDQUFDLFNBQVMsQ0FBQztPQUNiLE9BQU8sQ0FLWjtJQUFELGNBQUM7Q0FMRCxBQUtDLElBQUE7QUFHRDtJQUFrQyx3QkFBWTtJQUE5QztRQUFBLHFFQTRwQ0M7UUExcENHLFVBQUksR0FBUyxJQUFJLENBQUM7UUFFbEIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQWEsRUFBRSxDQUFDO1FBRXJCLFVBQUksR0FBYyxJQUFJLENBQUM7UUFFdkIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQWMsSUFBSSxDQUFDO1FBRXZCLG1CQUFhLEdBQWMsRUFBRSxDQUFDO1FBRTlCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFFdkIsV0FBSyxHQUFVLElBQUksQ0FBQztRQUVwQixZQUFNLEdBQVcsSUFBSSxDQUFDO1FBRXRCLGFBQU8sR0FBYSxJQUFJLENBQUM7UUFFekIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGlCQUFXLEdBQWMsRUFBRSxDQUFDO1FBRzVCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxzQkFBZ0IsR0FBaUIsSUFBSSxDQUFDO1FBRXRDLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGNBQVEsR0FBaUIsSUFBSSxDQUFDO1FBRTlCLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxtQkFBYSxHQUFpQixJQUFJLENBQUM7UUFFbkMsbUJBQWEsR0FBaUIsSUFBSSxDQUFDO1FBRW5DLHFCQUFlLEdBQWlCLElBQUksQ0FBQztRQUVyQyxXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGFBQU8sR0FBYSxFQUFFLENBQUM7UUFDdkIsaUJBQVcsR0FBZSxJQUFJLG9CQUFVLEVBQUUsQ0FBQztRQUMzQyxXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFDakIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixXQUFLLEdBQVcsSUFBSSxDQUFDO1FBQ3JCLFlBQU0sR0FBVyxJQUFJLENBQUM7UUFDdEIsY0FBUSxHQUFXLElBQUksQ0FBQztRQUN4QixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUN4QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFzSnJCLGFBQU8sR0FBRyxVQUFDLElBQVk7WUFDbkIsSUFBSSxPQUFPLEdBQUcsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25DLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQTtRQTRFRCx1QkFBaUIsR0FBRztZQUNoQixLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU07Z0JBQ25CLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQTtZQUN4QixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQTtRQThjRCxnQkFBVSxHQUFHO1lBQ1Qsb0RBQW9EO1lBQ3BELHdDQUF3QztZQUV4Qyw2RUFBNkU7WUFFN0UsMEJBQTBCO1lBQzFCLHFDQUFxQztZQUNyQywrRUFBK0U7WUFDL0UsUUFBUTtZQUNSLEtBQUs7WUFHTCxJQUFJLE1BQU0sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRTVCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQTtZQUVyQixLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFDLE1BQU0sRUFBRSxLQUFLO2dCQUMzQixJQUFJLENBQUMsS0FBSztvQkFBRSxPQUFNO2dCQUVsQixJQUFJLFVBQVUsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3QyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ2hCLFVBQVUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtpQkFDckQ7Z0JBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFFN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Z0JBRXBELGFBQWEsSUFBSSxVQUFVLENBQUE7WUFDL0IsQ0FBQyxDQUFDLENBQUE7WUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBRWhDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsQ0FBQztZQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFFcEQsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7WUFDeEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUluQixLQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7WUFDdkIsdUJBQXVCO1lBQ3ZCLDZCQUE2QjtZQUM3QixpQkFBaUI7WUFDakIsa0RBQWtEO1lBQ2xELG9DQUFvQztZQUNwQyw4QkFBOEI7WUFDOUIsZ0NBQWdDO1lBQ2hDLHdDQUF3QztZQUN4QyxzQ0FBc0M7WUFDdEMsZ0NBQWdDO1lBQ2hDLFlBQVk7WUFDWix5Q0FBeUM7WUFDekMsaURBQWlEO1lBQ2pELGdDQUFnQztZQUNoQyx5QkFBeUI7WUFDekIsUUFBUTtZQUNSLElBQUk7WUFDSix5QkFBeUI7WUFDekIsYUFBRyxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLHFCQUFxQjtZQUVyQiwwRUFBMEU7UUFDOUUsQ0FBQyxDQUFBO1FBR0QscUJBQWUsR0FBRyxVQUFDLE1BQWM7WUFDN0IsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUM1QixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUE7WUFDbEIsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtnQkFDdkIsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFBO2FBQ2xDO2lCQUFNLElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEVBQUU7Z0JBQzFCLFVBQVUsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFBO2dCQUMzQixnQ0FBZ0M7YUFDbkM7aUJBQU0sSUFBSSxNQUFNLENBQUMsY0FBYyxFQUFFLEVBQUU7Z0JBQ2hDLFVBQVUsSUFBSSxLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQTthQUNwQztpQkFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDcEMsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7aUJBQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ3RDLE9BQU8sQ0FBQyxDQUFBO2FBQ1g7aUJBQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsRUFBRTtnQkFDMUIsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7aUJBQ0k7Z0JBQ0QsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7WUFFRCxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFO2dCQUN2QyxVQUFVLElBQUksQ0FBQyxDQUFBO2FBQ2xCO1lBQ0QsT0FBTyxVQUFVLENBQUE7UUFDckIsQ0FBQyxDQUFBO1FBb0tELGNBQVEsR0FBRztZQUNQLHVEQUF1RDtZQUN2RCw2QkFBNkI7WUFDN0IsdURBQXVEO1lBQ3ZELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUN0QixnQkFBZ0I7WUFDcEIsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUE7O0lBcUpMLENBQUM7YUE1cENvQixJQUFJO0lBb0VyQix3QkFBd0I7SUFFeEIscUJBQU0sR0FBTjtRQUFBLGlCQThEQztRQTdERyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRSxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFFcEMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBQyxHQUFHLEVBQUUsS0FBSztZQUNsRCxJQUFJLGlCQUFpQixHQUFHLElBQUksT0FBTyxDQUFDLFVBQUEsT0FBTztnQkFDdkMsSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEVBQUUsRUFBRSxJQUFJO29CQUNoQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQUssRUFBRSxPQUFPO3dCQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSzs0QkFBRSxPQUFNO3dCQUMzQixJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFBO3dCQUNoQixJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFBO3dCQUdsQyxRQUFRLENBQUMsRUFBRTs0QkFDUCxLQUFLLENBQUM7Z0NBQ0YsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtnQ0FDUCxNQUFNOzRCQUNWLEtBQUssRUFBRTtnQ0FDSCxDQUFDLEdBQUcsR0FBRyxDQUFBO2dDQUNQLE1BQU07NEJBQ1YsS0FBSyxFQUFFO2dDQUNILENBQUMsR0FBRyxHQUFHLENBQUE7Z0NBQ1AsTUFBTTs0QkFDVixLQUFLLEVBQUU7Z0NBQ0gsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtnQ0FDUCxNQUFNO3lCQUNiO3dCQUVELElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTt3QkFDdEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFBO3dCQUNyQixJQUFJLE9BQU8sR0FBRyxDQUFDOzRCQUFFLGFBQWEsSUFBSSxJQUFJLENBQUE7d0JBRXRDLHVEQUF1RDt3QkFDdkQsK0NBQStDO3dCQUMvQyx1REFBdUQ7d0JBQ3ZELE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQTt3QkFDeEIsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDdkQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUM5RixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQTt3QkFDckUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUE7d0JBQ3JFLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFBO3dCQUM1RCxPQUFPLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQTt3QkFDdEIsdURBQXVEO3dCQUN2RCx3QkFBd0I7d0JBQ3hCLHVEQUF1RDt3QkFDdkQsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzVCLElBQUksSUFBSSxLQUFLLEVBQUUsSUFBSSxLQUFLLEtBQUssR0FBRyxFQUFFOzRCQUM5QixVQUFVLENBQUM7Z0NBQ1AsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBOzRCQUNkLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzt5QkFDWDtvQkFDTCxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDLENBQUMsQ0FBQTtZQUNOLENBQUMsQ0FBQyxDQUFBO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFO2dCQUNwQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7WUFDbkIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Z0JBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUN4RCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFlBQVk7SUFFWix1QkFBUSxHQUFSO1FBQUEsaUJBOEVDO1FBN0VHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRW5CLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTVELElBQUksZ0JBQU0sQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztTQUN4QjtRQUVELElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQTtRQUVmLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDO1lBQUUsR0FBRyxHQUFHLFNBQVMsQ0FBQzthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDO1lBQUUsR0FBRyxHQUFHLGFBQWEsQ0FBQTtRQUNqRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDaEMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtpQkFDakI7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ2Y7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUVGLCtDQUErQztRQUMvQyxnQ0FBZ0M7UUFDaEMscUJBQXFCO1FBQ3JCLGtDQUFrQztRQUNsQyw4Q0FBOEM7UUFDOUMsdUJBQXVCO1FBQ3ZCLDBDQUEwQztRQUMxQyw4Q0FBOEM7UUFDOUMsbUNBQW1DO1FBQ25DLGVBQWU7UUFDZix1QkFBdUI7UUFDdkIsUUFBUTtRQUNSLElBQUk7UUFJSixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxhQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRSxJQUFJLGdCQUFNLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxnQkFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7U0FDL0Q7UUFFRCxJQUFJLGdCQUFNLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixHQUFHLENBQUMsT0FBTyxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDdkQsR0FBRyxDQUFDLFdBQVcsQ0FBQyxnQkFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMzQzthQUFNO1lBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO2dCQUNuQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELENBQUMsQ0FBQyxDQUFBO1NBQ0w7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUMzQixFQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUNqQixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQy9CLENBQUMsQ0FBQztRQUVILElBQUksZ0JBQU0sQ0FBQyxXQUFXLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUM5QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUM1QjtRQUVELGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFVRCx3QkFBUyxHQUFUO1FBQ0ksc0JBQXNCO1FBQ3RCLGdEQUFnRDtRQUNoRCxzRUFBc0U7SUFDMUUsQ0FBQztJQUVELGVBQWU7SUFDZiw0REFBNEQ7SUFDNUQscURBQXFEO0lBQ3JELG1FQUFtRTtJQUNuRSxxREFBcUQ7SUFDckQsbUVBQW1FO0lBQ25FLHFEQUFxRDtJQUNyRCxxRUFBcUU7SUFDckUsc0RBQXNEO0lBQ3RELHFFQUFxRTtJQUNyRSxzREFBc0Q7SUFDdEQscUVBQXFFO0lBQ3JFLHNEQUFzRDtJQUN0RCxRQUFRO0lBQ1IsMkJBQTJCO0lBQzNCLElBQUk7SUFFSiwyQkFBWSxHQUFaLFVBQWEsR0FBYTtRQUN0QixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksTUFBSSxDQUFDLElBQUk7WUFBRSxPQUFPO1FBRXBDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBRXBELElBQUksQ0FBQyxRQUFRO1lBQUUsT0FBTztRQUV0QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztRQUU1QyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDeEM7YUFBTTtZQUNILElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQy9CLE9BQU87U0FDVjtRQUVELElBQUksWUFBWSxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLFNBQVMsRUFBRTtZQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RCLE9BQU87U0FDVjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixPQUFPO1NBQ1Y7UUFFRCxJQUFJLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFO1lBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQsOEJBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUNyQix3QkFBd0I7UUFDeEIsd0NBQXdDO1FBQ3hDLHlEQUF5RDtRQUN6RCwwRUFBMEU7UUFDMUUsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCx1Q0FBdUM7UUFDdkMsSUFBSTtJQUNSLENBQUM7SUFRRCw0QkFBYSxHQUFiO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksTUFBSSxDQUFDLElBQUksRUFBRTtZQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pDO2FBQU07WUFDSCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLE1BQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3BELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUN0RCxhQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hCO1FBQ0QsY0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbkMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELDRCQUFhLEdBQWI7UUFDSSxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuQywrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBRXZCLHVEQUF1RDtRQUN2RCxrQ0FBa0M7UUFDbEMsdURBQXVEO1FBSXZELDRDQUE0QztRQUU1Qyx1Q0FBdUM7UUFDdkMsNkJBQTZCO1FBQzdCLHdEQUF3RDtRQUN4RCxVQUFVO1FBQ1YsY0FBYztRQUNkLElBQUk7UUFFSixtQ0FBbUM7UUFDbkMseUJBQXlCO1FBQ3pCLDBEQUEwRDtRQUMxRCxNQUFNO1FBRU4sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQyxNQUFNLEVBQUUsS0FBSztZQUMzQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQUUsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFBO2lCQUFNO2dCQUM5QyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUE7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUdGLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTTtZQUNwQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUE7UUFDM0IsQ0FBQyxDQUFDLENBQUE7UUFFRiwwREFBMEQ7UUFDMUQsb0ZBQW9GO1FBQ3BGLGtEQUFrRDtRQUNsRCx3REFBd0Q7UUFDeEQseUNBQXlDO1FBQ3pDLElBQUk7UUFFSixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBRUQscUNBQXNCLEdBQXRCO1FBQ0kseURBQXlEO1FBQ3pELHdEQUF3RDtRQUN4RCxrQ0FBa0M7UUFDbEMsSUFBSTtRQUNKLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLE1BQU0sRUFBL0MsQ0FBK0MsQ0FBQyxDQUFBO1FBQ2hHLE9BQU8sV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCw0QkFBYSxHQUFiLFVBQWMsSUFBYTtRQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsRUFBRTtZQUMvQixJQUFJLEtBQUssR0FBRyxhQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBRUQsdUJBQVEsR0FBUixVQUFTLEdBQWMsRUFBRSxLQUFlO1FBQ3BDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQzdCLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRCQUFhLEdBQWIsVUFBYyxRQUFnQjtRQUFoQix5QkFBQSxFQUFBLGdCQUFnQjtRQUMxQixJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFFN0MsVUFBVSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFDNUIsSUFBSSxRQUFRLEVBQUU7WUFDVixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFBO1lBQ3BELG9CQUFvQjtZQUNwQixVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUE7WUFFMUMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO1lBRS9GLFNBQVMsR0FBRyxJQUFJLENBQUE7WUFDaEIsSUFBSSxDQUFDLFNBQVMsSUFBSSxVQUFVLEVBQUU7Z0JBQzFCLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTthQUNwQztZQUNELFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQTtTQUM1QztJQUNMLENBQUM7SUFFRCx3QkFBUyxHQUFULFVBQVUsS0FBYztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCx3QkFBUyxHQUFULFVBQVUsS0FBYztRQUNwQixrQ0FBa0M7UUFDbEMsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1FBQ1osd0NBQXdDO1FBQ3hDLDJEQUEyRDtRQUUzRCw2RUFBNkU7UUFDN0UscUNBQXFDO1FBQ3JDLElBQUk7UUFFSiwyQkFBMkI7UUFDM0IscUNBQXFDO1FBQ3JDLElBQUk7UUFFSix3QkFBd0I7UUFDeEIsbUNBQW1DO1FBQ25DLHNCQUFzQjtRQUN0Qix5QkFBeUI7SUFDN0IsQ0FBQztJQUdELDRCQUFhLEdBQWI7UUFDSSx3QkFBd0I7UUFDeEIsNEJBQTRCO0lBQ2hDLENBQUM7SUFFRCxtQkFBSSxHQUFKO1FBQUEsaUJBbURDO1FBbERHLGFBQUcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzNCLElBQUksYUFBRyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN6QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUN6QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUNqRCxPQUFNO1NBQ1Q7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ25DLElBQUksSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO2dCQUNyQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDOUIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDckM7U0FDSjtRQUVELEVBQUUsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFBO1FBRS9DLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7UUFFcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDO1lBQ2QsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ1YsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3JCLENBQUMsQ0FBQyxDQUFBO1FBRUYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM5RTtTQUNKO1FBQ0Qsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxFQUNwRCxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FDekMsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQ2pCLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBTSxPQUFBLGNBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFsQyxDQUFrQyxDQUFDLENBQ3hELENBQUMsQ0FBQztRQUVILGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCx5QkFBVSxHQUFWLFVBQVcsTUFBYyxFQUFFLE1BQWU7UUFDdEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELHFCQUFNLEdBQU4sVUFBTyxNQUFjO1FBQ2pCLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUM5QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQy9DLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDaEQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNoRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzFDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUMsR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzdGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3QyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7UUFDRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLEVBQUU7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNsQztRQUVELE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN2QyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQscUJBQU0sR0FBTixVQUFPLE1BQWMsRUFBRSxLQUFnQixFQUFFLE1BQXVCO1FBQXZCLHVCQUFBLEVBQUEsY0FBdUI7UUFDNUQsSUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDeEcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFDbEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxjQUFNLE9BQUEsY0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUMvQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM5QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXpDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUViLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3pDLElBQUksTUFBTTtnQkFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBRS9CLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQ25ELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsTUFBSSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QzthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDOUM7UUFDRCxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzlCO1FBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzVDO2FBQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLG1CQUFTLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUN0RSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMxQzthQUFNLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDdEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDekM7YUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUM1RCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN6QztRQUNELElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzFCO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFDZixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUM3QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUJBQVEsR0FBUixVQUFTLE1BQWUsRUFBRSxNQUFjO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO2dCQUNsQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDO29CQUN6QixNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDcEI7Z0JBQ0QsSUFBSSxJQUFJLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzlCLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JCLEtBQUssSUFBSSxJQUFJLENBQUM7YUFDakI7U0FDSjtRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEIsYUFBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELGdEQUFnRDtJQUNoRCx5RUFBeUU7SUFDekUsMkVBQTJFO0lBRTNFLHNEQUFzRDtJQUN0RCxxQkFBcUI7SUFDckIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QyxpREFBaUQ7SUFDakQsbUNBQW1DO0lBQ25DLGdDQUFnQztJQUNoQyx3Q0FBd0M7SUFDeEMsc0NBQXNDO0lBQ3RDLGdDQUFnQztJQUNoQyxZQUFZO0lBQ1osc0RBQXNEO0lBQ3RELDhEQUE4RDtJQUM5RCxxRUFBcUU7SUFDckUsaURBQWlEO0lBQ2pELGdDQUFnQztJQUNoQyx5QkFBeUI7SUFDekIsUUFBUTtJQUNSLDZCQUE2QjtJQUM3QiwyQ0FBMkM7SUFDM0MsMkJBQTJCO0lBQzNCLG1DQUFtQztJQUNuQyw2QkFBNkI7SUFDN0IsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxRQUFRO0lBQ1IsSUFBSTtJQUVKLHFCQUFNLEdBQU4sVUFBTyxPQUFlO1FBQ2xCLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBQzdDLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMxQixJQUFJLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsRUFBRTtZQUM1QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM1QjthQUFNLElBQUksT0FBTyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ25DLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzVCO2FBQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7WUFDbkMsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxvQkFBSyxHQUFMLFVBQU0sR0FBVztRQUNiLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNqRCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QixJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUIsSUFBSSxHQUFHLElBQUksR0FBRyxJQUFJLGFBQUcsQ0FBQyxzQkFBc0IsRUFBRSxFQUFFO1lBQzVDLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEI7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQztJQUVELHlCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCw4QkFBZSxHQUFmLFVBQWdCLE1BQU0sRUFBRSxHQUFXO1FBQy9CLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0IsYUFBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVELHlCQUFVLEdBQVYsVUFBVyxFQUFVO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2hCLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FDaEMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFLLEdBQUw7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUN4QztRQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDM0I7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUM5QjtRQUNELGlDQUFpQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFRCw2QkFBYyxHQUFkLFVBQWUsSUFBVSxFQUFFLFFBQWlCO0lBRTVDLENBQUM7SUFFRCx1QkFBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELHdCQUFTLEdBQVQsVUFBVSxJQUFZO1FBQ2xCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDN0IsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNoQyx5Q0FBeUM7SUFDN0MsQ0FBQztJQUVELDJCQUFZLEdBQVo7UUFDSSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDckIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7SUFDeEIsQ0FBQztJQUVELDhCQUFlLEdBQWY7SUFFQSxDQUFDO0lBRUQsNkJBQWMsR0FBZDtJQUVBLENBQUM7SUFFRCw2QkFBYyxHQUFkO1FBQ0ksSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUN4QixNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXhCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDbkMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7U0FDMUI7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUNwQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQW9HSyw2QkFBYyxHQUFwQixVQUFxQixNQUFjLEVBQUUsSUFBYTs7Ozs7Ozs7d0JBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFBO3dCQUN6QyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNULHNCQUFPLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQTt5QkFDM0I7d0JBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUN4QixNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUV2QixJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRTs0QkFDakIsc0JBQU8sTUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxJQUFJLENBQUM7b0NBQ2xELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUE7Z0NBQzVCLENBQUMsQ0FBQyxFQUFBO3lCQUNMO3dCQUVHLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO3dCQUVqRCxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxFQUFFOzRCQUNuQixNQUFNLEdBQUcsSUFBSSxDQUFBO3lCQUNoQjt3QkFFRCxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNULE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBdEIsQ0FBc0IsQ0FBQyxJQUFJLFNBQVMsQ0FBQTt5QkFDdkU7d0JBRUQsSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7NEJBQUUsTUFBTSxHQUFHLEtBQUssQ0FBQTt3QkFFdEMsSUFBSSxPQUFBLE1BQU0sQ0FBQyxLQUFLLDBDQUFFLE1BQU0sTUFBSyxDQUFDOzRCQUFFLE1BQU0sR0FBRyxLQUFLLENBQUE7d0JBRTlDLHFCQUFNLE1BQUksQ0FBQyxLQUFLLENBQUMsTUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQzt3QkFDaEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTt3QkFDeEIscUJBQXFCO3dCQUNyQixJQUFJLE1BQU0sRUFBRTs0QkFDUixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3lCQUN4Qjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsQ0FBQTs0QkFDM0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO3lCQUNsQjs7Ozs7S0F3Qko7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxJQUFJLE9BQU8sR0FBa0IsRUFBRSxDQUFDO1FBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELG1DQUFvQixHQUFwQjtRQUFBLGlCQW1CQztRQWxCRyxnRUFBZ0U7UUFDaEUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNOztZQUNuQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsSUFBSSxXQUFJLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxPQUFPLDBDQUFFLE9BQU8sQ0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUE7UUFDckgsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUFFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLCtDQUErQztRQUVoSCw2Q0FBNkM7UUFDN0MsMkVBQTJFO1FBQzNFLG9DQUFvQztRQUNwQyxPQUFPLFFBQVEsQ0FBQTtRQUNmLG9EQUFvRDtRQUNwRCxrQ0FBa0M7UUFDbEMsNEVBQTRFO1FBQzVFLDRDQUE0QztRQUM1Qyx1Q0FBdUM7UUFDdkMsSUFBSTtRQUNKLGVBQWU7SUFDbkIsQ0FBQztJQU1LLHVCQUFRLEdBQWQsVUFBZSxNQUFjLEVBQUUsUUFBZTtRQUFmLHlCQUFBLEVBQUEsZUFBZTs7Ozs7O3dCQUN0QyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFFNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTt3QkFDeEYsSUFBSSxFQUFDLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxJQUFJLENBQUEsRUFBRTs0QkFDYixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7NEJBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7NEJBQzlCLG1DQUFtQzs0QkFDbkMsdUJBQXVCOzRCQUN2QixpREFBaUQ7NEJBQ2pELE1BQU07NEJBQ04sc0JBQU07eUJBQ1Q7d0JBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBRXJCLElBQUksQ0FBQyxRQUFROzRCQUFFLHNCQUFNO3dCQUNyQiwyR0FBMkc7d0JBQzNHLG1DQUFtQzt3QkFDbkMsMEJBQTBCO3dCQUMxQixtREFBbUQ7d0JBQ25ELE1BQU07d0JBRU4scUJBQU0sTUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQTs7d0JBTm5CLDJHQUEyRzt3QkFDM0csbUNBQW1DO3dCQUNuQywwQkFBMEI7d0JBQzFCLG1EQUFtRDt3QkFDbkQsTUFBTTt3QkFFTixTQUFtQixDQUFBO3dCQUVuQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBRTs0QkFDaEIsOEJBQThCO3lCQUNqQzs2QkFBTTs0QkFDSCxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEVBQUUsSUFBSSxPQUFBLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBVCxDQUFTLENBQUMsQ0FBQTt5QkFDcEM7d0JBQ0QsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFFcEIsT0FBTyxHQUFHLE1BQU0sQ0FBQTt3QkFFcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTs0QkFDbkMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7eUJBQzNCO3dCQUVELElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxFQUFFLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOzRCQUNuRCxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBOzRCQUN6QixPQUFPLENBQUMsS0FBSyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBRyxZQUFVLE9BQU8sQ0FBQyxLQUFLLG9CQUFPLENBQUEsQ0FBQyxDQUFBOzRCQUMvRixZQUFZOzRCQUNaLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRXpCLHNCQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQTt5QkFDekI7d0JBQ0QsdURBQXVEO3dCQUN2RCxxREFBcUQ7d0JBQ3JELHVEQUF1RDt3QkFDdkQsa0JBQWtCO3dCQUNsQixrQkFBa0I7d0JBRWxCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFOzRCQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQTt5QkFDckM7NkJBQU07NEJBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTs0QkFDeEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO3lCQUNsQjs7Ozs7S0FRSjtJQVdELHNCQUFPLEdBQVAsVUFBUSxNQUFlLEVBQUUsR0FBVztRQUNoQyxXQUFXO1FBQ1gsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDbEMsQ0FBQyxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUN2QixDQUFDLENBQUMsYUFBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUvQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO1FBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELDZCQUFjLEdBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO0lBQy9CLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQy9CO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDOUIsQ0FBQztJQUVELHNCQUFPLEdBQVAsVUFBUSxJQUFhO1FBQ2pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNsRCxPQUFPLElBQUksQ0FBQzthQUNmO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsNEJBQWEsR0FBYixVQUFjLE1BQWlCLEVBQUUsSUFBYTtRQUMxQyxnQkFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7SUFDM0MsQ0FBQztJQUVELG1DQUFvQixHQUFwQixVQUFxQixLQUFhO1FBQzlCLElBQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFLGNBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUIsa0JBQWtCO1FBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCwyQkFBWSxHQUFaO1FBQUEsaUJBUUM7UUFQRyxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEMsYUFBRyxDQUFDLE1BQU0sQ0FBQztZQUNQLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBRTtZQUNDLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMxQixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ3JELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVCQUFRLEdBQVI7UUFBQSxpQkFTQztRQVJHLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2xELGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUU7WUFDQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGFBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVLLGdDQUFpQixHQUF2QixVQUF3QixDQUFDLEVBQUUsS0FBSzs7Ozs7d0JBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQzt3QkFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDOzZCQUVoRCxDQUFBLEtBQUssSUFBSSxDQUFDLENBQUEsRUFBVix3QkFBVTt3QkFDVixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7d0JBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7d0JBQ3BCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTt3QkFDcEIscUJBQU0sTUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsVUFBVSxDQUFDOzRCQUNqQywwQ0FBMEM7NEJBQzFDLHNCQUFzQjswQkFGVzs7d0JBQWpDLFNBQWlDLENBQUE7d0JBQ2pDLDBDQUEwQzt3QkFDMUMsc0JBQXNCO3dCQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7d0JBQ2pDLHFCQUFNLE1BQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUFyQixTQUFxQixDQUFBO3dCQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO3dCQUN6QyxxQkFBTSxNQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBdEIsU0FBc0IsQ0FBQTt3QkFDdEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBOzs7d0JBRWYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUE7d0JBQy9CLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFLFNBQVM7NEJBQ3ZCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTs0QkFDbkIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtnQ0FDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBOzZCQUM5QjtpQ0FBTTtnQ0FDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBOzZCQUN0Qzt5QkFDSjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7eUJBQ3hCOzs7Ozs7S0FHUjtJQUVELDRCQUE0QjtJQUM1QiwyREFBMkQ7SUFDM0QsMEJBQTBCO0lBQzFCLDJEQUEyRDtJQUMzRCxvQ0FBb0M7SUFDcEMsSUFBSTtJQUVKLFFBQVE7SUFDUix3QkFBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELHlCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDOztJQUVNLFVBQUssR0FBRyxVQUFPLElBQVk7O1lBQzlCLHNCQUFPLElBQUksT0FBTyxDQUFDLFVBQUEsT0FBTztvQkFDdEIsVUFBVSxDQUFDO3dCQUNQLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ2IsQ0FBQyxDQUFDLEVBQUE7O1NBQ0wsQ0FBQTtJQUVlLFNBQUksR0FBVyxDQUFDLENBQUMsQ0FBQyxrQkFBa0I7SUFDcEMsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLFlBQVk7SUFDOUIsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87SUFDekIsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87SUFDekMsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsd0RBQXdEO0lBRXhDLGVBQVUsR0FBVyxHQUFHLENBQUEsQ0FBQSxPQUFPO0lBQy9CLGVBQVUsR0FBVyxFQUFFLENBQUM7SUFDeEIsYUFBUSxHQUFXLElBQUksQ0FBQztJQUN4Qiw2QkFBd0IsR0FBVyxJQUFJLENBQUM7SUF4cEN4RDtRQURDLFFBQVEsQ0FBQyxjQUFJLENBQUM7c0NBQ0c7SUFFbEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztzQ0FDRztJQUV2QjtRQURDLFFBQVEsQ0FBQyxnQkFBTSxDQUFDO3VDQUNJO0lBRXJCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7c0NBQ0c7SUFFdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztzQ0FDRztJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3NDQUNHO0lBRXZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ1k7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt3Q0FDSztJQUV2QjtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7dUNBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQzt3Q0FDSztJQUV0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3lDQUNNO0lBRXpCO1FBREMsUUFBUSxDQUFDLG1CQUFTLENBQUM7c0NBQ0c7SUFFdkI7UUFEQyxRQUFRLENBQUMsZUFBSyxDQUFDO3VDQUNJO0lBRXBCO1FBREMsUUFBUSxDQUFDLE9BQU8sQ0FBQzs2Q0FDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsyQ0FDRjtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7K0NBQ0U7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2tEQUNLO0lBRXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsyQ0FDRjtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7MkNBQ0Y7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDOzBDQUNIO0lBRTlCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsrQ0FDRTtJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7K0NBQ0U7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDOytDQUNFO0lBRW5DO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztpREFDSTtJQUVyQztRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7dUNBQ0k7SUF0REgsSUFBSTtRQUR4QixPQUFPO09BQ2EsSUFBSSxDQTRwQ3hCO0lBQUQsV0FBQztDQTVwQ0QsQUE0cENDLENBNXBDaUMsRUFBRSxDQUFDLFNBQVMsR0E0cEM3QztrQkE1cENvQixJQUFJIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFBsYXllciBmcm9tIFwiLi9QbGF5ZXJcIjtcbmltcG9ydCBEZWNrIGZyb20gXCIuL0RlY2tcIjtcbmltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcbmltcG9ydCBIZWxwZXIgZnJvbSBcIi4vSGVscGVyXCI7XG5pbXBvcnQgQ2FyZEdyb3VwIGZyb20gXCIuL0NhcmRHcm91cFwiO1xuaW1wb3J0IENvbW1vbkNhcmQgZnJvbSBcIi4vQ29tbW9uQ2FyZFwiO1xuaW1wb3J0IFRvYXN0IGZyb20gXCIuL1RvYXN0XCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IE5vdGljZSBmcm9tIFwiLi9Ob3RpY2VcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4vQ29uZmlnXCI7XG5pbXBvcnQgdXRpbCBmcm9tIFwiLi91dGlsXCI7XG5pbXBvcnQgU3BpbldoZWVsIGZyb20gXCIuL1NwaW5XaGVlbFwiO1xuaW1wb3J0IFBvcHVwIGZyb20gXCIuL1BvcHVwXCI7XG5pbXBvcnQgQm90IGZyb20gXCIuL0JvdFwiO1xuaW1wb3J0IE1vZGFsIGZyb20gXCIuL3BvcG9wL01vZGFsXCI7XG5pbXBvcnQgRXZlbnRLZXlzIGZyb20gXCIuL0V2ZW50S2V5c1wiO1xuaW1wb3J0IExhbmd1YWdlIGZyb20gXCIuL0xhbmd1YWdlXCI7XG5cbmxldCBzdWl0ZUFzc2V0ID0ge1xuICAgIEg6ICdjbycsXG4gICAgRDogXCJyb1wiLFxuICAgIEM6IFwiY2h1b25cIixcbiAgICBTOiBcImLDrWNoXCJcbn1cblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3MoJ1Byb2ZpbGUnKVxuY2xhc3MgUHJvZmlsZSB7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxuICAgIGF2YXRhcjogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TdHJpbmcpXG4gICAgdXNlcm5hbWU6IHN0cmluZyA9ICcnO1xufVxuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KERlY2spXG4gICAgZGVjazogRGVjayA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgICBwbGF5OiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQbGF5ZXIpXG4gICAgc2VhdHM6IFBsYXllcltdID0gW107XG4gICAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgICBmaXJlOiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgZm9sZDogY2MuQnV0dG9uID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICAgIHNvcnQ6IGNjLkJ1dHRvbiA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcmVzdWx0RWZmZWN0czogY2MuTm9kZVtdID0gW107XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgYmlnV2luOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoVG9hc3QpXG4gICAgdG9hc3Q6IFRvYXN0ID0gbnVsbDtcbiAgICBAcHJvcGVydHkoTm90aWNlKVxuICAgIG5vdGljZTogTm90aWNlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgYmV0VGV4dDogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShTcGluV2hlZWwpXG4gICAgc3BpbjogU3BpbldoZWVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoUG9wdXApXG4gICAgcG9wdXA6IFBvcHVwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoUHJvZmlsZSlcbiAgICBwcm9maWxlQm90czogUHJvZmlsZVtdID0gW107XG5cbiAgICBAcHJvcGVydHkoY2MuVG9nZ2xlKVxuICAgIHNvdW5kVG9nZ2xlOiBjYy5Ub2dnbGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9Gb2xkOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvU2hvd0NhcmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9HYXJiYWdlQ2FyZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5BdWRpb0NsaXAgfSlcbiAgICBhdWRpb0ZpcmU6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9Mb3NlOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvV2luOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvU29ydENhcmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9RdWl0R2FtZTogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5BdWRpb0NsaXAgfSlcbiAgICBhdWRpb0RlYWxDYXJkOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvRmlyZVNpbmdsZTogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoTW9kYWwpXG4gICAgbW9kYWw6IE1vZGFsID0gbnVsbDtcblxuICAgIHBsYXllcnM6IFBsYXllcltdID0gW107XG4gICAgY29tbW9uQ2FyZHM6IENvbW1vbkNhcmQgPSBuZXcgQ29tbW9uQ2FyZCgpO1xuICAgIHN0YXRlOiBudW1iZXIgPSAwO1xuICAgIHR1cm46IG51bWJlciA9IDA7XG4gICAgbGVhdmVHYW1lOiBib29sZWFuID0gZmFsc2U7XG4gICAgb3duZXI6IFBsYXllciA9IG51bGw7XG4gICAgbXlzZWxmOiBQbGF5ZXIgPSBudWxsO1xuICAgIGJldFZhbHVlOiBudW1iZXIgPSAxMDAwO1xuICAgIHRvdGFsUGxheWVyOiBudW1iZXIgPSA2O1xuICAgIHN0YXJ0VGltZTogbnVtYmVyID0gMDtcbiAgICBwbGF5VGltZTogbnVtYmVyID0gMDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMub25Ub3VjaFN0YXJ0LCB0aGlzKTtcbiAgICAgICAgbGV0IEFzID0gY2MuZmluZCgnQ2FudmFzL2NhcmRzL0FTJyk7XG5cbiAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQoJ2NhcmRzJywgY2MuU3ByaXRlQXRsYXMsIChlcnIsIGF0bGFzKSA9PiB7XG4gICAgICAgICAgICBsZXQgaW5pdENhcmRzUmVzb3VjZXMgPSBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICAgICAgICAgICAgICBuZXcgQXJyYXkoMTMpLmZpbGwoJycpLm1hcCgoZWwsIHJhbmspID0+IHtcbiAgICAgICAgICAgICAgICAgICAgWydTJywgJ0MnLCAnRCcsICdIJ10ubWFwKChzdWl0ZSwgaWRTdWl0ZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyYW5rICYmICFzdWl0ZSkgcmV0dXJuXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgciA9IHJhbmsgKyAxXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUoQXMpXG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByID0gJ0EnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMTE6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHIgPSAnSidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxMjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgciA9ICdRJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDEzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByID0gJ0snXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgYXNzZXRTdWl0ZU5hbWUgPSBzdWl0ZUFzc2V0W3N1aXRlXVxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJhbmtBc3NldG5hbWUgPSByXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaWRTdWl0ZSA8IDIpIHJhbmtBc3NldG5hbWUgKz0gJy0xJ1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhyYW5rQXNzZXRuYW1lLCBhc3NldFN1aXRlTmFtZSwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3Tm9kZS5uYW1lID0gciArIHN1aXRlXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgc3VpdGVTcHJpdGUgPSBhdGxhcy5nZXRTcHJpdGVGcmFtZShhc3NldFN1aXRlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmNoaWxkcmVuWzBdLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gYXRsYXMuZ2V0U3ByaXRlRnJhbWUocmFua0Fzc2V0bmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmNoaWxkcmVuWzFdLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gc3VpdGVTcHJpdGVcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld05vZGUuY2hpbGRyZW5bMl0uZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBzdWl0ZVNwcml0ZVxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3Tm9kZS5jaGlsZHJlblszXS5nZXRDb21wb25lbnQoY2MuV2lkZ2V0KS50YXJnZXQgPSBuZXdOb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmFjdGl2ZSA9IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhuZXdOb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIEFzLnBhcmVudC5hZGRDaGlsZChuZXdOb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyYW5rID09PSAxMiAmJiBzdWl0ZSA9PT0gJ0gnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoMSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCA1MDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgUHJvbWlzZS5hbGwoW2luaXRDYXJkc1Jlc291Y2VzXSkudGhlbihycyA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbml0R2FtZSgpXG4gICAgICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBzdGFydCgpe31cblxuICAgIGluaXRHYW1lKCkge1xuICAgICAgICB0aGlzLnN0YXJ0VGltZSA9IERhdGUubm93KCk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLldBSVQ7XG4gICAgICAgIHRoaXMuYmV0VmFsdWUgPSBDb25maWcuYmV0VmFsdWU7XG4gICAgICAgIHRoaXMudG90YWxQbGF5ZXIgPSBDb25maWcudG90YWxQbGF5ZXI7XG4gICAgICAgIHRoaXMuYmV0VGV4dC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCgnTk8nKSArICc6ICcgKyB1dGlsLm51bWJlckZvcm1hdCh0aGlzLmJldFZhbHVlKTtcbiAgICAgICAgdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc29ydC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnBsYXkubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKCk7XG4gICAgICAgIHRoaXMubm90aWNlLmhpZGUoKTtcblxuICAgICAgICB0aGlzLnNwaW4ub25TcGluSGlkZSA9IHRoaXMub25TcGluSGlkZS5iaW5kKHRoaXMpO1xuICAgICAgICB0aGlzLnNwaW4ub25TcGluQ29tcGxldGVkID0gdGhpcy5vblNwaW5Db21wbGV0ZWQuYmluZCh0aGlzKTtcblxuICAgICAgICBpZiAoQ29uZmlnLmJhdHRsZSkge1xuICAgICAgICAgICAgdGhpcy50b3RhbFBsYXllciA9IDI7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgYXJyID0gLzB8My9cblxuICAgICAgICBpZiAodGhpcy50b3RhbFBsYXllciA9PT0gNCkgYXJyID0gLzB8MXwzfDUvOyBlbHNlIGlmICh0aGlzLnRvdGFsUGxheWVyID09PSA2KSBhcnIgPSAvMHwxfDJ8M3w0fDUvXG4gICAgICAgIHRoaXMuc2VhdHMubWFwKChzZWF0LCBpKSA9PiB7XG4gICAgICAgICAgICBzZWF0LnNlYXQgPSB0aGlzLnBsYXllcnMubGVuZ3RoO1xuICAgICAgICAgICAgaWYgKGFyci50ZXN0KGkudG9TdHJpbmcoKSkpXG4gICAgICAgICAgICAgICAgdGhpcy5hZGRTZWF0KHNlYXQpXG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBzZWF0LmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcblxuICAgICAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudG90YWxQbGF5ZXI7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IHNlYXQgPSB0aGlzLnNlYXRzW2ldO1xuICAgICAgICAvLyAgICAgc2VhdC5zZWF0ID0gaTtcbiAgICAgICAgLy8gICAgIGlmIChpIDwgdGhpcy50b3RhbFBsYXllcikge1xuICAgICAgICAvLyAgICAgICAgIGxldCBwcm9maWxlID0gdGhpcy5nZXRQcm9maWxlQm90KCk7XG4gICAgICAgIC8vICAgICAgICAgc2VhdC5zaG93KCk7XG4gICAgICAgIC8vICAgICAgICAgc2VhdC5zZXRBdmF0YXIocHJvZmlsZS5hdmF0YXIpO1xuICAgICAgICAvLyAgICAgICAgIHNlYXQuc2V0VXNlcm5hbWUocHJvZmlsZS51c2VybmFtZSk7XG4gICAgICAgIC8vICAgICAgICAgdGhpcy5wbGF5ZXJzLnB1c2goc2VhdCk7XG4gICAgICAgIC8vICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgICAgIHNlYXQuaGlkZSgpO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG5cblxuXG4gICAgICAgIHRoaXMubXlzZWxmID0gdGhpcy5wbGF5ZXJzW3RoaXMudG90YWxQbGF5ZXIgLyAyXTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0VXNlcm5hbWUoQXBpLnVzZXJuYW1lKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0Q29pbihBcGkuY29pbik7XG4gICAgICAgIHRoaXMubXlzZWxmLnNldFRpbWVDYWxsYmFjayh0aGlzLm9uUGxheWVyVGltZW91dCwgdGhpcywgdGhpcy5ub2RlKTtcbiAgICAgICAgaWYgKENvbmZpZy51c2VycGhvdG8pIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNldEF2YXRhcihuZXcgY2MuU3ByaXRlRnJhbWUoQ29uZmlnLnVzZXJwaG90bykpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKENvbmZpZy5iYXR0bGUpIHtcbiAgICAgICAgICAgIGxldCBib3QgPSB0aGlzLnBsYXllcnNbMV07XG4gICAgICAgICAgICBib3Quc2V0Q29pbihDb25maWcuYmF0dGxlLmNvaW4pO1xuICAgICAgICAgICAgYm90LnNldEF2YXRhcihuZXcgY2MuU3ByaXRlRnJhbWUoQ29uZmlnLmJhdHRsZS5waG90bykpO1xuICAgICAgICAgICAgYm90LnNldFVzZXJuYW1lKENvbmZpZy5iYXR0bGUudXNlcm5hbWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcChwbGF5ZXIgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChwbGF5ZXIuaXNCb3QoKSkgcGxheWVyLnNldENvaW4oQ29uZmlnLmJvdENvaW4pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICBjYy5kZWxheVRpbWUoMC43KSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuZGVhbCwgdGhpcylcbiAgICAgICAgKSk7XG5cbiAgICAgICAgaWYgKENvbmZpZy5zb3VuZEVuYWJsZSkge1xuICAgICAgICAgICAgdGhpcy5zb3VuZFRvZ2dsZS51bmNoZWNrKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNvdW5kVG9nZ2xlLmNoZWNrKCk7XG4gICAgICAgIH1cblxuICAgICAgICBBcGkucHJlbG9hZEludGVyc3RpdGlhbEFkKCk7XG4gICAgfVxuXG4gICAgYWRkU2VhdCA9IChzZWF0OiBQbGF5ZXIpID0+IHtcbiAgICAgICAgbGV0IHByb2ZpbGUgPSB0aGlzLmdldFByb2ZpbGVCb3QoKTtcbiAgICAgICAgc2VhdC5zaG93KCk7XG4gICAgICAgIHNlYXQuc2V0QXZhdGFyKHByb2ZpbGUuYXZhdGFyKTtcbiAgICAgICAgc2VhdC5zZXRVc2VybmFtZShwcm9maWxlLnVzZXJuYW1lKTtcbiAgICAgICAgdGhpcy5wbGF5ZXJzLnB1c2goc2VhdCk7XG4gICAgfVxuXG4gICAgb25EZXN0cm95KCkge1xuICAgICAgICAvLyB0aGlzLmNsZWFyTG9nVGltZSgpXG4gICAgICAgIC8vIGNvbnN0IGR1cmF0aW9uID0gRGF0ZS5ub3coKSAtIHRoaXMuc3RhcnRUaW1lO1xuICAgICAgICAvLyBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfRFVSQVRJT04sIE1hdGguZmxvb3IoZHVyYXRpb24gLyAxMDAwKSk7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlKGR0KSB7XG4gICAgLy8gICAgIGlmICh0aGlzLnBsYXlUaW1lIDwgMzAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDMwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctMzAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgNjAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDYwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctNjAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgOTAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDkwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctOTAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgMTgwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSAxODApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy0xODAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgMzYwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSAzNjApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy0zNjAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgNTAwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSA1MDApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy01MDAnKTtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgICB0aGlzLnBsYXlUaW1lICs9IGR0O1xuICAgIC8vIH1cblxuICAgIG9uVG91Y2hTdGFydChldnQ6IGNjLlRvdWNoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlICE9IEdhbWUuUExBWSkgcmV0dXJuO1xuXG4gICAgICAgIGxldCBzZWxlY3RlZCA9IHRoaXMubXlzZWxmLnRvdWNoKGV2dC5nZXRMb2NhdGlvbigpKTtcblxuICAgICAgICBpZiAoIXNlbGVjdGVkKSByZXR1cm47XG5cbiAgICAgICAgbGV0IHByZXBhcmVDYXJkcyA9IHRoaXMubXlzZWxmLnByZXBhcmVDYXJkcztcblxuICAgICAgICBpZiAocHJlcGFyZUNhcmRzLmNvbnRhaW5zKHNlbGVjdGVkKSkge1xuICAgICAgICAgICAgdGhpcy5teXNlbGYudW5zZWxlY3RDYXJkKHNlbGVjdGVkKTtcbiAgICAgICAgICAgIHRoaXMub25DYXJkU2VsZWN0ZWQoc2VsZWN0ZWQsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNlbGVjdENhcmQoc2VsZWN0ZWQpO1xuICAgICAgICAgICAgdGhpcy5vbkNhcmRTZWxlY3RlZChzZWxlY3RlZCwgdHJ1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy50dXJuICE9IHRoaXMubXlzZWxmLnNlYXQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwcmVwYXJlQ2FyZHMua2luZCA9PSBDYXJkR3JvdXAuVW5ncm91cGVkKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZShmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5jb21tb25DYXJkcy5sZW5ndGgoKSA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwcmVwYXJlQ2FyZHMuZ3QodGhpcy5jb21tb25DYXJkcy5wZWVrKCkpKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWxsb3dGaXJlKGZhbHNlKTtcbiAgICB9XG5cbiAgICBvblBsYXllclRpbWVvdXQoKSB7XG4gICAgICAgIHRoaXMub25TdGFuZENsaWNrZWQoKVxuICAgICAgICAvLyB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgLy8gaWYgKHRoaXMuY29tbW9uQ2FyZHMubGVuZ3RoKCkgPT0gMCkge1xuICAgICAgICAvLyAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNaW5DYXJkKHRoaXMubXlzZWxmLmNhcmRzKTtcbiAgICAgICAgLy8gICAgIHRoaXMub25GaXJlKHRoaXMubXlzZWxmLCBuZXcgQ2FyZEdyb3VwKFtjYXJkc10sIENhcmRHcm91cC5TaW5nbGUpKTtcbiAgICAgICAgLy8gICAgIHRoaXMubXlzZWxmLnJlb3JkZXIoKTtcbiAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgLy8gICAgIHJldHVybiB0aGlzLm9uRm9sZCh0aGlzLm15c2VsZik7XG4gICAgICAgIC8vIH1cbiAgICB9XG5cbiAgICBzaG93QWxsUGxheWVyQ2FyZCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcChwbGF5ZXIgPT4ge1xuICAgICAgICAgICAgcGxheWVyLnNob3dBbGxDYXJkKClcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBvblF1aXRDbGlja2VkKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlFVSVRfR0FNRSk7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlID09IEdhbWUuUExBWSB8fCB0aGlzLnN0YXRlID09IEdhbWUuREVBTCkge1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvUXVpdEdhbWUpO1xuICAgICAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdob21lJyk7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkJhY2tDbGlja2VkKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZSA9PSBHYW1lLlBMQVkgfHwgdGhpcy5zdGF0ZSA9PSBHYW1lLkRFQUwpIHtcbiAgICAgICAgICAgIGxldCBjb2luID0gTWF0aC5tYXgoMCwgQXBpLmNvaW4gLSB0aGlzLmJldFZhbHVlICogMTApO1xuICAgICAgICAgICAgQXBpLnVwZGF0ZUNvaW4oY29pbik7XG4gICAgICAgIH1cbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb1F1aXRHYW1lKTtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdob21lJyk7XG4gICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICB9XG5cbiAgICBzaG93SGFuZENhcmRzKCkge1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvU2hvd0NhcmQpO1xuICAgICAgICAvLyB0aGlzLm15c2VsZi5zaG93SGFuZENhcmRzKCk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLlBMQVk7XG5cbiAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLm15c2VsZi5jYXJkcyk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcblxuXG5cbiAgICAgICAgLy8gaWYgKEhlbHBlci5pc0JpZ1dpbih0aGlzLm15c2VsZi5jYXJkcykpIHtcblxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgICAgICBjYy5kZWxheVRpbWUoMC43KSxcbiAgICAgICAgLy8gICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLm9uQmlnV2luLCB0aGlzLCB0aGlzLm15c2VsZilcbiAgICAgICAgLy8gICAgICkpO1xuICAgICAgICAvLyAgICAgcmV0dXJuO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSgwLjcpLFxuICAgICAgICAvLyAgICAgY2MuY2FsbEZ1bmModGhpcy5teXNlbGYuc29ydEhhbmRDYXJkcywgdGhpcy5teXNlbGYpXG4gICAgICAgIC8vICkpO1xuXG4gICAgICAgIHRoaXMucGxheWVycy5tYXAoKHBsYXllciwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGlmIChwbGF5ZXIuaXNCb3QoKSkgcGxheWVyLnNob3dDYXJkQ291bnRlcigpIGVsc2Uge1xuICAgICAgICAgICAgICAgIHBsYXllci5zaG93QWxsQ2FyZCgpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG5cblxuICAgICAgICBsZXQgY2hlY2tCSiA9IHRoaXMucGxheWVycy5maWx0ZXIocGxheWVyID0+IHtcbiAgICAgICAgICAgIHBsYXllci5jaGVja0JsYWNrSmFjaygpXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy8gbGV0IGlzVXNlcldpbiA9IGNoZWNrQkouZmluZChwbGF5ZXIgPT4gcGxheWVyLmlzVXNlcigpKVxuICAgICAgICAvLyBsZXQgaXNVc2VyT3JEZWFsZXJXaW4gPSBjaGVja0JKLmZpbmQocGxheWVyID0+ICFwbGF5ZXIuaXNCb3QoKSB8fCBwbGF5ZXIuaXNDYWkoKSlcbiAgICAgICAgLy8gaWYgKGlzVXNlck9yRGVhbGVyV2luKSByZXR1cm4gdGhpcy5zaG93UmVzdWx0KClcbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSAtIDE7IGkgPiAwOyAtLWkpIHtcbiAgICAgICAgLy8gICAgIHRoaXMucGxheWVyc1tpXS5zaG93Q2FyZENvdW50ZXIoKTtcbiAgICAgICAgLy8gfVxuXG4gICAgICAgIGlmICghdGhpcy5vd25lcikge1xuICAgICAgICAgICAgdGhpcy5vd25lciA9IHRoaXMuZmluZFBsYXllckhhc0ZpcnN0Q2FyZCgpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFR1cm4odGhpcy5vd25lciwgdHJ1ZSk7XG4gICAgICAgIHRoaXMuc29ydC5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgZmluZFBsYXllckhhc0ZpcnN0Q2FyZCgpIHtcbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgIC8vICAgICBpZiAoSGVscGVyLmZpbmRDYXJkKHRoaXMucGxheWVyc1tpXS5jYXJkcywgMywgMSkpXG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgLy8gfVxuICAgICAgICBsZXQgdXNlckluUm91bmQgPSB0aGlzLnBsYXllcnMuZmlsdGVyKHBsYXllciA9PiBwbGF5ZXIuaXNJblJvdW5kKCkgJiYgIXBsYXllci5pc0NhaSgpICYmIHBsYXllcilcbiAgICAgICAgcmV0dXJuIHVzZXJJblJvdW5kWzBdO1xuICAgIH1cblxuICAgIHNob3dEYXNoYm9hcmQobGVhZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmFsbG93Rm9sZCghbGVhZCk7XG4gICAgICAgIGlmICh0aGlzLmNvbW1vbkNhcmRzLmxlbmd0aCgpID4gMCkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gQm90LnN1Z2dlc3QodGhpcy5jb21tb25DYXJkcywgdGhpcy5teXNlbGYuY2FyZHMpO1xuICAgICAgICAgICAgdGhpcy5hbGxvd0ZpcmUoISFjYXJkcyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmZvbGQubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmZpcmUubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cblxuICAgIHNldENvbG9yKGJ0bjogY2MuQnV0dG9uLCBjb2xvcjogY2MuQ29sb3IpIHtcbiAgICAgICAgYnRuLnRhcmdldC5jb2xvciA9IGNvbG9yO1xuICAgICAgICBidG4udGFyZ2V0LmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4ge1xuICAgICAgICAgICAgY2hpbGQuY29sb3IgPSBjb2xvcjtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGlkZURhc2hib2FyZChuZWVkU2hvdyA9IGZhbHNlKSB7XG4gICAgICAgIGxldCBhY3Rpb25Vc2VyID0gY2MuZmluZCgnQ2FudmFzL2FjdGlvblVzZXInKVxuXG4gICAgICAgIGFjdGlvblVzZXIuYWN0aXZlID0gbmVlZFNob3dcbiAgICAgICAgaWYgKG5lZWRTaG93KSB7XG4gICAgICAgICAgICBsZXQgc2hvd0luc3VyciA9IHRoaXMucGxheWVyc1swXS5jYXJkc1swXS5yYW5rID09PSAxXG4gICAgICAgICAgICAvLyBzaG93SW5zdXJyID0gdHJ1ZVxuICAgICAgICAgICAgYWN0aW9uVXNlci5jaGlsZHJlbls0XS5hY3RpdmUgPSBzaG93SW5zdXJyXG5cbiAgICAgICAgICAgIGxldCBzaG93U3BsaXQgPSAhdGhpcy5teXNlbGYuaXNTcGxpdCAmJiB0aGlzLm15c2VsZi5jYXJkc1swXS5yYW5rID09PSB0aGlzLm15c2VsZi5jYXJkc1sxXS5yYW5rXG5cbiAgICAgICAgICAgIHNob3dTcGxpdCA9IHRydWVcbiAgICAgICAgICAgIGlmICghc2hvd1NwbGl0ICYmIHNob3dJbnN1cnIpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25Vc2VyLmNoaWxkcmVuWzRdLnggPSAxOTAuNDRcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFjdGlvblVzZXIuY2hpbGRyZW5bM10uYWN0aXZlID0gc2hvd1NwbGl0XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhbGxvd0ZvbGQoYWxsb3c6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5mb2xkLmludGVyYWN0YWJsZSA9IGFsbG93O1xuICAgICAgICBsZXQgY29sb3IgPSBhbGxvdyA/IGNjLkNvbG9yLldISVRFIDogY2MuQ29sb3IuR1JBWTtcbiAgICAgICAgdGhpcy5zZXRDb2xvcih0aGlzLmZvbGQsIGNvbG9yKTtcbiAgICB9XG5cbiAgICBhbGxvd0ZpcmUoYWxsb3c6IGJvb2xlYW4pIHtcbiAgICAgICAgLy8gdGhpcy5maXJlLmludGVyYWN0YWJsZSA9IGFsbG93O1xuICAgICAgICBsZXQgY29sb3IgPSBhbGxvdyA/IGNjLkNvbG9yLldISVRFIDogY2MuQ29sb3IuR1JBWTtcbiAgICAgICAgdGhpcy5zZXRDb2xvcih0aGlzLmZpcmUsIGNvbG9yKTtcbiAgICB9XG5cbiAgICBvblNvcnRDYXJkQ2xpY2tlZCgpIHtcbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb1NvcnRDYXJkKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc29ydEhhbmRDYXJkcygpO1xuICAgIH1cblxuICAgIG9uRmlyZUNsaWNrZWQoKSB7XG4gICAgICAgIHRoaXMub25IaXQoKVxuICAgICAgICAvLyBsZXQgY2FyZHMgPSB0aGlzLm15c2VsZi5wcmVwYXJlQ2FyZHM7XG4gICAgICAgIC8vIGlmIChjYXJkcy5jb3VudCgpID09IDApIHsgcmV0dXJuIGNvbnNvbGUubG9nKCdlbXB0eScpOyB9XG5cbiAgICAgICAgLy8gaWYgKHRoaXMuY29tbW9uQ2FyZHMubGVuZ3RoKCkgPiAwICYmICFjYXJkcy5ndCh0aGlzLmNvbW1vbkNhcmRzLnBlZWsoKSkpIHtcbiAgICAgICAgLy8gICAgIHJldHVybiBjb25zb2xlLmxvZygnaW52YWxpZCcpO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gaWYgKGNhcmRzLmlzSW52YWxpZCgpKSB7XG4gICAgICAgIC8vICAgICByZXR1cm4gY29uc29sZS5sb2coJ2ludmFsaWQnKTtcbiAgICAgICAgLy8gfVxuXG4gICAgICAgIC8vIHRoaXMuaGlkZURhc2hib2FyZCgpO1xuICAgICAgICAvLyB0aGlzLm9uRmlyZSh0aGlzLm15c2VsZiwgY2FyZHMpO1xuICAgICAgICAvLyAvLyByZXNvcnQgaGFuZCBjYXJkXG4gICAgICAgIC8vIHRoaXMubXlzZWxmLnJlb3JkZXIoKTtcbiAgICB9XG5cblxuICAgIG9uRm9sZENsaWNrZWQoKSB7XG4gICAgICAgIC8vIHRoaXMuaGlkZURhc2hib2FyZCgpO1xuICAgICAgICAvLyB0aGlzLm9uRm9sZCh0aGlzLm15c2VsZik7XG4gICAgfVxuXG4gICAgZGVhbCgpIHtcbiAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCk7XG4gICAgICAgIGlmIChBcGkuY29pbiA8PSB0aGlzLmJldFZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLm9wZW4obnVsbCwgMyk7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fT1BFTik7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICB0aGlzLnBsYXkubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5kZWNrLnNodWZmbGUoKTtcbiAgICAgICAgdGhpcy5jb21tb25DYXJkcy5yZXNldCgpO1xuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGJvdCA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgICAgIGlmIChib3QuZ2V0Q29pbigpIDw9IHRoaXMuYmV0VmFsdWUgKiAzKSB7XG4gICAgICAgICAgICAgICAgbGV0IHByb2ZpbGUgPSB0aGlzLmdldFByb2ZpbGVCb3QoKTtcbiAgICAgICAgICAgICAgICBsZXQgcmF0ZSA9IDAuNSArIE1hdGgucmFuZG9tKCkgKiAwLjU7XG4gICAgICAgICAgICAgICAgYm90LnNldENvaW4oTWF0aC5mbG9vcih0aGlzLm15c2VsZi5nZXRDb2luKCkgKiByYXRlKSk7XG4gICAgICAgICAgICAgICAgYm90LnNldEF2YXRhcihwcm9maWxlLmF2YXRhcik7XG4gICAgICAgICAgICAgICAgYm90LnNldFVzZXJuYW1lKHByb2ZpbGUudXNlcm5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY2MuZmluZCgnQ2FudmFzL2FjdGlvblVzZXIvaW5zdXJyJykueCA9IDQwMi4wMzhcblxuICAgICAgICB0aGlzLnNlYXRzWzZdLmhpZGUoKVxuXG4gICAgICAgIHRoaXMucGxheWVycy5tYXAocCA9PiB7XG4gICAgICAgICAgICBwLnJlc2V0KCk7XG4gICAgICAgICAgICBwLnVwZGF0ZVBvaW50KCcwJyk7XG4gICAgICAgICAgICBwLnNldEJvbnVzVHlwZSgwKVxuICAgICAgICB9KVxuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgMjsgaSsrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKTsgaisrKSB7XG4gICAgICAgICAgICAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tqXTtcbiAgICAgICAgICAgICAgICBsZXQgY2FyZCA9IHRoaXMuZGVjay5waWNrKCk7XG4gICAgICAgICAgICAgICAgcGxheWVyLnB1c2goY2FyZCwgMC4zICsgKGkgKiB0aGlzLmdldFRvdGFsUGxheWVyKCkgKyBqKSAqIEdhbWUuREVBTF9TUEVFRCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy8gdGhpcy5kZWNrLmhpZGUoKTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZShHYW1lLkRFQUxfU1BFRUQgKiB0aGlzLnRvdGFsUGxheWVyICogMiksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnNob3dIYW5kQ2FyZHMsIHRoaXMpKVxuICAgICAgICApO1xuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MuZGVsYXlUaW1lKDAuNCksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYygoKSA9PiB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvRGVhbENhcmQpKVxuICAgICAgICApKTtcblxuICAgICAgICBBcGkucHJlbG9hZEludGVyc3RpdGlhbEFkKCk7XG4gICAgfVxuXG4gICAgc2hvd0VmZmVjdChwbGF5ZXI6IFBsYXllciwgZWZmZWN0OiBjYy5Ob2RlKSB7XG4gICAgICAgIGVmZmVjdC5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBlZmZlY3Quc2V0UG9zaXRpb24ocGxheWVyLm5vZGUuZ2V0UG9zaXRpb24oKSk7XG4gICAgfVxuXG4gICAgb25Gb2xkKHBsYXllcjogUGxheWVyKSB7XG4gICAgICAgIHV0aWwucGxheUF1ZGlvKHRoaXMuYXVkaW9Gb2xkKTtcbiAgICAgICAgaWYgKHRoaXMuY29tbW9uQ2FyZHMuaGFzQ29tYmF0KCkpIHtcbiAgICAgICAgICAgIGxldCBjb3VudCA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0Q29tYmF0TGVuZ3RoKCk7XG4gICAgICAgICAgICBsZXQgdmljdGltID0gdGhpcy5jb21tb25DYXJkcy5nZXRDb21iYXRWaWN0aW0oKTtcbiAgICAgICAgICAgIGxldCB3aW5uZXIgPSB0aGlzLmNvbW1vbkNhcmRzLmdldENvbWJhdFdpbm5lcigpO1xuICAgICAgICAgICAgbGV0IGJpZ1BpZyA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0Q29tYmF0KCk7XG4gICAgICAgICAgICBsZXQgc3BvdFdpbiA9IE1hdGgucG93KDIsIGNvdW50IC0gMSkgKiBIZWxwZXIuY2FsY3VsYXRlU3BvdFdpbihiaWdQaWcuY2FyZHMpICogdGhpcy5iZXRWYWx1ZTtcbiAgICAgICAgICAgIGxldCBjb2luID0gTWF0aC5taW4oc3BvdFdpbiwgdmljdGltLmNvaW5WYWwpO1xuICAgICAgICAgICAgd2lubmVyLmFkZENvaW4oY29pbik7XG4gICAgICAgICAgICB2aWN0aW0uc3ViQ29pbihjb2luKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5jb21tb25DYXJkcy5pc0NvbWJhdE9wZW4oKSkge1xuICAgICAgICAgICAgdGhpcy5jb21tb25DYXJkcy5yZXNldENvbWJhdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgcGxheWVyLnNldEluUm91bmQoZmFsc2UpO1xuICAgICAgICBwbGF5ZXIuc2V0QWN0aXZlKGZhbHNlKTtcbiAgICAgICAgbGV0IHBsYXllcnMgPSB0aGlzLmdldEluUm91bmRQbGF5ZXJzKCk7XG4gICAgICAgIGlmIChwbGF5ZXJzLmxlbmd0aCA+PSAyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5uZXh0VHVybigpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubmV4dFJvdW5kKHBsYXllcnNbMF0pO1xuICAgIH1cblxuICAgIG9uRmlyZShwbGF5ZXI6IFBsYXllciwgY2FyZHM6IENhcmRHcm91cCwgaXNEb3duOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgY29uc3QgYXVkaW9DbGlwID0gY2FyZHMuaGlnaGVzdC5yYW5rID09IDE1IHx8IGNhcmRzLmNvdW50KCkgPiAxID8gdGhpcy5hdWRpb0ZpcmUgOiB0aGlzLmF1ZGlvRmlyZVNpbmdsZTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjE3KSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKCgpID0+IHV0aWwucGxheUF1ZGlvKGF1ZGlvQ2xpcCkpXG4gICAgICAgICkpO1xuXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUoZmFsc2UpO1xuICAgICAgICBsZXQgY2FyZENvdW50ID0gY2FyZHMuY291bnQoKTtcbiAgICAgICAgbGV0IHBvcyA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0UG9zaXRpb24oKTtcblxuICAgICAgICBjYXJkcy5zb3J0KCk7XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmNvdW50KCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGlzRG93bikgY2FyZHMuYXQoaSkuc2hvdygpO1xuXG4gICAgICAgICAgICBsZXQgY2FyZCA9IGNhcmRzLmF0KGkpO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMuY29tbW9uQ2FyZHMudG90YWxDYXJkcyArIGk7XG4gICAgICAgICAgICBsZXQgbW92ZSA9IGNjLm1vdmVUbygwLjMsIGNjLnYyKHBvcy54ICsgKGkgLSBjYXJkQ291bnQgLyAyKSAqIEdhbWUuQ0FSRF9TUEFDRSwgcG9zLnkpKTtcbiAgICAgICAgICAgIGNhcmQubm9kZS5ydW5BY3Rpb24obW92ZSk7XG4gICAgICAgICAgICBsZXQgc2NhbGUgPSBjYy5zY2FsZVRvKDAuMywgMC42KTtcbiAgICAgICAgICAgIGNhcmQubm9kZS5ydW5BY3Rpb24oc2NhbGUpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY29tbW9uQ2FyZHMucHVzaChjYXJkcyk7XG4gICAgICAgIGlmIChjYXJkcy5oaWdoZXN0LnJhbmsgPT0gMTUpIHtcbiAgICAgICAgICAgIHRoaXMuY29tbW9uQ2FyZHMucHVzaENvbWJhdChwbGF5ZXIsIGNhcmRzKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmNvbW1vbkNhcmRzLmlzQ29tYmF0T3BlbigpKSB7XG4gICAgICAgICAgICB0aGlzLmNvbW1vbkNhcmRzLnB1c2hDb21iYXQocGxheWVyLCBjYXJkcyk7XG4gICAgICAgIH1cbiAgICAgICAgcGxheWVyLnJlbW92ZUNhcmRzKGNhcmRzKTtcbiAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgICAgICBwbGF5ZXIudXBkYXRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjYXJkcy5oaWdoZXN0LnJhbmsgPT0gMTUpIHtcbiAgICAgICAgICAgIHRoaXMubm90aWNlLnNob3dCaWdQaWcoY2FyZHMuY291bnQoKSwgMyk7XG4gICAgICAgIH0gZWxzZSBpZiAoY2FyZHMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIGNhcmRzLmNvdW50KCkgPT0gKDMgKiAyKSkge1xuICAgICAgICAgICAgdGhpcy5ub3RpY2Uuc2hvdyhOb3RpY2UuVEhSRUVfUEFJUiwgMyk7XG4gICAgICAgIH0gZWxzZSBpZiAoY2FyZHMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIGNhcmRzLmNvdW50KCkgPT0gKDQgKiAyKSkge1xuICAgICAgICAgICAgdGhpcy5ub3RpY2Uuc2hvdyhOb3RpY2UuRk9VUl9QQUlSLCAzKTtcbiAgICAgICAgfSBlbHNlIGlmIChjYXJkcy5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiBjYXJkcy5jb3VudCgpID09IDQpIHtcbiAgICAgICAgICAgIHRoaXMubm90aWNlLnNob3coTm90aWNlLkZPVVJfQ0FSRCwgMyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHBsYXllci5jYXJkcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5uZXh0VHVybigpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zdGF0ZSA9IEdhbWUuTEFURTtcbiAgICAgICAgdGhpcy5zb3J0Lm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICBjYy5kZWxheVRpbWUoMiksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnNob3dSZXN1bHQsIHRoaXMsIHBsYXllcilcbiAgICAgICAgKSk7XG4gICAgfVxuXG4gICAgb25CaWdXaW4oc2VuZGVyOiBjYy5Ob2RlLCB3aW5uZXI6IFBsYXllcikge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgICAgICB0aGlzLmJpZ1dpbi5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBsZXQgdG90YWwgPSAwO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgICAgIGlmIChwbGF5ZXIgIT0gd2lubmVyKSB7XG4gICAgICAgICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHBsYXllci5oaWRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgcGxheWVyLnNob3dIYW5kQ2FyZHMoKTtcbiAgICAgICAgICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbGV0IGxvc3QgPSAxMyAqIHRoaXMuYmV0VmFsdWU7XG4gICAgICAgICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIuc3ViQ29pbihsb3N0KTtcbiAgICAgICAgICAgICAgICB0b3RhbCArPSBsb3N0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHdpbm5lci5hZGRDb2luKHRvdGFsKTtcbiAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIHRoaXMub25XaW4odG90YWwpO1xuICAgIH1cblxuICAgIC8vIHNob3dSZXN1bHQoc2VuZGVyOiBjYy5Ob2RlLCB3aW5uZXI6IFBsYXllcikge1xuICAgIC8vICAgICBsZXQgcGxheWVycyA9IHRoaXMucGxheWVycy5maWx0ZXIoKHBsYXllcikgPT4gKHBsYXllciAhPSB3aW5uZXIpKTtcbiAgICAvLyAgICAgcGxheWVycy5zb3J0KChhLCBiKSA9PiB7IHJldHVybiBhLmNhcmRzLmxlbmd0aCAtIGIuY2FyZHMubGVuZ3RoOyB9KTtcblxuICAgIC8vICAgICB0aGlzLnNob3dFZmZlY3Qod2lubmVyLCB0aGlzLnJlc3VsdEVmZmVjdHNbMF0pO1xuICAgIC8vICAgICBsZXQgdG90YWwgPSAwO1xuICAgIC8vICAgICBsZXQgcmFua2luZyA9IDE7XG4gICAgLy8gICAgIGlmICh3aW5uZXIgPT0gdGhpcy5teXNlbGYpIHJhbmtpbmcgPSAxO1xuICAgIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBsYXllcnMubGVuZ3RoOyBpKyspIHtcbiAgICAvLyAgICAgICAgIGxldCBwbGF5ZXIgPSBwbGF5ZXJzW2ldO1xuICAgIC8vICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgLy8gICAgICAgICAgICAgcGxheWVyLmhpZGVDYXJkQ291bnRlcigpO1xuICAgIC8vICAgICAgICAgICAgIHBsYXllci5zaG93SGFuZENhcmRzKCk7XG4gICAgLy8gICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgICAgIGlmIChwbGF5ZXIgPT0gdGhpcy5teXNlbGYpIHJhbmtpbmcgPSBpICsgMjtcbiAgICAvLyAgICAgICAgIHRoaXMuc2hvd0VmZmVjdChwbGF5ZXIsIHRoaXMucmVzdWx0RWZmZWN0c1tpICsgMV0pO1xuICAgIC8vICAgICAgICAgbGV0IGxvc3QgPSBIZWxwZXIuY2FsY3VsYXRlKHBsYXllci5jYXJkcykgKiB0aGlzLmJldFZhbHVlO1xuICAgIC8vICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAvLyAgICAgICAgIHBsYXllci5zdWJDb2luKGxvc3QpO1xuICAgIC8vICAgICAgICAgdG90YWwgKz0gbG9zdDtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgICB3aW5uZXIuYWRkQ29pbih0b3RhbCk7XG4gICAgLy8gICAgIEFwaS51cGRhdGVDb2luKHRoaXMubXlzZWxmLmNvaW5WYWwpO1xuICAgIC8vICAgICB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgIC8vICAgICBpZiAod2lubmVyID09IHRoaXMubXlzZWxmKSB7XG4gICAgLy8gICAgICAgICB0aGlzLm9uV2luKHRvdGFsKTtcbiAgICAvLyAgICAgfSBlbHNlIHtcbiAgICAvLyAgICAgICAgIHRoaXMub25Mb3NlKHJhbmtpbmcpO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuXG4gICAgb25Mb3NlKHJhbmtpbmc6IG51bWJlcikge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLkxPU0UgKyAnXycgKyByYW5raW5nKTtcbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb0xvc2UpO1xuICAgICAgICBjb25zdCBybmQgPSBNYXRoLnJhbmRvbSgpO1xuICAgICAgICBpZiAocmFua2luZyA9PSAyICYmIHJuZCA8PSAwLjMpIHtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgfSBlbHNlIGlmIChyYW5raW5nID09IDMgJiYgcm5kIDw9IDAuMikge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICB9IGVsc2UgaWYgKHJhbmtpbmcgPT0gNCAmJiBybmQgPD0gMC4xKSB7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuICAgIH1cblxuICAgIG9uV2luKHdvbjogbnVtYmVyKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuV0lOKTtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9TUElOX09QRU4pO1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvV2luKTtcbiAgICAgICAgY29uc3Qgcm5kID0gTWF0aC5yYW5kb20oKTtcbiAgICAgICAgaWYgKHJuZCA8PSAwLjMgJiYgQXBpLmlzSW50ZXJzdGl0aWFsQWRMb2FkZWQoKSkge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zcGluLnNob3cod29uKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uU3BpbkhpZGUoKSB7XG4gICAgICAgIHRoaXMuZGVsYXlSZXNldCgzKTtcbiAgICB9XG5cbiAgICBvblNwaW5Db21wbGV0ZWQocmVzdWx0LCB3b246IG51bWJlcikge1xuICAgICAgICBsZXQgYm9udXMgPSB0aGlzLnNwaW4uY29tcHV0ZShyZXN1bHQuaWQsIHdvbik7XG4gICAgICAgIGlmIChib251cyA+IDApIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLmFkZENvaW4oYm9udXMpO1xuICAgICAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkZWxheVJlc2V0KGR0OiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZShkdCksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnJlc2V0LCB0aGlzKVxuICAgICAgICApKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMucmVzdWx0RWZmZWN0cy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRFZmZlY3RzW2ldLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmdldFRvdGFsUGxheWVyKCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgdGhpcy5wbGF5ZXJzW2ldLnJlc2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuYmlnV2luLmFjdGl2ZSkge1xuICAgICAgICAgICAgdGhpcy5iaWdXaW4uYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLy8gdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMucGxheS5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgb25DYXJkU2VsZWN0ZWQoY2FyZDogQ2FyZCwgc2VsZWN0ZWQ6IGJvb2xlYW4pIHtcblxuICAgIH1cblxuICAgIG5leHRUdXJuKCkge1xuICAgICAgICBsZXQgbmV4dCA9IHRoaXMuZ2V0TmV4dEluUm91bmRQbGF5ZXIoKTtcbiAgICAgICAgdGhpcy5zZXRDdXJyZW50VHVybihuZXh0LCBmYWxzZSk7XG4gICAgfVxuXG4gICAgbmV4dFJvdW5kKGxlYWQ6IFBsYXllcikge1xuICAgICAgICB0aGlzLmNvbW1vbkNhcmRzLm5leHRSb3VuZCgpO1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5nZXRUb3RhbFBsYXllcigpIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIHRoaXMucGxheWVyc1tpXS5zZXRJblJvdW5kKHRydWUpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFR1cm4obGVhZCwgdHJ1ZSk7XG4gICAgICAgIC8vIHV0aWwucGxheUF1ZGlvKHRoaXMuYXVkaW9HYXJiYWdlQ2FyZCk7XG4gICAgfVxuXG4gICAgb25IaXRDbGlja2VkKCkge1xuICAgICAgICBsZXQgcGxheWVyID0gdGhpcy5teXNlbGZcbiAgICAgICAgdGhpcy50YWtlQ2FyZChwbGF5ZXIpXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuaGlkZURhc2hib2FyZCgpXG4gICAgfVxuXG4gICAgb25Eb3VibGVDbGlja2VkKCkge1xuXG4gICAgfVxuXG4gICAgb25TcGxpdENsaWNrZWQoKSB7XG5cbiAgICB9XG5cbiAgICBvblN0YW5kQ2xpY2tlZCgpIHtcbiAgICAgICAgbGV0IHBsYXllciA9IHRoaXMubXlzZWxmXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUoZmFsc2UpO1xuXG4gICAgICAgIGlmICghcGxheWVyLmluUm91bmQgJiYgcGxheWVyLnN1YlVzZXIpIHtcbiAgICAgICAgICAgIHBsYXllciA9IHBsYXllci5zdWJVc2VyXG4gICAgICAgIH1cblxuICAgICAgICBwbGF5ZXIuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKClcbiAgICAgICAgdGhpcy5uZXh0VHVybigpXG4gICAgfVxuXG4gICAgc2hvd1Jlc3VsdCA9ICgpID0+IHtcbiAgICAgICAgLy8gbGV0IGxlYWRlclBvaW50ID0gdGhpcy5wbGF5ZXJzLnNsaWNlKC0xKVswXS5wb2ludFxuICAgICAgICAvLyBsZXQgdXNlclBvaW50ID0gdGhpcy5wbGF5ZXJzWzBdLnBvaW50XG5cbiAgICAgICAgLy8gbGV0IHJlc3VsdFN0cmluZyA9IFtgxJFp4buDbSBjw6FpOiAke2xlYWRlclBvaW50fWAsIGDEkGnhu4NtIHVzZXI6ICR7dXNlclBvaW50fWBdXG5cbiAgICAgICAgLy8gdGhpcy5wbGF5ZXJzLm1hcChwID0+IHtcbiAgICAgICAgLy8gICAgIGlmIChwLmlzQm90KCkgJiYgIXAuaXNDYWkoKSkge1xuICAgICAgICAvLyAgICAgICAgIHJlc3VsdFN0cmluZy5wdXNoKGDEkGnhu4NtIGJvdCAke3Jlc3VsdFN0cmluZy5sZW5ndGggLSAxfTogJHtwLnBvaW50fWApXG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vIH0pXG5cblxuICAgICAgICBsZXQgZGVhbGVyID0gdGhpcy5wbGF5ZXJzWzBdXG5cbiAgICAgICAgbGV0IGRlYWxlckFkZENvaW4gPSAwXG5cbiAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcCgocGxheWVyLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgaWYgKCFpbmRleCkgcmV0dXJuXG5cbiAgICAgICAgICAgIGxldCBwbGF5ZXJDb2luID0gdGhpcy5jYWxDb2ludEVuZEdhbWUocGxheWVyKVxuICAgICAgICAgICAgaWYgKHBsYXllci5zdWJVc2VyKSB7XG4gICAgICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmNhbENvaW50RW5kR2FtZShwbGF5ZXIuc3ViVXNlcilcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcGxheWVyLmNoYW5nZUNvaW4ocGxheWVyQ29pbilcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ3VzZXIgJyArIGluZGV4ICsgJyAnICsgcGxheWVyQ29pbik7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG5cbiAgICAgICAgICAgIGRlYWxlckFkZENvaW4gLT0gcGxheWVyQ29pblxuICAgICAgICB9KVxuXG4gICAgICAgIGRlYWxlci5jaGFuZ2VDb2luKGRlYWxlckFkZENvaW4pXG5cbiAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICBjb25zb2xlLmxvZygnbmhhIGNhaSAnICsgZGVhbGVyQWRkQ29pbik7XG4gICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcblxuICAgICAgICB0aGlzLnNob3dBbGxQbGF5ZXJDYXJkKClcbiAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuXG5cblxuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICAvLyB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgICAgICAvLyB0aGlzLmJpZ1dpbi5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAvLyBsZXQgdG90YWwgPSAwO1xuICAgICAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgLy8gICAgIGlmIChwbGF5ZXIgIT0gd2lubmVyKSB7XG4gICAgICAgIC8vICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIHBsYXllci5oaWRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgLy8gICAgICAgICAgICAgcGxheWVyLnNob3dIYW5kQ2FyZHMoKTtcbiAgICAgICAgLy8gICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAgICAgLy8gICAgICAgICB9XG4gICAgICAgIC8vICAgICAgICAgbGV0IGxvc3QgPSAxMyAqIHRoaXMuYmV0VmFsdWU7XG4gICAgICAgIC8vICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAgICAgLy8gICAgICAgICBwbGF5ZXIuc3ViQ29pbihsb3N0KTtcbiAgICAgICAgLy8gICAgICAgICB0b3RhbCArPSBsb3N0O1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG4gICAgICAgIC8vIHdpbm5lci5hZGRDb2luKHRvdGFsKTtcbiAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIC8vIHRoaXMub25XaW4odG90YWwpO1xuXG4gICAgICAgIC8vIHJldHVybiB0aGlzLnRvYXN0LnNob3coYFThu5VuZyBjbW4ga+G6v3QgcuG7k2lcXG4gJHtyZXN1bHRTdHJpbmcuam9pbignXFxuJyl9YClcbiAgICB9XG5cblxuICAgIGNhbENvaW50RW5kR2FtZSA9IChwbGF5ZXI6IFBsYXllcikgPT4ge1xuICAgICAgICBsZXQgZGVhbGVyID0gdGhpcy5wbGF5ZXJzWzBdXG4gICAgICAgIGxldCBwbGF5ZXJDb2luID0gMFxuICAgICAgICBpZiAocGxheWVyLmJvbnVzVHlwZSA9PSAyKSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luIC09IHRoaXMuYmV0VmFsdWUgLyAyXG4gICAgICAgIH0gZWxzZSBpZiAocGxheWVyLnBvaW50ID4gMjEpIHtcbiAgICAgICAgICAgIHBsYXllckNvaW4gLT0gdGhpcy5iZXRWYWx1ZVxuICAgICAgICAgICAgLy8gcGxheWVyLnN1YkNvaW4odGhpcy5iZXRWYWx1ZSlcbiAgICAgICAgfSBlbHNlIGlmIChwbGF5ZXIuY2hlY2tCbGFja0phY2soKSkge1xuICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmJldFZhbHVlICogMS41XG4gICAgICAgIH0gZWxzZSBpZiAocGxheWVyLnBvaW50ID4gZGVhbGVyLnBvaW50KSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luICs9IHRoaXMuYmV0VmFsdWVcbiAgICAgICAgfSBlbHNlIGlmIChwbGF5ZXIucG9pbnQgPT09IGRlYWxlci5wb2ludCkge1xuICAgICAgICAgICAgcmV0dXJuIDBcbiAgICAgICAgfSBlbHNlIGlmIChkZWFsZXIucG9pbnQgPiAyMSkge1xuICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmJldFZhbHVlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luIC09IHRoaXMuYmV0VmFsdWVcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgvMS8udGVzdChwbGF5ZXIuYm9udXNUeXBlLnRvU3RyaW5nKCkpKSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luICo9IDJcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGxheWVyQ29pblxuICAgIH1cblxuICAgIGFzeW5jIHNldEN1cnJlbnRUdXJuKHBsYXllcjogUGxheWVyLCBsZWFkOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMucGxheWVycy5tYXAocCA9PiBwLnNldEFjdGl2ZShmYWxzZSkpXG4gICAgICAgIGlmICghcGxheWVyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zaG93UmVzdWx0KClcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnR1cm4gPSBwbGF5ZXIuc2VhdDtcbiAgICAgICAgcGxheWVyLnNldEFjdGl2ZSh0cnVlKTtcblxuICAgICAgICBpZiAocGxheWVyLmlzVXNlcigpKSB7XG4gICAgICAgICAgICByZXR1cm4gR2FtZS5zbGVlcChHYW1lLldBSVRfUkVfU0hPV19VU0VSX0FDVElPTikudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKHRydWUpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGNhbkhpdCA9IHBsYXllci5wb2ludCA+IHRoaXMucGxheWVyc1swXS5wb2ludFxuXG4gICAgICAgIGlmIChwbGF5ZXIucG9pbnQgPCAxOCkge1xuICAgICAgICAgICAgY2FuSGl0ID0gdHJ1ZVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFjYW5IaXQpIHtcbiAgICAgICAgICAgIGNhbkhpdCA9IHRoaXMucGxheWVycy5maW5kKHAgPT4gcGxheWVyLnBvaW50ID4gcC5wb2ludCkgIT0gdW5kZWZpbmVkXG4gICAgICAgIH1cblxuICAgICAgICBpZiAocGxheWVyLnBvaW50ID49IDE4KSBjYW5IaXQgPSBmYWxzZVxuXG4gICAgICAgIGlmIChwbGF5ZXIuY2FyZHM/Lmxlbmd0aCA9PT0gNSkgY2FuSGl0ID0gZmFsc2VcblxuICAgICAgICBhd2FpdCBHYW1lLnNsZWVwKEdhbWUuV0FJVF9CT1QpO1xuICAgICAgICBwbGF5ZXIuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgLy8gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGlmIChjYW5IaXQpIHtcbiAgICAgICAgICAgIHRoaXMudGFrZUNhcmQocGxheWVyKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy50b2FzdC5zaG93KHBsYXllci51c2VybmFtZS5zdHJpbmcgKyAnIHN0YW5kJyArIHBsYXllci5wb2ludCArIFwiIMSRaeG7g21cIilcbiAgICAgICAgICAgIHRoaXMubmV4dFR1cm4oKVxuICAgICAgICB9XG4gICAgICAgIC8vIH0sIDI1MDApO1xuXG5cbiAgICAgICAgLy8gaWYgKCFwbGF5ZXIuaXNCb3QoKSkge1xuICAgICAgICAvLyAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKHRydWUpXG4gICAgICAgIC8vIH0gZWxzZSB7XG5cbiAgICAgICAgLy8gaWYoKXtcblxuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gfVxuXG5cblxuICAgICAgICAvLyBpZiAodGhpcy50dXJuID4gMCkge1xuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgICAgICBjYy5kZWxheVRpbWUoMS41KSxcbiAgICAgICAgLy8gICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLmV4ZWNCb3QsIHRoaXMsIHBsYXllcilcbiAgICAgICAgLy8gICAgICkpO1xuICAgICAgICAvLyB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgdGhpcy5zaG93RGFzaGJvYXJkKGxlYWQpO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgZ2V0SW5Sb3VuZFBsYXllcnMoKTogQXJyYXk8UGxheWVyPiB7XG4gICAgICAgIGxldCBwbGF5ZXJzOiBBcnJheTxQbGF5ZXI+ID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmdldFRvdGFsUGxheWVyKCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKHRoaXMucGxheWVyc1tpXS5pc0luUm91bmQoKSlcbiAgICAgICAgICAgICAgICBwbGF5ZXJzLnB1c2godGhpcy5wbGF5ZXJzW2ldKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGxheWVycztcbiAgICB9XG5cbiAgICBnZXROZXh0SW5Sb3VuZFBsYXllcigpOiBQbGF5ZXIge1xuICAgICAgICAvLyB0aW0gdGhhbmcgbmFvIGNvIGdoZSBuZ29pID4gdHVybiB0cmMgdmEgZGFuZyBvIHRyb25nIHZhbiBjaG9pXG4gICAgICAgIGxldCBuZXh0VHVybiA9IHRoaXMucGxheWVycy5maW5kKHBsYXllciA9PiB7XG4gICAgICAgICAgICByZXR1cm4gKHBsYXllci5zZWF0ID09PSB0aGlzLnR1cm4gJiYgcGxheWVyPy5zdWJVc2VyPy5pblJvdW5kKSB8fCAocGxheWVyLnNlYXQgPiB0aGlzLnR1cm4gJiYgcGxheWVyLmlzSW5Sb3VuZCgpKVxuICAgICAgICB9KVxuXG4gICAgICAgIGlmICghbmV4dFR1cm4gJiYgdGhpcy5wbGF5ZXJzWzBdLmluUm91bmQpIHJldHVybiB0aGlzLnBsYXllcnNbMF0gLy8gbmV1IGtob25nIGNvbiBhaSB0aGkgY2h1eWVuIGx1b3QgY2hvIG5oYSBjYWlcblxuICAgICAgICAvLyBsZXQgbmV4dFR1cm4gPSB0aGlzLnBsYXllcnNbdGhpcy50dXJuICsgMV1cbiAgICAgICAgLy8gaWYgKCFuZXh0VHVybiAmJiB0aGlzLnBsYXllcnNbMF0uaXNJblJvdW5kKCkpIG5leHRUdXJuID0gdGhpcy5wbGF5ZXJzWzBdXG4gICAgICAgIC8vIGlmICghbmV4dFR1cm4uaXNJblJvdW5kKCkpIHJldHVyblxuICAgICAgICByZXR1cm4gbmV4dFR1cm5cbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IDE7IGkgPCB0aGlzLmdldFRvdGFsUGxheWVyKCk7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IG9mZnNldCA9IHRoaXMudHVybiArIGk7XG4gICAgICAgIC8vICAgICBpZiAob2Zmc2V0ID49IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSkgb2Zmc2V0IC09IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKTtcbiAgICAgICAgLy8gICAgIGlmICh0aGlzLnBsYXllcnNbb2Zmc2V0XS5pc0luUm91bmQoKSlcbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gdGhpcy5wbGF5ZXJzW29mZnNldF07XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgY2FsY3VsYXRlUG9pbnQ6IChwbGF5ZXI6IFBsYXllcikgPT4ge1xuXG4gICAgfVxuXG4gICAgYXN5bmMgdGFrZUNhcmQocGxheWVyOiBQbGF5ZXIsIG5lZWROZXh0ID0gdHJ1ZSkge1xuICAgICAgICBsZXQgY2FyZCA9IHRoaXMuZGVjay5waWNrKCk7XG5cbiAgICAgICAgdGhpcy50b2FzdC5zaG93KHBsYXllci51c2VybmFtZS5zdHJpbmcgKyAnOiAnICsgcGxheWVyLnBvaW50ICsgXCLEkSBi4buRYyB0aMOqbVwiICsgY2FyZC5yYW5rKVxuICAgICAgICBpZiAoIWNhcmQ/Lm5vZGUpIHtcbiAgICAgICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLkxBVEU7XG4gICAgICAgICAgICB0aGlzLnNvcnQubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAvLyAgICAgY2MuZGVsYXlUaW1lKDIpLFxuICAgICAgICAgICAgLy8gICAgIGNjLmNhbGxGdW5jKHRoaXMuc2hvd1Jlc3VsdCwgdGhpcywgcGxheWVyKVxuICAgICAgICAgICAgLy8gKSk7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBwbGF5ZXIucHVzaChjYXJkLCAwKTtcblxuICAgICAgICBpZiAoIW5lZWROZXh0KSByZXR1cm5cbiAgICAgICAgLy8gY29uc3QgYXVkaW9DbGlwID0gY2FyZHMuaGlnaGVzdC5yYW5rID09IDE1IHx8IGNhcmRzLmNvdW50KCkgPiAxID8gdGhpcy5hdWRpb0ZpcmUgOiB0aGlzLmF1ZGlvRmlyZVNpbmdsZTtcbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSgwLjE3KSxcbiAgICAgICAgLy8gICAgIGNjLmNhbGxGdW5jKCgpID0+IHV0aWwucGxheUF1ZGlvKGF1ZGlvQ2xpcCkpXG4gICAgICAgIC8vICkpO1xuXG4gICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoMClcblxuICAgICAgICBpZiAocGxheWVyLmlzQm90KCkpIHtcbiAgICAgICAgICAgIC8vIHBsYXllci51cGRhdGVDYXJkQ291bnRlcigpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGxheWVyLmNhcmRzLm1hcChlbCA9PiBlbC5zaG93KCkpXG4gICAgICAgIH1cbiAgICAgICAgcGxheWVyLnNldEFjdGl2ZShmYWxzZSk7XG5cbiAgICAgICAgbGV0IF9wbGF5ZXIgPSBwbGF5ZXJcblxuICAgICAgICBpZiAoIXBsYXllci5pblJvdW5kICYmIHBsYXllci5zdWJVc2VyKSB7XG4gICAgICAgICAgICBfcGxheWVyID0gcGxheWVyLnN1YlVzZXJcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfcGxheWVyLnBvaW50ID49IDIxIHx8IF9wbGF5ZXIuY2FyZHMubGVuZ3RoID09PSA1KSB7XG4gICAgICAgICAgICBfcGxheWVyLnNldEluUm91bmQoZmFsc2UpXG4gICAgICAgICAgICBfcGxheWVyLnBvaW50ID4gMjEgJiYgdGhpcy50b2FzdC5zaG93KF9wbGF5ZXIudXNlcm5hbWUuc3RyaW5nICsgYCBUaHVhOiAke19wbGF5ZXIucG9pbnR9IMSRaeG7g21gKVxuICAgICAgICAgICAgLy8gYm90IHN0YW5kXG4gICAgICAgICAgICBfcGxheWVyLnNldEFjdGl2ZShmYWxzZSk7XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLm5leHRUdXJuKClcbiAgICAgICAgfVxuICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHBsYXllci5jYXJkcy5tYXAoZWwgPT4gZWwubm9kZS5uYW1lKSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgLy8gdGhpcy5jaGVja1dpbigpXG4gICAgICAgIC8vIHRoaXMubmV4dFR1cm4oKVxuXG4gICAgICAgIGlmICghcGxheWVyLmJvbnVzVHlwZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRDdXJyZW50VHVybihwbGF5ZXIsIGZhbHNlKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGxheWVyLnNldEluUm91bmQoZmFsc2UpXG4gICAgICAgICAgICB0aGlzLm5leHRUdXJuKClcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHNldFRpbWVvdXQodGhpcy5uZXh0VHVybiwgMTUwMCk7XG5cbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSguMTUpLFxuICAgICAgICAvLyAgICAgY2MuY2FsbEZ1bmModGhpcy5uZXh0VHVybilcbiAgICAgICAgLy8gKSk7XG4gICAgfVxuXG4gICAgY2hlY2tXaW4gPSAoKSA9PiB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5wbGF5ZXJzKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG4gICAgICAgICAgICAvLyB0aGlzLnBsYXllcnMuXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZXhlY0JvdChzZW5kZXI6IGNjLk5vZGUsIGJvdDogUGxheWVyKSB7XG4gICAgICAgIC8vIGFsZXJ0KDEpXG4gICAgICAgIGxldCBjYXJkcyA9IHRoaXMuY29tbW9uQ2FyZHMuaXNFbXB0eSgpXG4gICAgICAgICAgICA/IEJvdC5yYW5kb20oYm90LmNhcmRzKVxuICAgICAgICAgICAgOiBCb3Quc3VnZ2VzdCh0aGlzLmNvbW1vbkNhcmRzLCBib3QuY2FyZHMpO1xuXG4gICAgICAgIGlmICghY2FyZHMpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9uRm9sZChib3QpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5vbkZpcmUodGhpcy5wbGF5ZXJzW3RoaXMudHVybl0sIGNhcmRzLCB0cnVlKTtcbiAgICB9XG5cbiAgICBnZXRUb3RhbFBsYXllcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGxheWVycy5sZW5ndGg7XG4gICAgfVxuXG4gICAgZ2V0UHJvZmlsZUJvdCgpOiBQcm9maWxlIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCAxMDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcm5kID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy5wcm9maWxlQm90cy5sZW5ndGgpO1xuICAgICAgICAgICAgaWYgKCF0aGlzLmlzVXNhZ2UodGhpcy5wcm9maWxlQm90c1tybmRdKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnByb2ZpbGVCb3RzW3JuZF1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5wcm9maWxlQm90c1swXVxuICAgIH1cblxuICAgIGlzVXNhZ2UodXNlcjogUHJvZmlsZSkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKHRoaXMucGxheWVyc1tpXS51c2VybmFtZS5zdHJpbmcgPT0gdXNlci51c2VybmFtZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBvblNvdW5kVG9nZ2xlKHNlbmRlcjogY2MuVG9nZ2xlLCBpc09uOiBib29sZWFuKSB7XG4gICAgICAgIENvbmZpZy5zb3VuZEVuYWJsZSA9ICFzZW5kZXIuaXNDaGVja2VkO1xuICAgIH1cblxuICAgIGNsYWltQmFua3J1cHRjeU1vbmV5KGJvbnVzOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgbXNnID0gY2MuanMuZm9ybWF0U3RyKExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdNT05FWTEnKSwgdXRpbC5udW1iZXJGb3JtYXQoYm9udXMpKTtcbiAgICAgICAgdGhpcy50b2FzdC5zaG93KG1zZyk7XG4gICAgICAgIEFwaS5jb2luSW5jcmVtZW50KGJvbnVzKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0Q29pbihBcGkuY29pbik7XG4gICAgICAgIHRoaXMucG9wdXAuY2xvc2UobnVsbCwgMyk7XG4gICAgICAgIC8vIHVwZGF0ZSBiZXQgcm9vbVxuICAgICAgICB0aGlzLmJldFZhbHVlID0gTWF0aC5yb3VuZChBcGkuY29pbiAqIDAuMyk7XG4gICAgICAgIHRoaXMuYmV0VGV4dC5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdCh0aGlzLmJldFZhbHVlKTtcbiAgICB9XG5cbiAgICBpbnZpdGVGaXJlbmQoKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuSU5WSVRFX0ZSSUVORCk7XG4gICAgICAgIEFwaS5pbnZpdGUoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShDb25maWcuYmFua3J1cHRfYm9udXMpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDMpO1xuICAgICAgICAgICAgdGhpcy50b2FzdC5zaG93KCdN4budaSBi4bqhbiBjaMahaSBraMO0bmcgdGjDoG5oIGPDtG5nJyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFkUmV3YXJkKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fQ0xJQ0spO1xuICAgICAgICBBcGkuc2hvd1Jld2FyZGVkVmlkZW8oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShDb25maWcuYmFua3J1cHRfYm9udXMpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDMpO1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShBcGkucmFuZG9tQm9udXMoKSk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fRVJST1IpO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIGFzeW5jIG9uU2V0Qm9udXNDbGlja2VkKGUsIGJvbnVzKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgY29uc29sZS5sb2coYm9udXMpO1xuICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG5cbiAgICAgICAgaWYgKGJvbnVzID09IDMpIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNwbGl0KHRoaXMuc2VhdHNbNl0pXG4gICAgICAgICAgICB0aGlzLnNlYXRzWzZdLnNob3coKVxuICAgICAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKClcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoR2FtZS5ERUFMX1NQRUVEKVxuICAgICAgICAgICAgLy8gdGhpcy5zZXRDdXJyZW50VHVybih0aGlzLm15c2VsZiwgZmFsc2UpXG4gICAgICAgICAgICAvLyB0aGlzLm9uSGl0Q2xpY2tlZCgpXG4gICAgICAgICAgICB0aGlzLnRha2VDYXJkKHRoaXMubXlzZWxmLCBmYWxzZSlcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoNTAwKVxuICAgICAgICAgICAgdGhpcy50YWtlQ2FyZCh0aGlzLm15c2VsZi5zdWJVc2VyLCBmYWxzZSlcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoMTAwMClcbiAgICAgICAgICAgIHRoaXMubmV4dFR1cm4oKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5teXNlbGYuc2V0Qm9udXNUeXBlKGJvbnVzKVxuICAgICAgICAgICAgaWYgKGJvbnVzID09IDEpIHsgLy8gZG91YmxlXG4gICAgICAgICAgICAgICAgdGhpcy5vbkhpdENsaWNrZWQoKVxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm15c2VsZi5pblJvdW5kKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlzZWxmLmluUm91bmQgPSBmYWxzZVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlzZWxmLnN1YlVzZXIuaW5Sb3VuZCA9IGZhbHNlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uU3RhbmRDbGlja2VkKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgLy8gb25TZXRCb251cyA9IChib251cykgPT4ge1xuICAgIC8vICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGJvbnVzKTtcbiAgICAvLyAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgIC8vICAgICAvLyB0aGlzLm15c2VsZi5zZXRCb251c1R5cGUoKVxuICAgIC8vIH1cblxuICAgIC8vIGNoZWF0XG4gICAgY2hlYXRfd2luKCkge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgdGhpcy5zaG93UmVzdWx0KG51bGwsIHRoaXMubXlzZWxmKTtcbiAgICB9XG5cbiAgICBjaGVhdF9sb3NlKCkge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgdGhpcy5zaG93UmVzdWx0KG51bGwsIHRoaXMucGxheWVyc1sxXSk7XG4gICAgfVxuXG4gICAgc3RhdGljIHNsZWVwID0gYXN5bmMgKHdhaXQ6IG51bWJlcikgPT4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKDEpO1xuICAgICAgICAgICAgfSwgd2FpdCk7XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IFdBSVQ6IG51bWJlciA9IDA7IC8vIHdhaXQgZm9yIHBsYXllclxuICAgIHN0YXRpYyByZWFkb25seSBERUFMOiBudW1iZXIgPSAxOyAvLyBkZWFsIGNhcmRcbiAgICBzdGF0aWMgcmVhZG9ubHkgUExBWTogbnVtYmVyID0gMjsgLy8gcGxheVxuICAgIHN0YXRpYyByZWFkb25seSBMQVRFOiBudW1iZXIgPSAzOyAvLyBsYXRlXG4gICAgLy8gc3RhdGljIHJlYWRvbmx5IERFQUxfU1BFRUQ6IG51bWJlciA9IDAvLzEuNjY7XG4gICAgLy8gc3RhdGljIHJlYWRvbmx5IENBUkRfU1BBQ0U6IG51bWJlciA9IDQ1O1xuICAgIC8vIHN0YXRpYyByZWFkb25seSBXQUlUX0JPVDogbnVtYmVyID0gMDtcbiAgICAvLyBzdGF0aWMgcmVhZG9ubHkgV0FJVF9SRV9TSE9XX1VTRVJfQUNUSU9OOiBudW1iZXIgPSAwO1xuXG4gICAgc3RhdGljIHJlYWRvbmx5IERFQUxfU1BFRUQ6IG51bWJlciA9IDEuMi8vMS42NjtcbiAgICBzdGF0aWMgcmVhZG9ubHkgQ0FSRF9TUEFDRTogbnVtYmVyID0gNDU7XG4gICAgc3RhdGljIHJlYWRvbmx5IFdBSVRfQk9UOiBudW1iZXIgPSAyMDAwO1xuICAgIHN0YXRpYyByZWFkb25seSBXQUlUX1JFX1NIT1dfVVNFUl9BQ1RJT046IG51bWJlciA9IDEwMDA7XG5cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/image.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52ca30fTzNLN67UCDz74xRK', 'image');
// Scripts/image.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var image_base64 = "data:image/jpeg;base64,/9j/4QlQaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MCA3OS4xNjA0NTEsIDIwMTcvMDUvMDYtMDE6MDg6MjEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+0ALFBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/bAIQABAMDAwMDBAMDBAYEAwQGBwUEBAUHCAYGBwYGCAoICQkJCQgKCgwMDAwMCgwMDQ0MDBERERERFBQUFBQUFBQUFAEEBQUIBwgPCgoPFA4ODhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/90ABABa/+4ADkFkb2JlAGTAAAAAAf/AABEIAhwC0AMAEQABEQECEQH/xADCAAACAgMBAQEAAAAAAAAAAAACAwABBAYHBQgJAQEBAQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgQAAEDAgUCBAMFBAYFCgUDBQEAAhEDBAUGEiExB0ETIlFhMnGBCBQjQpEVUqGxFiQzYnLBGCWSstEXNENUZHN0dYLhRFNVY6ImNUU2ZYPw8REBAQACAQQAAwYDBwIFBAMAAAECEQMEEiExBSJBBhMyUWFxI0KBFDNScpGhwQexJDTR4fAVJUNiNYKy/9oADAMAAAERAhEAPwD3aVPRwvzD7g5dq9UDGcme6qBqNa1st5KxW4Gi1m57pFrLot0haQdQwECHPbPG/ZA2j6+igzJnSjIH6Q/U/dBVNul+tnBVx9jIOwPut5DDqNdMjhcPq3D6YcYW4zTyNgR2VSBrOGxPCNF0nNLhsjOXpmue1zIjsqzCwKfflRuKaG6+VIU5jNRW450RlhgqoLdpEd0FGmA4kd9ys1uLa6JB7qKaxsSTwQrGaa+jOktO/K0yogjzE7HZAvSBUkd0DRTLgghDggX5fEAd3QTw4qEsO3Zc77bnpTTLi0iXStRTGguIEbjhaZyN0OaZcZB4RlHtLthygEsJ3J2CA2AkGe3CCgS0oI1AzQ7dzTwEAMB0z37oDpk6TPKsAuaZb8lQstl2kd+VKGNYGGD8Kgt3hwdkCNGkkkRPCB58zQ4cjlATfhatMBLN9PpuiqqND90agQ0kaRwEAiGuUovTvPqoC0koFuGmoGlALxpHl+LsgNsafPueyCHy7/mQIeXd0BU/hKBZa48FBNJaN0DBuPkgrymQ7sgQ94J0ws5LBtpgiVG1PB2A9VsLNM6lgE50HSgjhLZ9FtSpndBYaHDdBTmNYCTyeECdMCPqpUU1kuhZFvp9iVYFvaGvC0oS7dBHebhALmwNkCi1xQKeTGkoFljI3UoWdDTxyoqxo5I2RnJTnMBkco5v/9DYaUjlfmH3DHNE7coAcTMHgKoF0ESsVuBpA6pHCYrWax8CCNlpFuqe3KBbmB24G/qgOkCJb2Kgy2gx7oyx9UP0OQZFAwC3tKuPsMeTC3kMcudqAPw91wbjKp+g4W4zRyAYPB5VqQFVsiRwjSqTAN+6Jl6ZTYAhViE3DTqGgwo3EpluoSZPdSFZDC5rtU+X0W45015ZUAKqKkQGjsggDgDPKzW4mglmvvPKimiC0AqxmmyS2JWmSqhOj37IINJcJG4QOa4g+yAnGeUC3Uw4h/ogpwa4ksMEcrnfbc9KpFgeCOZ3K1FZb2zJb9IWmcg6DAJ57oyW4kAkcoCYA5oHrygNwLQACgEgncndBKbS6R39UBgFrTHPdAuajY08IGgyJI3PKsEJ22+SoU4aXEt5UoLzFs91BAQWwR5vVANV7nRvxwgukIB9+UDWloG42C0yFzxufzdkAgCJ7lGojARMDZAktOvUTAnhShhI1ADgKAiYAPugW8te7URwgW8Et1ILplyAnBzgTG4QKeZ8rtz2QUxrgY/L3QR9OXEM2hBWlwadRmEBs06RA3PKCNDS47IE1WAO42WclitRaDCjZbqzmUy4fEFoUyqXNa52xPKyCgPdMIDLZkB2y2FFukH2RVtaY1N4QLrNdUI9kA9jt5h3UqFtJEzysgHOdIMqwU4Fx1HlaVGNa8wRugJ7N9uEE0cIBeWjaNwgxns1EmECIiQQpQp3ExuoomNLmS7hGcglkztujm//0djaREr8w+4ttQTugN0P3aJQ0Q7YQpY1Eo6huPXhJBm0wXxIiFQ14pgbhAlx2gCEDbZzTII37IHgoyxa1OXSDus7a0zKDWtpgkqzwyJ8EbcLVuxjP327rlfbcZFHVABHA5W4zTQ0PHO6tSEg7EOMRwjQqPxST9ESsl7RGzk2zop0EiT7KNB8CKgLTMdkhWQ2eCtxzpzAwCITa6RwgyAps0gcQII3KLIvV5SzsO6ijbJAAC1jGaJxgRMFVkLHtdsRt3KzsFLQ7UrsO1Mc3iCtSAHeyibUdUQDCztrQA0AwDJPKjamjSS0DnuqMukCGxMwqzkZuBJ4PCrJTwDMFQA3yt0jf3QOaQ4QRytaAvLWAy3hQEyo0Alu5PdBcHSgBrtJDef8kDQ4HYCStQU8AjZS0L2LiCYPZZtBtnSRHCCNgtO0OWtBWk/H/BQGzdA5rN4IlaZDVpEGeyAHQBtwjULJfpGn9EAPBLQXcqUG0AjVwVA0AFh3QKIDB6ygTVlrdtwoJQc54JDYhNg26xLSee61IAcyBud/VQUwEcboCd5WSdnd0AA6mmSgJgAaEFOaeWoEukugrNWLYIqAOGyja3sa5sAd1djHqUXyHH4R2UBNA08oLAgTyVsW9ss2PP8ABFKaXDZp2QGCQ4akS0Lw0uPZRJdsaoDMppVBuvYFNA2t2gnfsqpbmOa6BtPdBCXAaSN0E32QU5vdADgIlAoUwSZ7rNCnUxuPTuobADHl7Il8qnSfVGdP/9LZyIHwr8w+4jWtcNwgkmmSG/CjUJqb7tQHbRJBQZrTB42QW9oeJlAhzXwYG4QMpNOx790Snbt8x2CVmAO7lzrpD6bS5mnutsDcNLY9EGMeZCzfbcZFJ5iSNuFqM00Ahuocq1MSXw8gj4u6NDDRIPdQrJ0FwB7d0ZJqMmS3cBBdF5Dt0i1maARIW451fhgDUOVGoAguAgqKjQZIPpsgJoMEu5QPpt2BmJ4W8WaOo1rgW9/VWsscN0gt9VzEbBdpPZWDIYNJkiGrpBHNIGocLNZADO6xXSKADH6+x5SNC0gukd90DQxxBMxC1GaYA4gA+khVkOkAB3Z2wUoXT+MtdyEgd8JjuN1sLc0vB1LIYxrWskCQgPV5UCSPxJHflAxo0HZanoW4iOVihTdnF0TCyGUp3ee+0LUFuYRLmrd9BTXjTDlkGB6IGBrwZnZaZU57mnfzNPqgAQ8nTwjUARt5Tv6IA0zxv6qUWxriSANlBbS7UR2j+KC6jQWgFAio1xaW9ipQNMeGyAd1IDd5xsd10gp7g7busi2M9CgJ2+0SRygWWggkbII0iIHblAUkbASSgWWw6TypVgXuLjP6LLaUg4tcHfRAupr0xOyAQ0AQTugjf3T6rYupqbxwUUoeQyeEU5wa9uodtkYoHMaQCDJ7omLHcHbiNkbKB8M7IGTMEHdAVU8OAlAvVLpeId6II/zbtQCQSECnT9EFOAjUO2yzUKeC4SdlAnTpdM7IBduRCD//09ppvEeZfmH3EJDvhQLcz1O6NFEOaY/J6oG0nMafL35QZrHB20coBcSx4A4QAXuFXSRsUDqZBBA7oUbvOzQOQlYhAOh+6510jPpOAErbCP3BPqgxmAB2/CzfbcZVGlIPuZWozTHCGlvorUjDGxKNMikNTZ9FCnU3BwLXfD3RkhzfCq6WmWlBkUGtc5ItZABcCAtxzqN/cPbZRqCAAEKKBrCHygZ4Qe5pmAOUDhTbqgH5fNbxZqmN0OcCZVrIHOaHAkbLmGaA5wc3lWC++mpsukBNEeUrLJb6ZEkLFdIXBDTPdI0ZTaQASgPxC2R7LUZonVm02tLu+yrIXOElo7cfVSgrdzXktPxDlIGOG2v6LYBYBU/K1rTwqHOAbueECnAE6hwUBtY125K1PQXVbBGlYoUGu1e3dZGVT08RstQU8mfbj9VuhTmaXhvblZBM+Ij0QM1eVaZQkO5QKO1QaUaQgSUFNBAMDupQDHu8QyFAxpY6Z5QKdDdu6BdQntypRdEn8wUgM6TwukC3t0nZZFtMBBRqAbnugFzvLI7oKFMkBw47oGO8rt0C6jgTClWFn4QstmUXiC08kR+qBLzodpKBZDdXKC2/EtqN+4I9ECXHyR3lFExp0xPKMUO7XaUTEmo7S6PVGwVAGwURYBdEIogY8pQU4OjVCATs35oFtl0+iCtBB52QU+NBhZqFOnSIUCagc7b0QLI0j3Qf/9TaaZavzL7wHy10oKMnzDkqLC6heRpKKKhTbMnlErPHlCIheADKzVhAqjVsN1IlZdEBwJPK2xVuYVCMYhpd/eVdGZQaabIduiDdAZt37IMeiCHQeVBm0zBlHOreyQXKriwyJfHcbo2zKDoZ5voiU0FrRxylSMZ7vMudbPoLcYPOzmgcLcDqlMPaY7FQBGlzQoLOoOlvKC2CXaXd9ygfyPNyOFYzVEmI7LTIS2Q30UoOmdLoHCkB1C30XSAYCVmFVQWEHlpXOukTTTeyW8qNKgsa1p33lAxoDzC3izTxTaQB3G60yBzAefiUopjXNeCFA6Q4QeeyCBjfzIJDRp0oLc+Dq+iBJDnSJ2lA5oLAB9UA+pKBTqYPmJj0QOaSYAMwEBlmse4QLJcDHYILZsS7sgNhpn4lplCTO3HZBACXb/RGoVrIJ1eqCtWky3upRZpjZw5PKgg22QC4QY9UCqoOgFSiqRPfhSBpLR8IXSBZElZACW1YKBzw0iAgx3DR8igJoIEt4PKAtXiS2N0C3N0DdSrAOeADIn2WWxtawgObsUC6jS4x+VbYhTqYPmR0gmNEakEJaJd6oEVC3Us0U1waZCKa78Ruy1ilY5BALe6VCtx/aDZYUTYkFphEoizXyVWBAfhaVp0YztRMIDLSAI57oFkAGAgvQ1zDPKBZ8sNQKexkz3KBFSiCg//V2WmJZqHK/MvvDc4PbBG4QACWAeqiwNSpKKOk3V5h2RKytXk4k9kRRGpu23qs1Yxy4sepErNpOLN52IW2KyC5rmyNlCMN1Nniamnf1VdGdTGoAt2A590RVXTy3lAFLS6oZUGQxh1Qdx2Rzq3PIJbGyq4sV+kPkHdG2XRAdCJT/K8RHCVIxqlMa9lzrZ1IAcLcYPaRG7eFuBjXAiBtPKgF4gg9/VQEx0DUeUFkfnQGXa3COVYzTHAaYWmSgXfm+EcKUMpjT5juDwpA4APYT+ZdIAcxoEgbpWSqjRokHdc66QsyxrQ36qNGNa2oQPRAYpgO9FvFmmjTPqVpkD9nSpQVJ2xJEqC3Q3ePkgrUC0l25QGym4t1AbDsgoNJMPECUFFgJMcIGt4hABaTPogWHSId2OyB7HNA9Twgo7AmUCttP97ugNgkboDbSDgYMLTIXOIIAE+rkEL3F7QBA/e9UagatMnzzIKBb/Lp0jlShg1KCgPNugCpqmRuUCnvD2Bo2cOVBbAIiVIGCW87rpAJbp3WQqoS5wdG47oDDwWiRv6oFPM7dkEZzHZAzg7II9use6lWElukrLaQ2UEc4THZbYhG7hHZHSLptIBB4QQ0g6ROw4CDGewA8ys0VGoaQ2D6orIpNDG6XGStYpSarS2pqaZb6JULcHVDLogLBCwGEx3CFG4CJCrFQOfG53WnRGATJO6Bjmtjy8oE+HIj8yAA3n1CBbtyZ39ECXN3nhAt0+s+yD//1tmYdJB7HsvzD7uzXaZBGyGw1A0j5I1GPAJRWXRI0ER3RKdSc1pOoTPCIF/lO3BKlgQQHOg9u6mis2kzaSZ9lpnQ3RpgKLIx2hpdPJVaZzfKABwQjHcB7SCT2KzauwU6ZkuBgqwZJp1YDmO+arOluJY0B3KLIxnaC6Y3RplUgA0RsfVEpzmwwOafmlSMd5IO2651s2mXTEbrcYZTjGy3BYaSBCgmmW7ndQC1wB3/AEQPgVGxMIDY0bD07rWkyVVn8u6rAQdQgBSgqTt9LhsFIMhgkmNpXSAX8QeVm5L2lloI0x9VlrSnwABCiq0HYtMIGkGGk8hbxZotIjUNitMhme3sgZRLJLSpoGR5o5BTQW9gAIUDKb3t2iR6LXaJUcHcrIW1omAUDGthxkrUibDqcXERtwpYsCaYkT2U0thmlrQCPlCukBWa7bTx6JoK3DpP6KBragG0IGNaNJ091plQ2MRt3QXUBDhHB4CLsDIgtJn2Q2U4OBnsCppTGvDiJ2jlNCttyOAmhdQEAEclNDHcxvin+KlgjmtG7fMVmAm1QOQukELxEkKWBetu4hZBANLI/igTpgxKoto7oDaZJQQHzKLCapJdCabA2ZG2yaBuj4o+iqaJLgTDUaEHADSeUEIIBPMoMeoGxvs5ZoW0unnZQ2y2BhG/K1KAcGlh+aqMYsDTpnlZ0FOpgGQoGNLdIkqs6XVaB5mmVo7ggzAIQ7gkODjHCHchBO8wUO5YHqh3FkAOModwKuggfNIdxBa2duVdHc//19oe2ANuF+YfcGWhwBb9UAQN5KNwh+x8qKzaLQW+6JTAAD6ogX0yXSDKBQB1lBl0w6B6ILLX64PCBbaRZU9kGe0tI91XImoSHEOMDsudbhlBmxI3HYrWPpaawuA0niVUVVbrJgyB9EWMbQdUDcorLpN2hyJTWuDm6P1SpGHUeGPIJ+S51tl25luv83otxg2d5PC3AxomSCoBDwDBKgmxOwlQOaDEAQtQGGvbyNitJkt7mtEIwUCBJbv6qUNYzUJHKkDWy1u/xLpAFWmXecFcnQsOcBJQFIJg8oCB7BA0tc4SBwt4s0IcAIK0yvSAdX5UE8OJd+iAmu8w9EFvBILhwswE2YmNl0grw9TSVzoW1pAkcoGMJO55W56ZqVGiJB3UrcA2Ilx+SkapoZPy5CrCnHSN0oWYJlZFgA9kBEOaZaPKtMpwZKAy4k7DYIEAuDjqEe6AXU36pmZ7IsMDeJECN0VTdLZb+iCqrnADTvCABT8xf27qX0La0Ml/YrEFeQ8BdILPmlmncJQjw5JI4XOgmNb3KsA1QI8vKoATpjugNkR7oJwZPCLCKhgl3YI6KY4Rv34QEzzOjsgUWDVHCCQzVJO4QEXyTHCBFZod5p3WahVMSYUGQ1ugiTstQC6ATHBKqkVmPG4UqEOa8CSVkFp8su2QGA7StuanciOUF7/ogqCUAvdpHugCY2PKCnRxCsCnx6Kj/9DbiHHsvzD7hLpaYQCWndG4VoOqUV6NIANDhwESiLNUvCIVDnb+6gjIdUIQZbRAAVFVHaXhALXgv4QZTdARyDWLTBCxW4Ki7stT0tOaQCZ7qouo1pbsYRYxGtLKkjedlFZzJgEjZVKU/wAtSQdkqQhzGPfJ9VzrbMZSLDqB8q3GGQdOkLcEdpB+agB7AfhUFTMNHIUD2kkADtytQOBIAJ4C0mSPArCR2UYKFPSTKUOpmFIDcukCzqaIO4XJ0VpBbPoUELoIBGx4KCtLqTpG7SgyGawNXY7reLNXrZU8hEFaZA9unyygMNMcoDYzue26AA4y6eFmBrS6dMLpAFRvm2O3dc6KawctO/ogIS0b8rc9M0Bl4LRsVK3FNYSPD7qRrI8DQQwneOO6rCVGT80oDR4XO5PCyKLQfi2KAwS0aC7daYC7hATXkCIRS6gkaygWHyiwYdH1RRCJQW4A9kC439u6l9CBwc3T7rMgp7PRbggLmg+p2SjGc+pTOlw2WKDaNbduyQLH4jo9NlQJYWmEFtaZQMdu3T3RYx3UzBlGyQd49EDC0xqagB586Cnt1OBH1RTNiCAgx6jDHKzUAwOYCRuoGAg791qC4BmVQt0wQe/ClCTTIMn6LIrU4nTCBjeFtzDAGx5PCCDUGkeiChUh0HlBbg17T6hBjFpgPQXILfdWBL6ZA1dlR//R2zW6F+ZfZhZaXGTwo3EcAACOEbU3S58O4RWUwQPbsiUe/ZEKJOrdZqwNFn40nhSJXohoAn0W2PqRX3CNioifiQZLQ2EYIqtGqRysVuG0d9j8S1PSU8sI5MqoVWBDNlnJYGmTrbPCRWWyYMLQVXYwkRyUC9DTDUGWGODdIKMiAmQfiCAmGYJQSpsWkc90FaZdP6oMinv5TweUFkOjQziUZqg19MxO3dVlPKXAl26BoDVoGC13lQKdLTA+FYdFES2RwCgjuQ6eVA2A+N5K1AbQ4/IdluM1ZgHhVlZGtsfVEoqXPyRBFzqZ24KIUX6ncIp1NqAXaSSDsVUQMLIcTITudZR1C1zQQnftnKbpRMQXDZa1sl0yrSzuLp4FKm539792V0x4rWc+R7dplS0o3Au7yo59bTGlejHheTLkem/C8JqDek7Udg8chd/uvDjOTVedfZZika9hUFVrfM5jvigei83JwvTjzNcLHtqOa5ha5uxXhyx1XpmW0a1dGA+E5p2Ox5QVUpElpB7LOl2pu+pr+Snc6THZOgajtxsE7i4sim0RDh8ita2xbobaBe4NY1z3fLZa+6tScunrW+Xry4Ae8ilT/eK648Nc8uZ6lHL2G0gPHc6o/wDu8L0Y8TzZcu4KplzCqrCLdzqTztKuWCYZvBxDBrnDXRUGugfhrDsvNlxvbjyPIeGNdAEu7FefWnbO7JeHa/fus2uePtTRAnuUjd9gawySURYmY7d1KLjfbnuoCbMzwtY+wmqHAyDLXcq0KFOTPZYBnyDUEC3eYakC/MeEdIg1jnhFR7RUCBTQGmEDogAqxmqOwPvuqwU90NBSrAD19Vl0LJcTB+FENp09R5WY5grfEAtwQlwHspQk7uBQWDuQrBbgNO6oXwNuUAPkmEH/0tsZ5m78r8y+zFlvljuo3CSSDpduAjZbSC+IRXpMaAADvKJROaRx3RCHkDdyzVg6Ia4FzfiUiVktOoQTBHZbY+oKgCNipg+qDJpU5BJRgqtTAEt2jusVuDpEytT0lZBgQSqgK8RsJCzksLojWd9o4KRWWGEQWnbutBVamA9xG22x90CWs/EA1c8oM1oDSjJjqY16htPKAS2D5dgEFFwBkjdBQeR5okIHU3OLgeB6IHHY7cFGaEFzXEEzPCrITu4SgfTayNwtCtLWvBBgIKqFuggDdYdC99A0mB3QDyASoH0YA91qDIiRq7hbjNVs6AQqyHUWzAn2RKYwtj0KINzpYCUAADsED2aRygCvSD9+/ZNmgtaNEOMEcFXs2mz6FpWunCnSpk/3uy6Y8NZvLp7dtl6lTAqXj9RHFLtC9vHxPNnyvXYKdNgZSaGMHAC9eOGnmy5Nqc6Bt32K6TTnsOrVseEuUjNlp1Cq6m4FnYyApqWMTKyvIzFYsaxmI0Bpp1TFWPVfN5eN9Thz3GvlsN1DheR6ABrvD3O5QUdNPTIMxv6IgQC4mAS7sAtfd7anJplW+E3948eFRIB5e7YBdMeCsZ88e7b5boUmzd1tbtpY3gL0Y8Onmy5np0aVtbsDaNINA7r04YaebLO1bq7iYn6LrJHPdB8+6t0zqrkAQOFhqMhpp3dI2lw2WPENJ7FZ7Y7TJo2JWL8Puqls7mkefUFfP5cdPfjlt5x8z/kvDPbupoBEkbroBPBUCm6t5KlFjW0yO6gsucTBGxVx9gHtLBB3B4WqADtoHfZYBBsiOyBWkhpA4QAHhvPKOkFq1BFAYDDHPqgTp7zugc10t0nurGclloGzt1WCntY7y6dkqxjv1NG2wHZZdA6y5sIgqZLXT2WY5irCSCFuCAuLQCdvRShRZ5xHCAankcrBZe1zQD2VCwQHCO6CVGukQZ9kH//T2t3EtX5l9qQOp7j7I6SLeGxq7qNFU2jVPugz6ZgeqJTGncyiFvptMyVmrA0BDobt7qRKyJEif1W2Pqj2mJ7I2lMcb8oMnXobA3RNEueC078rOlhtBsRvKsSnvYXt9BPKqEV9TIB+H1WclgKckgDg7SkWsymC0EHj1Wk2GqCQN9jsSiltpHxBvsgy2uHfda0yaASE0KZzpI+qyKLQ8QOQggIaPl2QNpy8Ej9FqRm04zpIIj0Kvaz3bKJJA9QpoSO559E0HNAJidoVEdBI24QQtY6YPHZTTWyzE8x7KaNqgOIhZ+q7WwkOiNgtRLkyhLhtx3W4zclahIaBxyqm1t5JHPohsQbO5H1VZW5vlAnhXSbXTBLg0ceqy3o0NPzPqOFdIqnRuLh/h0mlzuNhsumPG55cmnt2OX9MVr8iW/kG69/HxPJnz6+j12hlv5bdgpt9OV6O2R57yXLyIyQT+Y7zyty6Yqgdo7jurbtNJErOlDss3Df1alW12k7Fbx8M5Y7ZVOlTuqNW0qCadQHSPRyxnj3O3Hl2tLq0alKtUt6gAfTdpLV8i42PodyqVlc3DtFFhfvy0bLWPHazc3qUcs1K4BvX+EBvA3K9M6b9Xny6jV1p61ththZj8OmHkfmK9GPHI43ltZHiuOw2aPyt2Xaajjd0Dpcdzt6BXaaBGl3JgqKhYJUXaPP8FNLsIg91dJsdMkAA7wZCLt5maKWptG/9Rpq/PsvN1GHh6uHk3Wn1PLUnsF8qTVfSs8LaQ6Y2HZarMCBzOygoNgoC1AbR9VNCxBEKzwBrMJHt6qhAZus6BSBymgsu2dATQx3N178EdlGtiYCZQ7hVGw3SdirIdzF8N8+yWHcdsWgRuEiW7E1wDfMPqqgXBhOx3SqU6ByNllqVjvLQZ7I0jXNcIB+qzHI7T5V0gUR6FLBW2xJ3CmgLjrkKgRSjngoKIYJA5QpQD5k8KyJt/9TbaUOEFfmX24Gqwt2ajpCdWluknhRUpOBKDNpuBCJTIkTMIgHDT35WasXQbIJCkSnOGxDu66MfUp2vTEy1Rs6nwD2CBrQ1zXSd0CnBoEHlBk0C07DkcolNqGWwD3RCnnWND+3Czk1AUXOpvjTLUhWZpDgSTAK0xPZTmwwid0bDSl1SCdggzPDBbDfi99ltAU6pDodxwgyHCXam8d1lAgOaSQNlBHU5G3fdA1jW6YBj1W450ekgSDIHK1fTMLc4NIPZZaM0AsLkEYHBsnmUDWt1FAtrdDz6FAJaC4lADfI+DwsX239DG/FEbHdajFZTWNP5oWoyotDXSqKY0kk9lYhzW+6WNQb27Qe6z5a8GUKL6jw2i0uI2kDZd8MHHLN7lvgYO987S3/5bP8AivZjxPHlyvVpMt7Znh2tMNHG/P6r2TikeW8lpTnHUARPrur26Te1yPWVAUiNkAg7qwFwqKDZQixSJ4BKNM2hTNJzXu2A33RSLixw+tdPuyyX1N3D3XL7mV1vIgf4Y00WimzuGjdWccjF5AGpvBPyndac6jnEnf8AgiKHCC/dAs7u27IDBBQU5oPyQBo9EFtBCC72gL3Crm3IkgeI36LlzeY7cN1WgagRDx5ivk2ar62OW4EDztgbDlZouowbnt2UCg4gw4QOyCzDj7ICDRpmUC5IJBOyCuJMILBa9sEeZAqoAAdPfhAho8wB+qyGupgQWnfuEFVAIBJ3W56GOXOktClBNHrt6qQXUbEbbFUVoPMbBKFPae/Cy1GM86tiNkdIoAN2aFmOLJg6fotwJAJcQtUU5kSSoKa2BI78IKJf34QKcAfMESoC5zSHbLUR/9Xa2n91fmX24skv39NijpCKlPxPM3nuoo6TDqhyDKDQxwPZEo3HkjvwiFVnHZZqwy18savh7qRKzXeE5sDk8LbH1Yznhp0kI2fT8PQgEu08cFAqo7cIMq2LQTq2KJWU9rRpI47ohVQ7ksHHKzk1EpuD4kJCnwR8lpiewO9uUbLokeJ5tigyiWu52W0Fpa0NQOHdZRRcQ0tCgpjqgbHZAxjJW4503dgkb+q1fTMBVZTc0O4WWgsimIJmUDGgiSeDwgMNLWygqY8p+IoBDt4fwNkC6jRr247LF9t/Q3ZoDlqMU5p1gELUZEPKd9wVRYEmBwrFh1KhUqODKY1OJ2AXfDHbGd0922wLxA194dLf3QvXjwx48uV69BltbM8Kg0ADuV3nHp57yWoZPK6yacb5U3la7m+1C2TKvtNaCWhYokQoLDNwrBkClPyVF+HRYdbjuOyEEys0GGCPdGgOqazB3QDv6q45FAufJlpiwt/xBSXcWGRO60IRAQQcIKLY3QU1BZ4QU3hBEDrUgOc127XDSfqs5eY3hdNDxm3dZ4jc0QPLqL2fInZfM5Zp9PjrH1OLQQN4Xmd6WSS7S4wiKc2e8wgEcx6oKc1ze+yCO80EbRygA1WtEE8ooqbh6IhT/hQKAY5wEwVkOFJzXGPMDxG6aqbMNrVcwvFMkD2K6SXRuMN9Nw8zhpPoVmxR0w18AqQR8nY/CFQM7KUKJAMnhRqMV7ZeXDj0RuKaXPOwWY5HBzoj0XSAfM0zHKoh80gqBRnYII5sCJ5QLDZlvoJRKn5S0crUR//W2tn4b4/KvzL7cSpyUdIWX6RtyFEXRe5z90GcylTJDzyEZo3NaTPopVjFrNc523C5tn2rSGw7hajNZJazbZbY+pNUyQ5v5UbNY8OZuECqm2zfhQJcxxLS07DhBnUd418qJWXtGyIxnA6jBWcmoZSJJgjYd1YVk7OMnhaYntjvcWvBHCNjYATqKBrmwNXK2yZy3yoLpOcNlgXUdsZQEA0sBPKM0bBIK3GREEcLVFucHM0v4WRGBukBoSJTmuc3YcLSI7jUgA7vE7u9VkLrscBM90dIoMc4jdc7PLbIa2TDuIW5dMWbMoMaHODjHotzy560OnTqeJopgvLjDQuuPFtnv09uxwGvVbqvIpN50henDi7bty5OXw9y3trWybFuzd3516sY8eWY3Oc5el57kBzVFRBTWwZQM0uO44QWKT3iI+qB1O3AHndwgJxpMHkG/qgU6q94LS6G8oEaOTqn2UoppgqBp4QRaAuagAt4WQ0CGoFu4QW3hBCkEHK0LUEUAd0F03Oa9XFHiZwtxrtb1rfLVHh1j7t4Xk5493DWtNc4mHcdl8575VVWwJjdc7F2WxzjO3Ckuks2Magr3J2oHHVupva6Lc3S8uiVZjtLloLyx0QQD7iStfc2s/esi1sbquxzadJ1Rp9BC1j09Zy5Y9W1y7iFVs19FFno9enHhebLkZlDLGH03+JXcahHIHH0W5wrebw9Nlth9tHg0W7cTyvVjNPNlls5twxojQ3f8sLra5y6Yt3hVhiLCH0206p2aWrzZ4bevDNpOIYXc4Zcut6g1NG7X9ivHy8Wnrwz2xu2k8rz2NXyx6zXN8jO6xrysVJad+YWlY7g4O1IDplrxpbsswU4aPLC6QU0k7HhSggwAbKAXCSgBzY39ECgdTj8kSoG7rUR/9fbQR+YSvzL7cA8gmG7BHSEnZxBUQ6m0Dzd0GUyXNO26M0bg4MUqwoE6T6rm2bRqwC0jf1WozWQCCN+Ftj6khulx83kPZGz6bdtMb+qBFUFjtM8oA2Y4AdkGZSgkTyolZI4RCajJ3HKzWodQMw1ysKfUp6dhtPdaYntjuaDsDujYmtkQUGRShjYIkLaINLT5d/ZAROmTESsotmmqNxB7qCywN2Rmms0tbutxkzylshavoL8r/KZWRYlrmgDyDlIlZGzhDfLPdaRTWu/s37t/eQAPw/IdysiqrBUaN+EdIjKUagDuO5VkWnUwT5I1OPGncrrjxbcryae5YYDXuA2rXIZT9fzL04cOnnz5496haWVmAKLQ53755Xsww082XJsbzqM+q9FxmnnuVXMAD9FjTKpPZaTtCCe4lGhtYXGY29kDxbuPIgIDaylTHmdMdkE+8ACGDS30QKc4uMoBJKAAN0F8BSgAAOVBcoCc4ei0JyEFEDZZFOJGw4QQtkBBYECUEgFIKiCtCy07D1UFHy87qCBurjZBNO890wKXi9r9/wWvSiXUYqj5hc+aO/Hk0EOJABbx/NfJy9voY3wbuQdQkAbBJGtl0tzBA34UvHtqZ6OZa3NUxTpucTxAW5wsXlkehRy5iNUAvYKQPdy7TgrjednMyxa04NxWdUeeQ3hdseHTlly7Z1vhuGUIFKgHOG+pwleiYR57lWWawaIY0NHsIW+2M99Lc8u+Iz81rwzss7rXhjyAiSs2NbUdt+6zpLPKwYIPdamm5dAxe0p3+HPIEXFIF7H9/dc+WSx6MMtVobmTpcO/wDkvkck1X0MPMAS0EA8+qzrwfUNam1w1g7jdZaYpcD8QlBQbTBlZgZ8Qk7+i6QUGCCe6lCRrBidlBepoHm5QCCHSDuECabdJM+qJRvgbgbrUR//0Nuc9jxsvzL7cJc4E6eIR0hT5dBG/qoaOp6iQeyGma3Vtp47oxkGo524nlZqxiNqPc4sOxlZkaehRI0wW/Va0lE4RvO3otbTRAIc+Fnas5rtLOJK0m2PVGrcHdZ2pIa4uEpsZ9Cm74grEZJcIlKaK1SfYcrPtYNokgjj1WtFO3fIcfkqxPbHDfDfJRsVA1Huc5wgDhBm03NLdwtufco0oMtQ7kdsAXbrLSjGnU0wAoGMPiNB+iM0wDaIn0W4ycwtpjjnZavoLLofxLVkEBBkcHkICDCfM0/MKypo0Alu52XSYxCS2SXEggcFcdt6VqbszbU7cAc7rpMLU7tPVw/Ar29PnHh0TvqdsYXs4+m3N7eXk6nV1ps1lhdjhzJpt11e73L148enly5LkyZBMjn+C7a289iEBvmO5Kuk0m0SrFXpmCeyGxtoucJH8VF2NrKYEVNk2ghUZR2pgD3KbAOrPPLpTYWHmU2JBmU2IT7JsTUO6CGOQqBO4Uop3E+ii6QDYFDRhE9ldmggbJs0hBIBUNBIJKGhgbQhpCJaQhpGjS31SGk+i0aE5pjZDSmgk7hTSLLD22TQmk9t0xmmmRaUyXFjvgqAhyZTuXHw092X743NVrGBtLW7S88QSvn58Hze3sx5NRm0cssb/wA7qyfRvEKzg/Vm9RZ9GdRw7C7XanRDnDlzl2x4Y5XqLfoe1waIpMDfSAu0wkcrnapz6kSSS71P/BdPDHsp2o78Kba0AAe8qKLdGdFgEqLpekq6VCDCrOgFpcqsWBAghTSmU4eHUyNi0hJjtZfLQryk6jcVqZEaXbD2Xx+ear6/F6Y3h6zJXKZeCz5inMc0x2WdxrRDxvCbhoBbI0x9VPCapjQRDYV3DVU5jlvUqqABJB5TU/MKc3cyFLBYYIlvblQLduZ4I5RmhdJHlTcZ8v/R2wFrRAC/MvtwotGuT3R0gdBbJJ2KimUNYhsSJ5+aDM1FrYA5RjJBDgT+YKUjBaCK3mG8rMar02gEDT25W0WXt4PIUoXTINThYi1nFjAB+6V0jnSHs2JbwsNEgAkHuUVlUtdM6ux7LUGQ6oIG26lCtW8dipisZVLQWGOAFtKt/wAII78qMT2xrgEEAHf0VbZVvGzSOyB3wHcCFtxW6ImUEa1pEzKy3EB2LCFFWAGED1RmngaYc74e63GTDpcJG45Wr6AjSRDgsiUyRUA5ngJEprmgSQYPcLppJVtcNI9T27p21uWM6xwe7vWhzB4bPzFwgrvjxPJlyvcs8Ew+0Ot7PGrj8x4BXuw4o8uXK9EvLgGu4GwA2hd+3Xhy3vyh3byroW1sQAjNM0F20cKobToQPNA+qApo09zuR2QA6uSDoEAqBJc75qATzugsyeEEAIO6Ai6BPZBRMoJMKwETIiFRUFSkQgcFRtbGyY7IDAngIBIE7cIL0g8IKDR37coDDZEjhBCwgcIK0mOCkFik47xstIaKfGxQohbuJkN2RlZt4QXpY3hhRotxcdmAtA3QKe2qd4O/6LOvLe/BT6VU8D6LUxYyCaLzENI9VbNMCbQcOxUVZoOIgBSrC3W74IDd1GgC2qfuFBfgv40lBDQd2agrwXfulUU+i8D4VUUKLv3UBig4DcICbThwdHB3Vl0barjuG134nUfRplzHiRC+fy4br3cfJqPKdhl8z/oHfouF4tzTpOTyqrht9pH4Dv0XP+z1072I7CMQJnwHQn9np94A4TiIMm3d+if2en38EMIxSQfu7oT+z0+/izhWJkkCgVqdPXO8uy/2PiR38B2pX+z1PvFnBsSc0/1c6lqcVh94UcFxZp3oOAV+7PvAuwjEzsLcx3Kl4bT77QRguJNMCgYWf7PT7+P/0tt0r8y+3AO5R0gXQ46XcDhRTmMe5ogwQUGQNUAEzHJRzyR4FONPLuVKsJDC6qCVmNVnU2chbQFWnp39VKFsoOnUCsRazC0jS48LpHOqqgEyOPRYaKAAcDGyKzAW8xsVqAS1zZ9DupQDPDD5J3HZTFYaIe0+GdK2lPa3RTBcZUYntjVJD3VDwN4VbZlEh4BHogMNIdutuKywwd0EpPjynsstxdV2wjkqKE6/KAjNZMP0w7vC3GRk6Yb2Wr6FwARPBWRehrXtqTEbhWTyzfTNt8Ou78jw2Q3947bL14YvNllp79nglraBr6n4lfuDwvfhwyvJlzWPRNXbSIa30C6dmnn76EO9N1d6bk2Y0SAVuXa60bSoF5nsqMjwaQI1HccLNShdXDSWtaiEPcSdUx7IF6pO/KBg4UFSPRQG5iBY2KCzygnOyCh3CCwFYJMFUGIg+yixVPS87mG9yo0fTfSpnyjX80DDcsH/AEYQD96b+439EF/e2j8jf0QUbtonyNQQXmw0ho+iCOvHxsG/ogoXjo8wb7QkFG8fyAtIW2+qOnaEKIX9UbSjKnXr/RALr2p6o0EXdSOUAuvKkDdYtYtD99rRsVqUUL2t3K1btU+91HclQQ3NQfmUqwt11UPDio0D7zXG+s7IA+/XE/EEA/fa/wC8gMX1WOVpFi9qu2nhAD72rMgoF/fazuXcIFuvKxBAft3WalAb2rIdOw2nup27SZCN5UcPiKmtN9yjeVSI1cLXcfeVRuq0fEncv3lC68q+qndE7S/vlb98p3Re0P3qtJ8yd6b0tl3V/eTvO5RvKs7OKfiO5X3yr3cVOw7kN1UG8ytSaS+S3XdTutbjHa//09u3LG6l+afZga40xo4PKy3ChHI5XNtkUNRMHhbjFZHhmDoO5VrKnsIp+bdwUbgJcWtj4v8AJVqs6i4hn95EVVI078qUKpFsrEWsljYHm8y6RzoKrQd2o2GkS86H9+EGY0DTEIlU6AeIEIhLmAPFRgn1RYyKBM7tgHdVmmufo2/giT2x6bpeWuEB2yOjKpNDTDUQ1xcz4t1pyXp1O0zyEC4LXaTwNkDdI1tjhAY1AkDhTu0sh9MxuQtTJKKo1rm6iPL+YrVxuTO9G2tldXhDbamSzs5/YLrhxOWfJpsVlgNratFS5P3iqN9J7L3ceGrK8efLuaeqTADGN00wNmhex5gNDiUDW0j+6gcy2jdwAB3lAZdSpERufVAp1w/cN2bKAWvdIdKAtRJJPdAHJhSiFpjblTQEamkSgaN1oTV+VBHCQsgANJQHygoDeUFlxBgKwDUc3QSTClajx6mKtqXjLCgdVY/2pHZo3WZ7W+nqtPZogRv7ldGBtLkFrQvUgE7lBSCwYKlFOMlQQIAdPZADQ5ICWhWpAPKyLPCBbuEAHhBEFgwgjjIQC7hAGqdkCi3dBZ5QE3lBRdugEmZQI33hBQmTKCOEoKcIQCEEdugH8yzoC5qBTjGyCMMoKPKsFt5VEeYVgTUMhUf/1NuduYPC/NPswNSm4iWmR6LLpCWAh5lc2mTS1A8rcYp7HODplWsic4OJlRuFSQ7bkKtVk06sQiLeC/j9FKF0mEv0nywsRazabgBsF0jnQuEA+6NlBwBBjccIMulVlqJV1fPBhEJhzS/93bZFjJokuA2gnYIzS6hfUrBrD8HPuqk9rlwI1D6o6MoCWAs2KBjQX+Unzeq04oWw8EcoLfBPG/dATC3iNx3QGJBiYHMlWYbO7TNtbWtcnRQpOc7978q9GHC8+fI9+2wKhSAdfPD3dqI4BXr4+HTz5cr1Rpa0NpNDKbdg0bL0zB5889rDSRsNyuunn1dm06Tzudj6o6GspU6fxGUFuuGj4RCBTnzvPO6AKh4hAImN0BN22HCBo3CACIOyCxMH1V0JvBlNCMMGFAWkTMboKaTO6yKcdJJ7II1wcJAQTzeqCnSASOVYPGxXEHWtrVqOIDWgypWo0/Id+cVxnFb1xkMc1lM+0LM9rfTo3f5LowY0mEFwVoBuggQRBYG6lEgSVBI5QB7oKnZIIYhaAEDsgmwWRR3QCQgogQUC2kkboLCCn7HZBORugGAgB3KAN0BhALu5QC334QAAQ4gcIIRuZQD2QASSUEIEBBICBf5ldCEymglzDqLjweyxRBtuBCASrBBxKoh35VgQ9pOwVH//1dv8rhqB2X5nb7UgW1GulvHuo6SFbauZ91nSsime54CsZ0bILZHdU7VAOgnt6o1oIadU9kKzKTdUCN/VER7XtmPooAZq16Zlx5U0tPa0xI3WnOmPksiPN3CNk06U1ASfKdwgy2tA2P8ABGLfKF3oNgilOfrcY29VWmQzyNmfkoxVUKbhqqHYlKk9rB1PA7TuUb2yWCJA7LWhZDtncErWox2mQSQTz6LO00EU3HW/bnjusy3bXbNHWtrWuXAUGOc487cL1YcW3HLORslhltrfNfO1N50Dhe7i4JI8fJzWV7dFlOkzw6IDGN2AC9WOMjzZclpopOf3/huulu3K+TRQDRqedvVQWalNkBu5RrYHVnE7bBELc4kTKCwQeeUAlwn5IITq3QFAiZQUI7IGNcewQTcbnhAbY4V2KI80JsDp3lQESewQDMmFNAzTJHsmhQpxsJTQa2k4jYGU0KdZvcNyAPnug8PG8Ptn2lZlWXANJLex2SxdtH6Y+ELnGW02hrGVw1oHsFiezbpXuuiGN4QSD6rS6FA5naYPzRm1qWJdSsi4PfVsNxPHLa2xCg4trW73Q9pHb5rFy01J4bLb16F3b0rq2f4ltXaKlGoOHMcJBTHLZo5o7rdiKPJU0KJSwBPZQQ7hAJ9FdiDZNgdKguIEoAB1Sgh2lAtrYQXCAH7lBY4QUgAiUFHZBBwroC7cJoCPRNChtumhCJEpoARG31TQAt7poUTt8lBQMoBLYMq7AzO6bE2IWQD9thwmgBED5oK4AVEG/KsAuGyo/9bbdfhtLANl+ZfbhIY5zi5o2R0iBrgoVk0IcNDkQ/wwAAEWK1tgsJgjlFLb+G8dwiVneIPKAP0RC6jnRqBQAwuDg9FrJY4x5VHOhqPeBrHJ2KNrazU2e44KoyqJY5knkbFHO+x6B9CqsIdTDHSO6NnhodDJ3CjFSprYPLwlSewnywW/VGmXT3aDwfdaDXEuhrNyFyx2lyHSbUuHhlJpe/gRuP1Xrw47XG5vfsst1HxUxB2kchjf817sOCaeTLm8veoUKFr5bRnh7Qe5Xrw45Hly5ac2g47yQCZdK62acu7Y3eGzYCVFELiBDWwSgB7nRJM+yBQcdyR8kFh0g+qCgSRBQW0yZ/MEF6Z3QWBAQEAI3QUABwgNphAR8wQEG6d0Flp1auyCwAfb5oLawuMQgYLRxMwB8ygYKbae7iNuyCGtTB8rZQA64dEDYeiBJeTu5Zo8rFo+7VZ4LT/Ja+g590qANTG3d/vZ/gCuX8w6b6LqGNQWtNBfUbRY6s8wxo8Qk8eQGUcq/MbO+MDMOcMcxt4Dql/eV6mp0yNJI2/RZyxamT766IY2MwdLsu3hdNWlbC2qbzDqB0f5LEjUroAIA+a7UqjuSVEVypQEGVBUiYQU7lBJCCIIfhhApvl5QW5AAQTUEAHfhBcbQgqCgFADiOEFjhaFRI2QAAZlBRIAMoKkFu3dADiJQUePnwlAngrIEbDdBRIdwgGDugGCEEO4QUYI2QC6ABKASRGysAEn6Kj/19vYWhul3K/MvtwJc4bjgbI6QLXioYUQ9tN1OD+VBka2wC36osKcW7uPdFRlEVBqHKJWVQboEO3KICo3y6uw3QXRb+Y8IrIBaOOFHOqeadTy+irYaMtJ9EGXTADSQjnfa2nyyqsJnU+ewKNipteXPqjgGFGKfXqGGEjY8qVJ7RrRUALePUKxpl21JzzoY01HdhC6zG36M3Kfm2DD8uVqv414fCp+g5X0cenkeLLkbFbWNvatAtaAYf3iP4r1Y8UePPkZTmRvVfzyunpj35L102Hyb+5UuSXFfjveIOw4Vl2zrRBGknflVTAPKgrUTxygp2qDKARsCUEb5+NkBMG7kDG8II06SZ7oKEhAYagvTCBgYSNkB06VV8hA8UWNjxD+iCz93b/eQCLgT+GEEfXc74tvkgW6COUC+NggomUAnhSjzcWE21X/AAlX6DQOlYh2OH/tjly/mHTAJErqDagtaaap1Pxunl7p9mLFHHT4NlWp0XcEVKjdDf4lHKvzxwfA3YllzMeNluqphNO3qA8z94rhp+u66a8OW/L6s+yHjjbnJuL4G52p2G3fjUxMxTrjt7SvP9XfB9FnYAegA/gujVQd0RQ5UoHuoAHxII7lAKAkFOQAeEEPCAB3QAeUF8OhBaCIFHlBRb5kE9VoUO6CvVABE7IKiNkCnII8+VqUQbg/JZAIBOyCIKJEIAnsgp2yAC7iUEcQWhWBbtgqP//Q25zdJX5d9uLc1umVXSERBlqIy6TnOYQUDARogIKc0eHJ+iCrdxD4CDLl4Do7oFMe9wg8TugbbhpLg/hGTn040hvwoEvDmGPVVo2iZ8v5kGS0ODdz9FEq26dJMRuiFSHEtHdN6O3bJoMEeGVdbT0utTe4aGNLyO7VucFy8M/fSPZwnLl3WpivdjwLf90cwvZx9P2vNyc221W9naWLWiiwa/8A5ncr6OGOo+bn5rNZUbTEv3cujKVLrUPJsgxqlSo+N9wstRQl+x3Km1NAgQt4s0LhuHei0gp7oLDoKCj5ilFDYkLIJvlQMQW1BbviQGGl5ACB7KLyPQd5QMFKg3cvk9wgLxabBDGBx91YFOrvn0HoFQtz3OQLcgNrkFoLAO+6AQ6DBKCu5QB+dBgYmJtqvyKlGgdLBH7a/wDGuWZ7HS2crYIN3QWqVwz7VeN/s3pkMODtNXFrulSLfVlM6/5gLccq4z0dyk/GOjnUu88OalRlP7p7vt2F5/iFnD8VTP1B/ZGxr7jnm+wRztFPErItYD3qW51ALz8k+Z34/wAL7UmSYGw4911rERRpR7KwR3KoEd0oWW+ZZF/CgFBNKCxsECj8RQD2ClEU0LHKQQ8LQAO3QUOVoUdkC1zBD4VuBX5lQSAD3PogAb7oAIlBOEFIFuQUTtp9UC4jb0WaIeEFHsgF3CC3mA1WBdQyFR//0dxPmlfl324S57dMTuq6QNJ41Qd4RGYzc7bDuEBaA1pQRrwRpI+SC6TC3zjfeIQZQeY3G/qgx/FAcWhsSgfTaC3YIycHOcIAiEFVA5wnuFWg0RpOr83dBmRraXTuolTSSBv8wiFMp6bgFnnB/IF0w4+5nLPtbBY4DdXkVXjwKPqdivbhwPHyczYrbD7HD2/gM11O73DeV7uPjmN28WfJtkmvDNbzEd138OW6x7K88fVWIlsQ35zyptdsgPJc6TMIg3ExwgETMxztCmU8NPPxfMmA5eom5xjELeypyCDVqAE9o08rjjd1XpW9zRu7eldW1RtW2rtFSjVYZD2O3BXp0zTJlEThBZ+HblBTSZSiyJKyGNAKBhA7IKBMQBuO6COeyWgb1HcAIHsqspiAPN3Pugo1qjidyUAiZkoDLp27eisFKipQXAQCNkDG7oCgIALRMhBIQUQJ4QefiX9hU27FSjQelw/DxoxB++v3WZ7HSae255WwchBXv2VK+Rvti42K2M5dy813ktqL72u08TUcAyflBW45V1X7O+WWWXRu2oV6Yc/GxcXD2kfEy4MMn5Bc+PznTl/DHyr06vKmResmG06xI+44m+zuHHbyveaRB9lnmnlrhvh+hZkeX93ZZl3G9LB9VoTZWAXcqiD2QURvKyKgFADhB2QSUEkoFvHccoAgwmhFdCzsdk0KJMFQLHKC1oA4lBTRK5gjsNluBJBmVQQO26ACgEgNGyBYMlBcBALtkC3cwgGDMoBdIMrIEuHwoKdII3QRw2QCZI33VgWd1R//0twaYBPqvzD7cA6kCNSzt0gGNGrVESrEZQ23HCosHWdIJhAsmJnbSgdbudBjfvCDKLgA2e/ZAuu1hALRBQNpGAAqaHqPbZRkl1R5d6AJtO5kU5dHG6NMjemImdXAVktZtkZ9lguIXxADTSpHl7tl7OPppl9Xlz5+36Npw/A7DDA1xaKtwOXu4lfQ4ummH13t4+Tnuf0Z9StrO/HYDYBeiYyPPbsBcTsePVas3NM68vBzbfOsMJuajXQQ3S0/4lys00zMIBp2Nq106hTBd7ytSbR6TRzvueyoYSfVBbPjEnsYCZ35Vlfnv9p6rfUurGM2txc1X2xp29SjRLj4bWmk2YHEyvLhfK3w+g/sp9TW5oy0/JuJ1wcZwZv9Xc/Y1LbsPmN17p5m3OZbfQ7SBO/PHyWK6aHpLkl2xvyhBAVVC3T9eUoIQBKyCYUBh7ZQDXrCkx7zs0Db6coPKwW9fdsrXp3pPeW2/fYGJ/UK6HrAkyDzPKaDGg+qaaxktGA4xHEgH6q3HUc92y2fRruPZ4yvlfVTxjEGUblg81Dl4kSNl48ue430+58P+E83VcXfJ9dfR62F4pZ4xh9tiVjUFW2umeJTe2Yhd8OTueP4h0mXSdReG/Rmgd11eDhvdjbn4v0RFU5pmEBNJHKAgZQW4wgrndABJlS0YGJgii8Du0/yVsGg9LtQp41/4165z2OjgGF0Fx5S5BYPkA7u4CY+TLw+AvtD4u7MHWDFLemdbbV9LDaLPikhvt7uWrdOPt9x5PwluX8s4HgoGn7nZ0KJI2gtYDx9V58c7jlb+bvyYy4R8N9esNflrrDi1zRZ4NKtXo4lTI9akVHBv1Xqyw7puvPx3t8PujLGJMxzLmFYwxwIvbWjWMb7uYJ/ivJjl9Hor045XeRFt+KFkQiXH2V2K4KbFndNADsmgLt900ABJ7JoWmgJPITQFpkKiO7II4d1KB5lQABugtXYW5NigIU0LJ2WgPZABQD3QU8SIQKAhwQETCmxRE7psKIlyoklADpdspoLjb3TQHflNAxu1NAXHaFdBZb3Qf/T23cNkr8w+3Ait5SFiukVTcXu9lqekZsNa2RuFRbXtBDnCO2yBdRnJ9UDrdmlm3KDIe0aAR8Q7KjH8YEEEbgKUZABGgxsVz2U+B6LWnO1jvb59PDud0xxtrds09TDMMvr0htNmmn3e4QI9l7uPi28mfJptmHYFZ2YFSufvFX0PAXvw4Jp4s+a7emaxI0xFMcNG0Bd5hpxuey5B77ei6RgPBlUNEOarBpvUdr3Zcu3M+Jjdf0aQSueQ9vL9229wewu2birRYCfotYj1WzqnsoHQgEQHA9wVnP0R8Kfa1s2W3Vmjc1BNO6s6FYz7O8OPlAXLCeVyaZYX+J9GepFhito533VgoXVMcCvaXLWuI94kwvVbq6c8Y/RTA8Wscw4VZ43hzxUs7+m2tTe0y3ziSPoVbG9vTa4wPdc57TQgZMLSrLZG6UWGCFkCOUAhp1e6DzMyXL7TCbuuz/o6NR4/wBn/igxskPZUy3h9RvmL6YdvxqJJK1BsTfU8nlA0GE+rWOO8hQIgjeQQfSFrL059PyTHKyvkXr9H/KNd6t/waJ292gRC+Tye3+oPsL03Dl0HdZ9fyjpH2ds2OvMMu8q3jya1i7xbME80jyPoey7cd0/Df8AUL4XOPn/ALRjPFdzHA9xIXtx8v4xzzv7e36VXsq0gLpkoLO/CCCZQEQSEFxsgCRMLNGHiQ1UnRxBldBoXTLTpxlo5++1P4LhPxDovDV1FcsIQKurhljbXF7UMNtqRqmeNLGlx/gEwM35jY1jF1f5svcwUn/1utfVrim/SSS7xCWGN+BCuTjG6N6+dYAA1uO3OgCGRTBAjbaWLOWPiVcsvo03Nmacx5uvxiuZrupd35p+CK9Rmk6QNhwBsuu/lTD2+4vs4Y4Mb6WYW0vDq2HuqWdXff8ADMtn6ELw4e3oydaI3PtyvZGQt2euQnclUV3lILWgtyCjwgAQOUFkgSeyAOSYQU0RIKCnEbICO4lSgBsSFAB2KC0AESgvYBaC3bkwgocQUAkGUAnaEEkEoFuEGUoAlZBE7IBEEytCiAEAEDdAojsgCCgL8vyQBugh90H/1NukAauQvzD7cKeWVNwI9ViukMpDYH8vZaiMtoDmbcKgS4M5QKr1DqkcFA+kX+GNPM/wQZEeWZ8w4QJBZUdB+JKM+lpeBLvhTDHbFZdtY3F47wrZmp54cR5f1XrnHXG2Niw/LVrZAVsQPjVz+XsCvXjw6ePLme2KjGNFOi0MY3YNHYL3YYR5Ms6AAk889l0vhmTc2Zp2UnljLwWW+ia0YXaBp7o2JjoVHmZgs24hhVzbuH9oxzf1C5ZDUelmIVauDVMOrH8TD61S1M8jQ7ZbxHQ2qA9Y9UFgSZWMvSx8XfbRsSM35fvBv49i+h8g15d/mnF7Mk6gZJfnToNlHqJhrPFxHBLM2+IQJc61pPLA73LSP4rpzfjn7M4emyfZF6mtfRr9N8WqkOaDc4SXO4b+amCeTJ4Xea0x52+tADOrsQI/zXD6uv0ENjKqLLilBNOyyB7oJ+coPJzDS+8YbdW//wAyk9v6tQa90suzVyvRt3GX2lR9I+2lxWoN9buB7bIDiCEb4stZwXotZenHPi3lbHyb1v0f8qs1RqohtqajfVu0r5Wft/pv7E8X/wBo3v1v/s8uyurjpp1NNxJFvSrtqPHAdbXUOb8xDlJ4r2dRwY/FPhd+tk/rvb7Btbqld0qV1bkG2rN8agRv5agX0eK7j/M3U8d6bqM+OmjldHl0t3m42URbWoLIjdBUlBYJhAJHmCzkMS//ALF/yK6DQ+mggYyf+3Vf5hcJ+IdBnZdQTIQaP1jxv+j/AExzJiQMFtq+jTPfVXOgR+qYGb5Q+yxle2x/qDVvL+i27scMsqlVzXNDm+LUIptkHvuSrk5R9pf0Xy7+bCbOR/8AYYFnK/LCxxT7UGTsMd01di2HYfRtq+G3NOs+pb02scadQ6HAx6Qu3HNxi3TXPsd4z4lnmHLdVxdUpVGXTG9x4nldt82rzZTWTpx3cfUUmI+e/wBV1joqN5XMQ8KgUFrQByAXcIAPIQR3CAeBKC4/igpzeEFeyUCeVkC7lBEAoIeFoL7oKQTaJQLcgocoBqtLhASgAwhpWRDwgpq0I7hADuECjygEoJ2KAEEcJQf/1dwGnRpX5d9uMdw0gqukOtnNjzfREZIa8bg+X0VFOe30hQA7Q5p9RwgyKLnaQO6B/wALSfzIMNwNN5cQAD3Us3NG9PXwK0bi10G03EUGfGfkvb0vDY83Ny7b7RFOhS8K3phjWr7Pb4fKyySo9z2A+iOa2QYJ5QGQ0eYkNDdy8mAB3TLPGY6rUx5M/lwlqUrihWbNBzX0z8Lmu1ApxXU3Hf8As+Wtck1+4odJ3WZy3O3f0eOYTG2RY9ytNrgDYKUKrfiMc3nbhcrBy7A6n7B6g4jhjjptcUb95o+9Tuu+F8DqjXaqbdKoprYQPY7aFr6NZ3xHyb9tWzJp5UxAD4XXFJx/9IK8mHjMv4XRPsw+BjvRW3wi9ptr23iXNm+m74Sx3mcD9HL3dRd2fs83D6r5Vz3lvGOiPVPThxfSp2ldt9hFxw19s9xcBPqNwV5Z7emvvvIGcrHPmVcOzLYOaWXlIeNSHNOs3Z4PzO69H0cZ7bPEHSs10WRG65itYG0KgmbjzIIA0u1IEX1NtSkRHM/xCDnPTh/3HGsfwOpsGVzWYP8AvIUHUIhxg7LpAcxyVnL3GosO3Vy9M4f3dfJPXolvUi4qN+JtGg4fQBfN/mf6Z+wuPd8Es/zf/wCWz9c8rB+CZdzhatJm1o2d+4ehY11N5+UkJy47j4n2O+LTi5+Xg353O3/lvfQjNn7dysMIuKgdiGFO8Pfl1LsV26W6j8d9v/g16XnnNJ8uV39fbq69+38xnL3RPzKM4zsuxrLe9pv34QSA06hwgiCLOQwr4aqLx6groNF6bD8PF3f9urD9CFwn4hv35V1EZsQEHxN9oXqxmzEsfx7p5WqUqeAWd2xtJjW+d7QwO3PzK1cXLbSOjvUbMeRMeZZ4F4TaeNXFtRu61VkuLfEDSB+q53E2/RFu7Wn1aCZ5kiVvHw678Plr7U/UXMWDXv8AQW0dSbgOMWIfdh7NT51x5fThdJXDKPnjI/UDMnTzEK2LZarClc16Rp1vEbMtBndc8/bXHNR+h2ScWu8dyjg2M35BvL21p16+nZut4kwFz06vf7LUgo8KgUFIKcgWgA/Egj+UEQRvKCz8SAT8SATygB/dAPJbqQRyCkFEQZQA4yUFflKAeyBYQW/hKIeFkAgnZaC0FF2yABuUAnlBSAe6CRKD/9bbQ5ujjf1X5d9uFEF20qukE1kQPREZjXENBLvoqCc0bO5CgQXAO2EygzKQdGqIAQVWqtjcwg17GMXFlbOdq1PJhv1XTim85P1Yzusa6PkyydZ4NbPJ1Va7NdVx5l24X6LHjmL4uee2zDdu+88rptxk2mqIb29FyUQ34EION/aAzBmLAsNw+lhNw62tL1z6VzVYYdxsF+c+I8ueHJbPWn9O+wfQ8PVdTZySX92mdDOpdTDcVOWMbuDUtb139Ur1XToqkRvPqvZ8O6uZzVfc+1v2buPJvhnj9I+nTw3eYEE+vuPYr7WeMnmfV/Fs8bhyZY33FgBYQPdSicccpIOW9RKIwvFsGzHSEfdrjRVj9x5iPlusW6o6bY1mVrdtWn8Dw1zT7FdhkaZPKAmgxAVxTO+Hzb9sezNXJGCXRE+Dflpd6CoyI+sLyXxk6zziyvse3nidN8TtHu8lriDi8khoa1zWmZK9WeW3m43mfazvMg41l2gf2pQdm/Dquu2oW7g91Sk/Z7SRwODC5R3rSPsn9TRgGP1MlYnX0YZi7g6y1ny0rlhgN9tYXo+jjPb7d3kzIMmPX3n29Fmugi4njaOVzAkklUG3+9ugjgGoAfJpu+UhBy9jhhPU9u+lmJ2+47a28KDqbeASZPc+66QOERws5e4sTgk9lcvSYf3dfJPX0lvUO6Prb0Y/2V8vL8T/AE99gLr4Nf8A+3/Z9G3GXaGaen1HA7kDTdYbRZSLvy1TSaWn/ahezLHeL+EdN1ufR/FLnvxx5f6vmzpjj1fIee2W15NO3dVdY4jTdto0nRq+cwfkvHx5ayf3v7TdPh8W+G92Hmyd35/R9fh0t1NgsOnS4d2PEgr6Eyf5e6jgvDktdovLN4bEoxPSauAVFE74NkAt4QQrNGLff2LiOYXQaN02bFLGfQYhWj+C4T8Q3z8q6iEbbcqjU8T6W9PcXva+J4pl20u8QujquLmqwl73bCSZ9lu1jsJtekvTazr07mhliyp16Tg+lUbTMtc06gRv2IlYuR2N0AAAA4GwlY208DHcjZQzLdNvsewi2v7xjPDZVrs1uaznSDPC6SpY8kdIumnmnLFgQ74vwjv/ABVqyabdZ2dnh9rSsrKk2haUGinSosENYxuwACzpTT6BBDwoBdsQgE87IIgFBUDfZAt3KCjwgscIBJMoIgooAIMoKcEAoKgoLPwlBjknVygITCCigoAeiCilEd8KyEtJJ3QW7ZaAOQASEFbRsgE/DPeUFDhAtxjdBWsnhB//19opjWOYX5d93S3MIIAVU6m0AeY7hA9pYRJ4QWTqBHA7IEfC7bdBmU6w0R3QYdyWhrjM7cIOV50xV1GrQYTpaazZ9hK7dP8A3mP7xy5fwX9n0xgpYcNtjTIg0qRA7EFoX6jOPg729RhEQV5bWpdLMdlUMYQRxCDk/wBoTCjf5DfdN3fYXFOqHDkA7FfE+KeMfT+gfYjmuHX4zeu7w+W7ewuXYbUx20nRZ1WsuHt+JmoDS4R7r4nHMuLHuxf3zqOp4/v5w5yWZT3fb6u6M9Q6eb8HGFX1Qft3Dg2i8E/2lMDZwn25X6zpef73CXL2/wA//a34D/YuuyuNtwz8/R1HVt/L3916ub5J48vwWcy7tYzcQAGSVMPmw3WstS+F6SRLVraNR6gYZ+1MBu6LW+drdTI3Opolcssd+RXTrGBi2WbInarTaaNX1D6ZghdMbsba0PDv7vcrV8B49W7jumDNm3DPtaWv3jpLWrNEut723qD2EkFePP8AE7S+NPi3L2Ys5WdpXy7li7u2UL+o11xaWgcTUPH5eJXt7dxy1MXTcmfZm6mZwcL3FKP7Hs6nmFa/JfcHffbt9Vi46O54XVbpfi3RjNVhSt7h9zZVmsu8OxEDSXVmHcSO7TwtTJmPt3ox1AodR8i2GM06gfiNFgtsSbxoq09vNPqt2NbdCcWu3aYHpBkrhfYobLSjG6CSroR3wmAmhy3qHTNhjWBYw0aSy6ZTc7+68wZWR06iQ9jXg+V28/NblDwRtG6XzWoMmWkLWU8OeN/h18l9fjPUSu3b/m1H+Uf5L5Wcnc/0x9hOSY/BbcvE3l/2fT2B3tpbZbwqtd1mUKRsrf43gGfCaF6pyyzT+Ddf03JzdVl91N2X/X93zD1vGAvzg/E8BvaVd180VbxlCIZXZ5Z+vdePLGS7f6C+xHF1XJ0WfD1OHbNWS73fLvvSXNbc0ZLs6z3h19ZAW9y2ZJLdhK9XHlt/Fftd8KvSdTcPpG9t3Xr3p+Kly322fL+a0WqI3RBAyIKAWkgx2QEQVLBi3kmi/wCSto0jpwYZjLfXEKy5z2N8DZELoKPMILIBStbVHuVntNpqA8pIDjOgesBSzTnvyk9+xVlbggV13oqnFTaB43UtFd1BHtmEA8bIKQURBI9EAz/FADvVAI3CCA7KUSE2BTYtu+5VAkoFkkoIdkF8BADnbFAqO6Ci5BDwgqYEoBJQQmRCmgGnSU0BcdpVCyZQAQgqYEIJ2hBUCEAOAVFQAU0P/9DanDQfLwvzD7wHVSXBBkU3BxAdx3QOa1uoj8vKCwGOJAMRsgV4YNSATKB+hzRpj3lB5t8XBpEwD3Qcb6j0ag012klrTq+o3XXhus8b+rnyTeN/Z9CdKMwMx/KlhcPM1abRSqj00iAv0sy2+HlNOggNiRwueUZnlGmTCyDB0kgqjW+oWF/tjJWNWTRJdbPcAf32CQvl/FMO7hfb+AdReLr+P94+buidtZYriuK5RxJoNtjFm5gns9hMEe4Xk+Hcc5eOy+4/tn2wufSzi58fyk/328BzMe6TZ4JYXU6thUkTxVtye/rIXh4+W8XLcX1Ozi+O9D3T8WM/b/1fYOV8yWWbMEt8asHB1Ou0F7BzSf3ZC/WcOc5MX+e/inBeh5bx2PaAiWuHz9vmrfF0+Nj4nn6rZIB9CjTFvKAqUHsInUDP6KDm3Tys3CMxY5gNX4KVYV6DO0VRv/FTjHVGEnb1XTL0CnQIHHdMEct+0daC56OZiIbq8Cm2sB38jhJH6ryZfidI4X9i8WdXMOY7e5oU6tx93p1KLntDnM85kgkGF9PCfK8/JfL7LLg7Yk6dWzh+6DtPruvPyeG8HzX9r7MWAU8t2OWMRsKlbGrtwvcMvW7MtzTdDwHep9F5JfLeU8PlDLebc34NTucNyxfXNt+0XA1qFpqcXvB2MNHJXtwYfpD04xPEcWyPgt1jNCrbYo60osuqVfaoalIRJHofVMorZuCuaja4ILgrQIQWkIOf9U7I18u16rBNW0i4Z7FhC532Nmy9fi+wWyugZFSkwk++kLcHstA7KtQztC3l6csf7uvkzr+C3qHXcRP9VokH03O6+Pyzdf6h/wCnX/8AFT92t2WHdQ82CnRtaF/e2rNLKZbqbRa0cb+i1jxWvq9V1vw3oeS5Wef8uNbdbfZ8zy+yrXt5Ut7V7aLqjaLXa6j3N8waT2lZy4LI/PdR9v8AocObDHCZecpPwa/5ToVmirlvNr8Evz4VnijhReyptorjhOPLTX21+H49Z0d6nD3Jt9XNG5HeYj5iQvpYZbj/ADV97bxWfUQ34WzH0sN7IqaSHT2QCRIJCBjRI90GLeHTTeO8LNGj9O2gNxk9/wBoVlJ7G+N9PRbATJQWdkE549CfoO6DjHVfqzQyZn7JuCGqG29Wv4mLHsyhWHhtJ995hWzwx9XZnOD/ADtHlefLG433Eey4ukWCF13sqiURRQVCAj2QCRKADsgo7k+6AYKAHDsgHgboINlKJIUA8pBbRsVoKJg7oL2QCd0Ak9kAuOyAQdkAEbhBZOwQVBhAJCAZhBTj3QCdxCBSATygjhCCgJ4QUfRAsmDurBNQJVH/0dqO6/MPvE6makGTSaTB/J2QPiD5eEFOY7ttKgVTqubU0xJCDMNRzmzG6ow72nrp7gRG8IOb52wvx7JxaNUA7K43ViZehdA8ymwxm7y1dP0065mgCdp9Avv8GW3xeeWPpuk6QvVnHnwFoIMrko9QG3dAu5pfebavQcJp1ab6RH+MQvPnj37xvrT08V+6yw5J7lfHGULp+WOqds150tt799s/tFN7y1fA+HcnZ1Fxnq7f6J+J5T4j8FmX17Z/2fQfWnp2M44E7EbFjf25YNL6VYDerT5IPyAX0ev6aTLun1fyr7K/Hc/h/LeO/hz1/wDPLivRjP8AcZIxo4Riji3B72p4dem6ZpVZ534Xn6Dnsz1fD+kfaj7P8fW9N/asPNk34v8AxH11TeK1Flam7VTqDXTe3cPYeN197Ky3b/P+WF87mrBjjZHGeUc1zhupSXbkuYGHAc/4Zi42oX02tYcDU7YEqcauqUXeSfTZdMg4yWApgjkn2h875ZwDIGMZbxe4c3FMes30sPtmidZBBJntwvJlPmdI+IsidRsydOr28xLLVRltfXtA2zi/zAAkH/JfQl1HPObr9FummZ35xyNgWYa//Obq2YbkFun8Vkh2x9Vyy8szw8rqb0my91TpYXRx2pVotwys6troHS57HiNDjyB8lzmHld7eplLpjkbJdOlRy9g1vbvaI+9aA+s+eSS7+ZXeeINvgBjdgHCWgDmPdcbfKqAlVRtCCA7laBN+FB42ZbNt5hV1QI+Om5rp/vAhYo1vpZeePlxlnUk1LOpUt3essP8AwVg34RILeCtfVqGRsFvL0442fd18mfaD0/8AKFVDx/8AC0oB9RK+XldZP9N/YDHDP4PZl68/9n09lLQzLODhsU2i0o/C0NJc6mD2Xs484/gfxSXHrc8rvUyeq+rSoUalapAbTBe97pd5WiTsumdllfKw5b1PLJrWvW3xhn2/whme77FcrXJqYe+q25Y+mNOiqXSQ2Y7r5eU16f6r+z/Dn1Pw6cPJLPH7O7dNOrt5nPG2YDf0W0S21D6dYRqe6ntv811wyr+T/av7MYdBxZZY2X+rrwcSZiJ7L6EfyZYO6ApkIKHBQW0xCDGvh+G4rNGkdPPgxj/zCt/kpPY3scn5LYoN3QU7cSOByUHl5jx7Dss4He4/i9TwMPsaRqV3TBIZu1gnkuKo/N7PGbr/ADzmrEcy4iSal7WLqVGdqdGfK0f4RwulnysfV9l/Zy6msztlBuA4jX15hwRrab3uPmq0W7MfHJ25XjrcdoBGo9t9/muuN8LU7rSIghQR3CASSgW4lBEEQCeUAOQUVKBKgpWCwfKVQsiUEQCgFAB3JQSIQUR3QASgInZAskoFkmUBP+FACAEAH4kBOGyACdIlAIO0lAt6sAjlUf/S2jVDi1fmH6Alh85BHdBn040GOEZppcWtBHKoEl53PC55ARSl2tv1VgyQzyzyfRaCKpBBbHKDwMXw8XFB7InY7KUcTu3XWUs02uKUPK2jXl3yJ3X0uiy08XPNvsPAMWpYvhVtiFuQ9tam10r7OXzR8jKPb1tcPdclV+cQimNnnkyIHsNys3KyVqZdstn5PjTqlYHA+qF/XHkBuGXTPdrod/kvy3SyYdR5/N/pD7P8ePU/CMb9e3X+z66s8Ws3YHZ4reV6dK2qUKdV1R7gB5mDn1lfpOa4Yy2fV/A8Oj5+Tlyw7bvC/L4fI3V24y1d5wr3OVK4fQq73haPIK/932X4/l6nLvr/AED9ksOrvDeLnlk19Zp2PoP1KditkcpYvX/1lZj+qVHba2AcD5L9V0ueOfF+r+a/bL4DydLzXPjny/pHcGy0+b6gfvL34Tw/m/T4YyXu9iI1KZYvJ00/iZf1c26q2JqYN+0KAJuLCq24b/8A41iOzbsuX7MTwizv2u1Mr0m1T/igT/Fbg9xvC1fQ5L1v6Njq3aYRSt7xtjeYZWdUqVy3W427xpLABuJK54jEyP8AZk6c5RfTqX1q7HMRogF1S7jQDHZvoF6NueXt2O2tLeyoU7e0pMoWzNqdOk0MpgDsAFi1ZD4mFithPkdCgaDMFASCII4t8qBm/qgxr1gfTcxwlrm6SfmrfQ5z0/qOscx5gwlx0htUXNNv+PYrnB1BpEQPy+X6hdLPMWTV7vyIv7pljY3F5VBfToU3VHMby4MEmFrLLUdOl6XHn5ph/ifGPU3N1rnbNNXGLKi6jbmiyiGVD5vKSeV8vK92T/WH2b+Gcfw/o5xbnnz/AKuvdCuoOOY9jFTAsXrGpYULNrbNrWEBtSkY3d32XbCP5d9tfhXS8e+Tis7/ANPbvVVgq030yJY9pa4HiCIXqs+Wv5B0HJleSZcvvC7m3CLH7Nlu+5rVcYxZ7bbU6rTpWrNLtLnE6dT5HdeXDj0/rfJ/1C6jp+HWNu/6f+jpeVemeUco3NO8wqznE2N8MXNV58UD5L0TB+J+Lfa3qviXFccrf9m5b9+e66vyk3rypFEOEEQQoF3f9kfks0aP094xj/zCt/kpPY3kcLYgEkDie6AYc4jYRJ3JhsM/M5VLXxX9pnq9/SfFv6GYHXJy/hNQm9ewyLm8/Mz3a0cLcYtfO5cOy1WdtnyFnXFch5ks8xYRUIqUXBlaj+WpTO7mn2hefKOkr9FMm5rwrO2XrTMmC1W1rW7YHOpk+enUGzmu+RWJGnvkEeXkRP6rcC+60LdygM8IAdygW5BEFjgoA7lAvuUFxIQA4EcIBBMboLQUOSgEcoKcgWgpBRQUfhKAEFdigh4CBZ5QRADnIB1IBIbs4rIjvbhBT4jflWBO/f6Kibd0FANlB//T2Y/Dq/N6r8w/QAY+HAO790Gcxp0jTu1GaNxJHpCoBxqaCJJXPIHRc5ukO77FWB29N0djutBdcgiRx3QYlRrSwmfmpRyzqNgjbilUr0278/VduDPVcs8dxvXQDNBvcEfgd06bmxcGNBO+kei/RcF7o+PzY6dtYwl0zt6KX24n7CCopoc0eYGDCZ67GsJu3b5a+0nYOoZqsMQaPLd2+gv9Sxx2P0X5Lr8Lx5bj+4/YLrcuTp8+P6Y5a/2aFSxnO+bqNpl+2qXN/b0AKDLWkSGRsBJHAhWXl5Mcfen6zm6Loek5fvMu2Wfo6zk77ONWs2lfZuuRQZp3s7c7tJ41O7r6uHw3Hs3l7fjPi/284+HlmPDJqflb/wCjm+bMv410szqDbvc0UqnjWFyCYfSmdJPy5Xy8eXLh5dfR+26Lq+D4z0ustXKz930hlvrLlLFMHw27vb1tHErt7LevafmFc7A/Jfe4epmT+GfE/sz1PH1OWWMvbv8AT/1dM8w+QIBI/dIkFfQ3uPyueH3edn1eLmKyZfYddWtQAivTcIXKOTTOk18/9lXGCVTNXDazreHc6CSQtwdJa6e6su6HNlw0Hdq1ZoEGhhWZU0kw4OO4Jg+30UtakazmjqRknJJptzHi9CzuHNllu46nkT8RA4+SxFpmXM9ZRzowVMt4tRviz42MMP39j6LTLZafwlp2IMb8oCe7QPVBTX6uyC3AEwAgMb7fRAqvxpJ27q30OZH/AFR1Mo1I8mJ2zmH0L6e4lc4OoUnEtEcnzEe5Xarb8lgq9NtahVoVTppVmOpPhuqWvEFcsptOkzz4ubHk+mN25hhPQTIuHVXVLulWv6pcXaax1U2gmY07cLjjxau39G6v7b80kywy9Tt9/wDs6JhmB4LgtPwMLsaNmxohgpMDTvzuu8xfiuo+MdR1GfdnbZ+70mkhq6aeDkznJZcfGltfBkcrOmMMsebPtyXPO6u3TeHHn2yQQ4CM32EyD7Igp39kBd0Ak+aECrp34ZWaNL6fcYuf/wC4VlJ7G8TtC2KBgIOM/aP6kPyHkz7lh1bTjeO6ra3LTD2UiBrcPSJRztfBb3u1ay4ufMlx3JPM/qukc7S5b2CqbWDyOx5WdOkrvP2ZOpdXKma2ZXv6h/YWOO8NocfLSuohsDtqGxXLKarpH2+Zjb4BsPVIqCFoRx2QWDtugGp6hAv57oLkIBnlBR4QKcgtpKCnIBbwgs8IBQCgFAMcoKA5QUQgDsgEcoI6AgEmRCAIMhBHIFwCUFOAHCBdQEgQs0TtCCnAkqwUeFQpyChyg//U2WfLDtl+YfoFU6YnczPCDMouLfL/AARk10gzGyACdpCxkI0uPI/RWB7jIA7jb3WhTqctIHJQIa0HyaeOZUHh5iw5tzZVG6ZkLUknkk3HKMk4q/JvUCiaxNO0u3eE8dvNwV9noefd0+X1GD69tKwr0mPadnN1Ar6GeOnz8busuAWCDvC4bdrijYILTyRCtwuTPLqcfy/icO+0thPiZfw3FQJNrX8Mn2qCJXwPiWVysxs9P6r/ANOOXLix5MLPOV2Z9mk2NfLd/TFBn7Qo3Xnqx5ywt2/RfV6TLH7uY6nh4ft3n1PF1GHdbjjnv1fyd0BhzdxJMTHl+RC9fJ+Hw/mPNycd1jvd/N81/aRxa/fi9hgVzZtZYU2eJa3TGk1HuPIX5fqJcrrT+9/Yri4eDimWWeTUsi9Hs4Zpq0bz7v8As3DGlr2X1caXugz5W8g+5Xt6TpfO919r459o+HiwuGOGGX62eX2DhtF9nZW9nXrGvUoUm06lb94tESvuSaf526nk7ua5z6irsD2kHeQR+qmU046cmwoOwDqbeWerTb4rS8Wm08a2+ikyNOsU9wDxPZdNaQ+S3gpaLBMRM91iLHhZ2zKzKOUcYzI+NWG2tStQB48aIZ/+RCzVfmTj2P4nmfFrrHMXrvuL27qOq1PFcSBq3AAPATDzVvll5TzVjOTsbtscwi5qUKtCoH1GNcdD6YPmaW8brvMI45XT9MsoZio5syzhOY6OzMQtmVt+BU0jUB9ZWLjpZdvcJJid1hoewEhBQPda0GTIk7JoA5oezYSQVRzPqEDY4rgmMN8otrpjXO9nbFc9DpFpUFSi2o07u3+h4W5dk8XbI1Egg9xCsdvvfkuOp5HJM6fLJk++0Le9+Hj4eCY49n4t3flZENBDYn9VzuWnqxmcz7csZ2/mIFsxMiOwSZbcZMvvL2TxPKVatG2pOr13tp0GCXVnkNb/ABUzunbp+Dk5ebxPNeTZ5py5iFwbWxxO3r15gMbUaCf1XHvr6XV/Buq4f4vbv969qOw57dx+q743b43Pc+PKSz2HUHGO3YrdjtnjMZKgmduFlMpqjcS2EZCRLlYEXW1N36qWDUcgMAp4vvziFb/JYnsboBBhbEJ3QfD/ANrbE6131Jo2NQ/1ewsKbKY7Ne95JcP8XH0W9OLgLhEhVnQQxztm7EnS2e55P8FmZeW+yL92/CeJ9F20zfDLw+6qWN5QvaJIr29SnWpOby19N2oFefP26YXw/T/Abp1/gmHXr/iuLahVM8y+m1xP1JWW2eQrKKOwlUXOyCnGAgW47SgnZAMoKc4+iBZ35QWDCCnGUFMQEQIQB6oBQCgiCcIFuMoB4CAJ3QU7dAOw3QQzsRwgF3KBPiN1RO6CzuCgGdlNCoTQsQAqFuQKdPZUSO/dND//1djd5hC/MP0AqTQ8jcyNkGY0Rv8AmCMr8V5JbGw3JQKe52mQNljIPtnGOFYGloa7UTC0Jq1EaT+qA5a9u4iOSO6lS+mJeUNVBw5lTfhJXD+ouFvo1WYhRH4rHa59C3hdel5O2uHJht9A9KcyHMeVLG7c6bim3wqo92iF+k7tx8jt1XQmEnkR7KdpsyAHSrx8ms9JhhrK5VzDrzc4U/IF9Z3tenTvXFrrWm4w5zgewXwfiuc7vD9t9kfvs+q+T0+bcpZ/zDky2vmYJVFL71pLyRv5D2/VeHpOa+n91+LfC+HruD+N4vHvXqe32bkzGDmLLeHYtVYWVa1BviNcILngbmPmv03Fe6eX+Zfinw7j4+ezG/X82TiOXsFxe6tbvFbKnd3FmD4D6vmDJ9lM+nl8nH8S5eKfd4vWYGtZpY0NaPKANob9F14+PteDqOp5eS+apwLg0nkcgbBXJywxv1E4eVW+XRyTqdQdhuJ4RmGhIfa3Apvd/wDbcVy9K6fYXNO5s6Nwwy2owEevC7z0wyzJAI4PCxQQBkBZajSusmB3WYemOY8KsmF91UtvEAHOmkQ8/wAlmj80IqNP4gio0lr52IIMQR6pj7UTHOe9tNgLqrjpY0CSXcAfVeqVwz9v0KyNmnK3S3pvlnBc44vRscQFq2q61qu/FY6r5w3SPYrnkuLouWs4ZYzjQdXy1idDEWU/7VtF0uZ8wYKzpt7+lsD3/wA00A427qg2TEFBBADgO5QaN1NsRcZbungeegRXb/6N1ij3sp3gxDAcNugZFW3Y6e/CuI9wkE7LQY2QJbue31Ut03hhc7qOX52635fyhiNTCbaicTxCntcaT+Gw77avaN15c8n7/wCC/Yzqut4u7LWr+uv+C8jdcsIzZibMHvbb9nXtbe3IdLHHsCfUqzOOv2l+xXP8O4JyYas3587/AOHOOvmdsVuczVcqWtZ9DDbBrTWbTdp1veJ3hceTkfvfsD8E4ubgnLyTz/8AP0cftL+7srllzaV30bhhlr2mCuXc/q3J0PByZdmU8f0fX/RvOtbN2VWVLwh2IWD/AAa/qQBAP1Xu48n+Y/tn8KnTdRe313eP2b+71HC9L+f8uW5DAZEt4WW6oku5RFE7ytQY10ZpO+R/kpRqnT/+zxb/AMwr/wCSx9RuhPJWgBJ7IPk77W3T/Ea2K4fnzDbepcWb7dtpfMpt1aHUyS1xA7eY7re3PT5etbG7vLina2dF9zcVXilTp0ml7nOdtwN4TZp2DOmSLbIL+meD4vb024rXDrzG2ES4ePcNIY72DdlNfU217rJ07xPI+csRpus3U8Evq5ucLrsaTR8GsNbWSNgWzBWu5Hh5ByVi+dcyWWCYfbVHtq1qf3uuAdFKg10uLncDZc8vbWL9JrO1o2Npb2NAzStKTLem71ZSaGg/wWWzirALjsqLPCAXnZAB3EIJMDdAIMoLI7d0AERugqO6CiNkFAGEElBXqgqEAHYwgg3QRw2QLI3QKe7eEAiZj1QXE7eiAdu/CAidtkC3coE+C3Vq7oL4G6AT8KARxugouQCgGN9+FYJCo//W2J7HBfmH6AFPxA/ynYoPQpTqlyMmVIJDQInugVUIp7cwsZBlu6dwrAw1tT/DLdud1oDUpiZa6D6dlA2iC5hb+ZEvpVSQ3SQump2uUaRm/Cm3VhWBbJI22Xn45qu+M28noTjxwnH73K14dNG6M2wJiHD0X6XhymXp8Xmx1a+mWSQJ3Pcr13w8kvk0BpIBXPHj3ltvK7mnKus3TXEM9U8NrYK4Mv7WpoqOqfB4LvbuQV8TrenvJm/efZP41h8Ptt/4/wCV5G6EZcyy5l7jIGLYq2NWv+yafYL28PRY8eM06fGvthy9dyXHDeMx/OTz+zq9GjSt2tpUA1lGmNLKbBpa0egC9eM16fzzludy7rRggugnjlXvu9NzKSdw57DdLk195sTYPzWpNudzQeVu+6Yt7ah1Cwj9qZdvLaB4pp6qbvRzd1jk8E8sLpji7MUyzamoZr2s0Ks86mbFdMfUZrfKZ1NCzRNU+UfqsxqFXt1ZWVlWusRrU6FnRE1qtYhrA0bmSduBws0fFXVPKvQzMuLYli+Us108OxJ/iV69kaZNvUqQTLHAQN9lMVcGy3jX9HsdsMbZb0r1+H1RWp0KomlVfTPlJH8V6Z6cMzcxY9imZ8Wuccxi7fd4jfPqVSCfIwaoLGA8aRELn9Vxex09z1i+QszWePYXcvp06T2NuaIMU6tLUNepo7wummn6c4ff08UsbfEKMeDe0qdwwegqU9QCaU7usBgIhBB3QeRmO0F5hV1QcJFSm5sfMQsUap0nu6rsvusax/Ew+4q25B5DQ7yyriOhQIBPqtDDx2//AGZgeIYgw6X21vVqNPu1hI/iufJ6ezoumvUc+HHLJvKe/wB3wddXVa6e+7rvL7i5e6rVedy5zzqP8183K7r/AGfxz+y9Nhhj+Xsdpc17W5o3lF2mrb1G1aLgYMsMq4106zp/7R0+WOfmWPazpmNmbMx1sdpsNH7xSpNLD3qU2QSVrO7vh8f4J8NvS4SStfHmKYx97lk5fE9voX7MjqofmJg+D+rkE/CC7UD/ACXXjvl/EP8Aqbx48ePHN+dPoNw7Be+en8Fx9TaNJAhHbKaolGVHhagxrkHwnfL/ACUo1TIBili//mFf/JYnsbmDOy0LgSgXc0aNzTfSrsZVo1PLUpVW6qbm/IoPFw3J2UsIvDiGEYHY2d5ufvFO3Y2qe53hB8SfaNzXSzD1SvPuNYVLbB2ssqNTVM1aZ1VIPpqXS6043e31/kbEsE6jZAwTE8QoUMRp3Fqxt3RrtFVorMGl4IPEELz+dtyNlwrAMCwOmaOCYZa2DD8TLWk2kXD5jst1t6AM957Tx9PooIVYBdwqLPCAHcIBQU7hBQCCygp3CARwUEPAQUEC+6C0EQLdygjeUFu4QLPKBW2pBWmHgoK9UAA7oCPAQA7lBSBb0AnhAPZACCIIVYKVH//X2J7naV+YfeFbtndBkhjpBB2QMeSNyeECiNXmmZWMlh9q0QCeysZplcbFzeVcvST2x9Z8o7Ssxpn0/I+T6LYKrpI1eqg8XELcVmOpn4VRx3H6VfK2ZrPHbMafBrNe4/3Zgr2/DcuzKvJzzcfVuXMXoY1hVviNB+ptdjXbe4X2+S912+PrVezI3jkbJWkaSRuiCaJKv0rN9wRbzHO0fquPH7deT08LNmZLbKuHNxa8b/VPHp0q7vQVHBs/RM85MvL3dB8P5OtznDh+K+fW/T2LO9tb+hSvbCs2tZVgH06jTOx7LpMMcvTydZP7Nn91njq/myQIM+qzlncPDGPBjjO63YtK6ytXDHJh4hQZXtn03iWkOb+oXn5I5X5PTlfTqqcHzTjmW6g00nv+8Ww9nHdbwvg3vy680jgcdlc4CYAJnk7fKSFzxy1Go+RvthZ9xJuJWOQrCs+hh3gC6xAUzDqr3u8gPtAMrPuq+TyA/VtAJ54DSPywu+OOmaOnTfXcKY1F7iGFlNs6SeP1WkOuKFzbV3213SNvds8tSi8Q5hbyPryhBW9GrWrNtrekataoQxjBy5zjAA+ZW8a55P1Mybh1bCsn4FhtyS6vb2NGlWefiD20wd/lMKZVrF7LVzaR8yEB0j2d9ECbxpqUy08QQf5rFHNshvfh2bMwYPUMNrOZdMHqHK4jqTXS0SNt4/VaCMRtKWI4fc4fWEU7um+g4+gqNLZ/iuXLNx6el5vuuXHP8rHwrjeEXWBYpeYLeNLLizqvpAEbuptcQx31C+bnjqv9kfDOr4/iHRY3Gy3UeYHE+XsFqR7+Lkys7b9Fqu1+XyJg7qLx8e73Pqj7OmDPs8nXOJ1mxUxO51Uz60qOwP6yvRw4+X+av+pfX49T1uGE/k8f7uxHkxwvoZen8v5cPniwuWPpne1OWhG8LUCrn+zf/hP8lKNQyF/Z4t/5hX/yWPqNyZytC3coKc7yoI3YiTMEk+7SIKDUrjpnkGvWfXq4Da1a1RxfUqOYC5znGSTsg9rCcHwvArQ4fgtoyytSdXg0hpaCTJMD1KD0Z2PtsgocoIeUFO4QUeyAex+aBbkEDZPmQFxsOEFHlBJhADnIKmUFFAJ4QU0oLQLdygEESdkFlAt5hAuZQUgkwgGN5QWgW9u6CnFyBe/5kF7duEAnlADuUAFBUwrADnKj/9D33vLed1+YfeMtnuPZBlB5BMhAJJqSRwgmmWAAwR6LGSw2jqbDZVjNZJPlgCSrl6SMUtdqAI7rMaZw83HMLYkDQdf0QYlanr4EINFzxgf3ywrNDZIbIPst459lZ5Mdx7/QXML6+FXOAV3TcYa4Fmo+YsJ4+i/RcN7sdvi8uOnbmNPMnffdacIY0+qKawclPon1XIkA7HeD9FnCNZuY9faxb04v2bAPr0Gie41iV8T4ly3HG6/N+7+xuN/+oYdvvWX/AGcJ6Y9XsTyW6lh96513gZOk03GX02+rfRcui66zxk/qHxv7J9N13HcsZJn/AJfL6nyznLAM1WjbvCbxlbUAXMLgHMPoWr9B97hnNv4T1vwHrOgzuOctn03psTXTBjyngkwmNfGnDy8ftVZoLHBu3/FMo3L3OL5qdUy71BwjGmjTQvHG1rEbDfiVzxvk1p2K3fqY2D259l3oaJkwdz3+QK43FqPjL7Y+WLu3zNhObWsJw+/tm2lWqPhp1KROmT2mVmeKr5mcweUVZ32eBudjsZXol2xk7R0TyhWx/LPUHEzQbUOHYZTqW90WyGX1J+vyH1DRuq5bdVZ0jyz9oLKeH56wC7bhWa302UcZpcUqlwwaQ4gcFwElS+msfbZuln2V8OyfjdLH8237MYvbQh9nZ026aGpplrz6lpEq43wmT6RDpEkc/wCamVawAWxwo0F87fxQENwEBVo8MiNyFijl10DhPU+yrny0cQomjPYlvAVxHUmkhu/w9loHJgkzpjeOUs21Jvbj3WvpdWzJSOZsBph2O2rA29oN28ak0bH3cAvFy4P6h9gPtNl0vJePmvy/rXy85kOeHMLH0yWVGOEOa8cghef0/wBHcHVcXUY3PDz4QjygqT278vnilZFtaVb65o2FoJr3NUW9L1Je7TKrx9b1U6bpe+vuvLGC0cu4DhuCUQG07Siym8jYawPP+rpK+jhj4j/HPxTqb1fW5Z383rOEbfyXSvnd8y5rF9kcoCSUUxq1Ai6gU3fI/wAlKNPyCZo4v/5jX/yWPqN0b6rQokSgA7mBwgIQBHfgIIgF2x2QQIBncoLlBR4QVKCkEgHsgAkoLCCiQOyBZMlBNu6CS1BUfogByARyEBoBgFBR24QLc5AskHlBW3ZBNhygB0z7IKHCCIK55QRAt6AAOd0AoKcgUUAElWCKj//R2KozeF+YfeMtmESZHsgyAwlup0T7IApk7gAEIAPiB4gbKWDKptLiD6IDcS2oC7YcKZJoBcHVDB2bx7qRWVSPfutinvgQ4EoAeNQlvCDGxG1FxauaW76Y+alnf7a9xyfB71+S+oltcavDs7x5p152b5tgvtdHzX8L5fU4SPqm1rirSpvB1AtG/qF9TkwmN1HzcfTKEOM8LjWhg7x2Vk3KY+c8ZfVMiZAEmCs8Hzb23lJ3WX1HEvtKYpSt8p2OGtd+Jd3QIH92mNU/qvz/AMSus+1/T/sFw9/Vzk+mMv8AvHyvJDi7ueV8aTT+8SM/DMWxLB67bvDLqpa12GQ6m4gfUcL1cfUZYPPz9Fw801njL/SOs5f+0Rm7CmNt8VpU8So041VHbPOrjjuvp8fX53LWo/A9V9jOk5O7uzykn5afTOXMSu8awSzxW7tjaVryn4n3c8tBEiV96z5O5/BOq4MeHqsuPC7xxvv6tI6uYaa+APvKO9ayc24Y4dtJXnw8158vbbMl4nTxbAbC+a/UalNoqH0cBuvTfSPfMgnvysTI28XNmUcFztgVzgOYbdtzh1w3gxNN/ZzfcLncV2+aK32MyMUa6wzE1mCBxOirTJrhp7AjZdcIzfLvWC5BwHIfTvEctYNTDqP3S4dc3DgGvrVTTPmcueXJZU7XyT9mzqZWyNm5uCXIfVwTGHClcNDS7w3ydLgOF1nmEmn3qxwDWvBimQCDEiHCRASeCzZ7XNOwUvlZNKMFFCQgJrdggInaPUQpocs6msdY3ODY2wn+pXjNZ7aHnSZ/VT0OmWj/ABqDXzLdtPyIkKyjLbtwtxLlZ6QkbEidI49Se6554Sryb49ZS6/ZxDq/0cbi7a2asq0QzFNJN5atgNqhu5IA/N6LzZcUj+w/ZL7V58PH91nqy+Lbvb5srMfS10XtLatN2h7HCCHgwQV4vO3+gZyTk6aXj8+nTeg+Xf25ni3vK9PXZ4Ux10/08QbNBXoww2/nv25+JZdN0HZPdfWknTHIO8FfQnp/mOZ6y7h6yQPVHHHHt5Ln9avWS3jdG1gCJRFh8A+yoTdkeESfRQadkHajipG4OI3E/QhTQ3UEBvuqBI7oLaPzIKcd5QSZQUd0FcIK7+5QWQAPdBSASYKCHiUFSgFBJQFsRui6Kd6ozQA6pnsi4+U2SgiRCkqFughVQjkICLoQTb1QC7iUa0SdyUNAIAEqWogU2inLQo7oB42QQ7BAAcIJ9EEDpQCd0CiNzugrjZBDxKBXJhBRbBVgqAqP/9L3qlWSvzD7xlHUdwPmgygZZ3hAmkXB50/D7oGOeQCT6oMmg9kDVMoLutDY3n0hZyGOwmQ5qkGZTquiCIK2LqAkanO2QI1PYQPyFBlGnrpGPRXBY5L1LwZ9S3fd02xUoxUa8ch7TIXfg5O3J5uXHbsfSnMrsfyvZXFR4dcUAKFwDzqbtK/Rd3dNvkZ49t06MwyVisI4w6Uxvis3x5LrXBYWsZu52xjsCueN7G+efJ4918rfaHxoXeabfBWP1tw2iBVb+7WqGSPoF+W6zk7uR/ffsB0N4ei78vdrjq8j+mwbduVHSN06YZaGas8YXh1Ruq1bU+8XQjYspbwvq9Hx992/JfanrZ0vSZZT3X3D4TGNLKfkaGBjD6BuwEfJfpf5dP8AL+HJ33LK+68TMeHsxLDbm3e3asxzHjtuNlx1ploPR6+qW9riGA1visLhzWh37pMrrLsdXDe3cKUFIEDv6qC/MCPMSOxn/wBlZdBF1atvLW4sbiXULqm6lULTpdoeIO/yXKzdGtZV6ZZGybRbTwLBrelUAh1zUYKlR8GQSXcFdsb4G5Bx+kmT7R6ILaA0bIIZCCwZEoCDoaD2KCyZAhBpXUmwF5lfECG6n0qfitHeWmQsZD08k35xLLWH3ZMvfRYKgP7zRBVg2Np2Wmcrpcn1R6s8ZyYaMcBBMR3BCzZ4eHjx5OLenCOtfStt5Rr5vwCiG3tsNWIWtJu1dvq0DuvHlxv7f9ivtX9zjjwct/8An9a9L7OGEttMr32M1GgV7+48Km71p0xx+q68eOnz/wDqP8Q++58ccfWnZTER3HZel/JZ6V/JFECJ34QVq3j9EE33CBdyJpOHsg03p/qFDFgeBiNx/EhBuskxHCAidoQWNhCATugkGJQSO6ATvugE+o5CCwdXPKCeyADuUFk7IBQDIQRBc7QjULJHwz5v3e6MVzTF+ufTrAcSucJxfEKlriFq7TWovpkEH/3TTG9Mdn2g+ltVlWo3FzpogF0sIkH0WpGpSj9ovpR8P7XcDz8BSrsP+kX0o4/a7v8AYKyncn+kT0p/+rO/2Ch3IftEdKj/APy5/wBgodyf6Q/Sv/6uf9gobUftEdKiI/ax/wBgox3F/wCkL0tk/wCtj/sFDuUftCdLi3bFSf8A0KfVqZLZ9oLpbx+1jMwBoMk+ytmm42nKufstZ3FV+XK7rqlR2qVdJDASYifVYxu1sbKN1tAd4QWTI27IA3giECxIElBczugA7lAJ5QUeEARuSgondWATwqP/0/cADnR39F+XfeZVGm7SYP0VD2S0Q/hABZqBLPKPdAmSXBj/AF5QZ4pt0beiAajRAHJWaBpOAO3/ALKQZD3NaAYk+y2A1Hwy53CAA8lzacfVBmUnlrS0rWA8LMVgy/tKrAJ1SHA/JcN2ZJppfSDGKuXc3XmW7l0W15L6AOw1N7BfpOlzl45N+XyeqxvfX0vRqaoIPlI2Xoymvbx7jJ1R5uQsT2WPMxi+p4Rhl5iZpuquoU3VfDYC5ziwagAB7hcuq8Y+Ht4sZnz4YX1uPhbMd7iOI4zd4tizH072/qPr1fFBa4Fx2EH0C/H5TK5bsr/U3R59Pw4cfFx542dvnV28v0Prwpt9iWUYnsJjsq6x9B/ZiwfXe43jj2y6g2nbUXH1f5nQfo1fpPhWPy+X8Y/6h9VfHHK+kpcSZC+pfxP4nzy8eeMnq2FXLQ6k4DnZZyjtn7unG7F5y51SfRdtbYvTLWjtrHdZwvll2Si8uaHHgj+Wy65ew/b5z3WBZbAkfooIPU8q6An4RCAwTAWgz8s9kBFwQAT6d0BAukjsOEEk6gCgwMatvvNjcUYkPpvaR6yFnIaZ0ouXtwi4wuqfxcPuH0nT2BMhIOitVp293gSRMOS400kaeJ9l0jpnz38gwHtc10EOEPY7gtPZc88Ux3jnjyz28rBMEs8u2JwzDmabM1KldrP3DVdq2+UqYx7fiHXZdTy47er9Z91p4aiIiCb6hCAt53QKuifCdHof5INNyAXeDi4P/wBRr/5IN1bwgiAkAoITsggPlQASgEFAbeUFHlAsEz7ILQRAs8oCQRFCfMCATt8Ubc8EIzXBftEdI25uwp2asv27RmPDWl1djBBuKTeZjkhd+KSuGe3yLlPAf6SZhtsu171mHffHmma1wXBjXj8pjiD6qZzTWNdku/su3uHPZRvs34Xb1HCadOu51NxaeCJ5Xnm7XXL0xj9mszH9NMHn/vT/AMVqefTltP8ARskf/wBbYPB4PjH/AIqS79Ch9mlxJAzthEt+IeKdv4qpbpf+jVVif6Z4PETPinj9VdVbdB/0bXyR/TXCJHP4p/4qTz4i68bV/o1VCT/+ssJM8HxOf4pJu6XXjZOK/ZsvcJwO6zFVzLh9XDLVpc6rSJIcR2DuFMfm8zzGMrMbquV5PyvieccftcDwprqz6tTS+s0TopAwXmeF0zs074P0AyPk3C8iZetsFw6lpdTANeptL6hG7pC8vFvdbybJOnU1d2Adie6AZ5QDqKCO+FAA4QLJIKCIISEAeqATyrBR4VH/1Pa0efU0r8pH3mWzYSDuukDGvdHmE+hREFRx2IhRuKcBqaQqVlsdAHp3VZVVl5JbwoEMY6dzPsgzG05ZA2JCBTtTWFp3hGg0nulroQZzBLo9QjLHuaIdTeCOxCDjmcbarl/GrLMNnLa1pUbUPuyd16ekuuTbz803H0xlfF6OM4RaYhRIc24pipt2JG6/R8uXdHwcsfmex4pZse644eI9Ms0cAKjC0jZw4iZWMsducyuF3Hj4nlHL2O0H0MRw2hUEeYmm0P8AoV58ul75qvr8Hx7m4P7q2ZfppxjOf2drcU332TKxbWcCXWFYyHH0aey+P1HQ9vp/T/g3254scZjz+/1y/wDZwDFMMxLBb6rh2I0KlrdUiQ6m4RMdwTyF8q3LG6f1LofiOHW493FdT9PL6h+zdbNZki4u+HXF3UH+yAv1vQ46j+E/bznyy6x2gO2Xsy9v57le/UoC6fKRsVvP01rXhyDqzavsK+GZiogh9hXa4ubzoJgyvJjNUdRwW8ZfWFC4kGlWY17D7ESvTWa9FsRA4CiDb6oJq1GUEjTugmpA1vw6UAeH7oIRpb9UDGmRKCjH1QKrhxpOETIP6Qg5tkyr+z8+Y9hL/LTutN0werjsUHUWTAkdhv8AJAZMQgIO2QA5BJdKAmuQR3Y+qAplBEEK1Am5MUX/ACP8lKNRyAZpYt/5hX/yWJ7G6jhaFIIgFBEBBAo8lBSCwgL1QA34UEQCgFBEFjugByAQJ8sA6toPERvPzViV8afaO6U/0UxRufMtUn0sEvagqXTKQ3tbgGS6P3SV334cNeWS24PXPpk5gqaOo2VGeV4dp+9WzRJAA3JhYxy7ctulu5pg9LMr9Ncy9OcVtsxXBts4UKlc0x4jzct0CKYDRuSTMrnl/B/qzG0Dpx0Xtss4Jd3l/Vp3pq0G31B9R/j1N/MKlMbifZSY/dzu/Nv3NBdlToi/qicJN1/qP7j4jbbxKn3dtwXCdwdWrT6rfLOzDG/m4dvf8v8AheBgeSekuKf0x+945UpOw+vWZglIV3SWsBLdI5P1W+XLszxn5rfn+b/CDDcrdI62ScDZd4u85lvLlra9UveajG6/NrA4Eeq5T+FvL62ulnmY/TKbYWd+mOWbvPuCZV6b4rWvKd+JxANqlwpAH4p7CJTK3iw7/wCbJMNXP7u+oR1tzhaURY9KMpvc/BcKDaF5UonUbi6Ijc9913wxnT8fZ+fn/VxzwvJn3f0dy6BdLqWSsv08YxGiDmHEmB1Qn/o2RAaPnyvm3zk+jjO2OwO2geggn3Xr+jnaFRE+aAXBu6BaC3fCgUgFzUF8IAegHsghVgE8Kj//1fcGkObAX5SPvMmNtgukBiWtEn6IiqjyRLQjcDReZh4me5QrLDwO0hVlC/02B7KAabT+qDJBdEA8IALiNiEaBQkEg8DhBkh5HmCMpUrEs3bseSg07OeE0sTw2oNMua0gT6Hsky7btnObjM6D5gf91ucr3Tv63hziaYJ5pO2H6L9L02Xfi+Ny4aruBY35gCN/VX6vLlvxoymdxP0WpXbGy+zCJeEuWp4S8M3vH2vvB77Fcrh3zyxlwYTza0vqD05wfPeH1aNekKWKNH9XuwAKgI4Gr0lePPpMfb9f8G+0HN0uWOOFutz6vN6L4DfZbyvc4FijCy7sr2tqP5XMcB5mr19NNR3+0/WcXU9T4dHa704XSecn4fk+XOaHMuj2Wsna1p2f8KOJ4Be0Ni51NxbP90LlYjzuk+KuxDK1G3qGa1ifu9Un4paf+C3KldDGxIHwzsVUMCCaR6wguQ4bncIKQXq8yA0Egn5IIJCAhpO/dBHbsLQeQUHK8WcMI6k4XeuhtO9pOt3H1dEiUHVWu2A1SAgMEILkRsgh4QDJ9UBjSgomfkgsFAex3QCHAyPRagXcQaTh7KUadkJsU8UIO37Ruf5tWJ7G7N4WhEFO+FBSC5CC42JBQLIgTEoA1SdxEIDJEbcoBkoLbwUFIBQC7lAUiEASgEoBd8J99iqlebjeC4fmTCbvBsWpeNY3lM0qoIkgEcj3HZWVLHwncNx7oF1Tkhxp2dYuIB8lxZVDwPXy/wAV0zw3i443d06Nj11hPS7qJhfVvCLVt9lLM1HxvDptDvBqVBNQDtqEwuHPfvda+jpWxYvjdahnCx6z3OUagyU+1bbueQ015eZbVDJ5nuu+X8TGYz6M71TKWOUcsZvxbqXmPJlahlvGaFJlldhjH1KcD4zTnbWsW/e/J/hXj+TK5X6vKy/f4LlHNmN55zllKth2D4+af7Bmm17Rq5Dmzs5y55fxfnn8rPHj2Y3G/U7CsXyzkK+zJm3OuU3YdhOYH+JgjKjGOD6YbGgAE6XOmVrk/i8mOvw68rctcd/ONdwi4temGQcX6ieEKGZ82VKlHL1uRBo2zydxO4gLtnjM+TX8scLb2Sz8Tyvs6dOamb8w1s44yw1cLsXmtSdU3bWrEyeeYK8nPzfeZ7n7PoYSSPstrQwBrRoaPhb2A7QmOBckJgR25XRzipHZFUgBwCAY90Ed8KAEAO5QUUCiSggQFt3VgBxCo//W9octMr8tp95nMqNDfMN1oXUgnfhDQCRoJajQ6EVBpcIPqESslrWtaQ7jse6IW7SAfRS0MtQ0ggnfspKH6dIPutDHcHkEk79kXa6bXEuIQ2yaMaYciIIaXN5b2lBhV7dlc1Ke5Bbp09t1izdX6OaW9w7JXUGyxH4LO8cKFw48AEwCvu9Hy9k1HzOfF9OWtdlxRZUpmWECD695Xus+v5vn45e2QIBkrNjncNnaiNJbvKSNdvjW1ubIk8rp3N/TSwRABHC5ZY9yY/L6UYBJDQCTqMfID/JaxnbHPLDuz77bsQO/ASTV26ZfNd1e4cSs78rtiYhRbXtntIBBBn67KZG3Junlc4JnXGsBf5aNwfGosPvyVOO7R2Km46R85Hy7LtoODisSiwSqLdHxRBCCDiSgGd0DNRQMaZCCudkFGn6EoLDS0KWjmHU2kbV2GYy3mxumF08aXbJsdLsqjbi2p1WmQ5jXD3BEpKH7AqiztCAxBEIFnYwgI7AILIhBQ9UBcoJAV2FVwDSdvGyDUcg/2OK7/wD8jc/zas6G6N9FRZ2QCd2oB1FBYEiUEJgQO6CBxAiUFh2xJ3PZBNnNng+yBTjEoLadkEQCdkAv+JBXqgEmAIQRvmJlADj5VRKfPfjspPBfLhf2l+nTc1ZS/pFhtEftjBWF0t5dbjd7T325C9nDl3Xtv5PJy/w53RxzpLcW/UTIGOdKMSdNzbUziOX3P+Nj27uaCffePdeLpfGdld+f5cZcW89NLjP3U3pziuTal7bWLsFBw59V7S65PhOlrYO3aOFePO8fJ+lTtlw39VZjxbqvmrpRXtzhtqy0w64p2lauHHxa7LNwGpgJ2aSPMt9NJx8ueX+Ksct3hjpmdS3dScyYflTIVzYWllc4xUpVvvzHF7adS0h3B4McwufTfw+LPH/FHXlu88bPTwM7HPGd8+4D0lzEbWpRsalK9uLu2aQ19Kk0eZ2qY2C1nb0/FMcPmuX5/wDDPDhOXLLK/wArQesGNVs/dQrbKOAy7DMMc3DcOpM+EuENqP8AnyvR1UnD0ss85Vx6W93LbX2HkfKllk7K9hl+1YGtt6bfFI5NQgF0x7r5vDx/Lt69a8NiPryvX3JotzisqsIIgW87FAokgTKCy+RCCiYCACZ3QDqJCADyggQQnZULcdk2m3//1/Zo0w9wA/LyvzD7zLhpb7g7IC1NcACihe7wpA3aRt80UVnWgkOHKJWU51NslxRCnEH5LNBUwRuw7qQP1mo2B8QWwl+rUIBjugbRqaHEHkoDIAcDvDkBVHACByeEAUpp6g4bnghdL+BGgdQcJ/aVhVrMH4lEh08EFpkELHT8l7nm5cduodJsyDHcr2rqzpuqA8GqPentK/T90sj5eeOq38+Zpj//AGVHMVNxESgYXHUPRAcE8IKcgJx3CC9zypJ5A1WTTcBO/CZwcZzk12X894Pj1IaaVZ/3esR6P23XLHwOyUKgqMa8egH0C9QeCuetA2kSgvuPRBbiCICBE7oHjcSgNswgjSJj0QEXTwgmrUCO4Uo03qLh7b/LV6wDztbrZ82bqD0Mh4h+0Mr4bck+c0gx3zbt/krBsjhv7qizwgjT2QRwMygs7gICIkbIAJ07FATSgp2+wQKuAfCcPVBp2QCfCxT3xC5I+pCDd2g7FAZ3QCdhCAIQEDAQU87BABKCTsQgJphqCj8Md0AzAQUTHKCyZQC7dxQUOCgEiQgjRBM+iAS0kH2QC0x3hw4QLuaFC7oVra4YH0K7HU6rTuC14g/zTu7fKXHumnwRjFvedF+tTnUHFlraXba1Ij4XWlZ0OA9RBj6Lp1ePZqxw6S925W9Zy/bWSertpe5VxV2FYDncUrl1Ru9Jvj6Wud6AidlOpx3hLF6a6zsrervJ2csPxaz6W2GZwMvYtSrYjUvHsDrkOB1PZz8Ljwsc11hjY1xY7zyl+hFfLfUrNVvXzNc5ho22KZJrVqNi2lTGiqLfZ1SpvsXgLryzWeMjlxZfJlb9Gm5LzFidplTOXWXMbxUx6/a6xw/s0PdsdE8Bb6afe8+r6jXFl2T92B9l/Kv7ezZeZuxFviUsOl1Nztwa1QySPXcrw9Tn39XcPp/7O3Hx9l2+xDJcexneOF31pupEBRAFp5QU50CEFQUAPkIAduIQTSgjuEAcAoABjZAEiSgpxhBUyiUtxRH/0PaoVNJBO08yvzD7zM1U5gCZ5hANSn3BgeiKh0Bnm3KKukaY3hErIewGnqbBRAB4ADXARCzQykGuMhSBmlrSDO3dbAOcNXkJhAdBwO55QZDhIYI39UCXtM6f3dkDqb2vGl2xHErNv0Z+rCxezp17V7Y3e3S4ey1hjJ5LNtJ6W4m7LmdrzL9VxbaXxmgHbAOb2HzlfY6Tk7tx8zqcdafRVJxdPoBH1C+hp4jW9lA8ARPdAYe1ADt+EEPKB7Q0gLUAvBgRwpkOZdWsLdd4DUq0h+PbkV6bhyCwyvP9RsuR8ZGNZbw++G73Umtqf4mbFerH0Npa4OHuOVKIsgnElAEHdBTPKUD2kQgNvCC2hsmRugjoDfKgGYgDvypR52K0PvVlXoES2oxzT9QoNO6U3LqeGXuE1Nn4fd1KIB5DZkKwdGBkk9lQQQVwUBAg90EkeqCwSgjhLZ7oBb7oL7oF3HwGEGm5ADvAxI9vv9x/vBBvDeAgKR6oBcgFBcIBfwEAoLHBQUSYhBUngoIUFP7IJBQASZKAWk8d/RBYO26ojtt00bUe3ugAx9SgomOe3I9k1s3Y+X/te5TZVw7Bs40Wee3f9xu3NHNJ27ZPtuvVhrlw8vJnLx5+GiYtc1c7fZ+w3FwS/G8l3f3etUH9p93cYZvzsS1cOnvdvHPx+W3bqZ26uHn9nrUsGtrHprZdUaWcbh+b6DGCi99UONNtR4a6loneAuPD82dwvuNcvyyZY/V6nVKla5O6dWV/lHMdapiGYX0ziLfGDvvT6wBc8AHYSV6OK92Fzy+jzck1lJj521PrRcf0ayDkvIFA6ajqAxG/A5dUq7gOHzlTpbePjy5L4u7r9Tqfx4zHz+z6A6AZYblvpzhzalPRcXzvvdckQ4tdOkLw8E7t8l9vrdRJMJp1Bpc4z7THoF6dvNVl23zVRN4E7Jo2W8EQTxKgIgxPZAp53b6FBIahtSCnIFu4QKcYKBYaXEmUBwEEIAQpLuUZf//R9mi3xBqX5h95l0aZCAqrTyTsikPgiBuinMZ5PfsiUwVCKQb3B3RAO0mJ5RYZSmnv2RTSfID7oFh26BlLlBkF7iQW/COUZU5++ojb1QGA17muA3QMrMLwdMFoEH5qjlOe7avhOJ2uYLMaatnUFVxb+6CJXq6bLWTzc+O5H0HljFqeNYRaYhSeHNuKbXOH94jdfoMrvF8XPHVe80SP8K5Y+nTfgxvCrCnGeyCN1fCgscFAVMwEDWGZCDxsfsWX2H17Z4BFRpb7x3XPKeRzvpHfOtf2plyqS19lXcaTT+48yP5LrL4HWGeUn0MH6rMga06jCot5jZABdsgjeWoHFsFAQKCy7dBNWyAZ7/RagXWbqpuZ6jb5rNHNsqVDhmfMbw139neBt1T+fDlmex1BpMQOAVoNHCCkFN0oC2QWOEEcYagEGQgiBVwYouQaj09M2eI/+PuP94IN37IBdygmpBEFk7IA8pcA47OQUW6SQOAggQQmEAEy5BY2KCi7dBbnbIAe7ZJCwtzmspufU0toNGp+s6QPfUuk43K1zLN3X3pzk11S3r4gL+9ZsbayHjOB9D2CuteCXblGJfbEpNqOGF5c1U/yurVYcfpGymxeGfbDsqjw3FsvPpU581W3qy8fIRupV26/k7rVkLPBp0cOvxb39WALO8ApvJ9NUqNyugO2Ghp9PKNx9D3ClataJ1dy43NXTzHsLc3W40X3FuO4qUhr2/RdOD25cvp8r/Z7rtxa3zZkG9dFHF8OqVKYPavSBAd+v8lnn/Hj+8OHxjf2IyHfZDo9Pc0ZfzPYVamO2lebSrD3kAv0SANhHO6THXVd37uOF3i2XOmH5KzBnbIOXcnW7qdR3hvvWDVApCOWv27dlz5L/wCE7fz26dPPmax1Truzl1tGCWv4lvbV6GH0QOIpEav5ldufP+DjP0jPTTXJl+77Zw6ypWFjbWNEaaVvSZTa300iF4+B7uTLyyKjm0galVzW0miS5x0ge5K9Uw35efPk1XNc39d+n+U/EoV8QN5eMkmhbQ9sjaJCutMd23M7z7XmGtM2eAVHUjsHVH6SR8ktWMrCPtY5Yuntp4nhNe2DtjUY/XH0hYrTrWVupuSc4t/1RitJ1btRrO0VJ9AFyqz22wx8Tdo2jkEHuElaoHLpMXOxNSNhHJQDViNuUCoJElBUQEEHZBHcqwKcY3VH/9L26IDfLTMr8w+8yC+pTIB7oCcdbTJkorDOpneRKKzbZxIk8olN0gOmNjyiMdwcNR99kWMmiR4fm3RUDXaQPfZBT54Ag+oQMpNGknV554QZIaGNMmUZK1giO3YIDpOLNxuUGQXgw0DnlUazm2wF9Y1qGlpljmkEcypM+3KHbuJ0Kxp7aN7lm7eTXw558LUdyw+nsF+nwu8HxefHVduYZAc13lLdx7ypHnNaYCovVPZBbQdaAnARwgo7DZBGvjkoF12NezfmD+izYOOg/wBHeqFNzvJa4szwvRoeOD81mUdlpuJY09nCf02XbXgGDBlZEc6RzuggIIjugNoQHJI3QXI0z3QEeyCjs6fy+iCCPotQVUgtI4Mgz8lKOYY7/qrqFg1+PLSvg+3eRsPNBE/osT2OpUHtcwOPLiXAew2WgyfThBJCCBrZdugCe38UBiW7EoLJkIABKA5hAm5I8ByDT+nbv6niP/j7j/eCDdg73QWgpBaCHhApzZc0oGGAgoEIKcQQgA+vdBTigpBf5UHk5jzJg+VsJuMaxy5ba4fbtJc93LnDhjR3ld+3Tj94+LupvXrN3UbETgGVm1rHBKjiy1sbaTdXEmAXFu8FXvkLNjy/9nu6p2lLHupON0ctYc86hQqOa+7LSJPlcdyuWWW6smnoO/0XMA1W7v2hjtantUuKby1pBIEkHYbrOwpg+zFmTTbURiGBXJJ03DqhewN7Et4VSvGzR0HxTCsM/pZ08xZmZ8IpDxtVmQ26psnkhu+yLK2no99pDEcEuaOWs8Vn3WEBwpW1+6TXt3cRUncwpfTpt9YVcRwy5weriLLinVwp9B9XxyW6DSLTJJ9+IXTg9sc3p8LdHL+3tuuFr9xOmzuLm4otZ603udt8hK59VdWU4vwZfs305X6k5SxHPuFYNlF1/huYK722t4XNgBx1S0dl25LO7DL84zw4fK2vLOG5yzF1Hy3j2YcqnB6GD4dVt6lxLSH1KbfKT6FcLjvHDH86mGXbk4xkPELP/l6be4y6KT8UuCHOPDy4gSVepmvH5OnDPNr7gxvHMMy5h9fF8XuWW+H241VKpcD2kNb6kq8HH4Z5c/mfGvVDrzmPPt+cCyuallgz3+FTbRJ8ev2Exvuuty7fDNx7rsvA+gVanhrMd6h45Ry9Z1RrFKqQbl077tO65XLZMdPQOG/Znwio2hXur7Eaghj7hjnDW71A4SOkMOUfs9Zkf4WD41d4HeVNqbbmXeYbcOUqtczD0UztlJjsfyrdtxrCqX4gvLB0VGt9S1vJXOzZW5dKvtIXuHVKWX88l1azJDKd6ZbWpEbaak7q9uiV9U2d7ZYna0b+zri4tbhgeyvS3ZB4G3dO/TejnfCtIEHb3QC7cIBQV2QAUASZMlWATvsOVR//0/UtyW1AeAvzD7zKqPe54aBLT3QV+JSG/HuiqbUDvn7orKotgSUQb6h3A7IaLc7UPdBktpFtNpnb+KG0qO0ERwENkOqNmZMopjGwwaCSSZJPKDJIdpiZBRkuN4QPp6mvgwZQMHxEe+yoxb+iKrRq2iSFm4S+fyWZac2pXD8oZ6s8WYSyzuHeDcEcHWZBP1X1uj6nLP5b6fO58JX0paVm1KTHiPM0O9vNuvqZSS+Hy/V0zZgCO6y0tpk7oGAw6UEJJQR3wSPiQBpncoDeA5vz2+iuvC6cl6s2JoMs8coSKlhWY6e0T3Xml8mnScCvad/htvdU3aqdWm1zD8xv/Feq+h6XKygTI9IQWwwZQOHEoJJiEF9oQFJQXJIgoBJggK7FwNJ/VQc16pUjSsrHFmbOsrinUJHZuqD/ADUvgdCsKrK9pSrsMhzGx9QCrKM1vMdlrQoneFkW3ff1QU7seCguZCCxxCASSHABATohBj3O9JwCDUOnUfcsR9fv9x/vBBu4QXKCtRQFKC+QgAmI9kEJndAI53QWYB24QUd0Au7KxKqJ2HPZZyuvRLtj39/a4Zh9bE72q2hZ27HVa73mAxjOSfn2XTDHbnnnY+FOpvUHMfWnOtPA8AbUfhBreDhNiyYc2YNRw/iunNn2+Z+FwmDe7ajlvoRTs8BwejSzB1hxXSwNdpc20fWGwB7RK4T13Z+vo6zKw2p03q0s0NxLr7ij7u3xK3dWw2hTe5zDcDd1ItaZDmtOy5zculuVrFyVlurjeTM54VlbAMPuMIt7m4p2F1iLAy9DNAOkcHYbgnuu+jbz8w4p0lxrKuS2XmB17R1vc07fFn0bctPh0xpcC8DzanBSM2vPpnEunuYMdzb01un2uUcKqUPFwTENVN9xTrDz6KbpJZKVZWN1Nyjl/PGV29X8hUfAaHEZkwSmPNRq96jQO0pPLVunK8NzPni5sqeVcNxG7r2tfytw+g5z9esSABzsOy3f4c3Gtd88s7C8p9ScExG3xTDMBxK2v7Z5fSuW0DIMLy5ZXl9rj8s1G7f0x+0XIJGLGAf/AIc8nvwt3O2Yz/DNNY2YzSf00+0UWOa5mLQ8Fr/6vyDz2VnJZcb/AIbtyvHjvbntfKOfnXdbEq2C37LgVPHqXApODg8nVI955WplebK9zWV+7ngnFc65vxy1p4RjGJ3Ne1puNNtrVJEPbxI9ZWry5cfiJMJn5ruGQcq4X0ty3a51zBZm9zfirQ7CMM063MaTHiFvaFzu+T5qb7fEexe5TwTGsdvcX6i4xXx+wrWTrqi61a4UrRxG9N44Dm+inbYb28vDMYyNeZRwrLtpgHgYoLxtOxxe7oRRfTbUlrnvjeQd1vHZfD0M+5XwK96jYNhmcsOtsNws2heLnCYLXPH5nae0BWxnurycNwPPWVL/ABTG+ld+cayRZVCXUa9Xd4a2XM0GZhZk8+C5BxjLWU+teD3GOZRpMwnPdozVf4TAaK7m/FpH7y1je66qTJ5nRjq1iuQsZ/olmc1P2U6oaT6VWdVB4MSJ7Llzccx9Oszr7Eo3FC6oUq9B4fQrN1MqN3BC3uO1xi9uJVc1E7QoB7Sgh2CBZKAJklWBZmVR/9T1rX8QTC/MPvMioSxs7zKCn1NTd9wigFNkgid0VmEFrQG/VBZADASYlAknQZmQeIRKymjWGkOiOxRDKgb3IO26DEDGufI4Rpk0paDtzwgOCyC07lGQVHvJE9uSgcHFwbo390DPM0y7lUR9QQde87NUZrR884W67wyrpaDVpxUaW8y3cLtwZdlY5MPDpHSzMn7dytbPrO13Ft+BWd31MC/QY5d2O3xc8dVv7GkgOmQqwN3YBBcGZQGN0EmDJ4QWNxsgtkkFLfDUaznbCmYngV3avbLntcR8wFx15Vr3SXFDdZbbZVSfGsKj6DgefKdl234SujDcfQH9UZC5m0hALSfQoMpu4hBI3hAQEmO6Amjt3QQoKIlBWqNuxQajn3DmX2XsQpAT+E5zR7tGr/JShnT6/OJZVw2q8zVbSa14/vM23SDbmnzAroJ3WBbSIhBZ3CCggsFBD6+iAXevqgRc6hScUGpdOSHYbiDgIJv7gj/aCDdgCEFygEbmEFiUBAwN0C3GTsgsHaEEIIBKCoJb7oBGxQW/sUpVes8DuunHjti+Hy59q3qXVoMo9PMIrkVHtFbFnMPLT8NMx6r0zWLnfLWMsW9t0O6f0s13Vr4/UjM7NOCWZaXvtrZ4+KIkLw/iz7L+FrR9ll7JlTo5Vz/iV+09Srm4Nyy+rVv6zSvRWhlMNO4AHMrtyYzLHtvqeh0DEbPHcrZoyJmLF7utm7Er6lVpDC4aTTdWptca9AExtESVw4N3CW+2a8DGcfy409Q8Sx3ErzKmM3E29DAaVbwalRzKY8OoWtBBLyfNHZbyRrw6i5uzblPL+X8l5Ee6lhda3rXdZ1LyVXW5DvKXAfGRJTBK9u+v+olxnvDM75h6aVTh9lZvtK1nT01Q7XuHFoO/6LVWNP6eZmtsC6h1qN0z7hgmdrm5scUy1WYaRtQ8nQ8teADM8rDWXpozfu3SHrPUN3SNWwwW+e5rBuXW72y0j6PCvJ6dMPT6K/0rMhPeGfsy9a+o5rKbH02t8xHAk891nhnimT3MT684Tg1h+1MVy1iNrhuxbduYw0nB3BDmkha0wxsP+0XlXFrGpiFhg95Xs6RipVaKQA/VwV0PLuftUdO6Bq0K1ncyGwaZYwjkyDBXXgx1tjkvp87dOMIs8/8AVoOdRLcMqXNW/qUTyKFNxqD/AN1z5ZLXfDxi6BifVssz/WzDgls/G7yk9+GUMENLxKFO0pmA9sTDiQnd2+HHe14HmLrDY2GP0rfJlStRxypUrBz6e1HxREBpG+yzeRqF431Zv25Zw/JuZ8pPwqi19KliF86iWgNa4anMIGxhMc9rXQbK5ytSzFh+JdN6QzLdVbZ7L+wq1PE0UIEEF3Dh3CWponCbLNNPBszY7lu+p4M59So+6y3cNaW0WtHmO+4JC1xec4zfTT8TyllrLeSrTqpknGPu2YqRFe8aKoPi1XO87NAO2648vjPwzHj55wvDOq2UKfUbLtJtHMmHMDMcsqWxcAJ1wO69Wt4+XSN8+zh1OdjFkco4xV/rVo3+oued3D037r593HbHLb6DDXkiRE8+y64XZkoDfldazFuCigduJQL9UAEwVYJHdUf/1fUo1ix0adl+YfeZYIqsOrbfugosAYR+iKUwnW0HsisyXEEDnsEC6hLmaHHfsEC2FuptMj3lErPOhgEIiwGgk8yNvRBiuAL+dPyRplN1Fsg7eqAi6WyUZAJeZO7UGSzQ3SOED4Y4+oVGPV0gkRxws33GMvcYt/aMuaZYOXNIB7T7q8l/J3utNV6a37stZzvMCuDps8Q/Et2cN8Ru5X3+lzl49b8vh8+Nl2+haLpaS0bE7L06rymgTypPIuQgppKAncIIw9igMODNvVZrUYt8NdJ5IkARp9ZQcmyi85f6g4pgr9ra8/rFBvYzzCYpXX6R2ngcQtIaAT/xTYoMMfVAwbcIKeTOyA5IIPsgjHcoClBY7oBjdB5uLUBcW1alEh7S39RClGkdKLg0bXEcHcfxLK6ew+zSZb+qQdIDjIC2DBMrIs7IClBEEQU47eyCgZAQJvDFF/yQad01dqwy9j/r1z/vBBvfZBXzQUTDtkFAmUB9kCTIKCwdt0EDiTHZAQO8dkA90BEkQW89lL6WPIzJjdrlnAr/AB2+Om3w+k+qQdgS0Ej9TsvV083HHkvl8OdNcJq9VuqN3j+YnF+EWtSrjGKVKnAp0zLKZJ4bwvNzZ3LLWPm/ozL+bpGQuomC5g6vYvnDOFMUMuUbd1nlt9yybSmKT9IawkadTgNl15pjr7uXzPJ596ehZO6W3Fj1Avsaw/wsVxK6rVsNwy4pOZWq0tEU6lGn2JO4hJrlw7b4mP1Td3r6taxm1blXIWS865bxq5xLqK2pToWltVqmrVFN40uo+C4mNOzTAUyyk/Qnn0ycWw7LOSajuoHWeozHeoOJaKltgFONFHaWB49u6dmU82aiXKS6rQcd+0b1FxqqbPAXU8Isi406GH2NEAtbwNwFvLX0auNn0edR6ndccE/r9xeYpToN5qXNIiiAeCSQuNsSN6wXrjkXOIsndWcGa7GcPqNqWuM2LQ1z30jI8TvurjGr6cq6g52tc19SbzOVtb/1F9dlShQfE+Hb6WgOB9QFebXb4dcPT6xy7jHT7PuDUsVbg+Hus2UqYxGi6iwVWOfDS5pAB8pH6Lzcds2l9mXnSPCbvG7fLt7ilzWySymb+0wMuBpF+ofm+IiDMSr3NdraKvTfpnZNFu7CqNrRdDtFMlrSDsNQ9CteU7XK82dOOmd5m+zwU4fRpC+uTRmi4g7UtXr6wvTx2yPLn5rhvTzMmHdK+qF6zGab/wBmU6lzYXDhvUFJ5LRB+S45TLu3p6ZlO3TcsW6x5GyLb1LHpRhDXX1y5xq4tdNDny4yYn0ldu3G+7HHHGyeWo1utPWa5H3tl5dttuWPp0jp/gFm4YfnHSS17GDfaRzGS2wzxh9tjuEHy1mVqTRVg7SNplYuEx9NNzwfDML8/UjoVcNGI0ml+JZbrHd35nBo52hYpoxjsJzlkjFM8YhiNSxzTUuC7ErNlTROgw6iaXKuFuN2zZ4Oxmw6S4Xe5Vxuzqtp2Neq2liWHFxfTDtMnxKfbddce3LLzXLHz5eXmbMuVMh9VLW6ytUo1suYvRDMWs7b/m7mv8vymCunNdenT6b+jR85YXX6WdRrXHcEd/qa+ey8sXM48N5ktkeixyccyny+WeO683w+y8s4zQzDgdpi9s/Uy6Y1zjMgGBP8V4+L5t9vnT1cssm3pzv6Bd97c56RzkUE7b8IKMIFFWAZJMKj/9b1qTWkS7svzL7x2r04QU95LYHKisdr9FT8SUVn27vPqAnZBKo1EuiIQY4cdcgSUSs1lTUAyoIlEG6D8J43QI1N1+ZGmQ13k8vwoDDWuHKMqALPI3ugYwGRq5QZDXaNo53VCK1TURtClWG026qZETIVSuc52p1cLvLPMNoIqWdVr6h/uA+b+C9XTXXJK8fUzeFjv+WsVo4thNpf0XamV6bXj/1Bfore7F8jt09rUuPH42ovK5qCtvyoLmRvyOUANMklA2Aee26AagbUZ8/8lmjjmfGPwLNeC5iZtT8UUax/+287q4XyOtWlZj6YcDqY8SD7nf8AktUZeoxtwsa8gg5aFtQRBbXdjwgJ4iC36oJMkICLo+qCkCLhuum5o7oOZ5WqDC+ouMYeRFK8pC5pj1cw6T/NB1TymHeoQW0e6A/ibqQUgiC5gIBnUCEEZ6eiBF8YoPPsf5INM6XvnDL4f9uuf94IN+bwgpzkAoCQRALkC0FjgoDYgrugtVY4D9rDNj8IyNa4Db1NN1jNwA4etGn5nfzC9nTXt28uV+ZyHBaNfJPQKteWjCcw58vDZWwZ/bfdKXAb383mC8fSfL1FyXlnhvVrmHCKXSrCen78s3VXNmG/dq99gwtwXhlCoKhr6/7w2We3VuX+Lw69252/4fL1sTzpb4jjWEdXrXLVS6yNglpVsrq7exrbptQuhx0HllIiJW5P5P6/6Odv8/8AT/Vp2D4pgOFMzF9oLF7I07a7ruo5Pwmt8LqxEeK2n23Elb6fD7/lsy9RjP8Ah4+PbkuXsvZm615sv8Xxa8NGxa43OMYrWP4NtR50sJ2BhOr6i55fdY+o68XBOSd2Xttlz1OyjkNwy50mwKjfXLT4L8bvWCtVrPH52NM9+FeTCYSSfkTlue5f5XQ6OM9TbC1w/E+olJl/kDHbR37Y1UmUnWXieRux3BHIXm7fLLkmOfZ6zfTvzdZWbTxrLly7xLLEaFZoaaDzLG1W/lc3uu+/DX0IqdI6mXc0ZSwDEL+le45id237/Y27hUFFmsO3I9hC82UdMbqO05ty9idlnXMNHJFrb0rS1tLUXuHt2LmFrvENMD8xAXXH8mMvzbDhPVPDMXOG3OsNrYdULy2p5alFu1E0nj0jddZwyOf3trIzh1DwoYVc4pXjwX0q7Lai13ncaQBbH1XSahu1xujY5lqYthGc8Xrvt6LLptWjaO+NrHAAE/MOC83JnZVweNnHpViub8946zBLmh+0nMbfNtKzxT8Zp2hpPcwu/wB5e1yk1k8nKvQfN78Up18z237HwO2cK17d3DwIZTM6Gz6ryceHddvbyZeHRMG6q4rmLqBRyXk+jasyzQIoUXVLdrwQwaS9wjeT7pycXljiy8PV6jZXyRc3rMCz/Rs8Lxi62sscw8imA7salIcBeq8fyxmXdril3aZx6H5woXTLgvtnOFS2vKP/ADe7oH5bSQuetNOrYp/R66usD604LRP7GfUZTzPhlHenTqTDqhZws5Xumi5dvlslLHsl3Wab/M97hBGTcVs22lHFKtMeCKw5OnsXcSrlx/NLPTGuz5P8X/LVcOrdLbvIGZ7TQz9rtqV3WjXsP3g7/hmn3hdP5v0Na/hvFe49ReitRtUF+N5WfDJEVRRb6rp0/wAmdw/Nz553YSOh/Zgza7EcBusvXNSa9q4OpNd2pnlfO4b9xnlj/i3/ALvocuffhjPy07+SSZPPsu8mppxvsJ5VQLuEAHhAMwSrADgOSYVH/9f2Q3YDsvzL7xjQAdxsgqo6m1w7SopLoeIG5lFZFOq5rQ2PqgJzzG288oAp/FJ2J4j1RKzXNljQ4Q4d0QHiNGwAlAkQ96NMhpADWD9EBOY5jiBwUZJa6p4gA3g8oPQZ5hqPKCtZaSCFQLnNJErNWG0TLSAd+yqV5GZsMp3mHVqLwCHMIIPclb7u3y5ZY93gXRTG6v3O7y7cOi4wypDA47mk709gv0nT3uwfK5sdOyU3OkGJBU9V54ZAVE0ajIMBBYZpJk8oFVGuY6W8eiBzDqbJ54QXoEEfos0aD1OwYYjl27AH41AeNRPeWb7LMuhndP8AE3YplnD7moZqFsVe51NEbrpBuOwbHqtaBgCFkRvKCO5QENKAuyBYQED67oCBCBdQaiADHdByzMbjg3UHAcRdApXE29Rw2nVOxQdUpumI4j+ZQNQXqAEdvRBY4QUguJCACDOyBjRt7oMW+3tqny/yQaX0w/8A2u9I/wCvXP8AvBBv/ZAL2627GCgjATygtBGzvPCAXGUANQEeyAggCd0FlB8Wfa0xOrifUPDcBpEuFja02sYNwKtydPHrwu/LezCPHb8zYc62eM0M99OcoZWw39puyth1viFWwb5GuLw11SdXBBC4X5cO96OSeGz0eqGI2WPY71Ouco3dPLFxbMw1ldoYbinc0jph7f3S7utb3jJ+V25zxbf8U013Hb7qLl/KNhkTEMLtRZ59uq1G0qU3k1LWnd1PEfRc310ldJ+Pv/TTO/k7P12539oHGBUzHg/TbL4/1ZlqjSsaFNnD7t7QNwOTvut5X7ni3PdOP5uW5ZfhTqhiVHpzlHDOkWAvFK+fSZeZquGeV1SvXGsUZHIHELn0HFq3k5Pq6c3Jd6w9OJ2l3XsLmjfWrzSu6Lg6jVp/Gx49B6Bcs8rlnb9Po32yR3jqP9oS1zxkl2TKtpUdUdQti/EZ0l11QA1lwHaF1mLm41h+a8yYfRfY4dil1b2lU6qtGnUc1rg7aAAVjPxHTF9O9MOjtPBsy5Zz1dYhWuXXts68q2twD41KoxgLiZ/L5liTaW6dEybimB4n1OzE9txpxGm8UqVJ2xqtDZ29QJ4WbdVqTbx+pP2ebXMuI1sfyliLsFxK7cBeU4i3qepAHBW7lW5hIvIfRPLFK2Y/MNxcY3iNi5wNSo8ii2oDv5OFwtydJ2xrHXfMuF4JeVrA1WU7hrabaFuw+YAuYRHpAC9ucnbP2ePj85Vzrr0y6squXs04dVqW7qtMUnVqJ0nZrXt3H+Irph23FMprJyPEs9Zwxi2+54pjV1XtCI8KpVc4Ee6mGpXbObxefhGO4tgNapcYPc1LS5e3SK1KS/f5Ljy5+U4sXoZrzbiOcLyhiGLO13dvbsoPqBxOvR3PvK63OdsZk1a6jkPEqXU3KN304zBU8TGLGk64y/dEy6WCRT1Hf6LlbtsjohjZsMcxHp3mARhuNNfa1aTvhZdCWggHvKvHrHKW+mc5uab63Fsfusq4j0vssC++XGXLhv3ioeDb0qmv9SFngy1xZY5fiu9Gd78pl+SVM9ZBbnbAscZgr6eGstjaXj/B/CZcARBMRMq4/wBx238W4W7z72PlLFLE9VcewujYPssDzLRcylb1W6G1A5kamjjlTly1ZyJhO7KxrPR64qZQ6s18EcS2k6vVtiCY8oJDZWOu4/w5z9Dpc+/Kz8n2UXAbJLubdr7CXKoDV7oBcfRBQ5VgEqj/0PX8Rw07CF+ZfeMbUmZ2QBWDXlunkchRQUnBtSCJP8EVlOe0CS39EFHcS3hAuhu8l3bhErPY4PBDjueEQnSGvO+3dADWkVCCdzuEaZBAbpfBQNE1A7feNkTRFMaXbndDTKo1Yq6Xjy9iEQdWq2SI+SDH06jO4QZFAtpSdy7sCqLuT41MgjzEcHhTKbmk05zRvn5S6gWd6ZbY3cW9d3Yl55K+t0fUZT5fo+fz4yvo6zuG1KbXNMtIGk+siV9Pkxnt823trJ1bwsqZTdsQ3+KA51DfkII0wRIkn1QSNyRsgsSSASpoeZi1BtzRq0XiQ9pb9CN1mwc06X16uG4hjGXKr/PaVzWptd/8uodlqDrYf4jAY27Eeq6fQECYhYB6i3c7lBNYeZCAkBtIIQA4aeEFmAEFjTsQgp7WnffbdWDmfVe3LMPs8Ta3/mNyyqXegDhKzaOh4VctubKjcDcVqbHSONLhI/itTzBmFxCgtu4lAQO0IBcYhAbXHSgsBBUwSFYMW+dFtV/wlKNL6XH/AFVex/165/3gs7HQOyoGN+UBgBvCAOTughMNJCAeyCAQgh3+iC5QUIO6CE7gIPhbqYXY39o6pbuMhuJWdL20s8Pb+C69V6xx/Z5uLCZ7t+jptvcZ4d14zzjOTqFvcHB7M2dZl27SHMDA4BgO+/qs5492uL6f7td9y47fyeKOoOeLvprY5KxLL1a3xDNV590wzGCP6q41628f3m9pWcJ5yv6LfOOM/VsdKtm+66z4fgmda9tXtMmYZVxG3r29M0mPqGj5C9pJ8wIiVvju9YfSzbnl7uX1l04f0xpNzj1kuMdxIeNaWda7xm51cfgy4Hftwpy258nbfo1njPu5i8jC8u4z1o6j3TqQqVaGIXlV9e+AltuzzFhcfTYAJz8mWUmLXT4TGPCz/wBPMwdN8Sbh+PeG2rVa+rTbSeCXUg6A4jtqW+PHux8/Q7vNev0y6RY71Me44VWpMsbWppvWucG1GMI1TB3MnYLz3myl1HTsmmqZjwLE8oY/c4TiNCrbV7SrrtxWZpLqbXeVzf0XXLHeO6zLqvqT7PObcx5kwrFcRzJeOvXWrPueHVHAAsa9suGw4gNXDDK703njNbbRaZctcYw6ve4NVZh2a231avYYq0R+I3QwMcf3d10zmmOPJ7uXeq9xaM/ZOd7F9pjlBsOr2zTUpXLmmNbQdgSumOqtafj3VTErFlfDMiYZUrYlfVqhr4hcgsp0Q4zqjeYBW7I5Vqdx0qpY1Rq4lmC8fimZbpge6u4y1taKhIZ/d8oXmx3yWy/Ru4zj8z6svEMAss25BwFmKNBZbCj4sHceGX03fxhMvk8RqYzLzXH+rnSWnkC2w7F8Nruq4ViDn0HU3nUWXLRq/QhdeLG3yxlyammrdOcNt8Rzdh9K4vKNlbU6zX1X3G7H02ndhHuF4uouUye3gxlx26/9oPp9lrLWEWtzlChbfjPNzfljwakPAc3SOzSvp4cMvHuvB328mvo4XlTG6+Xsw2GNWjzTFtXZUd/h7gj0XD1jbHo180jonVa3bgPULD80YX+DZYuy3xGgW8B7o1kfVXDCc3Blcvc/I5Pk5ZjPVdcxHEcx2eesFxjKQpvfnHDWNrtq7UnVmDSXH3Xl7ryTj5b77tePWox29mNxn1efiWY7HLWSsVydj2H6sy2F02u8UqOqjUeXagA6JC9+peXv+urP0Yn4e36POznnSpi2asj4s3C62GG3DA+tUaAx7XnZrIjZcbxzLguF9YmOXZyTX8zXs6NOC9bGXtNppi5q0LkAbD8SJXTky7ul8/S6Z6fH7vqLjPrH2Lb1G16FKqOHsa4fULx8V+SPXnNZWGCIXdgDoCuhXZNC5hABdG5Qf//R9gQafuF+ZfeH4bXNDnHzAbIMattU1A791FMoRMxJRWUa7SPDc3lBCWtkjcHsEAUoJPuiU8tGxBhER9PiTygQ3ck76m8FGmfSqa6QkQR6oBBbTJJkAoElwc/ynkoMgM/EBJgIlXUlsmZ3gIgHOe14BE/JBkMEOD+3cKh5e1zXFgBIjYpEaPn/AAf73YeKyGvpzUae4c3cLfBnrNw5MNx0zpZj39IMr2NzUdNzQZ4VYdxUbtv9F+kl7o+PyzVb1DQZhRlXwGB3QGNoJPzQG/4gRwgBwkoCYNIO6BVw0PpujmIH12Uo5Bfg5d6kWt9Gm3xNhoVj2Lm/CkHYKVQGnAPlmQAugMmTCwDmBA/iggYBuEF6hBHdBKLjuD+Xuga7cIFkgwCgMDYAFBckOj12WoNXzzYNxHL2I0NMl1NxYPcBc8vYx+m2JHEMq2D3GXsYKL55mkdK3j6G4kSoCaNoQWPRBT/5II0oGSOUAOMlagxL/a2qk/un+SlGl9Kt8Jvv/HXH8XBYnsdCnZaFAygtBRI7IKI8seqCRsgpBJQUT2QUJCCuSJ4JA/ig+GccOr7Tb2P3H7apNPuNLY/kuvVe8f6OPT+sv6t6tcKz5ivVnqo/JmJUcPqAOp3La7fENeWABrf3Y7lTL+9jE/uqfY5lzDmzLORcg43hFxgVOteUmMzJt4ZqWry4Poz8LnEQs4fz/s39Mf3VbYff4Bm/rBcYniNTFbmywak2ld1z+IGVBsz/ANMwVri/Hj/lc8vWX+Zybos4WWXuomOgk17PBnUqbxyPHfpIWb/e11z/AAtW6TdQ7rpxmejjAfVdhkH73ZUztXhpDA4H0JXXPHy1xeldVOorupeNUMeu8PZaXrKfgVxTcXMeGklhE8EBazvbHLXl6/SHq87pbcXJo4ey9/aNVn3y4qEh4pM5a0cSOy88x8t7apn3N15nPM+IY5dV6lxb1KtQ2oqnzU6DnEtYfkCvRnZ2aTT6W6FUKGDdPrN1WBcYiatYj1L3sYz+DSvBh+J6M/wtm6aYvTv8MqE1G6aGJ3Vu5o3AcKo/yC78rhxOq4lhFniOAuoutqd7XDS2m94mN52XLDJ0sc0xGwFsalGhbi2uGM0O0jT53NA/yXTLJzsZthRdcOe1pJfTZqBiCXvYA0/qXLrwQ5r4jScnG3ucMZlvxtd1aYlc0bql6W9OoKxd8pC48s3k1x35Wpfabx61p4LgmV2f86q13X9UfmYxzSGfUhe/hymHH5eLPdz0+aRTuabKd21rmMkihXjZzztAPqF4s8pndvoYZds09HGcfxXMd7TqX1d76vh07ceYtb+H5QI4XTk5fk054Y7zlLv8CxbDK1SniFlVovpgGoXNMBrhLTI2g9lxx88drWd1yR1DqA92J9LMl427+3tvEsXvPoN2hXpb/ByhzX+Pj/V0XDDieM5G6e4jhNUUcVsr02tGo7doa7kOXDCa4p+ltXkbfiONYfky8x+yzxWpXWKYtSFzQuzSDg8gFop6fy7r27+bH9ZtxjmOf843mNZYyjQvMGq2Ro12ChdRDXtYdtJG/wCqY/3ef7Jf7zD92D1h8ufsEutXnrWlq+fcAys5f+V/quH/AJv+j62wOoamDWDzy63pk/7IXk4vwx6+X8VZsmV6I5qeZ4WgGrsoJKBbnA7IP//S9Wk4nyr8y+8KoSAQOR2QYxJMSop9NrgA5qKyDBbL+UAAhocR3QXRcdUEQERkuYYkc+iIRVrFwjgt7IpludxI55RWS5keWYQLqa4GrcHhADAyeIcgymS+B3CIFzCHHuJlEHrBZqG5HMIDoua4b8KhzWhgc7gniVcfaVhYlb/e6D6bwNOkgTwV5pbM2tbjWelGKnL+b8Qy1XcRb4g417YcCRyAv1XBnLj7fE6jG7fQDKgO577BdHn2txnfugmp2lA4GQCUAnlAQ7AII5oIg94lSjmXVmwe7C6eM0W/jYbVbW250tKQbrlzEG4nhNrdNg+LSpvLhvuRut/QeuQJkeiyCaD+ZBeraEAoCBgGOUDo8ohAl5MbchBbXugSEDADIdCsGHiVHxrWrT7uDh+oWMhz7pXWNqcYwR2xsrt7mT+7U3W8fQ6c0nuoLaSggdugt24lALSeEBh3ZBZHdagwMQd/Vqo7aXfyKlGmdKXThF9H/Xq/81iex0IExutCHbhBJKoiCpMx2QF2QCFBDyEFd0EQCPy/P/NUfC3UmcE+0fWunAtaMVtKvpLXeHJH6rp1Hm46ceDxMt/q6N+wM+XvXLPltkvFKeFOurUVq9Sq3W2rRqtEfI+6mX97KxJ/DsVa49juccHyXkjHcKuMLtLLFfuV1mNhil41nOnwvQvKzjNd/wCzf0x/djPy+/LNTrbgzr2piH+rKVZtzWdrr6akO3PtwVri/Hj/AJXPL1l/mcs6Vu8Tp51PpNEOfhlF23/e8K+PvLXXP8LjhLSAT8JEqctv0a4/T18uZZxjNd6/DcDomve06Tq4pA+ZzGcho7lcs896awx97Kx3AcYy1dsscetH2d09greA86XhjtgXDsV1viMeN+2+4D0pq4zkG+xt7KrcwVC2thNu3cV7Rk+K9o76QvPjlbk6ZSad+wW2bhuA4LhobDrGzs3uj94jxDP+2FMJe5c7O17GAWGF4S6jdYONVjiR+/uYNx95Y97av8125PLjx+3ZsPrPrWdKoGeEYB0Bc8I71pGc6PhYgyoI1XLmuMdtO3811yjnXjW1zVsqFWswxUY9rpI/JqOkfR2pXhu9uXL5nhxjpbjdtTzDnPF6zw+tUvHNoCeSHlzwPZwbC3qd3kwvhxzOmP3eOdQbvE8yWz61Nt0GVbNx0FlAGGNaD7Fa6mamo1xYS5brv+fMh9NLLpHaYi2lWZTsh97pUaTgarH12wNYG8Bc+m4vzXqNT0+XsAr4RbYvbXGM031MIFQPrMaZeWA7R7rjz4a3p14bPG30n1kzTkTGun9lYYVefcsTuLalcUxoBqVGUthTqHkGCvTxSTi8uXLN8ksc5xWKnQTDnu3fTxRxDT8LRoPB91x4J/Dyn5HJd8+P9W45UpYnddIsHp4dX8C/bi9P7vWH5S8iCuc12Zfs3yfT9Wz1b1uWsx4vQ6q3FLEL28sQ+yujT1CGyAwehXb+bi/yOMadn/NmKYxgeVsJr4M+ws6ddr7W5I8tSmPLE+/K1LJx57/JLN8uGM97ed1ie1+esDtQN6Vjaj13A4Vyxv8AZda+u/6fmuHnqt/TWv6vrnAhGC2G0f1ent/6QvFw+cJY9fL+Os7aPdeiOYQdjK0FOPooK1IFPdBQf//T9I7BrmlfmX3k8Ul3CBXiNDzq78IrJYxwAcDt6IrIedbRtEKDHLo8vogbSaSZBgwgcw1HAhzvkgBzQwy4TPdAbJ/KqyyXOlv95AqpUIaNX5FGka1pcDPKB7g5m7HBUSXncOHG4QDTLhs4QD6IlZDQwfD80Qb2Pe0EK4zdZy9BcDHhnkeqxlj5bx9ObZyoVsGxezzBbSyrZ1QXOb+6Tv8AwXu4OTVjw82O30Jl7E6OK4bbX1E66dxTY4H0cV9+4Pk609jSuV8KnCA27tj0QUDvCAxAEkwguQQSDPqtQeJmjD6eJYPdWThPjUi0j6LlRpvSXEKj8ErYXXP9Zw2s6i4f3Qdl1nodJaZbssi5dqagI8oLHdAImdkDGkgwUEdEbIIDACA21J2QBXEsJ52lByzBi7Cep19aTpoYjQFZo/vjlB1XVs1AzsgoN3CAyIEIFk6TKAm+qCy+QVqDAxHa1rH+67+RUo0rpK7/AFRff+Or/wA1j6jopctCtSC9S0IgiCwgHsfmpROyghMIK1SgobSStY+0r4r+1fhtTCOpWG5gpt0svbanV1f/AHLd0/8ABdaw27H6easT6kZPxvJWIjDLrOGC0qNzdndhFu0F23quVR5uNZjxrDMtjpHmO0uXV8OxuiL/ADPbk+C22fVFVtVrgPK8+q3l/N+sYbHTyZaZWzvnXALG+fiNHMeW3XjX16ni1DUpTqBPcbKYe8f0x0OJdDB96OdsvuMvxHA67ms9atuS8/pKn1b+jjbtTYBHGxXSmPt1roDn7A8j5xp3GMWVN/30toMxGo4t+6tJ3dHcHuvLcd5R6L+GsLrdnfAc+5sqY3gNrVt6jGm3rue/Uyr4RhlVoPEjsvZyz5Xg458zqGQ8Qw6jl/LDGHEq7m2z6TqFNsMZTrPLaul3cP2B9F5sY9GbZ8IdfWVG+tMVeDcWlS500y4O8O3bo8NriO7WiFpmNju7qxwSphtGzIfbaTWowdtFcNd/vKX03HZcLuqd3YUarCJc1moN7bLlxuteBjzG3eKU6LgCyizzu/vO/wDbdemuVa2+0D7G5qNGlviEEckkg8f7RKcHy2pcXy7g1G5y91OucGsLU3rBdi7Zb1HlmvT5uI4IlZ5Mvm2xMXh9acKxahmupme/tGYdQxeq59OjSdq01GAfwW7e75nS3Xhoz8y49Vo17ete1qlG6aKdam5xcwtbwI+Sn3h2beWeYcyBMhgOw7Ays53eGX7LMdGPr1q5pfeXueaILWajIDT2S3eE/SOnG6/mIC06GYBbVBDrrEKlWPUNaR/mp08+Tkv56ef/APNG45cw3E7zpNgGHYXU8DEL3FWm2f6Fp2K4Y+eO3828/WP6Zbe9RxSjhWM4/lzqRWbiuZKtBlLDbrQC1/iCWsHoQV6vWXH+mDE/m/XJrOP4nmbFcfypkbH8MbZCxdTfTeB8TZj/ACXHqJrpbl/ijpxf+bxy/KvFz0/9t9ZBZUBqbb1KFo0fKAf5r3dbn91wYz/FjJ/rHHppvky/zW/7vsi1YKNrRogR4dNjI/wtAXzenw7OPHH8o9nLd52oXbru5hLkAFyBZcgU92yD/9T0Kfh6A2dwvzL7ywROyBNVrdYd39EVmUqoLAAijc90KBL9htuSgZRJeQJiBuga4QNQ2IQC94cBvugbR4VZZH9k+DuPVAiq9oJ7g9lGkY4Pe3Tz6IHuOkgO5HPyVGNWLhU8VhhvEIMhlUOa0k/oiVm0QxzT6kIhrXbBoPCsuqlRzQ8z+pS3Y13NOHC/tK1AtBkHkT2TG6sZyx2zuiWOOqYbcYBdOIucPqENDjuW9l+mw5e58blmnYNW0hXkcMVtOoSsT01V03EODT3VRenSTv3QWI4O6C2s0mfXstQKrsDxpIkHY/JcshyrAIwHqPf4dOm3xWl47G8DxKZ3A9yus9DqtMkwZ0iFkHKAkAP8QRoPJ3QOaCPi57wggmdkEBdJBGyBbiC7yk/JA5kQghMtO6DlWdHOwnOuXsYBApuqut6hHpVECUHU6ZLwCN2mCD7IDY6OUBtO6AnEwgAiWwgjTtCAjGy1BgYqYs6x/uO/kpRpHSR84Vfz/wBer/zWJ7HRZBWhEFrQiAhwgGSgF08qUW1226gskIBkdkFGY2WsfaV86/a8y8MQydhmYWM1VMMujSrQNxSriP02XWsOfZfxDFswdHcu4vgV192zLkzFWWzaxM6LW7d4bS4/uyd1yqN2uq+bcn3GacjY3ZVM3Ynmqz/adreWrAPhZpLXA7NDCdit5fT9awxMBscq4Bmfp7jWGYrVuMTxy1rYJjNtc1TVr0jUo7Ncx5OnzTssT+a/ldDkeQmtyb17fg92Q20q311hVQDYCnc6msn2Ihav4m/o5rnHBquXs04xgtVpbUs7utTIPprJb/Ahbpi8GJ5+q54+3ovpZceBs6QdcTEeo91vnz8OPFj5d76a3F3mLJ2H4HaYq9+IWl3WpfsafCcW1hrGiqIgS3hccLuJye3QrLALzEbzHLuqxllSuMHbVFoyprex7AW1HvM7uJG57rTMaLmOxzn05ucIo4+52IZeu30TY4w2YbRcA/Q/0iYCl9OkfTuC4taMwWwt8PrNddYrSFU1J2awCdvRcuN0q8XvKbLW6pW7g99wBpq/nGo6Nj8gvTXKtffeOouo0Z/D8QPuPWC0RCzne0tcWz9a4jZ9bcGzHhdsH218adrb0GEAkMYQ8kHj4gul4+7HbOOTM6rUMCt8KIz9UYy5osqOsLOhvUNxoc1oL/qDCvFj/C/qzlfmaj0C6Y5bzrXuL2+vC67t2up3GHPaNOlwIa4H2XjnnJ6O7Ucz6iZTsMl5luMBw7EBiP3efEc0QKYdvpdPcTyvbnx9uM/Vzxz7ttdZht2adCu6i77pXf4VOqfgLm7xPqvHx3fHl+m3Xjda6tNdhuV8kZbb5KlO1NzWp+9U7L09NP8Awty/N5r/AH0dKtsv4jd4BkTAMFvhh+JMZUxIVHcgAbGO68vD54cf1ysduTxcp+U2xbjGbW5wLEMKuWU8V6n08QNNr2tBc+pTMNc08hkDhenk/FP0mnGfT9ZszBsZxjN3UZmI5lw39n3GXbNz7qmedTGyHfJW4d+HHxfrXTj9Z5/lGk9MKT829XX4pU87HXTq5I32DjC8/wAXz7vu8Z/LlIvw+byy/a19lFw3j6SutWXfkokfVRSnvjgoB1AhAtzkCnu2KD//1ci3LnEuI3PK/Mvv6Oa0gE90NKHmcJ5PKKewFjtJG3KA3VpEDlQJdUDXAd/dBkUmkCR3QNc4yA4fVBjOjUCPVBlsaWs1BVk9zwWgP5PKBFQAGRuPdRpVIfitcNgUGa6dOo7kqhDtLhojdBVEMAMjjlErKMU2seJ0u/miG05gvHf1U1sF4hBLfy9/Vak0aJu262E9iIKqtBw+/OVOoNveVJZY4jpo1fQP/Kvp9Jlv2+b1PHI+jbaqKlLUCILdQX1eSeHzMfFPaQOOFyjoIjhwVTS3EESOUFDflEFMcLUVHtLmEtMOhZsNOUdR6FTCsWwfMNKGvtbkCq/t4dTZ0/qkv0NOmWFRle0p1NyCA5pPcHhasRlAAn2XO0FAWhGhsEe/KAm7bILOx2QDuTM/NBQgmRz3QEN+DA90EfDQWuPKDmvVi3d+xaeIME1LOvTrAj+6d0G+4Hd077DLO5pk6KtEOB9wFrQ9AAFNAxsmhA4nYpoTgpoDEFNCw4oMDFH/ANTrzxod/JSjS+kwH7IviO97X/msT2OiNaOV00CHMJoUTCCxuJQWDsgFxjhBTjsggAhNCEBNCojdNCiTG3Kej21jqFlyjm3JuN5fdBqXtu7wSeRUY3U0j3kBdMfLGc0+PPs/34o5ixvpzir/AAaOYLWtZtD9gy8oTp54cCNljKaZx8uh5Qus5ZXw+76oY/e1cZu8BrVcu4lgdXy1GWtOoA17HjvMfNLd6/S7YeJmu8ypYZdu83Yvhb8D6lMxuni1nb1g4VjQqVdQ0AbaAw7+6sniz87sah9oG2NhnrB8/YQNNtj9ra4jRezg1KMF2477CVrk8Tu+q7ry+u1tTxHF8Gz7YNH3DNlhTuXOG4bdUWinVbHrIBTjvdPLeLK6Y9BL7qRle5xxtxUw2tSrsFB1Zn4VahB1lvuCvPhllcnXbkeI2TLDGLnD6b/HpW9c0G1BI8UNdDvl6LXPjlUxsx9PrHCOl2UMAwS0ZQxN9lbZqbQ+7XjKgfXsMTa2WAOEQHSsYY2QsmVaX1Cv7/pDjtjlSyum4liN5YtoYxeOcS4ipUd5h9HSvTnJMdxww85adT6j4pb0um1LLePt8d5oUnWt2Rq1fgy3WPyv3WOKTOXa81uFmnN+mmcKjaWHYZiT3219YtdSdSrS13huPlLQeQuU8enbG2+3cbCtQxBtI03BwgNZvtLfJv8AUSt7tXLGH22GMvr6nSeNPiODif7vmj/8YV554mmccJfb5v8AtH3WJ5fzfg1K2rupXdlSNzbVGmCHVIHb2AXt47fu/LxXKzPUY2R8SHVjLGNZGzK41scpNfiWDYm8TVNZm7mF3pHZePDl89n0erk45PMc2wXNubOn7r7DcJqmwu6zx49QfGWsMggniVvLj7PMXHWU8vMxPMdfHswux7FaDa9Su5rrymPKKgZAd9SAryc1ykn5McPHJ3fs+qr7L+QcZyDgOG4XaUrbELyvRuKFkxwNRr3xqJ9oBXHgwxm8L/N/y6y2cdy+rjGfbj+l3VunhVjL7eyqUcPt2jcAUYbx8+V6s5OHj+6np5OnuXLvPL3HVc2WOG3GMYjXp4w/D7nJ2HU6NsKLgx5rObLtu49l4eGXDCYX+W7n7vVy3uts9Wary62L4dg9HAM2ZQw4YzeYcz75j900RUeHthzT7zuvTZ3Td/E81t7pr8Mmg4lmqt/QrM+fbqj92v8AMRFnYj8zaTRBXo6b+8nJ/hdc8u3jyxnvJm/Zby65rb7MNRkgjwqTj++V8Pqsrnz9v8u9vZ0mE4uO5fzWWPpJxif4r6WXt5sPwwh/Ky2Q/hBTTsgFzkCKj4CD/9bLouIHZfmX6Bb3weUASWvDhuEGXTeCZcd/RBKoaBLeVBjPI1iQZQZ1N/kA90Bve4eU8IFENdxz2QPt3OcNJMgdu6rJ5LS4au3IQKrnTvw1RpVN8jyfRBktqTDXd1Qt/kqGEC6T/wAQtIO/dEr0dLmhg2LY+iIsOLTvuPZILDRJc7g9u60BPPPlHARWk58w03WGOumN/rFB3isPcOHEL09Pnp5+fHbpvTfHxmHLFldOeTcMYGVgezm7Qf0X28cu6Pj5Y6reNyZ4HolQxhJ2PCCO244RKsHgIghsd1qLBn4YBUqtP6hYU3Fcu31vH4opmpTPuzcfyWPqUrpvi9XFsr2VWu78ek3wqo9CzaCt7ZbjTMCDypoEDJiUFGQ6EB9/ZAQIIjugWSQYQQHeBz3QNkNEAIBfDxuN/ZBrWdbFt9l++tw2TUpu0zwCAqPO6W37rzKlo1x1VLcGg4f3mFUb2zcwgudigpp3QEUFHmEAk6VR5+LEfcq8/uO/ks0aT0gcTg18exvq/wDNYnsdKGwldBGneUFO5hBbT5UFdpQUTIQUXCEBjZBCUAnhBW/ZUBEeUAbGWkrXpL5fDPXbL15026r08x4Sw0bXEKzcTsnDYCuwzVZ/Ip7c/TpeMYNWz3jmX8VwPGXYXlDP1vTq4oKcOpnErJurQ4HZusjzLDDHzDmbOGI3t3j+PZUtMWw2xFxlQAFr3vuy4sbVDncCeF0g0y9w68zl0Qv8Jvrd1LNnTu6e51u9pbVZavdJbB3IE7eyzld+GpGvZXaOoPR3GsomH4/lWp+18Hb/ANI62dtXY3vsN1mXt8L6eP0/625lyHgWKYPaV6lTxmsbhYedTLZ7HAkweZEhb47239zbnuM4vd43idzjF21jbu7qGrVbSaGU9bjJIA4Xbm5JoZdlmXMFOjTwuheV6tqLqnd06Ooud41M+WPkF58894NY+31Ni+HYH1NsquP4fYurYnQwdttc169JzSL3yhrZI5BG6493d4Xt7fL0Me/ZWe8iYZXo1zSuLaxdb1y3vUsyGVGkHkAg8/NdZO2VizvrZc3dJ8uZrytheImoMLx6wtKfgYpS8k6WjT4nEg8BYwxtdrdOcZBzbf4fjVxlLMFMUsToDXSqNM0304gub35Xa+HHLLbt1low94xLEH+Eyuw/dKZ+KSDH/wCICxw/Pk1le2PkDrZiWJZ2ztotqRuLrD7bRXazfSGFzyZ9A1ezqMpjjpwwx3dt36K5Vr4pkhmMYLd07DFLG6q/fq9Yf/DvYabtJHs6V8fgxt5NvVy5Tscg6s32EYjnW8/YjxXs7dlO3bXaAPFfSEOO3uvsc2U08nBvbRi0SOziQB6gLwT8Uv0eu2Tx+btPSQ1sOw/G8+4tVe5mFWzqdl4hMCqRpY1oO0pnd8ss9HJe3HS+h+HvxTNV/m7EW6mWDal5Xcf33S4QV06n+Jz4SfqY64uO17r8dyJe3VjmK9o3D8Zu8Sc/FqWl5aLZrvK54iIhd89Z3vn8v/Dy4fJLhf5v+Wbi17mHL+L3+D4VhdO3wPOjwbNoEOp0idiB2BC5ZZby+8+np1mGsfu/6tc6z3rG3WBdP8LMW2FU2mu2nvqru5ld5l2dPfzZk7+f9H0r0qwAZZyZYWRaGV6oFapHdzhIXyemx7pbfb1c+Ws5J6bk98klerH0ZzWVJcZWmCnHaCgAuACBT6gQYtR8nY7IP//XyeHgDhfmX39qc07koq6G5g7lEZLWimdR3Pogp7zGw2lRS3jUQQEGTSnYem6BtfZocOe6DGAJMg7hBnM0tLSzmNz2VZVVLnNNUBArUXjzbhRTKQDII4P8EVkuYCCZiOFRj1HEQe/CBtuG1ZnYjhErMoVNVJ7HczEogGPgEH8vdWA3uOzm7xvAVAioH8iCisO/tm3NCpSfBBG4TG6Zzm2v9J8UdgebL7LNydNrdk17Vp2837oX3ekzl8bfJ5sdO903AtBmQe69OU815T2wJ9FlUcZ4RKgO4RFvHBViwQBkJVIv7dlek5h3DhB+q50cw6dVDhOY8fy1UdDW1PvVu07eVxgwrGXVGkRM7LrAWiCsi2iSZQUSZQWCQZJQQEOO+yCg0NeTPKBpQU6dJjlB517Tc+1ex2+oEH6hBzzpTcGyvMbwIne2ujUZP7ryqOqh0GRx6oGDhBQ5QWTugqdp7oKdBb7+io8vGHOFnX/7t38lmjTOj5nAr0/9vr/zWZPI6TJhbBNIAQU4+ZBAQBugEnZBAUEd2AQGgFxgIK1diguQCrPaUEwZ/mmS4uSfaGyA3O+Qq9zZ09WNYITd2BG5cHCHt+olXG/m5Zz8nz70QxmlmbAcX6RYtcG0ubwOvcuXodpqUL6lyxvpJEqMvayzkvPmJdL82WFljunHMPv3XGJ4NXB8UV7U621Gk8TEytxHqZVt6eAPwPqbiOM/tfCs7AYNmuhWcA6nVqN0Ax30ERK54S/eX8nSWacu03vQ3rM6lXBfhra8SR5K+G3Ugj0I0u/gs8v4vDNa31fyczJmc7q1sRrwTEWjEMJqjdrra48wAPctJgrvZLpcf1aH5dMiduZ9ly5u2/V17WRh14/DsQtL9gl1tVZWjvDHSVJjO1nT9C7LO2VL7IdPGadelRp3dqKte2oNDagquZBAAG79tlz6efP5debUwcB6f41cYxXzJgtxbutabfvZtqTm6S2nXpy0kep07r3dR2+NOPT+q2bM2Z809PclW2F445zrdlF1Gi6r+KD4jfw3Mqdw0mS124Vwwc8sr9WmZepOs8bwrGrr+sY1jAqOe9x1AUj5mNZ8l5ea36N4Tft1zM+ZTgeB1Mcxaprrvd4eHYeN3Pruot2aPmSu3T49uPdfDn1F7tTDz+zQOmGTr51XEa2JW7auN4k3W88jwryRDj2IAIXi5eS5Zb+jtxTtx+bxWu9SM3YXkbJVPIOUxUscWvKtRuMky2ozS7YfIhfV4+GYYbvh5eTK3LX0ap0Kybl7PWO1sIx62eadJouKNyx0aC0+bV/iXyeXPLLLWPl9DixxmPmj62dP8Py7nLw8HvaNRl2WMo4bT/taI2jUPderOfd8OWWXi68PHhbnyyT82V1IqNyrkrBenlkf67dht9imn4tVT4WuWui479xlycnyz6ba6u93LMcfNbhgmE3WWen+HZfwwAZnzXVaQHfF4DY5HovP01twy5fy9OnL81nH9fyehVOM4pXzRlivhFvbYmy1oW9PEaAb4VJtIbudtyV3m8fknrJw3v58vFxeXlHF8Ur0bvPmcq4qty3Qda4WeKb3s8o0juVOPG5ZfdY+fr/o1ln24/e3x9P9Wp9K8EvuonUWpjV/L2mqbqu87t0t3hc+s5Jc+3Dzi9PSYXHj7svGT7KY1rabNEBrAAxo7AcLlxy45a+iY+d3L2jie5Xpykl8Jvfkl1WNlkIfUkoFuqQEGO+oe26DGfUdJQf/0M1z2taNvMvzL7cJqay6fyo6RGktfLeCiMrVuNSAnOYfKOSiwqC14n4UVlDcaxwgBxc/5DlAtrX6pHCIzKRdHCIYHkNc0jyoFktMQIaswGxupp0cDhbgZBLBv5hyEqkPd4hDYgjZRT6LS2D6KxGRSYWkk91UGA0tcPzSgFzv3OBygp4Y8Ah0FAut+G7bfUk9LK5zmxlTBMbssxWssda1WuqEfuuO69nRZayeHqMdvofBMSoYphlteUXfh1mtLPk4L7du6+XZp6wJG3YbKIIGTI4UoYQCJHZQWfMyO4QDTLhIdygJ4mmP1/RS+hyTM85f6hYRjTfJb3jnW1yfXVu3+KQdWt6rKjA4fCW6luB+pBNbQghOqCgkTugkQUAumdkBEOQFDoKBFYRTM8oOUYaf2L1Ur0T5bfE6BeP8VNZHXWfA1A3UtCakAl26AmmQUA90HmY2Ysa/+B38ig0vo0ZwC+/8fX/mg6Tq4CAkE/MgpyANSAmmd0EQQPG8oB3MzwEEbuJQEeyATyEAOY2ozSWh0hwqsd8Jag+GuuOQ8Q6WZ+o5ry+HUMJvq7cQsqzNhRug6XUfqd49Ftysbri+JtzZg+FdYst1q1BjTTseoWF2ro1UAdLqhYN9+Z9FHOx5v7B6Y3HUOva2NzVrdMrmydWNwA91pZYnUHkcI2EQCfdblY15YmecPo9UemrcZwuo26zTkd9SxxBzN33Vi0+SsPYAArFdsWvYUP8AlW6TVcFJ8TN2SWmvh2retc4fHnZ6nSd1cfEM53WNR6M5fwHM+drbB8zU2jCatN5rl7xSNIs9z3JXHs3XeXUen1/y1lnLmd69rlm6a+jWpsfXsKbY8EaQBv3kbr1ZYawefHL5ne/s0YlaZryBd4PiVvSq3eDvdQpPAB10HiWO+YOy8OF+Z6eWfK1+rbVMG6oXNJrdFDELKuYAga6TSD/kvRyOfDdNn6tOsryhgOHX0V7a6qUbeDvDqlLQf4ldpyXtceXzk57mbpP1H6dYrZ4vlonH8CsJNGkd69FkSdXqF4Jnlcna+MXiZavcQ6j9QmX+LXFavhuGUxVrU3eQW9Vm4AZ7PEfJevPk1Pu/z8uXTYaytduxVuL5bpXfUTDLplqLa301bWsAKdYETt6EOBA+axwcPfl2fk11Gfl8cZ6zjf57zLc5jxGm2lXrkDw2CAGt2C9nPy93yuOHny83Ccw43gXiOwi8qWlSryaRjVp33Xgx/h3b2XHcda6fitnHM9zn/NJacPwag2vc1XcVHUW+Ru/cldcr/asscPpHnl+53l9Ywsp2V11S6kV8bv5GGisbmvPw07emZA+gCz1HP35Tpp6jWM7MPv77rd8wvGYsRr5/wzGqdhb5fuGYdhdrqA8moNLl3knHj91PTnxbzn3t9sHF6eYaGb35cyxjD8Q/pHSpvxiu3cNc7cgEcCFz5Mvuse3+a+lxx+9y7v5Z7eH1Xx2hbfcenOXHzY4eGi7fS3Na4dyD9V2uX9m4u7/8mX+vn2zhj/aOXt/kx/08eneOieSmZUyvSuLmnpxK9HiVHd2j91fI4sX0ssnTGVOY47L3PPkF9TZEjGqVmt0z3RSqlQF2yBNR/CBLqiDFfUgyg//RyC8O5X5l9uA1H12R0hlMsqgjgtRGQ0D6gIEmdRPEIsG0zyinsdp549EFueIMIApVSSYG4QOt6hcXayR7IyYSYc1Atus+UrMGTSOghnotwF4nmj+KVSXluswJ+SinsDgyWj9VYhjdenU489lUGwxJQEHggtAEnaUCyHU3SRIQMcynULSQQFZ6Y213NWHC/wANr0tIcXtMAieFnhz1klx29Lonjz6+F1cAu3TcYW8sY1x8xY7Yc+i/S4Xcj4vLNZ393YacgaTy3ZacxtESpRUkOKgsOOpAcy4IDcfLp9QpfQ5z1awsXWX6l4wHx7AtrUyOZYZ2P0SDZMp4lTxXA7K8Y6WV6TS4+jtMEfqukHvFu8g7KBdQk7e6BrRAAHCAwdt0FyOUFbHcID7IKB5QKeZYe5Qcoz81uGZqwHFxsG3It6jv7r+VkdXtHl9JjpmQgc33WhDygMwEAF4mEFnhB5GPmLG4/wC7d/JBpXRgxgd8Advv1f8AmEHTD29UBtcDsgJxCBeoIBHKCygiC2wOUEeRCAWkaUBE8IAcUE//AOINW6gZJw3qDle7y5iFMTWDn2lQ7upV2jZ09lpHxVkjMOKdFc/3uXcz0tWEV3G0xy2eJZUtz5RVaDsdt1XOx0HHMSHRmxxfB7KxpYzkDO7XV8Fvj8FCpXaGim9392VnbGjsJxLGMm43glX+jYbh9jhlO3zhWtoNvd4fceVlyANjpB83yWorRMzYdddD+qVnmHBnG4yzih+9YbUH9lXsrgy5hI22aeFMmpXg9Y8pWmA43RzTlpzm5QzVT++4bWYfLTquHnpOI9D2VxW3bnV/iF7iNwbu/rOq3LmtYajyXOhg0jcrv37mmO3V2+lfsfXjqeIZjsAQA+jSe0emnVv+q+XvWb22bxdH6gYbTtM2YNfFwZ4lxUt3uI+OpcUXSB/dA/ivd7eTDw0DqnU1U8Ls3vcLqnU+80i0mR4TgQf4Ltbjpm+cnUcN6n3DcIol7re5c2iI8aoKFcjTvqDtlw4JjcnXl8YuW9IKmH4lmPMV6+iyldXlw97jTcHhrKlQyZHI+GF5up8cvd9J4b4/GL2PtT40MFyZhmWLZ8vxCs01ANvJTG+3oSV9jpp2/wAV8/ku8nzFk3JGMZ5va+H4LBu6DPGDHTDtH5V8iZb5q9vHhrFjYhlTFcJzEcuX1GMSY5ratJkktNQ7K9V+UbuWnVM/VmZMynh3TTCjOLX5Zc4yWcyQCxm38l7enx+447nfrPDzZ43kske/Tt6nTLp3TtLWkauZ8xCazaY1VKdsfi43Gy8HRYXHky5M/rvTXWX72Tjw+jxb6nkrDIb+zburhN7Yhls10hlW/cPMY9ivdwaxyuWfqs9Rl8mOGHuPWoCh0kyRUxm5Pi5txmnosaTzL6NJwgGTvsFy6bG8ud5c/WPr+jfUZTjwnFh/N7/q8/ofkG5zTjb8zYu0vs7eoa9RzhOqo4z353Xi6nlvLl3vZwcc4sex9XMimwUWjS0bADYBdcMXHuUHQF2qIXjSpBjPhwkoEFwb3QJqP7zsgx3VN+UGPUqcxyg//9IqmxX5l93SNJ0kIorVxDiEGZMvJ9AEAEzuoKo+appPCLs+oANggHhsooaJh5hBk0Pj+arJzgNTkCg4ueQe3CzA+i4l0nkLcBPGxd3ClVjVCWP2PuibZlNxNMEpstMIgj3TabNotDiQU2bB6n04TaqLyWSeQmwbiX02kn9FZdeGNF3DGua7UPhbAWJNXbU8NCyjWqWHUzw7U6GXVJ4rN7HQJBX3+mzuUkr5HU4yW19H0CXNaXGSQJK9efh5cPMPJOpSgASHfNQESgjHEmUDCJAPdB42ZaNO4wq4o1RqY5jmke0FY2NJ6O1ahy14LjLKVzVYyezWO2XXDyOlMcRspsW8agJTYJh2j0WdroRWpV0PSC35rSVQ4jsFEECSClAtJIMqbC+RHumxzDrG0DAaNUD8SnWplju8lwWZR0LBXuq4fbPcdzTaTHqQtSD06bieVQZ7FBCZMdoQJjzoGj07KjxMwvIw26PcU3fyV0NL6Kk/sS9Hre1z/ELnvyOoH+S0IzZ/zQEdzCBfqgtvqgsnYlBTTIQCXHVCAXuP8UBN4QWUAu+IBBcoAePTYkQSluh8zfa4y5hJwTDMztoBmMNr/dXVm7a6REw4d47K43bnWqdGXHPHS/NeUMy/1zB8Jom5w4O/taFQAnyP3IGymXjPGfmzfGNrSMiZkzBmO7tsqYhidwMIZZ3FmW0n6Kjrbcim5xBloWeozvHxzKe96WTeWv8A9dtxp025m+ztizcam5qZWvDQwes7+1p0mvIDS7uIW5dyUuMkjw8hk5n6I5ywXGPx7LAQy9wofnoVSN9Lt/Ke4UpI5jkbA7HMGLV7LENZostn1W+G7SdVOCN4K4553CbjvjjMvFfQf2dsNtsD6l4xh9jqFu7DqdU6zqdqdDjuvPhe67re/o6h1YOqvbXBAL7W5tRQHZprVBqI94EL6cm48Wd7cpI53ma2pYhmiiLkahb27XUwNt3GDK45Tw3J8ztWKYdhzcAtmOs6FT+quYHPptLgBTmQY5TpMJcnTqPGLj2QcGw6wx/LN/Z0RRrYj94t7xrdmVGNIeCW+srny+ccp/8AsY/Sfo0b7XNeq7NOFW5d+G22cQPQl3/svp91nSyfp/y8eOMvN2/RqX2fsx4lgmcGW9iaYZdwyoXs1OAI/Key8OHHPvMb+eo9mfJccLr6NrypUGZermL41i1NlW9thWqMhsMLqLTp1DvC11uEx6jHCeq82GVz4rnfbWenzBm7qu+5x0m4qm5c70HkJgRvst/Fc7Mpxz1i9PRecN/mzc3Zvxyw6iYziVvWaKtvqs6NJzQ6k2iRBAafkunU4S44z9JXi6HO4ZZWfnY2HKFU5szfhdtjTW1bbDrYVrai0aWCo6XEkd15/iePfxce/rHo6H5ebky+srnOesUvcx9QbqhilTxKFCv4FCk3ytYwGIaOy9vxK/d9uGPrU/7PP0GPdblfdr7GybhVjg2WrC0w+kKVHw2udHLiR+Y918bjnfbb9H088rM5h9LHsOK9UmnHQDsFapbnGCjO2M5xhGoSSSCCpauiahMQmzTFqFJUrGqEqpt//9k=";
exports.default = image_base64;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL2ltYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBTSxZQUFZLEdBQVcscXJpRkFBcXJpRixDQUFDO0FBQ250aUYsa0JBQWUsWUFBWSxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgaW1hZ2VfYmFzZTY0OiBzdHJpbmcgPSBcImRhdGE6aW1hZ2UvanBlZztiYXNlNjQsLzlqLzRRbFFhSFIwY0RvdkwyNXpMbUZrYjJKbExtTnZiUzk0WVhBdk1TNHdMd0E4UDNod1lXTnJaWFFnWW1WbmFXNDlJdSs3dnlJZ2FXUTlJbGMxVFRCTmNFTmxhR2xJZW5KbFUzcE9WR042YTJNNVpDSS9QaUE4ZURwNGJYQnRaWFJoSUhodGJHNXpPbmc5SW1Ga2IySmxPbTV6T20xbGRHRXZJaUI0T25odGNIUnJQU0pCWkc5aVpTQllUVkFnUTI5eVpTQTFMall0WXpFME1DQTNPUzR4TmpBME5URXNJREl3TVRjdk1EVXZNRFl0TURFNk1EZzZNakVnSUNBZ0lDQWdJQ0krSUR4eVpHWTZVa1JHSUhodGJHNXpPbkprWmowaWFIUjBjRG92TDNkM2R5NTNNeTV2Y21jdk1UazVPUzh3TWk4eU1pMXlaR1l0YzNsdWRHRjRMVzV6SXlJK0lEeHlaR1k2UkdWelkzSnBjSFJwYjI0Z2NtUm1PbUZpYjNWMFBTSWlMejRnUEM5eVpHWTZVa1JHUGlBOEwzZzZlRzF3YldWMFlUNGdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBOFAzaHdZV05yWlhRZ1pXNWtQU0ozSWo4Ky8rMEFMRkJvYjNSdmMyaHZjQ0F6TGpBQU9FSkpUUVFsQUFBQUFBQVExQjJNMlk4QXNnVHBnQW1ZN1BoQ2Z2L2JBSVFBQkFNREF3TURCQU1EQkFZRUF3UUdCd1VFQkFVSENBWUdCd1lHQ0FvSUNRa0pDUWdLQ2d3TURBd01DZ3dNRFEwTURCRVJFUkVSRkJRVUZCUVVGQlFVRkFFRUJRVUlCd2dQQ2dvUEZBNE9EaFFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVS85MEFCQUJhLys0QURrRmtiMkpsQUdUQUFBQUFBZi9BQUJFSUFod0MwQU1BRVFBQkVRRUNFUUgveEFEQ0FBQUNBZ01CQVFFQUFBQUFBQUFBQUFBQ0F3QUJCQVlIQlFnSkFRRUJBUUVCQVFFQkFRQUFBQUFBQUFBQUFRSURCQVVHQndnUUFBRURBZ1VDQkFNRkJBWUZDZ1VEQlFFQUFoRURCQVVHRWlFeEIwRVRJbEZoTW5HQkNCUWpRcEVWVXFHeEZpUXpZbkxCR0NXU3N0RVhORU5VWkhOMGRZTGhSRk5WWTZJbU5VVTJaWVB3OFJFQkFRQUNBUVFBQXdZREJ3SUZCQU1BQUFFQ0VRTUVFaUV4QlNKQkJoTXlVV0Z4STBLQkZETlNjcEdod1FleEpEVFI0ZkFWSlVOaU5ZS3kvOW9BREFNQUFBRVJBaEVBUHdEM2FWUFJ3dnpEN2c1ZHE5VURHY21lNnFCcU5hMXN0NUt4VzRHaTFtNTdwRnJMb3QwaGFRZFF3RUNIUGJQRy9aQTJqNitpZ3pKblNqSUg2US9VL2RCVk51bCt0bkJWeDlqSU93UHV0NUREcU5kTWpoY1BxM0Q2WWNZVzR6VHlOZ1IyVlNCck9HeFBDTkYwbk5MaHNqT1hwbXVlMXpJanNxekN3S2ZmbFJ1S2FHNitWSVU1ak5SVzQ1MFJsaGdxb0xkcEVkMEZHbUE0a2Q5eXMxdUxhNkpCN3FLYXhzU1R3UXJHYWErak9rdE8vSzB5b2dqekU3SFpBdlNCVWtkMERSVExnZ2hEZ2dYNWZFQWQzUVR3NHFFc08zWmM3N2JucFRUTGkwaVhTdFJUR2d1SUViamhhWnlOME9hWmNaQjRSbEh0THRoeWdFc0ozSjJDQTJBa0dlM0NDZ1Mwb0kxQXpRN2R6VHdFQU1CMHozN29EcGs2VFBLc0F1YVpiOGxRc3RsMmtkK1ZLR05ZR0dEOEtndDNod2RrQ05Ha2trUlBDQjU4elE0Y2psQVRmaGF0TUJMTjlQcHVpcXFORDkwYWdRMGthUndFQWlHdVVvdlR2UHFvQzBrb0Z1R21vR2xBTHhwSGwrTHNnTnNhZlB1ZXlDSHk3L21RSWVYZDBCVS9oS0JaYTQ4RkJOSmFOMERCdVBrZ3J5bVE3c2dROTRKMHdzNUxCdHBnaVZHMVBCMkE5VnNMTk02bGdFNTBIU2dqaExaOUZ0U3BuZEJZYUhEZEJUbU5ZQ1R5ZUVDZE1DUHFwVVUxa3VoWkZ2cDlpVllGdmFHdkMwb1M3ZEJIZWJoQUxtd05rQ2kxeFFLZVRHa29GbGpJM1VvV2REVHh5b3F4bzVJMlJuSlRuTUJrY281di85RFlhVWpsZm1IM0RITkU3Y29BY1RNSGdLb0YwRVNzVnVCcEE2cEhDWXJXYXg4Q0NObHBGdXFlM0tCYm1CMjRHL3FnT2tDSmIyS2d5Mmd4N295eDlVUDBPUVpGQXdDM3RLdVBzTWVUQzNrTWN1ZHFBUHc5MXdiaktwK2c0VzR6UnlBWVBCNVZxUUZWc2lSd2pTcVRBTis2Smw2WlRZQWhWaUUzRFRxR2d3bzNFcGx1b1NaUGRTRlpEQzVydFUrWDBXNDUwMTVaVUFLcUtrUUdqc2dnRGdEUEt6VzRtZ2xtdnZQS2ltaUMwQXF4bW15UzJKV21TcWhPajM3SUlOSmNKRzRRT2E0Zyt5QW5HZVVDM1V3NGgvb2dwd2E0a3NNRWNybmZiYzlLcEZnZUNPWjNLMUZaYjJ6SmI5SVdtY2c2REFKNTdveVc0a0FrY29DWUE1b0hyeWdOd0xRQUNnRWduY25kQktiUzZSMzlVQmdGclRIUGRBdWFqWTA4SUdneUpJM1BLc0VKMjIrU29VNGFYRXQ1VW9MekZzOTFCQVFXd1I1dlZBTlY3blJ2eHdndWtJQjkrVURXbG9HNDJDMHlGenh1Znpka0FnQ0o3bEdvakFSTURaQWt0T3ZVVEFuaFNoaEkxQURnS0FpWUFQdWdXOHRlN1VSd2dXOEV0MUlMcGx5QW5CemdURzRRS2VaOHJ0ejJRVXhyZ1kvTDNRUjlPWEVNMmhCV2x3YWRSbUVCczA2UkEzUEtDTkRTNDdJRTFXQU80MldjbGl0UmFEQ2paYnF6bVV5NGZFRm9VeXFYTmE1MnhQS3lDZ1BkTUlETFprQjJ5MkZGdWtIMlJWdGFZMU40UUxyTmRVSTlrQTlqdDVoM1VxRnRKRXp5c2dIT2RJTXF3VTRGeDFIbGFWR05hOHdSdWdKN045dUVFMGNJQmVXamFOd2d4bnMxRW1FQ0lpUVFwUXAzRXh1b29tTkxtUzdoR2NnbGt6dHVqbS8vMGRqYVJFcjh3KzR0dFFUdWdOMFAzYUpRMFE3WVFwWTFFbzZodVBYaEpCbTB3WHhJaUZRMTRwZ2JoQWx4MmdDRURiWnpUSUkzN0lIZ295eGExT1hTRHVzN2EwektEV3RwZ2txend5SjhFYmNMVnV4alAzMjdybGZiY1pGSFZBQkhBNVc0elRRMFBITzZ0U0VnN0VPTVJ3alFxUHhTVDlFU3NsN1JHemsyem9wMEVpVDdLTkI4Q0tnTFRNZGtoV1EyZUN0eHpwekF3Q0lUYTZSd2d5QXBzMGdjUUlJM0tMSXZWNVN6c082aWpiSkFBQzFqR2FKeGdSTUZWa0xIdGRzUnQzS3pzRkxRN1Vyc08xTWMzaUN0U0FIZXlpYlVkVVFEQ3p0clFBMEF3REpQS2phbWpTUzBEbnVxTXVrQ0d4TXdxemtadUJKNFBDckpUd0RNRlFBM3l0MGpmM1FPYVE0UVJ5dGFBdkxXQXkzaFFFeW8wQWx1NVBkQmNIU2dCcnRKRGVmOGtEUTRIWUNTdFFVOEFqWlMwTDJMaUNZUFpadEJ0blNSSENDTmd0TzBPV3RCV2svSC9CUUd6ZEE1ck40SWxhWkRWcEVHZXlBSFFCdHdqVUxKZnBHbjlFQVBCTFFYY3FVRzBBalZ3VkEwQUZoM1FLSURCNnlnVFZscmR0d29KUWM1NEpEWWhOZzI2eExTZWU2MUlBY3lCdWQvVlFVd0VjYm9DZDVXU2RuZDBBQTZtbVNnSmdBYUVGT2FlV29FdWt1Z3JOV0xZSXFBT0d5amEzc2E1c0FkMWRqSHFVWHlISDRSMlVCTkEwOG9MQWdUeVZzVzlzczJQUDhBQkZLYVhEWnAyUUdDUTRha1MwTHcwdVBaUkpkc2FvRE1wcFZCdXZZRk5BMnQyZ25mc3FwYm1PYTZCdFBkQkNYQWFTTjBFMzJRVTV2ZEFEZ0lsQW9Vd1NaN3JOQ25VeHVQVHVvYkFESGw3SWw4cW5TZlZHZFAvOUxaeUlId3I4dys0ald0Y053Z2ttbVNHL0NqVUpxYjd0UUhiUkpCUVpyVEI0MlFXOW9lSmxBaHpYd1lHNFFNcE5PeDc5MFNuYnQ4eDJDVm1BTzdsenJwRDZiUzVtbnV0c0RjTkxZOUVHTWVaQ3pmYmNaRko1aVNOdUZxTTAwQWh1b2NxMU1TWHc4Z2o0dTZORERSSVBkUXJKMEZ3QjdkMFpKcU1tUzNjQkJkRjVEdDBpMW1hQVJJVzQ1MWZoZ0RVT1ZHb0FndUFncUtqUVpJUHBzZ0pvTUV1NVFQcHQyQm1KNFc4V2FPbzFyZ1c5L1ZXc3NjTjBndDlWekViQmRwUFpXRElZTkpraUdycEJITklHb2NMTlpBRE82eFhTS0FESDYreDVTTkMwZ3VrZDkwRFF4eEJNeEMxR2FZQTRnQStraFZrT2tBQjNaMndVb1hUK010ZHlFZ2Q4Smp1TjFzTGMwdkIxTElZeHJXc2tDUWdQVjVVQ1NQeEpIZmxBeG8wSFphbm9XNGlPVmloVGRuRjBUQ3lHVXAzZWUrMExVRnVZUkxtcmQ5QlRYalREbGtHQjZJR0Jyd1puWmFaVTU3bW5mek5QcWdBUThuVHdqVUFSdDVUdjZJQTB6eHY2cVVXeHJpU0FObEJiUzdVUjJqK0tDNmpRV2dGQWlvMXhhVzlpcFFOTWVHeUFkMUlEZDV4c2QxMGdwN2c3YnVzaTJNOUNnSjIrMFNSeWdXV2dna2JJSTBpSUhibEFVa2JBU1NnV1d3NlR5cFZnWHVMalA2TExhVWc0dGNIZlJBdXByMHhPeUFRMEFRVHVnamYzVDZyWXVwcWJ4d1VVb2VReWVFVTV3YTl1b2R0a1lvSE1hUUNESjdvbUxIY0hiaU5rYktCOE03SUdUTUVIZEFWVThPQWxBdlZMcGVJZDZJSS96YnRRQ1FTRUNuVDlFRk9BalVPMnl6VUtlQzRTZGxBblRwZE03SUJkdVJDRC8vMDlwcHZFZVpmbUgzRUpEdmhRTGN6MU82TkZFT2FZL0o2b0cwbk1hZkwzNVFackhCMjBjb0JjU3g0QTRRQVh1RlhTUnNVRHFaQkJBN29VYnZPelFPUWxZaEFPaCs2NTEwalBwT0FFcmJDUDNCUHFneG1BQjIvQ3pmYmNaVkdsSVB1WldvelRIQ0dsdm9yVWpER3hLTk1pa05UWjlGQ25VM0J3TFhmRDNSa2h6ZkNxNldtV2xCa1VHdGM1SXRaQUJjQ0F0eHpxTi9jUGJaUnFDQUFFS0tCckNIeWdaNFFlNXBtQU9VRGhUYnFnSDVmTmJ4WnFtTjBPY0NaVnJJSE9hSEFrYkxtR2FBNXdjM2xXQysrbXBzdWtCTkVlVXJMSmI2WkVrTEZkSVhCRFRQZEkwWlRhUUFTZ1B4QzJSN0xVWm9uVm0wMnRMdSt5cklYT0VsbzdjZlZTZ3JkelhrdFB4RGxJR09HMnY2TFlCWUJVL0sxclR3cUhPQWJ1ZUVDbkFFNmh3VUJ0WTEyNUsxUFFYVmJCR2xZb1VHdTFlM2RaR1ZUMDhSc3RRVThtZmJqOVZ1aFRtYVhodmJsWkJNK0lqMFFNMWVWYVpRa081UUtPMVFhVWFRZ1NVRk5CQU1EdXBRREh1OFF5RkF4cFk2WjVRS2REZHU2QmRRbnR5cFJkRW44d1VnTTZUd3VrQzN0MG5aWkZ0TUJCUnFBYm51Z0Z6dkxJN29LRk1rQnc0N29HTzhydDBDNmpnVENsV0ZuNFFzdG1VWGlDMDhrUitxQkx6b2RwS0JaRGRYS0MyL0V0cU4rNEk5RUNYSHlSM2xGRXhwMHhQS01VTzdYYVVURW1vN1M2UFZHd1ZBR3dVUllCZEVJb2dZOHBRVTRPalZDQVRzMzVvRnRsMCtpQ3RCQjUyUVUrTkJoWnFGT25TSVVDYWdjN2IwUUxJMGozUWYvOVRhYVphdnpMN3dIeTEwb0tNbnpEa3FMQzZoZVJwS0tLaFRiTW5sRXJQSGxDSWhlQURLelZoQXFqVnNOMUlsWmRFQndKUEsyeFZ1WVZDTVlocGQvZVZkR1pRYWFiSWR1aURkQVp0MzdJTWVpQ0hRZVZCbTB6QmxIT3JleVFYS3Jpd3lKZkhjYm8yektEb1o1dm9pVTBGclJ4eWxTTVo3dk11ZGJQb0xjWVBPem1nY0xjRHFsTVBhWTdGUUJHbHpRb0xPb09sdktDMkNYYVhkOXlnZnlQTnlPRll6VkVtSTdMVElTMlEzMFVvT21kTG9IQ2tCMUMzMFhTQVlDVm1GVlFXRUhscFhPdWtUVFRleVc4cU5LZ3NhMXAzM2xBeG9EekMzaXpUeFRhUUIzRzYweUJ6QWVmaVVvcGpYTmVDRkE2UTRRZWV5Q0JqZnpJSkRScDBvTGMrRHEraUJKRG5TSjJsQTVvTEFCOVVBK3BLQlRxWVBtSmowUU9hU1lBTXdFQmxtc2U0UUxKY0RIWUlMWnNTN3NnTmhwbjRscGxDVE8zSFpCQUNYYi9SR29WcklKMWVxQ3RXa3kzdXBSWnBqWnc1UEtnZzIyUUM0UVk5VUNxb09nRlNpcVJQZmhTQnBMUjhJWFNCWkVsWkFDVzFZS0J6dzBpQWd4M0RSOGlnSm9JRXQ0UEtBdFhpUzJOMEMzTjBEZFNyQU9lQURJbjJXV3h0YXdnT2JzVUM2alM0eCtWYlloVHFZUG1SMGdtTkVha0VKYUpkNm9FVkMzVXMwVTF3YVpDS2E3OFJ1eTFpbFk1QkFMZTZWQ3R4L2FEWllVVFlrRnBoRW9pelh5VldCQWZoYVZwMFl6dFJNSURMU0FJNTdvRmtBR0FndlExekRQS0JaOHNOUUtleGt6M0tCRlNpQ2cvL1YyV21KWnFISy9NdnZEYzRQYkJHNFFBQ1dBZXFpd05TcEtLT2szVjVoMlJLeXRYazRrOWtSUkdwdTIzcXMxWXh5NHNlcEVyTnBPTE41MklXMkt5QzVybXlObENNTjFObmlhbW5mMVZkR2RUR29BdDJBNTkwUlZYVHkzbEFGTFM2b1pVR1F4aDFRZHgyUnpxM1BJSmJHeXE0c1Yra1BrSGRHMlhSQWRDSlQvSzhSSENWSXhxbE1hOWx6cloxSUFjTGNZUGFSRzdlRnVCalhBaUJ0UEtnRjRnZzkvVlFFeDBEVWVVRmtmblFHWGEzQ09WWXpUSEFhWVdtU2dYZm0rRWNLVU1walQ1anVEd3BBNEFQWVQrWmRJQWN4b0VnYnBXU3FqUm9rSGRjNjZRc3l4clEzNnFOR05hMm9RUFJBWXBnTzlGdkZtbWpUUHFWcGtEOW5TcFFWSjJ4SkVxQzNRM2VQa2dyVUMwbDI1UUd5bTR0MUFiRHNnb05KTVBFQ1VGRmdKTWNJR3Q0aEFCYVRQb2dXSFNJZDJPeUI3SE5BOVR3Z283QW1VQ3R0UDk3dWdOZ2tib0RiU0RnWU1MVElYT0lJQUUrcmtFTDNGN1FCQS9lOVVhZ2F0TW56eklLQmIvTHAwamxTaGcxS0NnUE51Z0NwcW1SdVVDbnZEMkJvMmNPVkJiQUlpVklHQ1c4N3JwQUpicDNXUXFvUzV3ZEc0N29ERHdXaVJ2Nm9GUE03ZGtFWnpIWkF6ZzdJSTl1c2U2bFdFbHVrckxhUTJVRWM0VEhaYlloRzdoSFpIU0xwdElCQjRRUTBnNlJPdzRDREdld0E4eXMwVkdvYVEyRDZvcklwTkRHNlhHU3RZcFNhclMycHFhWmI2SlVMY0hWRExvZ0xCQ3dHRXgzQ0ZHNENKQ3JGUU9mRzUzV25SR0FUSk82QmptdGp5OG9FK0hJajh5QUEzbjFDQmJ0eVozOUVDWE4zbmhBdDArcyt5RC8vMXRtWWRKQjdIc3Z6RDd1elhhWkJHeUd3MUEwajVJMUdQQUpSV1hSSTBFUjNSS2RTYzFwT29UUENJRi9sTzNCS2xnUVFIT2c5dTZtaXMya3phU1o5bHBuUTNScGdLTEl4MmhwZFBKVmFaemZLQUJ3UWpIY0I3U0NUMkt6YXV3VTZaa3VCZ3F3WkpwMVlEbU8rYXJPbHVKWTBCM0tMSXhuYUM2WTNScGxVZ0EwUnNmVkVwem13d09hZm1sU01kNUlPMjY1MXMybVhURWJyY1laVGpHeTNCWWFTQkNnbW1XN25kUUMxd0IzL0FFUVBnVkd4TUlEWTBiRDA3cldreVZWbjh1NnJBUWRRZ0JTZ3FUdDlMaHNGSU1oZ2ttTnBYU0FYOFFlVm01TDJsbG9JMHg5VmxyU253QUJDaXEwSFl0TUlHa0dHazhoYnhab3RJalVOaXRNaG1lM3NnWlJMSkxTcG9HUjVvNUJUUVc5Z0FJVURLYjN0MmlSNkxYYUpVY0hjcklXMW9tQVVER3RoeGtyVWliRHFjWEVSdHdwWXNDYVlrVDJVMHRobWxyUUNQbEN1a0JXYTdiVHg2Sm9LM0RwUDZLQnJhZ0cwSUdOYU5KMDkxcGxRMk1SdDNRWFVCRGhIQjRDTHNESWd0Sm4yUTJVNE9CbnNDcHBUR3ZEaUoyamxOQ3R0eU9BbWhkUUVBRWNsTkRIY3h2aW4rS2xnam10RzdmTVZtQW0xUU9RdWtFTHhFa0tXQmV0dTRoWkJBTkxJL2lnVHBneEtvdG83b0RhWkpRUUh6S0xDYXBKZENhYkEyWkcyeWFCdWo0bytpcWFKTGdURFVhRUhBRFNlVUVJSUJQTW9NZW9HeHZzNVpvVzB1bm5aUTJ5MkJoRy9LMUtBY0dsaCthcU1Zc0RUcG5sWjBGT3BnR1FvR05MZElrcXM2WFZhQjVtbVZvN2dnekFJUTdna09EakhDSGNoQk84d1VPNVlIcWgzRmtBT01vZHdLdWdnZk5JZHhCYTJkdVZkSGMvLzE5b2UyQU51RitZZmNHV2h3QmI5VUFRTjVLTndoK3g4cUt6YUxRVys2SlRBQUQ2b2dYMHlYU0RLQlFCMWxCbDB3NkI2SUxMWDY0UENCYmFSWlU5a0dlMHRJOTFYSW1vU0hFT01Ec3VkYmhsQm14STNIWXJXUHBhYXd1QTBuaVZVVlZickpneUI5RVdNYlFkVURjb3JMcE4yaHlKVFd1RG02UDFTcEdIVWVHUElKK1M1MXRsMjVsdXY4M290eGcyZDVQQzNBeG9tU0NvQkR3REJLZ214T3dsUU9hREVBUXRRR0d2YnlOaXRKa3Q3bXRFSXdVQ0JKYnY2cVVOWXpVSkhLa0RXeTF1L3hMcEFGV21YZWNGY25Rc09jQkpRRklKZzhvQ0I3QkEwdGM0U0J3dDRzMEljQUlLMHl2U0FkWDVVRThPSmQraUFtdTh3OUVGdkJJTGh3c3dFMlltTmwwZ3J3OVRTVnpvVzFwQWtjb0dNSk81NVc1NlpxVkdpSkIzVXJjQTJJbHgrU2thcG9aUHk1Q3JDbkhTTjBvV1lKbFpGZ0E5a0JFT2FaYVBLdE1wd1pLQXk0azdEWUlFQXVEanFFZTZBWFUzNnBtWjdJc01EZUpFQ04wVlRkTFpiK2lDcXJuQURUdkNBQlQ4eGYyN3FYMExhME1sL1lyRUZlUThCZElMUG1sbW5jSlFqdzVKSTRYT2dtTmIzS3NBMVFJOHZLb0FUcGp1Z05rUjdvSndaUENMQ0toZ2wzWUk2S1k0UnYzNFFFenpPanNnVVdEVkhDQ1F6VkpPNFFFWHlUSENCRlpvZDVwM1dhaFZNU1lVR1ExdWdpVHN0UUM2QVRIQktxa1ZtUEc0VXFFT2E4Q1NWa0ZwOHN1MlFHQTdTdHVhbmNpT1VGNy9vZ3FDVUF2ZHBIdWdDWTJQS0NuUnhDc0NueDZLai85RGJpSEhzdnpEN2hMcGFZUUNXbmRHNFZvT3FVVjZOSUFORGh3RVNpTE5VdkNJVkRuYis2Z2pJZFVJUVpiUkFBVkZWSGFYaEFMWGd2NFFaVGRBUnlEV0xUQkN4VzRLaTdzdFQwdE9hUUNaN3FvdW8xcGJzWVJZeEd0TEtramVkbEZaekpnRWpaVktVL3dBdFNRZGtxUWh6R1BmSjlWenJiTVpTTERxQjhxM0dHUWRPa0xjRWRwQithZ0I3QWZoVUZUTU5ISVVEMmtrQUR0eXRRT0JJQUo0QzBtU1BBckNSMlVZS0ZQU1RLVU9wbUZJRGN1a0N6cWFJTzRYSjBWcEJiUG9VRUxvSUJHeDRLQ3RMcVRwRzdTZ3lHYXdOWFk3cmVMTlhyWlU4aEVGYVpBOXVueXlnTU5NY29EWXp1ZTI2QUE0eTZlRm1CclM2ZE1McEFGUnZtMk8zZGM2S2F3Y3RPL29nSVMwYjhyYzlNMEJsNExSc1ZLM0ZOWVNQRDdxUnJJOERRUXduZU9PNnJDVkdUODBvRFI0WE81UEN5S0xRZmkyS0F3UzBhQzdkYVlDN2hBVFhrQ0lSUzZna2F5Z1dIeWl3WWRIMVJSQ0pRVzRBOWtDNDM5dTZsOUNCd2MzVDdyTWdwN1BSYmdnTG1nK3AyU2pHYytwVE9sdzJXS0RhTmJkdXlRTEg0am85TmxRSllXbUVGdGFaUU1kdTNUM1JZeDNVekJsR3lRZDQ5RURDMHhxYWdCNTg2Q250MU9CSDFSVE5pQ0FneDZqREhLelVBd09ZQ1J1b0dBZzc5MXFDNEJtVlF0MHdRZS9DbENUVElNbjZMSXJVNG5UQ0JqZUZ0ekRBR3g1UENDRFVHa2VpQ2hVaDBIbEJiZzE3VDZoQmpGcGdQUVhJTGZkV0JMNlpBMWRsUi8vUjJ6VzZGK1pmWmhaYVhHVHdvM0VjQUFDT0ViVTNTNThPNFJXVXdRUGJzaVVlL1pFS0pPcmRacXdORm40MG5oU0pYb2hvQW4wVzJQcVJYM0NOaW9pZmlRWkxRMkVZSXF0R3FSeXNWdUcwZDlqOFMxUFNVOHNJNU1xb1ZXQkRObG5KWUdtVHJiUENSV1d5WU1MUVZYWXdrUnlVQzlEVERVR1dHT0RkSUtNaUFtUWZpQ0FtR1lKUVNwc1drYzkwRmFaZFA2b01pbnY1VHdlVUZrT2pRemlVWnFnMTlNeE8zZFZsUEtYQWwyNkJvRFZvR0MxM2xRS2RMVEErRllkRkVTMlJ3Q2dqdVE2ZVZBMkErTjVLMUFiUTQvSWRsdU0xWmdIaFZsWkd0c2ZWRW9xWFB5UkJGenFaMjRLSVVYNm5jSXAxTnFBWGFTU0RzVlVRTUxJY1RJVHVkWlIxQzF6UVFuZnRuS2JwUk1RWERaYTFzbDB5clN6dUxwNEZLbTUzOTc5MlYweDRyV2MrUjdkcGxTMG8zQXU3eW81OWJUR2xlakhoZVRMa2VtL0M4SnFEZWs3VWRnOGNoZC91dkRqT1RWZWRmWlppa2E5aFVGVnJmTTVqdmlnZWk4M0p3dlRqek5jTEh0cU9hNWhhNXV4WGh5eDFYcG1XMGExZEdBK0U1cDJPeDVRVlVwRWxwQjdMT2wycHUrcHIrU25jNlRIWk9nYWp0eHNFN2k0c2ltMFJEaDhpdGEyeGJvYmFCZTROWTF6M2ZMWmErNnRTY3VuclcrWHJ5NEFlOGlsVC9lSzY0OE5jOHVaNmxITDJHMGdQSGM2by93RHU4TDBZOFR6WmN1NEtwbHpDcXJDTGR6cVR6dEt1V0NZWnZCeERCcm5EWFJVR3VnZmhyRHN2Tmx4dmJqeVBJZUdOZEFFdTdGZWZXbmJPN0plSGEvZnVzMnVlUHRUUkFudVVqZDlnYXd5U1VSWW1ZN2QxS0xqZmJudW9DYk16d3RZK3dtcUhBeURMWGNxMEtGT1RQWllCbnlEVUVDM2VZYWtDL01lRWRJZzFqbmhGUjdSVUNCVFFHbUVEb2dBcXhtcU93UHZ1cXdVOTBOQlNyQUQxOVZsMExKY1RCK0ZFTnAwOVI1V1k1Z3JmRUF0d1Fsd0hzcFFrN3VCUVdEdVFyQmJnTk82b1h3TnVVQVBrbUVILzB0c1o1bTc4cjh5K3pGbHZsanVvM0NTU0RwZHVBalpiU0MrSVJYcE1hQUFEdktKUk9hUngzUkNIa0RkeXpWZzZJYTRGemZpVWlWa3RPb1FUQkhaYlkrb0tnQ05pcGcrcURKcFU1QkpSZ3F0VEFFdDJqdXNWdURwRXl0VDBsWkJnUVNxZ0s4UnNKQ3prc0xvaldkOW80S1JXV0dFUVduYnV0QlZhbUE5eEcyMng5MENXcy9FQTFjOG9NMW9EU2pKanFZMTZodFBLQVMyRDVkZ0VGRndCa2pkQlFlUjVva0lIVTNPTGdlQjZJSEhZN2NGR2FFRnpYRUV6UENySVR1NFNnZlRheU53dEN0TFd2QkJnSUtxRnVnZ0RkWWRDOTlBMG1CM1FEeUFTb0gwWUE5MXFESWlScTdoYmpOVnM2QVFxeUhVV3pBbjJSS1l3dGowS0lOenBZQ1VBQURzRUQyYVJ5Z0N2U0Q5Ky9aTm1ndGFORU9NRWNGWHMybXo2RnBXdW5DblNway8zdXk2WThOWnZMcDdkdGw2bFRBcVhqOVJIRkx0Qzl2SHhQTm55dlhZS2ROZ1pTYUdNSEFDOWVPR25teTVOcWM2QnQzMks2VFRuc09yVnNlRXVVak5scDFDcTZtNEZuWXlBcHFXTVRLeXZJekZZc2F4bUkwQnBwMVRGV1BWZk41ZU45VGh6M0d2bHNOMURoZVI2QUJydkQzTzVRVWROUFRJTXh2NklnUUM0bUFTN3NBdGZkN2FuSnBsVytFMzk0OGVGUklCNWU3WUJkTWVDc1o4OGU3YjVib1VtemQxdGJ0cFkzZ0wwWThPbm15NW5wMGFWdGJzRGFOSU5BN3IwNFlhZWJMTzFicTdpWW42THJKSFBkQjgrNnQwenFya0FRT0ZocU1ocHAzZEkybHcyV1BFTko3Rlo3WTdUSm8ySldMOFB1cWxzN21rZWZVRmZQNWNkUGZqbHQ1eDh6L2t2RFBidXBvQkVrYnJvQlBCVUNtNnQ1S2xGalcweU82Z3N1Y1RCR3hWeDlnSHRMQkIzQjRXcUFEdG9IZlpZQkJzaU95QldraHBBNFFBSGh2UEtPa0ZxMUJGQVlEREhQcWdUcDd6dWdjMTB0MG51ckdjbGxvR3p0MVdDbnRZN3k2ZGtxeGp2MU5HMndIWlpkQTZ5NXNJZ3FaTFhUMldZNWlyQ1NDRnVDQXVMUUNkdlJTaFJaNXhIQ0FhbmtjckJaZTF6UUQyVkN3UUhDTzZDVkd1a1FaOWtILy9UMnQzRXRYNWw5cVFPcDdqN0k2U0xlR3hxN3FORlUyalZQdWd6NlpnZXFKVEduY3lpRnZwdE15Vm1yQTBCRG9idDdxUkt5SkVpZjFXMlBxajJtSjdJMmxNY2I4b01uWG9iQTNSTkV1ZUMwNzhyT2xodEJzUnZLc1NudllYdDlCUEtxRVY5VElCK0gxV2NsZ0tja2dEZzdTa1dzeW1DMEVIajFXazJHcUNRTjlqc1NpbHRwSHhCdnNneTJ1SGZkYTB5YUFTRTBLWnpwSStxeUtMUThRT1FnZ0lhUGwyUU5weThFajlGcVJtMDR6cElJajBLdmF6M2JLSkpBOVFwb1NPNTU5RTBITkFKaWRvVkVkQkkyNFFRdFk2WVBIWlRUV3l6RTh4N0thTnFnT0loWitxN1d3a09pTmd0UkxreWhMaHR4M1c0emNsYWhJYUJ4eXFtMXQ1SkhQb2hzUWJPNUgxVlpXNXZsQW5oWFNiWFRCTGcwY2VxeTNvME5QelBxT0ZkSXFuUnVMaC9oMG1senVOaHN1bVBHNTVjbW50Mk9YOU1WcjhpVy9rRzY5L0h4UEpuejYrajEyaGx2NWJkZ3B0OU9WNk8yUjU3eVhMeUl5UVQrWTd6eXR5NllxZ2RvN2p1cmJ0TkpFck9sRHNzM0RmMWFsVzEyazdGYng4TTVZN1pWT2xUdXFOVzBxQ2FkUUhTUFJ5eG5qM08zSGwydExxMGFsS3RVdDZnQWZUZHBMVjhpNDJQb2R5cVZsYzNEdEZGaGZ2eTBiTFdQSGF6YzNxVWNzMUs0QnZYK0VCdkEzSzlNNmI5WG55NmpWMXA2MXRodGhaajhPbUhrZm1LOUdQSEk0M2x0WkhpdU93MmFQeXQyWGFhampkMERwY2R6dDZCWGFhQkdsM0pncUtoWUpVWGFQUDhGTkxzSWc5MWRKc2RNa0FBN3daQ0x0NW1hS1dwdEcvOVJwcS9Qc3ZOMUdIaDZ1SGszV24xUExVbnNGOHFUVmZTczhMYVE2WTJIWmFyTUNCek95Z29OZ29DMUFiUjlWTkN4QkVLendCck1KSHQ2cWhBWnVzNkJTQnltZ3N1MmRBVFF4M04xNzhFZGxHdGlZQ1pRN2hWR3czU2RpcklkekY4TjgreVdIY2RzV2dSdUVpVzdFMXdEZk1QcXFnWEJoT3gzU3FVNkJ5TmxscVZqdkxRWjdJMGpYTmNJQitxekhJN1Q1VjBnVVI2RkxCVzJ4SjNDbWdManJrS2dSU2puZ29LSVlKQTVRcFFENWs4S3lKdC85VGJhVU9FRmZtWDI0R3F3dDJhanBDZFdsdWtuaFJVcE9CS0ROcHVCQ0pUSWtUTUlnSERUMzVXYXNYUWJJSkNrU25PR3hEdTY2TWZVcDJ2VEV5MVJzNm53RDJDQnJRMXpYU2QwQ25Cb0VIbEJrMEMwN0RrY29sTnFHV3dEM1JDbm5XTkQrM0N6azFBVVhPcHZqVExVaFdacERnU1RBSzB4UFpUbXd3aWQwYkRTbDFTQ2RnZ3pQREJiRGZpOTlsdEFVNnBEb2R4d2d5SENYYW04ZDFsQWdPYVNRTmxCSFU1RzNmZEExalc2WUJqMVc0NTBla2dTRElISzFmVE1MYzROSVBaWmFNMEFzTGtFWUhCc25tVURXdDFGQXRyZER6NkZBSmFDNGxBRGZJK0R3c1gyMzlERy9GRWJIZGFqRlpUV05QNW9Xb3lvdERYU3FLWTBrazlsWWh6Vys2V05RYjI3UWU2ejVhOEdVS0w2ancyaTB1STJrRFpkOE1ISExON2x2Z1lPOTg3UzMvNWJQOEFpdlpqeFBIbHl2VnBNdDdabmgydE1OSEcvUDZyMlRpa2VXOGxwVG5IVUFSUHJ1cjI2VGUxeVBXVkFVaU5rQWc3cXdGd3FLRFpRaXhTSjRCS05NMmhUTkp6WHUyQTMzUlNMaXh3K3RkUHV5eVgxTjNEM1hMN21WMXZJZ2Y0WTAwV2ltenVHamRXY2NqRjVBR3B2QlB5bmRhYzZqbkVuZjhBZ2lLSENDL2RBczd1MjdJREJCUVU1b1B5UUJvOUVGdEJDQzcyZ0wzQ3JtM0lrZ2VJMzZMbHplWTdjTjFXZ2FnUkR4NWl2azJhcjYyT1c0RUR6dGdiRGxab3Vvd2JudDJVQ2c0Z3c0UU95Q3pEajdJQ0RScG1VQzVJSkJPeUN1Sk1JTEJhOXNFZVpBcW9BQWRQZmhBaG84d0IrcXlHdXBnUVduZnVFRlZBSUJKM1c1NkdPWE9rdENsQk5IcnQ2cVFYVWJFYmJGVVZvUE1iQktGUGFlL0N5MUdNODZ0aU5rZElvQU4yYUZtT0xKZzZmb3R3SkFKY1F0VVU1a1NTb0thMkJJNzhJS0pmMzRRS2NBZk1FU29DNXpTSGJMVVIvOVhhMm45MWZtWDI0c2t2MzlOaWpwQ0tsUHhQTTNudW9vNlREcWh5REtEUXh3UFpFbzNIa2p2d2lGVm5IWlpxd3kxOHNhdmg3cVJLelhlRTVzRGs4TGJIMVl6bmhwMGtJMmZUOFBRZ0V1MDhjRkFxbzdjSU1xMkxRVHEyS0pXVTlyUnBJNDdvaFZRN2tzSEhLemsxRXB1RDRrSkNud1I4bHBpZXdPOXVVYkxva2VKNXRpZ3lpV3U1MlcwRnBhME5RT0hkWlJSY1EwdENncGpxZ2JIWkF4akpXNDUwM2Rna2IrcTFmVE1CVlpUYzBPNFdXZ3NpbUlKbVVER2dpU2VEd2dNTkxXeWdxWThwK0lvQkR0NGZ3TmtDNmpScjI0N0xGOXQvUTNab0RscU1VNXAxZ0VMVVpFUEtkOXdWUllFbUJ3ckZoMUtoVXFPREtZMU9KMkFYZkRIYkdkMDkyMndMeEExOTRkTGYzUXZYand4NDh1VjY5Qmx0Yk04S2cwQUR1VjNuSHA1N3lXb1pQSzZ5YWNiNVUzbGE3bSsxQzJUS3Z0TmFDV2hZb2tRb0xETndyQmtDbFB5VkYrSFJZZGJqdU95RUV5czBHR0NQZEdnT3FhekIzUUR2NnE0NUZBdWZKbHBpd3QveEJTWGNXR1JPNjBJUkFRUWNJS0xZM1FVMUJaNFFVM2hCRURyVWdPYzEyN1hEU2ZxczVlWTNoZE5EeG0zZFo0amMwUVBMcUwyZkluWmZNNVpwOVBqckgxT0xRUU40WG1kNldTUzdTNHdpS2MyZTh3Z0VjeDZvS2MxemUreUNPODBFYlJ5Z0ExV3RFRThvb3FiaDZJaFQvaFFLQVk1d0V3VmtPRkp6WEdQTUR4RzZhcWJNTnJWY3d2Rk1rRDJLNlNYUnVNTjlOdzh6aHBQb1ZteFIwdzE4QXFRUjhuWS9DRlFNN0tVS0pBTW5oUnFNVjdaZVhEajBSdUthWFBPd1dZNUhCem9qMFhTQWZNMHpIS29oODBncUJSbllJSTVzQ0o1UUxEWmx2b0pSS241UzBjclVSLy9XMnRuNGI0L0t2ekw3Y1NweVVkSVdYNlJ0eUZFWFJlNXo5MEdjeWxUSkR6eUVabzNOYVRQb3BWakZyTmM1MjNDNXRuMnJTR3c3aGFqTlpKYXpiWmJZK3BOVXlRNXY1VWJOWThPWnVFQ3FtMnpmaFFKY3h4TFMwN0RoQm5VZDQxOHFKV1h0R3lJeG5BNmpCV2Ntb1pTSkpnallkMVlWazdPTW5oYVludGp2Y1d2QkhDTmpZQVRxS0JybXdOWEsyeVp5M3lvTHBPY05sZ1hVZHNaUUVBMHNCUEtNMGJCSUszR1JFRWNMVkZ1Y0hNMHY0V1JHQnVrQm9TSlRtdWMzWWNMU0k3alVnQTd2RTd1OVZrTHJzY0JNOTBkSW9NYzRqZGM3UExiSWEyVER1SVc1ZE1XYk1vTWFIT0RqSG90enk1NjBPblRxZUpvcGd2TGpEUXV1UEZ0bnYwOXV4d0d2VmJxdklwTjUwaGVuRGk3YnR5NU9Ydzl5M3RyV3liRnV6ZDM1MTZzWThlV1kzT2M1ZWw1N2tCelZGUkJUV3daUU0wdU80NFFXS1QzaUkrcUIxTzNBSG5kd2dKeHBNSGtHL3FnVTZxOTRMUzZHOG9FYU9UcW4yVW9wcGdxQnA0UVJhQXVhZ0F0NFdRMENHb0Z1NFFXM2hCQ2tFSEswTFVFVUFkMEYwM09hOVhGSGlad3R4cnRiMXJmTFZIaDFqN3Q0WGs1NDkzRFd0TmM0bUhjZGw4NTc1VlZXd0pqZGM3RjJXeHpqTzNDa3VrczJNYWdyM0oyb0hIVnVwdmE2TGMzUzh1aVZaanRMbG9MeXgwUVFEN2lTdGZjMnMvZXNpMXNicXV4emFkSjFScDlCQzFqMDlaeTVZOVcxeTdpRlZzMTlGRm5vOWVuSGhlYkxrWmxETEdIMDMrSlhjYWhISUhIMFc1d3JlYnc5Tmx0aDl0SGcwVzdjVHl2VmpOUE5sbHM1dHd4b2pRM2Y4c0xyYTV5Nll0M2hWaGlMQ0gwMjA2cDJhV3J6WjRiZXZETnBPSVlYYzRaY3V0NmcxTkc3WDlpdkh5OFducnd6Mnh1Mms4cnoyTlh5eDZ6WE44ak82eHJ5c1ZKYWQrWVdsWTdnNE8xSURwbHJ4cGJzc3dVNGFQTEM2UVUwazdIaFNnZ3dBYktBWENTZ0J6WTM5RUNnZFRqOGtTb0c3clVSLzlmYlFSK1lTdnpMN2NBOGdtRzdCSFNFblp4QlVRNm0wRHpkMEdVeVhOTzI2TTBiZzRNVXF3b0U2VDZybTJiUnF3QzBqZjFXb3pXUUNDTitGdGo2a2h1bHg4M2tQWkd6NmJkdE1iK3FCRlVGanRNOG9BMlk0QWRrR1pTZ2tUeW9sWkk0UkNhakozSEt6V29kUU13MXlzS2ZVcDZkaHRQZGFZbnRqdWFEc0R1alltdGtRVUdSU2hqWUlrTGFJTkxUNWQvWkFST21URVNzb3RtbXFOeEI3cUN5d04yUm1tczB0YnV0eGt6eWxzaGF2b0w4ci9LWldSWWxybWdEeURsSWxaR3poRGZMUGRhUlRXdS9zMzd0L2VRQVB3L0lkeXNpcXJCVWFOK0VkSWpLVWFnRHVPNVZrV25Vd1Q1STFPUEduY3JyanhiY3J5YWU1WVlEWHVBMnJYSVpUOWZ6TDA0Y09ubno1NDk2aGFXVm1BS0xRNTM3NTVYc3d3MDgyWEpzYnpxTStxOUZ4bW5udVZYTUFEOUZqVEtwUFphVHRDQ2U0bEdodFlYR1kyOWtEeGJ1UElnSURheWxUSG1kTWRrRSs4QUNHRFMzMFFLYzR1TW9CSktBQU4wRjhCU2dBQU9WQmNvQ2M0ZWkwSnlFRkVEWlpGT0pHdzRRUXRrQkJZRUNVRWdGSUtpQ3RDeTA3RDFVRkh5ODdxQ0J1cmpaQk5PODkwd0tYaTlyOS93V3ZTaVhVWXFqNWhjK2FPL0hrMEVPSkFCYngvTmZKeTl2b1kzd2J1UWRRa0FiQkpHdGwwdHpCQTM0VXZIdHFaNk9aYTNOVXhUcHVjVHhBVzV3c1hsa2VoUnk1aU5VQXZZS1FQZHk3VGdyamVkbk15eGEwNE54V2RVZWVRM2hkc2VIVGxseTdaMXZodUdVSUZLZ0hPRytwd2xlaVlSNTdsV1dhd2FJWTBOSHNJVysyTTk5TGM4dStJejgxcnd6c3M3clhoanlBaVNzMk5iVWR0KzZ6cExQS3dZSVBkYW1tNWRBeGUwcDMrSFBJRVhGSUY3SDkvZGMrV1N4Nk1NdFZvYm1UcGNPL3dEa3ZrY2sxWDBNUE1BUzBFQTgrcXpyd2ZVTmFtMXcxZzdqZFphWXBjRDhRbEJRYlRCbFpnWjhRazcraTZRVUdDQ2U2bENSckJpZGxCZXBvSG01UUNDSFNEdUVDYWJkSk0rcUpSdmdiZ2JyVVIvLzBOdWM5anhzdnpMN2NKYzRFNmVJUjBoVDVkQkcvcW9hT3A2aVFleUdtYTNWdHA0N294a0dvNTI0bmxacXhpTnFQYzRzT3hsWmthZWhSSTB3Vy9WYTBsRTRSdk8zb3RiVFJBSWMrRm5hczVydExPSkswbTJQVkdyY0hkWjJwSWE0dUVwc1o5Q203NGdyRVpKY0lsS2FLMVNmWWNyUHRZTm9rZ2pqMVd0Rk8zZkljZmtxeFBiSERmRGZKUnNWQTFIdWM1d2dEaEJtMDNOTGR3dHVmY28wb010UTdrZHNBWGJyTFNqR25VMHdBb0dNUGlOQitpTTB3RGFJbjBXNHljd3RwampuWmF2b0xMb2Z4TFZrRUJCa2NIa0lDRENmTTAvTUt5cG8wQWx1NTJYU1l4Q1MyU1hFZ2djRmNkdDZWcWJzemJVN2NBYzdycE1MVTd0UFZ3L0FyMjlQbkhoMFR2cWRzWVhzNCttM043ZVhrNm5WMXBzMWxoZGpoekpwdDExZTczTDE0OGVubHk1TGt5WkJNam4rQzdhMjg5aUVCdm1PNUt1azBtMFNyRlhwbUNleUd4dG91Y0pIOFZGMk5yS1lFVk5rMmdoVVpSMnBnRDNLYkFPclBQTHBUWVdIbVUySkJtVTJJVDdKc1RVTzZDR09RcUJPNFVvcDNFK2lpNlFEWUZEUmhFOWxkbWdnYkpzMGhCSUJVTkJJSktHaGdiUWhwQ0phUWhwR2pTMzFTR2sraTBhRTVwalpEU21nazdoVFNMTEQyMlRRbWs5dDB4bW1tUmFVeVhGanZncUFoeVpUdVhIdzA5Mlg3NDNOVnJHQnRMVzdTODhRU3ZuNThIemUzc3g1TlJtMGNzc2Ivd0E3cXlmUnZFS3pnL1ZtOVJaOUdkUnc3QzdYYW5SRG5EbHpsMng0WTVYcUxmb2Uxd2FJcE1EZlNBdTB3a2NybmFwejZrU1NTNzFQL0JkUERIc3Aybzc4S2JhMEFBZThxS0xkR2RGZ0VxTHBla3E2VkNEQ3JPZ0ZwY3FzV0JBZ2hUU21VNGVIVXlOaTBoSmp0WmZMUXJ5azZqY1ZxWkVhWGJEMlh4K2VhcjYvRjZZM2g2ekpYS1plQ3o1aW5NYzB4MldkeHJSRHh2Q2Job0JiSTB4OVZQQ2FwalFSRFlWM0RWVTVqbHZVcXFBQkpCNVRVL01LYzNjeUZMQllZSWx2YmxRTGR1WjRJNVJtaGRKSGxUY1o4di9SMndGclJBQy9NdnR3b3RHdVQzUjBnZEJiSkoyS2ltVU5ZaHNTSjUrYURNMUZyWUE1UmpKQkRnVCtZS1VqQmFDSzNtRzhyTWFyMDJnRURUMjVXMFdYdDRQSVVvWFRJTlRoWWkxbkZqQUIrNlYwam5TSHMySmJ3c05FZ0FrSHVVVmxVdGRNNnV4N0xVR1E2b0lHMjZsQ3RXOGRpcGlzWlZMUVdHT0FGdEt0L3dBSUk3OHFNVDJ4cmdFRUFIZjBWYlpWdkd6U095QjN3SGNDRnR4VzZJbVVFYTFwRXpLeTNFQjJMQ0ZGV0FHRUQxUm1uZ2FZYzc0ZTYzR1REcGNKRzQ1V3I2QWpTUkRnc2lVeVJVQTVuZ0pFcHJtZ1NRWVBjTHBwSlZ0Y05JOVQyN3AyMXVXTTZ4d2U3dldoekI0YlB6RndncnZqeFBKbHl2Y3M4RXcrME90N1BHcmo4eDRCWHV3NG84dVhLOUV2TGdHdTRHd0EyaGQrM1hoeTN2eWgzYnlyb1cxc1FBak5NMEYyMGNLb2JUb1FQTkErcUFwbzA5enVSMlFBNnVTRG9FQXFCSmM3NXFBVHp1Z3N5ZUVFQUlPNkFpNkJQWkJSTW9KTUt3RVRJaUZSVUZTa1FnY0ZSdGJHeVk3SURBbmdJQklFN2NJTDBnOElLRFIzN2NvRERaRWpoQkN3Z2NJSzBtT0NrRmlrNDd4c3RJYUtmR3hRb2hidUprTjJSbFp0NFFYcFkzaGhSb3R4Y2RtQXRBM1FLZTJxZDRPLzZMT3ZMZS9CVDZWVThENkxVeFl5Q2FMekVOSTlWYk5NQ2JRY094VVZab09JZ0JTckMzVzc0SURkMUdnQzJxZnVGQmZndjQwbEJEUWQyYWdyd1hmdWxVVStpOEQ0VlVVS0x2M1VCaWc0RGNJQ2JUaHdkSEIzVmwwYmFyanVHMTM0blVmUnBsekhpUkMrZnk0YnIzY2ZKcVBLZGhsOHovb0hmb3VGNHR6VHBPVHlxcmh0OXBINER2MFhQK3oxMDcySTdDTVFKbndIUW45bnA5NEE0VGlJTW0zZCtpZjJlbjM4RU1JeFNRZnU3b1QrejArL2l6aFdKa2tDZ1ZxZFBYTzh1eS8yUGlSMzhCMnBYK3oxUHZGbkJzU2MwLzFjNmxxY1ZoOTRVY0Z4WnAzb09BVis3UHZBdXdqRXpzTGN4M0tsNGJUNzdRUmd1Sk5NQ2dZV2Y3UFQ3K1AvMHR0MHI4eSszQU81UjBnWFE0NlhjRGhSVG1NZTVvZ3dRVUdRTlVBRXpISlJ6eVI0Rk9OUEx1VktzSkRDNnFDVm1OVm5VMmNoYlFGV25wMzlWS0Zzb09uVUNzUmF6QzBqUzQ4THBIT3FxZ0V5T1BSWWFLQUFjREd5S3pBVzh4c1ZxQVMxelo5RHVwUURQREQ1SjNIWlRGWWFJZTArR2RLMmxQYTNSVEJjWlVZbnRqVkpEM1ZEd040VmJabEVoNEJIb2dNTklkdXR1S3l3d2QwRXBQanluc3N0eGRWMndqa3FLRTYvS0FqTlpNUDB3N3ZDM0dSazZZYjJXcjZGd0FSUEJXUmVoclh0cVRFYmhXVHl6ZlROdDhPdTc4ancyUTM5NDdiTDE0WXZObGxwNzluZ2xyYUJyNm40bGZ1RHd2Zmh3eXZKbHpXUFJOWGJTSWEzMEM2ZG1ubjc2RU85TjFkNmJrMlkwU0FWdVhhNjBiU29GNW5zcU1qd2FRSTFIY2NMTlNoZFhEU1d0YWlFUGNTZFV4N0lGNnBPL0tCZzRVRlNQUlFHNWlCWTJLQ3p5Z25PeUNoM0NDd0ZZSk1GVUdJZyt5aXhWUFM4N21HOXlvMGZUZlNwbnlqWDgwRERjc0gvQUVZUUQ5NmIrNDM5RUYvZTJqOGpmMFFVYnRvbnlOUVFYbXcwaG8raUNPdkh4c0cvb2dvWGpvOHdiN1FrRkc4ZnlBdElXMitxT25hRUtJWDlVYlNqS25Yci9SQUxyMnA2bzBFWGRTT1VBdXZLa0RkWXRZdEQ5OXJSc1ZxVVVMMnQzSzFidFUrOTFIY2xRUTNOUWZtVXF3dDExVVBEaW8wRDd6WEcrczdJQSsvWEUvRUVBL2ZhL3dDOGdNWDFXT1ZwRmk5cXUybmhBRDcyck1nb0YvZmF6dVhjSUZ1dkt4QkFmdDNXYWxBYjJySWRPdzJudXAyN1NaQ041VWNQaUttdE45eWplVlNJMWNMWGNmZVZSdXEwZkVuY3YzbEM2OHErcW5kRTdTL3ZsYjk4cDNSZTBQM3F0Sjh5ZDZiMHRsM1YvZVR2TzVSdktzN09LZmlPNVgzeXIzY1ZPdzdrTjFVRzh5dFNhUytTM1hkVHV0YmpIYS8vMDl1M0xHNmwrYWZaZ2E0MHhvNFBLeTNDaEhJNVhOdGtVTlJNSGhiakZaSGhtRG9PNVZyS25zSXArYmR3VWJnSmNXdGo0djhBSlZxczZpNGhuOTVFVlZJMDc4cVVLcEZzckVXc2xqWUhtOHk2UnpvS3JRZDJvMkdrUzg2SDkrRUdZMERURUlsVTZBZUlFSWhMbUFQRlJnbjFSWXlLQk03dGdIZFZtbXVmbzIvZ2lUMng2YnBlV3VFQjJ5T2pLcE5EVERVUTF4Y3o0dDFweVhwMU8wenlFQzRMWGFUd05rRGRJMXRqaEFZMUFrRGhUdTBzaDlNeHVRdFRKS0tvMXJtNmlQTCtZclZ4dVRPOUcydGxkWGhEYmFtU3pzNS9ZTHJoeE9XZkpwc1ZsZ05yYXRGUzVQM2lxTjlKN0wzY2VHcks4ZWZMdWFlcVRBREdOMDB3Tm1oZXg1Z05EaVVEVzBqKzZnY3kyamR3QUIzbEFaZFNwRVJ1ZlZBcDF3L2NOMmJLQVd2ZElkS0F0UkpKUGRBSEpoU2lGcGpibFRRRWFta1NnYU4xb1RWK1ZCSENRc2dBTkpRSHlnb0RlVUZseEJnS3dEVWMzUVNUQ2xhang2bUt0cVhqTENnZFZZLzJwSFpvM1daN1crbnF0UFpvZ1J2N2xkR0J0TGtGclF2VWdFN2xCU0N3WUtsRk9NbFFRSUFkUFpBRFE1SUNXaFdwQVBLeUxQQ0JidUVBSGhCRUZnd2dqaklRQzdoQUdxZGtDaTNkQlo1UUUzbEJSZHVnRW1aUUkzM2hCUW1US0NPRW9LY0lRQ0VFZHVnSDh5em9DNXFCVGpHeUNNTW9LUEtzRnQ1VkVlWVZnVFVNaFVmLzFOdWR1WVBDL05Qc3dOU200aVdtUjZMTHBDV0FoNWxjMm1UUzFBOHJjWXA3SE9EcGxXc2ljNE9KbFJ1RlNRN2JrS3RWazA2c1FpTGVDL2o5RktGMG1FdjBueXdzUmF6YWJnQnNGMGpuUXVFQSs2TmxCd0JCamNjSU11bFZscUpWMWZQQmhFSmh6Uy85M2JaRmpKb2t1QTJnbllJelM2aGZVckJyRDhIUHVxazlybHdJMUQ2bzZNb0NXQXMyS0JqUVgrVW56ZXEwNG9XdzhFY29MZkJQRy9kQVRDM2lOeDNRR0pCaVlITWxXWWJPN1ROdGJXdGNuUlFwT2M3OTc4cTlHSEM4K2ZJOSsyd0toU0FkZlBEM2RxSTRCWHI0K0hUejVjcjFScGEwTnBOREtiZGcwYkwwekI1ODg5ckRTUnNOeXV1bm4xZG0wNlR6dWRqNm82R3NwVTZmeEdVRnV1R2o0UkNCVG56dlBPNkFLaDRoQUltTjBCTjIySENCbzNDQUNJT3lDeE1IMVYwSnZCbE5DTU1HRkFXa1RNYm9LYVRPNnlLY2RKSjdJSTF3Y0pBUVR6ZXFDblNBU09WWVBHeFhFSFd0clZxT0lEV2d5cFdvMC9JZCtjVnhuRmIxeGtNYzFsTSswTE05cmZUbzNmNUxvd1kwbUVGd1ZvQnVnZ1FSQllHNmxFZ1NWQkk1UUI3b0tuWklJWWhhQUVEc2dtd1dSUjNRQ1Fnb2dRVUMya2tib0xDQ243SFpCT1J1Z0dBZ0IzS0FOMEJoQUx1NVFDMzM0UUFBUTRnY0lJUnVaUUQyUUFTU1VFSUVCQklDQmY1bGRDRXltZ2x6RHFMandleXhSQnR1QkNBU3JCQnhLb2gzNVZnUTlwT3dWSC8vMWR2OHJocUIyWDVuYjdVZ1cxR3Vsdkh1bzZTRmJhdVo5MW5Tc2ltZTU0Q3NaMGJJTFpIZFU3VkFPZ250Nm8xb0lhZFU5a0t6S1RkVUNOL1ZFUjdYdG1Qb29BWnExNlpseDVVMHRQYTB4STNXbk9tUGtzaVBOM0NOazA2VTFBU2ZLZHdneTJ0QTJQOEFCR0xmS0Yzb05naWxPZnJjWTI5VldtUXp5Tm1ma294VlVLYmhxcUhZbEtrOXJCMVBBN1R1VWIyeVdDSkE3TFdoWkR0bmNFcldveDJtUVNRVHo2TE8wMEVVM0hXL2JuanVzeTNiWGJOSFd0cld1WEFVR09jNDg3Y0wxWWNXM0hMT1JzbGhsdHJmTmZPMU41MERoZTdpNEpJOGZKeldWN2RGbE9renc2SURHTjJBQzlXT01qelpjbHBvcE9mMy9odXVsdTNLK1RSUURScWVkdlZRV2FsTmtCdTVScllIVm5FN2JCRUxjNGtUS0N3UWVlVUFsd241SUlUcTNRRkFpWlFVSTdJR05jZXdRVGNibmhBYlk0VjJLSTgwSnNEcDNsUUVTZXdRRE1tRk5BelRKSHNtaFFweHNKVFFhMms0allHVTBLZFp2Y055QVBudWc4UEc4UHRuMmxabFdYQU5KTGV4MlN4ZHRINlkrRUxuR1cwMmhyR1Z3MW9Ic0ZpZXpicFh1dWlHTjRRU0Q2clM2RkE1bmFZUHpSbTFxV0pkU3NpNFBmVnNOeFBITGEyeENnNHRyVzczUTlwSGI1ckZ5MDFKNGJMYjE2RjNiMHJxMmY0bHRYYUtsR29PSE1jSkJUSExabzVvN3JkaUtQSlUwS0pTd0JQWlFRN2hBSjlGZGlEWk5nZEtndUlFb0FCMVNnaDJsQXRyWVFYQ0FIN2xCWTRRVWdBaVVGSFpCQndyb0M3Y0pvQ1BSTkNodHVtaENKRXBvQVJHMzFUUUF0N3BvVVR0OGxCUU1vQkxZTXE3QXpPNmJFMklXUUQ5dGh3bWdCRUQ1b0s0QVZFRy9Lc0F1R3lvLzliYmRmaHRMQU5sK1pmYmhJWTV6aTVvMlIwaUJyZ29WazBJY05Ea1Evd3dBQUVXSzF0Z3NKZ2psRkxiK0c4ZHdpVm5lSVBLQVAwUkM2am5ScUJRQXd1RGc5RnJKWTR4NVZIT2hxUGVCckhKMktOcmF6VTJlNDRLb3lxSlk1a25rYkZITyt4NkI5Q3FzSWRUREhTTzZObmhvZERKM0NqRlNwcllQTHdsU2V3bnl3Vy9WR21YVDNhRHdmZGFEWEV1aHJOeUZ5eDJseUhTYlV1SGhsSnBlL2dSdVAxWHJ3NDdYRzV2ZnNzdDFIeFV4QjJrY2hqZjgxN3NPQ2FlVExtOHZlb1VLRnI1YlJuaDdRZTVYcnc0NUhseTVhYzJnNDd5UUNaZEs2MmFjdTdZM2VHellDVkZFTGlCRFd3U2dCN25SSk0reUJRY2R5UjhrRmgwZytxQ2dTUkJRVzB5Wi9NRUY2WjNRV0JBUUVBSTNRVUFCd2dOcGhBUjh3UUVHNmQwRmxwMWF1eUN3QWZiNW9MYXd1TVFnWUxSeE13Qjh5Z1lLYmFlN2lOdXlDR3RUQjhyWlFBNjRkRURZZWlCSmVUdTVabzhyRm8rN1ZaNExUL0phK2c1OTBxQU5URzNkL3ZaL2dDdVg4dzZiNkxxR05RV3ROQmZVYlJZNnM4d3hvOFFrOGVRR1VjcS9NYk8rTURNT2NNY3h0NERxbC9lVjZtcDB5TkpJMi9SWnl4YW1UNzY2SVkyTXdkTHN1M2hkTldsYkMycWJ6RHFCMGY1TEVqVXJvQUlBK2E3VXFqdVNWRVZ5cFFFR1ZCVWlZUVU3bEJKQ0NJSWZoaEFwdmw1UVc1QUFRVFVFQUhmaEJjYlFncUNnRkFEaU9FRmpoYUZSSTJRQUFabEJSSUFNb0trRnUzZEFEaUpRVWVQbndsQW5ncklFYkRkQlJJZHdnR0R1Z0dDRUVPNFFVWUkyUUM2QUJLQVNSR3lzQUVuNktqLzE5dllXaHVsM0svTXZ0d0pjNGJqZ2JJNlFMWGlvWVVROXROMU9EK1ZCa2Eyd0MzNm9zS2NXN3VQZEZSbEVWQnFIS0pXVlFib0VPM0tJQ28zeTZ1dzNRWFJiK1k4SXJJQmFPT0ZIT3FlYWRUeStpcllhTXRKOUVHWFRBRFNRam5mYTJueXlxc0puVStld0tOaXB0ZVhQcWpnR0ZHS2ZYcUdHRWpZOHFWSjdSclJVQUxlUFVLeHBsMjFKenpvWTAxSGRoQzZ6RzM2TTNLZm0yREQ4dVZxdjQxNGZDcCtnNVgwY2Vua2VMTGtiRmJXTnZhdEF0YUFZZjNpUDRyMVk4VWVQUGtaVG1SdlZmenl1bnBqMzVMMTAySHliKzVVdVNYRmZqdmVJT3c0VmwyenJSQkdrbmZsVlRBUEtnclVUeHlncDJxREtBUnNDVUViNStOa0JNRzdrREc4SUkwNlNaN29LRWhBWWFndlRDQmdZU05rQjA2VlY4aEE4VVdOanhEK2lDejkzYi9lUUNMZ1QrR0VFZlhjNzR0dmtnVzZDT1VDK05nZ29tVUFuaFNqemNXRTIxWC9BQWxYNkRRT2xZaDJPSC90amx5L21IVEFKRXJxRGFndGFhYXAxUHh1bmw3cDltTEZISFQ0TmxXcDBYY0VWS2pkRGY0bEhLdnp4d2ZBM1lsbHpNZU5sdXFwaE5PM3FBOHo5NHJocCt1NjZhOE9XL0w2cyt5SGpqYm5KdUw0RzUycDJHM2ZqVXhNeFRyanQ3U3ZQOVhmQjlGbllBZWdBL2d1alZRZDBSUTVVb0h1b0FIeElJN2xBS0FrRk9RQWVFRVBDQUIzUUFlVUY4T2hCYUNJRkhsQlJiNWtFOVZvVU82Q3ZWQUJFN0lLaU5rQ25JSTgrVnFVUWJnL0paQUlCT3lDSUtKRUlBbnNncDJ5QUM3aVVFY1FXaFdCYnRncVAvL1EyNXpkSlg1ZDl1TGMxdW1WWFNFUkJscUl5NlRuT1lRVURBUm9nSUtjMGVISitpQ3JkeEQ0Q0RMbDREbzdvRk1lOXdnOFR1Z2JiaHBMZy9oR1RuMDQwaHZ3b0V2RG1HUFZWbzJpWjh2NWtHUzBPRGR6OUZFcTI2ZEpNUnVpRlNIRXRIZE42TzNiSm9NRWVHVmRiVDB1dFRlNGFHTkx5TzdWdWNGeThNL2ZTUFp3bkxsM1dwaXZkandMZjkwY3d2Wng5UDJ2TnljMjIxVzluYVdMV2lpd2EvOEE1bmNyNk9HT28rYm41ck5aVWJURXYzY3VqS1ZMclVQSnNneHFsU28rTjl3c3RSUWwreDNLbTFOQWdRdDRzMExodUhlaTBncDdvTERvS0NqNWlsRkRZa0xJSnZsUU1RVzFCYnZpUUdHbDVBQ0I3S0x5UFFkNVFNRktnM2N2azl3Z0x4YWJCREdCeDkxWUZPcnZuMEhvRlF0ejNPUUxjZ05ya0ZvTEFPKzZBUTZEQktDdTVRQitkQmdZbUp0cXZ5S2xHZ2RMQkg3YS93REd1V1o3SFMyY3JZSU4zUVdxVnd6N1ZlTi9zM3BrTU9EdE5YRnJ1bFNMZlZsTTYvNWdMY2NxNHowZHlrL0dPam5VdTg4T2FsUmxQN3A3dnQyRjUvaUZuRDhWVFAxQi9aR3hyN2pubSt3Unp0RlBFckl0WUQzcVc1MUFMejhrK1ozNC93QUw3VW1TWUd3NDkxMXJFUlJwUjdLd1IzS29FZDBvV1crWlpGL0NnRkJOS0N4c0VDajhSUUQyQ2xFVTBMSEtRUThMUUFPM1FVT1ZvVWRrQzF6QkQ0VnVCWDVsUVNBRDNQb2dBYjdvQUlsQk9FRklGdVFVVHRwOVVDNGpiMFdhSWVFRkhzZ0YzQ0MzbUExV0JkUXlGUi8vMGR4UG1sZmwzMjRTNTdkTVR1cTZRTko0MVFkNFJHWXpjN2JEdUVCYUExcFFScndScEkrU0M2VEMzempmZUlRWlFlWTNHL3FneC9GQWNXaHNTZ2ZUYUMzWUl5Y0hPY0lBaUVGVkE1d251RldnMFJwT3I4M2RCbVJyYVhUdW9sVFNTQnY4d2lGTXA2YmdGbm5CL0lGMHc0KzVuTFB0YkJZNERkWGtWWGp3S1BxZGl2Ymh3UEh5Y3pZcmJEN0hEMi9nTTExTzczRGVWN3VQam1OMjhXZkp0a212RE5iekVkMTM4T1c2eDdLODhmVldJbHNRMzV6eXB0ZHNnUEpjNlRNSWczRXh3Z0VUTXh6dENtVThOUFB4Zk1tQTVlb201eGpFTGV5cHlDRFZxQUU5bzA4cmpqZDFYcFc5elJ1N2VsZFcxUnRXMnJ0RlNqVllaRDJPM0JYcDB6VEpsRVRoQlorSGJsQlRTWlNpeUpLeUdOQUtCaEE3SUtCTVFCdU82Q09leVdnYjFIY0FJSHNxc3BpQVBOM1B1Z28xcWppZHlVQWlaa29ETHAyN2Vpc0ZLaXBRWEFRQ05rREc3b0NnSUFMUk1oQklRVVFKNFFlZmlYOWhVMjdGU2pRZWx3L0R4b3hCKyt2M1daN0hTYWUyNTVXd2NoQlh2MlZLK1J2dGk0MksyTTVkeTgxM2t0cUw3MnUwOFRVY0F5ZmxCVzQ1VjFYN08rV1dXWFJ1Mm9WNlljL0d4Y1hEMmtmRXk0TU1uNUJjK1B6blRsL0RIeXIwNnZLbVJlc21HMDZ4SSs0NG0renVISGJ5dmVhUkI5bG5tbmxyaHZoK2haa2VYOTNaWmwzRzlMQjlWb1RaV0FYY3FpRDJRVVJ2S3lLZ0ZBRGhCMlFTVUVrb0Z2SGNjb0Fnd21oRmRDenNkazBLSk1GUUxIS0Mxb0E0bEJUUks1Z2pzTmx1QkpCbVZRUU8yNkFDZ0VnTkd5QllNbEJjQkFMdGtDM2N3Z0dETW9CZElNcklFdUh3b0tkSUkzUVJ3MlFDWkkzM1ZnV2QxUi8vMHR3YVlCUHF2ekQ3Y0E2a0NOU3p0MGdHTkdyVkVTckVaUTIzSENvc0hXZElKaEFzbUpuYlNnZGJ1ZEJqZnZDREtMZ0EyZS9aQXV1MWhBTFJCUU5wR0FBcWFIcVBiWlJrbDFSNWQ2QUp0TzVrVTVkSEc2Tk1qZW1JbWRYQVZrdFp0a1o5bGd1SVh4QURUU3BIbDd0bDdPUHBwbDlYbHo1KzM2TnB3L0E3RERBMXhhS3R3T1h1NGxmUTR1bW1IMTN0NCtUbnVmMFo5U3RyTy9IWURZQmVpWXlQUGJzQmNUc2VQVmFzM05NNjh2QnpiZk9zTUp1YWpYUVEzUzAvNGx5czAwek1JQnAyTnExMDZoVEJkN3l0U2JSNlRSenZ1ZXlvWVNmVkJiUGpFbnNZQ1ozNVZsZm52OXA2cmZVdXJHTTJ0eGMxWDJ4cDI5U2pSTGo0YldtazJZSEV5dkxoZkszdytnL3NwOVRXNW95MC9KdUoxd2Nad1p2OVhjL1kxTGJzUG1OMTdwNW0zT1piZlE3U0JPL1BIeVdLNmFIcExrbDJ4dnloQkFWVkMzVDllVW9JUUJLeUNZVUJoN1pRRFhyQ2t4N3pzMERiNmNvUEt3VzlmZHNyWHAzcFBlVzIvZllHSi9VSzZIckFreUR6UEthREdnK3FhYXhrdEdBNHhIRWdINnEzSFVjOTJ5MmZScnVQWjR5dmxmVlR4akVHVWJsZzgxRGw0a1NObDQ4dWU0MzArNThQK0U4M1ZjWGZKOWRmUjYyRjRwWjR4aDl0aVZqVUZXMnVtZUpUZTJZaGQ4T1R1ZVA0aDBtWFNkUmVHL1JtZ2QxMWVEaHZkamJuNHYwUkZVNXBtRUJOSkhLQWdaUVc0d2dybmRBQkpsUzBZR0pnaWk4RHUwL3lWc0dnOUx0UXA0MS80MTY1ejJPamdHRjBGeDVTNUJZUGtBN3U0Q1krVEx3K0F2dEQ0dTdNSFdERkxlbWRiYlY5TERhTFBpa2h2dDd1V3JkT1B0OXg1UHdsdVg4czRIZ29HbjduWjBLSkkyZ3RZRHg5VjU4YzdqbGIrYnZ5WXk0UjhOOWVzTmZscnJEaTF6Ulo0Tkt0WG80bFRJOWFrVkhCdjFYcXl3N3B1dlB4M3Q4UHVqTEdKTXh6TG1GWXd4d0l2YldqV01iN3VZSi9pdkpqbDlIb3IwNDVYZVJGdCtLRmtRaVhIMlYySzRLYkZuZE5BRHNtZ0x0OTAwQUJKN0pvV21nSlBJVFFGcGtLaU83SUk0ZDFLQjVsUUFCdWd0WFlXNU5pZ0lVMExKMldnUFpBQlFEM1FVOFNJUUtBaHdRRVRDbXhSRTdwc0tJbHlva2xBRHBkc3BvTGpiM1RRSGZsTkF4dTFOQVhIYUZkQlpiM1FmL1QyM2NOa3I4dyszQWl0NVNGaXVrVlRjWHU5bHFla1pzTmEyUnVGUmJYdEJEbkNPMnlCZFJuSjlVRHJkbWxtM0tESWUwYUFSOFE3S2pIOFlFRUViZ0tVWkFCR2d4c1Z6MlUrQjZMV25PMWp2YjU5UER1ZDB4eHRyZHMwOVRETU12cjBodE5tbW4zZTRRSTlsN3VQaTI4bWZKcHRtSFlGWjJZRlN1ZnZGWDBQQVh2dzRKcDRzK2E3ZW1heEkweEZNY05HMEJkNWhweHVleTVCNzdlaTZSZ1BCbFVORU9hckJwdlVkcjNaY3UzTStKamRmMGFRU3VlUTl2TDkyMjl3ZXd1MmJpclJZQ2ZvdFlqMVd6cW5zb0hRZ0VRSEE5d1ZuUDBSOEtmYTFzMlczVm1qYzFCTk82czZGWXo3TzhPUGxBWExDZVZ5YVpZWCtKOUdlcEZoaXRvNTMzVmdvWFZNY0N2YVhMV3VJOTRrd3ZWYnE2YzhZL1JUQThXc2N3NFZaNDNoenhVczcrbTJ0VGUweTN6aVNQb1ZiRzl2VGE0d1BkYzU3VFFnWk1MU3JMWkc2VVdHQ0ZrQ09VQWhwMWU2RHpNeVhMN1RDYnV1ei9vNk5SNC93Qm4vaWd4c2tQWlV5M2g5UnZtTDZZZHZ4cUpKSzFCc1RmVThubEEwR0UrcldPTzhoUUlnamVRUWZTRnJMMDU5UHlUSEt5dmtYcjlIL0tOZDZ0L3dhSjI5MmdSQytUeWUzK29Qc0wwM0RsMEhkWjlmeWpwSDJkczJPdk1NdThxM2p5YTFpN3hiTUU4MGp5UG9leTdjZDAvRGY4QVVMNFhPUG4vQUxSalBGZHpIQTl4SVh0eDh2NHh6enY3ZTM2VlhzcTBnTHBrb0xPL0NDQ1pRRVFTRUZ4c2dDUk1MTkdIaVExVW5SeEJsZEJvWFRMVHB4bG81KysxUDRMaFB4RG92RFYxRmNzSVFLdXJobGpiWEY3VU1OdHFScW1lTkxHbHgvZ0V3TTM1alkxakYxZjVzdmN3VW4vMXV0ZlZyaW0vU1NTN3hDV0dOK0JDdVRqRzZONitkWUFBMXVPM09nQ0dSVEJBamJhV0xPV1BpVmNzdm8wM05tYWN4NXV2eGl1WnJ1cGQzNXArQ0s5Um1rNlFOaHdCc3V1L2xURDIrNHZzNFk0TWI2V1lXMHZEcTJIdXFXZFhmZjhBRE10bjZFTHc0ZTNveWRhSTNQdHl2WkdRdDJldVFuY2xVVjNsSUxXZ3R5Q2p3Z0FRT1VGa2dTZXlBT1NZUVUwUklLQ25FYklDTzRsU2dCc1NGQUIyS0MwQUVTZ3ZZQmFDM2Jrd2dvY1FVQWtHVUFuYUVFa0VvRnVFR1VvQWxaQkU3SUJFRXl0Q2lBRUFFRGRBb2pzZ0NDZ0w4dnlRQnVnaDkwSC8xTnVrQWF1UXZ6RDdjS2VXVk53STlWaXVrTXBEWUg4dlphaU10b0RtYmNLZ1M0TTVRS3IxRHFrY0ZBK2tYK0dOUE0vd1FaRWVXWjh3NFFKQlpVZEIrSktNK2xwZUJMdmhUREhiRlpkdFkzRjQ3d3JabXA1NGNSNWYxWHJuSFhHMk5pdy9MVnJaQVZzUVBqVnorWHNDdlhqdzZlUExtZTJLakdORk9pME1ZM1lOSFlMM1lZUjVNczZBQWs4ODlsMHZobVRjMlpwMlVubGpMd1dXK2lhMFlYYUJwN28ySmpvVkhtWmdzMjRoaFZ6YnVIOW94emYxQzVaRFVlbG1JVmF1RFZNT3JIOFRENjFTMU04alE3WmJ4SFEycUE5WTlVRmdTWldNdlN4OFhmYlJzU00zNWZ2QnY0OWkraDhnMTVkL21uRjdNazZnWkpmblRvTmxIcUpoclBGeEhCTE0yK0lRSmM2MXBQTEE3M0xTUDRycHpmam43TTRlbXlmWkY2bXRmUnI5TjhXcWtPYURjNFNYTzRiK2FtQ2VUSjRYZWEweDUyK3RBRE9yc1FJL3pYRDZ1djBFTmpLcUxMaWxCTk95eUI3b0orY29QSnpEUys4WWJkVy8vd0F5azl2NnRRYTkwc3V6Vnl2UnQzR1gybFI5SSsybHhXb045YnVCN2JJRGlDRWI0c3Rad1hvdFplbkhQaTNsYkh5YjF2MGY4cXMxUnFvaHRxYWpmVnUwcjVXZnQvcHY3RThYL3dCbzN2MXYvczh1eXVyanBwMU5OeEpGdlNydHFQSEFkYlhVT2I4eERsSjRyMmRSd1kvRlBoZCt0ay9ydmI3QnRicWxkMHFWMWJrRzJyTjhhZ1J2NWFnWDBlSzdqL00zVThkNmJxTStPbWpsZEhsMHQzbTQyVVJiV29MSWpkQlVsQllKaEFKSG1DemtNUy8vQUxGL3lLNkRRK21nZ1l5ZiszVmY1aGNKK0lkQm5aZFFUSVFhUDFqeHYrai9BRXh6SmlRTUZ0cStqVFBmVlhPZ1IrcVlHYjVRK3l4bGUyeC9xRFZ2TCtpMjdzY01zcWxWelhORG0rTFVJcHRrSHZ1U3JrNVI5cGYwWHk3K2JDYk9SLzhBWVlGbksvTEN4eFQ3VUdUc01kMDFkaTJIWWZSdHErRzNOT3MrcGIwMnNjYWRRNkhBeDZRdTNITnhpM1RYUHNkNHo0bG5tSExkVnhkVXBWR1hURzl4NG5sZHQ4MnJ6WlRXVHB4M2NmVVVtSStlL3dCVjFqb3FONVhNUThLZ1VGclFCeUFYY0lBUElRUjNDQWVCS0M0L2lncHplRUZleVVDZVZrQzdsQkVBb0llRm9MN29LUVRhSlFMY2dvY29CcXRMaEFTZ0F3aHBXUkR3Z3BxMEk3aEFEdUVDanlnRW9KMktBRUVjSlFmLzFkd0duUnBYNWQ5dU1kdzBncXVrT3RuTmp6ZlJFWklhOGJnK1gwVkZPZTMwaFFBN1E1cDlSd2d5S0xuYVFPNkIvd0FMU2Z6SU1Od05ONWNRQUQzVXMzTkc5UFh3SzBiaTEwRzAzRVVHZkdma3ZiMHZEWTgzTnk3YjdSRk9oUzhLM3BoaldyN1BiNGZLeXlTbzl6MkEraU9hMlFZSjVRR1EwZVlrTkRkeThtQUIzVExQR1k2clV4NU0vbHdscVVyaWhXYk5CelgwejhMbXUxQXB4WFUzSGY4QXMrV3RjazErNG9kSjNXWnkzTzNmMGVPWVRHMlJZOXl0TnJnRFlLVUtyZmlNYzNuYmhjckJ5N0E2bjdCNmc0amhqanB0Y1ViOTVvKzlUdXUrRjhEcWpYYXFiZEtvcHJZUVBZN2FGcjZOWjN4SHliOXRXekpwNVV4QUQ0WFhGSngvOUlLOG1Iak12NFhSUHN3K0JqdlJXM3dpOXB0cjIzaVhObSttNzRTeDNtY0Q5SEwzZFJkMmZzODNENnI1VnozbHZHT2lQVlBUaHhmU3AybGR0OWhGeHcxOXM5eGNCUHFOd1Y1WjdlbXZ2dklHY3JIUG1WY096TFlPYVdYbEllTlNITk9zM1o0UHpPNjlIMGNaN2JQRUhTczEwV1JHNjVpdFlHMEtnbWJqeklJQTB1MUlFWDFOdFNrUkhNL3hDRG5QVGgvM0hHc2Z3T3BzR1Z6V1lQOEF2SVVIVUloeGc3THBBY3h5Vm5MM0dvc08zVnk5TTRmM2RmSlBYb2x2VWk0cU4rSnRHZzRmUUJmTi9tZjZaK3d1UGQ4RXMvemYvd0NXejljOHJCK0NaZHpoYXRKbTFvMmQrNGVoWTExTjUrVWtKeTQ3ajRuMk8rTFRpNStYZzM1M08zL2x2ZlFqTm43ZHlzTUl1S2dkaUdGTzhQZmwxTHNWMjZXNmo4ZDl2L2cxNlhubk5KOHVWMzlmYnE2OSszOHhuTDNSUHpLTTR6c3V4ckxlOXB2MzRRU0EwNmh3Z2lDTE9Rd3I0YXFMeDZncm9ORjZiRDhQRjNmOXVyRDlDRnduNGh2MzVWMUVac1FFSHhOOW9YcXhtekVzZng3cDVXcVVxZUFXZDJ4dEpqVytkN1F3TzNQeksxY1hMYlNPanZVYk1lUk1lWlo0RjRUYWVOWEZ0UnU2MVZrdUxmRURTQitxNTNFMi9SRnU3V24xYUNaNWtpVnZIdzY3OFBscjdVL1VYTVdEWHY4QVFXMGRTYmdPTVdJZmRoN05UNTF4NWZUaGRKWERLUG5qSS9VRE1uVHpFSzJMWmFyQ2xjMTZScDF2RWJNdEJuZGM4L2JYSE5SK2gyU2NXdThkeWpnMk0zNUJ2TDIxcDE2K25adXQ0a3dGejA2dmY3TFVnbzhLZ1VGSUtjZ1dnQS9FZ2orVUVRUnZLQ3o4U0FUOFNBVHlnQi9kQVBKYnFRUnlDa0ZFUVpRQTR5VUZmbEtBZXlCWVFXL2hLSWVGa0FnblphQzBGRjJ5QUJ1VUFubEJTQWU2Q1JLRC85YmJRNXVqamYxWDVkOXVGRUYyMHF1a0Uxa1FQUkVaalhFTkJMdm9xQ2MwYk81Q2dRWEFPMkV5Z3pLUWRHcUlBUVZXcXRqY3dnMTdHTVhGbGJPZHExUEpodjFYVGltODVQMVl6dXNhNlBreXlkWjROYlBKMVZhN05kVng1bDI0WDZMSGptTDR1ZWUyekRkdSs4OHJwdHhrMm1xSWIyOUZ5VVEzNEVJT04vYUF6Qm1MQXNOdytsaE53NjJ0TDF6NlZ6VllZZHhzRitjK0k4dWVISmJQV245Tyt3ZlE4UFZkVFp5U1g5Mm1kRE9wZFREY1ZPV01idURVdGIxMzlVcjFYVG9xa1J2UHF2WjhPNnVaelZmYysxdjJidVBKdmhuajlJK25UdzNlWUVFK3Z1UFlyN1dlTW5tZlYvRnM4Ymh5WlkzM0ZnQllRUGRTaWNjY3BJT1c5UktJd3ZGc0d6SFNFZmRyalJWajl4NWlQbHVzVzZvNmJZMW1WcmR0V244RHcxelQ3RmRoa2FaUEtBbWd4QVZ4VE8rSHpiOXNlek5YSkdDWFJFK0RmbHBkNkNveUkrc0x5WHhrNnp6aXl2c2UzbmlkTjhUdEh1OGxyaURpOGtob2ExeldtWks5V2VXM200M21mYXp2TWc0MWwyZ2YycFFkbS9EcXV1Mm9XN2c5MVNrL1o3U1J3T0RDNVIzclNQc245VFJnR1AxTWxZblgwWVppN2c2eTFueTBybGhnTjl0WVhvK2pqUGI3ZDNreklNbVBYM24yOUZtdWdpNG5qYU9WekFra2xVRzMrOXVnamdHb0FmSnB1K1VoQnk5amhoUFU5dStsbUoyKzQ3YTI4S0RxYmVBU1pQYys2NlFPRVJ3czVlNHNUZ2s5bGN2U1lmM2RmSlBYMGx2VU82UHJiMFkvMlY4dkw4VC9BRTk5Z0xyNE5mOEErMy9aOUczR1hhR2FlbjFIQTdrRFRkWWJSWlNMdnkxVFNhV24vYWhlekxIZUwrRWROMXVmUi9GTG52eHg1ZjZ2bXpwamoxZkllZTJXMTVOTzNkVmRZNGpUZHRvMG5ScStjd2Zrdkh4NWF5ZjN2N1RkUGg4VytHOTJIbXlkMzUvUjlmaDB0MU5nc09uUzRkMlBFZ3I2RXlmNWU2amd2RGt0ZG92TE40YkVveFBTYXVBVkZFNzROa0F0NFFRck5HTGZmMkxpT1lYUWFOMDJiRkxHZlFZaFdqK0M0VDhRM3o4cTZpRWJiY3FqVThUNlc5UGNYdmErSjRwbDIwdThRdWpxdUxtcXdsNzNiQ1NaOWx1MWpzSnRla3ZUYXpyMDdtaGxpeXAxNlRnK2xVYlRNdGMwNmdSdjJJbFl1UjJOMEFBQUE0R3dsWTIwOERIY2paUXpMZE52c2V3aTJ2N3hqUERaVnJzMXVhem5TRFBDNlNwWThrZEl1bW5tbkxGZ1E3NHZ3anYvQUJWcXlhYmRaMmRuaDlyU3NyS2syaGFVR2luU29zRU5ZeHV3QUN6cFRUNkJCRHdvQmRzUWdFODdJSWdGQlVEZlpBdDNLQ2p3Z3NjSUJKTW9JZ29vQUlNb0tjRUFvS2dvTFB3bEJqa25WeWdJVENDaWdvQWVpQ2lsRWQ4S3lFdEpKM1FXN1phQU9RQVNFRmJSc2dFL0RQZVVGRGhBdHhqZEJXc25oQi8vMTlvcGpXT1lYNWQ5M1MzTUlJQVZVNm0wQWVZN2hBOXBZUko0UVdUcUJIQTdJRWZDN2JkQm1VNncwUjNRWWR5V2hyak03Y0lPVjUweFYxR3JRWVRwYWF6WjloSzdkUDhBM21QN3h5NWZ3WDluMHhncFljTnRqVElnMHFSQTdFRm9YNmpPUGc3MjlSaEVRVjViV3BkTE1kbFVNWVFSeENEay93Qm9UQ2pmNURmZE4zZllYRk9xSERrQTdGZkUrS2VNZlQrZ2ZZam11SFg0emV1N3crVzdld3VYWWJVeDIwblJaMVdzdUh0K0ptb0RTNFI3cjRuSE11TEh1eGYzenFPcDQvdjV3NXlXWlQzZmI2dTZNOVE2ZWI4SEdGWDFRZnQzRGcyaThFLzJsTURad24yNVg2enBlZjczQ1hMMi93QS8vYTM0RC9ZdXV5dU50d3o4L1IxSFZ0L0wzOTE2dWI1SjQ4dndXY3k3dFl6Y1FBR1NWTVBtdzNXc3RTK0Y2U1JMVnJhTlI2Z1laKzFNQnU2TFcrZHJkVEkzT3BvbGNzc2QrUlhUckdCaTJXYkluYXJUYWFOWDFENlpnaGRNYnNiYTBQRHY3dmNyVjhCNDlXN2p1bURObTNEUHRhV3YzanBMV3JORXV0NzIzcUQyRWtGZVBQOEFFN1MrTlBpM0wyWXM1V2RwWHk3bGk3dTJVTCtvMTF4YVdnY1RVUEg1ZUpYdDdkeHkxTVhUY21mWm02bVp3Y0wzRktQN0hzNm5tRmEvSmZjSGZmYnQ5Vmk0Nk81NFhWYnBmaTNSak5WaFN0N2g5elpWbXN1OE94RURTWFZtSGNTTzdUd3RUSm1QdDNveDFBb2RSOGkyR00wNmdmaU5GZ3RzU2J4b3EwOXZOUHF0Mk5iZENjV3UzYVlIcEJrcmhmWW9iTFNqRzZDU3JvUjN3bUFtaHkzcUhUTmhqV0JZdzBhU3k2WlRjNys2OHdaV1IwNmlROWpYZytWMjgvTmJsRHdSdEc2WHpXb01tV2tMV1U4T2VOL2gxOGw5ZmpQVVN1M2IvbTFIK1VmNUw1V2NuYy8weDloT1NZL0JiY3ZFM2wvMmZUMkIzdHBiWmJ3cXRkMW1VS1JzcmY0M2dHZkNhRjZweXl6VCtEZGYwM0p6ZFZsOTFOMlgvWDkzekQxdkdBdnpnL0U4QnZhVmQxODBWYnhsQ0laWFo1Wit2ZGVQTEdTN2Y2Qyt4SEYxWEowV2ZEMU9IYk5XUzczZkx2dlNYTmJjMFpMczZ6M2gxOVpBVzl5MlpKTGRoSzlYSGx0L0ZmdGQ4S3ZTZFRjUHBHOXQzWHIzcCtLbHkzMjJmTCthMFdxSTNSQkF5SUtBV2tneDJRRVFWTEJpM2ttaS93Q1N0bzBqcHdZWmpMZlhFS3k1ejJOOERaRUxvS1BNSUxJQlN0YlZIdVZudE5wcUE4cElEak9nZXNCU3pUbnZ5azkreFZsYmdnVjEzb3FuRlRhQjQzVXRGZDFCSHRtRUE4YklLUVVSQkk5RUF6L0ZBRHZWQUkzQ0NBN0tVU0UyQlRZdHUrNVZBa29Ga2tvSWRrRjhCQURuYkZBcU82Q2k1QkR3Z3FZRW9CSlFRbVJDbWdHblNVMEJjZHBWQ3laUUFRZ3FZRUlKMmhCVUNFQU9BVkZRQVUwUC85RGFuRFFmTHd2ekQ3d0hWU1hCQmtVM0J4QWR4M1FPYTF1b2o4dktDd0dPSkFNUnNnVjRZTlNBVEtCK2h6UnBqM2xCNXQ4WEJwRXdEM1FjYjZqMGFnMDEya2xyVHErbzNYWGh1czhiK3JueVRlTi9aOUNkS013TXgvS2xoY1BNMWFiUlNxajAwaUF2MHN5MitIbE5PZ2dOaVJ3dWVVWm5sR21UQ3lEQjBrZ3FqVytvV0YvdGpKV05XVFJKZGJQY0FmMzJDUXZsL0ZNTzdoZmIrQWRSZUxyK1A5NCtidWlkdFpZcml1SzVSeEpvTnRqRm01Z25zOWhNRWU0WGsrSGNjNWVPeSs0L3RuMnd1ZlN6aTU4ZnlrLzMyOEJ6TWU2VFo0SllYVTZ0aFVrVHhWdHllL3JJWGg0K1c4WExjWDFPemkrTzlEM1Q4V00vYi8xZllPVjh5V1diTUV0OGFzSEIxT3UwRjdCelNmM1pDL1djT2M1TVgrZS9pbkJlaDVieDJQYUFpV3VIejl2bXJmRjArTmo0bm42clpJQjlDalRGdktBcVVIc0luVURQNktEbTNUeXMzQ014WTVnTlg0S1ZZVjZETzBWUnYvRlRqSFZHRW5iMVhUTDBDblFJSEhkTUVjdCswZGFDNTZPWmlJYnE4Q20yc0IzOGpoSkg2cnlaZmlkSTRYOWk4V2RYTU9ZN2U1b1U2dHg5M3AxS0xudERuTTg1a2drR0Y5UENmSzgvSmZMN0xMZzdZazZkV3poKzZEdFBydXZQeWVHOEh6WDlyN01XQVU4dDJPV01Sc0tsYkdydHd2Y012VzdNdHpUZER3SGVwOUY1SmZMZVU4UGxETGViYzM0TlR1Y055eGZYTnQrMFhBMXFGcHFjWHZCMk1OSEpYdHdZZnBEMDR4UEVjV3lQZ3Qxak5DcmJZbzYwb3N1cVZmYW9hbElSSkhvZlZNb3JadUN1YWphNElMZ3JRSVFXa0lPZjlVN0kxOHUxNnJCTlcwaTRaN0ZoQzUzMk5teTlmaSt3V3l1Z1pGU2t3aysra0xjSHN0QTdLdFF6dEMzbDZjc2Y3dXZrenIrQzNxSFhjUlA5Vm9rSDAzTzYrUHl6ZGY2aC93Q25YLzhBRlQ5MnQyV0hkUTgyQ25SdGFGL2Uyck5MS1picWJSYTBjYitpMWp4V3ZxOVYxdnczb2VTNVdlZjh1TmJkYmZaOHp5K3lyWHQ1VXQ3VjdhTHFqYUxYYTZqM044d2FUMmxaeTRMSS9QZFI5djhBb2NPYkRIQ1plY3BQd2EvNVRvVm1pcmx2TnI4RXZ6NFZuaWpoUmV5cHRvcmpoT1BMVFgyMStINDlaMGQ2bkQzSnQ5WE5HNUhlWWo1aVF2cFlaYmovQURWOTdieFdmVVEzNFd6SDBzTjdJcWFTSFQyUUNSSUpDQmpSSTkwR0xlSFRUZU84TE5HajlPMmdOeGs5L3dCb1ZsSjdHK045UFJiQVRKUVdka0U1NDlDZm9PNkRqSFZmcXpReVpuN0p1Q0dxRzI5V3Y0bUxIc3loV0hodEo5OTVoV3p3eDlYWm5PRC9BRHRIbGVmTEc0MzNFZXk0dWtXQ0YxM3NxaVVSUlFWQ0FqMlFDUktBRHNnbzdrKzZBWUtBSERzZ0hnYm9JTmxLSklVQThwQmJSc1ZvS0pnN29MMlFDZDBBazlrQXVPeUFRZGtBRWJoQlpPd1FWQmhBSkNBWmhCVGozUUNkeENCU0FUeWdqaENDZ0o0UVVmUkFzbUR1ckJOUUpWSC8wZHFPNi9NUHZFNm1ha0dUU2FUQi9KMlFQaUQ1ZUVGT1k3dHRLZ1ZUcXViVTB4SkNETU5Sem16RzZvdzcybnJwN2dSRzhJT2I1Mnd2eDdKeGFOVUE3SzQzVmlaZWhkQTh5bXd4bTd5MWRQMDA2NW1nQ2RwOUF2djhHVzN4ZWVXUHB1azZRdlZuSG53Rm9JTXJrbzlRRzNkQXU1cGZlYmF2UWNKcDFhYjZSSCtNUXZQbmozN3h2clQwOFYrNnl3NUo3bGZIR1VMcCtXT3FkczE1MHR0Nzk5cy90Rk43eTFmQStIY25aMUZ4bnE3ZjZKK0o1VDRqOEZtWDE3Wi8yZlFmV25wMk00NEU3RWJGamYyNVlOTDZWWURlclQ1SVB5QVgwZXY2YVRMdW4xZnlyN0svSGMvaC9MZU8vaHoxL3dEUExpdlJqUDhBY1pJeG80UmlqaTNCNzJwNGRlbTZacFZaNTM0WG42RG5zejFmRCtrZmFqN1A4Zlc5Ti9hc1BOazM0djhBeEgxMVRlSzFGbGFtN1ZUcURYVGUzY1BZZU4xOTdLeTNiL1ArV0Y4N21yQmpqWkhHZVVjMXpodXBTWGJrdVlHSEFjLzRaaTQyb1gwMnRZY0RVN1lFcWNhdXFVWGVTZlRaZE1nNHlXQXBnamtuMmg4NzVad0RJR01aYnhlNGMzRk1lczMwc1B0bWlkWkJCSm50d3ZKbFBtZEkrSXNpZFJzeWRPcjI4eExMVlJsdGZYdEEyemkvekFBa0gvSmZRbDFIUE9icjlGdW1tWjM1eHlOZ1dZYS8vT2JxMllia0Z1bjhWa2gyeDlWeXk4c3p3OHJxYjBteTkxVHBZWFJ4MnBWb3R3eXM2dHJvSFM1N0hpTkRqeUI4bHptSGxkN2VwbExwamtiSmRPbFJ5OWcxdmJ2YUkrOWFBK3MrZVNTNytaWGVlSU52Z0JqZGdIQ1dnRG1QZGNiZktxQWxWUnRDQ0E3bGFCTitGQjQyWmJOdDVoVjFRSStPbTVycC92QWhZbzF2cFplZVBseGxuVWsxTE9wVXQzZXNzUDhBd1ZnMzRSSUxlQ3RmVnFHUnNGdkwwNDQyZmQxOG1mYUQwLzhBS0ZWRHgvOEFDMG9COVJLK1hsZFpQOU4vWURIRFA0UFpsNjgvOW4wOWxMUXpMT0Roc1UyaTBvL0MwTkpjNm1EMlhzNDg0L2dmeFNYSHJjOHJ2VXllcStyU29VYWxhcEFiVEJlOTdwZDVXaVRzdW1kbGxmS3c1YjFQTEpyV3ZXM3hobjIvd2htZTc3RmNyWEpxWWUrcTI1WSttTk9pcVhTUTJZN3I1ZVUxNmY2cit6L0RuMVB3NmNQSkxQSDdPN2ROT3J0NW5QRzJZRGYwVzBTMjFENmRZUnFlNm50djgxMXd5citUL2F2N01ZZEJ4WlpZMlgrcnJ3Y1NaaUo3TDZFZnlaWU82QXBrSUtIQlFXMHhDREd2aCtHNHJOR2tkUFBneGovekN0L2twUFkzc2NuNUxZb04zUVU3Y1NPQnlVSGw1ang3RHNzNEhlNC9pOVR3TVBzYVJxVjNUQkladTFnbmt1S28vTjdQR2JyL0FEem1yRWN5NGlTYWw3V0xxVkdkcWRHZkswZjRSd3VsbnlzZlY5bC9aeTZtc3p0bEJ1QTRqWDE1aHdScmFiM3VQbXEwVzdNZkhKMjVYanJjZG9CR285dDkvbXV1TjhMVTdyU0lnaFFSM0NBU1NnVzRsQkVFUUNlVUFPUVVWS0JLZ3BXQ3dmS1ZRc2lVRVFDZ0ZBQjNKUVNJUVVSM1FBU2dJblpBc2tvRmttVUJQK0ZBQ0FFQUg0a0JPR3lBQ2RJbEFJTzBsQXQ2c0FqbFVmL1MyalZEaTFmbUg2QWxoODVCSGRCbjA0MEdPRVpwcGNXdEJIS29FbDUzUEM1NUFSU2wydHYxVmd5UXp5enlmUmFDS3BCQmJIS0R3TVh3OFhGQjdJblk3S1VjVHUzWFdVczAydUtVUEsyalhsM3lKM1gwdWl5MDhYUE52c1BBTVdwWXZoVnRpRnVROXRhbTEwcjdPWHpSOGpLUGIxdGNQZGNsVitjUWltTm5ua3lJSHNOeXMzS3lWcVpkc3RuNVBqVHFsWUhBK3FGL1hIa0J1R1hUUGRyb2Qva3Z5M1N5WWRSNS9OL3BEN1A4ZVBVL0NNYjllM1grejY2czhXczNZSFo0cmVWNmRLMnFVS2RWMVI3Z0I1bURuMWxmcE9hNFl5MmZWL0E4T2o1K1RseXc3YnZDL0w0ZkkzVjI0eTFkNXdyM09WSzRmUXE3M2hhUElLLzkzMlg0L2w2bkx2ci9BRUQ5a3NPcnZEZUxubGsxOVpwMlBvUDFLZGl0a2NwWXZYLzFsWmorcVZIYmEyQWNENUw5VjB1ZU9mRityK2EvYkw0RHlkTHpYUGpueS9wSGNHeTArYjZnZnZMMzRUdy9tL1Q0WXlYdTlpSTFLWll2SjAwL2laZjFjMjZxMkpxWU4rMEtBSnVMQ3EyNGIvOEE0MWlPemJzdVg3TVR3aXp2MnUxTXIwbTFUL2lnVC9GYmc5eHZDMWZRNUwxdjZOanEzYVlSU3Q3eHRqZVlaV2RVcVZ5M1c0Mjd4cExBQnVKSzU0akV5UDhBWms2YzVSZlRxWDFxN0hNUm9nRjFTN2pRREhadm9GNk51ZVh0Mk8ydExleW9VN2UwcE1vV3pOcWRPazBNcGdEc0FGaTFaRDRtRml0aFBrZENnYURNRkFTQ0lJNHQ4cUJtL3FneHIxZ2ZUY3h3bHJtNlNmbXJmUTV6MC9xT3NjeDVnd2x4MGh0VVhOTnYrUFlybkIxQnBFUVB5K1g2aGRMUE1XVFY3dnlJdjdwbGpZM0Y1VkJmVG9VM1ZITWJ5NE1FbUZyTExVZE9sNlhIbjVwaC9pZkdQVTNOMXJuYk5OWEdMS2k2amJtaXlpR1ZENXZLU2VWOHZLOTJUL1dIMmIrR2Nmdy9vNXhibm56L0FLdXZkQ3VvT09ZOWpGVEFzWHJHcFlVTE5yYk5yV0VCdFNrWTNkMzJYYkNQNWQ5dGZoWFM4ZStUaXM3L0FOUGJ2VlZncTAzMHlKWTlwYTRIaUNJWHFzK1d2NUIwSEpsZVNaY3Z2QzdtM0NMSDdObHUrNXJWY1l4WjdiYlU2clRwV3JOTHRMbkU2ZFQ1SGRlWERqMC9yZkovMUM2anArSFdOdS82ZitqcGVWZW1lVWNvM05POHdxem5FMk44TVhOVjU4VUQ1TDBUQitKK0xmYTNxdmlYRmNjcmY5bTViOStlNjZ2eWszcnlwRkVPRUVRUW9GM2Y5a2ZrczBhUDA5NHhqL3pDdC9rcFBZM2tjTFlnRWtEaWU2QVljNGpZUkozSmhzTS9NNVZMWHhYOXBucTkvU2ZGdjZHWUhYSnkvaE5RbTlld3lMbTgvTXozYTBjTGNZdGZPNWNPeTFXZHRueUZuWEZjaDVrczh4WVJVSXFVWEJsYWorV3BUTzdtbjJoZWZLT2tyOUZNbTVyd3JPMlhyVE1tQzFXMXJXN1lIT3BrK2VuVUd6bXUrUldKR252a0VlWGtSUDZyY0MrNjBMZHlnTThJQWR5Z1c1QkVGamdvQTdsQXZ1VUZ4SVFBNEVjSUJCTWJvTFFVT1NnRWNvS2NnV2dwQlJRVWZoS0FFRmRpZ2g0Q0JaNVFSQURuSUIxSUJJYnM0cklqdmJoQlQ0amZsV0JPL2Y2S2liZDBGQU5sQi8vVDJZL0RxL042cjh3L1FBWStIQU83OTBHY3hwMGpUdTFHYU54SkhwQ29CeHFhQ0pKWFBJSFJjNXVrTzc3RldCMjlOMGRqdXRCZGNnaVJ4M1FZbFJyU3dtZm1wUnl6cU5namJpbFVyMDI3OC9WZHVEUFZjczhkeHZYUUROQnZjRWZnZDA2Ym14Y0dOQk8ra2VpL1JjRjdvK1B6WTZkdFl3bDB6dDZLWDI0bjdDQ29wb2MwZVlHRENaNjdHc0p1M2I1YSswbllPb1pxc01RYVBMZDIrZ3Y5U3h4MlAwWDVMcjhMeDViais0L1lMcmN1VHA4K1A2WTVhLzJhRlN4bk8rYnFOcGwrMnFYTi9iMEFLRExXa1NHUnNCSkhBaFdYbDVNY2ZlbjZ6bTZMb2VrNWZ2TXUyV2ZvNnprNzdPTldzMmxmWnV1UlFacDNzN2M3dEo0MU83cjZ1SHczSHMzbDdmalBpLzI4NCtIbG1QREpxZmxiL3dDam0rYk12NDEwc3pxRGJ2YzBVcW5qV0Z5Q1lmU21kSlB5NVh5OGVYTGg1ZGZSKzI2THErRDR6MHVzdFhLejkzMGhsdnJMbExGTUh3Mjd2YjF0SEVydDdMZXZhZm1GYzdBL0pmZTRlcG1UK0dmRS9zejFQSDFPV1dNdmJ2OEFULzFkTTh3K1FJQkkvZElrRmZRM3VQeXVlSDNlZG4xZUxtS3laZllkZFd0UUFpdlRjSVhLT1RUT2sxOC85bFhHQ1ZUTlhEYXpyZUhjNkNTUXR3ZEphNmU2c3U2SE5sdzBIZHExWm9FR2hoV1pVMGt3NE9PNEpnKzMwVXRha2F6bWpxUmtuSkpwdHpIaTlDenVITmxsdTQ2bmtUOFJBNCtTeEZwbVhNOVpSem93Vk10NHRSdml6NDJNTVAzOWo2TFRMWmFmd2xwMklNYjhvQ2U3UVBWQlRYNnV5QzNBRXdBZ01iN2ZSQXF2eHBKMjdxMzBPWkgvQUZSMU1vMUk4bUoyem1IMEw2ZTRsYzRPb1VuRXRFY256RWU1WGFyYjhsZ3E5TnRhaFZvVlRwcFZtT3BQaHVxV3ZFRmNzcHRPa3p6NHViSGsrbU4yNWhoUFFUSXVIVlhWTHVsV3Y2cGNYYWF4MVUyZ21ZMDdjTGpqeGF1MzlHNnY3Yjgwa3l3eTlUdDkvd0RzNkpobUI0TGd0UHdNTHNhTm14b2hncE1EVHZ6dXU4eGZpdW8rTWRSMUdmZG5iWis3MG1raHE2YWVEa3puSlpjZkdsdGZCa2NyT21NTXNlYlB0eVhQTzZ1M1RlSEhuMnlRUTRDTTMyRXlEN0lncDM5a0JkMEFrK2FFQ3JwMzRaV2FOTDZmY1l1Zi93QzRWbEo3RzhUdEMyS0JnSU9NL2FQNmtQeUhrejdsaDFiVGplTzZyYTNMVEQyVWlCcmNQU0pSenRmQmIzdTFheTR1Zk1seDNKUE0vcXVrYzdTNWIyQ3FiV0R5T3g1V2RPa3J2UDJaT3BkWEttYTJaWHY2aC9ZV09POE5vY2ZMU3VvaHNEdHFHeFhMS2FycEgyK1pqYjRCc1BWSXFDRm9SeDJRV0R0dWdHcDZoQXY1N29Ma0lCbmxCUjRRS2NndHBLQ25JQmJ3Z3M4SUJRQ2dGQU1jb0tBNVFVUWdEc2dFY29JNkFnRW1SQ0FJTWhCSElGd0NVRk9BSENCZFFFZ1FzMFR0Q0NuQWtxd1VlRlFweUNoeWcvL1UyV2ZMRHRsK1lmb0ZVNlluY3pQQ0RNb3VMZkwvQUFSazEwZ3pHeUFDZHBDeGtJMHVQSS9SV0I3aklBN2piM1doVHFjdElISlFJYTBIeWFlT1pVSGg1aXc1dHpaVkc2WmtMVWtua2szSEtNazRxL0p2VUNpYXhOTzB1M2VFOGR2TndWOW5vZWZkMCtYMUdENjl0S3dyMG1QYWRuTjFBcjZHZU9uejhidXN1QVdDRHZDNGJkcmlqWUlMVHlSQ3R3dVRQTHFjZnkvaWNPKzB0aFBpWmZ3M0ZRSk5yWDhNbjJxQ0pYd1BpV1Z5c3hzOVA2ci9BTk9PWExpeDVNTFBPVjJaOW1rMk5mTGQvVEZCbjdRbzNYbnF4NXl3dDIvUmZWNlRMSDd1WTZuaDRmdDNuMVBGMUdIZGJqam52MWZ5ZDBCaHpkeEpNVEhsK1JDOWZKK0h3L21QTnljZDFqdmQvTjgxL2FSeGEvZmk5aGdWelp0WllVMmVKYTNUR2sxSHVQSVg1ZnFKY3JyVCs5L1lyaTRlRGltV1dlVFVzaTlIczRacHEwYno3djhBczNER2xyMlgxY2FYdWd6NVc4Zys1WHQ2VHBmTzkxOXI0NTlvK0hpd3VHT0dHWDYyZVgyRGh0RjluWlc5blhyR3ZVb1VtMDZsYjk0dEVTdnVTYWY1MjZuazd1YTV6Nmlyc0Qya0hlUVIrcW1VMDQ2Y213b093RHFiZVdlclRiNHJTOFdtMDhhMitpa3lOT3NVOXdEeFBaZE5hUStTM2dwYUxCTVJNOTFpTEhoWjJ6S3pLT1VjWXpJK05XRzJ0U3RRQjQ4YUlaLytSQ3pWZm1UajJQNG5tZkZyckhNWHJ2dUwyN3FPcTFQRmNTQnEzQUFQQVREelZ2bGw1VHpWak9Uc2J0c2N3aTVxVUt0Q29IMUdOY2RENllQbWFXOGJydk1JNDVYVDlNc29aaW81c3l6aE9ZNk96TVF0bVZ0K0JVMGpVQjlaV0xqcFpkdmNKSmlkMWhvZXdFaEJRUGRhMEdUSWs3Sm9BNW9lellTUVZSelBxRURZNHJnbU1OOG90cnBqWE85bmJGYzlEcEZwVUZTaTJvMDd1MytoNFc1ZGs4WGJJMUVnZzl4Q3NkdnZma3VPcDVISk02ZkxKaysrMExlOStIajRlQ1k0OW40dDNmbFpFTkJEWW45Vnp1V25xeG1jejdjc1oyL21JRnN4TWlPd1NaYmNaTXZ2TDJUeFBLVmF0RzJwT3IxM3RwMEdDWFZua05iL0FCVXp1bmJwK0RrNWVieFBOZVRaNXB5NWlGd2JXeHhPM3IxNWdNYlVhQ2YxWEh2cjZYVi9CdXE0ZjR2YnY5NjlxT3c1N2R4K3E3NDNiNDNQYytQS1N6MkhVSEdPM1lyZGp0bmpNWktnbWR1RmxNcHFqY1MyRVpDUkxsWUVYVzFOMzZxV0RVY2dNQXA0dnZ6aUZiL0pZbnNib0JCaGJFSjNRZkQvQU5yYkU2MTMxSm8yTlEvMWV3c0tiS1k3TmU5NUpjUDhYSDBXOU9MZ0xoRWhWblFReHp0bTdFblMyZTU1UDhGbVplVyt5TDkyL0NlSjlGMjB6ZkRMdys2cVdONVF2YUpJcjI5U25XcE9ieTE5TjJvRmVmUDI2WVh3L1QvQWJwMS9nbUhYci9pdUxhaFZNOHkrbTF4UDFKV1cyZVFyS0tPd2xVWE95Q25HQWdXNDdTZ25aQU1vS2M0K2lCWjM1UVdEQ0NuR1VGTVFFUUlRQjZvQlFDZ2lDY0lGdU1vQjRDQUozUVU3ZEFPdzNRUXpzUndnRjNLQlBpTjFSTzZDenVDZ0dkbE5Db1RRc1FBcUZ1UUtkUFpVU08vZE5ELy8xZGpkNWhDL01QMEFxVFE4amN5TmtHWTBSdjhBbUNNcjhWNUpiR3czSlFLZTUybVFObGpJUHRuR09GWUdsb2E3VVRDMEpxMUVhVCtxQTVhOXU0aU9TTzZsUyttSmVVTlZCdzVsVGZoSlhEK291RnZvMVdZaFJINHJIYTU5QzNoZGVsNU8ydUhKaHQ5QTlLY3lITWVWTEc3YzZiaW0zd3FvOTJpRitrN3R4OGp0MVhRbUVua1I3S2Rwc3lBSFNyeDhtczlKaGhySzVWekRyemM0VS9JRjlaM3RlblR2WEZycldtNHc1emdld1h3Zml1Yzd2RDl0OWtmdnMrcStUMCtiY3BaL3pEa3kydm1ZSlZGTDcxcEx5UnY1RDIvVmVIcE9hK245MStMZkMrSHJ1RCtONHZIdlhxZTMyYmt6R0RtTExlSFl0VllXVmExQnZpTmNJTG5nYm1QbXYwM0ZlNmVYK1pmaW53N2o0K2V6Ry9YODJUaU9Yc0Z4ZTZ0YnZGYktuZDNGbUQ0RDZ2bURKOWxNK25sOG5IOFM1ZUtmZDR2V1lHdFpwWTBOYVBLQU5vYjlGMTQrUHRlRHFPcDVlUythcHdMZzBua2NnYkJYSnl3eHYxRTRlVlcrWFJ5VHFkUWRodUo0Um1HaElmYTNBcHZkL3dEYmNWeTlLNmZZWE5PNXM2Tnd3eTJvd0VldkM3ejB3eXpKQUk0UEN4UVFCa0JaYWpTdXNtQjNXWWVtT1k4S3NtRjkxVXR2RUFIT21rUTgvd0FsbWo4MElxTlA0Z2lvMGxyNTJJSU1RUjZwajdVVEhPZTl0TmdMcXJqcFkwQ1NYY0FmVmVxVnd6OXYwS3lObW5LM1MzcHZsbkJjNDR2UnNjUUZxMnE2MXF1L0ZZNnI1dzNTUFlybmt1TG91V3M0Wll6alFkWHkxaWRERVdVLzdWdEYwdVo4d1lLenB0Nytsc0QzL3dBMDBBNDI3cWcyVEVGQkJBRGdPNVFhTjFOc1JjWmJ1bmdlZWdSWGIvNk4xaWozc3AzZ3hEQWNOdWdaRlczWTZlL0N1STl3a0U3TFFZMlFKYnVlMzFVdDAzaGhjN3FPWDUyNjM1ZnloaU5UQ2JhaWNUeENudGNhVCtHdzc3YXZhTjE1YzhuNy93Q0MvWXpxdXQ0dTdMV3IrdXYrQzhqZGNzSXpaaWJNSHZiYjluWHRiZTNJZExISHNDZlVxek9PdjJsK3hYUDhPNEp5WWFzMzU4Ny9BT0hPT3ZtZHNWdWN6VmNxV3RaOUREYkJyVFdiVGRwMXZlSjNoY2VUa2Z2ZnNEOEU0dWJnbkx5VHovOEFQMGNmdEwrN3NybGx6YVYzMGJoaGxyMm1DdVhjL3EzSjBQQnlaZG1VOGYwZlgvUnZPdGJOMlZXVkx3aDJJV0QvQUFhL3FRQkFQMVh1NDhuK1kvdG44S25UZFJlMzEzZVAyYis3MUhDOUwrZjh1VzVEQVpFdDRXVzZva3U1UkZFN3l0UVkxMFpwTytSL2twUnFuVC8renhiL0FNd3Ivd0NTeDlSdWhQSldnQko3SVBrNzdXM1QvRWEySzRmbnpEYmVwY1diN2R0cGZNcHQxYUhVeVMxeEE3ZVk3cmUzUFQ1ZXRiRzd2TGluYTJkRjl6Y1ZYaWxUcDBtbDduT2R0d040VFpwMkRPbVNMYklMK21lRDR2YjAyNHJYRHJ6RzJFUzRlUGNOSVk3MkRkbE5mVTIxN3JKMDd4UEkrY3NScHVzM1U4RXZxNXVjTHJzYVRSOEdzTmJXU05nV3pCV3U1SGg1QnlWaStkY3lXV0NZZmJWSHRxMXFmM3V1QWRGS2cxMHVMbmNEWmM4dmJXTDlKck8xbzJOcGIyTkF6U3RLVExlbTcxWlNhR2cvd1dXemlyQUxqc3FMUENBWG5aQUIzRUlKTURkQUlNb0xJN2QwQUVSdWdxTzZDaU5rRkFHRUVsQlhxZ3FFQUhZd2dnM1FSdzJRTEkzUUtlN2VFQWlaajFRWEU3ZWlBZHUvQ0FpZHRrQzNjb0UrQzNWcTdvTDRHNkFUOEtBUnh1Z291UUNnR045K0ZZSkNvLy9XMko3SEJmbUg2QUZQeEEveW5Zb1BRcFRxbHlNbVZJSkRRSW51Z1ZVSXA3Y3dzWkJsdTZkd3JBdzF0VC9ETGR1ZDFvRFVwaVphNkQ2ZGxBMmlDNWhiK1pFdnBWU1EzU1F1bXAydVVhUm0vQ20zVmhXQmJKSTIyWG40NXF1K00yOG5vVGp4d25INzNLMTRkTkc2TTJ3SmlIRDBYNlhoeW1YcDhYbXgxYSttV1NRSjNQY3IxM3c4a3ZrMEJwSUJYUEhqM2x0dks3bW5LdXMzVFhFTTlVOE5yWUs0TXY3V3BvcU9xZkI0THZidVFWOFRyZW52Sm0vZWZaUDQxaDhQdHQvNC93Q1Y1RzZFWmN5eTVsN2pJR0xZcTJOV3YreWFmWUwyOFBSWThlTTA2Zkd2dGh5OWR5WEhEZU14L09Ueit6cTlHalN0MnRwVUExbEdtTkxLYkJwYTBlZ0M5ZU0xNmZ6emx1ZHk3clJnZ3VnbmpsWHZ1OU56S1NkdzU3RGRMazE5NXNUWVB6V3BOdWR6UWVWdSs2WXQ3YWgxQ3dqOXFaZHZMYUI0cHA2cWJ2UnpkMWprOEU4c0xwamk3TVV5emFtb1pyMnMwS3M4Nm1iRmRNZlVacmZLWjFOQ3pSTlUrVWZxc3hxRlh0MVpXVmxXdXNSclU2Rm5SRTFxdFlockEwYm1TZHVCd3MwZkZYVlBLdlF6TXVMWWxpK1VzMTA4T3hKL2lWNjlrYVpOdlVxUVRMSEFRTjlsTVZjR3kzalg5SHNkc01iWmIwcjErSDFSV3AwS29tbFZmVFBsSkg4VjZaNmNNemN4WTlpbVo4V3VjY3hpN2ZkNGpmUHFWU0NmSXdhb0xHQThhUkVMbjlWeGV4MDl6MWkrUXN6V2VQWVhjdnAwNlQyTnVhSU1VNnRMVU5lcG83d3VtbW42YzRmZjA4VXNiZkVLTWVEZTBxZHd3ZWdxVTlRQ2FVN3VzQmdJaEJCM1FlUm1PMEY1aFYxUWNKRlNtNXNmTVFzVWFwMG51NnJzdnVzYXgvRXcrNHEyNUI1RFE3eXlyaU9oUUlCUHF0RER4Mi8vQUdaZ2VJWWd3NlgyMXZWcU5QdTFoSS9pdWZKNmV6b3VtdlVjK0hITEp2S2Uvd0Izd2RkWFZhNmUrN3J2TDdpNWU2clZlZHk1enpxUDgxODNLN3IvQUdmeHoreTlOaGhqK1hzZHBjMTdXNW8zbEYybXJiMUcxYUxnWU1zTXE0MTA2enAvN1IwK1dPZm1XUGF6cG1ObWJNeDFzZHBzTkg3eFNwTkxEM3FVMlFTVnJPN3ZoOGY0SjhOdlM0U1N0ZkhtS1l4OTdsazVmRTl2b1g3TWpxb2ZtSmcrRCtya0UvQ0M3VUQvQUNYWGp2bC9FUDhBcWJ4NDhlUEhOK2RQb053N0JlK2VuOEZ4OVRhTkpBaEhiS2FvbEdWSGhhZ3hya0h3bmZML0FDVW8xVElCaWxpLy9tRmYvSlluc2JtRE95MExnU2dYYzBhTnpUZlNyc1pWbzFQTFVwVlc2cWJtL0lvUEZ3M0oyVXNJdkRpR0VZSFkyZDV1ZnZGTzNZMnFlNTNoQjhTZmFOelhTekQxU3ZQdU5ZVkxiQjJzc3FOVFZNMWFaMVZJUHBxWFM2MDQzZTMxL2tiRXNFNmpaQXdURThRb1VNUnAzRnF4dDNScnRGVm9yTUdsNElQRUVMeitkdHlObHdyQU1Dd09tYU9DWVphMkREOFRMV2sya1hENWpzdDF0NkFNOTU3VHg5UG9vSVZZQmR3cUxQQ0FIY0lCUVU3aEJRQ0N5Z3AzQ0FSd1VFUEFRVUVDKzZDMEVRTGR5Z2plVUZ1NFFMUEtCVzJwQldtSGdvSzlVQUE3b0NQQVFBN2xCU0JiMEFuaEFQWkFDQ0lJVllLVkgvL1gySjduYVYrWWZlRmJ0bmRCa2hqcEJCMlFNZVNOeWVFQ2lOWG1tWldNbGg5cTBRQ2V5c1pwbGNiRnplVmN2U1QyeDlaOG83U3N4cG4wL0krVDZMWUtycEkxZXFnOFhFTGNWbU9wbjRWUngzSDZWZksyWnJQSGJNYWZCck5lNC8zWmdyMi9EY3V6S3ZKenpjZlZ1WE1Yb1kxaFZ2aU5CK3B0ZGpYYmU0WDIrUzkxMitQclZlekkzamtiSldrYVNSdWlDYUpLdjByTjl3UmJ6SE8wZnF1UEg3ZGVUMDhMTm1aTGJLdUhOeGE4Yi9WUEhwMHE3dlFWSEJzL1JNODVNdkwzZEI4UDVPdHpuRGgrSytmVy9UMkxPOXRiK2hTdmJDczJ0WlZnSDA2alRPeDdMcE1NY3ZUeWRaUDdObjkxbmpxL215UUlNK3F6bG5jUERHUEJqak82M1l0SzZ5dFhESEpoNGhRWlh0bjAzaVdrT2Irb1huNUk1WDVQVGxmVHFxY0h6VGptVzZnMDBudis4V3c5bkhkYnd2ZzN2eTY4MGpnY2RsYzRDWUFKbms3ZktTRnp4eTFHbytSdnRoWjl4SnVKV09RckNzK2hoM2dDNnhBVXpEcXIzdThnUHRBTXJQdXErVHlBL1Z0QUo1NERTUHl3dStPT21hT25UZlhjS1kxRjdpR0ZsTnM2U2VQMVdrT3VLRnpiVjMyMTNTTnZkczh0U2k4UTVoYnlQcnloQlc5R3JXck50cmVrYXRhb1F4akJ5NXpqQUErWlc4YTU1UDFNeWJoMWJDc240Rmh0eVM2dmIyTkdsV2VmaUQyMHdkL2xNS1pWckY3TFZ6YVI4eUVCMGoyZDlFQ2J4cHFVeTA4UVFmNXJGSE5zaHZmaDJiTXdZUFVNTnJPWmRNSHFISzRqcVRYUzBTTnQ0L1ZhQ01SdEtXSTRmYzRmV0VVN3VtK2c0K2dxTkxaL2l1WExOeDZlbDV2dXVYSFA4ckh3cmplRVhXQllwZVlMZU5MTGl6cXZwQUVidXB0Y1F4MzFDK2JuanF2OWtmRE9yNC9pSFJZM0d5M1VlWUhFK1hzRnFSNytMa3lzN2I5RnF1MStYeUpnN3FMeDhlNzNQcWo3T21EUHM4blhPSjFteFV4TzUxVXo2MHFPd1A2eXZSdzQrWCthditwZlg0OVQxdUdFL2s4Zjd1eEhreHd2b1plbjh2NWNQbml3dVdQcG5lMU9XaEc4TFVDcm4remYvaFA4bEtOUXlGL1o0dC81aFgveVdQcU55Wnl0QzNjb0tjN3lvSTNZaVRNRWsrN1NJS0RVcmpwbmtHdldmWHE0RGExYTFSeGZVcU9ZQzV6bkdTVHNnOXJDY0h3dkFyUTRmZ3RveXl0U2RYZzBocGFDVEpNRDFLRDBaMlB0c2dvY29JZVVGTzRRVWV5QWV4K2FCYmtFRFpQbVFGeHNPRUZIbEJKaEFEbklLbVVGRkFKNFFVMG9MUUxkeWdFRVNka0ZsQXQ1aEF1WlFVZ2t3Z0dONVFXZ1c5dTZDbkZ5QmUvNWtGN2R1RUFubEFEdVVBRkJVd3JBRG5Lai85RDMzdkxlZDErWWZlTXRudVBaQmxCNUJNaEFKSnFTUndnbW1XQUF3UjZMR1N3MmpxYkRaVmpOWkpQbGdDU3JsNlNNVXRkcUFJN3JNYVp3ODNITUxZa0RRZGYwUVlsYW5yNEVJTkZ6eGdmM3l3ck5EWkliSVBzdDQ1OWxaNU1keDcvUVhNTDYrRlhPQVYzVGNZYTRGbW8rWXNKNCtpL1JjTjdzZHZpOHVPbmJtTlBNbmZmZGFjSVkwK3FLYXdjbFBvbjFYSWtBN0hlRDlGbkNOWnVZOWZheGIwNHYyYkFQcjBHaWU0MWlWOFQ0bHkzSEc2L04rNyt4dU4vK29ZZHZ2V1gvQUdjSjZZOVhzVHlXNmxoOTY1MTNnWk9rMDNHWDAyK3JmUmN1aTY2enhrL3FIeHY3SjlOMTNIY3NaSm4vQUpmTDZueXpuTEFNMVdqYnZDYnhsYlVBWE1MZ0hNUG9XcjlCOTdobk52NFQxdndIck9nenVPY3RuMDNwc1RYVEJqeW5na3dtTmZHbkR5OGZ0VlpvTEhCdTMvRk1vM0wzT0w1cWRVeTcxQndqR21qVFF2SEcxckViRGZpVnp4dmsxcDJLM2ZxWTJEMjU5bDNvYUprd2R6MytRSzQzRnFQakw3WStXTHUzek5oT2JXc0p3Ky90bTJsV3FQaHAxS1JPbVQybVZtZUtyNW1jd2VVVlozMmVCdWRqc1pYb2wyeGs3UjBUeWhXeC9MUFVIRXpRYlVPSFlaVHFXOTBXeUdYMUordnlIMURSdXE1YmRWWjBqeXo5b0xLZUg1NndDN2JoV2EzMDJVY1pwY1VxbHd3YVE0Z2NGd0VsUyttc2ZiWnVsbjJWOE95ZmpkTEg4MjM3TVl2YlFoOW5aMDI2YUdwcGxyejZscEVxNDN3bVQ2UkRwRWtjL3dDYW1WYXdBV3h3bzBGODdmeFFFTndFQlZvOE1pTnlGaWpsMTBEaFBVK3lybnkwY1FvbWpQWWx2QVZ4SFVta2h1L3c5bG9ISmdrenBqZU9VczIxSnZiajNXdnBkV3pKU09ac0JwaDJPMnJBMjlvTjI4YWswYkgzY0F2Rnk0UDZoOWdQdE5sMHZKZVBtdnkvclh5ODVrT2VITUxIMHlXVkdPRU9hOGNnaGVmMC93QkhjSFZjWFVZM1BEejRRanlncVQyNzh2bmlsWkZ0YVZiNjVvMkZvSnIzTlVXOUwxSmU3VEtyeDliMVU2YnBlK3Z1dkxHQzBjdTREaHVDVVFHMDdTaXltOGpZYXdQUCtycEsramhqNGovSFB4VHFiMWZXNVozODNyT0ViZnlYU3ZuZDh5NXJGOWtjb0NTVVV4cTFBaTZnVTNmSS93QWxLTlB5Q1pvNHYvNWpYL3lXUHFOMGI2clFva1NnQTdtQndnSVFCSGZnSUlnRjJ4MlFRSUJuY29MbEJSNFFWS0NrRWdIc2dBa29MQ0NpUU95QlpNbEJOdTZDUzFCVWZvZ0J5QVJ5RUJvQmdGQlIyNFFMYzVBc2tIbEJXM1pCTmh5Z0IwejdJS0hDQ0lLNTVRUkF0NkFBT2QwQW9LY2dVVUFFbFdDS2ovL1IyS296ZUYrWWZlTXRtRVNaSHNneUF3bHVwMFQ3SUFwazdnQUVJQVBpQjRnYktXREtwdExpRDZJRGNTMm9DN1ljS1pKb0JjSFZEQjJieDdxUldWU1BmdXRpbnZnUTRFb0FlTlFsdkNER3hHMUZ4YXVhVzc2WSthbG5mN2E5eHlmQjcxK1Mrb2x0Y2F2RHM3eDVwMTUyYjV0Z3Z0ZEh6WDhMNWZVNFNQcW0xcmlyU3B2QjFBdEcvcUY5VGt3bU4xSHpjZlRLRU9NOExqV2hnN3gyVmszS1krYzhaZlZNaVpBRW1DczhIemIyM2xKM1dYMUhFdnRLWXBTdDhwMk9HdGQrSmQzUUlIOTJtTlUvcXZ6L0FNU3VzKzEvVC9zRnc5L1Z6ayttTXY4QXZIeXZKRGk3dWVWOGFUVCs4U00vRE1XeExCNjdidkRMcXBhMTJHUTZtNGdmVWNMMWNmVVpZUFB6OUZ3ODAxbmpML1NPczVmKzBSbTdDbU50OFZwVThTbzA0MVZIYlBPcmpqdXZwOGZYNTNMV28vQTlWOWpPazVPN3V6eWtuNWFmVE9YTVN1OGF3U3p4Vzd0amFWcnluNG4zYzh0QkVpVjk2ejVPNS9CT3E0TWVIcXN1UEM3eHh2djZ0STZ1WWFhK0FQdktPOWF5YzI0WTRkdEpYbnc4MTU4dmJiTWw0blR4YkFiQythL1VhbE5vcUgwY0J1dlRmU1BmTWdudnlzVEkyOFhObVVjRnp0Z1Z6Z09ZYmR0emgxdzNneE5OL1p6ZmNMbmNWMithSzMyTXlNVWE2d3pFMW1DQnhPaXJUSnJocDdBalpkY0l6Zkx2V0M1QndISWZUdkVjdFlOVERxUDNTNGRjM0RnR3ZyVlRUUG1jdWVYSlpVN1h5VDltenFaV3lObTV1Q1hJZlZ3VEdIQ2xjTkRTN3czeWRMZ09GMW5tRW1uM3F4d0RXdkJpbVFDREVpSENSQVNlQ3paN1hOT3dVdmxaTktNRkZDUWdKcmRnZ0luYVBVUXBvY3M2bXNkWTNPRFkyd24rcFhqTlo3YUhuU1ovVlQwT21Xai9BQnFEWHpMZHRQeUlrS3lqTGJ0d3R4TGxaNlFrYkVpZEk0OVNlNjU1NFNyeWI0OVpTNi9aeERxLzBjYmk3YTJhc3EwUXpGTkpONWF0Z05xaHU1SUEvTjZMelpjVWordy9aTDdWNThQSDkxbnF5K0xidmI1c3JNZlMxMFh0TGF0TjJoN0hDQ0hnd1FWNHZPMytnWnlUazZhWGo4K25UZWcrWGYyNW5pM3ZLOVBYWjRVeDEwLzA4UWJOQlhvd3cyL252MjUrSlpkTjBIWlBkZldrblRISU84RmZRbnAvbU9aNnk3aDZ5UVBWSEhISHQ1TG45YXZXUzNqZEcxZ0NKUkZoOEEreW9UZGtlRVNmUlFhZGtIYWppcEc0T0kzRS9RaFRRM1VFQnZ1cUJJN29MYVB6SUtjZDVRU1pRVWQwRmNJSzcrNVFXUUFQZEJTQVNZS0NIaVVGU2dGQkpRRnNSdWk2S2Q2b3pRQTZwbnNpNCtVMlNnaVJDa3FGdWdoVlFqa0lDTG9RVGIxUUM3aVVhMFNkeVVOQUlBRXFXb2dVMmluTFFvN29CNDJRUTdCQUFjSUo5RUVEcFFDZDBDaU56dWdyalpCRHhLQlhKaEJSYkJWZ3FBcVAvOUwzcWxXU3Z6RDd4bEhVZHdQbWd5Z1paM2hBbWtYQjUwL0Q3b0dPZVFDVDZvTW1nOWtEVk1vTHV0RFkzbjBoWnlHT3dtUTVxa0daVHF1aUNJSzJMcUFrYW5PMlFJMVBZUVB5RkJsR25ycEdQUlhCWTVMMUx3WjlTM2ZkMDJ4VW94VWE4Y2g3VElYZmc1TzNKNXVYSGJzZlNuTXJzZnl2WlhGUjRkY1VBS0Z3RHpxYnRLL1JkM2ROdmtaNDl0MDZNd3lWaXNJNHc2VXh2aXMzeDVMclhCWVdzWnU1Mnhqc0N1ZU43RytlZko0OTE4cmZhSHhvWGVhYmZCV1AxdHcyaUJWYis3V3FHU1BvRitXNnprN3VSL2Zmc0IwTjRlaTc4dmRyanE4aittd2JkdVZIU04wNllaYUdhczhZWGgxUnVxMWJVKzhYUWpZc3Bid3ZxOUh4OTkyL0pmYW5yWjB2U1paVDNYM0Q0VEdOTEtma2FHQmpENkJ1d0VmSmZwZjVkUDhBTCtISjMzTEsrNjhUTWVIc3hMRGJtM2UzYXN4ekhqdHVObHgxcGxvUFI2K3FXOXJpR0ExdmlzTGh6V2gzN3BNcnJMc2RYRGUzY0tVRklFRHY2cUMvTUNQTVNPeG4vd0JsWmRCRjFhdHZMVzRzYmlYVUxxbTZsVUxUcGRvZUlPL3lYS3pkR3RaVjZaWkd5YlJiVHdMQnJlbFVBaDF6VVlLbFI4R1FTWGNGZHNiNEc1Qngra21UN1I2SUxhQTBiSUlaQ0N3WkVvQ0RvYUQyS0N5WkFoQnBYVW13RjVsZkVDRzZuMHFmaXRIZVdtUXNaRDA4azM1eExMV0gzWk12ZlJZS2dQN3pSQlZnMk5wMldtY3JwY24xUjZzOFp5WWFNY0JCTVIzQkN6WjRlSGp4NU9MZW5DT3RmU3R0NVJyNXZ3Q2lHM3RzTldJV3RKdTFkdnEwRHV2SGx4djdmOWl2dFg5empqd2N0LzhBbjlhOUw3T0dFdHRNcjMyTTFHZ1Y3KzQ4S203MXAweHgrcTY4ZU9uei93RHFQOFErKzU4Y2NmV25aVEVSM0haZWwvSlo2Vi9KRkVDSjM0UVZxM2o5RUUzM0NCZHlKcE9Ic2cwM3AvcUZERmdlQmlOeC9FaEJ1c2t4SENBaWRvUVdOaENBVHVna0dKUVNPNkFUdnVnRStvNUNDd2RYUEtDZXlBRHVVRms3SUJRRElRUkJjN1FqVUxKSHd6NXYzZTZNVnpURit1ZlRyQWNTdWNKeGZFS2xyaUZxN1RXb3Zwa0VILzNUVEc5TWRuMmcrbHRWbFdvM0Z6cG9nRjBzSWtIMFdwR3BTajlvdnBSOFA3WGNEejhCU3JzUCtrWDBvNC9hN3Y4QVlLeW5jbitrVDBwLytyTy8yQ2gzSWZ0RWRLai9BUHk1L3dCZ29keWY2US9Tdi82dWY5Z29iVWZ0RWRLaUkvYXgvd0Jnb3gzRi93Q2tMMHRrL3dDdGovc0ZEdVVmdENkTGkzYkZTZjhBMEtmVnFaTFo5b0xwYngrMWpNd0JvTWsreXRtbTQybkt1ZnN0WjNGVitYSzdycWxSMnFWZEpEQVNZaWZWWXh1MXNiS04xdEFkNFFXVEkyN0lBM2dpRUN4SUVsQmN6dWdBN2xBSjVRVWVFQVJ1U2dvbmRXQVR3cVAvMC9jQURuUjM5RitYZmVaVkdtN1NZUDBWRDJTMFEvaEFCWnFCTFBLUGRBbVNYQmovQUY1UVo0cHQwYmVpQWFqUkFISldhQnBPQU8zL0FMS1FaRDNOYUFZayt5MkExSHd5NTNDQUE4bHphY2ZWQm1VbmxyUzByV0E4TE1WZ3kvdEtyQUoxU0hBL0pjTjJaSnBwZlNER0t1WGMzWG1XN2wwVzE1TDZBT3cxTjdCZnBPbHpsNDVOK1h5ZXF4dmZYMHZScWFvSVBsSTJYb3ltdmJ4N2pKMVI1dVFzVDJXUE14aStwNFJobDVpWnB1cXVvVTNWZkRZQzV6aXdhZ0FCN2hjdXE4WStIdDRzWm56NFlYMXVQaGJNZDdpT0k0emQ0dGl6SDA3Mi9xUHIxZkZCYTRGeDJFSDBDL0g1VEs1YnNyL1UzUjU5UHc0Y2ZGeDU0MmR2blYyOHYwUHJ3cHQ5aVdVWW5zSmpzcTZ4OUIvWml3ZlhlNDNqajJ5NmcybmJVWEgxZjVuUWZvMWZwUGhXUHkrWDhZLzZoOVZmSEhLK2twY1NaQytwZnhQNG56eThlZU1ucTJGWExRNms0RG5aWnlqdG43dW5HN0Y1eTUxU2ZSZHRiWXZUTFdqdHJIZFp3dmxsMlNpOHVhSEhnaitXeTY1ZXcvYjV6M1dCWmJBa2Zvb0lQVThxNkFuNFJDQXdUQVdnejhzOWtCRndRQVQ2ZDBCQXVranNPRUVrNmdDZ3dNYXR2dk5qY1VZa1BwdmFSNnlGbklhWjBvdVh0d2k0d3VxZnhjUHVIMG5UMkJNaElPaXRWcDI5M2dTUk1PUzQwMGthZUo5bDBqcG56Mzhnd0h0YzEwRU9FUFk3Z3RQWmM4OFV4M2puanl6MjhyQk1Fczh1Mkp3ekRtYWJNMUtsZHJQM0RWZHEyK1VxWXg3ZmlIWFpkVHk0N2VyOVo5MXA0YWlJaUNiNmhDQXQ1M1FLdWlmQ2RIb2Y1SU5OeUFYZURpNFAvd0JSci81SU4xYndnaUFrQW9JVHNnZ1BsUUFTZ0VGQWJlVUZIbEFzRXo3SUxRUkFzOG9DUVJGQ2ZNQ0FUdDhVYmM4RUl6WEJmdEVkSTI1dXdwMmFzdjI3Um1QRFdsMWRqQkJ1S1RlWmpraGQrS1N1R2UzeUxsUEFmNlNaaHRzdTE3MW1IZmZIbW1hMXdYQmpYajhwamlENnFaelRXTmRrdS9zdTN1SFBaUnZzMzRYYjFIQ2FkT3U1MU54YWVDSjVYbm03WFhMMHhqOW1zekg5Tk1Ibi92VC9BTVZxZWZUbHRQOEFSc2tmL3dCYllQQjRQakgvQUlxUzc5Q2g5bWx4SkF6dGhFdCtJZUtkdjRxcGJwZitqVlZpZjZaNFBFVFBpbmo5VmRWYmRCLzBiWHlSL1RYQ0pIUDRwLzRxVHo0aTY4YlYvbzFWQ1QvK3NzSk04SHhPZjRwSnU2WFhqWk9LL1pzdmNKd082ekZWekxoOVhETFZwYzZyU0pJY1IyRHVGTWZtOHp6R01yTWJxdVY1UHl2aWVjY2Z0Y0R3cHJxejZ0VFMrczBUb3BBd1htZUYwenMwNzRQMEF5UGszQzhpWmV0c0Z3NmxwZFRBTmVwdEw2aEc3cEM4dkZ2ZGJ5YkpPblUxZDJBZGllNkFaNVFEcUtDTytGQUE0UUxKSUtDSUlTRUFlcUFUeXJCUjRWSC8xUGEwZWZVMHI4cEgzbVd6WVNEdXVrREd2ZEhtRStoUkVGUngySWhSdUtjQnFhUXFWbHNkQUhwM1ZaVlZsNUpid29FTVk2ZHpQc2d6RzA1WkEySkNCVHRUV0ZwM2hHZzBudWxyb1FaekJMbzlRakxIdWFJZFRlQ094Q0RqbWNiYXJsL0dyTE1ObkxhMXBVYlVQdXlkMTZla3V1VGJ6ODAzSDB4bGZGNk9NNFJhWWhSSWMyNHBpcHQySkc2L1I4dVhkSHdjc2ZtZXg0cFpzZTY0NGVJOU1zMGNBS2pDMGpadzRpWldNc2R1Y3l1RjNIajRubEhMMk8wSDBNUncyaFVFZVltbTBQOEFvVjU4dWw3NXF2cjhIeDdtNFA3cTJaZnBweGpPZjJkcmNVMzMyVEt4YldjQ1hXRll5SEgwYWV5K1AxSFE5dnAvVC9nMzI1NHNjWmp6Ky8xeS93RFp3REZNTXhMQmI2cmgySTBLbHJkVWlRNm00Uk1kd1R5RjhxM0xHNmYxTG9maU9IVzQ5M0ZkVDlQTDZoK3pkYk5aa2k0dStIWEYzVUgreUF2MXZRNDZqK0UvYnpueXk2eDJnTzJYc3k5djU3bGUvVW9DNmZLUnNWdlAwMXJYaHlEcXphdnNLK0daaW9naDloWGE0dWJ6b0pneXZKak5VZFJ3VzhaZldGQzRrR2xXWTE3RDdFU3ZUV2E5RnNSQTRDaURiNm9KcTFHVUVqVHVnbXBBMXZ3NlVBZUg3b0lScGI5VURHbVJLQ2pIMVFLcmh4cE9FVElQNlFnNXRreXIrejgrWTloTC9MVHV0TjB3ZXJqc1VIVVdUQWtkaHY4QUpBWk1RZ0lPMlFBNUJKZEtBbXVRUjNZK3FBcGxCRUVLMUFtNU1VWC9BQ1A4bEtOUnlBWnBZdC81aFgveVdKN0c2amhhRklJZ0ZCRUJCQW84bEJTQ3dnTDFRQTM0VUVRQ2dGQkVGanVnQnlBUUo4c0E2dG9QRVJ2UHpWaVY4YWZhTzZVLzBVeFJ1Zk10VW4wc0V2YWdxWFRLUTN0YmdHUzZQM1NWMzM0Y05lV1MyNFBYUHBrNWdxYU9vMlZHZVY0ZHArOVd6UkpBQTNKaFl4eTdjdHVsdTVwZzlMTXI5TmN5OU9jVnRzeFhCdHM0VUtsYzB4NGp6Y3QwQ0tZRFJ1U1RNcm5sL0IvcXpHMERweDBYdHNzNEpkM2wvVnAzcHEwRzMxQjlSL2oxTi9NS2xNYmlmWlNZL2R6dS9OdjNOQmRsVG9pL3FpY0pOMS9xUDdqNGpiYnhLbjNkdHdYQ2R3ZFdyVDZyZkxPekRHL200ZHZmOHY4QWhlQmdlU2VrdUtmMHgrOTQ1VXBPdyt2V1pnbElWM1NXc0JMZEk1UDFXK1hMc3p4bjVyZm4rYi9DRERjcmRJNjJTY0RaZDR1ODVsdkxscmE5VXZlYWpHNi9OckE0RWVxNVQrRnZMNjJ1bG5tWS9US2JZV2QrbU9XYnZQdUNaVjZiNHJXdktkK0p4QU5xbHdwQUg0cDdDSlRLM2l3Ny93Q2JKTU5YUDd1K29SMXR6aGFVUlk5S01wdmMvQmNLRGFGNVVvblViaTZJamM5OTEzd3huVDhmWitmbi9WeHp3dkpuM2YwZHk2QmRMcVdTc3YwOFl4R2lEbUhFbUIxUW4vbzJSQWFQbnl2bTN6aytqak8yT3dPMmdlZ2duM1hyK2puYUZSRSthQVhCdTZCYUMzZkNnVWdGelVGOElBZWdIc2doVmdFOEtqLy8xZmNHa09iQVg1U1B2TW1OdGd1a0JpV3RFbjZJaXFqeVJMUWpjRFJlWmg0bWU1UXJMRHdPMGhWbEMvMDJCN0tBYWJUK3FESkJkRUE4SUFMaU5pRWFCUWtFZzhEaEJraDVIbUNNcFVyRXMzYnNlU2cwN09lRTBzVHcyb05NdWEwZ1Q2SHNreTdidG5PYmpNNkQ1Z2Y5MXVjcjNUdjYzaHppYVlKNXBPMkg2TDlMMDJYZmkrTnk0YXJ1QlkzNWdDTi9WWDZ2TGx2eG95bWR4UDBXcFhiR3krekNKZUV1V3A0UzhNM3ZIMnZ2Qjc3RmNyaDN6eXhsd1lUemEwdnFEMDV3ZlBlSDFhTmVrS1dLTkg5WHV3QUtnSTRHcjBsZVBQcE1mYjlmOEcrMEhOMHVXT09GdXR6NnZONkw0RGZaYnl2YzRGaWpDeTdzcjJ0cVA1WE1jQjVtcjE5Tk5SMyswL1djWFU5VDRkSGE3MDRYU2VjbjRmaytYT2FITXVqMldzbmExcDJmOEtPSjRCZTBOaTUxTnhiUDkwTGxZanp1aytLdXhESzFHM3FHYTFpZnU5VW40cGFmK0MzS2xkREd4SUh3enNWVU1DQ2FSNndndVE0Ym5jSUtRWHE4eUEwRWduNUlJSkNBaHBPL2RCSGJzTFFlUVVISzhXY01JNms0WGV1aHRPOXBPdDNIMWRFaVVIVld1MkExU0FnTUVJTGtSc2doNFFESjlVQmpTZ29tZmtnc0ZBZXgzUUNIQXlQUmFnWGNRYVRoN0tVYWRrSnNVOFVJTzM3UnVmNXRXSjdHN040V2hFRk8rRkJTQzVDQzQySkJRTElnVEVvQTFTZHhFSURKRWJjb0Jrb0xid1VGSUJRQzdsQVVpRUFTZ0VvQmQ4Sjk5aXFsZWJqZUM0Zm1UQ2J2QnNXcGVOWTNsTTBxb0lrZ0VjajNIWldWTEh3bmNOeDdvRjFUa2h4cDJkWXVJQjhseFpWRHdQWHkvd0FWMHp3M2k0NDNkMDZOajExaFBTN3FKaGZWdkNMVnQ5bExNMUh4dkRwdER2QnFWQk5RRHRxRXd1SFBmdmRhK2pwV3hZdmpkYWhuQ3g2ejNPVWFneVUrMWJidWVRMDE1ZVpiVkRKNW51dStYOFRHWXo2TTcxVEtXT1Vjc1p2eGJxWG1QSmxhaGx2R2FGSmxsZGhqSDFLY0Q0elRuYldzVy9lL0ovaFhqK1RLNVg2dkt5L2Y0TGxITm1ONTV6bGxLdGgyRDQrYWY3Qm1tMTdScTVEbXpzNXk1NWZ4Zm5uOHJQSGoyWTNHL1U3Q3NYeXprSyt6Sm0zT3VVM1lkaE9ZSCtKZ2pLakdPRDZZYkdnQUU2WE9tVnJrL2k4bU92dzY4cmN0Y2QvT05kd2k0dGVtR1FjWDZpZUVLR1o4MlZLbEhMMXVSQm8yenlkeE80Z0x0bmpNK1RYOHNjTGIyU3o4VHl2czZkT2FtYjh3MXM0NHl3MWNMc1htdFNkVTNiV3JFeWVlWUs4blB6ZmVaN243UG9ZU1NQc3RyUXdCclJvYVBoYjJBN1FtT0Jja0pnUjI1WFJ6aXBIWkZVZ0J3Q0FZOTBFZDhLQUVBTzVRVVVDaVNnZ1FGdDNWZ0J4Q28vL1c5b2N0TXI4dHA5NW5NcU5EZk1OMW9YVWduZmhEUUNSb0phalE2RVZCcGNJUHFFU3Nscld0YVE3anNlNklXN1NBZlJTME10UTBnZ25mc3BLSDZkSVB1dERIY0hrRWs3OWtYYTZiWEV1SVEyeWFNYVljaUlJYVhONWIybEJoVjdkbGMxS2U1QmJwMDl0MWl6ZFg2T2FXOXc3SlhVR3l4SDRMTzhjS0Z3NDhBRXdDdnU5SHk5azFIek9mRjlPV3RkbHhSWlVwbVdFQ0Q2OTVYdXMrdjV2bjQ1ZTJRSUJrck5qbmNObmFpTkpidktTTmR2alcxdWJJazhycDNOL1RTd1JBQkhDNVpZOXlZL0w2VVlCSkRRQ1RxTWZJRC9KYXhuYkhQTER1ejc3YnNRTy9BU1RWMjZaZk5kMWU0Y1NzNzhydGlZaFJiWHRudElCQkJuNjdLWkczSnVubGM0Sm5YR3NCZjVhTndmR29zUHZ5Vk9PN1IyS200NlI4NUh5N0x0b09EaXNTaXdTcUxkSHhSQkNDRGlTZ0dkMEROUlFNYVpDQ3Vka0ZHbjZFb0xEUzBLV2ptSFUya2JWMkdZeTNteHVtRjA4YVhiSnNkTHNxamJpMnAxV21RNWpYRDNCRXBLSDdBcWl6dENBeEJFSUZuWXdnSTdBSUxJaEJROVVCY29KQVYyRlZ3RFNkdkd5RFVjZy8yT0s3L3dEOGpjL3phczZHNk45RlJaMlFDZDJvQjFGQllFaVVFSmdRTzZDQnhBaVVGaDJ4SjNQWkJObk5uZyt5QlRqRW9MYWRrRVFDZGtBditKQlhxZ0VtQUlRUnZtSmxBRGo1VlJLZlBmanNwUEJmTGhmMmwrblRjMVpTL3BGaHRFZnRqQldGMHQ1ZGJqZDdUMzI1QzluRGwzWHR2NVBKeS93NTNSeHpwTGNXL1VUSUdPZEtNU2ROemJVemlPWDNQK05qMjd1YUNmZmVQZGVMcGZHZGxkK2Y1Y1pjVzg5TkxqUDNVM3B6aXVUYWw3YldMc0ZCdzU5VjdTNjVQaE9scllPM2FPRmVQTzhmSitsVHRsdzM5VlpqeGJxdm1ycFJYdHpodHF5MHc2NHAybGF1SEh4YTdMTndHcGdKMmFTUE10OU5KeDh1ZVgrS3NjdDNoanBtZFMzZFNjeVlmbFRJVnpZV2xsYzR4VXBWdnZ6SEY3YWRTMGgzQjRNY3d1ZlRmdytMUEgvRkhYbHU4OGJQVHdNN0hQR2Q4KzREMGx6RWJXcFJzYWxLOXVMdTJhUTE5S2swZVoycVkyQzFuYjAvRk1jUG11WDUvd0REUERoT1hMTEsvd0FyUWVzR05Wcy9kUXJiS09BeTdETU1jM0RjT3BNK0V1RU5xUDhBbnl2UjFVbkQwc3M4NVZ4Nlc5M0xiWDJIa2ZLbGxrN0s5aGwrMVlHdHQ2YmZGSTVOUWdGMHg3cjV2RHgvTHQ2OWE4TmlQcnl2WDNKb3R6aXNxc0lJZ1c4N0ZBb2tnVEtDeStSQ0NpWUNBQ1ozUURxSkNBRHlnZ1FRblpVTGNkazJtMy8vMS9abzB3OXdBL0x5dnpEN3pMaHBiN2c3SUMxTmNBQ2loZTd3cEEzYVJ0ODBVVm5XZ2tPSEtKV1U1MU5zbHhSQ25FSDVMTkJVd1J1dzdxUVAxbW8yQjhRV3dsK3JVSUJqdWdiUnFhSEVIa29ESUFjRHZEa0JWSEFDQnllRUFVcHA2ZzRibmdoZEwrQkdnZFFjSi9hVmhWck1INGxFaDA4RUZwa0VMSFQ4bDdubTVjZHVvZEpzeURIY3IycnF6cHVxQThHcVBlbnRLL1Q5MHNqNWVlT3EzOCtacGovL0FHVkhNVk54RVNnWVhIVVBSQWNFOElLY2dKeDNDQzl6eXBKNUExV1RUY0JPL0Nad2NaemsxMlg4OTRQajFJYWFWWi8zZXNSNlAyM1hMSHdPeVVLZ3FNYThlZ0gwQzlRZUN1ZXRBMmtTZ3Z1UFJCYmlDSUNCRTdvSGpjU2dOc3dnalNKajBRRVhUd2dtclVDTzRVbzAzcUxoN2IvTFY2d0R6dGJyWjgyYnFEME1oNGgrME1yNGJjaytjMGd4M3pidC9rckJzamh2N3FpendnalQyUVJ3TXlnczdnSUNJa2JJQUowN0ZBVFNncDIrd1FLdUFmQ2NQVkJwMlFDZkN4VDN4QzVJK3BDRGQyZzdGQVozUUNkaENBSVFFREFRVTg3QkFCS0NUc1FnSnBocUNqOE1kMEF6QVFVVEhLQ3laUUM3ZHhRVU9DZ0VpUWdqUkJNK2lBUzBrSDJRQzB4M2h3NFFMdWFGQzdvVnJhNFlIMEs3SFU2clR1QzE0Zy96VHU3ZktYSHVtbndSakZ2ZWRGK3RUblVIRmxyYVhiYTFJajRYV2xaME9BOVJCajZMcDFlUFpxeHc2UzkyNVc5WnkvYldTZXJ0cGU1VnhWMkZZRG5jVXJsMVJ1OUp2ajZXdWQ2QWlkbE9weDNoTEY2YTZ6c3JlcnZKMmNzUHhhejZXMkdad012WXRTcllqVXZIc0Rya09CMVBaejhMandzYzExaGpZMXhZN3p5bCtoRmZMZlVyTlZ2WHpOYzVobzIyS1pKclZxTmkybFRHaXFMZloxU3B2c1hnTHJ5eldlTWpseFpmSmxiOUdtNUx6RmlkcGxUT1hXWE1ieFV4Ni9hNnh3L3MwUGRzZEU4QmI2YWZlOCtyNmpYRmwyVDkyQjlsL0t2N2V6WmVadXhGdmlVc09sMU56dHdhMVF5U1BYY3J3OVRuMzlYY1BwLzdPM0h4OWwyK3hESmNleG5lT0YzMXB1cEVCUkFGcDVRVTUwQ0VGUVVBUGtJQWR1SVFUU2dqdUVBY0FvQUJqWkFFaVNncHhoQlV5aVV0eFJILzBQYW9WTkpCTzA4eXZ6RDd6TTFVNWdDWjVoQU5TbjNCZ2VpS2gwQm5tM0tLdWthWTNoRXJJZXdHbnFiQlJBQjRBRFhBUkN6UXlrR3VNaFNCbWxyU0RPM2RiQU9jTlhrSmhBZEJ3TzU1UVpEaElZSTM5VUNYdE02ZjNka0RxYjJ2R2wyeEhFck52MForckN4ZXpwMTdWN1kzZTNTNGV5MWhqSjVMTnRKNlc0bTdMbWRyekw5VnhiYVh4bWdIYkFPYjJIemxmWTZUazd0eDh6cWNkYWZSVkp4ZFBvQkgxQytocDRqVzlsQThBUlBkQVllMUFEdCtFRVBLQjdRMGdMVUF2QmdSd3BrT1pkV3NMZGQ0RFVxMGgrUGJrVjZiaHlDd3l2UDlSc3VSOFpHTlpidysrRzczVW10cWY0bWJGZXJIME5wYTRPSHVPVktJc2duRWxBRUhkQlRQS1VEMmtRZ052Q0MyaHNtUnVnam9EZktnR1lnRHZ5cFI1MkswUHZWbFhvRVMyb3h6VDlRb05PNlUzTHFlR1h1RTFObjRmZDFLSUI1RFprS3dkR0JrazlsUVFRVndVQkFnOTBFa2VxQ3dTZ2poTFo3b0JiN29MN29GM0h3R0VHbTVBRHZBeEk5dnY5eC92QkJ2RGVBZ0tSNm9CY2dGQmNJQmZ3RUFvTEhCUVVTWWhCVW5nb0lVRlA3SUpCUUFTWktBV2s4ZC9SQllPMjZvanR0MDBiVWUzdWdBeDlTZ29tT2UzSTlrMXMzWStYL3RlNVRaVnc3QnM0MFdlZTNmOXh1M05ITkoyN1pQdHV2VmhybHc4dkpuTHg1K0dpWXRjMWM3ZlordzNGd1MvRzhsM2YzZXRVSDlwOTNjWVp2enNTMWNPbnZkdkhQeCtXM2JxWjI2dUhuOW5yVXNHdHJIcHJaZFVhV2NiaCtiNkRHQ2k5OVVPTk50UjRhNmxvbmVBdVBEODJkd3Z1TmN2eXlaWS9WNm5WS2xhNU82ZFdWL2xITWRhcGlHWVgwemlMZkdEdnZUNndCYzhBSFlTVjZPSzkyRnp5K2p6Y2sxbEpqNTIxUHJSY2YwYXlEa3ZJRkE2YWpxQXhHL0E1ZFVxN2dPSHpsVHBiZVBqeTVMNHU3cjlUcWZ4NHpIeit6NkE2QVpZYmx2cHpoemFsUFJjWHp2dmRja1E0dGRPa0x3OEU3dDhsOXZyZFJKTUpwMUJwYzR6N1RIb0Y2ZHZOVmwyM3pWUk40RTdKbzJXOEVRVHhLZ0lneFBaQXA1M2I2RkJJYWh0U0NuSUZ1NFFLY1lLQllhWEVtVUJ3RUVJQVFwTHVVWmYvL1I5bWkzeEJxWDVoOTVsMGFaQ0FxclR5VHNpa1BnaUJ1aW5NWjVQZnNpVXdWQ0tRYjNCM1JBTzBtSjVSWVpTbW52MlJUU2ZJRDdvRmgyNkJsTGxCa0Y3aVFXL0NPVVpVNSsrb2piMVFHQTE3bXVBM1FNck1Md2RNRm9FSDVxamxPZTdhdmhPSjJ1WUxNYWF0blVGVnhiKzZDSlhxNmJMV1R6YytPNUgwSGxqRnFlTllSYVloU2VITnVLYlhPSDk0amRmb01ydkY4WFBIVmU4MFNQOEs1WStuVGZneHZDckNuR2V5Q04xZkNnc2NGQVZNd0VEV0daQ0R4c2ZzV1gySDE3WjRCRlJwYjd4M1hQS2VSenZwSGZPdGYycGx5cVMxOWxYY2FUVCs0OHlQNUxyTDRIV0dlVW4wTUg2ck1nYTA2akNvdDVqWkFCZHNnamVXb0hGc0ZBUUtDeTdkQk5XeUFaNy9SYWdYV2JxcHVaNmpiNXJOSE5zcVZEaG1mTWJ3MTM5bmVCdDFUK2ZEbG1leDFCcE1RT0FWb05IQ0NrRk4wb0MyUVdPRUVjWWFnRUdRZ2lCVndZb3VRYWowOU0yZUkvK1B1UDk0SU4zN0lCZHlnbXBCRUZrN0lBOHBjQTQ3T1FVVzZTUU9BZ2dRUW1FQUV5NUJZMktDaTdkQmJuYklBZTdaSkN3dHptc3B1ZlUwdG9OR3ArczZRUGZVdWs0M0sxekxOM1gzcHprMTFTM3I0Z0wrOVpzYmF5SGpPQjlEMkN1dGVDWGJsR0pmYkVwTnFPR0Y1YzFVL3l1clZZY2ZwR3lteGVHZmJEc3FqdzNGc3ZQcFU1ODFXM3F5OGZJUnVwVjI2L2s3clZrTFBCcDBjT3Z4YjM5V0FMTzhBcHZKOU5VcU55dWdPMkdocDlQS054OUQzQ2xhdGFKMWR5NDNOWFR6SHNMYzNXNDBYM0Z1TzRxVWhyMi9SZE9EMjVjdnA4ci9aN3J0eGEzelprRzlkRkhGOE9xVktZUGF2U0JBZCt2OGxubi9Iais4T0h4amYySXlIZlpEbzlQYzBaZnpQWVZhbU8ybGViU3JEM2tBdjBTQU5oSE82VEhYVmQzN3VPRjNpMlhPbUg1S3pCbmJJT1hjblc3cWRSM2h2dldEVkFwQ09XdjI3ZGx6NUwvd0NFN2Z6MjZkUFBtYXgxVHJ1emwxdEdDV3Y0bHZiVjZHSDBRT0lwRWF2NWxkdWZQK0RqUDBqUFRUWEpsKzc3Wnc2eXBXRmpiV05FYWFWdlNaVGEzMDBpRjQrQjd1VEx5eUtqbTBnYWxWelcwbWlTNXgwZ2U1SzlVdzM1ZWZQazFYTmMzOWQrbitVL0VvVjhRTjVlTWttaGJROXNqYUpDdXRNZDIzTTd6N1htR3RNMmVBVkhVanNIVkg2U1I4a3RXTXJDUHRZNVl1bnRwNG5oTmUyRHRqVVkvWEgwaFlyVHJXVnVwdVNjNHQvMVJpdEoxYnRSck8wVko5QUZ5cXoyMnd4OFRkbzJqa0VIdUVsYW9ITHBNWE94TlNOaEhKUURWaU51VUNvSkVsQlVRRUVIWkJIY3F3S2NZM1ZILzlMMjZJRGZMVE1yOHcrOHlDK3BUSUI3b0NjZGJUSmtvckRPcG5lUktLemJaeElrOG9sTjBnT21OanlpTWR3Y05SOTlrV01taVI0Zm0zUlVEWGFRUGZaQlQ1NEFnK29RTXBOR2tuVjU1NFFaSWFHTk1tVVpLMWdpTzNZSURwT0xOeHVVR1FYZ3cwRG5sVWF6bTJ3RjlZMXFHbHBsam1rRWN5cE0rM0tIYnVKMEt4cDdhTjdsbTdlVFh3NTU4TFVkeXcrbnNGK253dThIeGVmSFZkdVlaQWMxM2xMZHg3eXBIbk5hWUNvdlZQWkJiUWRhQW5BUndnbzdEWkJHdmprb0YxMk5lemZtRCtpellPT2cvd0JIZXFGTnp2SmE0c3p3dlJvZU9EODFtVWRscHVKWTA5bkNmMDJYYlhnR0RCbFpFYzZSenVnZ0lJanVnTm9RSEpJM1FYSTB6M1FFZXlDanM2ZnkraUNDUG90UVZVZ3RJNE1nejhsS09ZWTcvcXJxRmcxK1BMU3ZnKzNlUnNQTkJFL29zVDJPcFVIdGN3T1BMaVhBZXcyV2d5ZlRoQkpDQ0JyWmR1Z0NlMzhVQmlXN0VvTEprSUFCS0E1aEFtNUk4QnlEVCtuYnY2bmlQL2o3ai9lQ0RkZzczUVdncEJhQ0hoQXB6WmMwb0dHQWdvRUlLY1FRZ0ErdmRCVGlncEJmNVVIazVqekpnK1ZzSnVNYXh5NWJhNGZidEpjOTNMbkRoalIzbGQrM1RqOTQrTHVwdlhyTjNVYkVUZ0dWbTFySEJLaml5MXNiYVRkWEVtQVhGdThGWHZrTE5qeS85bnU2cDJsTEh1cE9OMGN0WWM4NmhRcU9hKzdMU0pQbGNkeXVXV1c2c21ub08vMFhNQTFXN3YyaGp0YW50VXVLYnkxcEJJRWtIWWJyT3dwZyt6Rm1UVGJVUmlHQlhKSjAzRHFoZXdON0V0NFZTdkd6UjBIeFRDc00vcFowOHhabVo4SXBEeHRWbVEyNnBzbmtodSt5TEsybm85OXBERWNFdWFPV3M4Vm4zV0VCd3BXMSs2VFh0M2NSVW5jd3BmVHB0OVlWY1J3eTV3ZXJpTExpblZ3cDlCOVh4eVc2RFNMVEpKOStJWFRnOXNjM3A4TGRITCszdHV1RnI5eE9tenVMbTRvdFo2MDN1ZHQ4aEs1OVZkV1U0dndaZnMzMDVYNms1U3hIUHVGWU5sRjEvaHVZSzcyMnQ0WE5nQngxUzBkbDI1TE83REw4NHp3NGZLMnZMT0c1eXpGMUh5M2oyWWNxbkI2R0Q0ZFZ0Nmx4TFNIMUtiZktUNkZjTGp2SERIODZtR1hiazR4a1BFTFAvbDZiZTR5NktUOFV1Q0hPUER5NGdTVmVwbXZINU9uRFBOcjdneHZITU15NWg5ZkY4WHVXVytIMjQxVktwY0Qya05iNmtxOEhINFo1Yy9tZkd2VkRyem1QUHQrY0N5dWFsbGd6MytGVGJSSjhldjJFeHZ1dXR5N2ZETng3cnN2QStnVmFuaHJNZDZoNDVSeTlaMVJyRktxUWJsMDc3dE82NVhMWk1kUFFPRy9abndpbzJoWHVyN0VhZ2hqN2hqbkRXNzFBNFNPa01PVWZzOVprZjRXRDQxZDRIZVZOcWJibVhlWWJjT1VxdGN6RDBVenRsSmpzZnlyZHR4ckNxWDRndkxCMFZHdDlTMXZKWE96Wlc1ZEt2dElYdUhWS1dYODhsMWF6SkRLZDZaYldwRWJhYWs3cTl1aVY5VTJkN1pZbmEwYit6cmk0dGJoZ2V5dlMzWkI0RzNkTy9UZWpuZkN0SUVIYjNRQzdjSUJRVjJRQVVBU1pNbFdBVHZzT1ZSLy8wL1V0eVcxQWVBdnpEN3pLcVBlNTRhQkxUM1FWK0pTRy9IdWlxYlVEdm43b3JLb3RnU1VRYjZoM0E3SWFMYzdVUGRCa3RwRnROcG5iK0tHMHFPMEVSd0VOa09xTm1aTW9wakd3d2FDU1NaSlBLREpJZHBpWkJSa3VONFFQcDZtdmd3WlFNSHhFZSt5b3hiK2lLclJxMmlTRm00UytmeVdaYWMycFhEOG9aNnM4V1lTeXp1SGVEY0VjSFdaQlAxWDF1ajZuTFA1YjZmTzU4SlgwcGFWbTFLVEhpUE0wTzl2TnV2cVpTUytIeS9WMHpaZ0NPNnkwdHBrN29HQXc2VUVKSlFSM3dTUGlRQnBuY29EZUE1dnoyK2l1dkM2Y2w2czJKb01zOGNvU0tsaFdZNmUwVDNYbWw4bW5TY0N2YWQvaHR2ZFUzYXFkV20xekQ4eHYvRmVxK2g2WEt5Z1RJOUlRV3d3WlFPSEVvSkppRUY5b1FGSlFYSklnb0JKZ2dLN0Z3TkovVlFjMTZwVWpTc3JIRm1iT3NyaW5VSkhadXFEL0FEVXZnZENzS3JLOXBTcnNNaHpHeDlRQ3JLTTF2TWRsclFvbmVGa1czZmYxUVU3c2VDZ3VaQ0N4eENBU1NIQUJBVG9oQmozTzlKd0NEVU9uVWZjc1I5ZnY5eC92QkJ1NFFYS0N0UlFGS0MrUWdBbUk5a0VKbmRBSTUzUVdZQjI0UVVkMEF1N0t4S3FKMkhQWlp5dXZSTHRqMzkvYTRaaDliRTcycTJoWjI3SFZhNzNtQXhqT1NmbjJYVERIYm5ublkrRk9wdlVITWZXbk90UEE4QWJVZmhCcmVEaE5peVljMllOUncvaXVuTm4yK1orRndtRGU3YWpsdm9SVHM4QndlalN6QjFoeFhTd05kcGMyMGZXR3dCN1JLNFQxM1ordm82ekt3MnAwM3EwczBOeExyN2lqN3UzeEszZFd3MmhUZTV6RGNEZDFJdGFaRG10T3k1emN1bHVWckZ5Vmx1cmplVE01NFZsYkFNUHVNSXQ3bTRwMkYxaUxBeTlETkFPa2NIWWJnbnV1K2piejh3NHAwbHhyS3VTMlhtQjE3UjF2YzA3ZkZuMGJjdFBoMHhwY0M4RHphbkJTTTJ2UHBuRXVudVlNZHpiMDF1bjJ1VWNLcVVQRndURU5WTjl4VHJEejZLYnBKWktWWldOMU55amwvUEdWMjlYOGhVZkFhSEVaa3dTbVBOUnE5NmpRTzBwUExWdW5LOE56UG5pNXNxZVZjTnhHN3IydGZ5dHcrZzV6OWVzU0FCenNPeTNmNGMzR3RkODhzN0M4cDlTY0V4RzN4VERNQnhLMnY3WjVmU3VXMERJTUx5NVpYbDlyajhzMUc3ZjB4KzBYSUpHTEdBZi9BSWM4bnZ3dDNPMll6L0ROTlkyWXpTZjAwKzBVV09hNW1MUThGci82dnlEejJWbkpaY2IvQUlidHl2SGp2Ym50ZktPZm5YZGJFcTJDMzdMZ1ZQSHFYQXBPRGc4blZJOTU1V3BsZWJLOXpXVis3bmduRmM2NXZ4eTFwNFJqR0ozTmUxcHVOTnRyVkpFUGJ4STlaV3J5NWNmaUpNSm41cnVHUWNxNFgwdHkzYTUxekJabTl6ZmlyUTdDTU0wNjNNYVRIaUZ2YUZ6dStUNXFiN2ZFZXhlNVR3VEdzZHZjWDZpNHhYeCt3cldUcnFpNjFhNFVyUnhHOU40NERtK2luYlliMjh2RE1ZeU5lWlJ3ckx0cGdIZ1lvTHh0T3h4ZTdvUlJmVGJVbHJudmplUWQxdkhaZkQwTSs1WHdLOTZqWU5obWNzT3RzTndzMmhlTG5DWUxYUEg1bmFlMEJXeG51cnljTndQUFdWTC9BQlRHK2xkK2NheVJaVkNYVWE5WGQ0YTJYTTBHWmhaazgrQzVCeGpMV1UrdGVEM0dPWlJwTXduUGRvelZmNFRBYUs3bS9GcEg3eTFqZTY2cVRKNW5SanExaXVRc1ovb2xtYzFQMlU2b2FUNlZXZFZCNE1TSjdMbHpjY3g5T3N6cjdFbzNGQzZvVXE5QjRmUXJOMU1xTjNCQzN1TzF4aTl1SlZjMUU3UW9CN1NnaDJDQlpLQUprbFdCWm1WUi85VDFyWDhRVEMvTVB2TWlvU3hzN3pLQ24xTlRkOXdpZ0ZOa2dpZDBWbUVGclFHL1ZCWkFEQVNZbEFrblFabVFlSVJLeW1qV0drT2lPeFJES2diM0lPMjZERURHdWZJNFJwazBwYUR0endnT0N5QzA3bEdRVkh2SkU5dVNnY0hGd2JvMzkwRFBNMHk3bFVSOVFRZGU4N05VWnJSODg0VzY3d3lycGFEVnB4VWFXOHkzY0x0d1pkbFk1TVBEcEhTek1uN2R5dGJQck8xM0Z0K0JXZDMxTUMvUVk1ZDJPM3hjOGRWdjdHa2dPbVFxd04zWUJCY0daUUdOMEVtREo0UVdOeHNndGtrRkxmRFVhem5iQ21ZbmdWM2F2YkxudGNSOHdGeDE1VnIzU1hGRGRaYmJaVlNmR3NLajZEZ2VmS2RsMjM0U3VqRGNmUUg5VVpDNW0waEFMU2ZRb01wdTRoQkkzaEFRRW1PNkFtanQzUVFvS0lsQldxTnV4UWFqbjNEbVgyWHNRcEFUK0U1elI3dEdyL0pTaG5UNi9PSlpWdzJxOHpWYlNhMTQvdk0yM1NEYm1uekFyb0ozV0JiU0loQlozQ0NnZ3NGQkQ2K2lBWGV2cWdSYzZoU2NVR3BkT1NIWWJpRGdJSnY3Z2ovYUNEZGdDRUZ5Z0VibUVGaVVCQXdOMEMzR1RzZ3NIYUVFSUlCS0NvSmI3b0JHeFFXL3NVcFZlczhEdXVuSGp0aStIeTU5cTNxWFZvTW85UE1JcmtWSHRGYkZuTVBMVDhOTXg2cjB6V0xuZkxXTXNXOXQwTzZmMHMxM1ZyNC9Vak03Tk9DV1phWHZ0clo0K0tJa0x3L2l6N0wrRnJSOWxsN0psVG81VnovaVYrMDlTcm00Tnl5K3JWdjZ6U3ZSV2hsTU5PNEFITXJ0eVl6TEh0dnFlaDBERWJQSGNyWm95Sm1MRjd1dG03RXI2bFZwREM0YVRUZFdwdGNhOUFFeHRFU1Z3NE4zQ1crMmE4REdjZnk0MDlROFN4M0VyekttTTNFMjlEQWFWYndhbFJ6S1k4T29XdEJCTHlmTkhaYnlScnc2aTV1emJsUEwrWDhsNUVlNmxoZGEzclhkWjFMeVZYVzVEdktYQWZHUkpUQks5dSt2K29seG52RE03NWg2YVZUaDlsWnZ0SzFuVDAxUTdYdUhGb08vNkxWV05QNmVabXRzQzZoMXFOMHo3aGdtZHJtNXNjVXkxV1lhUnRROG5ROHRlQURNOHJEV1hwb3pmdTNTSHJQVU4zU05Xd3dXK2U1ckJ1WFc3MnkwajZQQ3ZKNmRNUFQ2Sy8wck1oUGVHZnN5OWErbzVyS2JIMDJ0OHhIQWs4OTFuaG5pbVQzTVQ2ODRUZzFoKzFNVnkxaU5yaHV4YmR1WXcwbkIzQkRta2hhMHd4c1ArMFhsWEZyR3BpRmhnOTVYczZSaXBWYUtRQS9Wd1YwUEx1ZnRVZE82QnEwSzFuY3lHd2FaWXdqa3lEQlhYZ3gxdGprdnA4N2RPTUlzOC84QVZvT2RSTGNNcVhOVy9xVVR5S0ZOeHFEL0FOMXo1WkxYZkR4aTZCaWZWc3N6L1d6RGdscy9HN3lrOStHVU1FTkx4S0ZPMHBtQTlzVERpUW5kMitISGUxNEhtTHJEWTJHUDByZkpsU3RSeHlwVXJCejZlMUh4UkVCcEcreXplUnFGNDMxWnYyNVp3L0p1WjhwUHdxaTE5S2xpRjg2aVdnTmE0YW5NSUd4aE1jOXJYUWJLNXl0U3pGaCtKZE42UXpMZFZiWjdMK3dxMVBFMFVJRUVGM0RoM0NXcG9uQ2JMTk5QQnN6WTdsdStwNE01OVNvKzZ5M2NOYVcwV3RIbU8rNEpDMXhlYzR6ZlRUOFR5bGxyTGVTclRxcGtuR1B1MllxUkZlOGFLb1BpMVhPODdOQU8yNjQ4dmpQd3pIajU1d3ZET3EyVUtmVWJMdEp0SE1tSE1ETWNzcVd4Y0FKMXdPNjlXdDQrWFNOOCt6aDFPZGpGa2NvNHhWL3JWbzMrb3VlZDNEMDM3cjU5M0hiSExiNkREWGtpUkU4K3k2NFhaa29EZmxkYXpGdUNpZ2R1SlFMOVVBRXdWWUpIZFVmLzFmVW8xaXgwYWRsK1lmZVpZSXFzT3JiZnVnb3NBWVIraUtVd25XMEhzaXN5WEVFRG5zRUM2aExtYUhIZnNFQzJGdXB0TWozbEVyUE9oZ0VJaXdHZ2s4eU52UkJpdUFMK2RQeVJwbE4xRnNnN2VxQWk2V3lVWkFKZVpPN1VHU3pRM1NPRUQ0WTQrb1ZHUFYwZ2tSeHdzMzNHTXZjWXQvYU11YVpZT1hOSUI3VDdxOGwvSjN1dE5WNmEzN3N0Wnp2TUN1RHBzOFEvRXQyY044UnU1WDMrbHpsNDliOHZoOCtObDIraGFMcGFTMGJFN0wwNnJ5bWdUeXBQSXVRZ3BwS0FuY0lJdzlpZ01PRE52VlpyVVl0OE5kSjVJa0FScDlaUWNteWk4NWY2ZzRwZ3I5cmE4L3JGQnZZenpDWXBYWDZSMm5nY1F0SWFBVC94VFlvTU1mVkF3YmNJS2VUT3lBNUlJUHNnakhjb0NsQlk3b0JqZEI1dUxVQmNXMWFsRWg3UzM5UkNsR2tkS0xnMGJYRWNIY2Z4TEs2ZXcrelNaYitxUWRJRGpJQzJEQk1ySXM3SUNsQkVFUVU0N2V5Q2daQVFKdkRGRi95UWFkMDFkcXd5OWovcjF6L3ZCQnZmWkJYelFVVER0a0ZBbVVCOWtDVElLQ3dkdDBFRGlUSFpBUU84ZGtBOTBCRWtRVzg5bEw2V1BJekpqZHJsbkFyL0FCMitPbTN3K2srcVFkZ1MwRWo5VHN2VjA4M0hIa3ZsOE9kTmNKcTlWdXFOM2orWW5GK0VXdFNyakdLVktuQXAwekxLWko0Ynd2TnpaM0xMV1BtL296TCticEdRdW9tQzVnNnZZdm5ET0ZNVU11VWJkMW5sdDl5eWJTbUtUOUlhd2thZFRnTmwxNXBqcjd1WHpQSjU5NmVoWk82VzNGajFBdnNhdy93c1Z4SzZyVnNOd3k0cE9aV3EwdEVVNmxHbjJKTzRoSnJsdzdiNG1QMVRkM3I2dGF4bTFibFhJV1M4NjVieHE1eExxSzJwVG9XbHRWcW1yVkZONDB1bytDNG1OT3pUQVV5eWsvUW5uMHljV3c3TE9TYWp1b0hXZW96SGVvT0phS2x0Z0ZPTkZIYVdCNDl1NmRtVTgyYWlYS1M2clFjZCswYjFGeHFxYlBBWFU4SXNpNDA2R0gyTkVBdGJ3TndGdkxYMGF1Tm4wZWRSNm5kY2NFL3I5eGVZcFRvTjVxWE5JaWlBZUNTUXVOc1NONndYcmprWE9Jc25kV2NHYTdHY1BxTnFXdU0yTFExejMwakk4VHZ1cmpHcjZjcTZnNTJ0YzE5U2J6T1Z0Yi8xRjlkbFNoUWZFK0hiNldnT0I5UUZlYlhiNGRjUFQ2eHk3akhUN1B1RFVzVmJnK0h1czJVcVl4R2k2aXdWV09mRFM1cEFCOHBINkx6Y2RzMmw5bVhuU1BDYnZHN2ZMdDdpbHpXeVN5bWIrMHdNdUJwRitvZm0rSWlETVNyM05kcmFLdlRmcG5aTkZ1N0NxTnJSZER0Rk1sclNEc05ROUN0ZVU3WEs4MmRPT21kNW0rendVNGZScEMrdVRSbWk0ZzdVdFhyNnd2VHgyeVBMbjVyaHZUek1tSGRLK3FGNnpHYWIvd0JtVTZsellYRGh2VUZKNUxSQitTNDVUTHUzcDZabE8zVGNzVzZ4NUd5TGIxTEhwUmhEWFgxeTV4cTR0ZE5Ebnk0eVluMGxkdTNHKzdISEhHeWVXbzF1dFBXYTVIM3RsNWR0dHVXUHAwanAvZ0ZtNFlmbkhTUzE3R0RmYVJ6R1Myd3p4aDl0anVFSHkxbVZxVFJWZzdTTnBsWXVFeDlOTnp3ZkRNTDgvVWpvVmNOR0kwbWwrSlpickhkMzVuQm81MmhZcG94anNKemxrakZNOFloaU5TeHpUVXVDN0VyTmxUUk9ndzZpYVhLdUZ1TjJ6WjRPeG13NlM0WGU1Vnh1enF0cDJOZXEybGlXSEZ4ZlREdE1ueEtmYmRkY2UzTEx6WExIejVlWG1iTXVWTWg5VkxXNnl0VW8xc3VZdlJETVdzN2IvbTdtdjh2eW1DdW5OZGVuVDZiK2pSODVZWFg2V2RSclhIY0VkL3FhK2V5OHNYTTQ4TjVrdGtlaXh5Y2N5bnkrV2VPNjgzdyt5OHM0elF6RGdkcGk5cy9VeTZZMXpqTWdHQlA4VjQrTDV0OXZuVDFjc3NtM3B6djZCZDk3YzU2UnprVUU3YjhJS01JRkZXQVpKTUtqLzliMXFUV2tTN3N2ekw3eDJyMDRRVTk1TFlIS2lzZHI5RlQ4U1VWbjI3dlBxQW5aQktvMUV1aUlRWTRjZGNnU1VTczFsVFVBeW9JbEVHNkQ4SjQzUUkxTjErWkdtUTEzazh2d29ERFd1SEtNcUFMUEkzdWdZd0dScTVRWkRYYU5vNTNWQ0sxVFVSdENsV0cwMjZxWkVUSVZTdWM1MnAxY0x2TFBNTm9JcVdkVnI2aC91QStiK0M5WFRYWEpLOGZVemVGanYrV3NWbzR0aE5wZjBYYW1WNmJYai8xQmZvcmU3RjhqdDA5clV1UEg0Mm92SzVxQ3R2eW9MbVJ2eU9VQU5Na2xBMkFlZTI2QWFnYlVaOC84bG1qam1mR1B3TE5lQzVpWnRUOFVVYXgvKzI4N3E0WHlPdFdsWmo2WWNEcVk4U0Q3bmY4QWt0VVplb3h0d3NhOGdnNWFGdFFSQmJYZGp3Z0o0aUMzNm9KTWtJQ0xvK3FDa0NMaHV1bTVvN29PWjVXcURDK291TVllUkZLOHBDNXBqMWN3NlQvTkIxVHltSGVvUVcwZTZBL2licVFVZ2lDNWdJQm5VQ0VFWjZlaUJGOFlvUFBzZjVJTk02WHZuREw0Zjl1dWY5NElOK2J3Z3B6a0FvQ1FSQUxrQzBGamdvRFlncnVndFZZNEQ5ckROajhJeU5hNERiMU5OMWpOd0E0ZXRHbjVuZnpDOW5UWHQyOHVWK1p5SEJhTmZKUFFLdGVXakNjdzU4dkRaV3daL2JmZEtYQWIzODNtQzhmU2ZMMUZ5WGxuaHZWcm1IQ0tYU3JDZW43OHMzVlhObUcvZHE5OWd3dHdYaGxDb0tocjYvN3cyV2UzVnVYK0x3NjkyNTIvNGZMMXNUenBiNGpqV0VkWHJYTFZTNnlOZ2xwVnNycTdleHJicHRRdWh4MEhsbElpSlc1UDVQNi82T2R2OC84QVQvVnAyRDRwZ09GTXpGOW9MRjdJMDdhN3J1bzVQd210OExxeEVlSzJuMjNFbGI2ZkQ3L2xzeTlSalA4QWg0K1Bia3VYc3ZabTYxNXN2OFh4YThOR3hhNDNPTVlyV1A0TnRSNTBzSjJCaE9yNmk1NWZkWStvNjhYQk9TZDJYdHRsejFPeWprTnd5NTBtd0tqZlhMVDRMOGJ2V0N0VnJQSDUyTk05K0ZlVENZU1Nma1RsdWU1ZjVYUTZPTTlUYkMxdy9FK29sSmwva0RIYlIzN1kxVW1VbldYaWVSdXgzQkhJWG03ZkxMa21PZlo2emZUdnpkWldiVHhyTGx5N3hMTEVhRlpvYWFEekxHMVcvbGMzdXUrL0RYMElxZEk2bVhjMFpTd0RFTCtsZTQ1aWQyMzcvWTI3aFVGRm1zTzNJOWhDODJVZE1icU8wNXR5OWlkbG5YTU5ISkZyYjByUzF0TFVYdUh0MkxtRnJ2RU5NRDh4QVhYSDhtTXZ6YkRoUFZQRE1YT0czT3NOcllkVUx5MnA1YWxGdTFFMG5qMGpkZFp3eU9mM3RySXpoMUR3b1lWYzRwWGp3WDBxN0xhaTEzbmNhUUJiSDFYU2FodTF4dWpZNWxxWXRoR2M4WHJ2dDZMTHB0V2phTytOckhBQUUvTU9DODNKblpWd2VObkhwVml1Yjg5NDZ6QkxtaCswbk1iZk50S3p4VDhacDJocFBjd3Uvd0I1ZTF5azFrOG5LdlFmTjc4VXAxOHoyMzdId08yY0sxN2QzRHdJWlRNNkd6NnJ5Y2VIZGR2YnlaZUhSTUc2cTRybUxxQlJ5WGsramFzeXpRSW9VWFZMZHJ3UXdhUzl3amVUN3B5Y1hsaml5OFBWNmpaWHlSYzNyTUN6L1JzOEx4aTYyc3NjdzhpbUE3c2FsSWNCZXE4Znl4bVhkcmlsM2FaeDZINXdvWFRMZ3Z0bk9GUzJ2S1AvQURlN29INWJTUXVldE5PcllwL1I2NnVzRDYwNExSUDdHZlVaVHpQaGxIZW5UcVREcWhad3M1WHVtaTVkdmxzbExIc2wzV2FiL005N2hCR1RjVnMyMmxIRkt0TWVDS3c1T25zWGNTcmx4L05MUFRHdXo1UDhYL0xWY09yZExidklHWjdUUXo5cnRxVjNXalhzUDNnNy9obW4zaGRQNXYwTmEvaHZGZTQ5UmVpdFJ0VUYrTjVXZkRKRVZSUmI2cnAwL3dBbWR3L056NTUzWVNPaC9aZ3phN0VjQnVzdlhOU2E5cTRPcE5kMnBubGZPNGI5eG5sai9pMy9BTHZvY3VmZmhqUHkwNytTU1pQUHN1OG1wcHh2c0o1VlFMdUVBSGhBTXdTckFEZ09TWVZILzlmMlEzWURzdnpMN3hqUUFkeHNncW82bTF3N1NvcExvZUlHNWxGWkZPcTVyUTJQcWdKenpHMjg4b0FwL0ZKMko0ajFSS3pYTmxqUTRRNGQwUUhpTkd3QWxBa1E5Nk5NaHBBRFdEOUVCT1k1amlCd1VaSmE2cDRnQTNnOG9QUVo1aHFQS0N0WmFTQ0ZRTG5OSkVyTldHMFRMU0FkK3lxVjVHWnNNcDNtSFZxTHdDSE1JSVBjbGI3dTN5NVpZOTNnWFJURzZ2M083eTdjT2k0d3lwREE0N21rNzA5Z3YwblQzdXdmSzVzZE95VTNPa0dKQlU5VjU0WkFWRTBhaklNQkJZWnBKazhvRlZHdVk2VzhlaUJ6RHFiSjU0UVhvRUVmb3MwYUQxT3dZWWpsMjdBSDQxQWVOUlBlV2I3TE11aG5kUDhBRTNZcGxuRDdtb1pxRnNWZTUxTkVicnBCdU93YkhxdGFCZ0NGa1J2S0NPNVFFTktBdXlCWVFFRDY3b0NCQ0JkUWFpQURIZEJ5ek1iamczVUhBY1JkQXBYRTI5UncyblZPeFFkVXB1bUk0aitaUU5RWHFBRWR2UkJZNFFVZ3VKQ0FDRE95QmpSdDdvTVcrM3RxbnkveVFhWDB3LzhBMnU5SS93Q3ZYUDhBdkJCdi9aQUwyNjI3R0NnakFUeWd0Qkd6dlBDQVhHVUFOUUVleUFnZ0NkMEZsQjhXZmEweE9yaWZVUERjQnBFdUZqYTAyc1lOd0t0eWRQSHJ3dS9MZXpDUEhiOHpZYzYyZU0wTTk5T2NvWld3MzlwdXl0aDF2aUZXd2I1R3VMdzExU2RYQkJDNFg1Y085Nk9TZUd6MGVxR0kyV1BZNzFPdWNvM2RQTEZ4Yk13MWxkb1liaW5jMGpwaDdmM1M3dXRiM2pKK1YyNXp4YmY4VTAxM0hiN3FMbC9LTmhrVEVNTHRSWjU5dXExRzBxVTNrMUxXbmQxUEVmUmMzMTBsZEorUHYvVFRPL2s3UDEyNTM5b0hHQlV6SGcvVGJMNC8xWmxxalNzYUZObkQ3dDdRTndPVHZ1dDVYN25pM1BkT1A1dVc1WmZoVHFoaVZIcHpsSERPa1dBdkZLK2ZTWmVacXVHZVYxU3ZYR3NVWkhJSEVMbjBIRnEzazVQcTZjM0pkNnc5T0oybDNYc0xtamZXcnpTdTZMZzZqVnAvR3g0OUI2QmNzOHJsbmI5UG8zMnlSM2pxUDlvUzF6eGtsMlRLdHBVZFVkUXRpL0VaMGwxMVFBMWx3SGFGMW1MbTQxaCthOHlZZlJmWTRkaWwxYjJsVTZxdEduVWMxcmc3YUFBVmpQeEhURjlPOU1PanRQQnN5NVp6MWRZaFd1WFh0czY4cTJ0d0Q0MUtveGdMaVovTDVsaVRhVzZkRXliaW1CNG4xT3pFOXR4cHhHbThVcVZKMnhxdERaMjlRSjRXYmRWcVRieCtwUDJlYlhNdUkxc2Z5bGlMc0Z4SzdjQmVVNGkzcWVwQUhCVzdsVzVoSXZJZlJQTEZLMlkvTU54Y1kzaU5pNXdOU284aWkyb0R2NU9Gd3R5ZEoyeHJIWGZNdUY0SmVWckExV1U3aHJhYmFGdXcrWUF1WVJIcEFDOXVjbmJQMmVQajg1VnpycjB5NnNxdVhzMDRkVnFXN3F0TVVuVnFKMG5aclh0M0grSXJwaDIzRk1wckp5UEVzOVp3eGkyKzU0cGpWMVh0Q0k4S3BWYzRFZTZtR3BYYk9ieGVmaEdPNHRnTmFwY1lQYzFMUzVlM1NLMUtTL2Y1TGp5NStVNHNYb1pyemJpT2NMeWhpR0xPMTNkdmJzb1BxQnhPdlIzUHZLNjNPZHNaazFhNmprUEVxWFUzS04zMDR6QlU4VEdMR2s2NHkvZEV5NldDUlQxSGY2TGxidHNqb2hqWnNNY3hIcDNtQVJodU5OZmExYVR2aFpkQ1dnZ0h2S3ZIckhLVyttYzV1YWI2M0ZzZnVzcTRqMHZzc0MrK1hHWExodjNpb2VEYjBxbXY5U0ZuZ3kxeFpZNWZpdTlHZDc4cGwrU1ZNOVpCYm5iQXNjWmdyNmVHc3RqYVhqL0IvQ1pjQVJCTVJNcTQvd0J4MjM4VzRXN3o3MlBsTEZMRTlWY2V3dWpZUHNzRHpMUmN5bGIxVzZHMUE1a2FtampsVGx5MVp5SmhPN0t4clBSNjRxWlE2czE4RWNTMms2dlZ0aUNZOG9KRFpXT3U0L3c1ejlEcGMrL0t6OG4yVVhBYkpMdWJkcjdDWEtvRFY3b0JjZlJCUTVWZ0Vxai8wUFg4UncwN0NGK1pmZU1iVW1aMlFCV0RYbHVua2NoUlFVbkJ0U0NKUDhFVmxPZTBDUzM5RUZIY1MzaEF1aHU4bDNiaEVyUFk0UEJEanVlRVFuU0d2TyszZEFEV2tWQ0NkenVFYVpCQWJwZkJRTkUxQTdmZU5rVFJGTWFYYm5kRFRLbzFZcTZYank5aUVRZFdxMlNJK1NESDA2ak80UVpGQXRwU2R5N3NDcUx1VDQxTWdqekVjSGhUS2JtazA1elJ2bjVTNmdXZDZaYlkzY1c5ZDNZbDU1Syt0MGZVWlQ1Zm8rZno0eXZvNnp1RzFLYlhOTXRJR2src2lWOVBreG50ODIzdHJKMWJ3c3FaVGRzUTMrS0E1MURma0lJMHdSSWtuMVFTTnlSc2dzU1NBU3BvZVppMUJ0elJxMFhpUTlwYjlDTjFtd2MwNlgxNnVHNGhqR1hLci9QYVZ6V3B0ZC84dW9kbHFEcllmNGpBWTI3RWVxNmZRRUNZaFlCNmkzYzdsQk5ZZVpDQWtCdElJUUE0YWVFRm1BRUZqVHNRZ3A3V25mZmJkV0RtZlZlM0xNUHM4VGEzL21OeXlxWGVnRGhLemFPaDRWY3R1YktqY0RjVnFiSFNPTkxoSS9pdFR6Qm1GeENndHU0bEFRTzBJQmNZaEFiWEhTZ3NCQlV3U0ZZTVcrZEZ0Vi93bEtOTDZYSC9BRlZleC8xNjUvM2dzN0hRT3lvR04rVUJnQnZDQU9UdWdoTU5KQ0FleUNBUWdoMytpQzVRVUlPNkNFN2dJUGhicVlYWTM5bzZwYnVNaHVKV2RMMjBzOFBiK0M2OVY2eHgvWjV1TENaN3QranB0dmNaNGQxNHp6ak9UcUZ2Y0hCN00yZFpsMjdTSE1EQTRCZ08rL3FzNTQ5MnVMNmY3dGQ5eTQ3ZnllS09vT2VMdnByWTVLeExMMWEzeEROVjU5MHd6R0NQNnE0MTYyOGYzbTlwV2NKNXl2NkxmT09NL1ZzZEt0bSs2Nno0ZmdtZGE5dFh0TW1ZWlZ4RzNyMjlNMG1QcUdqNUM5cEo4d0lpVnZqdTlZZlN6Ym5sN3VYMWwwNGYweHBOemoxa3VNZHhJZU5hV2RhN3htNTFjZmd5NEhmdHdweTI1OG5iZm8xbmpQdTVpOGpDOHU0ejFvNmozVHFRcVZhR0lYbFY5ZStBbHR1enpGaGNmVFlBSno4bVdVbUxYVDRUR1BDei93QlBNd2ROOFNiaCtQZUcyclZhK3JUYlNlQ1hVZzZBNGp0cVcrUEh1eDgvUTd2TmV2MHk2Ulk3MU1lNDRWV3BNc2JXcHB2V3VjRzFHTUkxVEIzTW5ZTHozbXlsMUhUc21tcVpqd0xFOG9ZL2M0VGlOQ3JiVjdTcnJ0eFdacExxYlhlVnpmMFhYTEhlTzZ6THF2cVQ3UE9iY3g1a3dyRmNSekplT3ZYV3JQdWVIVkhBQXNhOXN1R3c0Z05YRERLNzAzbmpOYmJSYVpjdGNZdzZ2ZTROVlpoMmEyMzFhdllZcTBSK0kzUXdNY2YzZDEwem1tT1BKN3VYZXE5eGFNL1pPZDdGOXBqbEJzT3IyelRVcFhMbW1OYlFkZ1N1bU9xdGFmajNWVEVyRmxmRE1pWVpVcllsZlZxaHI0aGNnc3AwUTR6cWplWUJXN0k1VnFkeDBxcFkxUnE0bG1DOGZpbVpicGdlNnU0eTF0YUtoSVovZDhvWG14M3lXeS9SdTR6ajh6NnN2RU1Bc3MyNUJ3Rm1LTkJaYkNqNHNIY2VHWDAzZnhoTXZrOFJxWXpMelhIK3JuU1dua0MydzdGOE5ydXE0VmlEbjBIVTNuVVdYTFJxL1FoZGVMRzN5eGx5YW1tcmRPY050OFJ6ZGg5SzR2S05sYlU2elgxWDNHN0gwMm5kaEh1RjR1b3VVeWUzZ3hseDI2LzlvUHA5bHJMV0VXdHpsQ2hiZmpQTnpmbGp3YWtQQWMzU096U3ZwNGNNdkh1dkIzMjhtdm80WGxURzYrWHN3MkdOV2p6VEZ0WFpVZC9oN2dqMFhEMWpiSG8xODBqb25WYTNiZ1BVTEQ4MFlYK0RaWXV5M3hHZ1c4QjdvMWtmVlhEQ2MzQmxjdmMvSTVQazVaalBWZGN4SEVjeDJlZXNGeGpLUXB2Zm5IRFdOcnRxN1VuVm1EU1hIM1hsN3J5VGo1Yjc3dGVQV294MjltTnhuMWVmaVdZN0hMV1NzVnlkajJINnN5MkYwMnU4VXFPcWpVZVhhZ0E2SkM5K3BlWHYrdXJQMFluNGUzNlBPem5uU3BpMmFzajRzM0M2MkdHM0RBK3RVYUF4N1huWnJJalpjYnh6TGd1RjlZbU9YWnlUWDh6WHM2Tk9DOWJHWHROcHBpNXEwTGtBYkQ4U0pYVGt5N3VsOC9TNlo2Zkg3dnFMalBySDJMYjFHMTZGS3FPSHNhNGZVTHg4VitTUFhuTlpXR0NJWGRnRG9DdWhYWk5DNWhBQmRHNVFmLy9SOWdRYWZ1RitaZmVINGJYTkRuSHpBYklNYXR0VTFBNzkxRk1vUk14SlJXVWE3U1BEYzNsQkNXdGtqY0hzRUFVb0pQdWlVOHRHeEJoRVI5UGlUeWdRM2NrNzZtOEZHbWZTcWE2UWtRUjZvQkJiVEpKa0FvRWx3Yy95bmtvTWdNL0VCSmdJbFhVbHNtWjNnSWdIT2UxNEJFL0pCa01FT0QrM2NLaDVlMXpYRmdCSWpZcEVhUG4vQUFmNzNZZUt5R3ZwelVhZTRjM2NMZkJuck53NU1OeDB6cFpqMzlJTXIyTnpVZE56UVo0VllkeFVidHY5RitrbDdvK1B5elZiMURRWmhSbFh3R0IzUUdOb0pQelFHLzRnUndnQndrb0NZTklPNkJWdzBQcHVqbUlIMTJVbzVCZmc1ZDZrV3Q5R20zeE5ob1ZqMkxtL0NrSFlLVlFHbkFQbG1RQXVnTW1UQ3dEbUJBL2lnZ1lCdUVGNmhCSGRCS0xqdUQrWHVnYTdjSUZrZ3dDZ01EWUFGQmNrT2oxMldvTlh6ellOeEhMMkkwTk1sMU54WVBjQmM4dll4K20ySkhFTXEyRDNHWHNZS0w1NW1rZEszajZHNGtTb0NhTm9RV1BSQlQvNUlJMG9HU09VQU9NbGFneEwvYTJxay91bitTbEdsOUt0OEp2di9IWEg4WEJZbnNkQ25aYUZBeWd0QlJJN0lLSThzZXFDUnNncEJKUVVUMlFVSkNDdVNKNEpBL2lnK0djY09yN1RiMlAzSDdhcE5QdU5MWS9rdXZWZThmNk9QVCtzdjZ0NnRjS3o1aXZWbnFvL0ptSlVjUHFBT3AzTGE3ZkVOZVdBQnJmM1k3bFRMKzlqRS91cWZZNWx6RG16TE9SY2c0M2hGeGdWT3RlVW1Nekp0NFpxV3J5NFBvejhMbkVRczRmei9zMzlNZjNWYllmZjRCbS9yQmNZbmlOVEZibXl3YWsybGQxeitJR1ZCc3ovQU5Nd1ZyaS9Iai9sYzh2V1grWnlib3M0V1dYdW9tT2drMTdQQm5VcWJ4eVBIZnBJV2IvZTExei9BQXRXNlRkUTdycHhtZWpqQWZWZGhrSDczWlV6dFhocERBNEgwSlhYUEh5MXhlbGRWT29ydXBlTlVNZXU4UFphWHJLZmdWeFRjWE1lR2tsaEU4RUJhenZiSExYbDYvU0hxODdwYmNYSm80ZXk5L2FOVm4zeTRxRWg0cE01YTBjU095ODh4OHQ3YXBuM04xNW5QTStJWTVkVjZseGIxS3RRMm9xbnpVNkRuRXRZZmtDdlJuWjJhVFQ2VzZGVUtHRGRQck4xV0JjWWlhdFlqMUwzc1l6K0RTdkJoK0o2TS93dG02YVl2VHY4TXFFMUc2YUdKM1Z1NW8zQWNLby95Qzc4cmh4T3E0bGhGbmlPQXVvdXRxZDdYRFMybTk0bU41MlhMREowc2MweEd3RnNhbEdoYmkydUdNME8walQ1M05BL3lYVExKenNadGhSZGNPZTFwSmZUWnFCaUNYdllBMC9xWExyd1E1cjRqU2NuRzN1Y01abHZ4dGQxYVlsYzBicWw2VzlPb0t4ZDhwQzQ4czNrMXgzNVdwZmFieDYxcDRMZ21WMmY4NnExM1g5VWZtWXh6U0dmVWhlL2h5bUhINWVMUGR6MCthUlR1YWJLZDIxcm1Na2loWGpaenp0QVBxRjRzOHBuZHZvWVpkczA5SEdjZnhYTWQ3VHFYMWQ3NnZoMDdjZVl0YitINVFJNFhUazVmazA1NFk3emxMdjhDeGJESzFTbmlGbFZvdnBnR29YTk1CcmhMVEkyZzlseHg4OGRyV2QxeVIxRHFBOTJKOUxNbDQyNyszdHZFc1h2UG9OMmhYcGIvQnloelgrUGovVjBYRERpZU01RzZlNGpoTlVVY1ZzcjAydEdvN2RvYTdrT1hEQ2E0cCtsdFhrYmZpT05ZZmt5OHgreXp4V3BYV0tZdFNGelF1elNEZzhnRm9wNmZ5N3IyNytiSDladHhqbU9mODQzbU5aWXlqUXZNR3EyUm8xMkNoZFJEWHRZZHRKRy93Q3FZLzNlZjdKZjd6RDkyRDFoOHVmc0V1dFhucldscStmY0F5czVmK1YvcXVIL0FKditqNjJ3T29hbURXRHp5NjNway83SVhrNHZ3eDYrWDhWWnNtVjZJNXFlWjRXZ0dyc29KS0JibkE3SVAvL1M5V2s0bnlyOHkrOEtvU0FRT1IyUVl4Sk1Tb3A5TnJnQTVxS3lEQmJMK1VBQWhvY1IzUVhSY2RVRVFFUmt1WVlrYytpSVJWckZ3amd0N0lwbHVkeEk1NVJXUzVrZVdZUUxxYTRHcmNIaEFEQXllSWNneW1TK0IzQ0lGekNISHVKbEVIckJacUc1SE1JRG91YTRiOEtoeldoZ2M3Z25pVmNmYVZoWWxiL2U2RDZid05Pa2dUd1Y1cGJNMnRialdlbEdLbkwrYjhReTFYY1JiNGc0MTdZY0NSeUF2MVhCbkxqN2ZFNmpHN2ZRREtnTzU3N0JkSG4ydHhuZnVnbXAybEE0R1FDVUFubEFRN0FJSTVvSWc5NGxTam1YVm13ZTdDNmVNMFcvalliVmJXMjUwdEtRYnJsekVHNG5oTnJkTmcrTFNwdkxodnVSdXQvUWV1UUprZWl5Q2FEK1pCZXJhRUFvQ0JnR09VRG84b2hBbDVNYmNoQmJYdWdTRURBRElkQ3NHSGlWSHhyV3JUN3VEaCtvV01oejdwWFdOcWNZd1IyeHNydDdtVCs3VTNXOGZRNmMwbnVvTGFTZ2dkdWd0MjRsQUxTZUVCaDNaQlpIZGFnd01RZC9WcW83YVhmeUtsR21kS1hUaEY5SC9YcS84MWlleDBJRXh1dENIYmhCSktvaUNwTXgyUUYyUUNGQkR5RUZkMEVRQ1B5L1AvTlVmQzNVbWNFKzBmV3VuQXRhTVZ0S3ZwTFhlSEpINnJwMUhtNDZjZUR4TXQvcTZOK3dNK1h2WExQbHRrdkZLZUZPdXJVVnE5U3EzVzJyUnF0RWZJKzZtWDk3S3hKL0RzVmE0OWp1Y2NIeVhrakhjS3VNTHRMTEZmdVYxbU5oaWw0MW5Pbnd2UXZLempOZC93Q3pmMHgvZGpQeSsvTE5UcmJnenIycGlIK3JLVlp0eldkcnI2YWtPM1B0d1ZyaS9Iai9BSlhQTDFsL21jczZWdThUcDUxUHBORU9maGxGMjMvZThLK1B2TFhYUDhMamhMU0FUOEpFcWN0djBhNC9UMTh1Wlp4ak5kNi9EY0RvbXZlMDZUcTRwQStaekdjaG83bGNzODk2YXd4OTdLeDNBY1l5MWRzc2NldEgyZDA5Z3JlQTg2WGhqdGdYRHNWMXZpTWVOKzIrNEQwcHE0emtHK3h0N0tyY3dWQzJ0aE51M2NWN1JrK0s5bzc2UXZQamxiazZaU2FkK3dXMmJodUE0TGhvYkRyR3pzM3VqOTRqeERQKzJGTUplNWM3TzE3R0FXR0Y0UzZqZFlPTlZqaVIrL3VZTng5NVk5N2F2ODEyNVBMangrM1pzUHJQcldkS29HZUVZQjBCYzhJNzFwR2M2UGhZZ3lvSTFYTG11TWR0TzM4MTF5am5YalcxelZzcUZXc3d4VVk5cnBJL0pxT2tmUjJwWGh1OXVYTDVuaHhqcGJqZHRUekRuUEY2encrdFV2SE5vQ2VTSGx6d1Bad2JDM3FkM2t3dmh4ek9tUDNlT2RRYnZFOHlXejYxTnQwR1ZiTngwRmxBR0dOYUQ3RmE2bWFtbzF4WVM1YnJ2K2ZNaDlOTExwSGFZaTJsV1pUc2g5N3BVYVRnYXJIMTJ3TllHOEJjK200dnpYcU5UMCtYc0FyNFJiWXZiWEdNMDMxTUlGUVByTWFaZVdBN1I3cmp6NGEzcDE0YlBHMzBuMWt6VGtUR3VuOWxZWVZlZmNzVHVMYWxjVXhvQnFWR1V0aFRxSGtHQ3ZUeFNUaTh1WExOOGtzYzV4V0tuUVREbnUzZlR4UnhEVDhMUm9QQjkxeDRKL0R5bjVISmQ4K1A5VzQ1VXBZbmRkSXNIcDRkWDhDL2JpOVA3dldINVM4aUN1YzEyWmZzM3lmVDlXejFiMXVXc3g0dlE2cTNGTEVMMjhzUSt5dWpUMUNHeUF3ZWhYYitiaS95T01hZG4vTm1LWXhnZVZzSnI0TSt3czZkZHI3VzVJOHRTbVBMRSsvSzFMSng1Ny9KTE44dUdNOTdlZDFpZTErZXNEdFFONlZqYWoxM0E0Vnl4djhBWmRhK3UvNmZtdUhucXQvVFd2NnZybkFoR0MyRzBmMWVudC82UXZGdytjSlk5ZkwrT3M3YVBkZWlPWVFkakswRk9Qb29LMUlGUGRCUWYvL1Q5STdCcm1sZm1YM2s4VWwzQ0JYaU5EenE3OElySll4d0FjRHQ2SXJJZWRiUnRFS0RITG84dm9nYlNhU1pCZ3dnY3cxSEFoenZrZ0J6UXd5NFRQZEFiSi9LcXl5WE9sdjk1QXFwVUlhTlg1RkdrYTFwY0RQS0I3ZzVtN0hCVVNYbmNPSEc0UURUTGhzNFFENklsWkRRd2ZEODBRYjJQZTBFSzR6ZFp5OUJjREhobmtlcXhsajVieDlPYlp5b1ZzR3hlenpCYlN5cloxUVhPYis2VHY4QXdYdTRPVFZqdzgyTzMwSmw3RTZPSzRiYlgxRTY2ZHhUWTRIMGNWOSs0UGs2MDlqU3VWOEtuQ0EyN3RqMFFVRHZDQXhBRWt3Z3VRUVNEUHF0UWVKbWpENmVKWVBkV1RoUGpVaTBqNkxsUnB2U1hFS2o4RXJZWFhQOVp3MnM2aTRmM1FkbDFub2RKYVpic3NpNWRxYWdJOG9MSGRBSW1ka0RHa2d3VUVkRWJJSURBQ0EyMUoyUUJYRXNKNTJsQnl6Qmk3Q2VwMTlhVHBvWWpRRlpvL3ZqbEIxWFZzMUF6c2dvTjNDQXlJRUlGazZUS0FtK3FDeStRVnFEQXhIYTFySCs2NytSVW8wcnBLNy9BRlJmZitPci93QTFqNmpvcGN0Q3RTQzlTMElnaUN3Z0hzZm1wUk95Z2hNSUsxU2dvYlNTdFkrMHI0cisxZmh0VENPcFdHNWdwdDBzdmJhblYxZi9BSExkMC84QUJkYXcyN0g2ZWFzVDZrWlB4dkpXSWpETHJPR0MwcU56ZG5kaEZ1MEYyM3F1VlI1dU5aanhyRE10anBIbU8wdVhWOE94dWlML0FEUGJrK0MyMmZWRlZ0VnJnUEs4K3EzbC9OK3NZYkhUeVphWld6dm5YQUxHK2ZpTkhNZVczWGpYMTZuaTFEVXBUcUJQY2JLWWU4ZjB4ME9KZERCOTZPZHN2dU12eEhBNjdtczlhdHVTOC9wS24xYitqamJ0VFlCSEd4WFNtUHQxcm9EbjdBOGo1eHAzR01XVk4vMzB0b014R280dCs2dEozZEhjSHV2TGNkNVI2TCtHc0xyZG5mQWMrNXNxWTNnTnJWdDZqR20zcnVlL1V5cjRSaGxWb1BFanN2Wnl6NVhnNDU4enFHUThRdzZqbC9MREdIRXE3bTJ6NlRxRk5zTVpUclBMYXVsM2NQMkI5RjVzWTlHYlo4SWRmV1ZHK3RNVmVEY1dsUzUwMHk0TzhPM2JvOE5yaU83V2lGcG1OanU3cXh3U3BodEd6SWZiYVRXb3dkdEZjTmQvdktYMDNIWmNMdXFkM1lVYXJDSmMxbW9ON2JMbHh1dGVCanpHM2VLVTZMZ0N5aXp6dS92Ty93RGJkZW11VmEyKzBEN0c1cU5HbHZpRUVja2tnOGY3UktjSHkycGNYeTdnMUc1eTkxT3VjR3NMVTNyQmRpN1piMUhsbXZUNXVJNElsWjVNdm0yeE1YaDlhY0t4YWhtdXBtZS90R1lkUXhlcTU5T2pTZHEwMUdBZndXN2U3NW5TM1hob3o4eTQ5Vm8xN2V0ZTFxbEc2YUtkYW01eGN3dGJ3SStTbjNoMmJlV2VZY3lCTWhnT3c3QXlzNTNlR1g3TE1kR1ByMXE1cGZlWHVlYUlMV2FqSURUMlMzZUUvU09uRzYvbUlDMDZHWUJiVkJEcnJFS2xXUFVOYVIvbXAwOCtUa3Y1NmVmL0FQTkc0NWN3M0U3enBOZ0dIWVhVOERFTDNGV20yZjZGcDJLNFkrZU8zODI4L1dQNlpiZTlSeFNqaFdNNC9senFSV2JpdVpLdEJsTERiclFDMS9pQ1dzSG9RVjZ2V1hIK21ERS9tL1hKck9QNG5tYkZjZnlwa2JIOE1iWkN4ZFRmVGVCOFRaai9BQ1hIcUpycGJsL2lqcHhmK2J4eS9LdkZ6MC85dDlaQlpVQnFiYjFLRm8wZktBZjVyM2Ribjkxd1l6L0ZqSi9ySEhwcHZreS96Vy83dnNpMVlLTnJSb2dSNGROakkvd3RBWHplbnc3T1BISDhvOW5MZDUyb1hicnU1aExrQUZ5QlpjZ1U5MnlELzlUMEtmaDZBMmR3dnpMN3l3Uk95Qk5WcmRZZDM5RVZtVXFvTEFBaWpjOTBLQkw5aHR1U2daUkplUUppQnVnYTRRTlEySVFDOTRjQnZ1Z2JSNFZaWkg5aytEdVBWQWlxOW9KN2c5bEdrWTRQZTNUejZJSHVPa2dPNUhQeVZHTldMaFU4VmhodkVJTWhsVU9hMGsvb2lWbTBReHpUNmtJaHJYYkJvUENzdXFsUnpROHorcFMzWTEzTk9IQy90SzFBdEJrSGtUMlRHNnNaeXgyenVpV09PcVliY1lCZE9JdWNQcUVORGp1VzlsK213NWU1OGJsbW5ZTlcwaFhrY01WdE9vU3NUMDFWMDNFT0RUM1ZSZW5TVHYzUVdJNE82QzJzMG1mWHN0UUtyc0R4cElrSFkvSmNzaHlyQUl3SHFQZjRkT20zeFdsNDdHOER4S1ozQTl5dXM5RHF0TWt3WjBpRmtIS0FrQVA4UVJvUEozUU9hQ1BpNTd3Z2dtZGtFQmRKQkd5QmJpQzd5ay9KQTVrUWdoTXRPNkRsV2RIT3duT3VYc1lCQXB1cXV0NmhIcFZFQ1VIVTZaTHdDTjJtQ0Q3SURZNk9VQnRPNkFuRXdnQWlXd2dqVHRDQWpHeTFCZ1lxWXM2eC91Ty9rcFJwSFNSODRWZnovd0Jlci96V0o3SFJaQldoRUZyUWlBaHdnR1NnRjA4cVVXMTIyNmdza0lCa2RrRkdZMldzZmFWODYvYTh5OE1ReWRobVlXTTFWTU11alNyUU54U3JpUDAyWFdzT2ZaZnhERnN3ZEhjdTR2Z1YxOTJ6TGt6RldXemF4TTZMVzdkNGJTNC91eWQxeXFOMnVxK2JjbjNHYWNqWTNaVk0zWW5tcXovYWRyZVdyQVBoWnBMWEE3TkRDZGl0NWZUOWF3eE1Cc2NxNEJtZnA3aldHWXJWdU1UeHkxcllKak50YzFUVnIwalVvN05jeDVPbnpUc3NUK2EvbGREa2VRbXR5YjE3Zmc5MlEyMHEzMTFoVlFEWUNuYzZtc24ySWhhdjRtL281cm5IQnF1WHMwNHhndFZwYlVzN3V0VElQcHJKYi9BaGJwaThHSjUrcTU0KzNvdnBaY2VCczZRZGNURWVvOTF2bno4T1BGajVkNzZhM0YzbUxKMkg0SGFZcTkrSVdsM1dwZnNhZkNjVzFockdpcUlnUzNoY2NMdUp5ZTNRckxBTHpFYnpITHVxeGxsU3VNSGJWRm95cHJleDdBVzFIdk03dUpHNTdyVE1hTG1PeHpuMDV1Y0lvNCs1MklaZXUzMFRZNHcyWWJSY0EvUS8waVlDbDlPa2ZUdUM0dGFNd1d3dDhQck5kZFlyU0ZVMUoyYXdDZHZSY3VOMHE4WHZLYkxXNnBXN2c5OXdCcHEvbkdvNk5qOGd2VFhLdGZmZU9vdW8wWi9EOFFQdVBXQzBSQ3puZTB0Y1d6OWE0alo5YmNHekhoZHNIMjE4YWRyYjBHRUFrTVlROGtIajRndWw0KzdIYk9PVE02clVNQ3Q4S0l6OVVZeTVvc3FPc0xPaHZVTnhvYzFvTC9xREN2RmovQy9xemxmbWFqMEM2WTVienJYdUwyK3ZDNjd0MnVwM0dIUGFOT2x3SWE0SDJYam5uSjZPN1VjejZpWlRzTWw1bHVNQnc3RUJpUDNlZkVjMFFLWWR2cGRQY1R5dmJueDl1TS9Wenh6N3R0ZFpodDJhZEN1Nmk3N3BYZjRWT3FmZ0xtN3hQcXZIeDNmSGwrbTNYamRhNnROZGh1VjhrWmJiNUtsTzFOeldwKzlVN0wwOU5QOEF3dHkvTjVyL0FIMGRLdHN2NGpkNEJrVEFNRnZoaCtKTVpVeElWSGNnQWJHTzY4dkQ1NGNmMXlzZHVUeGNwK1UyeGJqR2JXNXdMRU1LdVdVOFY2bjA4UU5OcjJ0QmMrcFRNTmMwOGhrRGhlbmsvRlAwbW5HZlQ5WnN6QnNaeGpOM1VabUk1bHczOW4zR1hiTno3cW1lZFRHeUhmSlc0ZCtISHhmclhUajlaNS9sR2s5TUtUODI5WFg0cFU4N0hYVHE1STMyRGpDOC93QVh6N3Z1OFovTGxJdncrYnl5L2ExOWxGdzNqNlN1dFdYZmtva2ZWUlNudmpnb0IxQWhBdHprQ251MktELy8xY2kzTG5FdUkzUEsvTXZ2Nk9hMGdFOTBOS0htY0o1UEtLZXdGanRKRzNLQTNWcEVEbFFKZFVEWEFkL2RCa1Vta0NSM1FOYzR5QTRmVkJqT2pVQ1BWQmxzYVdzMUJWazl6d1dnUDVQS0JGUUFHUnVQZFJwVklmaXRjTmdVR2E2ZE9vN2txaER0TGhvamRCVkVNQU1qamxFcktNVTJzZUowdS9taUcwNWd2SGYxVTFzRjRoQkxmeTkvVmFrMGFKdTI2MkU5aUlLcXRCdysvT1ZPb052ZVZKWlk0anBvMWZRUC9LdnA5Smx2MitiMVBISStqYmFxS2xMVUNJTGRRWDFlU2VIek1mRlBhUU9PRnlqb0lqaHdWVFMzRUVTT1VGRGZsRUZNY0xVVkh0TG1FdE1PaFpzTk9VZFI2RlRDc1d3Zk1OS0d2dGJrQ3EvdDRkVFowL3FrdjBOT21XRlJsZTBwMU55Q0E1cFBjSGhhc1JsQUFuMlhPMEZBV2hHaHNFZS9LQW03YklMT3gyUUR1VE0vTkJRZ21SejNRRU4rREE5MEVmRFFXdVBLRG12VmkzZCt4YWVJTUUxTE92VHJBais2ZDBHKzRIZDA3N0RMTzVwazZLdEVPQjl3RnJROUFBRk5BeHNtaEE0bllwb1RncG9ERUZOQ3c0b01ERkgvQU5Ucnp4b2QvSlNqUytrd0g3SXZpTzk3WC9tc1QyT2lOYU9WMDBDSE1Kb1VUQ0N4dUpRV0RzZ0Z4amhCVGpzZ2dBaE5DRUJOQ29qZE5DaVRHM0tlajIxanFGbHlqbTNKdU41ZmRCcVh0dTd3U2VSVVkzVTBqM2tCZE1mTEdjMCtQUHMvMzRvNWl4dnB6aXIvQUFhT1lMV3RadEQ5Z3k4b1RwNTRjQ05sakthWng4dWg1UXVzNVpYdys3Nm9ZL2UxY1p1OEJyVmN1NGxnZFh5MUdXdE9vQTE3SGp2TWZOTGQ2L1M3WWVKbXU4eXBZWmR1ODNZdmhiOEQ2bE14dW5pMW5iMWc0VmpRcVZkUTBBYmFBdzcrNnNuaXo4N3NhaDlvRzJOaG5yQjgvWVFOTnRqOXJhNGpSZXpnMUtNRjI0NzdDVnJrOFR1K3E3cnkrdTF0VHhIRjhHejdZTkgzRE5saFR1WE9HNGJkVVdpblZiSHJJQlRqdmRQTGVMSzZZOUJMN3FSbGU1eHh0eFV3MnRTcnNGQjFabjRWYWhCMWx2dUN2UGhsbGNuWGJrZUkyVExER0xuRDZiL0hwVzljMEcxQkk4VU5kRHZsNkxYUGpsVXhzeDlQckhDT2wyVU1Bd1MwWlF4TjlsYlpxYlErN1hqS2dmWHNNVGEyV0FPRVFIU3NZWTJRc21WYVgxQ3Y3L3BEanRqbFN5dW00bGlONVl0b1l4ZU9jUzRpcFVkNWg5SFN2VG5KTWR4d3c4NWFkVDZqNHBiMHVtMUxMZVB0OGQ1b1VuV3QyUnExZmd5M1dQeXYzV09LVE9YYTgxdUZtbk4rbW1jS2phV0hZWmlUMzIxOVl0ZFNkU3JTMTNodVBsTFFlUXVVOGVuYkcyKzNjYkN0UXhCdEkwM0J3Z05adnRMZkp2OEFVU3Q3dFhMR0gyMkdNdnI2blNlTlBpT0RpZjd2bWovOFlWNTU0bW1jY0pmYjV2OEF0SDNXSjVmemZnMUsycnVwWGRsU056YlZHbUNIVklIYjJBWHQ0N2Z1L0x4WEt6UFVZMlI4U0hWakxHTlpHeks0MXNjcE5maVdEWW04VFZOWm03bUYzcEhaZVBEbDg5bjBlcms0NVBNYzJ3WE51Yk9uN3I3RGNKcW13dTZ6eDQ5UWZHV3NNZ2duaVZ2TGo3UE1YSFdVOHZNeFBNZGZIc3d1eDdGYURhOVN1NXJyeW1QS0tnWkFkOVNBcnljMXlrbjVNY1BISjNmcytxcjdMK1FjWnlEZ09HNFhhVXJiRUx5dlJ1S0ZreHdOUnIzeHFKOW9CWEhnd3htOEwvTi95NnkyY2R5K3JqR2ZiaitsM1Z1bmhWakw3ZXlxVWNQdDJqY0FVWWJ4OCtWNnM1T0hqKzZucDVPbnVYTHZQTDNIVmMyV09HM0dNWWpYcDR3L0Q3bkoySFU2TnNLTGd4NXJPYkx0dTQ5bDRlR1hEQ1lYK1c3bjd2VnkzdXRzOVdhcnk2Mkw0ZGc5SEFNMlpRdzRZemVZY3o3NWo5MDBSVWVIdGh6VDd6dXZUWjNUZC9FODF0N3ByOE1tZzRsbXF0L1FyTStmYnFqOTJ2OEFNUkZuWWo4emFUUkJYbzZiKzhuSi9oZGM4dTNqeXhudkptL1pieTY1cmI3TU5Sa2dqd3FUaisrVjhQcXNybno5djh1OXZaMG1FNHVPNWZ6V1dQcEp4aWY0cjZXWHQ1c1B3d2gvS3kyUS9oQlRUc2dGemtDS2o0Q0QvOWJMb3VJSFpmbVg2QmIzd2VVQVNXdkRodUVHWFRlQ1pjZC9SQktvYUJMZVZCalBJMWlRWlFaMU4va0E5MEJ2ZTRlVThJRkVOZHh6MlFQdDNPY05KTWdkdTZySjVMUzRhdTNJUUtyblR2dzFScFZOOGp5ZlJCa3RxVERYZDFRdC9rcUdFQzZUL3dBUXRJTy9kRXIwZExtaGcyTFkraUlzT0xUdnVQWklMRFJKYzdnOXU2MEJQUFBsSEFSV2s1OHcwM1dHT3VtTi9yRkIzaXNQY09IRUwwOVBucDUrZkhicHZUZkh4bUhMRmxkT2VUY01ZR1ZnZXptN1FmMFgyOGN1NlBqNVk2cmVOeVo0SG9sUXhoSjJQQ0NPMjQ0UktzSGdJZ2hzZDFxTEJuNFlCVXF0UDZoWVUzRmN1MzF2SDRvcG1wVFB1emNmeVdQcVVycHZpOVhGc3IyVld1NzhlazN3cW85Q3phQ3Q3WmJqVE1DRHlwb0VESmlVRkdRNkVCOS9aQVFJSWp1Z1dTUVlRUUhlQnozUU5rTkVBSUJmRHh1Ti9aQnJXZGJGdDlsKyt0dzJUVXB1MHp3Q0FxUE82VzM3cnpLbG8xeDFWTGNHZzRmM21GVWIyemN3Z3VkaWdwcDNRRVVGSG1FQWs2VlI1K0xFZmNxOC91Ty9rczBhVDBnY1RnMThleHZxL3dETlluc2RLR3dsZEJHbmVVRk81aEJiVDVVRmRwUVVUSVFVWENFQmpaQkNVQW5oQlcvWlVCRWVVQWJHV2tyWHBMNWZEUFhiTDE1MDI2cjA4eDRTdzBiWEVLemNUc25EWUN1d3pWWi9JcDdjL1RwZU1ZTld6M2ptWDhWd1BHWFlYbERQMXZUcTRvS2NPcG5Fckp1clE0SFp1c2p6TERESHpEbWJPR0kzdDNqK1BaVXRNV3cyeEZ4bFFBRnIzdnV5NHNiVkRuY0NlRjBnMHk5dzY4emwwUXY4SnZyZDFMTm5UdTZlNTF1OXBiVlphdmRKYkIzSUU3ZXl6bGQrR3BHdlpYYU9vUFIzR3NvbUg0L2xXcCsxOEhiL0FOSTYyZHRYWTN2c04xbVh0OEw2ZVAwLzYyNWx5SGdXS1lQYVY2bFR4bXNiaFllZFRMWjdIQWt3ZVpFaGI0NzIzOXpibnVNNHZkNDNpZHpqRjIxamJ1N3FHclZiU2FHVTliakpJQTRYYm01Sm9aZGxtWE1GT2pUd3VoZVY2dHFMcW5kMDZPb3VkNDFNK1dQa0Y1ODg5NE5ZKzMxTmkrSFlIMU5zcXVQNGZZdXJZblF3ZHR0YzE2OUp6U0wzeWhyWkk1Qkc2NDkzZDRYdDdmTDBNZS9aV2U4aVlaWG8xelN1TGF4ZGIxeTN2VXN5R1ZHa0hrQWc4L05kWk8yVml6dnJaYzNkSjh1WnJ5dGhlSW1vTUx4Nnd0S2ZnWXBTOGs2V2pUNG5FZzhCWXd4dGRyZE9jWkJ6YmY0ZmpWeGxMTUZNVXNUb0RYU3FOTTAzMDRndWIzNVhhK0hITExidDFsb3c5NHhMRUgrRXl1dy9kS1orS1NESC93Q0lDeHcvUGsxbGUyUGtEclppV0paMnp0b3RxUnVMckQ3YlJYYXpmU0dGenlaOUExZXpxTXBqanB3d3gzZHQzNks1VnI0cGtobU1ZTGQwN0RGTEc2cS9mcTlZZi9EdllhYnRKSHM2VjhmZ3h0NU52Vnk1VHNjZzZzMzJFWWpuVzgvWWp4WHM3ZGxPM2JYYUFQRmZTRU9PM3V2c2MyVTA4bkJ2YlJpMFNPemlRQjZnTHdUOFV2MGV1MlR4K2J0UFNRMXNPdy9HOCs0dFZlNW1GV3pxZGw0aE1DcVJwWTFvTzBwbmQ4c3M5SEplM0hTK2grSHZ4VE5WL203RVc2bVdEYWw1WGNmMzNTNFFWMDZuK0p6NFNmcVk2NHVPMTdyOGR5SmUzVmptSzlvM0Q4WnU4U2MvRnFXbDVhTFpydks1NGlJaGQ4OVozdm44di9EeTRmSkxoZjV2K1diaTE3bUhMK0wzK0Q0VmhkTzN3UE9qd2JOb0VPcDBpZGlCMkJDNVpaYnkrOCtucDFtR3NmdS82dGM2ejNyRzNXQmRQOExNVzJGVTJtdTJudnFydTVsZDVsMmRQZnpaazcrZjlIMHIwcXdBWlp5WllXUmFHVjZvRmFwSGR6aElYeWVteDdwYmZiMWMrV3M1SjZiazk4a2xlckgwWnpXVkpjWldtQ25IYUNnQXVBQ0JUNmdRWXRSOG5ZN0lQLy9YeWVIZ0RoZm1YMzlxYzA3a29xNkc1ZzdsRVpMV2ltZFIzUG9ncDd6R3cybFJTM2pVUVFFR1RTblllbTZCdGZab2NPZTZER0FKTWc3aEJuTTB0TFN6bU56MlZaVlZMbk5OVUJBclVYanpiaFJUS1FESUk0UDhFVmt1WUNDWmlPRlJqMUhFUWUvQ0J0dUcxWm5ZamhFck1vVk5WSjdIY3pFb2dHUGdFSDh2ZFdBM3VPem03eHZBVkFpb0g4aUNpc08vdG0zTkNwU2ZCQkc0VEc2WnptMnY5SjhVZGdlYkw3TE55ZE5yZGsxN1ZwMjgzN29YM2Vremw4YmZKNXNkTzkwM0F0Qm1RZTY5T1U4MTVUMndKOUZsVWNaNFJLZ080UkZ2SEJWaXdRQmtKVkl2N2RsZWs1aDNEaEIrcTUwY3c2ZFZEaE9ZOGZ5MVVkRFcxUHZWdTA3ZVZ4Z3dyR1hWR2tSTTdMckFXaUNzaTJpU1pRVVNaUVdDUVpKUVFFT08reUNnME5lVFBLQnBRVTZkSmpsQjUxN1RjKzFleDIrb0VINmhCenpwVGNHeXZNYndJbmUydWpVWlA3cnlxT3FoMEdSeDZvR0RoQlE1UVdUdWdxZHA3b0tkQmI3K2lvOHZHSE9GblgvN3QzOGxtalRPajVuQXIwLzl2ci96V1pQSTZUSmhiQk5JQVFVNCtaQkFRQnVnRW5aQkFVRWQyQVFHZ0Z4Z0lLMWRpZ3VRQ3JQYVVFd1ovbW1TNHVTZmFHeUEzTytRcTl6WjA5V05ZSVRkMkJHNWNIQ0h0K29sWEcvbTVaejhuejcwUXhtbG1iQWNYNlJZdGNHMHVid092Y3VYb2RwcVVMNmx5eHZwSkVxTXZheXprdlBtSmRMODJXRmxqdW5ITVB2M1hHSjROWEI4VVY3VTYyMUdrOFRFeXR4SHFaVnQ2ZUFQd1BxYmlPTS90ZkNzN0FZTm11aFdjQTZuVnFOMEF4MzBFUks1NFMvZVg4blNXYWN1MDN2UTNyTTZsWEJmaHJhOFNSNUsrRzNVZ2owSTB1L2dzOHY0dkROYTMxZnljekptYzdxMXNScndURVdqRU1KcWpkcnJhNDh3QVBjdEpncnZaTHBjZjFhSDVkTWlkdVo5bHk1dTIvVjE3V1JoMTQvRHNRdEw5Z2wxdFZaV2p2REhTVkpqTzFuVDlDN0xPMlZMN0lkUEdhZGVsUnAzZHFLdGUyb05EYWdxdVpCQUFHNzl0bHo2ZWZQNWRlYlV3Y0I2ZjQxY1l4WHpKZ3R4YnV0YWJmdlp0cVRtNlMyblhweTBrZXAwN3IzZFIyK05PUFQrcTJiTTJaODA5UGNsVzJGNDQ1enJkbEYxR2k2citLRDRqZnczTXFkdzBtUzEyNFZ3d2M4c3I5V21aZXBPczhid3JHcnIrc1kxakFxT2U5eDFBVWo1bU5aOGw1ZWEzNk40VGZ0MXpNK1pUZ2VCMU1jeGFwcnJ2ZDRlSFllTjNQcnVvdDJhUG1TdTNUNDl1UGRmRG4xRjd0VER6K3pRT21HVHI1MVhFYTJKVzdhdU40azNXODhqd3J5UkRqMklBSVhpNWVTNVpiK2p0eFR0eCtieFd1OVNNM1lYa2JKVlBJT1V4VXNjV3ZLdFJ1TWt5Mm96UzdZZkloZlY0K0dZWWJ2aDVlVEszTFgwYXAwS3libDdQV08xc0l4NjJlYWRKb3VLTnl4MGFDMCtiVi9pWHllWFBMTExXUGw5RGl4eG1QbWo2MmRQOFB5N25MdzhIdmFOUmwyV01vNGJUL3RhSTJqVVBkZXJPZmQ4T1dXWGk2OFBIaGJueXlUODJWMUlxTnlya3JCZW5sa2Y2N2RodDlpbW40dFZUNFd1V3VpNDc5eGx5Y255ejZiYTZ1OTNMTWNmTmJoZ21FM1dXZW4rSFpmd3dBWm56WFZhUUhmRjREWTVIb3ZQMDF0d3k1Znk5T25MODFuSDlmeWVoVk9NNHBYelJsaXZoRnZiWW15MW9XOVBFYUFiNFZKdElidWR0eVYzbThma25ySnczdjU4dkZ4ZVhsSEY4VXIwYnZQbWNxNHF0eTNRZGE0V2VLYjNzOG8wanVWT1BHNVpmZFkrZnIvbzFsbjI0L2UzeDlQOVdwOUs4RXZ1b25VV3BqVi9MMm1xYnF1ODd0MHQzaGMrczVKYyszRHppOVBTWVhIajdzdkdUN0tZMXJhYk5FQnJBQXhvN0FjTGx4eTQ1YStpWStkM0wyamllNVhweWtsOEp2ZmtsMVdObGtJZlVrb0Z1cVFFR08rb2UyNkRHZlVkSlFmLzBNMXoydGFOdk12ekw3Y0pxYXk2ZnlvNlJHa3RmTGVDaU1yVnVOU0FuT1lmS09TaXdxQzE0bjRVVmxEY2F4d2dCeGMvNURsQXRyWDZwSENJektSZEhDSVlIa05jMGp5b0ZrdE1RSWFzd0d4dXBwMGNEaGJnWkJMQnY1aHlFcWtQZDRoRFlnalpSVDZMUzJENkt4R1JTWVdrazkxVUdBMHRjUHpTZ0Z6djNPQnlncDRZOEFoMEZBdXQrRzdiZlVrOUxLNXpteGxUQk1ic3N4V3NzZGExV3VxRWZ1dU82OW5SWmF5ZUhxTWR2b2ZCTVNvWXBobHRlVVhmaDFtdExQazRMN2R1NitYWnA2d0pHM1liS0lJR1RJNFVvWVFDSkhaUVdmTXlPNFFEVExoSWR5Z0o0bW1QMS9SUytoeVRNODVmNmhZUmpUZkpiM2puVzF5ZlhWdTMrS1FkV3Q2cktqQTRmQ1c2bHVCK3BCTmJRZ2hPcUNna1R1Z2tRVUF1bWRrQkVPUUZEb0tCRllSVE04b09VWWFmMkwxVXIwVDViZkU2QmVQOFZOWkhYV2ZBMUEzVXRDYWtBbDI2QW1tUVVBOTBIbVkyWXNhLytCMzhpZzB2bzBad0MrLzhmWC9tZzZUcTRDQWtFL01ncHlBTlNBbW1kMEVRUVBHOG9CM016d0VFYnVKUUVleUFUeUVBT1kyb3pTV2gwaHdxc2Q4SmFnK0d1dU9ROFE2V1orbzVyeStIVU1KdnE3Y1FzcXpOaFJ1ZzZYVWZxZDQ5RnR5c2JyaStKdHpaZytGZFlzdDFxMUJqVFRzZW9XRjJybzFVQWRMcWhZTjkrWjlGSE94NXY3QjZZM0hVT3ZhMk56VnJkTXJteWRXTndBOTFwWlluVUhrY0kyRVFDZmRibFkxNVltZWNQbzlVZW1yY1p3dW8yNnpUa2Q5U3h4QnpOMzNWaTArU3NQWUFBckZkc1d2WVVQOEFsVzZUVmNGSjhUTjJTV212aDJyZXRjNGZIblo2blNkMWNmRU01M1dOUjZNNWZ3SE0rZHJiQjh6VTJqQ2F0TjVybDd4U05Jczl6M0pYSHMzWGVYVWVuMS95MWxuTG1kNjlybG02YStqV3BzZlhzS2JZOEVhUUJ2M2ticjFaWWF3ZWZITDVuZS9zMFlsYVpyeUJkNFBpVnZTcTNlRHZkUXBQQUIxMEhpV08rWU95OE9GK1o2ZVdmSzErcmJWTUc2b1hOSnJkRkRFTEt1WUFnYTZUU0Qva3ZSeU9mRGRObjZ0T3NyeWhnT0hYMFY3YTZxVWJlRHZEcWxMUWY0bGRweVh0Y2VYems1N21icFAxSDZkWXJaNHZsb25IOENzSk5Ha2Q2OUZrU2RYcUY0Sm5sY25hK01YaVphdmNRNmo5UW1YK0xYRmF2aHVHVXhWclUzZVFXOVZtNEFaN1BFZkpldlBrMVB1L3o4dVhUWWF5dGR1eFZ1TDVicFhmVVRETHBscUxhMzAxYldzQUtkWUVUdDZFT0JBK2F4d2NQZmwyZmsxMUdmbDhjWjZ6amY1N3pMYzVqeEdtMmxYcmtEdzJDQUd0MkM5blB5OTN5dU9Ibnk4M0NjdzQzZ1hpT3dpOHFXbFNyeWFSalZwMzNYZ3gvaDNiMlhIY2RhNmZpdG5ITTl6bi9OSmFjUHdhZzJ2YzFYY1ZIVVcrUnUvY2xkY3IvYXNzY1BwSG5sKzUzbDlZd3NwMlYxMVM2a1Y4YnY1R0dpc2JtdlB3MDdlbVpBK2dDejFIUDM1VHBwNmpXTTdNUHY3N3JkOHd2R1lzUnI1L3d6R3FkaGI1ZnVHWWRoZHJxQThtb05MbDNrbkhqOTFQVG54YnpuM3Q5c0hGNmVZYUdiMzVjeXhqRDhRL3BIU3B2eGl1M2NOYzdjZ0VjQ0Z6NU12dXNlMythK2x4eCs5eTd2NVo3ZUgxWHgyaGJmY2VuT1hIelk0ZUdpN2ZTM05hNGR5RDlWMnVYOW00dTcvOG1YK3ZuMnpoai9hT1h0L2t4LzA4ZW5lT2llU21aVXl2U3VMbW5weEs5SGlWSGQyajkxZkk0c1gwc3NuVEdWT1k0N0wzUFBrRjlUWkVqR3FWbXQwejNSU3FsUUYyeUJOUi9DQkxxaURGZlVneWcvL1J5QzhPNVg1bDl1QTFIMTJSMGhsTXNxZ2pndFJHUTBENmdJRW1kUlBFSXNHMHp5aW5zZHA1NDlFRnVlSU1JQXBWU1NZRzRRT3Q2aGNYYXlSN0l5WVNZYzFBdHVzK1VyTUdUU09naG5vdHdGNG5taitLVlNYbHVzd0orU2luc0RneVdqOVZZaGpkZW5VNDg5bFVHd3hKUUVIZ2d0QUVuYVVDeUhVM1NSSVFNY3luVUxTUVFGWjZZMjEzTldIQy93QU5yMHRJY1h0TUFpZUZuaHoxa2x4MjlMb25qejYrRjFjQXUzVGNZVzhzWTF4OHhZN1ljK2kvUzRYY2o0dkxOWjM5M1lhY2dhVHkzWmFjeHRFU3BSVWtPS2dzT09wQWN5NElEY2ZMcDlRcGZRNXoxYXdzWFdYNmw0d0h4N0F0clV5T1pZWjJQMFNEWk1wNGxUeFhBN0s4WTZXVjZUUzQranRNRWZxdWtIdkZ1OGc3S0JkUWs3ZTZCclJBQUhDQXdkdDBGeU9VRmJIY0lEN0lLQjVRS2VaWWU1UWNvejgxdUdacXdIRnhzRzNJdDZqdjdyK1ZrZFh0SGw5SmpwbVFnYzMzV2hEeWdNd0VBRjRtRUZuaEI1R1BtTEc0L3dDN2QvSkJwWFJneGdkOEFkdnYxZjhBbUVIVEQyOVVCdGNEc2dKeENCZW9JQkhLQ3lnaUMyd09VRWVSQ0FXa2FVQkU4SUFjVUUvL0FPSU5XNmdaSnczcURsZTd5NWlGTVRXRG4ybFE3dXBWMmpaMDlscEh4VmtqTU9LZEZjLzN1WGN6MHRXRVYzRzB4eTJlSlpVdHo1UlZhRHNkdDFYT3gwSEhNU0hSbXh4ZkI3S3hwWXprRE83WFY4RnZqOEZDcFhhR2ltOTM5MlZuYkdqc0p4TEdNbTQzZ2xYK2pZYmg5amhsTzN6aFd0b052ZDRmY2VWbHlBTmpwQjgzeVdvclJNellkZGREK3FWbm1IQm5HNHl6aWgrOVliVUg5bFhzcmd5NWhJMjJhZUZNbXBYZzlZOHBXbUE0M1J6VGxwem01UXpWVCsrNGJXWWZMVHF1SG5wT0k5RDJWeFczYm5WL2lGN2lOd2J1L3JPcTNMbXRZYWp5WE9oZzBqY3J2MzdtbU8zVjIrbGZzZlhqcWVJWmpzQVFBK2pTZTBlbW5WditxK1h2V2IyMmJ4ZEg2Z1liVHRNMllOZkZ3WjRseFV0M3VJK09wY1VYU0IvZEEvaXZkN2VURHcwRHFuVTFVOExzM3ZjTHFuVSs4MGkwbVI0VGdRZjRMdGJqcG0rY25VY042bjNEY0lvbDdyZTVjMmlJOGFvS0ZjalR2cUR0bHc0SmpjblhsOFl1VzlJS21INGxtUE1WNitpeWxkWGx3OTdqVGNIaHJLbFF5WkhJK0dGNXVwOGN2ZDlKNGI0L0dMMlB0VDQwTUZ5WmhtV0xaOHZ4Q3MwMUFOdkpURyszb1NWOWpwcDIvd0FWOC9rdThuekZrM0pHTVo1dmErSDRMQnU2RFBHREhURHRINVY4aVpiNXE5dkhockZqWWhsVEZjSnpFY3VYMUdNU1k1cmF0SmtrdE5RN0s5VitVYnVXblZNL1ZtWk15bmgzVFRDak9MWDVaYzR5V2N5UUN4bTM4bDdlbngrNDQ3bmZyUER6WjQza3NrZS9UdDZuVExwM1R0TFdrYXVaOHhDYXphWTFWS2RzZmk0M0d5OEhSWVhIa3k1TS9ydlRYV1g3MlRqdytqeGI2bmtyREliK3pidXJoTjdZaGxzMTBobFcvY1BNWTlpdmR3YXh5dVdmcXM5Umw4bU9HSHVQV29DaDBreVJVeG01UGk1dHhtbm9zYVR6TDZOSndnR1R2c0Z5NmJHOHVkNWMvV1ByK2pmVVpUanduRmgvTjcvcTgvb2ZrRzV6VGpiOHpZdTB2czdlb2E5UnpoT3FvNHozNTNYaTZubHZMbDN2WndjYzRzZXg5WE1pbXdVV2pTMGJBRFlCZGNNWEh1VUhRRjJxSVhqU3BCalBod2tvRUZ3YjNRSnFQN3pzZ3gzVk4rVUdQVXFjeHlnLy85SXFteFg1bDkzU05KMGtJb3JWeERpRUdaTXZKOUFFQUV6dW9LbythcHBQQ0xzK29BTmdnSGhzb29hSmg1aEJrMFBqK2FySnpnTlRrQ2c0dWVRZTNDekEraTRsMG5rTGNCUEd4ZDNDbFZqVkNXUDJQdWliWmxOeE5NRXBzdE1JZ2ozVGFiTm90RGlRVTJiQjZuMDRUYXFMeVdTZVFtd2JpWDAya245RlpkZUdORjNER3VhN1VQaGJBV0pOWGJVOE5DeWpXcVdIVXp3N1U2R1hWSjRyTjdIUUpCWDMrbXp1VWtyNUhVNHlXMTlIMENYTmFYR1NRSks5ZWZoNWNQTVBKT3BTZ0FTSGZOUUVTZ2pIRW1VRENKQVBkQjQyWmFOTzR3cTRvMVJxWTVqbWtlMEZZMk5KNk8xYWh5MTRMakxLVnpWWXlleldPMlhYRHlPbE1jUnNwc1c4YWdKVFlKaDJqMFdkcm9SV3BWMFBTQzM1clNWUTRqc0ZFRUNTQ2xBdEpJTXFiQytSSHVteHpEckcwREFhTlVEOFNuV3BsanU4bHdXWlIwTEJYdXE0ZmJQY2R6VGFUSHFRdFNEMDZiaWVWUVo3RkJDWk1kb1FKanpvR2owN0tqeE13dkl3MjZQY1UzZnlWME5MNktrL3NTOUhyZTF6L0VMbnZ5T29IK1MwSXpaL3pRRWR6Q0JmcWd0dnFnc25ZbEJUVElRQ1hIVkNBWHVQOFVCTjRRV1VBdStJQkJjb0FlUFRZa1FTbHVoOHpmYTR5NWhKd1RETXp0b0JtTU5yL2RYVm03YTZSRXc0ZDQ3SzQzYm5XcWRHWEhQSFMvTmVVTXkvMXpCOEpvbTV3NE8vdGFGUUFueVAzSUd5bVhqUEdmbXpmR05yU01pWmt6Qm1PN3RzcVloaWR3TUlaWjNGbVcwbjZLanJiY2ltNXhCbG9XZW96dkh4ektlOTZXVGVXdjhBOWR0eHAwMjVtK3p0aXpjYW01cVpXdkRRd2VzNysxcDBtdklEUzd1SVc1ZHlVdU1ranc4aGs1bjZJNXl3WEdQeDdMQVF5OXdvZm5vVlNOOUx0L0tlNFVwSTVqa2JBN0hNR0xWN0xFTlpvc3RuMVcrRzdTZFZPQ040SzQ1NTNDYmp2ampNdkZmUWYyZHNOdHNENmw0eGg5anFGdTdEcWRVNnpxZHFkRGp1dlBoZTY3cmUvbzZoMVlPcXZiWEJBTDdXNXRSUUhacHJWQnFJOTRFTDZjbTQ4V2Q3Y3BJNTNtYTJwWWhtaWlMa2FoYjI3WFV3TnQzR0RLNDVUdzNKOHp0V0tZZGh6Y0F0bU9zNkZUK3F1WUhQcHRMZ0JUbVFZNVRwTUpjblRxUEdMajJRY0d3Nnd4L0xOL1owUlJyWWo5NHQ3eHJkbVZHTkllQ1crc3JueStjY3AvOEFzWS9TZm8wYjdYTmVxN05PRlc1ZCtHMjJjUVBRbDMvc3ZwOTFuU3lmcC95OGVPTXZOMi9ScVgyZnN4NGxnbWNHVzlpYVlaZHd5b1hzMU9BSS9LZXk4T0hIUHZNYitlbzltZkpjY0xyNk5yeXBVR1plcm1MNDFpMU5sVzl0aFdxTWhzTUxxTFRwMUR2QzExdUV4NmpIQ2VxODJHVno0cm5mYldlbnpCbTdxdSs1eDBtNHFtNWM3MEhrSmdSdnN0L0ZjN01weHoxaTlQUmVjTi9temMzWnZ4eXc2aVl6aVZ2V2FLdHZxczZOSnpRNmsyaVJCQWFma3VuVTRTNDR6OUpYaTZITzRaWldmblkySEtGVTVzemZoZHRqVFcxYmJEcllWcmFpMGFXQ282WEVrZDE1L2llUGZ4Y2UvckhvNkg1ZWJreStzcm5PZXNVdmN4OVFicWhpbFR4S0ZDdjRGQ2szeXRZd0dJYU95OXZ4Sy9kOXVHUHJVLzdQUDBHUGRibGZkcjdHeWJoVmpnMldyQzB3K2tLVkh3MnVkSExpUitZOTE4YmpuZmJiOUgwODhyTTVoOUxIc09LOVVtbkhRRHNGYXBibkdDak8yTTV4aEdvU1NTQ0NwYXVpYWhNUW16VEZxRkpVckdxRXFwdC8vOWs9XCI7XG5leHBvcnQgZGVmYXVsdCBpbWFnZV9iYXNlNjQ7Il19
//------QC-SOURCE-SPLIT------
