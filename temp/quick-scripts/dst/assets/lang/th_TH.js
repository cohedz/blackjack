
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/th_TH.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3b07by6DGRJu75lIR9EK7nZ', 'th_TH');
// lang/th_TH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'เล่นเลย',
    'WIN': 'ที่ 1',
    'LOSE2': 'ครั้งที่ 2',
    'LOSE3': 'วันที่ 3',
    'LOSE4': 'อันดับ 4',
    'PLAYER': 'จำนวนผู้เล่น',
    'BETNO': 'เดิมพัน',
    'DAILY': 'รางวัลรายวัน',
    'D1': 'วันที่ 1',
    'D2': 'วันที่ 2',
    'D3': 'วันที่ 3',
    'D4': 'วันที่ 4',
    'D5': 'วันที่ 5',
    'D6': 'วันที่ 6',
    'D7': 'วันที่ 7',
    '3TURNS': '3 รอบ',
    '5TURNS': '5 รอบ',
    'TURNS': 'รอบ',
    'RECEIVED': 'ได้รับ',
    'LEADER': 'ลีดเดอร์บอร์ด',
    'NOVIDEO': 'ไม่สามารถเล่นวิดีโอได้ในขณะนี้',
    'BET': 'เดิมพัน',
    'NO': 'จำนวน',
    'PASS': 'ผ่าน',
    'HIT': 'ตี',
    'ARRANGE': 'จัด',
    'QUITGAME': 'ออกจากเกม',
    'QUITGAMEP': 'คุณต้องการออกจากเกมหรือไม่? หากคุณออกจากเกมคุณจะเสียสิบเท่าของระดับการเดิมพัน',
    'QUIT': 'เลิก',
    '3PAIRS': '3 คู่ติดต่อกัน',
    '4PAIRS': '4 คู่ติดต่อกัน',
    'FOURKIND': 'สี่ชนิด',
    'FLUSH': 'สเตรทฟลัช',
    '1BEST': 'ดีที่สุด',
    '2BEST': '2 ดีที่สุด',
    '3BEST': '3 ดีที่สุด',
    'INSTRUCT': 'คำแนะนำ',
    'NOMONEY': 'คุณหมดเงินคลิกเพื่อรับเงินเพิ่ม',
    'RECEI2': 'ได้รับ',
    'SPINNOW': 'หมุน',
    'SPIN': 'หมุน',
    '1TURN': 'อีก 1 เทิร์น',
    'LUCKYSPIN': 'หมุนโชคดี',
    'LUCK': 'มีโชคในภายหลัง',
    'TURN': 'อีกหนึ่งเทิร์น',
    'X2': 'X2 เงิน',
    'X3': 'X3 เงิน',
    'X5': 'X5 เงิน',
    'X10': 'X10 เงิน',
    'MISS': 'นางสาว',
    'MONEY1': 'ยินดีด้วย! คุณมีเงิน %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RoX1RILnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQStCO0FBRS9CLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFNBQVM7SUFDcEIsS0FBSyxFQUFFLE9BQU87SUFDZCxPQUFPLEVBQUUsWUFBWTtJQUNyQixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsVUFBVTtJQUNuQixRQUFRLEVBQUUsY0FBYztJQUN4QixPQUFPLEVBQUUsU0FBUztJQUNsQixPQUFPLEVBQUUsY0FBYztJQUN2QixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixRQUFRLEVBQUUsT0FBTztJQUNqQixRQUFRLEVBQUUsT0FBTztJQUNqQixPQUFPLEVBQUUsS0FBSztJQUNkLFVBQVUsRUFBRSxRQUFRO0lBQ3BCLFFBQVEsRUFBRSxlQUFlO0lBQ3pCLFNBQVMsRUFBRSxnQ0FBZ0M7SUFDM0MsS0FBSyxFQUFFLFNBQVM7SUFDaEIsSUFBSSxFQUFFLE9BQU87SUFDYixNQUFNLEVBQUUsTUFBTTtJQUNkLEtBQUssRUFBRSxJQUFJO0lBQ1gsU0FBUyxFQUFFLEtBQUs7SUFDaEIsVUFBVSxFQUFFLFdBQVc7SUFDdkIsV0FBVyxFQUFFLCtFQUErRTtJQUM1RixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsUUFBUSxFQUFFLGdCQUFnQjtJQUMxQixVQUFVLEVBQUUsU0FBUztJQUNyQixPQUFPLEVBQUUsV0FBVztJQUNwQixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsWUFBWTtJQUNyQixPQUFPLEVBQUUsWUFBWTtJQUNyQixVQUFVLEVBQUUsU0FBUztJQUNyQixTQUFTLEVBQUUsaUNBQWlDO0lBQzVDLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLFNBQVMsRUFBRSxNQUFNO0lBQ2pCLE1BQU0sRUFBRSxNQUFNO0lBQ2QsT0FBTyxFQUFFLGNBQWM7SUFDdkIsV0FBVyxFQUFFLFdBQVc7SUFDeEIsTUFBTSxFQUFFLGdCQUFnQjtJQUN4QixNQUFNLEVBQUUsZ0JBQWdCO0lBQ3hCLElBQUksRUFBRSxTQUFTO0lBQ2YsSUFBSSxFQUFFLFNBQVM7SUFDZixJQUFJLEVBQUUsU0FBUztJQUNmLEtBQUssRUFBRSxVQUFVO0lBQ2pCLE1BQU0sRUFBRSxRQUFRO0lBQ2hCLFFBQVEsRUFBRSx5QkFBeUI7SUFDbkMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL3RoX1RIX3R1dCdcblxuZXhwb3J0IGRlZmF1bHQge1xuICAgICdQTEFZTk9XJzogJ+C5gOC4peC5iOC4meC5gOC4peC4oicsXG4gICAgJ1dJTic6ICfguJfguLXguYggMScsXG4gICAgJ0xPU0UyJzogJ+C4hOC4o+C4seC5ieC4h+C4l+C4teC5iCAyJyxcbiAgICAnTE9TRTMnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDMnLFxuICAgICdMT1NFNCc6ICfguK3guLHguJnguJTguLHguJogNCcsXG4gICAgJ1BMQVlFUic6ICfguIjguLPguJnguKfguJnguJzguLnguYnguYDguKXguYjguJknLFxuICAgICdCRVROTyc6ICfguYDguJTguLTguKHguJ7guLHguJknLFxuICAgICdEQUlMWSc6ICfguKPguLLguIfguKfguLHguKXguKPguLLguKLguKfguLHguJknLFxuICAgICdEMSc6ICfguKfguLHguJnguJfguLXguYggMScsXG4gICAgJ0QyJzogJ+C4p+C4seC4meC4l+C4teC5iCAyJyxcbiAgICAnRDMnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDMnLFxuICAgICdENCc6ICfguKfguLHguJnguJfguLXguYggNCcsXG4gICAgJ0Q1JzogJ+C4p+C4seC4meC4l+C4teC5iCA1JyxcbiAgICAnRDYnOiAn4Lin4Lix4LiZ4LiX4Li14LmIIDYnLFxuICAgICdENyc6ICfguKfguLHguJnguJfguLXguYggNycsXG4gICAgJzNUVVJOUyc6ICczIOC4o+C4reC4micsXG4gICAgJzVUVVJOUyc6ICc1IOC4o+C4reC4micsXG4gICAgJ1RVUk5TJzogJ+C4o+C4reC4micsXG4gICAgJ1JFQ0VJVkVEJzogJ+C5hOC4lOC5ieC4o+C4seC4micsXG4gICAgJ0xFQURFUic6ICfguKXguLXguJTguYDguJTguK3guKPguYzguJrguK3guKPguYzguJQnLFxuICAgICdOT1ZJREVPJzogJ+C5hOC4oeC5iOC4quC4suC4oeC4suC4o+C4luC5gOC4peC5iOC4meC4p+C4tOC4lOC4teC5guC4reC5hOC4lOC5ieC5g+C4meC4guC4k+C4sOC4meC4teC5iScsXG4gICAgJ0JFVCc6ICfguYDguJTguLTguKHguJ7guLHguJknLFxuICAgICdOTyc6ICfguIjguLPguJnguKfguJknLFxuICAgICdQQVNTJzogJ+C4nOC5iOC4suC4mScsXG4gICAgJ0hJVCc6ICfguJXguLUnLFxuICAgICdBUlJBTkdFJzogJ+C4iOC4seC4lCcsXG4gICAgJ1FVSVRHQU1FJzogJ+C4reC4reC4geC4iOC4suC4geC5gOC4geC4oScsXG4gICAgJ1FVSVRHQU1FUCc6ICfguITguLjguJPguJXguYnguK3guIfguIHguLLguKPguK3guK3guIHguIjguLLguIHguYDguIHguKHguKvguKPguLfguK3guYTguKHguYg/IOC4q+C4suC4geC4hOC4uOC4k+C4reC4reC4geC4iOC4suC4geC5gOC4geC4oeC4hOC4uOC4k+C4iOC4sOC5gOC4quC4teC4ouC4quC4tOC4muC5gOC4l+C5iOC4suC4guC4reC4h+C4o+C4sOC4lOC4seC4muC4geC4suC4o+C5gOC4lOC4tOC4oeC4nuC4seC4mScsXG4gICAgJ1FVSVQnOiAn4LmA4Lil4Li04LiBJyxcbiAgICAnM1BBSVJTJzogJzMg4LiE4Li54LmI4LiV4Li04LiU4LiV4LmI4Lit4LiB4Lix4LiZJyxcbiAgICAnNFBBSVJTJzogJzQg4LiE4Li54LmI4LiV4Li04LiU4LiV4LmI4Lit4LiB4Lix4LiZJyxcbiAgICAnRk9VUktJTkQnOiAn4Liq4Li14LmI4LiK4LiZ4Li04LiUJyxcbiAgICAnRkxVU0gnOiAn4Liq4LmA4LiV4Lij4LiX4Lif4Lil4Lix4LiKJyxcbiAgICAnMUJFU1QnOiAn4LiU4Li14LiX4Li14LmI4Liq4Li44LiUJyxcbiAgICAnMkJFU1QnOiAnMiDguJTguLXguJfguLXguYjguKrguLjguJQnLFxuICAgICczQkVTVCc6ICczIOC4lOC4teC4l+C4teC5iOC4quC4uOC4lCcsXG4gICAgJ0lOU1RSVUNUJzogJ+C4hOC4s+C5geC4meC4sOC4meC4sycsXG4gICAgJ05PTU9ORVknOiAn4LiE4Li44LiT4Lir4Lih4LiU4LmA4LiH4Li04LiZ4LiE4Lil4Li04LiB4LmA4Lie4Li34LmI4Lit4Lij4Lix4Lia4LmA4LiH4Li04LiZ4LmA4Lie4Li04LmI4LihJyxcbiAgICAnUkVDRUkyJzogJ+C5hOC4lOC5ieC4o+C4seC4micsXG4gICAgJ1NQSU5OT1cnOiAn4Lir4Lih4Li44LiZJyxcbiAgICAnU1BJTic6ICfguKvguKHguLjguJknLFxuICAgICcxVFVSTic6ICfguK3guLXguIEgMSDguYDguJfguLTguKPguYzguJknLFxuICAgICdMVUNLWVNQSU4nOiAn4Lir4Lih4Li44LiZ4LmC4LiK4LiE4LiU4Li1JyxcbiAgICAnTFVDSyc6ICfguKHguLXguYLguIrguITguYPguJnguKDguLLguKLguKvguKXguLHguIcnLFxuICAgICdUVVJOJzogJ+C4reC4teC4geC4q+C4meC4tuC5iOC4h+C5gOC4l+C4tOC4o+C5jOC4mScsXG4gICAgJ1gyJzogJ1gyIOC5gOC4h+C4tOC4mScsXG4gICAgJ1gzJzogJ1gzIOC5gOC4h+C4tOC4mScsXG4gICAgJ1g1JzogJ1g1IOC5gOC4h+C4tOC4mScsXG4gICAgJ1gxMCc6ICdYMTAg4LmA4LiH4Li04LiZJyxcbiAgICAnTUlTUyc6ICfguJnguLLguIfguKrguLLguKcnLFxuICAgICdNT05FWTEnOiAn4Lii4Li04LiZ4LiU4Li14LiU4LmJ4Lin4LiiISDguITguLjguJPguKHguLXguYDguIfguLTguJkgJXMnLFxuICAgICdUVVQnOiB0dXRvclxufVxuIl19