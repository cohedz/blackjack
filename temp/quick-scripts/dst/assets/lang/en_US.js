
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/en_US.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0d583jnHB5GtZrm4qYwUeQu', 'en_US');
// lang/en_US.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var en_US_tut_1 = require("./en_US_tut");
exports.default = {
    'PLAYNOW': 'Play now',
    'WIN': '1st',
    'LOSE2': '2nd',
    'LOSE3': '3rd',
    'LOSE4': '4th',
    'PLAYER': 'Number of players',
    'BETNO': 'Bet',
    'DAILY': 'Daily reward',
    'D1': 'Day 1',
    'D2': 'Day 2',
    'D3': 'Day 3',
    'D4': 'Day 4',
    'D5': 'Day 5',
    'D6': 'Day 6',
    'D7': 'Day 7',
    '3TURNS': '3 turns',
    '5TURNS': '5 turns',
    'TURNS': 'turns',
    'RECEIVED': 'Received',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Video cannot be played now',
    'BET': 'Bet',
    'NO': 'No',
    'PASS': 'Pass',
    'HIT': 'Hit',
    'ARRANGE': 'Arrange',
    'QUITGAME': 'Quit game',
    'QUITGAMEP': 'Do you want to quit game? If you quit game you\'ll lose ten times as bet level',
    'QUIT': 'Quit',
    '3PAIRS': '3 consecutive pairs',
    '4PAIRS': '4 consecutive pairs',
    'FOURKIND': 'Four of a kind',
    'FLUSH': 'Straight Flush',
    '1BEST': 'best',
    '2BEST': '2 best',
    '3BEST': '3 best',
    'INSTRUCT': 'Instruction',
    'NOMONEY': 'You\'ve run out of money, click to receive more money',
    'RECEI2': 'Claim',
    'SPINNOW': 'Spin',
    'SPIN': 'Spin',
    '1TURN': '1 more turn',
    'LUCKYSPIN': 'Lucky Spin',
    'LUCK': 'Have a luck later',
    'TURN': 'One more turn',
    'X2': 'X2 money',
    'X3': 'X3 money',
    'X5': 'X5 money',
    'X10': 'X10 money',
    'MISS': 'Miss',
    'MONEY1': 'Congratulation! You\'ve got %s',
    'TUT': en_US_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL2VuX1VTLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQWdDO0FBRWhDLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFVBQVU7SUFDckIsS0FBSyxFQUFFLEtBQUs7SUFDWixPQUFPLEVBQUUsS0FBSztJQUNkLE9BQU8sRUFBRSxLQUFLO0lBQ2QsT0FBTyxFQUFFLEtBQUs7SUFDZCxRQUFRLEVBQUUsbUJBQW1CO0lBQzdCLE9BQU8sRUFBRSxLQUFLO0lBQ2QsT0FBTyxFQUFFLGNBQWM7SUFDdkIsSUFBSSxFQUFFLE9BQU87SUFDYixJQUFJLEVBQUUsT0FBTztJQUNiLElBQUksRUFBRSxPQUFPO0lBQ2IsSUFBSSxFQUFFLE9BQU87SUFDYixJQUFJLEVBQUUsT0FBTztJQUNiLElBQUksRUFBRSxPQUFPO0lBQ2IsSUFBSSxFQUFFLE9BQU87SUFDYixRQUFRLEVBQUUsU0FBUztJQUNuQixRQUFRLEVBQUUsU0FBUztJQUNuQixPQUFPLEVBQUUsT0FBTztJQUNoQixVQUFVLEVBQUUsVUFBVTtJQUN0QixRQUFRLEVBQUUsYUFBYTtJQUN2QixTQUFTLEVBQUUsNEJBQTRCO0lBQ3ZDLEtBQUssRUFBRSxLQUFLO0lBQ1osSUFBSSxFQUFFLElBQUk7SUFDVixNQUFNLEVBQUUsTUFBTTtJQUNkLEtBQUssRUFBRSxLQUFLO0lBQ1osU0FBUyxFQUFFLFNBQVM7SUFDcEIsVUFBVSxFQUFFLFdBQVc7SUFDdkIsV0FBVyxFQUFFLGdGQUFnRjtJQUM3RixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxxQkFBcUI7SUFDL0IsUUFBUSxFQUFFLHFCQUFxQjtJQUMvQixVQUFVLEVBQUUsZ0JBQWdCO0lBQzVCLE9BQU8sRUFBRSxnQkFBZ0I7SUFDekIsT0FBTyxFQUFFLE1BQU07SUFDZixPQUFPLEVBQUUsUUFBUTtJQUNqQixPQUFPLEVBQUUsUUFBUTtJQUNqQixVQUFVLEVBQUUsYUFBYTtJQUN6QixTQUFTLEVBQUUsdURBQXVEO0lBQ2xFLFFBQVEsRUFBRSxPQUFPO0lBQ2pCLFNBQVMsRUFBRSxNQUFNO0lBQ2pCLE1BQU0sRUFBRSxNQUFNO0lBQ2QsT0FBTyxFQUFFLGFBQWE7SUFDdEIsV0FBVyxFQUFFLFlBQVk7SUFDekIsTUFBTSxFQUFFLG1CQUFtQjtJQUMzQixNQUFNLEVBQUUsZUFBZTtJQUN2QixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixJQUFJLEVBQUUsVUFBVTtJQUNoQixLQUFLLEVBQUUsV0FBVztJQUNsQixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSxnQ0FBZ0M7SUFDMUMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL2VuX1VTX3R1dCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdQbGF5IG5vdycsXG4gICAgJ1dJTic6ICcxc3QnLFxuICAgICdMT1NFMic6ICcybmQnLFxuICAgICdMT1NFMyc6ICczcmQnLFxuICAgICdMT1NFNCc6ICc0dGgnLFxuICAgICdQTEFZRVInOiAnTnVtYmVyIG9mIHBsYXllcnMnLFxuICAgICdCRVROTyc6ICdCZXQnLFxuICAgICdEQUlMWSc6ICdEYWlseSByZXdhcmQnLFxuICAgICdEMSc6ICdEYXkgMScsXG4gICAgJ0QyJzogJ0RheSAyJyxcbiAgICAnRDMnOiAnRGF5IDMnLFxuICAgICdENCc6ICdEYXkgNCcsXG4gICAgJ0Q1JzogJ0RheSA1JyxcbiAgICAnRDYnOiAnRGF5IDYnLFxuICAgICdENyc6ICdEYXkgNycsXG4gICAgJzNUVVJOUyc6ICczIHR1cm5zJyxcbiAgICAnNVRVUk5TJzogJzUgdHVybnMnLFxuICAgICdUVVJOUyc6ICd0dXJucycsXG4gICAgJ1JFQ0VJVkVEJzogJ1JlY2VpdmVkJyxcbiAgICAnTEVBREVSJzogJ0xlYWRlcmJvYXJkJyxcbiAgICAnTk9WSURFTyc6ICdWaWRlbyBjYW5ub3QgYmUgcGxheWVkIG5vdycsXG4gICAgJ0JFVCc6ICdCZXQnLFxuICAgICdOTyc6ICdObycsXG4gICAgJ1BBU1MnOiAnUGFzcycsXG4gICAgJ0hJVCc6ICdIaXQnLFxuICAgICdBUlJBTkdFJzogJ0FycmFuZ2UnLFxuICAgICdRVUlUR0FNRSc6ICdRdWl0IGdhbWUnLFxuICAgICdRVUlUR0FNRVAnOiAnRG8geW91IHdhbnQgdG8gcXVpdCBnYW1lPyBJZiB5b3UgcXVpdCBnYW1lIHlvdVxcJ2xsIGxvc2UgdGVuIHRpbWVzIGFzIGJldCBsZXZlbCcsXG4gICAgJ1FVSVQnOiAnUXVpdCcsXG4gICAgJzNQQUlSUyc6ICczIGNvbnNlY3V0aXZlIHBhaXJzJyxcbiAgICAnNFBBSVJTJzogJzQgY29uc2VjdXRpdmUgcGFpcnMnLFxuICAgICdGT1VSS0lORCc6ICdGb3VyIG9mIGEga2luZCcsXG4gICAgJ0ZMVVNIJzogJ1N0cmFpZ2h0IEZsdXNoJyxcbiAgICAnMUJFU1QnOiAnYmVzdCcsXG4gICAgJzJCRVNUJzogJzIgYmVzdCcsXG4gICAgJzNCRVNUJzogJzMgYmVzdCcsXG4gICAgJ0lOU1RSVUNUJzogJ0luc3RydWN0aW9uJyxcbiAgICAnTk9NT05FWSc6ICdZb3VcXCd2ZSBydW4gb3V0IG9mIG1vbmV5LCBjbGljayB0byByZWNlaXZlIG1vcmUgbW9uZXknLFxuICAgICdSRUNFSTInOiAnQ2xhaW0nLFxuICAgICdTUElOTk9XJzogJ1NwaW4nLFxuICAgICdTUElOJzogJ1NwaW4nLFxuICAgICcxVFVSTic6ICcxIG1vcmUgdHVybicsXG4gICAgJ0xVQ0tZU1BJTic6ICdMdWNreSBTcGluJyxcbiAgICAnTFVDSyc6ICdIYXZlIGEgbHVjayBsYXRlcicsXG4gICAgJ1RVUk4nOiAnT25lIG1vcmUgdHVybicsXG4gICAgJ1gyJzogJ1gyIG1vbmV5JyxcbiAgICAnWDMnOiAnWDMgbW9uZXknLFxuICAgICdYNSc6ICdYNSBtb25leScsXG4gICAgJ1gxMCc6ICdYMTAgbW9uZXknLFxuICAgICdNSVNTJzogJ01pc3MnLFxuICAgICdNT05FWTEnOiAnQ29uZ3JhdHVsYXRpb24hIFlvdVxcJ3ZlIGdvdCAlcycsXG4gICAgJ1RVVCc6IHR1dG9yXG59Il19