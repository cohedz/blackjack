
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/vi_VN.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '92027qqs+hEwbWfts3Mohj3', 'vi_VN');
// lang/vi_VN.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vi_VN_tut_1 = require("./vi_VN_tut");
exports.default = {
    'PLAYNOW': 'Chơi ngay',
    'WIN': 'Nhất',
    'LOSE2': 'Nhì',
    'LOSE3': 'Ba',
    'LOSE4': 'Bét',
    'PLAYER': 'Số người chơi',
    'BETNO': 'Mức cược',
    'DAILY': 'Thưởng hàng ngày',
    'D1': 'Ngày 1',
    'D2': 'Ngày 2',
    'D3': 'Ngày 3',
    'D4': 'Ngày 4',
    'D5': 'Ngày 5',
    'D6': 'Ngày 6',
    'D7': 'Ngày 7',
    '3TURNS': '3 lượt',
    '5TURNS': '5 lượt',
    'TURNS': '5 lượt',
    'RECEIVED': 'Đã nhận',
    'LEADER': 'Bảng xếp hạng',
    'NOVIDEO': 'Video hiện không khả dụng',
    'BET': 'Cược',
    'NO': 'Bàn',
    'PASS': 'Bỏ lượt',
    'HIT': 'Đánh',
    'ARRANGE': 'Xếp bài',
    'QUITGAME': 'Thoát game',
    'QUITGAMEP': 'Bạn có chắc muốn thoát game? Nếu thoát game bạn sẽ bị trừ 10 lần mức cược',
    'QUIT': 'Thoát',
    '3PAIRS': '3 đôi thông',
    '4PAIRS': '4 đôi thông',
    'FOURKIND': 'Tứ quý',
    'FLUSH': 'Tới trắng',
    '1BEST': 'Heo',
    '2BEST': 'Đôi heo',
    '3BEST': '3 heo',
    'INSTRUCT': 'Hướng dẫn',
    'NOMONEY': 'Bạn đã hết tiền, nhận tài trợ từ nhà cái',
    'RECEI2': 'Nhận',
    'SPINNOW': 'Quay ngay',
    'SPIN': 'Quay ',
    '1TURN': 'Thêm lượt',
    'LUCKYSPIN': 'Vòng quay may mắn',
    'LUCK': 'Chúc bạn may mắn lần sau',
    'TURN': 'Bạn được cộng thêm lượt',
    'X2': 'Nhân đôi tiền thưởng',
    'X3': 'Nhân ba tiền thưởng',
    'X5': 'Nhân năm tiền thưởng',
    'X10': 'Nhân mười tiền thưởng',
    'MISS': 'Mất lượt',
    'MONEY1': 'Chúc mừng bạn đã nhận được %s từ nhà cái',
    'TUT': vi_VN_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3ZpX1ZOLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQStCO0FBRS9CLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFdBQVc7SUFDdEIsS0FBSyxFQUFFLE1BQU07SUFDYixPQUFPLEVBQUUsS0FBSztJQUNkLE9BQU8sRUFBRSxJQUFJO0lBQ2IsT0FBTyxFQUFFLEtBQUs7SUFDZCxRQUFRLEVBQUUsZUFBZTtJQUN6QixPQUFPLEVBQUUsVUFBVTtJQUNuQixPQUFPLEVBQUUsa0JBQWtCO0lBQzNCLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsUUFBUSxFQUFFLFFBQVE7SUFDbEIsUUFBUSxFQUFFLFFBQVE7SUFDbEIsT0FBTyxFQUFFLFFBQVE7SUFDakIsVUFBVSxFQUFFLFNBQVM7SUFDckIsUUFBUSxFQUFFLGVBQWU7SUFDekIsU0FBUyxFQUFFLDJCQUEyQjtJQUN0QyxLQUFLLEVBQUUsTUFBTTtJQUNiLElBQUksRUFBRSxLQUFLO0lBQ1gsTUFBTSxFQUFFLFNBQVM7SUFDakIsS0FBSyxFQUFFLE1BQU07SUFDYixTQUFTLEVBQUUsU0FBUztJQUNwQixVQUFVLEVBQUUsWUFBWTtJQUN4QixXQUFXLEVBQUUsMkVBQTJFO0lBQ3hGLE1BQU0sRUFBRSxPQUFPO0lBQ2YsUUFBUSxFQUFFLGFBQWE7SUFDdkIsUUFBUSxFQUFFLGFBQWE7SUFDdkIsVUFBVSxFQUFFLFFBQVE7SUFDcEIsT0FBTyxFQUFFLFdBQVc7SUFDcEIsT0FBTyxFQUFFLEtBQUs7SUFDZCxPQUFPLEVBQUUsU0FBUztJQUNsQixPQUFPLEVBQUUsT0FBTztJQUNoQixVQUFVLEVBQUUsV0FBVztJQUN2QixTQUFTLEVBQUUsMENBQTBDO0lBQ3JELFFBQVEsRUFBRSxNQUFNO0lBQ2hCLFNBQVMsRUFBRSxXQUFXO0lBQ3RCLE1BQU0sRUFBRSxPQUFPO0lBQ2YsT0FBTyxFQUFFLFdBQVc7SUFDcEIsV0FBVyxFQUFFLG1CQUFtQjtJQUNoQyxNQUFNLEVBQUUsMEJBQTBCO0lBQ2xDLE1BQU0sRUFBRSx5QkFBeUI7SUFDakMsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QixJQUFJLEVBQUUscUJBQXFCO0lBQzNCLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsS0FBSyxFQUFFLHVCQUF1QjtJQUM5QixNQUFNLEVBQUUsVUFBVTtJQUNsQixRQUFRLEVBQUUsMENBQTBDO0lBQ3BELEtBQUssRUFBRSxtQkFBSztDQUNmLENBQUEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgdHV0b3IgZnJvbSAnLi92aV9WTl90dXQnXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdDaMahaSBuZ2F5JyxcbiAgICAnV0lOJzogJ05o4bqldCcsXG4gICAgJ0xPU0UyJzogJ05ow6wnLFxuICAgICdMT1NFMyc6ICdCYScsXG4gICAgJ0xPU0U0JzogJ0LDqXQnLFxuICAgICdQTEFZRVInOiAnU+G7kSBuZ8aw4budaSBjaMahaScsXG4gICAgJ0JFVE5PJzogJ03hu6ljIGPGsOG7o2MnLFxuICAgICdEQUlMWSc6ICdUaMaw4bufbmcgaMOgbmcgbmfDoHknLFxuICAgICdEMSc6ICdOZ8OgeSAxJyxcbiAgICAnRDInOiAnTmfDoHkgMicsXG4gICAgJ0QzJzogJ05nw6B5IDMnLFxuICAgICdENCc6ICdOZ8OgeSA0JyxcbiAgICAnRDUnOiAnTmfDoHkgNScsXG4gICAgJ0Q2JzogJ05nw6B5IDYnLFxuICAgICdENyc6ICdOZ8OgeSA3JyxcbiAgICAnM1RVUk5TJzogJzMgbMaw4bujdCcsXG4gICAgJzVUVVJOUyc6ICc1IGzGsOG7o3QnLFxuICAgICdUVVJOUyc6ICc1IGzGsOG7o3QnLFxuICAgICdSRUNFSVZFRCc6ICfEkMOjIG5o4bqtbicsXG4gICAgJ0xFQURFUic6ICdC4bqjbmcgeOG6v3AgaOG6oW5nJyxcbiAgICAnTk9WSURFTyc6ICdWaWRlbyBoaeG7h24ga2jDtG5nIGto4bqjIGThu6VuZycsXG4gICAgJ0JFVCc6ICdDxrDhu6NjJyxcbiAgICAnTk8nOiAnQsOgbicsXG4gICAgJ1BBU1MnOiAnQuG7jyBsxrDhu6N0JyxcbiAgICAnSElUJzogJ8SQw6FuaCcsXG4gICAgJ0FSUkFOR0UnOiAnWOG6v3AgYsOgaScsXG4gICAgJ1FVSVRHQU1FJzogJ1Rob8OhdCBnYW1lJyxcbiAgICAnUVVJVEdBTUVQJzogJ0LhuqFuIGPDsyBjaOG6r2MgbXXhu5FuIHRob8OhdCBnYW1lPyBO4bq/dSB0aG/DoXQgZ2FtZSBi4bqhbiBz4bq9IGLhu4sgdHLhu6sgMTAgbOG6p24gbeG7qWMgY8aw4bujYycsXG4gICAgJ1FVSVQnOiAnVGhvw6F0JyxcbiAgICAnM1BBSVJTJzogJzMgxJHDtGkgdGjDtG5nJyxcbiAgICAnNFBBSVJTJzogJzQgxJHDtGkgdGjDtG5nJyxcbiAgICAnRk9VUktJTkQnOiAnVOG7qSBxdcO9JyxcbiAgICAnRkxVU0gnOiAnVOG7m2kgdHLhuq9uZycsXG4gICAgJzFCRVNUJzogJ0hlbycsXG4gICAgJzJCRVNUJzogJ8SQw7RpIGhlbycsXG4gICAgJzNCRVNUJzogJzMgaGVvJyxcbiAgICAnSU5TVFJVQ1QnOiAnSMaw4bubbmcgZOG6q24nLFxuICAgICdOT01PTkVZJzogJ0LhuqFuIMSRw6MgaOG6v3QgdGnhu4FuLCBuaOG6rW4gdMOgaSB0cuG7oyB04burIG5ow6AgY8OhaScsXG4gICAgJ1JFQ0VJMic6ICdOaOG6rW4nLFxuICAgICdTUElOTk9XJzogJ1F1YXkgbmdheScsXG4gICAgJ1NQSU4nOiAnUXVheSAnLFxuICAgICcxVFVSTic6ICdUaMOqbSBsxrDhu6N0JyxcbiAgICAnTFVDS1lTUElOJzogJ1bDsm5nIHF1YXkgbWF5IG3huq9uJyxcbiAgICAnTFVDSyc6ICdDaMO6YyBi4bqhbiBtYXkgbeG6r24gbOG6p24gc2F1JyxcbiAgICAnVFVSTic6ICdC4bqhbiDEkcaw4bujYyBj4buZbmcgdGjDqm0gbMaw4bujdCcsXG4gICAgJ1gyJzogJ05ow6JuIMSRw7RpIHRp4buBbiB0aMaw4bufbmcnLFxuICAgICdYMyc6ICdOaMOibiBiYSB0aeG7gW4gdGjGsOG7n25nJyxcbiAgICAnWDUnOiAnTmjDom4gbsSDbSB0aeG7gW4gdGjGsOG7n25nJyxcbiAgICAnWDEwJzogJ05ow6JuIG3GsOG7nWkgdGnhu4FuIHRoxrDhu59uZycsXG4gICAgJ01JU1MnOiAnTeG6pXQgbMaw4bujdCcsXG4gICAgJ01PTkVZMSc6ICdDaMO6YyBt4burbmcgYuG6oW4gxJHDoyBuaOG6rW4gxJHGsOG7o2MgJXMgdOG7qyBuaMOgIGPDoWknLFxuICAgICdUVVQnOiB0dXRvclxufVxuIl19