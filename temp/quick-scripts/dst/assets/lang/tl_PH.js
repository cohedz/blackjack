
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/lang/tl_PH.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '95e70d0JFBKabdpj8E+Dcll', 'tl_PH');
// lang/tl_PH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'Maglaro',
    'WIN': 'Ika-1',
    'LOSE2': 'Ika-2',
    'LOSE3': 'Ika-3',
    'LOSE4': 'Ika-4',
    'PLAYER': 'bilang ng mga manlalaro',
    'BETNO': 'Taya',
    'DAILY': 'Daily reward',
    'D1': 'Araw 1',
    'D2': 'Araw 2',
    'D3': 'Araw 3',
    'D4': 'Araw 4',
    'D5': 'Araw 5',
    'D6': 'Araw 6',
    'D7': 'Araw 7',
    '3TURNS': '3 liko',
    '5TURNS': '5 liko',
    'TURNS': 'liko',
    'RECEIVED': 'Habol',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Hindi maaaring i-play ang video ngayon',
    'BET': 'Taya',
    'NO': 'Numero',
    'PASS': 'Pumasa',
    'HIT': 'Hit',
    'ARRANGE': 'Ayusin',
    'QUITGAME': 'Tumigil sa laro',
    'QUITGAMEP': 'Kung huminto ka sa laro mawawala sa iyo ng sampung beses bilang antas ng pusta',
    'QUIT': 'Umalis na',
    '3PAIRS': '3 magkasunod na pares',
    '4PAIRS': '4 magkasunod na pares',
    'FOURKIND': 'Apat ng isang uri',
    'FLUSH': 'Straight Flush',
    '1BEST': 'pinakamahusay',
    '2BEST': '2 pinakamahusay',
    '3BEST': '3 pinakamahusay',
    'INSTRUCT': 'Panuto',
    'NOMONEY': 'Naubusan ka ng pera, mag-click upang makatanggap ng mas maraming pera',
    'RECEI2': 'Habol',
    'SPINNOW': 'Paikutin',
    'SPIN': 'Paikutin',
    '1TURN': '1 pa naman',
    'LUCKYSPIN': 'masuwerteng paikutin',
    'LUCK': 'Mag swerte ka mamaya',
    'TURN': 'Isa pa',
    'X2': 'X2 pera',
    'X3': 'X3 pera',
    'X5': 'X5 pera',
    'X10': 'X10 pera',
    'MISS': 'Miss',
    'MONEY1': 'Pagbati! Mayroon kang %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9sYW5nL3RsX1BILnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQWdDO0FBRWhDLGtCQUFlO0lBQ1gsU0FBUyxFQUFFLFNBQVM7SUFDcEIsS0FBSyxFQUFFLE9BQU87SUFDZCxPQUFPLEVBQUUsT0FBTztJQUNoQixPQUFPLEVBQUUsT0FBTztJQUNoQixPQUFPLEVBQUUsT0FBTztJQUNoQixRQUFRLEVBQUUseUJBQXlCO0lBQ25DLE9BQU8sRUFBRyxNQUFNO0lBQ2hCLE9BQU8sRUFBRSxjQUFjO0lBQ3ZCLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsSUFBSSxFQUFFLFFBQVE7SUFDZCxJQUFJLEVBQUUsUUFBUTtJQUNkLElBQUksRUFBRSxRQUFRO0lBQ2QsUUFBUSxFQUFFLFFBQVE7SUFDbEIsUUFBUSxFQUFFLFFBQVE7SUFDbEIsT0FBTyxFQUFFLE1BQU07SUFDZixVQUFVLEVBQUUsT0FBTztJQUNuQixRQUFRLEVBQUUsYUFBYTtJQUN2QixTQUFTLEVBQUUsd0NBQXdDO0lBQ25ELEtBQUssRUFBRSxNQUFNO0lBQ2IsSUFBSSxFQUFFLFFBQVE7SUFDZCxNQUFNLEVBQUUsUUFBUTtJQUNoQixLQUFLLEVBQUUsS0FBSztJQUNaLFNBQVMsRUFBRSxRQUFRO0lBQ25CLFVBQVUsRUFBRSxpQkFBaUI7SUFDN0IsV0FBVyxFQUFFLGdGQUFnRjtJQUM3RixNQUFNLEVBQUUsV0FBVztJQUNuQixRQUFRLEVBQUUsdUJBQXVCO0lBQ2pDLFFBQVEsRUFBRSx1QkFBdUI7SUFDakMsVUFBVSxFQUFFLG1CQUFtQjtJQUMvQixPQUFPLEVBQUUsZ0JBQWdCO0lBQ3pCLE9BQU8sRUFBRSxlQUFlO0lBQ3hCLE9BQU8sRUFBRSxpQkFBaUI7SUFDMUIsT0FBTyxFQUFFLGlCQUFpQjtJQUMxQixVQUFVLEVBQUUsUUFBUTtJQUNwQixTQUFTLEVBQUUsdUVBQXVFO0lBQ2xGLFFBQVEsRUFBRSxPQUFPO0lBQ2pCLFNBQVMsRUFBRSxVQUFVO0lBQ3JCLE1BQU0sRUFBRSxVQUFVO0lBQ2xCLE9BQU8sRUFBRSxZQUFZO0lBQ3JCLFdBQVcsRUFBRSxzQkFBc0I7SUFDbkMsTUFBTSxFQUFFLHNCQUFzQjtJQUM5QixNQUFNLEVBQUUsUUFBUTtJQUNoQixJQUFJLEVBQUUsU0FBUztJQUNmLElBQUksRUFBRSxTQUFTO0lBQ2YsSUFBSSxFQUFFLFNBQVM7SUFDZixLQUFLLEVBQUUsVUFBVTtJQUNqQixNQUFNLEVBQUUsTUFBTTtJQUNkLFFBQVEsRUFBRSwwQkFBMEI7SUFDcEMsS0FBSyxFQUFFLG1CQUFLO0NBQ2YsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0dXRvciBmcm9tICcuL3RoX1RIX3R1dCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICAnUExBWU5PVyc6ICdNYWdsYXJvJyxcbiAgICAnV0lOJzogJ0lrYS0xJyxcbiAgICAnTE9TRTInOiAnSWthLTInLFxuICAgICdMT1NFMyc6ICdJa2EtMycsXG4gICAgJ0xPU0U0JzogJ0lrYS00JyxcbiAgICAnUExBWUVSJzogJ2JpbGFuZyBuZyBtZ2EgbWFubGFsYXJvJyxcbiAgICAnQkVUTk8nOiAgJ1RheWEnLFxuICAgICdEQUlMWSc6ICdEYWlseSByZXdhcmQnLFxuICAgICdEMSc6ICdBcmF3IDEnLFxuICAgICdEMic6ICdBcmF3IDInLFxuICAgICdEMyc6ICdBcmF3IDMnLFxuICAgICdENCc6ICdBcmF3IDQnLFxuICAgICdENSc6ICdBcmF3IDUnLFxuICAgICdENic6ICdBcmF3IDYnLFxuICAgICdENyc6ICdBcmF3IDcnLFxuICAgICczVFVSTlMnOiAnMyBsaWtvJyxcbiAgICAnNVRVUk5TJzogJzUgbGlrbycsXG4gICAgJ1RVUk5TJzogJ2xpa28nLFxuICAgICdSRUNFSVZFRCc6ICdIYWJvbCcsXG4gICAgJ0xFQURFUic6ICdMZWFkZXJib2FyZCcsXG4gICAgJ05PVklERU8nOiAnSGluZGkgbWFhYXJpbmcgaS1wbGF5IGFuZyB2aWRlbyBuZ2F5b24nLFxuICAgICdCRVQnOiAnVGF5YScsXG4gICAgJ05PJzogJ051bWVybycsXG4gICAgJ1BBU1MnOiAnUHVtYXNhJyxcbiAgICAnSElUJzogJ0hpdCcsXG4gICAgJ0FSUkFOR0UnOiAnQXl1c2luJyxcbiAgICAnUVVJVEdBTUUnOiAnVHVtaWdpbCBzYSBsYXJvJyxcbiAgICAnUVVJVEdBTUVQJzogJ0t1bmcgaHVtaW50byBrYSBzYSBsYXJvIG1hd2F3YWxhIHNhIGl5byBuZyBzYW1wdW5nIGJlc2VzIGJpbGFuZyBhbnRhcyBuZyBwdXN0YScsXG4gICAgJ1FVSVQnOiAnVW1hbGlzIG5hJyxcbiAgICAnM1BBSVJTJzogJzMgbWFna2FzdW5vZCBuYSBwYXJlcycsXG4gICAgJzRQQUlSUyc6ICc0IG1hZ2thc3Vub2QgbmEgcGFyZXMnLFxuICAgICdGT1VSS0lORCc6ICdBcGF0IG5nIGlzYW5nIHVyaScsXG4gICAgJ0ZMVVNIJzogJ1N0cmFpZ2h0IEZsdXNoJyxcbiAgICAnMUJFU1QnOiAncGluYWthbWFodXNheScsXG4gICAgJzJCRVNUJzogJzIgcGluYWthbWFodXNheScsXG4gICAgJzNCRVNUJzogJzMgcGluYWthbWFodXNheScsXG4gICAgJ0lOU1RSVUNUJzogJ1BhbnV0bycsXG4gICAgJ05PTU9ORVknOiAnTmF1YnVzYW4ga2EgbmcgcGVyYSwgbWFnLWNsaWNrIHVwYW5nIG1ha2F0YW5nZ2FwIG5nIG1hcyBtYXJhbWluZyBwZXJhJyxcbiAgICAnUkVDRUkyJzogJ0hhYm9sJyxcbiAgICAnU1BJTk5PVyc6ICdQYWlrdXRpbicsXG4gICAgJ1NQSU4nOiAnUGFpa3V0aW4nLFxuICAgICcxVFVSTic6ICcxIHBhIG5hbWFuJyxcbiAgICAnTFVDS1lTUElOJzogJ21hc3V3ZXJ0ZW5nIHBhaWt1dGluJyxcbiAgICAnTFVDSyc6ICdNYWcgc3dlcnRlIGthIG1hbWF5YScsXG4gICAgJ1RVUk4nOiAnSXNhIHBhJyxcbiAgICAnWDInOiAnWDIgcGVyYScsXG4gICAgJ1gzJzogJ1gzIHBlcmEnLFxuICAgICdYNSc6ICdYNSBwZXJhJyxcbiAgICAnWDEwJzogJ1gxMCBwZXJhJyxcbiAgICAnTUlTUyc6ICdNaXNzJyxcbiAgICAnTU9ORVkxJzogJ1BhZ2JhdGkhIE1heXJvb24ga2FuZyAlcycsXG4gICAgJ1RVVCc6IHR1dG9yXG59XG4iXX0=