
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Bot.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2fab9HUN9BE+7ULtb/jaU1f', 'Bot');
// Scripts/Bot.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardGroup_1 = require("./CardGroup");
var Helper_1 = require("./Helper");
var Bot = /** @class */ (function () {
    function Bot() {
    }
    Bot.random = function (handCards) {
        var cards = null;
        var firstCard = Helper_1.default.findCard(handCards, 3, 1);
        if (firstCard) {
            var straight = Bot.findAllCardByRank(handCards, 3);
            if (straight.length >= 3) {
                return new CardGroup_1.default(straight, CardGroup_1.default.House);
            }
            return new CardGroup_1.default([firstCard], CardGroup_1.default.Single);
        }
        cards = Bot.findStraightCard(handCards);
        if (cards) {
            return new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        cards = Bot.findHouseCard(handCards);
        if (cards && !(cards[0].rank == 15 && cards.length < handCards.length)) {
            return new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        var card = Helper_1.default.findMinCard(handCards);
        return new CardGroup_1.default([card], CardGroup_1.default.Single);
    };
    Bot.suggest = function (commonCards, handCards) {
        var common = commonCards.peek();
        if (common.highest.rank == 15 && common.count() == 3) {
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 2) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 4 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var pair = Helper_1.default.findHouseCard(handCards, common.highest, 2);
            if (pair) {
                return new CardGroup_1.default(pair, CardGroup_1.default.House);
            }
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 1) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 3 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var card = Helper_1.default.findMinCard(handCards, common.highest);
            if (card) {
                return new CardGroup_1.default([card], CardGroup_1.default.Single);
            }
            return null;
        }
        if (common.kind == CardGroup_1.default.Single) {
            var cards = Helper_1.default.findMinCard(handCards, common.highest);
            return cards == null ? null : new CardGroup_1.default([cards], CardGroup_1.default.Single);
        }
        if (common.kind == CardGroup_1.default.House) {
            var cards = Helper_1.default.findHouseCard(handCards, common.highest, common.count());
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        if (common.kind == CardGroup_1.default.Straight) {
            var cards = Helper_1.default.findStraightCard(handCards, common);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        if (common.kind == CardGroup_1.default.MultiPair) {
            var cards = Helper_1.default.findMultiPairCard(handCards, common.highest, common.count() / 2);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
        }
        return null;
    };
    Bot.findStraightCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            if (cardStarted.rank < 13) {
                var straight = Bot._findStraightCard(cards, cardStarted);
                if (straight.length >= 3) {
                    return straight;
                }
            }
        }
        return null;
    };
    Bot._findStraightCard = function (cards, started) {
        var straight = [started];
        for (var c = started.rank + 1; c < 15; c++) {
            var card = Helper_1.default.findCardByRank(cards, c);
            if (card == null) {
                break;
            }
            else {
                straight.push(card);
            }
        }
        return straight;
    };
    Bot.findHouseCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            var straight = Bot.findAllCardByRank(cards, cardStarted.rank);
            if (straight.length >= 2) {
                return straight;
            }
        }
        return null;
    };
    Bot.findAllCardByRank = function (cards, rank) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
            }
        }
        return setCards;
    };
    return Bot;
}());
exports.default = Bot;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0JvdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLHlDQUFvQztBQUNwQyxtQ0FBOEI7QUFHOUI7SUFBQTtJQXFJQSxDQUFDO0lBcElVLFVBQU0sR0FBYixVQUFjLFNBQXNCO1FBQ2hDLElBQUksS0FBSyxHQUFXLElBQUksQ0FBQztRQUN6QixJQUFJLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2pELElBQUksU0FBUyxFQUFFO1lBQ1gsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixPQUFPLElBQUksbUJBQVMsQ0FBQyxRQUFRLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuRDtZQUNELE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN2RDtRQUVELEtBQUssR0FBRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEMsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNuRDtRQUVELEtBQUssR0FBRyxHQUFHLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNwRSxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoRDtRQUVELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sV0FBTyxHQUFkLFVBQWUsV0FBdUIsRUFBRSxTQUFzQjtRQUMxRCxJQUFJLE1BQU0sR0FBRyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFaEMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNsRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNsRCxJQUFJLEtBQUssR0FBRyxnQkFBTSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksS0FBSyxFQUFFO2dCQUNQLE9BQU8sSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRSxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BEO1lBQ0QsSUFBSSxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwRCxJQUFJLElBQUksRUFBRTtnQkFDTixPQUFPLElBQUksbUJBQVMsQ0FBQyxJQUFJLEVBQUUsbUJBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMvQztZQUNELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksSUFBSSxFQUFFO2dCQUNOLE9BQU8sSUFBSSxtQkFBUyxDQUFDLElBQUksRUFBRSxtQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQy9DO1lBQ0QsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDbEQsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUM3RCxJQUFJLEtBQUssRUFBRTtnQkFDUCxPQUFPLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwRDtZQUNELElBQUksSUFBSSxHQUFHLGdCQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDcEQsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sT0FBTyxJQUFJLG1CQUFTLENBQUMsSUFBSSxFQUFFLG1CQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDL0M7WUFDRCxJQUFJLElBQUksR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pELElBQUksSUFBSSxFQUFFO2dCQUNOLE9BQU8sSUFBSSxtQkFBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNsRDtZQUNELE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDakMsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxtQkFBUyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMxRTtRQUVELElBQUksTUFBTSxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLEtBQUssR0FBRyxnQkFBTSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUM1RSxPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxtQkFBUyxDQUFDLEtBQUssRUFBRSxtQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZFO1FBRUQsSUFBSSxNQUFNLENBQUMsSUFBSSxJQUFJLG1CQUFTLENBQUMsUUFBUSxFQUFFO1lBQ25DLElBQUksS0FBSyxHQUFHLGdCQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLG1CQUFTLENBQUMsS0FBSyxFQUFFLG1CQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUU7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxTQUFTLEVBQUU7WUFDcEMsSUFBSSxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEYsT0FBTyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksbUJBQVMsQ0FBQyxLQUFLLEVBQUUsbUJBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMzRTtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxvQkFBZ0IsR0FBdkIsVUFBd0IsS0FBYTtRQUNqQyxnQkFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxXQUFXLENBQUMsSUFBSSxHQUFHLEVBQUUsRUFBRTtnQkFDdkIsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDekQsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtvQkFDdEIsT0FBTyxRQUFRLENBQUM7aUJBQ25CO2FBQ0o7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxxQkFBaUIsR0FBeEIsVUFBeUIsS0FBYSxFQUFFLE9BQWE7UUFDakQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtnQkFBRSxNQUFNO2FBQUU7aUJBQ3ZCO2dCQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFBRTtTQUNoQztRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFTSxpQkFBYSxHQUFwQixVQUFxQixLQUFhO1FBQzlCLGdCQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RCxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixPQUFPLFFBQVEsQ0FBQzthQUNuQjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLHFCQUFpQixHQUF4QixVQUF5QixLQUFhLEVBQUUsSUFBWTtRQUNoRCxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsS0FBSyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3hDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDM0I7U0FDSjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDTCxVQUFDO0FBQUQsQ0FySUEsQUFxSUMsSUFBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcbmltcG9ydCBDYXJkR3JvdXAgZnJvbSBcIi4vQ2FyZEdyb3VwXCI7XG5pbXBvcnQgSGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuaW1wb3J0IENvbW1vbkNhcmQgZnJvbSBcIi4vQ29tbW9uQ2FyZFwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCb3Qge1xuICAgIHN0YXRpYyByYW5kb20oaGFuZENhcmRzOiBBcnJheTxDYXJkPik6IENhcmRHcm91cCB7XG4gICAgICAgIGxldCBjYXJkczogQ2FyZFtdID0gbnVsbDtcbiAgICAgICAgbGV0IGZpcnN0Q2FyZCA9IEhlbHBlci5maW5kQ2FyZChoYW5kQ2FyZHMsIDMsIDEpO1xuICAgICAgICBpZiAoZmlyc3RDYXJkKSB7XG4gICAgICAgICAgICBsZXQgc3RyYWlnaHQgPSBCb3QuZmluZEFsbENhcmRCeVJhbmsoaGFuZENhcmRzLCAzKTtcbiAgICAgICAgICAgIGlmIChzdHJhaWdodC5sZW5ndGggPj0gMykge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKHN0cmFpZ2h0LCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoW2ZpcnN0Q2FyZF0sIENhcmRHcm91cC5TaW5nbGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FyZHMgPSBCb3QuZmluZFN0cmFpZ2h0Q2FyZChoYW5kQ2FyZHMpO1xuICAgICAgICBpZiAoY2FyZHMpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKGNhcmRzLCBDYXJkR3JvdXAuU3RyYWlnaHQpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FyZHMgPSBCb3QuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMpO1xuICAgICAgICBpZiAoY2FyZHMgJiYgIShjYXJkc1swXS5yYW5rID09IDE1ICYmIGNhcmRzLmxlbmd0aCA8IGhhbmRDYXJkcy5sZW5ndGgpKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChjYXJkcywgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBjYXJkID0gSGVscGVyLmZpbmRNaW5DYXJkKGhhbmRDYXJkcyk7XG4gICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKFtjYXJkXSwgQ2FyZEdyb3VwLlNpbmdsZSk7XG4gICAgfVxuXG4gICAgc3RhdGljIHN1Z2dlc3QoY29tbW9uQ2FyZHM6IENvbW1vbkNhcmQsIGhhbmRDYXJkczogQXJyYXk8Q2FyZD4pOiBDYXJkR3JvdXAge1xuICAgICAgICBsZXQgY29tbW9uID0gY29tbW9uQ2FyZHMucGVlaygpO1xuXG4gICAgICAgIGlmIChjb21tb24uaGlnaGVzdC5yYW5rID09IDE1ICYmIGNvbW1vbi5jb3VudCgpID09IDMpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgY29tbW9uLmNvdW50KCkgPT0gMikge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgbnVsbCwgNCAqIDIpO1xuICAgICAgICAgICAgaWYgKGNhcmRzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5NdWx0aVBhaXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGZvdXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIG51bGwsIDQpO1xuICAgICAgICAgICAgaWYgKGZvdXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChmb3VyLCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IHBhaXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIGNvbW1vbi5oaWdoZXN0LCAyKTtcbiAgICAgICAgICAgIGlmIChwYWlyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAocGFpciwgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgY29tbW9uLmNvdW50KCkgPT0gMSkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgbnVsbCwgMyAqIDIpO1xuICAgICAgICAgICAgaWYgKGNhcmRzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5NdWx0aVBhaXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGZvdXIgPSBIZWxwZXIuZmluZEhvdXNlQ2FyZChoYW5kQ2FyZHMsIG51bGwsIDQpO1xuICAgICAgICAgICAgaWYgKGZvdXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENhcmRHcm91cChmb3VyLCBDYXJkR3JvdXAuSG91c2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGNhcmQgPSBIZWxwZXIuZmluZE1pbkNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCk7XG4gICAgICAgICAgICBpZiAoY2FyZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ2FyZEdyb3VwKFtjYXJkXSwgQ2FyZEdyb3VwLlNpbmdsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb21tb24ua2luZCA9PSBDYXJkR3JvdXAuU2luZ2xlKSB7XG4gICAgICAgICAgICBsZXQgY2FyZHMgPSBIZWxwZXIuZmluZE1pbkNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCk7XG4gICAgICAgICAgICByZXR1cm4gY2FyZHMgPT0gbnVsbCA/IG51bGwgOiBuZXcgQ2FyZEdyb3VwKFtjYXJkc10sIENhcmRHcm91cC5TaW5nbGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNvbW1vbi5raW5kID09IENhcmRHcm91cC5Ib3VzZSkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRIb3VzZUNhcmQoaGFuZENhcmRzLCBjb21tb24uaGlnaGVzdCwgY29tbW9uLmNvdW50KCkpO1xuICAgICAgICAgICAgcmV0dXJuIGNhcmRzID09IG51bGwgPyBudWxsIDogbmV3IENhcmRHcm91cChjYXJkcywgQ2FyZEdyb3VwLkhvdXNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb21tb24ua2luZCA9PSBDYXJkR3JvdXAuU3RyYWlnaHQpIHtcbiAgICAgICAgICAgIGxldCBjYXJkcyA9IEhlbHBlci5maW5kU3RyYWlnaHRDYXJkKGhhbmRDYXJkcywgY29tbW9uKTtcbiAgICAgICAgICAgIHJldHVybiBjYXJkcyA9PSBudWxsID8gbnVsbCA6IG5ldyBDYXJkR3JvdXAoY2FyZHMsIENhcmRHcm91cC5TdHJhaWdodCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoY29tbW9uLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpcikge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNdWx0aVBhaXJDYXJkKGhhbmRDYXJkcywgY29tbW9uLmhpZ2hlc3QsIGNvbW1vbi5jb3VudCgpIC8gMik7XG4gICAgICAgICAgICByZXR1cm4gY2FyZHMgPT0gbnVsbCA/IG51bGwgOiBuZXcgQ2FyZEdyb3VwKGNhcmRzLCBDYXJkR3JvdXAuTXVsdGlQYWlyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHN0YXRpYyBmaW5kU3RyYWlnaHRDYXJkKGNhcmRzOiBDYXJkW10pOiBBcnJheTxDYXJkPiB7XG4gICAgICAgIEhlbHBlci5zb3J0KGNhcmRzKTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYXJkcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGNhcmRTdGFydGVkID0gY2FyZHNbaV07XG4gICAgICAgICAgICBpZiAoY2FyZFN0YXJ0ZWQucmFuayA8IDEzKSB7XG4gICAgICAgICAgICAgICAgbGV0IHN0cmFpZ2h0ID0gQm90Ll9maW5kU3RyYWlnaHRDYXJkKGNhcmRzLCBjYXJkU3RhcnRlZCk7XG4gICAgICAgICAgICAgICAgaWYgKHN0cmFpZ2h0Lmxlbmd0aCA+PSAzKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc3RhdGljIF9maW5kU3RyYWlnaHRDYXJkKGNhcmRzOiBDYXJkW10sIHN0YXJ0ZWQ6IENhcmQpOiBBcnJheTxDYXJkPiB7XG4gICAgICAgIGxldCBzdHJhaWdodCA9IFtzdGFydGVkXTtcbiAgICAgICAgZm9yIChsZXQgYyA9IHN0YXJ0ZWQucmFuayArIDE7IGMgPCAxNTsgYysrKSB7XG4gICAgICAgICAgICBsZXQgY2FyZCA9IEhlbHBlci5maW5kQ2FyZEJ5UmFuayhjYXJkcywgYyk7XG4gICAgICAgICAgICBpZiAoY2FyZCA9PSBudWxsKSB7IGJyZWFrOyB9XG4gICAgICAgICAgICBlbHNlIHsgc3RyYWlnaHQucHVzaChjYXJkKTsgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZEhvdXNlQ2FyZChjYXJkczogQ2FyZFtdKTogQXJyYXk8Q2FyZD4ge1xuICAgICAgICBIZWxwZXIuc29ydChjYXJkcyk7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2FyZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBjYXJkU3RhcnRlZCA9IGNhcmRzW2ldO1xuICAgICAgICAgICAgbGV0IHN0cmFpZ2h0ID0gQm90LmZpbmRBbGxDYXJkQnlSYW5rKGNhcmRzLCBjYXJkU3RhcnRlZC5yYW5rKTtcbiAgICAgICAgICAgIGlmIChzdHJhaWdodC5sZW5ndGggPj0gMikge1xuICAgICAgICAgICAgICAgIHJldHVybiBzdHJhaWdodDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZmluZEFsbENhcmRCeVJhbmsoY2FyZHM6IENhcmRbXSwgcmFuazogbnVtYmVyKTogQ2FyZFtdIHtcbiAgICAgICAgbGV0IHNldENhcmRzID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGNhcmRzW2ldLnJhbmsgPT0gcmFuaykge1xuICAgICAgICAgICAgICAgIHNldENhcmRzLnB1c2goY2FyZHNbaV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzZXRDYXJkcztcbiAgICB9XG59XG4iXX0=