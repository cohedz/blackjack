
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Home.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2c5ff9R/FJPip0TZkafs6WJ', 'Home');
// Scripts/Home.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Slider2_1 = require("./Slider2");
var Config_1 = require("./Config");
var Leaderboard_1 = require("./Leaderboard");
var util_1 = require("./util");
var Popup_1 = require("./Popup");
var DailyBonus_1 = require("./popop/bonus/DailyBonus");
var AutoHide_1 = require("./AutoHide");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Home = /** @class */ (function (_super) {
    __extends(Home, _super);
    function Home() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.avatar = null;
        _this.playerName = null;
        _this.coin = null;
        _this.betSetting = null;
        _this.leaderboard = null;
        _this.music = null;
        _this.dailyBonus = null;
        _this.popup = null;
        _this.toast = null;
        _this.modal = null;
        _this.playTime = 0;
        _this.countTime = 0;
        _this.itvCountTime = null;
        _this.preloadSceneGame = function () {
            cc.director.preloadScene('game', function (completedCount, totalCount, item) {
                // console.log('====================================');
                // console.log('preload', completedCount, totalCount, item);
                // console.log('====================================');
            }, function (error) {
                console.log('====================================');
                console.log('loaded game');
                console.log('====================================');
            });
        };
        _this.onChallenge = function (playerId, photo, username, coin) { return __awaiter(_this, void 0, void 0, function () {
            var error_1, minCoin;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Api_1.default.coin <= Config_1.default.bankrupt) {
                            Api_1.default.preloadRewardedVideo();
                            this.popup.open(this.node, 4);
                            Api_1.default.showInterstitialAd();
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, Api_1.default.challenge(playerId)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 4];
                    case 4:
                        if (Math.random() < 0.7) {
                            Api_1.default.showInterstitialAd();
                        }
                        cc.audioEngine.stopMusic();
                        Config_1.default.battle = { photo: photo, username: username, coin: coin };
                        minCoin = Math.min(Math.max(coin, 100000), Api_1.default.coin);
                        Config_1.default.betValue = Math.floor(minCoin * 0.3);
                        cc.director.loadScene('game');
                        Api_1.default.logEvent(EventKeys_1.default.PLAY_WITH_FRIEND);
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    Home.prototype.initCountTime = function () {
        var _this = this;
        this.itvCountTime = setInterval(function () {
            _this.countTime += Config_1.default.stepOfCountTime / 1000;
            util_1.default.logTimeInGame(_this.countTime);
        }, Config_1.default.stepOfCountTime);
    };
    Home.prototype.clearLogTime = function () {
        this.itvCountTime && clearInterval(this.itvCountTime);
        this.itvCountTime = null;
        this.countTime = 0;
    };
    // init logic
    Home.prototype.start = function () {
        var _this = this;
        util_1.default.logTimeInGame(0);
        this.initCountTime();
        this.betSetting.onValueChange = this.onBetChange;
        Api_1.default.initAsync(function (bonus, day) {
            Language_1.default.getInstance().load(Api_1.default.locale);
            cc.systemEvent.emit('LANG_CHAN');
            _this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
            _this.playerName.string = Api_1.default.username;
            cc.assetManager.loadRemote(Api_1.default.photo, function (err, tex) {
                _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
                Config_1.default.userphoto = tex;
            });
            if (bonus) {
                Api_1.default.preloadRewardedVideo(function () {
                    Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_SHOW);
                    _this.dailyBonus.show(day, Api_1.default.dailyBonusClaimed);
                });
                cc.systemEvent.on('claim_daily_bonus', _this.onClaimDailyBonus, _this);
                cc.systemEvent.on('claim_daily_bonus_fail', _this.onClaimDailyBonusFail, _this);
            }
        }, this.leaderboard);
        cc.systemEvent.on('lb_battle', this.onChallenge, this);
        cc.systemEvent.on('lb_share', this.onShare, this);
        cc.audioEngine.setMusicVolume(0.3);
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        // this.scheduleOnce(this.preloadSceneGame, 1);
        setTimeout(function () {
            _this.preloadSceneGame();
        }, 1);
        Api_1.default.preloadInterstitialAd();
    };
    Home.prototype.onShare = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Api_1.default.shareAsync()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Home.prototype.onClaimDailyBonus = function (day, bonus, extra) {
        if (bonus.coin) {
            Api_1.default.coinIncrement(extra ? 2 * bonus.coin : bonus.coin);
            this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        }
        if (bonus.ticket) {
            Api_1.default.ticket += extra ? 2 * bonus.ticket : bonus.ticket;
        }
        Api_1.default.claimDailyBonus(day);
    };
    Home.prototype.onClaimDailyBonusFail = function () {
        this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
    };
    Home.prototype.openPlayPopupClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_NOW);
        if (Api_1.default.coin > Config_1.default.bankrupt) {
            this.popup.open(this.node, 1);
        }
        else {
            Api_1.default.preloadRewardedVideo();
            Api_1.default.showInterstitialAd();
            this.popup.open(this.node, 4);
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
        }
    };
    Home.prototype.onPlayClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_ACCEPT);
        Config_1.default.battle = null;
        cc.audioEngine.stopMusic();
        cc.director.loadScene('game');
    };
    Home.prototype.onShopClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.SHOP_GO);
        cc.director.loadScene('shop');
    };
    Home.prototype.onDailyBonusClick = function () {
        Api_1.default.logEvent(EventKeys_1.default.DAILY_GIFT_CLICK);
        this.dailyBonus.show(Api_1.default.dailyBonusDay, Api_1.default.dailyBonusClaimed);
        Api_1.default.showInterstitialAd();
    };
    Home.prototype.onBetChange = function (value) {
        Config_1.default.betValue = value;
    };
    Home.prototype.onBotChange = function (sender, params) {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_MODE + '-' + params);
        if (sender.isChecked) {
            Config_1.default.totalPlayer = parseInt(params);
        }
    };
    Home.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
        if (Config_1.default.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }
        else {
            cc.audioEngine.stopMusic();
        }
    };
    Home.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.openWithText(null, msg);
        Api_1.default.coinIncrement(bonus);
        this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        this.popup.close(null, 4);
    };
    Home.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 2);
            _this.toast.openWithText(null, 'Mời bạn chơi không thành công');
        });
    };
    Home.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    __decorate([
        property(cc.Sprite)
    ], Home.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], Home.prototype, "coin", void 0);
    __decorate([
        property(Slider2_1.default)
    ], Home.prototype, "betSetting", void 0);
    __decorate([
        property(Leaderboard_1.default)
    ], Home.prototype, "leaderboard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Home.prototype, "music", void 0);
    __decorate([
        property(DailyBonus_1.default)
    ], Home.prototype, "dailyBonus", void 0);
    __decorate([
        property(Popup_1.default)
    ], Home.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], Home.prototype, "toast", void 0);
    __decorate([
        property(Modal_1.default)
    ], Home.prototype, "modal", void 0);
    Home = __decorate([
        ccclass
    ], Home);
    return Home;
}(cc.Component));
exports.default = Home;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0hvbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkJBQXdCO0FBQ3hCLHFDQUFnQztBQUNoQyxtQ0FBOEI7QUFDOUIsNkNBQXdDO0FBQ3hDLCtCQUEwQjtBQUMxQixpQ0FBNEI7QUFDNUIsdURBQWtEO0FBQ2xELHVDQUFrQztBQUNsQyx1Q0FBa0M7QUFDbEMseUNBQW9DO0FBQ3BDLHVDQUFrQztBQUU1QixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFrQyx3QkFBWTtJQUE5QztRQUFBLHFFQXlOQztRQXZORyxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBRXpCLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBRTVCLFVBQUksR0FBYSxJQUFJLENBQUM7UUFFdEIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFFM0IsaUJBQVcsR0FBZ0IsSUFBSSxDQUFDO1FBRWhDLFdBQUssR0FBaUIsSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUN0QixrQkFBWSxHQUFHLElBQUksQ0FBQztRQXlEcEIsc0JBQWdCLEdBQUc7WUFDZixFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQzNCLFVBQUMsY0FBYyxFQUFFLFVBQVUsRUFBRSxJQUFJO2dCQUM3Qix1REFBdUQ7Z0JBQ3ZELDREQUE0RDtnQkFDNUQsdURBQXVEO1lBQzNELENBQUMsRUFDRCxVQUFDLEtBQUs7Z0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDeEQsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLENBQUE7UUFNRCxpQkFBVyxHQUFHLFVBQU8sUUFBZ0IsRUFBRSxLQUFtQixFQUFFLFFBQWdCLEVBQUUsSUFBWTs7Ozs7d0JBQ3RGLElBQUksYUFBRyxDQUFDLElBQUksSUFBSSxnQkFBTSxDQUFDLFFBQVEsRUFBRTs0QkFDN0IsYUFBRyxDQUFDLG9CQUFvQixFQUFFLENBQUM7NEJBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7NEJBQzlCLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDOzRCQUN6QixzQkFBTzt5QkFDVjs7Ozt3QkFHRyxxQkFBTSxhQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzs7Ozt3QkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFLLENBQUMsQ0FBQTs7O3dCQUVwQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLEVBQUU7NEJBQ3JCLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO3lCQUM1Qjt3QkFFRCxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO3dCQUMzQixnQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssT0FBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7d0JBQ3RDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUFFLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDekQsZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7d0JBQzVDLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUM5QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs7OzthQUM1QyxDQUFBOztJQStGTCxDQUFDO0lBL0xHLDRCQUFhLEdBQWI7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxTQUFTLElBQUksZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFBO1lBQy9DLGNBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3RDLENBQUMsRUFBRSxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO0lBQzlCLENBQUM7SUFFRCwyQkFBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFlBQVksSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO0lBQ3RCLENBQUM7SUFFRCxhQUFhO0lBQ2Isb0JBQUssR0FBTDtRQUFBLGlCQXVDQztRQXJDRyxjQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUVwQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2pELGFBQUcsQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFjLEVBQUUsR0FBVztZQUN0QyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxhQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsYUFBRyxDQUFDLFFBQVEsQ0FBQztZQUN0QyxFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBZSxhQUFHLENBQUMsS0FBSyxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7Z0JBQ3pELEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbEQsZ0JBQU0sQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQzNCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsYUFBRyxDQUFDLG9CQUFvQixDQUFDO29CQUNyQixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsdUJBQXVCLENBQUMsQ0FBQztvQkFDaEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUMsQ0FBQztnQkFDSCxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxLQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLENBQUM7Z0JBQ3JFLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLHdCQUF3QixFQUFFLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxLQUFJLENBQUMsQ0FBQzthQUNqRjtRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFckIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFbEQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkMsSUFBSSxnQkFBTSxDQUFDLFdBQVcsRUFBRTtZQUNwQixFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzlDO1FBRUQsK0NBQStDO1FBQy9DLFVBQVUsQ0FBQztZQUNQLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO1FBQzNCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBRWhDLENBQUM7SUFpQkssc0JBQU8sR0FBYjs7Ozs0QkFDSSxxQkFBTSxhQUFHLENBQUMsVUFBVSxFQUFFLEVBQUE7O3dCQUF0QixTQUFzQixDQUFDOzs7OztLQUMxQjtJQTBCRCxnQ0FBaUIsR0FBakIsVUFBa0IsR0FBVyxFQUFFLEtBQXVDLEVBQUUsS0FBYztRQUNsRixJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDWixhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsRDtRQUNELElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLGFBQUcsQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztTQUN6RDtRQUNELGFBQUcsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELG9DQUFxQixHQUFyQjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELGlDQUFrQixHQUFsQjtRQUNJLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqQyxJQUFJLGFBQUcsQ0FBQyxJQUFJLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNqQzthQUFNO1lBQ0gsYUFBRyxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDM0IsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7SUFFRCwwQkFBVyxHQUFYO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BDLGdCQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNyQixFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCwwQkFBVyxHQUFYO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFHLENBQUMsYUFBYSxFQUFFLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9ELGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCwwQkFBVyxHQUFYLFVBQVksS0FBYTtRQUNyQixnQkFBTSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVELDBCQUFXLEdBQVgsVUFBWSxNQUFpQixFQUFFLE1BQWM7UUFDekMsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFDakQsSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ2xCLGdCQUFNLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFRCw0QkFBYSxHQUFiLFVBQWMsTUFBaUIsRUFBRSxJQUFhO1FBQzFDLGdCQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUV2QyxJQUFJLGdCQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3BCLEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDOUM7YUFBTTtZQUNILEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBRUQsbUNBQW9CLEdBQXBCLFVBQXFCLEtBQWE7UUFDOUIsSUFBTSxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEVBQUUsY0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNuQyxhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsMkJBQVksR0FBWjtRQUFBLGlCQVFDO1FBUEcsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3RDLGFBQUcsQ0FBQyxNQUFNLENBQUM7WUFDUCxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUU7WUFDQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLCtCQUErQixDQUFDLENBQUM7UUFDbkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUJBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEcsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDbEQsYUFBRyxDQUFDLGlCQUFpQixDQUFDO1lBQ2xCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBRTtZQUNDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM3QyxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUF0TkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt3Q0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzRDQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0NBQ0c7SUFFdEI7UUFEQyxRQUFRLENBQUMsaUJBQU8sQ0FBQzs0Q0FDUztJQUUzQjtRQURDLFFBQVEsQ0FBQyxxQkFBVyxDQUFDOzZDQUNVO0lBRWhDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzt1Q0FDTjtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzRDQUNTO0lBRTlCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt1Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDO3VDQUNJO0lBRXZCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt1Q0FDSTtJQXBCSCxJQUFJO1FBRHhCLE9BQU87T0FDYSxJQUFJLENBeU54QjtJQUFELFdBQUM7Q0F6TkQsQUF5TkMsQ0F6TmlDLEVBQUUsQ0FBQyxTQUFTLEdBeU43QztrQkF6Tm9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IFNsaWRlcjIgZnJvbSBcIi4vU2xpZGVyMlwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCBMZWFkZXJib2FyZCBmcm9tIFwiLi9MZWFkZXJib2FyZFwiO1xuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IFBvcHVwIGZyb20gXCIuL1BvcHVwXCI7XG5pbXBvcnQgRGFpbHlCb251cyBmcm9tIFwiLi9wb3BvcC9ib251cy9EYWlseUJvbnVzXCI7XG5pbXBvcnQgQXV0b0hpZGUgZnJvbSBcIi4vQXV0b0hpZGVcIjtcbmltcG9ydCBNb2RhbCBmcm9tIFwiLi9wb3BvcC9Nb2RhbFwiO1xuaW1wb3J0IEV2ZW50S2V5cyBmcm9tIFwiLi9FdmVudEtleXNcIjtcbmltcG9ydCBMYW5ndWFnZSBmcm9tIFwiLi9MYW5ndWFnZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSG9tZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBsYXllck5hbWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgY29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShTbGlkZXIyKVxuICAgIGJldFNldHRpbmc6IFNsaWRlcjIgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShMZWFkZXJib2FyZClcbiAgICBsZWFkZXJib2FyZDogTGVhZGVyYm9hcmQgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIG11c2ljOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShEYWlseUJvbnVzKVxuICAgIGRhaWx5Qm9udXM6IERhaWx5Qm9udXMgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShBdXRvSGlkZSlcbiAgICB0b2FzdDogQXV0b0hpZGUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShNb2RhbClcbiAgICBtb2RhbDogTW9kYWwgPSBudWxsO1xuXG4gICAgcGxheVRpbWU6IG51bWJlciA9IDA7XG4gICAgY291bnRUaW1lOiBudW1iZXIgPSAwO1xuICAgIGl0dkNvdW50VGltZSA9IG51bGw7XG5cbiAgICBpbml0Q291bnRUaW1lKCkge1xuICAgICAgICB0aGlzLml0dkNvdW50VGltZSA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY291bnRUaW1lICs9IENvbmZpZy5zdGVwT2ZDb3VudFRpbWUgLyAxMDAwXG4gICAgICAgICAgICB1dGlsLmxvZ1RpbWVJbkdhbWUodGhpcy5jb3VudFRpbWUpXG4gICAgICAgIH0sIENvbmZpZy5zdGVwT2ZDb3VudFRpbWUpXG4gICAgfVxuXG4gICAgY2xlYXJMb2dUaW1lKCkge1xuICAgICAgICB0aGlzLml0dkNvdW50VGltZSAmJiBjbGVhckludGVydmFsKHRoaXMuaXR2Q291bnRUaW1lKTtcbiAgICAgICAgdGhpcy5pdHZDb3VudFRpbWUgPSBudWxsO1xuICAgICAgICB0aGlzLmNvdW50VGltZSA9IDBcbiAgICB9XG5cbiAgICAvLyBpbml0IGxvZ2ljXG4gICAgc3RhcnQoKSB7XG5cbiAgICAgICAgdXRpbC5sb2dUaW1lSW5HYW1lKDApO1xuICAgICAgICB0aGlzLmluaXRDb3VudFRpbWUoKVxuXG4gICAgICAgIHRoaXMuYmV0U2V0dGluZy5vblZhbHVlQ2hhbmdlID0gdGhpcy5vbkJldENoYW5nZTtcbiAgICAgICAgQXBpLmluaXRBc3luYygoYm9udXM6IGJvb2xlYW4sIGRheTogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmxvYWQoQXBpLmxvY2FsZSk7XG4gICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KCdMQU5HX0NIQU4nKTtcbiAgICAgICAgICAgIHRoaXMuY29pbi5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdChBcGkuY29pbik7XG4gICAgICAgICAgICB0aGlzLnBsYXllck5hbWUuc3RyaW5nID0gQXBpLnVzZXJuYW1lO1xuICAgICAgICAgICAgY2MuYXNzZXRNYW5hZ2VyLmxvYWRSZW1vdGU8Y2MuVGV4dHVyZTJEPihBcGkucGhvdG8sIChlcnIsIHRleCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuYXZhdGFyLnNwcml0ZUZyYW1lID0gbmV3IGNjLlNwcml0ZUZyYW1lKHRleCk7XG4gICAgICAgICAgICAgICAgQ29uZmlnLnVzZXJwaG90byA9IHRleDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKGJvbnVzKSB7XG4gICAgICAgICAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9EQUlMWV9SRVdBUkRfU0hPVyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGFpbHlCb251cy5zaG93KGRheSwgQXBpLmRhaWx5Qm9udXNDbGFpbWVkKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignY2xhaW1fZGFpbHlfYm9udXMnLCB0aGlzLm9uQ2xhaW1EYWlseUJvbnVzLCB0aGlzKTtcbiAgICAgICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignY2xhaW1fZGFpbHlfYm9udXNfZmFpbCcsIHRoaXMub25DbGFpbURhaWx5Qm9udXNGYWlsLCB0aGlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgdGhpcy5sZWFkZXJib2FyZCk7XG5cbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oJ2xiX2JhdHRsZScsIHRoaXMub25DaGFsbGVuZ2UsIHRoaXMpO1xuICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbignbGJfc2hhcmUnLCB0aGlzLm9uU2hhcmUsIHRoaXMpO1xuXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnNldE11c2ljVm9sdW1lKDAuMyk7XG4gICAgICAgIGlmIChDb25maWcuc291bmRFbmFibGUpIHtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlNdXNpYyh0aGlzLm11c2ljLCB0cnVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHRoaXMuc2NoZWR1bGVPbmNlKHRoaXMucHJlbG9hZFNjZW5lR2FtZSwgMSk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5wcmVsb2FkU2NlbmVHYW1lKClcbiAgICAgICAgfSwgMSk7XG4gICAgICAgIEFwaS5wcmVsb2FkSW50ZXJzdGl0aWFsQWQoKTtcblxuICAgIH1cblxuICAgIHByZWxvYWRTY2VuZUdhbWUgPSAoKSA9PiB7XG4gICAgICAgIGNjLmRpcmVjdG9yLnByZWxvYWRTY2VuZSgnZ2FtZScsXG4gICAgICAgICAgICAoY29tcGxldGVkQ291bnQsIHRvdGFsQ291bnQsIGl0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ3ByZWxvYWQnLCBjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCwgaXRlbSk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbG9hZGVkIGdhbWUnKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgYXN5bmMgb25TaGFyZSgpIHtcbiAgICAgICAgYXdhaXQgQXBpLnNoYXJlQXN5bmMoKTtcbiAgICB9XG5cbiAgICBvbkNoYWxsZW5nZSA9IGFzeW5jIChwbGF5ZXJJZDogc3RyaW5nLCBwaG90bzogY2MuVGV4dHVyZTJELCB1c2VybmFtZTogc3RyaW5nLCBjb2luOiBudW1iZXIpID0+IHtcbiAgICAgICAgaWYgKEFwaS5jb2luIDw9IENvbmZpZy5iYW5rcnVwdCkge1xuICAgICAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCk7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLm9wZW4odGhpcy5ub2RlLCA0KTtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCBBcGkuY2hhbGxlbmdlKHBsYXllcklkKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHsgY29uc29sZS5sb2coZXJyb3IpIH1cblxuICAgICAgICBpZiAoTWF0aC5yYW5kb20oKSA8IDAuNykge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc3RvcE11c2ljKCk7XG4gICAgICAgIENvbmZpZy5iYXR0bGUgPSB7IHBob3RvLCB1c2VybmFtZSwgY29pbiB9O1xuICAgICAgICBsZXQgbWluQ29pbiA9IE1hdGgubWluKE1hdGgubWF4KGNvaW4sIDEwMDAwMCksIEFwaS5jb2luKTtcbiAgICAgICAgQ29uZmlnLmJldFZhbHVlID0gTWF0aC5mbG9vcihtaW5Db2luICogMC4zKTtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdnYW1lJyk7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9XSVRIX0ZSSUVORCk7XG4gICAgfVxuXG4gICAgb25DbGFpbURhaWx5Qm9udXMoZGF5OiBudW1iZXIsIGJvbnVzOiB7IGNvaW46IG51bWJlciwgdGlja2V0OiBudW1iZXIgfSwgZXh0cmE6IGJvb2xlYW4pIHtcbiAgICAgICAgaWYgKGJvbnVzLmNvaW4pIHtcbiAgICAgICAgICAgIEFwaS5jb2luSW5jcmVtZW50KGV4dHJhID8gMiAqIGJvbnVzLmNvaW4gOiBib251cy5jb2luKTtcbiAgICAgICAgICAgIHRoaXMuY29pbi5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdChBcGkuY29pbik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGJvbnVzLnRpY2tldCkge1xuICAgICAgICAgICAgQXBpLnRpY2tldCArPSBleHRyYSA/IDIgKiBib251cy50aWNrZXQgOiBib251cy50aWNrZXQ7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLmNsYWltRGFpbHlCb251cyhkYXkpO1xuICAgIH1cblxuICAgIG9uQ2xhaW1EYWlseUJvbnVzRmFpbCgpIHtcbiAgICAgICAgdGhpcy5tb2RhbC5zaG93KExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdOT1ZJREVPJykpO1xuICAgIH1cblxuICAgIG9wZW5QbGF5UG9wdXBDbGljaygpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QTEFZX05PVyk7XG4gICAgICAgIGlmIChBcGkuY29pbiA+IENvbmZpZy5iYW5rcnVwdCkge1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBBcGkucHJlbG9hZFJld2FyZGVkVmlkZW8oKTtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgICAgIHRoaXMucG9wdXAub3Blbih0aGlzLm5vZGUsIDQpO1xuICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9DT0lOX09QRU4pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25QbGF5Q2xpY2soKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9BQ0NFUFQpO1xuICAgICAgICBDb25maWcuYmF0dGxlID0gbnVsbDtcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc3RvcE11c2ljKCk7XG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnZ2FtZScpO1xuICAgIH1cblxuICAgIG9uU2hvcENsaWNrKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlNIT1BfR08pO1xuICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoJ3Nob3AnKTtcbiAgICB9XG5cbiAgICBvbkRhaWx5Qm9udXNDbGljaygpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5EQUlMWV9HSUZUX0NMSUNLKTtcbiAgICAgICAgdGhpcy5kYWlseUJvbnVzLnNob3coQXBpLmRhaWx5Qm9udXNEYXksIEFwaS5kYWlseUJvbnVzQ2xhaW1lZCk7XG4gICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICB9XG5cbiAgICBvbkJldENoYW5nZSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIENvbmZpZy5iZXRWYWx1ZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIG9uQm90Q2hhbmdlKHNlbmRlcjogY2MuVG9nZ2xlLCBwYXJhbXM6IHN0cmluZykge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfTU9ERSArICctJyArIHBhcmFtcyk7XG4gICAgICAgIGlmIChzZW5kZXIuaXNDaGVja2VkKSB7XG4gICAgICAgICAgICBDb25maWcudG90YWxQbGF5ZXIgPSBwYXJzZUludChwYXJhbXMpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25Tb3VuZFRvZ2dsZShzZW5kZXI6IGNjLlRvZ2dsZSwgaXNPbjogYm9vbGVhbikge1xuICAgICAgICBDb25maWcuc291bmRFbmFibGUgPSAhc2VuZGVyLmlzQ2hlY2tlZDtcblxuICAgICAgICBpZiAoQ29uZmlnLnNvdW5kRW5hYmxlKSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5TXVzaWModGhpcy5tdXNpYywgdHJ1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wTXVzaWMoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNsYWltQmFua3J1cHRjeU1vbmV5KGJvbnVzOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgbXNnID0gY2MuanMuZm9ybWF0U3RyKExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdNT05FWTEnKSwgdXRpbC5udW1iZXJGb3JtYXQoYm9udXMpKTtcbiAgICAgICAgdGhpcy50b2FzdC5vcGVuV2l0aFRleHQobnVsbCwgbXNnKTtcbiAgICAgICAgQXBpLmNvaW5JbmNyZW1lbnQoYm9udXMpO1xuICAgICAgICB0aGlzLmNvaW4uc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLmNvaW4pO1xuICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDQpO1xuICAgIH1cblxuICAgIGludml0ZUZpcmVuZCgpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5JTlZJVEVfRlJJRU5EKTtcbiAgICAgICAgQXBpLmludml0ZSgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsYWltQmFua3J1cHRjeU1vbmV5KENvbmZpZy5iYW5rcnVwdF9ib251cyk7XG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMucG9wdXAuY2xvc2UobnVsbCwgMik7XG4gICAgICAgICAgICB0aGlzLnRvYXN0Lm9wZW5XaXRoVGV4dChudWxsLCAnTeG7nWkgYuG6oW4gY2jGoWkga2jDtG5nIHRow6BuaCBjw7RuZycpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhZFJld2FyZCgpIHtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9DT0lOX0NMSUNLKTtcbiAgICAgICAgQXBpLnNob3dSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xhaW1CYW5rcnVwdGN5TW9uZXkoQ29uZmlnLmJhbmtydXB0X2JvbnVzKTtcbiAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShBcGkucmFuZG9tQm9udXMoKSk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fRVJST1IpO1xuICAgICAgICB9KVxuICAgIH1cbn1cbiJdfQ==