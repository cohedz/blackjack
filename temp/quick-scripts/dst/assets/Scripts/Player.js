
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Player.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4cea4WuKU1GD7EAlqpK5ThR', 'Player');
// Scripts/Player.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var Timer_1 = require("./Timer");
var TweenMove_1 = require("./tween/TweenMove");
var util_1 = require("./util");
var Config_1 = require("./Config");
var Game_1 = require("./Game");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fold = null;
        _this.timer = null;
        _this.coin = null;
        _this.username = null;
        _this.marker = null;
        _this.avatar = null;
        _this.effect = null;
        _this.pointLbl = null;
        _this.seat = 0;
        _this.cards = [];
        _this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        _this.inRound = true;
        _this.coinVal = 1;
        _this.cardMargin = 0;
        _this.cardRootPosition = null;
        _this.cardDirection = null;
        _this.labelCardCounter = null;
        _this.numOfAce = 0;
        _this.point = 0;
        _this.bonusType = 0; // 0 nothing,  1 double, 2 insurr
        _this.isSplit = 0;
        _this.subUser = null;
        _this.isSubUser = false;
        _this.setBonusType = function (bonusType) {
            try {
                var self = _this;
                var txtBonusUser = 'Canvas/user/bonus';
                if (!_this.inRound) {
                    self = _this.subUser;
                    txtBonusUser = 'Canvas/subUser/bonus';
                }
                self.bonusType = bonusType;
                var bonusLbl = cc.find(txtBonusUser);
                bonusLbl.active = !!bonusType;
                var str = bonusType == 1 ? 'x2' : '/2';
                bonusLbl.getComponent(cc.Label).string = str;
            }
            catch (error) {
            }
        };
        _this.showAllCard = function () {
            _this.cards.map(function (card) {
                card.show();
            });
        };
        _this.changeCoin = function (coin) {
            var color = cc.color(0xf9, 0xd2, 0x1e, 255), addString = '+';
            if (coin < 0) {
                color = cc.color(255, 0, 0, 255), addString = '-';
            }
            _this.effect.getComponent(cc.Label).string = addString + util_1.default.numberFormat(Math.abs(coin));
            _this.effect.node.color = color;
            _this.effect.play();
            _this.setCoin(_this.coinVal + coin);
        };
        _this.split = function (subUser) {
            _this.subUser = subUser;
            _this.subUser.isSubUser = true;
            if (_this.isSplit)
                return;
            _this.isSplit = 1;
            if (_this.cards[1].rank === 1)
                _this.numOfAce--;
            _this.cards.map(function (card, index) {
                if (index === 1) {
                    return _this.subUser.push(card, 0);
                }
                card.node.zIndex = index === 1 ? -1 : index;
                var pos = _this.getCardRootPosition();
                pos.x += index < 2 ? 0 : index * _this.cardMargin;
                var move = cc.sequence(cc.delayTime(Game_1.default.DEAL_SPEED), cc.moveTo(0.2, pos));
                card.node.runAction(move);
            });
            setTimeout(function () {
                _this.cards = _this.cards.filter(function (card, index) { return index !== 1; });
            }, 0);
            _this.updateCardCounter();
            var point = _this.getPoint();
            _this.updatePoint(point.toString());
        };
        _this.showCard2 = function () {
            // if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            //     console.log('====================================');
            //     console.log(123);
            //     console.log('====================================');
            //     this.cards[1].show()
            // }
        };
        _this.getPoint = function () {
            var point = _this.cards.reduce(function (point, current) {
                if (current.rank === 1) {
                    return point;
                }
                else if (current.rank > 10) {
                    return point + 10;
                }
                else {
                    return point + current.rank;
                }
            }, 0);
            if (_this.numOfAce > 0) {
                if (point > 11) {
                    point += _this.numOfAce * 1;
                }
                else {
                    point += _this.numOfAce * 11;
                }
            }
            _this.point = point;
            return point;
            // if (point > 21) {
            //     let txtThua = `$user thua`
            //     if (this.isCai()) {
            //         return alert('cai thua: ' + point + ' diem')
            //     } else if (this.isBot()) {
            //         alert('bot thua: ' + point + ' diem')
            //     } else {
            //         alert('user thua: ' + point + ' diem')
            //     }
            // }
        };
        _this.checkBlackJack = function () {
            var _a, _b;
            var rank1 = (_a = _this.cards[0]) === null || _a === void 0 ? void 0 : _a.rank;
            var rank2 = (_b = _this.cards[1]) === null || _b === void 0 ? void 0 : _b.rank;
            if (rank1 === 1 || rank2 === 1) {
                if (rank1 > 9 || rank2 > 9) {
                    _this.setInRound(false);
                    _this.updatePoint("Black Jack!");
                    return true;
                }
            }
            return false;
        };
        return _this;
    }
    Player_1 = Player;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Player.prototype.start = function () {
        this.fold.active = false;
        this.timer.hide();
        this.marker.active = false;
        // this.cardMargin = this.seat == 0 ? Player.CARD_MARGIN :
        //     0.5 * Player.CARD_MARGIN;
        this.cardMargin = 0.5 * Player_1.CARD_MARGIN;
        this.cardDirection = this.seat == 1 ? -1 : 1;
        this.labelCardCounter = this.marker.getComponentInChildren(cc.Label);
    };
    // update (dt) {}
    Player.prototype.show = function () {
        this.node.active = true;
    };
    Player.prototype.hide = function () {
        this.node.active = false;
    };
    Player.prototype.isBot = function () {
        return this.seat !== Config_1.default.totalPlayer / 2;
        return this.seat > 0;
    };
    Player.prototype.isUser = function () {
        return this.isSubUser || this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.isCai = function () {
        return this.seat === 0;
        return this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.setInRound = function (inRound) {
        this.fold.active = !inRound;
        this.inRound = inRound;
    };
    Player.prototype.isInRound = function () {
        return this.inRound;
    };
    Player.prototype.addCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '+' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(0xf9, 0xd2, 0x1e, 255);
        this.effect.play();
        this.setCoin(this.coinVal + val);
    };
    Player.prototype.subCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '-' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(255, 0, 0, 255);
        this.effect.play();
        this.setCoin(this.coinVal - val);
    };
    Player.prototype.setCoin = function (val) {
        this.coinVal = val;
        this.coin.string = util_1.default.numberFormat(val);
    };
    Player.prototype.getCoin = function () {
        return this.coinVal;
    };
    Player.prototype.setUsername = function (val) {
        this.username.string = val;
    };
    Player.prototype.setAvatar = function (sprite) {
        this.avatar.spriteFrame = sprite;
    };
    Player.prototype.reset = function () {
        this.marker.active = false;
        this.fold.active = false;
        this.cards = [];
        this.isSubUser = false;
        this.inRound = true;
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        this.timer.hide();
        this.numOfAce = 0;
        this.point = 0;
        this.bonusType = 0;
        this.isSplit = 0;
        if (this.subUser) {
            this.subUser.reset();
            this.subUser = null;
        }
    };
    Player.prototype.push = function (card, delay) {
        var _this = this;
        if (!this.inRound && this.subUser)
            return this.subUser.push(card, delay);
        card.node.zIndex = this.cards.length;
        this.cards.push(card);
        if (card.rank === 1)
            this.numOfAce += 1;
        var point = this.getPoint();
        var fun = function () {
            _this.updatePoint(point == 21 && _this.cards.length === 2 ? 'BlackJack' : point.toString());
            if (point > 20)
                _this.inRound = false;
            _this.cards[0].show();
            if (_this.isUser()) {
                card.show();
            }
            else {
                _this.updateCardCounter();
            }
        };
        if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            fun = function () {
                _this.cards[1].show();
                _this.updatePoint(null);
            };
        }
        // this.updatePoint()
        var pos = this.getCardRootPosition();
        var scale = cc.sequence(cc.delayTime(delay), cc.scaleTo(0.2, 0.5));
        card.node.runAction(scale);
        // if (this.isBot()) {
        //     if (this.cards.length < 3) {
        //         pos.x += (this.cards.length - 1) * this.cardMargin;
        //     } else {
        //         pos.x += 2 * this.cardMargin;
        //     }
        // } else {
        pos.x += (this.cards.length - 1) * this.cardMargin;
        // }
        var move = cc.sequence(cc.delayTime(delay), cc.moveTo(0.2, pos), cc.callFunc(fun));
        card.node.runAction(move);
    };
    Player.prototype.touch = function (pos) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            if (card.node.getBoundingBoxToWorld().contains(pos)) {
                return card;
            }
        }
        return null;
    };
    Player.prototype.selectCard = function (card) {
        // this.prepareCards.push(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y + 30);
    };
    Player.prototype.unselectCard = function (card) {
        // this.prepareCards.remove(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y);
    };
    Player.prototype.removeCards = function (cards) {
        for (var i = cards.count() - 1; i >= 0; --i) {
            Helper_1.default.removeBy(this.cards, cards.at(i));
        }
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
    };
    Player.prototype.setActive = function (on) {
        if (on) {
            this.timer.show(Player_1.TIME);
        }
        else {
            this.timer.hide();
        }
    };
    Player.prototype.setTimeCallback = function (selector, selectorTarget, data) {
        this.timer.onCompleted(selector, selectorTarget, data);
    };
    Player.prototype.showHandCards = function () {
        for (var i = 0; i < this.cards.length; i++) {
            this.cards[i].show();
        }
    };
    Player.prototype.reorder = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var pos = this.getCardRootPosition();
            pos.x += i * this.cardMargin * this.cardDirection;
            var card = this.cards[i];
            card.node.setPosition(pos);
            card.node.zIndex = this.seat == 1 ? this.cards.length - i : i;
        }
    };
    Player.prototype.sortHandCards = function () {
        Helper_1.default.sort(this.cards);
        this.reorder();
    };
    Player.prototype.showCardCounter = function () {
        this.marker.active = true;
        this.updateCardCounter();
    };
    Player.prototype.hideCardCounter = function () {
        this.marker.active = false;
    };
    Player.prototype.updateCardCounter = function () {
        try {
            this.labelCardCounter.string = this.cards.length.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.updatePoint = function (label) {
        if (Number(this.pointLbl.string) === NaN)
            return;
        this.point = this.getPoint();
        try {
            this.pointLbl.string = label || this.point.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.getCardRootPosition = function () {
        if (!this.cardRootPosition) {
            this.cardRootPosition = cc.v2(this.marker.position.x * this.node.scaleX + this.node.position.x, this.marker.position.y * this.node.scaleY + this.node.position.y);
        }
        return cc.v2(this.cardRootPosition);
    };
    var Player_1;
    Player.CARD_MARGIN = 60;
    Player.TIME = 30;
    __decorate([
        property(cc.Node)
    ], Player.prototype, "fold", void 0);
    __decorate([
        property(Timer_1.default)
    ], Player.prototype, "timer", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "coin", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "username", void 0);
    __decorate([
        property(cc.Node)
    ], Player.prototype, "marker", void 0);
    __decorate([
        property(cc.Sprite)
    ], Player.prototype, "avatar", void 0);
    __decorate([
        property(TweenMove_1.default)
    ], Player.prototype, "effect", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "pointLbl", void 0);
    Player = Player_1 = __decorate([
        ccclass
    ], Player);
    return Player;
}(cc.Component));
exports.default = Player;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1BsYXllci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBOEI7QUFFOUIseUNBQW9DO0FBQ3BDLGlDQUE0QjtBQUM1QiwrQ0FBMEM7QUFDMUMsK0JBQTBCO0FBQzFCLG1DQUE4QjtBQUM5QiwrQkFBMEI7QUFHMUIsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFFN0YsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBb0MsMEJBQVk7SUFBaEQ7UUFBQSxxRUFtWkM7UUFqWkcsVUFBSSxHQUFZLElBQUksQ0FBQztRQUVyQixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLFVBQUksR0FBYSxJQUFJLENBQUM7UUFFdEIsY0FBUSxHQUFhLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRTFCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFDakIsV0FBSyxHQUFnQixFQUFFLENBQUM7UUFDeEIsa0JBQVksR0FBYyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakUsYUFBTyxHQUFZLElBQUksQ0FBQztRQUN4QixhQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLHNCQUFnQixHQUFZLElBQUksQ0FBQztRQUNqQyxtQkFBYSxHQUFXLElBQUksQ0FBQztRQUM3QixzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFDbEMsY0FBUSxHQUFXLENBQUMsQ0FBQTtRQUNwQixXQUFLLEdBQVcsQ0FBQyxDQUFBO1FBRWpCLGVBQVMsR0FBVyxDQUFDLENBQUEsQ0FBQyxpQ0FBaUM7UUFDdkQsYUFBTyxHQUFXLENBQUMsQ0FBQTtRQUVuQixhQUFPLEdBQVcsSUFBSSxDQUFBO1FBQ3RCLGVBQVMsR0FBWSxLQUFLLENBQUE7UUFnQjFCLGtCQUFZLEdBQUcsVUFBQyxTQUFpQjtZQUU3QixJQUFJO2dCQUNBLElBQUksSUFBSSxHQUFHLEtBQUksQ0FBQTtnQkFDZixJQUFJLFlBQVksR0FBRyxtQkFBbUIsQ0FBQTtnQkFDdEMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2YsSUFBSSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUE7b0JBQ25CLFlBQVksR0FBRyxzQkFBc0IsQ0FBQTtpQkFDeEM7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7Z0JBRTFCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3JDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQTtnQkFDN0IsSUFBSSxHQUFHLEdBQUcsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7Z0JBQ3RDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7YUFDL0M7WUFBQyxPQUFPLEtBQUssRUFBRTthQUVmO1FBQ0wsQ0FBQyxDQUFBO1FBUUQsaUJBQVcsR0FBRztZQUNWLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDZixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7WUFDZixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQTtRQTZCRCxnQkFBVSxHQUFHLFVBQUMsSUFBWTtZQUN0QixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDN0QsSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFO2dCQUNWLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLFNBQVMsR0FBRyxHQUFHLENBQUE7YUFDcEQ7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxRixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBQzlCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQTtRQW1ERCxXQUFLLEdBQUcsVUFBQyxPQUFlO1lBRXBCLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO1lBQ3RCLEtBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQTtZQUU3QixJQUFJLEtBQUksQ0FBQyxPQUFPO2dCQUFFLE9BQU07WUFDeEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUE7WUFDaEIsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDO2dCQUFFLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUM3QyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUN2QixJQUFJLEtBQUssS0FBSyxDQUFDLEVBQUU7b0JBQ2IsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUE7aUJBQ3BDO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7Z0JBQzNDLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUNyQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQ2pELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxjQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDM0UsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUE7WUFDRixVQUFVLENBQUM7Z0JBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxLQUFLLEtBQUssQ0FBQyxFQUFYLENBQVcsQ0FBQyxDQUFBO1lBQ2hFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVOLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO1lBQ3hCLElBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO1FBRXRDLENBQUMsQ0FBQTtRQW9ERCxlQUFTLEdBQUc7WUFDUixrRUFBa0U7WUFDbEUsMkRBQTJEO1lBQzNELHdCQUF3QjtZQUN4QiwyREFBMkQ7WUFDM0QsMkJBQTJCO1lBQzNCLElBQUk7UUFDUixDQUFDLENBQUE7UUE0RkQsY0FBUSxHQUFHO1lBQ1AsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQyxLQUFLLEVBQUUsT0FBTztnQkFDekMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtvQkFDcEIsT0FBTyxLQUFLLENBQUE7aUJBQ2Y7cUJBQU0sSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLEVBQUUsRUFBRTtvQkFDMUIsT0FBTyxLQUFLLEdBQUcsRUFBRSxDQUFBO2lCQUNwQjtxQkFBTTtvQkFDSCxPQUFPLEtBQUssR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFBO2lCQUM5QjtZQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUVOLElBQUksS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQUU7Z0JBQ25CLElBQUksS0FBSyxHQUFHLEVBQUUsRUFBRTtvQkFDWixLQUFLLElBQUksS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUE7aUJBQzdCO3FCQUFNO29CQUNILEtBQUssSUFBSSxLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtpQkFDOUI7YUFDSjtZQUNELEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBQ2xCLE9BQU8sS0FBSyxDQUFBO1lBQ1osb0JBQW9CO1lBQ3BCLGlDQUFpQztZQUNqQywwQkFBMEI7WUFDMUIsdURBQXVEO1lBQ3ZELGlDQUFpQztZQUNqQyxnREFBZ0Q7WUFDaEQsZUFBZTtZQUNmLGlEQUFpRDtZQUNqRCxRQUFRO1lBQ1IsSUFBSTtRQUNSLENBQUMsQ0FBQTtRQUVELG9CQUFjLEdBQUc7O1lBQ2IsSUFBSSxLQUFLLFNBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsMENBQUUsSUFBSSxDQUFBO1lBQy9CLElBQUksS0FBSyxTQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLDBDQUFFLElBQUksQ0FBQTtZQUUvQixJQUFJLEtBQUssS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFDNUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7b0JBQ3hCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7b0JBQ3RCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUE7b0JBQy9CLE9BQU8sSUFBSSxDQUFBO2lCQUNkO2FBQ0o7WUFDRCxPQUFPLEtBQUssQ0FBQTtRQUNoQixDQUFDLENBQUE7O0lBWUwsQ0FBQztlQW5ab0IsTUFBTTtJQW1DdkIsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZixzQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLDBEQUEwRDtRQUMxRCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBTSxDQUFDLFdBQVcsQ0FBQztRQUMzQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHNCQUFzQixDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBc0JELGlCQUFpQjtJQUVqQixxQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFRRCxxQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLGdCQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUM1QyxPQUFPLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCx1QkFBTSxHQUFOO1FBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztRQUN2QixPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCwyQkFBVSxHQUFWLFVBQVcsT0FBZ0I7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7SUFDM0IsQ0FBQztJQUVELDBCQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQWNELHdCQUFPLEdBQVAsVUFBUSxHQUFXO1FBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsd0JBQU8sR0FBUCxVQUFRLEdBQVc7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCx3QkFBTyxHQUFQLFVBQVEsR0FBVztRQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELHdCQUFPLEdBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELDRCQUFXLEdBQVgsVUFBWSxHQUFXO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztJQUMvQixDQUFDO0lBRUQsMEJBQVMsR0FBVCxVQUFVLE1BQXNCO1FBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztJQUNyQyxDQUFDO0lBRUQsc0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO1FBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUE7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7U0FDdEI7SUFDTCxDQUFDO0lBOEJELHFCQUFJLEdBQUosVUFBSyxJQUFVLEVBQUUsS0FBYTtRQUE5QixpQkFnREM7UUEvQ0csSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUV4RSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQztZQUFFLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFBO1FBRXZDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtRQUUzQixJQUFJLEdBQUcsR0FBRztZQUNOLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFJLEVBQUUsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7WUFDekYsSUFBSSxLQUFLLEdBQUcsRUFBRTtnQkFBRSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTtZQUNwQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO1lBQ3BCLElBQUksS0FBSSxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTthQUFFO2lCQUM3QjtnQkFDRCxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTthQUMzQjtRQUNMLENBQUMsQ0FBQTtRQUVELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUMxRCxHQUFHLEdBQUc7Z0JBQ0YsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtnQkFDcEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUMxQixDQUFDLENBQUE7U0FDSjtRQUdELHFCQUFxQjtRQUNyQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUdyQyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixzQkFBc0I7UUFFdEIsbUNBQW1DO1FBQ25DLDhEQUE4RDtRQUM5RCxlQUFlO1FBQ2Ysd0NBQXdDO1FBQ3hDLFFBQVE7UUFHUixXQUFXO1FBQ1gsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbkQsSUFBSTtRQUVKLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQVdELHNCQUFLLEdBQUwsVUFBTSxHQUFZO1FBQ2QsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDakQsT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELDJCQUFVLEdBQVYsVUFBVyxJQUFVO1FBQ2pCLGdDQUFnQztRQUNoQyxpQ0FBaUM7UUFDakMseUVBQXlFO0lBQzdFLENBQUM7SUFFRCw2QkFBWSxHQUFaLFVBQWEsSUFBVTtRQUNuQixrQ0FBa0M7UUFDbEMsaUNBQWlDO1FBQ2pDLG9FQUFvRTtJQUN4RSxDQUFDO0lBRUQsNEJBQVcsR0FBWCxVQUFZLEtBQWdCO1FBQ3hCLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3pDLGdCQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG1CQUFTLENBQUMsRUFBRSxFQUFFLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDBCQUFTLEdBQVQsVUFBVSxFQUFXO1FBQ2pCLElBQUksRUFBRSxFQUFFO1lBQ0osSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hDO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQztJQUVELGdDQUFlLEdBQWYsVUFBZ0IsUUFBa0IsRUFBRSxjQUFtQixFQUFFLElBQVM7UUFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQsOEJBQWEsR0FBYjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELHdCQUFPLEdBQVA7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQzdDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQ3JDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNsRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNqRTtJQUNMLENBQUM7SUFFRCw4QkFBYSxHQUFiO1FBQ0ksZ0JBQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsZ0NBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsZ0NBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQsa0NBQWlCLEdBQWpCO1FBQ0ksSUFBSTtZQUNBLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDL0Q7UUFBQyxPQUFPLEtBQUssRUFBRTtTQUVmO0lBQ0wsQ0FBQztJQUVELDRCQUFXLEdBQVgsVUFBWSxLQUFhO1FBQ3JCLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRztZQUFFLE9BQU07UUFDaEQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7UUFDNUIsSUFBSTtZQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFBO1NBQ3hEO1FBQUMsT0FBTyxLQUFLLEVBQUU7U0FFZjtJQUNMLENBQUM7SUFnRE8sb0NBQW1CLEdBQTNCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUMxRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekU7UUFDRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDeEMsQ0FBQzs7SUFFZSxrQkFBVyxHQUFXLEVBQUUsQ0FBQztJQUN6QixXQUFJLEdBQVcsRUFBRSxDQUFDO0lBaFpsQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dDQUNHO0lBRXJCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzt5Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3dDQUNHO0lBRXRCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NENBQ087SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDSztJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzBDQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLG1CQUFTLENBQUM7MENBQ0s7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0Q0FDTztJQWhCVCxNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBbVoxQjtJQUFELGFBQUM7Q0FuWkQsQUFtWkMsQ0FuWm1DLEVBQUUsQ0FBQyxTQUFTLEdBbVovQztrQkFuWm9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuaW1wb3J0IENhcmQgZnJvbSBcIi4vQ2FyZFwiO1xuaW1wb3J0IENhcmRHcm91cCBmcm9tIFwiLi9DYXJkR3JvdXBcIjtcbmltcG9ydCBUaW1lciBmcm9tIFwiLi9UaW1lclwiO1xuaW1wb3J0IFR3ZWVuTW92ZSBmcm9tIFwiLi90d2Vlbi9Ud2Vlbk1vdmVcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuL3V0aWxcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4vQ29uZmlnXCI7XG5pbXBvcnQgR2FtZSBmcm9tIFwiLi9HYW1lXCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuXG4vLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gW0NoaW5lc2VdIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvemgvc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGxheWVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBmb2xkOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoVGltZXIpXG4gICAgdGltZXI6IFRpbWVyID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgY29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1c2VybmFtZTogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIG1hcmtlcjogY2MuTm9kZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KFR3ZWVuTW92ZSlcbiAgICBlZmZlY3Q6IFR3ZWVuTW92ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBvaW50TGJsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBzZWF0OiBudW1iZXIgPSAwO1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPiA9IFtdO1xuICAgIHByZXBhcmVDYXJkczogQ2FyZEdyb3VwID0gbmV3IENhcmRHcm91cChbXSwgQ2FyZEdyb3VwLlVuZ3JvdXBlZCk7XG4gICAgaW5Sb3VuZDogYm9vbGVhbiA9IHRydWU7XG4gICAgY29pblZhbDogbnVtYmVyID0gMTtcbiAgICBjYXJkTWFyZ2luOiBudW1iZXIgPSAwO1xuICAgIGNhcmRSb290UG9zaXRpb246IGNjLlZlYzIgPSBudWxsO1xuICAgIGNhcmREaXJlY3Rpb246IG51bWJlciA9IG51bGw7XG4gICAgbGFiZWxDYXJkQ291bnRlcjogY2MuTGFiZWwgPSBudWxsO1xuICAgIG51bU9mQWNlOiBudW1iZXIgPSAwXG4gICAgcG9pbnQ6IG51bWJlciA9IDBcblxuICAgIGJvbnVzVHlwZTogbnVtYmVyID0gMCAvLyAwIG5vdGhpbmcsICAxIGRvdWJsZSwgMiBpbnN1cnJcbiAgICBpc1NwbGl0OiBudW1iZXIgPSAwXG5cbiAgICBzdWJVc2VyOiBQbGF5ZXIgPSBudWxsXG4gICAgaXNTdWJVc2VyOiBib29sZWFuID0gZmFsc2VcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMuZm9sZC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy50aW1lci5oaWRlKCk7XG4gICAgICAgIHRoaXMubWFya2VyLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAvLyB0aGlzLmNhcmRNYXJnaW4gPSB0aGlzLnNlYXQgPT0gMCA/IFBsYXllci5DQVJEX01BUkdJTiA6XG4gICAgICAgIC8vICAgICAwLjUgKiBQbGF5ZXIuQ0FSRF9NQVJHSU47XG4gICAgICAgIHRoaXMuY2FyZE1hcmdpbiA9IDAuNSAqIFBsYXllci5DQVJEX01BUkdJTjtcbiAgICAgICAgdGhpcy5jYXJkRGlyZWN0aW9uID0gdGhpcy5zZWF0ID09IDEgPyAtMSA6IDE7XG4gICAgICAgIHRoaXMubGFiZWxDYXJkQ291bnRlciA9IHRoaXMubWFya2VyLmdldENvbXBvbmVudEluQ2hpbGRyZW4oY2MuTGFiZWwpO1xuICAgIH1cblxuICAgIHNldEJvbnVzVHlwZSA9IChib251c1R5cGU6IG51bWJlcikgPT4ge1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXNcbiAgICAgICAgICAgIGxldCB0eHRCb251c1VzZXIgPSAnQ2FudmFzL3VzZXIvYm9udXMnXG4gICAgICAgICAgICBpZiAoIXRoaXMuaW5Sb3VuZCkge1xuICAgICAgICAgICAgICAgIHNlbGYgPSB0aGlzLnN1YlVzZXJcbiAgICAgICAgICAgICAgICB0eHRCb251c1VzZXIgPSAnQ2FudmFzL3N1YlVzZXIvYm9udXMnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZWxmLmJvbnVzVHlwZSA9IGJvbnVzVHlwZVxuXG4gICAgICAgICAgICBsZXQgYm9udXNMYmwgPSBjYy5maW5kKHR4dEJvbnVzVXNlcik7XG4gICAgICAgICAgICBib251c0xibC5hY3RpdmUgPSAhIWJvbnVzVHlwZVxuICAgICAgICAgICAgbGV0IHN0ciA9IGJvbnVzVHlwZSA9PSAxID8gJ3gyJyA6ICcvMidcbiAgICAgICAgICAgIGJvbnVzTGJsLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc3RyXG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaG93KCkge1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzaG93QWxsQ2FyZCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5jYXJkcy5tYXAoY2FyZCA9PiB7XG4gICAgICAgICAgICBjYXJkLnNob3coKVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBpc0JvdCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhdCAhPT0gQ29uZmlnLnRvdGFsUGxheWVyIC8gMjtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhdCA+IDA7XG4gICAgfVxuXG4gICAgaXNVc2VyKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1N1YlVzZXIgfHwgdGhpcy5zZWF0ID09PSBDb25maWcudG90YWxQbGF5ZXIgLyAyO1xuICAgIH1cblxuICAgIGlzQ2FpKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWF0ID09PSAwO1xuICAgICAgICByZXR1cm4gdGhpcy5zZWF0ID09PSBDb25maWcudG90YWxQbGF5ZXIgLyAyO1xuICAgIH1cblxuICAgIHNldEluUm91bmQoaW5Sb3VuZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmZvbGQuYWN0aXZlID0gIWluUm91bmQ7XG4gICAgICAgIHRoaXMuaW5Sb3VuZCA9IGluUm91bmQ7XG4gICAgfVxuXG4gICAgaXNJblJvdW5kKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pblJvdW5kO1xuICAgIH1cblxuICAgIGNoYW5nZUNvaW4gPSAoY29pbjogbnVtYmVyKSA9PiB7XG4gICAgICAgIGxldCBjb2xvciA9IGNjLmNvbG9yKDB4ZjksIDB4ZDIsIDB4MWUsIDI1NSksIGFkZFN0cmluZyA9ICcrJztcbiAgICAgICAgaWYgKGNvaW4gPCAwKSB7XG4gICAgICAgICAgICBjb2xvciA9IGNjLmNvbG9yKDI1NSwgMCwgMCwgMjU1KSwgYWRkU3RyaW5nID0gJy0nXG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmVmZmVjdC5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IGFkZFN0cmluZyArIHV0aWwubnVtYmVyRm9ybWF0KE1hdGguYWJzKGNvaW4pKTtcbiAgICAgICAgdGhpcy5lZmZlY3Qubm9kZS5jb2xvciA9IGNvbG9yXG4gICAgICAgIHRoaXMuZWZmZWN0LnBsYXkoKTtcbiAgICAgICAgdGhpcy5zZXRDb2luKHRoaXMuY29pblZhbCArIGNvaW4pO1xuICAgIH1cblxuICAgIGFkZENvaW4odmFsOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5lZmZlY3QuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSAnKycgKyB1dGlsLm51bWJlckZvcm1hdCh2YWwpO1xuICAgICAgICB0aGlzLmVmZmVjdC5ub2RlLmNvbG9yID0gY2MuY29sb3IoMHhmOSwgMHhkMiwgMHgxZSwgMjU1KTtcbiAgICAgICAgdGhpcy5lZmZlY3QucGxheSgpO1xuICAgICAgICB0aGlzLnNldENvaW4odGhpcy5jb2luVmFsICsgdmFsKTtcbiAgICB9XG5cbiAgICBzdWJDb2luKHZhbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuZWZmZWN0LmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gJy0nICsgdXRpbC5udW1iZXJGb3JtYXQodmFsKTtcbiAgICAgICAgdGhpcy5lZmZlY3Qubm9kZS5jb2xvciA9IGNjLmNvbG9yKDI1NSwgMCwgMCwgMjU1KTtcbiAgICAgICAgdGhpcy5lZmZlY3QucGxheSgpO1xuICAgICAgICB0aGlzLnNldENvaW4odGhpcy5jb2luVmFsIC0gdmFsKTtcbiAgICB9XG5cbiAgICBzZXRDb2luKHZhbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuY29pblZhbCA9IHZhbDtcbiAgICAgICAgdGhpcy5jb2luLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KHZhbCk7XG4gICAgfVxuXG4gICAgZ2V0Q29pbigpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb2luVmFsO1xuICAgIH1cblxuICAgIHNldFVzZXJuYW1lKHZhbDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudXNlcm5hbWUuc3RyaW5nID0gdmFsO1xuICAgIH1cblxuICAgIHNldEF2YXRhcihzcHJpdGU6IGNjLlNwcml0ZUZyYW1lKSB7XG4gICAgICAgIHRoaXMuYXZhdGFyLnNwcml0ZUZyYW1lID0gc3ByaXRlO1xuICAgIH1cblxuICAgIHJlc2V0KCkge1xuICAgICAgICB0aGlzLm1hcmtlci5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5mb2xkLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmNhcmRzID0gW107XG4gICAgICAgIHRoaXMuaXNTdWJVc2VyID0gZmFsc2VcbiAgICAgICAgdGhpcy5pblJvdW5kID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5wcmVwYXJlQ2FyZHMgPSBuZXcgQ2FyZEdyb3VwKFtdLCBDYXJkR3JvdXAuVW5ncm91cGVkKTtcbiAgICAgICAgdGhpcy50aW1lci5oaWRlKCk7XG4gICAgICAgIHRoaXMubnVtT2ZBY2UgPSAwO1xuICAgICAgICB0aGlzLnBvaW50ID0gMDtcbiAgICAgICAgdGhpcy5ib251c1R5cGUgPSAwXG4gICAgICAgIHRoaXMuaXNTcGxpdCA9IDBcbiAgICAgICAgaWYgKHRoaXMuc3ViVXNlcikge1xuICAgICAgICAgICAgdGhpcy5zdWJVc2VyLnJlc2V0KClcbiAgICAgICAgICAgIHRoaXMuc3ViVXNlciA9IG51bGxcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNwbGl0ID0gKHN1YlVzZXI6IFBsYXllcikgPT4ge1xuXG4gICAgICAgIHRoaXMuc3ViVXNlciA9IHN1YlVzZXJcbiAgICAgICAgdGhpcy5zdWJVc2VyLmlzU3ViVXNlciA9IHRydWVcblxuICAgICAgICBpZiAodGhpcy5pc1NwbGl0KSByZXR1cm5cbiAgICAgICAgdGhpcy5pc1NwbGl0ID0gMVxuICAgICAgICBpZiAodGhpcy5jYXJkc1sxXS5yYW5rID09PSAxKSB0aGlzLm51bU9mQWNlLS1cbiAgICAgICAgdGhpcy5jYXJkcy5tYXAoKGNhcmQsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBpZiAoaW5kZXggPT09IDEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zdWJVc2VyLnB1c2goY2FyZCwgMClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSBpbmRleCA9PT0gMSA/IC0xIDogaW5kZXhcbiAgICAgICAgICAgIGxldCBwb3MgPSB0aGlzLmdldENhcmRSb290UG9zaXRpb24oKTtcbiAgICAgICAgICAgIHBvcy54ICs9IGluZGV4IDwgMiA/IDAgOiBpbmRleCAqIHRoaXMuY2FyZE1hcmdpbjtcbiAgICAgICAgICAgIGxldCBtb3ZlID0gY2Muc2VxdWVuY2UoY2MuZGVsYXlUaW1lKEdhbWUuREVBTF9TUEVFRCksIGNjLm1vdmVUbygwLjIsIHBvcykpO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnJ1bkFjdGlvbihtb3ZlKTtcbiAgICAgICAgfSlcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNhcmRzID0gdGhpcy5jYXJkcy5maWx0ZXIoKGNhcmQsIGluZGV4KSA9PiBpbmRleCAhPT0gMSlcbiAgICAgICAgfSwgMCk7XG5cbiAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpXG4gICAgICAgIGxldCBwb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KHBvaW50LnRvU3RyaW5nKCkpXG5cbiAgICB9XG5cbiAgICBwdXNoKGNhcmQ6IENhcmQsIGRlbGF5OiBudW1iZXIpIHtcbiAgICAgICAgaWYgKCF0aGlzLmluUm91bmQgJiYgdGhpcy5zdWJVc2VyKSByZXR1cm4gdGhpcy5zdWJVc2VyLnB1c2goY2FyZCwgZGVsYXkpXG5cbiAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZCk7XG4gICAgICAgIGlmIChjYXJkLnJhbmsgPT09IDEpIHRoaXMubnVtT2ZBY2UgKz0gMVxuXG4gICAgICAgIGxldCBwb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuXG4gICAgICAgIGxldCBmdW4gPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KHBvaW50ID09IDIxICYmIHRoaXMuY2FyZHMubGVuZ3RoID09PSAyID8gJ0JsYWNrSmFjaycgOiBwb2ludC50b1N0cmluZygpKVxuICAgICAgICAgICAgaWYgKHBvaW50ID4gMjApIHRoaXMuaW5Sb3VuZCA9IGZhbHNlXG4gICAgICAgICAgICB0aGlzLmNhcmRzWzBdLnNob3coKVxuICAgICAgICAgICAgaWYgKHRoaXMuaXNVc2VyKCkpIHsgY2FyZC5zaG93KCkgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0JvdCgpICYmICF0aGlzLmlzQ2FpKCkgJiYgdGhpcy5jYXJkcy5sZW5ndGggPT09IDIpIHtcbiAgICAgICAgICAgIGZ1biA9ICgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmNhcmRzWzFdLnNob3coKVxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUG9pbnQobnVsbClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLy8gdGhpcy51cGRhdGVQb2ludCgpXG4gICAgICAgIGxldCBwb3MgPSB0aGlzLmdldENhcmRSb290UG9zaXRpb24oKTtcblxuXG4gICAgICAgIGxldCBzY2FsZSA9IGNjLnNlcXVlbmNlKGNjLmRlbGF5VGltZShkZWxheSksIGNjLnNjYWxlVG8oMC4yLCAwLjUpKTtcbiAgICAgICAgY2FyZC5ub2RlLnJ1bkFjdGlvbihzY2FsZSk7XG4gICAgICAgIC8vIGlmICh0aGlzLmlzQm90KCkpIHtcblxuICAgICAgICAvLyAgICAgaWYgKHRoaXMuY2FyZHMubGVuZ3RoIDwgMykge1xuICAgICAgICAvLyAgICAgICAgIHBvcy54ICs9ICh0aGlzLmNhcmRzLmxlbmd0aCAtIDEpICogdGhpcy5jYXJkTWFyZ2luO1xuICAgICAgICAvLyAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gICAgICAgICBwb3MueCArPSAyICogdGhpcy5jYXJkTWFyZ2luO1xuICAgICAgICAvLyAgICAgfVxuXG5cbiAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgcG9zLnggKz0gKHRoaXMuY2FyZHMubGVuZ3RoIC0gMSkgKiB0aGlzLmNhcmRNYXJnaW47XG4gICAgICAgIC8vIH1cblxuICAgICAgICBsZXQgbW92ZSA9IGNjLnNlcXVlbmNlKGNjLmRlbGF5VGltZShkZWxheSksIGNjLm1vdmVUbygwLjIsIHBvcyksIGNjLmNhbGxGdW5jKGZ1bikpO1xuICAgICAgICBjYXJkLm5vZGUucnVuQWN0aW9uKG1vdmUpO1xuICAgIH1cblxuICAgIHNob3dDYXJkMiA9ICgpID0+IHtcbiAgICAgICAgLy8gaWYgKHRoaXMuaXNCb3QoKSAmJiAhdGhpcy5pc0NhaSgpICYmIHRoaXMuY2FyZHMubGVuZ3RoID09PSAyKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZygxMjMpO1xuICAgICAgICAvLyAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAvLyAgICAgdGhpcy5jYXJkc1sxXS5zaG93KClcbiAgICAgICAgLy8gfVxuICAgIH1cblxuICAgIHRvdWNoKHBvczogY2MuVmVjMik6IENhcmQge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmNhcmRzW2ldO1xuICAgICAgICAgICAgaWYgKGNhcmQubm9kZS5nZXRCb3VuZGluZ0JveFRvV29ybGQoKS5jb250YWlucyhwb3MpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNhcmQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgc2VsZWN0Q2FyZChjYXJkOiBDYXJkKSB7XG4gICAgICAgIC8vIHRoaXMucHJlcGFyZUNhcmRzLnB1c2goY2FyZCk7XG4gICAgICAgIC8vIHRoaXMucHJlcGFyZUNhcmRzLmNhbGN1bGF0ZSgpO1xuICAgICAgICAvLyBjYXJkLnNldFBvc2l0aW9uWSh0aGlzLm1hcmtlci5wb3NpdGlvbi55ICsgdGhpcy5ub2RlLnBvc2l0aW9uLnkgKyAzMCk7XG4gICAgfVxuXG4gICAgdW5zZWxlY3RDYXJkKGNhcmQ6IENhcmQpIHtcbiAgICAgICAgLy8gdGhpcy5wcmVwYXJlQ2FyZHMucmVtb3ZlKGNhcmQpO1xuICAgICAgICAvLyB0aGlzLnByZXBhcmVDYXJkcy5jYWxjdWxhdGUoKTtcbiAgICAgICAgLy8gY2FyZC5zZXRQb3NpdGlvblkodGhpcy5tYXJrZXIucG9zaXRpb24ueSArIHRoaXMubm9kZS5wb3NpdGlvbi55KTtcbiAgICB9XG5cbiAgICByZW1vdmVDYXJkcyhjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSBjYXJkcy5jb3VudCgpIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGhlbHBlci5yZW1vdmVCeSh0aGlzLmNhcmRzLCBjYXJkcy5hdChpKSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnByZXBhcmVDYXJkcyA9IG5ldyBDYXJkR3JvdXAoW10sIENhcmRHcm91cC5Vbmdyb3VwZWQpO1xuICAgIH1cblxuICAgIHNldEFjdGl2ZShvbjogYm9vbGVhbikge1xuICAgICAgICBpZiAob24pIHtcbiAgICAgICAgICAgIHRoaXMudGltZXIuc2hvdyhQbGF5ZXIuVElNRSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnRpbWVyLmhpZGUoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNldFRpbWVDYWxsYmFjayhzZWxlY3RvcjogRnVuY3Rpb24sIHNlbGVjdG9yVGFyZ2V0OiBhbnksIGRhdGE6IGFueSkge1xuICAgICAgICB0aGlzLnRpbWVyLm9uQ29tcGxldGVkKHNlbGVjdG9yLCBzZWxlY3RvclRhcmdldCwgZGF0YSk7XG4gICAgfVxuXG4gICAgc2hvd0hhbmRDYXJkcygpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmNhcmRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLmNhcmRzW2ldLnNob3coKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlb3JkZXIoKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgICAgICBsZXQgcG9zID0gdGhpcy5nZXRDYXJkUm9vdFBvc2l0aW9uKCk7XG4gICAgICAgICAgICBwb3MueCArPSBpICogdGhpcy5jYXJkTWFyZ2luICogdGhpcy5jYXJkRGlyZWN0aW9uO1xuICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmNhcmRzW2ldO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnNldFBvc2l0aW9uKHBvcyk7XG4gICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gdGhpcy5zZWF0ID09IDEgPyB0aGlzLmNhcmRzLmxlbmd0aCAtIGkgOiBpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc29ydEhhbmRDYXJkcygpIHtcbiAgICAgICAgaGVscGVyLnNvcnQodGhpcy5jYXJkcyk7XG4gICAgICAgIHRoaXMucmVvcmRlcigpO1xuICAgIH1cblxuICAgIHNob3dDYXJkQ291bnRlcigpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy51cGRhdGVDYXJkQ291bnRlcigpO1xuICAgIH1cblxuICAgIGhpZGVDYXJkQ291bnRlcigpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgdXBkYXRlQ2FyZENvdW50ZXIoKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmxhYmVsQ2FyZENvdW50ZXIuc3RyaW5nID0gdGhpcy5jYXJkcy5sZW5ndGgudG9TdHJpbmcoKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgdXBkYXRlUG9pbnQobGFiZWw6IHN0cmluZykge1xuICAgICAgICBpZiAoTnVtYmVyKHRoaXMucG9pbnRMYmwuc3RyaW5nKSA9PT0gTmFOKSByZXR1cm5cbiAgICAgICAgdGhpcy5wb2ludCA9IHRoaXMuZ2V0UG9pbnQoKVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy5wb2ludExibC5zdHJpbmcgPSBsYWJlbCB8fCB0aGlzLnBvaW50LnRvU3RyaW5nKClcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0UG9pbnQgPSAoKSA9PiB7XG4gICAgICAgIHZhciBwb2ludCA9IHRoaXMuY2FyZHMucmVkdWNlKChwb2ludCwgY3VycmVudCkgPT4ge1xuICAgICAgICAgICAgaWYgKGN1cnJlbnQucmFuayA9PT0gMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludFxuICAgICAgICAgICAgfSBlbHNlIGlmIChjdXJyZW50LnJhbmsgPiAxMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludCArIDEwXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBwb2ludCArIGN1cnJlbnQucmFua1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCAwKTtcblxuICAgICAgICBpZiAodGhpcy5udW1PZkFjZSA+IDApIHtcbiAgICAgICAgICAgIGlmIChwb2ludCA+IDExKSB7XG4gICAgICAgICAgICAgICAgcG9pbnQgKz0gdGhpcy5udW1PZkFjZSAqIDFcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcG9pbnQgKz0gdGhpcy5udW1PZkFjZSAqIDExXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5wb2ludCA9IHBvaW50XG4gICAgICAgIHJldHVybiBwb2ludFxuICAgICAgICAvLyBpZiAocG9pbnQgPiAyMSkge1xuICAgICAgICAvLyAgICAgbGV0IHR4dFRodWEgPSBgJHVzZXIgdGh1YWBcbiAgICAgICAgLy8gICAgIGlmICh0aGlzLmlzQ2FpKCkpIHtcbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gYWxlcnQoJ2NhaSB0aHVhOiAnICsgcG9pbnQgKyAnIGRpZW0nKVxuICAgICAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLmlzQm90KCkpIHtcbiAgICAgICAgLy8gICAgICAgICBhbGVydCgnYm90IHRodWE6ICcgKyBwb2ludCArICcgZGllbScpXG4gICAgICAgIC8vICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgICAgIGFsZXJ0KCd1c2VyIHRodWE6ICcgKyBwb2ludCArICcgZGllbScpXG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vIH1cbiAgICB9XG5cbiAgICBjaGVja0JsYWNrSmFjayA9ICgpID0+IHtcbiAgICAgICAgbGV0IHJhbmsxID0gdGhpcy5jYXJkc1swXT8ucmFua1xuICAgICAgICBsZXQgcmFuazIgPSB0aGlzLmNhcmRzWzFdPy5yYW5rXG5cbiAgICAgICAgaWYgKHJhbmsxID09PSAxIHx8IHJhbmsyID09PSAxKSB7XG4gICAgICAgICAgICBpZiAocmFuazEgPiA5IHx8IHJhbmsyID4gOSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVBvaW50KFwiQmxhY2sgSmFjayFcIilcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0Q2FyZFJvb3RQb3NpdGlvbigpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNhcmRSb290UG9zaXRpb24pIHtcbiAgICAgICAgICAgIHRoaXMuY2FyZFJvb3RQb3NpdGlvbiA9IGNjLnYyKHRoaXMubWFya2VyLnBvc2l0aW9uLnggKiB0aGlzLm5vZGUuc2NhbGVYICsgdGhpcy5ub2RlLnBvc2l0aW9uLngsXG4gICAgICAgICAgICAgICAgdGhpcy5tYXJrZXIucG9zaXRpb24ueSAqIHRoaXMubm9kZS5zY2FsZVkgKyB0aGlzLm5vZGUucG9zaXRpb24ueSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNjLnYyKHRoaXMuY2FyZFJvb3RQb3NpdGlvbik7XG4gICAgfVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IENBUkRfTUFSR0lOOiBudW1iZXIgPSA2MDtcbiAgICBzdGF0aWMgcmVhZG9ubHkgVElNRTogbnVtYmVyID0gMzA7XG59XG4iXX0=