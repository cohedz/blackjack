
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Deck.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cce96i6QBJMl7PTDBXefIeo', 'Deck');
// Scripts/Deck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Card_1 = require("./Card");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Deck = /** @class */ (function (_super) {
    __extends(Deck, _super);
    function Deck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cards = Array();
        _this.offset = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Deck.prototype.onLoad = function () {
        for (var i = 0; i < 52; i++) {
            var card = this.node.children[i].getComponent(Card_1.default);
            var cardName = card.node.name;
            card.rank = this.getRankFromName(cardName);
            card.suit = this.getSuitFromName(cardName);
            this.cards.push(card);
        }
    };
    // start() { }
    // update (dt) {}
    Deck.prototype.shuffle = function () {
        var length = this.cards.length;
        var lastIndex = length - 1;
        var index = -1;
        while (++index < length) {
            var rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
            var value = this.cards[rand];
            this.cards[rand] = this.cards[index];
            this.cards[index] = value;
        }
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            card.reset();
        }
        this.offset = 0;
        // this.cheat();
    };
    // cheat() {
    //     let pos = 0;
    //     for (let i = this.cards.length - 1; i >= 0; --i) {
    //         let card = this.cards[i];
    //         if (card.rank == 9) {
    //             let tmp = this.cards[pos];
    //             this.cards[pos] = card;
    //             this.cards[i] = tmp;
    //             pos += 4;
    //         }
    //     }
    // }
    Deck.prototype.pick = function () {
        var card = this.cards[this.offset];
        ++this.offset;
        return card;
    };
    Deck.prototype.hide = function () {
        for (var i = this.offset; i < this.cards.length; i++) {
            this.cards[i].node.active = false;
        }
    };
    Deck.prototype.getRankFromName = function (cardName) {
        if (cardName.length == 3)
            return 10;
        switch (cardName[0]) {
            case 'J': return 11;
            case 'Q': return 12;
            case 'K': return 13;
            case 'A': return 1;
            // case '2': return 15;
            default: return parseInt(cardName[0]);
        }
    };
    Deck.prototype.getSuitFromName = function (cardName) {
        switch (cardName[cardName.length - 1]) {
            case 'S': return 1;
            case 'C': return 2;
            case 'D': return 3;
            case 'H': return 4;
            default: throw new Error(cardName);
        }
    };
    Deck = __decorate([
        ccclass
    ], Deck);
    return Deck;
}(cc.Component));
exports.default = Deck;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0RlY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0JBQTBCO0FBRTFCLG9CQUFvQjtBQUNwQixrRkFBa0Y7QUFDbEYseUZBQXlGO0FBQ3pGLG1CQUFtQjtBQUNuQiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBQ25HLDhCQUE4QjtBQUM5Qiw0RkFBNEY7QUFDNUYsbUdBQW1HO0FBRTdGLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWtDLHdCQUFZO0lBQTlDO1FBQUEscUVBb0ZDO1FBbkZHLFdBQUssR0FBZ0IsS0FBSyxFQUFFLENBQUM7UUFDN0IsWUFBTSxHQUFXLENBQUMsQ0FBQzs7SUFrRnZCLENBQUM7SUFqRkcsd0JBQXdCO0lBRXhCLHFCQUFNLEdBQU47UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxjQUFJLENBQUMsQ0FBQztZQUNwRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQztJQUVELGNBQWM7SUFFZCxpQkFBaUI7SUFFakIsc0JBQU8sR0FBUDtRQUNJLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFBO1FBQ2hDLElBQU0sU0FBUyxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUE7UUFDNUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDZCxPQUFPLEVBQUUsS0FBSyxHQUFHLE1BQU0sRUFBRTtZQUNyQixJQUFNLElBQUksR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxTQUFTLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDeEUsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUE7U0FDNUI7UUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQzdDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsZ0JBQWdCO0lBQ3BCLENBQUM7SUFFRCxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLHlEQUF5RDtJQUN6RCxvQ0FBb0M7SUFDcEMsZ0NBQWdDO0lBQ2hDLHlDQUF5QztJQUN6QyxzQ0FBc0M7SUFDdEMsbUNBQW1DO0lBQ25DLHdCQUF3QjtJQUN4QixZQUFZO0lBQ1osUUFBUTtJQUNSLElBQUk7SUFFSixtQkFBSSxHQUFKO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELG1CQUFJLEdBQUo7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBRU8sOEJBQWUsR0FBdkIsVUFBd0IsUUFBZ0I7UUFDcEMsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDcEIsT0FBTyxFQUFFLENBQUM7UUFDZCxRQUFRLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNqQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDcEIsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNwQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2QixPQUFPLENBQUMsQ0FBQyxPQUFPLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFTyw4QkFBZSxHQUF2QixVQUF3QixRQUFnQjtRQUNwQyxRQUFRLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ25DLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkIsS0FBSyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuQixLQUFLLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25CLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN0QztJQUNMLENBQUM7SUFuRmdCLElBQUk7UUFEeEIsT0FBTztPQUNhLElBQUksQ0FvRnhCO0lBQUQsV0FBQztDQXBGRCxBQW9GQyxDQXBGaUMsRUFBRSxDQUFDLFNBQVMsR0FvRjdDO2tCQXBGb0IsSUFBSSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlY2sgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPiA9IEFycmF5KCk7XG4gICAgb2Zmc2V0OiBudW1iZXIgPSAwO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDUyOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5ub2RlLmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChDYXJkKTtcbiAgICAgICAgICAgIGxldCBjYXJkTmFtZSA9IGNhcmQubm9kZS5uYW1lO1xuICAgICAgICAgICAgY2FyZC5yYW5rID0gdGhpcy5nZXRSYW5rRnJvbU5hbWUoY2FyZE5hbWUpO1xuICAgICAgICAgICAgY2FyZC5zdWl0ID0gdGhpcy5nZXRTdWl0RnJvbU5hbWUoY2FyZE5hbWUpO1xuICAgICAgICAgICAgdGhpcy5jYXJkcy5wdXNoKGNhcmQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gc3RhcnQoKSB7IH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaHVmZmxlKCk6IHZvaWQge1xuICAgICAgICBjb25zdCBsZW5ndGggPSB0aGlzLmNhcmRzLmxlbmd0aFxuICAgICAgICBjb25zdCBsYXN0SW5kZXggPSBsZW5ndGggLSAxXG4gICAgICAgIGxldCBpbmRleCA9IC0xXG4gICAgICAgIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgICAgICAgICBjb25zdCByYW5kID0gaW5kZXggKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobGFzdEluZGV4IC0gaW5kZXggKyAxKSlcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5jYXJkc1tyYW5kXVxuICAgICAgICAgICAgdGhpcy5jYXJkc1tyYW5kXSA9IHRoaXMuY2FyZHNbaW5kZXhdXG4gICAgICAgICAgICB0aGlzLmNhcmRzW2luZGV4XSA9IHZhbHVlXG4gICAgICAgIH1cbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1tpXTtcbiAgICAgICAgICAgIGNhcmQucmVzZXQoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLm9mZnNldCA9IDA7XG4gICAgICAgIC8vIHRoaXMuY2hlYXQoKTtcbiAgICB9XG5cbiAgICAvLyBjaGVhdCgpIHtcbiAgICAvLyAgICAgbGV0IHBvcyA9IDA7XG4gICAgLy8gICAgIGZvciAobGV0IGkgPSB0aGlzLmNhcmRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgLy8gICAgICAgICBsZXQgY2FyZCA9IHRoaXMuY2FyZHNbaV07XG4gICAgLy8gICAgICAgICBpZiAoY2FyZC5yYW5rID09IDkpIHtcbiAgICAvLyAgICAgICAgICAgICBsZXQgdG1wID0gdGhpcy5jYXJkc1twb3NdO1xuICAgIC8vICAgICAgICAgICAgIHRoaXMuY2FyZHNbcG9zXSA9IGNhcmQ7XG4gICAgLy8gICAgICAgICAgICAgdGhpcy5jYXJkc1tpXSA9IHRtcDtcbiAgICAvLyAgICAgICAgICAgICBwb3MgKz0gNDtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgfVxuICAgIC8vIH1cblxuICAgIHBpY2soKTogQ2FyZCB7XG4gICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1t0aGlzLm9mZnNldF07XG4gICAgICAgICsrdGhpcy5vZmZzZXQ7XG4gICAgICAgIHJldHVybiBjYXJkO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLm9mZnNldDsgaSA8IHRoaXMuY2FyZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMuY2FyZHNbaV0ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0UmFua0Zyb21OYW1lKGNhcmROYW1lOiBzdHJpbmcpOiBudW1iZXIge1xuICAgICAgICBpZiAoY2FyZE5hbWUubGVuZ3RoID09IDMpXG4gICAgICAgICAgICByZXR1cm4gMTA7XG4gICAgICAgIHN3aXRjaCAoY2FyZE5hbWVbMF0pIHtcbiAgICAgICAgICAgIGNhc2UgJ0onOiByZXR1cm4gMTE7XG4gICAgICAgICAgICBjYXNlICdRJzogcmV0dXJuIDEyO1xuICAgICAgICAgICAgY2FzZSAnSyc6IHJldHVybiAxMztcbiAgICAgICAgICAgIGNhc2UgJ0EnOiByZXR1cm4gMTtcbiAgICAgICAgICAgIC8vIGNhc2UgJzInOiByZXR1cm4gMTU7XG4gICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gcGFyc2VJbnQoY2FyZE5hbWVbMF0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRTdWl0RnJvbU5hbWUoY2FyZE5hbWU6IHN0cmluZykge1xuICAgICAgICBzd2l0Y2ggKGNhcmROYW1lW2NhcmROYW1lLmxlbmd0aCAtIDFdKSB7XG4gICAgICAgICAgICBjYXNlICdTJzogcmV0dXJuIDE7XG4gICAgICAgICAgICBjYXNlICdDJzogcmV0dXJuIDI7XG4gICAgICAgICAgICBjYXNlICdEJzogcmV0dXJuIDM7XG4gICAgICAgICAgICBjYXNlICdIJzogcmV0dXJuIDQ7XG4gICAgICAgICAgICBkZWZhdWx0OiB0aHJvdyBuZXcgRXJyb3IoY2FyZE5hbWUpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19