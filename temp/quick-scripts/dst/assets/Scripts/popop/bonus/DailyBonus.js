
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/popop/bonus/DailyBonus.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2f9e8XOO6BHzap8qYPJCMDb', 'DailyBonus');
// Scripts/popop/bonus/DailyBonus.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Popup_1 = require("../../Popup");
var Config_1 = require("../../Config");
var DailyBonusItem_1 = require("./DailyBonusItem");
var Api_1 = require("../../Api");
var AutoHide_1 = require("../../AutoHide");
var util_1 = require("../../util");
var EventKeys_1 = require("../../EventKeys");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DailyBonus = /** @class */ (function (_super) {
    __extends(DailyBonus, _super);
    function DailyBonus() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.toast = null;
        _this.days = [];
        _this.effect = null;
        _this.current = 0;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    DailyBonus.prototype.start = function () {
    };
    // update (dt) {}
    DailyBonus.prototype.show = function (day, claimed) {
        if (claimed === void 0) { claimed = false; }
        if (!claimed) {
            day++;
        }
        if (day > Config_1.default.dailyBonus.length) {
            day = 1;
        }
        this.current = day;
        this.popup.open(this.node, 2);
        for (var i = 0; i < this.days.length; i++) {
            var d = i + 1;
            var claimable = (d == day) && !claimed;
            if (d < day) {
                this.days[i].setClaimable(claimable, true);
            }
            else if (d == day) {
                this.days[i].setClaimable(claimable, claimed);
            }
            else {
                this.days[i].setClaimable(claimable, false);
            }
        }
    };
    DailyBonus.prototype.onClaim = function (sender, day) {
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        this.onClaimCompl(sender, date, false);
        cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], false);
        this.popup.close(this.node, 2);
    };
    DailyBonus.prototype.onClaimExtra = function (sender, day) {
        var _this = this;
        if (Api_1.default.dailyBonusClaimed)
            return;
        var date = day ? parseInt(day) : this.current;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD);
        Api_1.default.showRewardedVideo(function () {
            _this.onClaimCompl(sender, date, false);
            cc.systemEvent.emit('claim_daily_bonus', date, Config_1.default.dailyBonus[date - 1], true);
            _this.popup.close(_this.node, 2);
        }, function () {
            cc.systemEvent.emit('claim_daily_bonus_fail');
            Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD_ERROR);
            _this.popup.close(_this.node, 2);
        });
    };
    DailyBonus.prototype.onClaimCompl = function (sender, day, double) {
        var gift = Config_1.default.dailyBonus[day - 1];
        if (!gift.coin) {
            return;
        }
        var e = cc.instantiate(this.effect);
        e.active = true;
        e.position = sender.target.position;
        e.scale = 1;
        e.parent = cc.director.getScene().getChildByName("Canvas");
        var coinBonus = double ? gift.coin * 2 : gift.coin;
        e.getComponent(cc.Label).string = util_1.default.numberFormat(coinBonus);
        e.runAction(cc.sequence(cc.moveBy(1.2, 0, 250), cc.removeSelf()));
    };
    __decorate([
        property(Popup_1.default)
    ], DailyBonus.prototype, "popup", void 0);
    __decorate([
        property(AutoHide_1.default)
    ], DailyBonus.prototype, "toast", void 0);
    __decorate([
        property(DailyBonusItem_1.default)
    ], DailyBonus.prototype, "days", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonus.prototype, "effect", void 0);
    DailyBonus = __decorate([
        ccclass
    ], DailyBonus);
    return DailyBonus;
}(cc.Component));
exports.default = DailyBonus;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3BvcG9wL2JvbnVzL0RhaWx5Qm9udXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYscUNBQWdDO0FBQ2hDLHVDQUFrQztBQUNsQyxtREFBOEM7QUFDOUMsaUNBQTRCO0FBQzVCLDJDQUFzQztBQUN0QyxtQ0FBOEI7QUFDOUIsNkNBQXdDO0FBRWxDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXdDLDhCQUFZO0lBQXBEO1FBQUEscUVBZ0ZDO1FBOUVHLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQXFCLEVBQUUsQ0FBQztRQUU1QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBQ3ZCLGFBQU8sR0FBVyxDQUFDLENBQUM7O0lBdUV4QixDQUFDO0lBdEVHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYsMEJBQUssR0FBTDtJQUVBLENBQUM7SUFFRCxpQkFBaUI7SUFFakIseUJBQUksR0FBSixVQUFLLEdBQVcsRUFBRSxPQUF3QjtRQUF4Qix3QkFBQSxFQUFBLGVBQXdCO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFBQyxHQUFHLEVBQUUsQ0FBQztTQUFDO1FBQ3RCLElBQUksR0FBRyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtZQUNoQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFNLENBQUMsR0FBRyxDQUFDLEdBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBTSxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUNULElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM5QztpQkFBTSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNqRDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDL0M7U0FDSjtJQUNMLENBQUM7SUFFRCw0QkFBTyxHQUFQLFVBQVEsTUFBZSxFQUFFLEdBQVc7UUFDaEMsSUFBSSxhQUFHLENBQUMsaUJBQWlCO1lBQUUsT0FBTztRQUNsQyxJQUFNLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNoRCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDdkMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLGdCQUFNLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsTUFBZSxFQUFFLEdBQVc7UUFBekMsaUJBYUM7UUFaRyxJQUFJLGFBQUcsQ0FBQyxpQkFBaUI7WUFBRSxPQUFPO1FBQ2xDLElBQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2hELGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlDLGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdkMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLGdCQUFNLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRixLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsRUFBRTtZQUNDLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDOUMsYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDcEQsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsTUFBTSxFQUFFLEdBQVcsRUFBRSxNQUFlO1FBQzdDLElBQU0sSUFBSSxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNaLE9BQU07U0FDVDtRQUNELElBQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUNuQixFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQ3RCLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FDbEIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQTdFRDtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7NkNBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsa0JBQVEsQ0FBQzs2Q0FDSTtJQUV2QjtRQURDLFFBQVEsQ0FBQyx3QkFBYyxDQUFDOzRDQUNHO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ0s7SUFSTixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBZ0Y5QjtJQUFELGlCQUFDO0NBaEZELEFBZ0ZDLENBaEZ1QyxFQUFFLENBQUMsU0FBUyxHQWdGbkQ7a0JBaEZvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBQb3B1cCBmcm9tIFwiLi4vLi4vUG9wdXBcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4uLy4uL0NvbmZpZ1wiO1xuaW1wb3J0IERhaWx5Qm9udXNJdGVtIGZyb20gXCIuL0RhaWx5Qm9udXNJdGVtXCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuLi8uLi9BcGlcIjtcbmltcG9ydCBBdXRvSGlkZSBmcm9tIFwiLi4vLi4vQXV0b0hpZGVcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuLi8uLi91dGlsXCI7XG5pbXBvcnQgRXZlbnRLZXlzIGZyb20gXCIuLi8uLi9FdmVudEtleXNcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERhaWx5Qm9udXMgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShBdXRvSGlkZSlcbiAgICB0b2FzdDogQXV0b0hpZGUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShEYWlseUJvbnVzSXRlbSlcbiAgICBkYXlzOiBEYWlseUJvbnVzSXRlbVtdID0gW107XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBjdXJyZW50OiBudW1iZXIgPSAwO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG5cbiAgICBzaG93KGRheTogbnVtYmVyLCBjbGFpbWVkOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgaWYgKCFjbGFpbWVkKSB7ZGF5Kys7fVxuICAgICAgICBpZiAoZGF5ID4gQ29uZmlnLmRhaWx5Qm9udXMubGVuZ3RoKSB7XG4gICAgICAgICAgICBkYXkgPSAxO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY3VycmVudCA9IGRheTtcbiAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMik7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5kYXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBkID0gaSsxO1xuICAgICAgICAgICAgY29uc3QgY2xhaW1hYmxlID0gKGQgPT0gZGF5KSAmJiAhY2xhaW1lZDtcbiAgICAgICAgICAgIGlmIChkIDwgZGF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzW2ldLnNldENsYWltYWJsZShjbGFpbWFibGUsIHRydWUpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChkID09IGRheSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZGF5c1tpXS5zZXRDbGFpbWFibGUoY2xhaW1hYmxlLCBjbGFpbWVkKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzW2ldLnNldENsYWltYWJsZShjbGFpbWFibGUsIGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ2xhaW0oc2VuZGVyOiBjYy5Ob2RlLCBkYXk6IHN0cmluZykge1xuICAgICAgICBpZiAoQXBpLmRhaWx5Qm9udXNDbGFpbWVkKSByZXR1cm47XG4gICAgICAgIGNvbnN0IGRhdGUgPSBkYXkgPyBwYXJzZUludChkYXkpIDogdGhpcy5jdXJyZW50O1xuICAgICAgICB0aGlzLm9uQ2xhaW1Db21wbChzZW5kZXIsIGRhdGUsIGZhbHNlKTtcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCgnY2xhaW1fZGFpbHlfYm9udXMnLCBkYXRlLCBDb25maWcuZGFpbHlCb251c1tkYXRlIC0gMV0sIGZhbHNlKTtcbiAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgIH1cblxuICAgIG9uQ2xhaW1FeHRyYShzZW5kZXI6IGNjLk5vZGUsIGRheTogc3RyaW5nKSB7XG4gICAgICAgIGlmIChBcGkuZGFpbHlCb251c0NsYWltZWQpIHJldHVybjtcbiAgICAgICAgY29uc3QgZGF0ZSA9IGRheSA/IHBhcnNlSW50KGRheSkgOiB0aGlzLmN1cnJlbnQ7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUE9QVVBfREFJTFlfUkVXQVJEX0FEKTtcbiAgICAgICAgQXBpLnNob3dSZXdhcmRlZFZpZGVvKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25DbGFpbUNvbXBsKHNlbmRlciwgZGF0ZSwgZmFsc2UpO1xuICAgICAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCgnY2xhaW1fZGFpbHlfYm9udXMnLCBkYXRlLCBDb25maWcuZGFpbHlCb251c1tkYXRlIC0gMV0sIHRydWUpO1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KCdjbGFpbV9kYWlseV9ib251c19mYWlsJyk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0RBSUxZX1JFV0FSRF9BRF9FUlJPUik7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKHRoaXMubm9kZSwgMik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIG9uQ2xhaW1Db21wbChzZW5kZXIsIGRheTogbnVtYmVyLCBkb3VibGU6IGJvb2xlYW4pIHtcbiAgICAgICAgY29uc3QgZ2lmdCA9IENvbmZpZy5kYWlseUJvbnVzW2RheSAtIDFdO1xuICAgICAgICBpZiAoIWdpZnQuY29pbikge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuZWZmZWN0KTtcbiAgICAgICAgZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBlLnBvc2l0aW9uID0gc2VuZGVyLnRhcmdldC5wb3NpdGlvbjtcbiAgICAgICAgZS5zY2FsZSA9IDE7XG4gICAgICAgIGUucGFyZW50ID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKTtcbiAgICAgICAgY29uc3QgY29pbkJvbnVzID0gZG91YmxlID8gZ2lmdC5jb2luICogMiA6IGdpZnQuY29pbjtcbiAgICAgICAgZS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KGNvaW5Cb251cyk7XG4gICAgICAgIGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MubW92ZUJ5KDEuMiwgMCwgMjUwKSxcbiAgICAgICAgICAgIGNjLnJlbW92ZVNlbGYoKVxuICAgICAgICApKVxuICAgIH1cbn1cbiJdfQ==