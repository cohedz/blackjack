
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Notice.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6cfdf+Ec8NOfKCqLqWb3Un0', 'Notice');
// Scripts/Notice.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Alert = /** @class */ (function (_super) {
    __extends(Alert, _super);
    function Alert() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
    }
    Alert_1 = Alert;
    // LIFE-CYCLE CALLBACKS:
    Alert.prototype.onLoad = function () {
        this.label = this.getComponent(cc.Label);
    };
    Alert.prototype.start = function () { };
    Alert.prototype.showBigPig = function (num, duration) {
        if (num == 2) {
            this.show(Alert_1.TWO_PIG, duration);
        }
        else if (num == 3) {
            this.show(Alert_1.THREE_PIG, duration);
        }
        else {
            this.show(Alert_1.ONE_PIG, duration);
        }
    };
    Alert.prototype.show = function (type, duration) {
        this.node.active = true;
        this.label.string = Language_1.default.getInstance().get(type);
        var action = cc.sequence(cc.delayTime(duration), cc.callFunc(this.hide, this));
        this.node.runAction(action);
    };
    Alert.prototype.hide = function () {
        this.node.active = false;
    };
    var Alert_1;
    // update (dt) {}
    Alert.ONE_PIG = '1BEST';
    Alert.TWO_PIG = '2BEST';
    Alert.THREE_PIG = '3BEST';
    Alert.THREE_PAIR = '3PAIRS';
    Alert.FOUR_CARD = 'FOURKIND';
    Alert.FOUR_PAIR = '4PAIRS';
    Alert = Alert_1 = __decorate([
        ccclass
    ], Alert);
    return Alert;
}(cc.Component));
exports.default = Alert;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL05vdGljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRix1Q0FBa0M7QUFFNUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBbUMseUJBQVk7SUFBL0M7UUFBQSxxRUEwQ0M7UUF6Q1csV0FBSyxHQUFhLElBQUksQ0FBQzs7SUF5Q25DLENBQUM7Y0ExQ29CLEtBQUs7SUFFdEIsd0JBQXdCO0lBRXhCLHNCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxxQkFBSyxHQUFMLGNBQVcsQ0FBQztJQUVaLDBCQUFVLEdBQVYsVUFBVyxHQUFXLEVBQUUsUUFBZ0I7UUFDcEMsSUFBRyxHQUFHLElBQUksQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ3RDO2FBQU0sSUFBRyxHQUFHLElBQUksQ0FBQyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBSyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQztJQUVELG9CQUFJLEdBQUosVUFBSyxJQUFZLEVBQUUsUUFBZ0I7UUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQ3BCLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQ3RCLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDL0IsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxvQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7O0lBRUQsaUJBQWlCO0lBRUQsYUFBTyxHQUFXLE9BQU8sQ0FBQztJQUMxQixhQUFPLEdBQVcsT0FBTyxDQUFDO0lBQzFCLGVBQVMsR0FBVyxPQUFPLENBQUM7SUFDNUIsZ0JBQVUsR0FBVyxRQUFRLENBQUM7SUFDOUIsZUFBUyxHQUFXLFVBQVUsQ0FBQztJQUMvQixlQUFTLEdBQVcsUUFBUSxDQUFDO0lBekM1QixLQUFLO1FBRHpCLE9BQU87T0FDYSxLQUFLLENBMEN6QjtJQUFELFlBQUM7Q0ExQ0QsQUEwQ0MsQ0ExQ2tDLEVBQUUsQ0FBQyxTQUFTLEdBMEM5QztrQkExQ29CLEtBQUsiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IExhbmd1YWdlIGZyb20gXCIuL0xhbmd1YWdlXCI7XG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWxlcnQgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIHByaXZhdGUgbGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcblxuICAgIG9uTG9hZCAoKSB7XG4gICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmdldENvbXBvbmVudChjYy5MYWJlbCk7XG4gICAgfVxuXG4gICAgc3RhcnQgKCkgeyB9XG5cbiAgICBzaG93QmlnUGlnKG51bTogbnVtYmVyLCBkdXJhdGlvbjogbnVtYmVyKSB7XG4gICAgICAgIGlmKG51bSA9PSAyKSB7XG4gICAgICAgICAgICB0aGlzLnNob3coQWxlcnQuVFdPX1BJRywgZHVyYXRpb24pO1xuICAgICAgICB9IGVsc2UgaWYobnVtID09IDMpIHtcbiAgICAgICAgICAgIHRoaXMuc2hvdyhBbGVydC5USFJFRV9QSUcsIGR1cmF0aW9uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc2hvdyhBbGVydC5PTkVfUElHLCBkdXJhdGlvbik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzaG93KHR5cGU6IHN0cmluZywgZHVyYXRpb246IG51bWJlcikge1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCh0eXBlKTtcbiAgICAgICAgbGV0IGFjdGlvbiA9IGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MuZGVsYXlUaW1lKGR1cmF0aW9uKSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihhY3Rpb24pO1xuICAgIH1cblxuICAgIGhpZGUoKSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IE9ORV9QSUc6IHN0cmluZyA9ICcxQkVTVCc7XG4gICAgc3RhdGljIHJlYWRvbmx5IFRXT19QSUc6IHN0cmluZyA9ICcyQkVTVCc7XG4gICAgc3RhdGljIHJlYWRvbmx5IFRIUkVFX1BJRzogc3RyaW5nID0gJzNCRVNUJztcbiAgICBzdGF0aWMgcmVhZG9ubHkgVEhSRUVfUEFJUjogc3RyaW5nID0gJzNQQUlSUyc7XG4gICAgc3RhdGljIHJlYWRvbmx5IEZPVVJfQ0FSRDogc3RyaW5nID0gJ0ZPVVJLSU5EJztcbiAgICBzdGF0aWMgcmVhZG9ubHkgRk9VUl9QQUlSOiBzdHJpbmcgPSAnNFBBSVJTJztcbn1cbiJdfQ==