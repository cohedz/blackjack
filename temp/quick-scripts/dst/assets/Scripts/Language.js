
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Language.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3d3c6mK5nNMjYk5tCjZK4el', 'Language');
// Scripts/Language.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vi_VN_1 = require("../lang/vi_VN");
var th_TH_1 = require("../lang/th_TH");
var en_US_1 = require("../lang/en_US");
var tl_PH_1 = require("../lang/tl_PH");
var Language = /** @class */ (function () {
    function Language() {
        this.lang = en_US_1.default;
    }
    Language.getInstance = function () {
        if (!Language._instance) {
            Language._instance = new Language();
        }
        return Language._instance;
    };
    Language.prototype.load = function (lang) {
        if (lang == 'vi_VN') {
            this.lang = vi_VN_1.default;
        }
        else if (lang == 'th_TH') {
            this.lang = th_TH_1.default;
        }
        else if (lang == 'tl_PH') {
            this.lang = tl_PH_1.default;
        }
        else {
            this.lang = en_US_1.default;
        }
    };
    Language.prototype.get = function (key) {
        return this.lang[key];
    };
    Language._instance = null;
    return Language;
}());
exports.default = Language;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0xhbmd1YWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQStCO0FBQy9CLHVDQUErQjtBQUMvQix1Q0FBK0I7QUFDL0IsdUNBQStCO0FBRS9CO0lBQUE7UUFHWSxTQUFJLEdBQVEsZUFBRSxDQUFDO0lBeUIzQixDQUFDO0lBdkJpQixvQkFBVyxHQUF6QjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFO1lBQ3JCLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztTQUN2QztRQUNELE9BQU8sUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUM5QixDQUFDO0lBRU0sdUJBQUksR0FBWCxVQUFZLElBQVk7UUFDcEIsSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBRSxDQUFDO1NBQ2xCO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLGVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFTSxzQkFBRyxHQUFWLFVBQVcsR0FBVztRQUNsQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQXpCYyxrQkFBUyxHQUFhLElBQUksQ0FBQztJQTJCOUMsZUFBQztDQTVCRCxBQTRCQyxJQUFBO2tCQTVCb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB2aSBmcm9tICcuLi9sYW5nL3ZpX1ZOJztcbmltcG9ydCB0aCBmcm9tICcuLi9sYW5nL3RoX1RIJztcbmltcG9ydCBlbiBmcm9tICcuLi9sYW5nL2VuX1VTJztcbmltcG9ydCBwaCBmcm9tICcuLi9sYW5nL3RsX1BIJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGFuZ3VhZ2Uge1xuICAgIHByaXZhdGUgc3RhdGljIF9pbnN0YW5jZTogTGFuZ3VhZ2UgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBsYW5nOiBhbnkgPSBlbjtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0SW5zdGFuY2UoKSB7XG4gICAgICAgIGlmICghTGFuZ3VhZ2UuX2luc3RhbmNlKSB7XG4gICAgICAgICAgICBMYW5ndWFnZS5faW5zdGFuY2UgPSBuZXcgTGFuZ3VhZ2UoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gTGFuZ3VhZ2UuX2luc3RhbmNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBsb2FkKGxhbmc6IHN0cmluZykge1xuICAgICAgICBpZiAobGFuZyA9PSAndmlfVk4nKSB7XG4gICAgICAgICAgICB0aGlzLmxhbmcgPSB2aTtcbiAgICAgICAgfSBlbHNlIGlmIChsYW5nID09ICd0aF9USCcpIHtcbiAgICAgICAgICAgIHRoaXMubGFuZyA9IHRoO1xuICAgICAgICB9IGVsc2UgaWYgKGxhbmcgPT0gJ3RsX1BIJykge1xuICAgICAgICAgICAgdGhpcy5sYW5nID0gcGg7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmxhbmcgPSBlbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBnZXQoa2V5OiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubGFuZ1trZXldO1xuICAgIH1cblxufVxuIl19