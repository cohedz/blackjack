
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/EventKeys.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fef6cR3M0xAfLpe1NdULNzR', 'EventKeys');
// Scripts/EventKeys.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventKeys = {
// PLAY_NOW: 'play_now',
// SHOP_GO: 'shop_go',
// VIP_GO: 'vip_go',
// DAILY_GIFT_CLICK: 'daily_gift_click',
// PLAY_MODE: 'play_mode',
// PLAY_ACCEPT: 'play_accept',
// QUIT_GAME: 'quit_game',
// WIN: 'win',
// LOSE: 'lose',
// PLAY_DURATION: 'play_duration',
// PLAY_TIME: 'play_time',
// INVITE_FRIEND: 'invite_friend',
// SPIN: 'spin',
// PLAY_WITH_FRIEND: 'play_with_friend',
// POPUP_ADREWARD_COIN_OPEN: '100k_popup',
// POPUP_ADREWARD_COIN_CLICK: '100k_clickpopup',
// POPUP_ADREWARD_COIN_ERROR: '100k_novideopopup',
// POPUP_ADREWARD_SPIN_OPEN: 'spin_popup',
// POPUP_ADREWARD_SPIN_CLICK: 'spin_click_popup',
// POPUP_ADREWARD_SPIN_ERROR: 'spin_novideo_popup',
// POPUP_DAILY_REWARD_SHOW: 'daily_reward_popup',
// POPUP_DAILY_REWARD_AD: 'daily_reward_click_video',
// POPUP_DAILY_REWARD_AD_ERROR: 'daily_reward_no_video',
};
exports.default = EventKeys;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0V2ZW50S2V5cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU0sU0FBUyxHQUFHO0FBQ2Qsd0JBQXdCO0FBQ3hCLHNCQUFzQjtBQUN0QixvQkFBb0I7QUFDcEIsd0NBQXdDO0FBQ3hDLDBCQUEwQjtBQUMxQiw4QkFBOEI7QUFDOUIsMEJBQTBCO0FBQzFCLGNBQWM7QUFDZCxnQkFBZ0I7QUFDaEIsa0NBQWtDO0FBQ2xDLDBCQUEwQjtBQUMxQixrQ0FBa0M7QUFDbEMsZ0JBQWdCO0FBQ2hCLHdDQUF3QztBQUN4QywwQ0FBMEM7QUFDMUMsZ0RBQWdEO0FBQ2hELGtEQUFrRDtBQUNsRCwwQ0FBMEM7QUFDMUMsaURBQWlEO0FBQ2pELG1EQUFtRDtBQUNuRCxpREFBaUQ7QUFDakQscURBQXFEO0FBQ3JELHdEQUF3RDtDQUMzRCxDQUFBO0FBRUQsa0JBQWUsU0FBUyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgRXZlbnRLZXlzID0ge1xuICAgIC8vIFBMQVlfTk9XOiAncGxheV9ub3cnLFxuICAgIC8vIFNIT1BfR086ICdzaG9wX2dvJyxcbiAgICAvLyBWSVBfR086ICd2aXBfZ28nLFxuICAgIC8vIERBSUxZX0dJRlRfQ0xJQ0s6ICdkYWlseV9naWZ0X2NsaWNrJyxcbiAgICAvLyBQTEFZX01PREU6ICdwbGF5X21vZGUnLFxuICAgIC8vIFBMQVlfQUNDRVBUOiAncGxheV9hY2NlcHQnLFxuICAgIC8vIFFVSVRfR0FNRTogJ3F1aXRfZ2FtZScsXG4gICAgLy8gV0lOOiAnd2luJyxcbiAgICAvLyBMT1NFOiAnbG9zZScsXG4gICAgLy8gUExBWV9EVVJBVElPTjogJ3BsYXlfZHVyYXRpb24nLFxuICAgIC8vIFBMQVlfVElNRTogJ3BsYXlfdGltZScsXG4gICAgLy8gSU5WSVRFX0ZSSUVORDogJ2ludml0ZV9mcmllbmQnLFxuICAgIC8vIFNQSU46ICdzcGluJyxcbiAgICAvLyBQTEFZX1dJVEhfRlJJRU5EOiAncGxheV93aXRoX2ZyaWVuZCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9PUEVOOiAnMTAwa19wb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9DTElDSzogJzEwMGtfY2xpY2twb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfQ09JTl9FUlJPUjogJzEwMGtfbm92aWRlb3BvcHVwJyxcbiAgICAvLyBQT1BVUF9BRFJFV0FSRF9TUElOX09QRU46ICdzcGluX3BvcHVwJyxcbiAgICAvLyBQT1BVUF9BRFJFV0FSRF9TUElOX0NMSUNLOiAnc3Bpbl9jbGlja19wb3B1cCcsXG4gICAgLy8gUE9QVVBfQURSRVdBUkRfU1BJTl9FUlJPUjogJ3NwaW5fbm92aWRlb19wb3B1cCcsXG4gICAgLy8gUE9QVVBfREFJTFlfUkVXQVJEX1NIT1c6ICdkYWlseV9yZXdhcmRfcG9wdXAnLFxuICAgIC8vIFBPUFVQX0RBSUxZX1JFV0FSRF9BRDogJ2RhaWx5X3Jld2FyZF9jbGlja192aWRlbycsXG4gICAgLy8gUE9QVVBfREFJTFlfUkVXQVJEX0FEX0VSUk9SOiAnZGFpbHlfcmV3YXJkX25vX3ZpZGVvJyxcbn1cblxuZXhwb3J0IGRlZmF1bHQgRXZlbnRLZXlzOyJdfQ==