
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c8251pV8NZGpa2LG0Z5yRed', 'Game');
// Scripts/Game.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = require("./Player");
var Deck_1 = require("./Deck");
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var CommonCard_1 = require("./CommonCard");
var Toast_1 = require("./Toast");
var Api_1 = require("./Api");
var Notice_1 = require("./Notice");
var Config_1 = require("./Config");
var util_1 = require("./util");
var SpinWheel_1 = require("./SpinWheel");
var Popup_1 = require("./Popup");
var Bot_1 = require("./Bot");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var suiteAsset = {
    H: 'co',
    D: "ro",
    C: "chuon",
    S: "bích"
};
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Profile = /** @class */ (function () {
    function Profile() {
        this.avatar = null;
        this.username = '';
    }
    __decorate([
        property(cc.SpriteFrame)
    ], Profile.prototype, "avatar", void 0);
    __decorate([
        property(cc.String)
    ], Profile.prototype, "username", void 0);
    Profile = __decorate([
        ccclass('Profile')
    ], Profile);
    return Profile;
}());
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deck = null;
        _this.play = null;
        _this.seats = [];
        _this.fire = null;
        _this.fold = null;
        _this.sort = null;
        _this.resultEffects = [];
        _this.bigWin = null;
        _this.toast = null;
        _this.notice = null;
        _this.betText = null;
        _this.spin = null;
        _this.popup = null;
        _this.profileBots = [];
        _this.soundToggle = null;
        _this.audioFold = null;
        _this.audioShowCard = null;
        _this.audioGarbageCard = null;
        _this.audioFire = null;
        _this.audioLose = null;
        _this.audioWin = null;
        _this.audioSortCard = null;
        _this.audioQuitGame = null;
        _this.audioDealCard = null;
        _this.audioFireSingle = null;
        _this.modal = null;
        _this.players = [];
        _this.commonCards = new CommonCard_1.default();
        _this.state = 0;
        _this.turn = 0;
        _this.leaveGame = false;
        _this.owner = null;
        _this.myself = null;
        _this.betValue = 1000;
        _this.totalPlayer = 6;
        _this.startTime = 0;
        _this.playTime = 0;
        _this.addSeat = function (seat) {
            var profile = _this.getProfileBot();
            seat.show();
            seat.setAvatar(profile.avatar);
            seat.setUsername(profile.username);
            _this.players.push(seat);
        };
        _this.showAllPlayerCard = function () {
            _this.players.map(function (player) {
                player.showAllCard();
            });
        };
        _this.showResult = function () {
            // let leaderPoint = this.players.slice(-1)[0].point
            // let userPoint = this.players[0].point
            // let resultString = [`điểm cái: ${leaderPoint}`, `Điểm user: ${userPoint}`]
            // this.players.map(p => {
            //     if (p.isBot() && !p.isCai()) {
            //         resultString.push(`Điểm bot ${resultString.length - 1}: ${p.point}`)
            //     }
            // })
            var dealer = _this.players[0];
            var dealerAddCoin = 0;
            _this.players.map(function (player, index) {
                if (!index)
                    return;
                var playerCoin = _this.calCointEndGame(player);
                if (player.subUser) {
                    playerCoin += _this.calCointEndGame(player.subUser);
                }
                player.changeCoin(playerCoin);
                console.log('====================================');
                console.log('user ' + index + ' ' + playerCoin);
                console.log('====================================');
                dealerAddCoin -= playerCoin;
            });
            dealer.changeCoin(dealerAddCoin);
            console.log('====================================');
            console.log('nha cai ' + dealerAddCoin);
            console.log('====================================');
            _this.showAllPlayerCard();
            _this.delayReset(3);
            _this.state = Game_1.LATE;
            // this.owner = winner;
            // this.bigWin.active = true;
            // let total = 0;
            // for (let i = 0; i < this.players.length; i++) {
            //     let player = this.players[i];
            //     if (player != winner) {
            //         if (player.isBot()) {
            //             player.hideCardCounter();
            //             player.showHandCards();
            //             player.reorder();
            //         }
            //         let lost = 13 * this.betValue;
            //         lost = Math.min(player.coinVal, lost);
            //         player.subCoin(lost);
            //         total += lost;
            //     }
            // }
            // winner.addCoin(total);
            Api_1.default.updateCoin(_this.myself.coinVal);
            // this.onWin(total);
            // return this.toast.show(`Tổng cmn kết rồi\n ${resultString.join('\n')}`)
        };
        _this.calCointEndGame = function (player) {
            var dealer = _this.players[0];
            var playerCoin = 0;
            if (player.bonusType == 2) {
                playerCoin -= _this.betValue / 2;
            }
            else if (player.point > 21) {
                playerCoin -= _this.betValue;
                // player.subCoin(this.betValue)
            }
            else if (player.checkBlackJack()) {
                playerCoin += _this.betValue * 1.5;
            }
            else if (player.point > dealer.point) {
                playerCoin += _this.betValue;
            }
            else if (player.point === dealer.point) {
                return 0;
            }
            else if (dealer.point > 21) {
                playerCoin += _this.betValue;
            }
            else {
                playerCoin -= _this.betValue;
            }
            if (/1/.test(player.bonusType.toString())) {
                playerCoin *= 2;
            }
            return playerCoin;
        };
        _this.checkWin = function () {
            // console.log('====================================');
            // console.log(this.players);
            // console.log('====================================');
            return new Promise(function (resolve) {
                // this.players.
            });
        };
        return _this;
    }
    Game_1 = Game;
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        var _this = this;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        var As = cc.find('Canvas/cards/AS');
        cc.resources.load('cards', cc.SpriteAtlas, function (err, atlas) {
            var initCardsResouces = new Promise(function (resolve) {
                new Array(13).fill('').map(function (el, rank) {
                    ['S', 'C', 'D', 'H'].map(function (suite, idSuite) {
                        if (!rank && !suite)
                            return;
                        var r = rank + 1;
                        var newNode = cc.instantiate(As);
                        switch (r) {
                            case 1:
                                r = 'A';
                                break;
                            case 11:
                                r = 'J';
                                break;
                            case 12:
                                r = 'Q';
                                break;
                            case 13:
                                r = 'K';
                                break;
                        }
                        var assetSuiteName = suiteAsset[suite];
                        var rankAssetname = r;
                        if (idSuite < 2)
                            rankAssetname += '-1';
                        // console.log('====================================');
                        // console.log(rankAssetname, assetSuiteName,);
                        // console.log('====================================');
                        newNode.name = r + suite;
                        var suiteSprite = atlas.getSpriteFrame(assetSuiteName);
                        newNode.children[0].getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(rankAssetname);
                        newNode.children[1].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[2].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                        newNode.children[3].getComponent(cc.Widget).target = newNode;
                        newNode.active = false;
                        // console.log('====================================');
                        // console.log(newNode);
                        // console.log('====================================');
                        As.parent.addChild(newNode);
                        if (rank === 12 && suite === 'H') {
                            setTimeout(function () {
                                resolve(1);
                            }, 500);
                        }
                    });
                });
            });
            Promise.all([initCardsResouces]).then(function (rs) {
                _this.initGame();
            }).catch(function (err) {
                console.log('====================================');
                console.log(err);
                console.log('====================================');
            });
        });
    };
    // start(){}
    Game.prototype.initGame = function () {
        var _this = this;
        this.startTime = Date.now();
        this.state = Game_1.WAIT;
        this.betValue = Config_1.default.betValue;
        this.totalPlayer = Config_1.default.totalPlayer;
        this.betText.string = Language_1.default.getInstance().get('NO') + ': ' + util_1.default.numberFormat(this.betValue);
        this.deck.node.active = false;
        this.sort.node.active = false;
        this.play.node.active = false;
        this.hideDashboard();
        this.notice.hide();
        this.spin.onSpinHide = this.onSpinHide.bind(this);
        this.spin.onSpinCompleted = this.onSpinCompleted.bind(this);
        if (Config_1.default.battle) {
            this.totalPlayer = 2;
        }
        var arr = /0|3/;
        if (this.totalPlayer === 4)
            arr = /0|1|3|5/;
        else if (this.totalPlayer === 6)
            arr = /0|1|2|3|4|5/;
        this.seats.map(function (seat, i) {
            seat.seat = _this.players.length;
            if (arr.test(i.toString()))
                _this.addSeat(seat);
            else {
                seat.hide();
            }
        });
        // for (let i = 0; i < this.totalPlayer; i++) {
        //     let seat = this.seats[i];
        //     seat.seat = i;
        //     if (i < this.totalPlayer) {
        //         let profile = this.getProfileBot();
        //         seat.show();
        //         seat.setAvatar(profile.avatar);
        //         seat.setUsername(profile.username);
        //         this.players.push(seat);
        //     } else {
        //         seat.hide();
        //     }
        // }
        this.myself = this.players[this.totalPlayer / 2];
        this.myself.setUsername(Api_1.default.username);
        this.myself.setCoin(Api_1.default.coin);
        this.myself.setTimeCallback(this.onPlayerTimeout, this, this.node);
        if (Config_1.default.userphoto) {
            this.myself.setAvatar(new cc.SpriteFrame(Config_1.default.userphoto));
        }
        if (Config_1.default.battle) {
            var bot = this.players[1];
            bot.setCoin(Config_1.default.battle.coin);
            bot.setAvatar(new cc.SpriteFrame(Config_1.default.battle.photo));
            bot.setUsername(Config_1.default.battle.username);
        }
        else {
            this.players.map(function (player) {
                if (player.isBot())
                    player.setCoin(Config_1.default.botCoin);
            });
        }
        this.node.runAction(cc.sequence(cc.delayTime(0.7), cc.callFunc(this.deal, this)));
        if (Config_1.default.soundEnable) {
            this.soundToggle.uncheck();
        }
        else {
            this.soundToggle.check();
        }
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.onDestroy = function () {
        // this.clearLogTime()
        // const duration = Date.now() - this.startTime;
        // Api.logEvent(EventKeys.PLAY_DURATION, Math.floor(duration / 1000));
    };
    // update(dt) {
    //     if (this.playTime < 30 && this.playTime + dt >= 30) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-30');
    //     } else if (this.playTime < 60 && this.playTime + dt >= 60) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-60');
    //     } else if (this.playTime < 90 && this.playTime + dt >= 90) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-90');
    //     } else if (this.playTime < 180 && this.playTime + dt >= 180) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-180');
    //     } else if (this.playTime < 360 && this.playTime + dt >= 360) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-360');
    //     } else if (this.playTime < 500 && this.playTime + dt >= 500) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-500');
    //     }
    //     this.playTime += dt;
    // }
    Game.prototype.onTouchStart = function (evt) {
        if (this.state != Game_1.PLAY)
            return;
        var selected = this.myself.touch(evt.getLocation());
        if (!selected)
            return;
        var prepareCards = this.myself.prepareCards;
        if (prepareCards.contains(selected)) {
            this.myself.unselectCard(selected);
            this.onCardSelected(selected, false);
        }
        else {
            this.myself.selectCard(selected);
            this.onCardSelected(selected, true);
        }
        if (this.turn != this.myself.seat) {
            return;
        }
        if (prepareCards.kind == CardGroup_1.default.Ungrouped) {
            this.allowFire(false);
            return;
        }
        if (this.commonCards.length() == 0) {
            this.allowFire(true);
            return;
        }
        if (prepareCards.gt(this.commonCards.peek())) {
            this.allowFire(true);
            return;
        }
        this.allowFire(false);
    };
    Game.prototype.onPlayerTimeout = function () {
        this.onStandClicked();
        // this.hideDashboard();
        // if (this.commonCards.length() == 0) {
        //     let cards = Helper.findMinCard(this.myself.cards);
        //     this.onFire(this.myself, new CardGroup([cards], CardGroup.Single));
        //     this.myself.reorder();
        // } else {
        //     return this.onFold(this.myself);
        // }
    };
    Game.prototype.onQuitClicked = function () {
        Api_1.default.logEvent(EventKeys_1.default.QUIT_GAME);
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            this.popup.open(this.node, 1);
        }
        else {
            util_1.default.playAudio(this.audioQuitGame);
            cc.director.loadScene('home');
            Api_1.default.showInterstitialAd();
        }
    };
    Game.prototype.onBackClicked = function () {
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
            var coin = Math.max(0, Api_1.default.coin - this.betValue * 10);
            Api_1.default.updateCoin(coin);
        }
        util_1.default.playAudio(this.audioQuitGame);
        cc.director.loadScene('home');
        Api_1.default.showInterstitialAd();
    };
    Game.prototype.showHandCards = function () {
        util_1.default.playAudio(this.audioShowCard);
        // this.myself.showHandCards();
        this.state = Game_1.PLAY;
        // console.log('====================================');
        // console.log(this.myself.cards);
        // console.log('====================================');
        // if (Helper.isBigWin(this.myself.cards)) {
        //     this.node.runAction(cc.sequence(
        //         cc.delayTime(0.7),
        //         cc.callFunc(this.onBigWin, this, this.myself)
        //     ));
        //     return;
        // }
        // this.node.runAction(cc.sequence(
        //     cc.delayTime(0.7),
        //     cc.callFunc(this.myself.sortHandCards, this.myself)
        // ));
        this.players.map(function (player, index) {
            if (player.isBot())
                player.showCardCounter();
            else {
                player.showAllCard();
            }
        });
        var checkBJ = this.players.filter(function (player) {
            player.checkBlackJack();
        });
        // let isUserWin = checkBJ.find(player => player.isUser())
        // let isUserOrDealerWin = checkBJ.find(player => !player.isBot() || player.isCai())
        // if (isUserOrDealerWin) return this.showResult()
        // for (let i = this.getTotalPlayer() - 1; i > 0; --i) {
        //     this.players[i].showCardCounter();
        // }
        if (!this.owner) {
            this.owner = this.findPlayerHasFirstCard();
        }
        this.setCurrentTurn(this.owner, true);
        this.sort.node.active = true;
    };
    Game.prototype.findPlayerHasFirstCard = function () {
        // for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
        //     if (Helper.findCard(this.players[i].cards, 3, 1))
        //         return this.players[i];
        // }
        var userInRound = this.players.filter(function (player) { return player.isInRound() && !player.isCai() && player; });
        return userInRound[0];
    };
    Game.prototype.showDashboard = function (lead) {
        this.allowFold(!lead);
        if (this.commonCards.length() > 0) {
            var cards = Bot_1.default.suggest(this.commonCards, this.myself.cards);
            this.allowFire(!!cards);
        }
        else {
            this.allowFire(true);
        }
        this.fold.node.active = true;
        this.fire.node.active = true;
    };
    Game.prototype.setColor = function (btn, color) {
        btn.target.color = color;
        btn.target.children.forEach(function (child) {
            child.color = color;
        });
    };
    Game.prototype.hideDashboard = function (needShow) {
        if (needShow === void 0) { needShow = false; }
        var actionUser = cc.find('Canvas/actionUser');
        actionUser.active = needShow;
        if (needShow) {
            var showInsurr = this.players[0].cards[0].rank === 1;
            // showInsurr = true
            actionUser.children[4].active = showInsurr;
            var showSplit = !this.myself.isSplit && this.myself.cards[0].rank === this.myself.cards[1].rank;
            showSplit = true;
            if (!showSplit && showInsurr) {
                actionUser.children[4].x = 190.44;
            }
            actionUser.children[3].active = showSplit;
        }
    };
    Game.prototype.allowFold = function (allow) {
        this.fold.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fold, color);
    };
    Game.prototype.allowFire = function (allow) {
        // this.fire.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fire, color);
    };
    Game.prototype.onSortCardClicked = function () {
        util_1.default.playAudio(this.audioSortCard);
        this.myself.sortHandCards();
    };
    Game.prototype.onFireClicked = function () {
        this.onHit();
        // let cards = this.myself.prepareCards;
        // if (cards.count() == 0) { return console.log('empty'); }
        // if (this.commonCards.length() > 0 && !cards.gt(this.commonCards.peek())) {
        //     return console.log('invalid');
        // }
        // if (cards.isInvalid()) {
        //     return console.log('invalid');
        // }
        // this.hideDashboard();
        // this.onFire(this.myself, cards);
        // // resort hand card
        // this.myself.reorder();
    };
    Game.prototype.onFoldClicked = function () {
        // this.hideDashboard();
        // this.onFold(this.myself);
    };
    Game.prototype.deal = function () {
        var _this = this;
        Api_1.default.preloadRewardedVideo();
        if (Api_1.default.coin <= this.betValue) {
            this.popup.open(null, 3);
            Api_1.default.showInterstitialAd();
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
            return;
        }
        this.play.node.active = false;
        this.deck.node.active = true;
        this.deck.shuffle();
        this.commonCards.reset();
        for (var i = 1; i < this.players.length; i++) {
            var bot = this.players[i];
            if (bot.getCoin() <= this.betValue * 3) {
                var profile = this.getProfileBot();
                var rate = 0.5 + Math.random() * 0.5;
                bot.setCoin(Math.floor(this.myself.getCoin() * rate));
                bot.setAvatar(profile.avatar);
                bot.setUsername(profile.username);
            }
        }
        cc.find('Canvas/actionUser/insurr').x = 402.038;
        this.seats[6].hide();
        this.players.map(function (p) {
            p.reset();
            p.updatePoint('0');
            p.setBonusType(0);
        });
        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < this.getTotalPlayer(); j++) {
                var player = this.players[j];
                var card = this.deck.pick();
                player.push(card, 0.3 + (i * this.getTotalPlayer() + j) * Game_1.DEAL_SPEED);
            }
        }
        // this.deck.hide();
        this.node.runAction(cc.sequence(cc.delayTime(Game_1.DEAL_SPEED * this.totalPlayer * 2), cc.callFunc(this.showHandCards, this)));
        this.node.runAction(cc.sequence(cc.delayTime(0.4), cc.callFunc(function () { return util_1.default.playAudio(_this.audioDealCard); })));
        Api_1.default.preloadInterstitialAd();
    };
    Game.prototype.showEffect = function (player, effect) {
        effect.active = true;
        effect.setPosition(player.node.getPosition());
    };
    Game.prototype.onFold = function (player) {
        util_1.default.playAudio(this.audioFold);
        if (this.commonCards.hasCombat()) {
            var count = this.commonCards.getCombatLength();
            var victim = this.commonCards.getCombatVictim();
            var winner = this.commonCards.getCombatWinner();
            var bigPig = this.commonCards.getCombat();
            var spotWin = Math.pow(2, count - 1) * Helper_1.default.calculateSpotWin(bigPig.cards) * this.betValue;
            var coin = Math.min(spotWin, victim.coinVal);
            winner.addCoin(coin);
            victim.subCoin(coin);
        }
        if (this.commonCards.isCombatOpen()) {
            this.commonCards.resetCombat();
        }
        player.setInRound(false);
        player.setActive(false);
        var players = this.getInRoundPlayers();
        if (players.length >= 2) {
            return this.nextTurn();
        }
        this.nextRound(players[0]);
    };
    Game.prototype.onFire = function (player, cards, isDown) {
        if (isDown === void 0) { isDown = false; }
        var audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        this.node.runAction(cc.sequence(cc.delayTime(0.17), cc.callFunc(function () { return util_1.default.playAudio(audioClip); })));
        player.setActive(false);
        var cardCount = cards.count();
        var pos = this.commonCards.getPosition();
        cards.sort();
        for (var i = cards.count() - 1; i >= 0; --i) {
            if (isDown)
                cards.at(i).show();
            var card = cards.at(i);
            card.node.zIndex = this.commonCards.totalCards + i;
            var move = cc.moveTo(0.3, cc.v2(pos.x + (i - cardCount / 2) * Game_1.CARD_SPACE, pos.y));
            card.node.runAction(move);
            var scale = cc.scaleTo(0.3, 0.6);
            card.node.runAction(scale);
        }
        this.commonCards.push(cards);
        if (cards.highest.rank == 15) {
            this.commonCards.pushCombat(player, cards);
        }
        else if (this.commonCards.isCombatOpen()) {
            this.commonCards.pushCombat(player, cards);
        }
        player.removeCards(cards);
        if (player.isBot()) {
            player.updateCardCounter();
        }
        if (cards.highest.rank == 15) {
            this.notice.showBigPig(cards.count(), 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (3 * 2)) {
            this.notice.show(Notice_1.default.THREE_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.MultiPair && cards.count() == (4 * 2)) {
            this.notice.show(Notice_1.default.FOUR_PAIR, 3);
        }
        else if (cards.kind == CardGroup_1.default.House && cards.count() == 4) {
            this.notice.show(Notice_1.default.FOUR_CARD, 3);
        }
        if (player.cards.length > 0) {
            return this.nextTurn();
        }
        this.state = Game_1.LATE;
        this.sort.node.active = false;
        this.node.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(this.showResult, this, player)));
    };
    Game.prototype.onBigWin = function (sender, winner) {
        this.state = Game_1.LATE;
        this.owner = winner;
        this.bigWin.active = true;
        var total = 0;
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player != winner) {
                if (player.isBot()) {
                    player.hideCardCounter();
                    player.showHandCards();
                    player.reorder();
                }
                var lost = 13 * this.betValue;
                lost = Math.min(player.coinVal, lost);
                player.subCoin(lost);
                total += lost;
            }
        }
        winner.addCoin(total);
        Api_1.default.updateCoin(this.myself.coinVal);
        this.onWin(total);
    };
    // showResult(sender: cc.Node, winner: Player) {
    //     let players = this.players.filter((player) => (player != winner));
    //     players.sort((a, b) => { return a.cards.length - b.cards.length; });
    //     this.showEffect(winner, this.resultEffects[0]);
    //     let total = 0;
    //     let ranking = 1;
    //     if (winner == this.myself) ranking = 1;
    //     for (let i = 0; i < players.length; i++) {
    //         let player = players[i];
    //         if (player.isBot()) {
    //             player.hideCardCounter();
    //             player.showHandCards();
    //             player.reorder();
    //         }
    //         if (player == this.myself) ranking = i + 2;
    //         this.showEffect(player, this.resultEffects[i + 1]);
    //         let lost = Helper.calculate(player.cards) * this.betValue;
    //         lost = Math.min(player.coinVal, lost);
    //         player.subCoin(lost);
    //         total += lost;
    //     }
    //     winner.addCoin(total);
    //     Api.updateCoin(this.myself.coinVal);
    //     this.owner = winner;
    //     if (winner == this.myself) {
    //         this.onWin(total);
    //     } else {
    //         this.onLose(ranking);
    //     }
    // }
    Game.prototype.onLose = function (ranking) {
        Api_1.default.logEvent(EventKeys_1.default.LOSE + '_' + ranking);
        util_1.default.playAudio(this.audioLose);
        var rnd = Math.random();
        if (ranking == 2 && rnd <= 0.3) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 3 && rnd <= 0.2) {
            Api_1.default.showInterstitialAd();
        }
        else if (ranking == 4 && rnd <= 0.1) {
            Api_1.default.showInterstitialAd();
        }
        this.delayReset(3);
    };
    Game.prototype.onWin = function (won) {
        Api_1.default.logEvent(EventKeys_1.default.WIN);
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_OPEN);
        util_1.default.playAudio(this.audioWin);
        var rnd = Math.random();
        if (rnd <= 0.3 && Api_1.default.isInterstitialAdLoaded()) {
            Api_1.default.showInterstitialAd();
            this.delayReset(3);
        }
        else {
            this.spin.show(won);
        }
    };
    Game.prototype.onSpinHide = function () {
        this.delayReset(3);
    };
    Game.prototype.onSpinCompleted = function (result, won) {
        var bonus = this.spin.compute(result.id, won);
        if (bonus > 0) {
            this.myself.addCoin(bonus);
            Api_1.default.updateCoin(this.myself.coinVal);
        }
    };
    Game.prototype.delayReset = function (dt) {
        this.node.runAction(cc.sequence(cc.delayTime(dt), cc.callFunc(this.reset, this)));
    };
    Game.prototype.reset = function () {
        for (var i = this.resultEffects.length - 1; i >= 0; --i) {
            this.resultEffects[i].active = false;
        }
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].reset();
        }
        if (this.bigWin.active) {
            this.bigWin.active = false;
        }
        // this.deck.node.active = false;
        this.play.node.active = true;
    };
    Game.prototype.onCardSelected = function (card, selected) {
    };
    Game.prototype.nextTurn = function () {
        var next = this.getNextInRoundPlayer();
        this.setCurrentTurn(next, false);
    };
    Game.prototype.nextRound = function (lead) {
        this.commonCards.nextRound();
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].setInRound(true);
        }
        this.setCurrentTurn(lead, true);
        // util.playAudio(this.audioGarbageCard);
    };
    Game.prototype.onHitClicked = function () {
        var player = this.myself;
        this.takeCard(player);
        player.setActive(true);
        this.hideDashboard();
    };
    Game.prototype.onDoubleClicked = function () {
    };
    Game.prototype.onSplitClicked = function () {
    };
    Game.prototype.onStandClicked = function () {
        var player = this.myself;
        player.setActive(false);
        if (!player.inRound && player.subUser) {
            player = player.subUser;
        }
        player.setInRound(false);
        this.hideDashboard();
        this.nextTurn();
    };
    Game.prototype.setCurrentTurn = function (player, lead) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var canHit;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.players.map(function (p) { return p.setActive(false); });
                        if (!player) {
                            return [2 /*return*/, this.showResult()];
                        }
                        this.turn = player.seat;
                        player.setActive(true);
                        if (player.isUser()) {
                            return [2 /*return*/, Game_1.sleep(Game_1.WAIT_RE_SHOW_USER_ACTION).then(function () {
                                    _this.hideDashboard(true);
                                })];
                        }
                        canHit = player.point > this.players[0].point;
                        if (player.point < 18) {
                            canHit = true;
                        }
                        if (!canHit) {
                            canHit = this.players.find(function (p) { return player.point > p.point; }) != undefined;
                        }
                        if (player.point >= 18)
                            canHit = false;
                        if (((_a = player.cards) === null || _a === void 0 ? void 0 : _a.length) === 5)
                            canHit = false;
                        return [4 /*yield*/, Game_1.sleep(Game_1.WAIT_BOT)];
                    case 1:
                        _b.sent();
                        player.setInRound(false);
                        // setTimeout(() => {
                        if (canHit) {
                            this.takeCard(player);
                        }
                        else {
                            this.toast.show(player.username.string + ' stand' + player.point + " điểm");
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.getInRoundPlayers = function () {
        var players = [];
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) {
            if (this.players[i].isInRound())
                players.push(this.players[i]);
        }
        return players;
    };
    Game.prototype.getNextInRoundPlayer = function () {
        var _this = this;
        // tim thang nao co ghe ngoi > turn trc va dang o trong van choi
        var nextTurn = this.players.find(function (player) {
            var _a;
            return (player.seat === _this.turn && ((_a = player === null || player === void 0 ? void 0 : player.subUser) === null || _a === void 0 ? void 0 : _a.inRound)) || (player.seat > _this.turn && player.isInRound());
        });
        if (!nextTurn && this.players[0].inRound)
            return this.players[0]; // neu khong con ai thi chuyen luot cho nha cai
        // let nextTurn = this.players[this.turn + 1]
        // if (!nextTurn && this.players[0].isInRound()) nextTurn = this.players[0]
        // if (!nextTurn.isInRound()) return
        return nextTurn;
        // for (let i = 1; i < this.getTotalPlayer(); i++) {
        //     let offset = this.turn + i;
        //     if (offset >= this.getTotalPlayer()) offset -= this.getTotalPlayer();
        //     if (this.players[offset].isInRound())
        //         return this.players[offset];
        // }
        // return null;
    };
    Game.prototype.takeCard = function (player, needNext) {
        if (needNext === void 0) { needNext = true; }
        return __awaiter(this, void 0, void 0, function () {
            var card, _player;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = this.deck.pick();
                        this.toast.show(player.username.string + ': ' + player.point + "đ bốc thêm" + card.rank);
                        if (!(card === null || card === void 0 ? void 0 : card.node)) {
                            this.state = Game_1.LATE;
                            this.sort.node.active = false;
                            // this.node.runAction(cc.sequence(
                            //     cc.delayTime(2),
                            //     cc.callFunc(this.showResult, this, player)
                            // ));
                            return [2 /*return*/];
                        }
                        player.push(card, 0);
                        if (!needNext)
                            return [2 /*return*/];
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        return [4 /*yield*/, Game_1.sleep(0)];
                    case 1:
                        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
                        // this.node.runAction(cc.sequence(
                        //     cc.delayTime(0.17),
                        //     cc.callFunc(() => util.playAudio(audioClip))
                        // ));
                        _a.sent();
                        if (player.isBot()) {
                            // player.updateCardCounter();
                        }
                        else {
                            player.cards.map(function (el) { return el.show(); });
                        }
                        player.setActive(false);
                        _player = player;
                        if (!player.inRound && player.subUser) {
                            _player = player.subUser;
                        }
                        if (_player.point >= 21 || _player.cards.length === 5) {
                            _player.setInRound(false);
                            _player.point > 21 && this.toast.show(_player.username.string + (" Thua: " + _player.point + " \u0111i\u1EC3m"));
                            // bot stand
                            _player.setActive(false);
                            return [2 /*return*/, this.nextTurn()];
                        }
                        // console.log('====================================');
                        // console.log(player.cards.map(el => el.node.name));
                        // console.log('====================================');
                        // this.checkWin()
                        // this.nextTurn()
                        if (!player.bonusType) {
                            this.setCurrentTurn(player, false);
                        }
                        else {
                            player.setInRound(false);
                            this.nextTurn();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Game.prototype.execBot = function (sender, bot) {
        // alert(1)
        var cards = this.commonCards.isEmpty()
            ? Bot_1.default.random(bot.cards)
            : Bot_1.default.suggest(this.commonCards, bot.cards);
        if (!cards) {
            return this.onFold(bot);
        }
        this.onFire(this.players[this.turn], cards, true);
    };
    Game.prototype.getTotalPlayer = function () {
        return this.players.length;
    };
    Game.prototype.getProfileBot = function () {
        for (var i = 0; i < 10; i++) {
            var rnd = Math.floor(Math.random() * this.profileBots.length);
            if (!this.isUsage(this.profileBots[rnd])) {
                return this.profileBots[rnd];
            }
        }
        return this.profileBots[0];
    };
    Game.prototype.isUsage = function (user) {
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].username.string == user.username) {
                return true;
            }
        }
        return false;
    };
    Game.prototype.onSoundToggle = function (sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
    };
    Game.prototype.claimBankruptcyMoney = function (bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get('MONEY1'), util_1.default.numberFormat(bonus));
        this.toast.show(msg);
        Api_1.default.coinIncrement(bonus);
        this.myself.setCoin(Api_1.default.coin);
        this.popup.close(null, 3);
        // update bet room
        this.betValue = Math.round(Api_1.default.coin * 0.3);
        this.betText.string = util_1.default.numberFormat(this.betValue);
    };
    Game.prototype.inviteFirend = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.toast.show('Mời bạn chơi không thành công');
        });
    };
    Game.prototype.adReward = function () {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function () {
            _this.popup.close(null, 3);
            _this.claimBankruptcyMoney(Api_1.default.randomBonus());
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
    };
    Game.prototype.onSetBonusClicked = function (e, bonus) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('====================================');
                        console.log(bonus);
                        console.log('====================================');
                        if (!(bonus == 3)) return [3 /*break*/, 4];
                        this.myself.split(this.seats[6]);
                        this.seats[6].show();
                        this.hideDashboard();
                        return [4 /*yield*/, Game_1.sleep(Game_1.DEAL_SPEED)
                            // this.setCurrentTurn(this.myself, false)
                            // this.onHitClicked()
                        ];
                    case 1:
                        _a.sent();
                        // this.setCurrentTurn(this.myself, false)
                        // this.onHitClicked()
                        this.takeCard(this.myself, false);
                        return [4 /*yield*/, Game_1.sleep(500)];
                    case 2:
                        _a.sent();
                        this.takeCard(this.myself.subUser, false);
                        return [4 /*yield*/, Game_1.sleep(1000)];
                    case 3:
                        _a.sent();
                        this.nextTurn();
                        return [3 /*break*/, 5];
                    case 4:
                        this.myself.setBonusType(bonus);
                        if (bonus == 1) { // double
                            this.onHitClicked();
                            if (this.myself.inRound) {
                                this.myself.inRound = false;
                            }
                            else {
                                this.myself.subUser.inRound = false;
                            }
                        }
                        else {
                            this.onStandClicked();
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    // onSetBonus = (bonus) => {
    //     console.log('====================================');
    //     console.log(bonus);
    //     console.log('====================================');
    //     // this.myself.setBonusType()
    // }
    // cheat
    Game.prototype.cheat_win = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.myself);
    };
    Game.prototype.cheat_lose = function () {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.players[1]);
    };
    var Game_1;
    Game.sleep = function (wait) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve(1);
                    }, wait);
                })];
        });
    }); };
    Game.WAIT = 0; // wait for player
    Game.DEAL = 1; // deal card
    Game.PLAY = 2; // play
    Game.LATE = 3; // late
    // static readonly DEAL_SPEED: number = 0//1.66;
    // static readonly CARD_SPACE: number = 45;
    // static readonly WAIT_BOT: number = 0;
    // static readonly WAIT_RE_SHOW_USER_ACTION: number = 0;
    Game.DEAL_SPEED = 1.2; //1.66;
    Game.CARD_SPACE = 45;
    Game.WAIT_BOT = 2000;
    Game.WAIT_RE_SHOW_USER_ACTION = 1000;
    __decorate([
        property(Deck_1.default)
    ], Game.prototype, "deck", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "play", void 0);
    __decorate([
        property(Player_1.default)
    ], Game.prototype, "seats", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fire", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "fold", void 0);
    __decorate([
        property(cc.Button)
    ], Game.prototype, "sort", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "resultEffects", void 0);
    __decorate([
        property(cc.Node)
    ], Game.prototype, "bigWin", void 0);
    __decorate([
        property(Toast_1.default)
    ], Game.prototype, "toast", void 0);
    __decorate([
        property(Notice_1.default)
    ], Game.prototype, "notice", void 0);
    __decorate([
        property(cc.Label)
    ], Game.prototype, "betText", void 0);
    __decorate([
        property(SpinWheel_1.default)
    ], Game.prototype, "spin", void 0);
    __decorate([
        property(Popup_1.default)
    ], Game.prototype, "popup", void 0);
    __decorate([
        property(Profile)
    ], Game.prototype, "profileBots", void 0);
    __decorate([
        property(cc.Toggle)
    ], Game.prototype, "soundToggle", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFold", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioShowCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioGarbageCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFire", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioSortCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioQuitGame", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioDealCard", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], Game.prototype, "audioFireSingle", void 0);
    __decorate([
        property(Modal_1.default)
    ], Game.prototype, "modal", void 0);
    Game = Game_1 = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0dhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBQzlCLCtCQUEwQjtBQUUxQixtQ0FBOEI7QUFDOUIseUNBQW9DO0FBQ3BDLDJDQUFzQztBQUN0QyxpQ0FBNEI7QUFDNUIsNkJBQXdCO0FBQ3hCLG1DQUE4QjtBQUM5QixtQ0FBOEI7QUFDOUIsK0JBQTBCO0FBQzFCLHlDQUFvQztBQUNwQyxpQ0FBNEI7QUFDNUIsNkJBQXdCO0FBQ3hCLHVDQUFrQztBQUNsQyx5Q0FBb0M7QUFDcEMsdUNBQWtDO0FBRWxDLElBQUksVUFBVSxHQUFHO0lBQ2IsQ0FBQyxFQUFFLElBQUk7SUFDUCxDQUFDLEVBQUUsSUFBSTtJQUNQLENBQUMsRUFBRSxPQUFPO0lBQ1YsQ0FBQyxFQUFFLE1BQU07Q0FDWixDQUFBO0FBRUQsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFFN0YsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBQTtRQUVJLFdBQU0sR0FBbUIsSUFBSSxDQUFDO1FBRTlCLGFBQVEsR0FBVyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUhHO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7MkNBQ0s7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2Q0FDRTtJQUpwQixPQUFPO1FBRFosT0FBTyxDQUFDLFNBQVMsQ0FBQztPQUNiLE9BQU8sQ0FLWjtJQUFELGNBQUM7Q0FMRCxBQUtDLElBQUE7QUFHRDtJQUFrQyx3QkFBWTtJQUE5QztRQUFBLHFFQTRwQ0M7UUExcENHLFVBQUksR0FBUyxJQUFJLENBQUM7UUFFbEIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQWEsRUFBRSxDQUFDO1FBRXJCLFVBQUksR0FBYyxJQUFJLENBQUM7UUFFdkIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQWMsSUFBSSxDQUFDO1FBRXZCLG1CQUFhLEdBQWMsRUFBRSxDQUFDO1FBRTlCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFFdkIsV0FBSyxHQUFVLElBQUksQ0FBQztRQUVwQixZQUFNLEdBQVcsSUFBSSxDQUFDO1FBRXRCLGFBQU8sR0FBYSxJQUFJLENBQUM7UUFFekIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGlCQUFXLEdBQWMsRUFBRSxDQUFDO1FBRzVCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxzQkFBZ0IsR0FBaUIsSUFBSSxDQUFDO1FBRXRDLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGNBQVEsR0FBaUIsSUFBSSxDQUFDO1FBRTlCLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxtQkFBYSxHQUFpQixJQUFJLENBQUM7UUFFbkMsbUJBQWEsR0FBaUIsSUFBSSxDQUFDO1FBRW5DLHFCQUFlLEdBQWlCLElBQUksQ0FBQztRQUVyQyxXQUFLLEdBQVUsSUFBSSxDQUFDO1FBRXBCLGFBQU8sR0FBYSxFQUFFLENBQUM7UUFDdkIsaUJBQVcsR0FBZSxJQUFJLG9CQUFVLEVBQUUsQ0FBQztRQUMzQyxXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFDakIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixXQUFLLEdBQVcsSUFBSSxDQUFDO1FBQ3JCLFlBQU0sR0FBVyxJQUFJLENBQUM7UUFDdEIsY0FBUSxHQUFXLElBQUksQ0FBQztRQUN4QixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUN4QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFzSnJCLGFBQU8sR0FBRyxVQUFDLElBQVk7WUFDbkIsSUFBSSxPQUFPLEdBQUcsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25DLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQTtRQTRFRCx1QkFBaUIsR0FBRztZQUNoQixLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU07Z0JBQ25CLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQTtZQUN4QixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQTtRQThjRCxnQkFBVSxHQUFHO1lBQ1Qsb0RBQW9EO1lBQ3BELHdDQUF3QztZQUV4Qyw2RUFBNkU7WUFFN0UsMEJBQTBCO1lBQzFCLHFDQUFxQztZQUNyQywrRUFBK0U7WUFDL0UsUUFBUTtZQUNSLEtBQUs7WUFHTCxJQUFJLE1BQU0sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRTVCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQTtZQUVyQixLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFDLE1BQU0sRUFBRSxLQUFLO2dCQUMzQixJQUFJLENBQUMsS0FBSztvQkFBRSxPQUFNO2dCQUVsQixJQUFJLFVBQVUsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3QyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ2hCLFVBQVUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtpQkFDckQ7Z0JBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFFN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Z0JBRXBELGFBQWEsSUFBSSxVQUFVLENBQUE7WUFDL0IsQ0FBQyxDQUFDLENBQUE7WUFFRixNQUFNLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBRWhDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsQ0FBQztZQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFFcEQsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7WUFDeEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUluQixLQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7WUFDdkIsdUJBQXVCO1lBQ3ZCLDZCQUE2QjtZQUM3QixpQkFBaUI7WUFDakIsa0RBQWtEO1lBQ2xELG9DQUFvQztZQUNwQyw4QkFBOEI7WUFDOUIsZ0NBQWdDO1lBQ2hDLHdDQUF3QztZQUN4QyxzQ0FBc0M7WUFDdEMsZ0NBQWdDO1lBQ2hDLFlBQVk7WUFDWix5Q0FBeUM7WUFDekMsaURBQWlEO1lBQ2pELGdDQUFnQztZQUNoQyx5QkFBeUI7WUFDekIsUUFBUTtZQUNSLElBQUk7WUFDSix5QkFBeUI7WUFDekIsYUFBRyxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLHFCQUFxQjtZQUVyQiwwRUFBMEU7UUFDOUUsQ0FBQyxDQUFBO1FBR0QscUJBQWUsR0FBRyxVQUFDLE1BQWM7WUFDN0IsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUM1QixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUE7WUFDbEIsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLENBQUMsRUFBRTtnQkFDdkIsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFBO2FBQ2xDO2lCQUFNLElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEVBQUU7Z0JBQzFCLFVBQVUsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFBO2dCQUMzQixnQ0FBZ0M7YUFDbkM7aUJBQU0sSUFBSSxNQUFNLENBQUMsY0FBYyxFQUFFLEVBQUU7Z0JBQ2hDLFVBQVUsSUFBSSxLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQTthQUNwQztpQkFBTSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDcEMsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7aUJBQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ3RDLE9BQU8sQ0FBQyxDQUFBO2FBQ1g7aUJBQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsRUFBRTtnQkFDMUIsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7aUJBQ0k7Z0JBQ0QsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUE7YUFDOUI7WUFFRCxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFO2dCQUN2QyxVQUFVLElBQUksQ0FBQyxDQUFBO2FBQ2xCO1lBQ0QsT0FBTyxVQUFVLENBQUE7UUFDckIsQ0FBQyxDQUFBO1FBb0tELGNBQVEsR0FBRztZQUNQLHVEQUF1RDtZQUN2RCw2QkFBNkI7WUFDN0IsdURBQXVEO1lBQ3ZELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUN0QixnQkFBZ0I7WUFDcEIsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUE7O0lBcUpMLENBQUM7YUE1cENvQixJQUFJO0lBb0VyQix3QkFBd0I7SUFFeEIscUJBQU0sR0FBTjtRQUFBLGlCQThEQztRQTdERyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRSxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFFcEMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBQyxHQUFHLEVBQUUsS0FBSztZQUNsRCxJQUFJLGlCQUFpQixHQUFHLElBQUksT0FBTyxDQUFDLFVBQUEsT0FBTztnQkFDdkMsSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEVBQUUsRUFBRSxJQUFJO29CQUNoQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQUssRUFBRSxPQUFPO3dCQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSzs0QkFBRSxPQUFNO3dCQUMzQixJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFBO3dCQUNoQixJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFBO3dCQUdsQyxRQUFRLENBQUMsRUFBRTs0QkFDUCxLQUFLLENBQUM7Z0NBQ0YsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtnQ0FDUCxNQUFNOzRCQUNWLEtBQUssRUFBRTtnQ0FDSCxDQUFDLEdBQUcsR0FBRyxDQUFBO2dDQUNQLE1BQU07NEJBQ1YsS0FBSyxFQUFFO2dDQUNILENBQUMsR0FBRyxHQUFHLENBQUE7Z0NBQ1AsTUFBTTs0QkFDVixLQUFLLEVBQUU7Z0NBQ0gsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtnQ0FDUCxNQUFNO3lCQUNiO3dCQUVELElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTt3QkFDdEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFBO3dCQUNyQixJQUFJLE9BQU8sR0FBRyxDQUFDOzRCQUFFLGFBQWEsSUFBSSxJQUFJLENBQUE7d0JBRXRDLHVEQUF1RDt3QkFDdkQsK0NBQStDO3dCQUMvQyx1REFBdUQ7d0JBQ3ZELE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQTt3QkFDeEIsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDdkQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUM5RixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQTt3QkFDckUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUE7d0JBQ3JFLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFBO3dCQUM1RCxPQUFPLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQTt3QkFDdEIsdURBQXVEO3dCQUN2RCx3QkFBd0I7d0JBQ3hCLHVEQUF1RDt3QkFDdkQsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzVCLElBQUksSUFBSSxLQUFLLEVBQUUsSUFBSSxLQUFLLEtBQUssR0FBRyxFQUFFOzRCQUM5QixVQUFVLENBQUM7Z0NBQ1AsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBOzRCQUNkLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzt5QkFDWDtvQkFDTCxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDLENBQUMsQ0FBQTtZQUNOLENBQUMsQ0FBQyxDQUFBO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFO2dCQUNwQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7WUFDbkIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Z0JBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUN4RCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFlBQVk7SUFFWix1QkFBUSxHQUFSO1FBQUEsaUJBOEVDO1FBN0VHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRW5CLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTVELElBQUksZ0JBQU0sQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztTQUN4QjtRQUVELElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQTtRQUVmLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDO1lBQUUsR0FBRyxHQUFHLFNBQVMsQ0FBQzthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDO1lBQUUsR0FBRyxHQUFHLGFBQWEsQ0FBQTtRQUNqRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDaEMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtpQkFDakI7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ2Y7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUVGLCtDQUErQztRQUMvQyxnQ0FBZ0M7UUFDaEMscUJBQXFCO1FBQ3JCLGtDQUFrQztRQUNsQyw4Q0FBOEM7UUFDOUMsdUJBQXVCO1FBQ3ZCLDBDQUEwQztRQUMxQyw4Q0FBOEM7UUFDOUMsbUNBQW1DO1FBQ25DLGVBQWU7UUFDZix1QkFBdUI7UUFDdkIsUUFBUTtRQUNSLElBQUk7UUFJSixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxhQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRSxJQUFJLGdCQUFNLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxnQkFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7U0FDL0Q7UUFFRCxJQUFJLGdCQUFNLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixHQUFHLENBQUMsT0FBTyxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDdkQsR0FBRyxDQUFDLFdBQVcsQ0FBQyxnQkFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMzQzthQUFNO1lBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO2dCQUNuQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELENBQUMsQ0FBQyxDQUFBO1NBQ0w7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUMzQixFQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUNqQixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQy9CLENBQUMsQ0FBQztRQUVILElBQUksZ0JBQU0sQ0FBQyxXQUFXLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUM5QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUM1QjtRQUVELGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFVRCx3QkFBUyxHQUFUO1FBQ0ksc0JBQXNCO1FBQ3RCLGdEQUFnRDtRQUNoRCxzRUFBc0U7SUFDMUUsQ0FBQztJQUVELGVBQWU7SUFDZiw0REFBNEQ7SUFDNUQscURBQXFEO0lBQ3JELG1FQUFtRTtJQUNuRSxxREFBcUQ7SUFDckQsbUVBQW1FO0lBQ25FLHFEQUFxRDtJQUNyRCxxRUFBcUU7SUFDckUsc0RBQXNEO0lBQ3RELHFFQUFxRTtJQUNyRSxzREFBc0Q7SUFDdEQscUVBQXFFO0lBQ3JFLHNEQUFzRDtJQUN0RCxRQUFRO0lBQ1IsMkJBQTJCO0lBQzNCLElBQUk7SUFFSiwyQkFBWSxHQUFaLFVBQWEsR0FBYTtRQUN0QixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksTUFBSSxDQUFDLElBQUk7WUFBRSxPQUFPO1FBRXBDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBRXBELElBQUksQ0FBQyxRQUFRO1lBQUUsT0FBTztRQUV0QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztRQUU1QyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDeEM7YUFBTTtZQUNILElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQy9CLE9BQU87U0FDVjtRQUVELElBQUksWUFBWSxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLFNBQVMsRUFBRTtZQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RCLE9BQU87U0FDVjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixPQUFPO1NBQ1Y7UUFFRCxJQUFJLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFO1lBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQsOEJBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUNyQix3QkFBd0I7UUFDeEIsd0NBQXdDO1FBQ3hDLHlEQUF5RDtRQUN6RCwwRUFBMEU7UUFDMUUsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCx1Q0FBdUM7UUFDdkMsSUFBSTtJQUNSLENBQUM7SUFRRCw0QkFBYSxHQUFiO1FBQ0ksYUFBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksTUFBSSxDQUFDLElBQUksRUFBRTtZQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pDO2FBQU07WUFDSCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLE1BQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3BELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUN0RCxhQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hCO1FBQ0QsY0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbkMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELDRCQUFhLEdBQWI7UUFDSSxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuQywrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBRXZCLHVEQUF1RDtRQUN2RCxrQ0FBa0M7UUFDbEMsdURBQXVEO1FBSXZELDRDQUE0QztRQUU1Qyx1Q0FBdUM7UUFDdkMsNkJBQTZCO1FBQzdCLHdEQUF3RDtRQUN4RCxVQUFVO1FBQ1YsY0FBYztRQUNkLElBQUk7UUFFSixtQ0FBbUM7UUFDbkMseUJBQXlCO1FBQ3pCLDBEQUEwRDtRQUMxRCxNQUFNO1FBRU4sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQyxNQUFNLEVBQUUsS0FBSztZQUMzQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQUUsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFBO2lCQUFNO2dCQUM5QyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUE7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUdGLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTTtZQUNwQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUE7UUFDM0IsQ0FBQyxDQUFDLENBQUE7UUFFRiwwREFBMEQ7UUFDMUQsb0ZBQW9GO1FBQ3BGLGtEQUFrRDtRQUNsRCx3REFBd0Q7UUFDeEQseUNBQXlDO1FBQ3pDLElBQUk7UUFFSixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBRUQscUNBQXNCLEdBQXRCO1FBQ0kseURBQXlEO1FBQ3pELHdEQUF3RDtRQUN4RCxrQ0FBa0M7UUFDbEMsSUFBSTtRQUNKLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLE1BQU0sRUFBL0MsQ0FBK0MsQ0FBQyxDQUFBO1FBQ2hHLE9BQU8sV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCw0QkFBYSxHQUFiLFVBQWMsSUFBYTtRQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsRUFBRTtZQUMvQixJQUFJLEtBQUssR0FBRyxhQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBRUQsdUJBQVEsR0FBUixVQUFTLEdBQWMsRUFBRSxLQUFlO1FBQ3BDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQzdCLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRCQUFhLEdBQWIsVUFBYyxRQUFnQjtRQUFoQix5QkFBQSxFQUFBLGdCQUFnQjtRQUMxQixJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFFN0MsVUFBVSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7UUFDNUIsSUFBSSxRQUFRLEVBQUU7WUFDVixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFBO1lBQ3BELG9CQUFvQjtZQUNwQixVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUE7WUFFMUMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO1lBRS9GLFNBQVMsR0FBRyxJQUFJLENBQUE7WUFDaEIsSUFBSSxDQUFDLFNBQVMsSUFBSSxVQUFVLEVBQUU7Z0JBQzFCLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQTthQUNwQztZQUNELFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQTtTQUM1QztJQUNMLENBQUM7SUFFRCx3QkFBUyxHQUFULFVBQVUsS0FBYztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCx3QkFBUyxHQUFULFVBQVUsS0FBYztRQUNwQixrQ0FBa0M7UUFDbEMsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1FBQ1osd0NBQXdDO1FBQ3hDLDJEQUEyRDtRQUUzRCw2RUFBNkU7UUFDN0UscUNBQXFDO1FBQ3JDLElBQUk7UUFFSiwyQkFBMkI7UUFDM0IscUNBQXFDO1FBQ3JDLElBQUk7UUFFSix3QkFBd0I7UUFDeEIsbUNBQW1DO1FBQ25DLHNCQUFzQjtRQUN0Qix5QkFBeUI7SUFDN0IsQ0FBQztJQUdELDRCQUFhLEdBQWI7UUFDSSx3QkFBd0I7UUFDeEIsNEJBQTRCO0lBQ2hDLENBQUM7SUFFRCxtQkFBSSxHQUFKO1FBQUEsaUJBbURDO1FBbERHLGFBQUcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzNCLElBQUksYUFBRyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN6QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUN6QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUNqRCxPQUFNO1NBQ1Q7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ25DLElBQUksSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO2dCQUNyQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDOUIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDckM7U0FDSjtRQUVELEVBQUUsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFBO1FBRS9DLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7UUFFcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDO1lBQ2QsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ1YsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3JCLENBQUMsQ0FBQyxDQUFBO1FBRUYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM5RTtTQUNKO1FBQ0Qsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxFQUNwRCxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FDekMsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQ2pCLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBTSxPQUFBLGNBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFsQyxDQUFrQyxDQUFDLENBQ3hELENBQUMsQ0FBQztRQUVILGFBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCx5QkFBVSxHQUFWLFVBQVcsTUFBYyxFQUFFLE1BQWU7UUFDdEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELHFCQUFNLEdBQU4sVUFBTyxNQUFjO1FBQ2pCLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUM5QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQy9DLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDaEQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNoRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzFDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUMsR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzdGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3QyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7UUFDRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLEVBQUU7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNsQztRQUVELE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN2QyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQscUJBQU0sR0FBTixVQUFPLE1BQWMsRUFBRSxLQUFnQixFQUFFLE1BQXVCO1FBQXZCLHVCQUFBLEVBQUEsY0FBdUI7UUFDNUQsSUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDeEcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFDbEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxjQUFNLE9BQUEsY0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUMvQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM5QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXpDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUViLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3pDLElBQUksTUFBTTtnQkFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBRS9CLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQ25ELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsTUFBSSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztTQUM5QzthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDOUM7UUFDRCxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzlCO1FBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzVDO2FBQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLG1CQUFTLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUN0RSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMxQzthQUFNLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxtQkFBUyxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDdEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDekM7YUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksbUJBQVMsQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtZQUM1RCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN6QztRQUNELElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzFCO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFDZixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUM3QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUJBQVEsR0FBUixVQUFTLE1BQWUsRUFBRSxNQUFjO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO2dCQUNsQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDO29CQUN6QixNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDcEI7Z0JBQ0QsSUFBSSxJQUFJLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzlCLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JCLEtBQUssSUFBSSxJQUFJLENBQUM7YUFDakI7U0FDSjtRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEIsYUFBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELGdEQUFnRDtJQUNoRCx5RUFBeUU7SUFDekUsMkVBQTJFO0lBRTNFLHNEQUFzRDtJQUN0RCxxQkFBcUI7SUFDckIsdUJBQXVCO0lBQ3ZCLDhDQUE4QztJQUM5QyxpREFBaUQ7SUFDakQsbUNBQW1DO0lBQ25DLGdDQUFnQztJQUNoQyx3Q0FBd0M7SUFDeEMsc0NBQXNDO0lBQ3RDLGdDQUFnQztJQUNoQyxZQUFZO0lBQ1osc0RBQXNEO0lBQ3RELDhEQUE4RDtJQUM5RCxxRUFBcUU7SUFDckUsaURBQWlEO0lBQ2pELGdDQUFnQztJQUNoQyx5QkFBeUI7SUFDekIsUUFBUTtJQUNSLDZCQUE2QjtJQUM3QiwyQ0FBMkM7SUFDM0MsMkJBQTJCO0lBQzNCLG1DQUFtQztJQUNuQyw2QkFBNkI7SUFDN0IsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxRQUFRO0lBQ1IsSUFBSTtJQUVKLHFCQUFNLEdBQU4sVUFBTyxPQUFlO1FBQ2xCLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBQzdDLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMxQixJQUFJLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsRUFBRTtZQUM1QixhQUFHLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM1QjthQUFNLElBQUksT0FBTyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ25DLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzVCO2FBQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7WUFDbkMsYUFBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxvQkFBSyxHQUFMLFVBQU0sR0FBVztRQUNiLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNqRCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QixJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUIsSUFBSSxHQUFHLElBQUksR0FBRyxJQUFJLGFBQUcsQ0FBQyxzQkFBc0IsRUFBRSxFQUFFO1lBQzVDLGFBQUcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEI7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQztJQUVELHlCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCw4QkFBZSxHQUFmLFVBQWdCLE1BQU0sRUFBRSxHQUFXO1FBQy9CLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0IsYUFBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVELHlCQUFVLEdBQVYsVUFBVyxFQUFVO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQzNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2hCLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FDaEMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFLLEdBQUw7UUFDSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUN4QztRQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDM0I7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUM5QjtRQUNELGlDQUFpQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFRCw2QkFBYyxHQUFkLFVBQWUsSUFBVSxFQUFFLFFBQWlCO0lBRTVDLENBQUM7SUFFRCx1QkFBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELHdCQUFTLEdBQVQsVUFBVSxJQUFZO1FBQ2xCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDN0IsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNoQyx5Q0FBeUM7SUFDN0MsQ0FBQztJQUVELDJCQUFZLEdBQVo7UUFDSSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDckIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7SUFDeEIsQ0FBQztJQUVELDhCQUFlLEdBQWY7SUFFQSxDQUFDO0lBRUQsNkJBQWMsR0FBZDtJQUVBLENBQUM7SUFFRCw2QkFBYyxHQUFkO1FBQ0ksSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUN4QixNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXhCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDbkMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7U0FDMUI7UUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtRQUNwQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQW9HSyw2QkFBYyxHQUFwQixVQUFxQixNQUFjLEVBQUUsSUFBYTs7Ozs7Ozs7d0JBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFBO3dCQUN6QyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNULHNCQUFPLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQTt5QkFDM0I7d0JBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUN4QixNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUV2QixJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRTs0QkFDakIsc0JBQU8sTUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxJQUFJLENBQUM7b0NBQ2xELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUE7Z0NBQzVCLENBQUMsQ0FBQyxFQUFBO3lCQUNMO3dCQUVHLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO3dCQUVqRCxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxFQUFFOzRCQUNuQixNQUFNLEdBQUcsSUFBSSxDQUFBO3lCQUNoQjt3QkFFRCxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNULE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBdEIsQ0FBc0IsQ0FBQyxJQUFJLFNBQVMsQ0FBQTt5QkFDdkU7d0JBRUQsSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7NEJBQUUsTUFBTSxHQUFHLEtBQUssQ0FBQTt3QkFFdEMsSUFBSSxPQUFBLE1BQU0sQ0FBQyxLQUFLLDBDQUFFLE1BQU0sTUFBSyxDQUFDOzRCQUFFLE1BQU0sR0FBRyxLQUFLLENBQUE7d0JBRTlDLHFCQUFNLE1BQUksQ0FBQyxLQUFLLENBQUMsTUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQzt3QkFDaEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTt3QkFDeEIscUJBQXFCO3dCQUNyQixJQUFJLE1BQU0sRUFBRTs0QkFDUixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3lCQUN4Qjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsQ0FBQTs0QkFDM0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO3lCQUNsQjs7Ozs7S0F3Qko7SUFFRCxnQ0FBaUIsR0FBakI7UUFDSSxJQUFJLE9BQU8sR0FBa0IsRUFBRSxDQUFDO1FBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELG1DQUFvQixHQUFwQjtRQUFBLGlCQW1CQztRQWxCRyxnRUFBZ0U7UUFDaEUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNOztZQUNuQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsSUFBSSxXQUFJLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxPQUFPLDBDQUFFLE9BQU8sQ0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUE7UUFDckgsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUFFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLCtDQUErQztRQUVoSCw2Q0FBNkM7UUFDN0MsMkVBQTJFO1FBQzNFLG9DQUFvQztRQUNwQyxPQUFPLFFBQVEsQ0FBQTtRQUNmLG9EQUFvRDtRQUNwRCxrQ0FBa0M7UUFDbEMsNEVBQTRFO1FBQzVFLDRDQUE0QztRQUM1Qyx1Q0FBdUM7UUFDdkMsSUFBSTtRQUNKLGVBQWU7SUFDbkIsQ0FBQztJQU1LLHVCQUFRLEdBQWQsVUFBZSxNQUFjLEVBQUUsUUFBZTtRQUFmLHlCQUFBLEVBQUEsZUFBZTs7Ozs7O3dCQUN0QyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFFNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTt3QkFDeEYsSUFBSSxFQUFDLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxJQUFJLENBQUEsRUFBRTs0QkFDYixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7NEJBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7NEJBQzlCLG1DQUFtQzs0QkFDbkMsdUJBQXVCOzRCQUN2QixpREFBaUQ7NEJBQ2pELE1BQU07NEJBQ04sc0JBQU07eUJBQ1Q7d0JBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBRXJCLElBQUksQ0FBQyxRQUFROzRCQUFFLHNCQUFNO3dCQUNyQiwyR0FBMkc7d0JBQzNHLG1DQUFtQzt3QkFDbkMsMEJBQTBCO3dCQUMxQixtREFBbUQ7d0JBQ25ELE1BQU07d0JBRU4scUJBQU0sTUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQTs7d0JBTm5CLDJHQUEyRzt3QkFDM0csbUNBQW1DO3dCQUNuQywwQkFBMEI7d0JBQzFCLG1EQUFtRDt3QkFDbkQsTUFBTTt3QkFFTixTQUFtQixDQUFBO3dCQUVuQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBRTs0QkFDaEIsOEJBQThCO3lCQUNqQzs2QkFBTTs0QkFDSCxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEVBQUUsSUFBSSxPQUFBLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBVCxDQUFTLENBQUMsQ0FBQTt5QkFDcEM7d0JBQ0QsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFFcEIsT0FBTyxHQUFHLE1BQU0sQ0FBQTt3QkFFcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTs0QkFDbkMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7eUJBQzNCO3dCQUVELElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxFQUFFLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOzRCQUNuRCxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBOzRCQUN6QixPQUFPLENBQUMsS0FBSyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBRyxZQUFVLE9BQU8sQ0FBQyxLQUFLLG9CQUFPLENBQUEsQ0FBQyxDQUFBOzRCQUMvRixZQUFZOzRCQUNaLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRXpCLHNCQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQTt5QkFDekI7d0JBQ0QsdURBQXVEO3dCQUN2RCxxREFBcUQ7d0JBQ3JELHVEQUF1RDt3QkFDdkQsa0JBQWtCO3dCQUNsQixrQkFBa0I7d0JBRWxCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFOzRCQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQTt5QkFDckM7NkJBQU07NEJBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQTs0QkFDeEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO3lCQUNsQjs7Ozs7S0FRSjtJQVdELHNCQUFPLEdBQVAsVUFBUSxNQUFlLEVBQUUsR0FBVztRQUNoQyxXQUFXO1FBQ1gsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDbEMsQ0FBQyxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUN2QixDQUFDLENBQUMsYUFBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUvQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO1FBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELDZCQUFjLEdBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO0lBQy9CLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQy9CO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDOUIsQ0FBQztJQUVELHNCQUFPLEdBQVAsVUFBUSxJQUFhO1FBQ2pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNsRCxPQUFPLElBQUksQ0FBQzthQUNmO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsNEJBQWEsR0FBYixVQUFjLE1BQWlCLEVBQUUsSUFBYTtRQUMxQyxnQkFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7SUFDM0MsQ0FBQztJQUVELG1DQUFvQixHQUFwQixVQUFxQixLQUFhO1FBQzlCLElBQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFLGNBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixhQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUIsa0JBQWtCO1FBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCwyQkFBWSxHQUFaO1FBQUEsaUJBUUM7UUFQRyxhQUFHLENBQUMsUUFBUSxDQUFDLG1CQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEMsYUFBRyxDQUFDLE1BQU0sQ0FBQztZQUNQLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBRTtZQUNDLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMxQixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ3JELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVCQUFRLEdBQVI7UUFBQSxpQkFTQztRQVJHLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2xELGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUU7WUFDQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGFBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVLLGdDQUFpQixHQUF2QixVQUF3QixDQUFDLEVBQUUsS0FBSzs7Ozs7d0JBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQzt3QkFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDOzZCQUVoRCxDQUFBLEtBQUssSUFBSSxDQUFDLENBQUEsRUFBVix3QkFBVTt3QkFDVixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7d0JBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7d0JBQ3BCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTt3QkFDcEIscUJBQU0sTUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsVUFBVSxDQUFDOzRCQUNqQywwQ0FBMEM7NEJBQzFDLHNCQUFzQjswQkFGVzs7d0JBQWpDLFNBQWlDLENBQUE7d0JBQ2pDLDBDQUEwQzt3QkFDMUMsc0JBQXNCO3dCQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7d0JBQ2pDLHFCQUFNLE1BQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUFyQixTQUFxQixDQUFBO3dCQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO3dCQUN6QyxxQkFBTSxNQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBdEIsU0FBc0IsQ0FBQTt3QkFDdEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBOzs7d0JBRWYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUE7d0JBQy9CLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFLFNBQVM7NEJBQ3ZCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTs0QkFDbkIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtnQ0FDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBOzZCQUM5QjtpQ0FBTTtnQ0FDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBOzZCQUN0Qzt5QkFDSjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7eUJBQ3hCOzs7Ozs7S0FHUjtJQUVELDRCQUE0QjtJQUM1QiwyREFBMkQ7SUFDM0QsMEJBQTBCO0lBQzFCLDJEQUEyRDtJQUMzRCxvQ0FBb0M7SUFDcEMsSUFBSTtJQUVKLFFBQVE7SUFDUix3QkFBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELHlCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDOztJQUVNLFVBQUssR0FBRyxVQUFPLElBQVk7O1lBQzlCLHNCQUFPLElBQUksT0FBTyxDQUFDLFVBQUEsT0FBTztvQkFDdEIsVUFBVSxDQUFDO3dCQUNQLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ2IsQ0FBQyxDQUFDLEVBQUE7O1NBQ0wsQ0FBQTtJQUVlLFNBQUksR0FBVyxDQUFDLENBQUMsQ0FBQyxrQkFBa0I7SUFDcEMsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLFlBQVk7SUFDOUIsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87SUFDekIsU0FBSSxHQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87SUFDekMsZ0RBQWdEO0lBQ2hELDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFDeEMsd0RBQXdEO0lBRXhDLGVBQVUsR0FBVyxHQUFHLENBQUEsQ0FBQSxPQUFPO0lBQy9CLGVBQVUsR0FBVyxFQUFFLENBQUM7SUFDeEIsYUFBUSxHQUFXLElBQUksQ0FBQztJQUN4Qiw2QkFBd0IsR0FBVyxJQUFJLENBQUM7SUF4cEN4RDtRQURDLFFBQVEsQ0FBQyxjQUFJLENBQUM7c0NBQ0c7SUFFbEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztzQ0FDRztJQUV2QjtRQURDLFFBQVEsQ0FBQyxnQkFBTSxDQUFDO3VDQUNJO0lBRXJCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7c0NBQ0c7SUFFdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztzQ0FDRztJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3NDQUNHO0lBRXZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ1k7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt3Q0FDSztJQUV2QjtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7dUNBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQzt3Q0FDSztJQUV0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3lDQUNNO0lBRXpCO1FBREMsUUFBUSxDQUFDLG1CQUFTLENBQUM7c0NBQ0c7SUFFdkI7UUFEQyxRQUFRLENBQUMsZUFBSyxDQUFDO3VDQUNJO0lBRXBCO1FBREMsUUFBUSxDQUFDLE9BQU8sQ0FBQzs2Q0FDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsyQ0FDRjtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7K0NBQ0U7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2tEQUNLO0lBRXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsyQ0FDRjtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7MkNBQ0Y7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDOzBDQUNIO0lBRTlCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzsrQ0FDRTtJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7K0NBQ0U7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDOytDQUNFO0lBRW5DO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztpREFDSTtJQUVyQztRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7dUNBQ0k7SUF0REgsSUFBSTtRQUR4QixPQUFPO09BQ2EsSUFBSSxDQTRwQ3hCO0lBQUQsV0FBQztDQTVwQ0QsQUE0cENDLENBNXBDaUMsRUFBRSxDQUFDLFNBQVMsR0E0cEM3QztrQkE1cENvQixJQUFJIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFBsYXllciBmcm9tIFwiLi9QbGF5ZXJcIjtcbmltcG9ydCBEZWNrIGZyb20gXCIuL0RlY2tcIjtcbmltcG9ydCBDYXJkIGZyb20gXCIuL0NhcmRcIjtcbmltcG9ydCBIZWxwZXIgZnJvbSBcIi4vSGVscGVyXCI7XG5pbXBvcnQgQ2FyZEdyb3VwIGZyb20gXCIuL0NhcmRHcm91cFwiO1xuaW1wb3J0IENvbW1vbkNhcmQgZnJvbSBcIi4vQ29tbW9uQ2FyZFwiO1xuaW1wb3J0IFRvYXN0IGZyb20gXCIuL1RvYXN0XCI7XG5pbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IE5vdGljZSBmcm9tIFwiLi9Ob3RpY2VcIjtcbmltcG9ydCBDb25maWcgZnJvbSBcIi4vQ29uZmlnXCI7XG5pbXBvcnQgdXRpbCBmcm9tIFwiLi91dGlsXCI7XG5pbXBvcnQgU3BpbldoZWVsIGZyb20gXCIuL1NwaW5XaGVlbFwiO1xuaW1wb3J0IFBvcHVwIGZyb20gXCIuL1BvcHVwXCI7XG5pbXBvcnQgQm90IGZyb20gXCIuL0JvdFwiO1xuaW1wb3J0IE1vZGFsIGZyb20gXCIuL3BvcG9wL01vZGFsXCI7XG5pbXBvcnQgRXZlbnRLZXlzIGZyb20gXCIuL0V2ZW50S2V5c1wiO1xuaW1wb3J0IExhbmd1YWdlIGZyb20gXCIuL0xhbmd1YWdlXCI7XG5cbmxldCBzdWl0ZUFzc2V0ID0ge1xuICAgIEg6ICdjbycsXG4gICAgRDogXCJyb1wiLFxuICAgIEM6IFwiY2h1b25cIixcbiAgICBTOiBcImLDrWNoXCJcbn1cblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3MoJ1Byb2ZpbGUnKVxuY2xhc3MgUHJvZmlsZSB7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxuICAgIGF2YXRhcjogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TdHJpbmcpXG4gICAgdXNlcm5hbWU6IHN0cmluZyA9ICcnO1xufVxuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KERlY2spXG4gICAgZGVjazogRGVjayA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgICBwbGF5OiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQbGF5ZXIpXG4gICAgc2VhdHM6IFBsYXllcltdID0gW107XG4gICAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgICBmaXJlOiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgZm9sZDogY2MuQnV0dG9uID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICAgIHNvcnQ6IGNjLkJ1dHRvbiA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcmVzdWx0RWZmZWN0czogY2MuTm9kZVtdID0gW107XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgYmlnV2luOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoVG9hc3QpXG4gICAgdG9hc3Q6IFRvYXN0ID0gbnVsbDtcbiAgICBAcHJvcGVydHkoTm90aWNlKVxuICAgIG5vdGljZTogTm90aWNlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgYmV0VGV4dDogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShTcGluV2hlZWwpXG4gICAgc3BpbjogU3BpbldoZWVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoUG9wdXApXG4gICAgcG9wdXA6IFBvcHVwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoUHJvZmlsZSlcbiAgICBwcm9maWxlQm90czogUHJvZmlsZVtdID0gW107XG5cbiAgICBAcHJvcGVydHkoY2MuVG9nZ2xlKVxuICAgIHNvdW5kVG9nZ2xlOiBjYy5Ub2dnbGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9Gb2xkOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvU2hvd0NhcmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9HYXJiYWdlQ2FyZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5BdWRpb0NsaXAgfSlcbiAgICBhdWRpb0ZpcmU6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9Mb3NlOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvV2luOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvU29ydENhcmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuQXVkaW9DbGlwIH0pXG4gICAgYXVkaW9RdWl0R2FtZTogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5BdWRpb0NsaXAgfSlcbiAgICBhdWRpb0RlYWxDYXJkOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkF1ZGlvQ2xpcCB9KVxuICAgIGF1ZGlvRmlyZVNpbmdsZTogY2MuQXVkaW9DbGlwID0gbnVsbDtcbiAgICBAcHJvcGVydHkoTW9kYWwpXG4gICAgbW9kYWw6IE1vZGFsID0gbnVsbDtcblxuICAgIHBsYXllcnM6IFBsYXllcltdID0gW107XG4gICAgY29tbW9uQ2FyZHM6IENvbW1vbkNhcmQgPSBuZXcgQ29tbW9uQ2FyZCgpO1xuICAgIHN0YXRlOiBudW1iZXIgPSAwO1xuICAgIHR1cm46IG51bWJlciA9IDA7XG4gICAgbGVhdmVHYW1lOiBib29sZWFuID0gZmFsc2U7XG4gICAgb3duZXI6IFBsYXllciA9IG51bGw7XG4gICAgbXlzZWxmOiBQbGF5ZXIgPSBudWxsO1xuICAgIGJldFZhbHVlOiBudW1iZXIgPSAxMDAwO1xuICAgIHRvdGFsUGxheWVyOiBudW1iZXIgPSA2O1xuICAgIHN0YXJ0VGltZTogbnVtYmVyID0gMDtcbiAgICBwbGF5VGltZTogbnVtYmVyID0gMDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMub25Ub3VjaFN0YXJ0LCB0aGlzKTtcbiAgICAgICAgbGV0IEFzID0gY2MuZmluZCgnQ2FudmFzL2NhcmRzL0FTJyk7XG5cbiAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQoJ2NhcmRzJywgY2MuU3ByaXRlQXRsYXMsIChlcnIsIGF0bGFzKSA9PiB7XG4gICAgICAgICAgICBsZXQgaW5pdENhcmRzUmVzb3VjZXMgPSBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICAgICAgICAgICAgICBuZXcgQXJyYXkoMTMpLmZpbGwoJycpLm1hcCgoZWwsIHJhbmspID0+IHtcbiAgICAgICAgICAgICAgICAgICAgWydTJywgJ0MnLCAnRCcsICdIJ10ubWFwKChzdWl0ZSwgaWRTdWl0ZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyYW5rICYmICFzdWl0ZSkgcmV0dXJuXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgciA9IHJhbmsgKyAxXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUoQXMpXG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByID0gJ0EnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMTE6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHIgPSAnSidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxMjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgciA9ICdRJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDEzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByID0gJ0snXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgYXNzZXRTdWl0ZU5hbWUgPSBzdWl0ZUFzc2V0W3N1aXRlXVxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJhbmtBc3NldG5hbWUgPSByXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaWRTdWl0ZSA8IDIpIHJhbmtBc3NldG5hbWUgKz0gJy0xJ1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhyYW5rQXNzZXRuYW1lLCBhc3NldFN1aXRlTmFtZSwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3Tm9kZS5uYW1lID0gciArIHN1aXRlXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgc3VpdGVTcHJpdGUgPSBhdGxhcy5nZXRTcHJpdGVGcmFtZShhc3NldFN1aXRlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmNoaWxkcmVuWzBdLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gYXRsYXMuZ2V0U3ByaXRlRnJhbWUocmFua0Fzc2V0bmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmNoaWxkcmVuWzFdLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gc3VpdGVTcHJpdGVcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld05vZGUuY2hpbGRyZW5bMl0uZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBzdWl0ZVNwcml0ZVxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3Tm9kZS5jaGlsZHJlblszXS5nZXRDb21wb25lbnQoY2MuV2lkZ2V0KS50YXJnZXQgPSBuZXdOb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmFjdGl2ZSA9IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhuZXdOb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIEFzLnBhcmVudC5hZGRDaGlsZChuZXdOb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyYW5rID09PSAxMiAmJiBzdWl0ZSA9PT0gJ0gnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoMSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCA1MDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgUHJvbWlzZS5hbGwoW2luaXRDYXJkc1Jlc291Y2VzXSkudGhlbihycyA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbml0R2FtZSgpXG4gICAgICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBzdGFydCgpe31cblxuICAgIGluaXRHYW1lKCkge1xuICAgICAgICB0aGlzLnN0YXJ0VGltZSA9IERhdGUubm93KCk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLldBSVQ7XG4gICAgICAgIHRoaXMuYmV0VmFsdWUgPSBDb25maWcuYmV0VmFsdWU7XG4gICAgICAgIHRoaXMudG90YWxQbGF5ZXIgPSBDb25maWcudG90YWxQbGF5ZXI7XG4gICAgICAgIHRoaXMuYmV0VGV4dC5zdHJpbmcgPSBMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCgnTk8nKSArICc6ICcgKyB1dGlsLm51bWJlckZvcm1hdCh0aGlzLmJldFZhbHVlKTtcbiAgICAgICAgdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc29ydC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnBsYXkubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKCk7XG4gICAgICAgIHRoaXMubm90aWNlLmhpZGUoKTtcblxuICAgICAgICB0aGlzLnNwaW4ub25TcGluSGlkZSA9IHRoaXMub25TcGluSGlkZS5iaW5kKHRoaXMpO1xuICAgICAgICB0aGlzLnNwaW4ub25TcGluQ29tcGxldGVkID0gdGhpcy5vblNwaW5Db21wbGV0ZWQuYmluZCh0aGlzKTtcblxuICAgICAgICBpZiAoQ29uZmlnLmJhdHRsZSkge1xuICAgICAgICAgICAgdGhpcy50b3RhbFBsYXllciA9IDI7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgYXJyID0gLzB8My9cblxuICAgICAgICBpZiAodGhpcy50b3RhbFBsYXllciA9PT0gNCkgYXJyID0gLzB8MXwzfDUvOyBlbHNlIGlmICh0aGlzLnRvdGFsUGxheWVyID09PSA2KSBhcnIgPSAvMHwxfDJ8M3w0fDUvXG4gICAgICAgIHRoaXMuc2VhdHMubWFwKChzZWF0LCBpKSA9PiB7XG4gICAgICAgICAgICBzZWF0LnNlYXQgPSB0aGlzLnBsYXllcnMubGVuZ3RoO1xuICAgICAgICAgICAgaWYgKGFyci50ZXN0KGkudG9TdHJpbmcoKSkpXG4gICAgICAgICAgICAgICAgdGhpcy5hZGRTZWF0KHNlYXQpXG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBzZWF0LmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcblxuICAgICAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudG90YWxQbGF5ZXI7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IHNlYXQgPSB0aGlzLnNlYXRzW2ldO1xuICAgICAgICAvLyAgICAgc2VhdC5zZWF0ID0gaTtcbiAgICAgICAgLy8gICAgIGlmIChpIDwgdGhpcy50b3RhbFBsYXllcikge1xuICAgICAgICAvLyAgICAgICAgIGxldCBwcm9maWxlID0gdGhpcy5nZXRQcm9maWxlQm90KCk7XG4gICAgICAgIC8vICAgICAgICAgc2VhdC5zaG93KCk7XG4gICAgICAgIC8vICAgICAgICAgc2VhdC5zZXRBdmF0YXIocHJvZmlsZS5hdmF0YXIpO1xuICAgICAgICAvLyAgICAgICAgIHNlYXQuc2V0VXNlcm5hbWUocHJvZmlsZS51c2VybmFtZSk7XG4gICAgICAgIC8vICAgICAgICAgdGhpcy5wbGF5ZXJzLnB1c2goc2VhdCk7XG4gICAgICAgIC8vICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgICAgIHNlYXQuaGlkZSgpO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG5cblxuXG4gICAgICAgIHRoaXMubXlzZWxmID0gdGhpcy5wbGF5ZXJzW3RoaXMudG90YWxQbGF5ZXIgLyAyXTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0VXNlcm5hbWUoQXBpLnVzZXJuYW1lKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0Q29pbihBcGkuY29pbik7XG4gICAgICAgIHRoaXMubXlzZWxmLnNldFRpbWVDYWxsYmFjayh0aGlzLm9uUGxheWVyVGltZW91dCwgdGhpcywgdGhpcy5ub2RlKTtcbiAgICAgICAgaWYgKENvbmZpZy51c2VycGhvdG8pIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNldEF2YXRhcihuZXcgY2MuU3ByaXRlRnJhbWUoQ29uZmlnLnVzZXJwaG90bykpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKENvbmZpZy5iYXR0bGUpIHtcbiAgICAgICAgICAgIGxldCBib3QgPSB0aGlzLnBsYXllcnNbMV07XG4gICAgICAgICAgICBib3Quc2V0Q29pbihDb25maWcuYmF0dGxlLmNvaW4pO1xuICAgICAgICAgICAgYm90LnNldEF2YXRhcihuZXcgY2MuU3ByaXRlRnJhbWUoQ29uZmlnLmJhdHRsZS5waG90bykpO1xuICAgICAgICAgICAgYm90LnNldFVzZXJuYW1lKENvbmZpZy5iYXR0bGUudXNlcm5hbWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcChwbGF5ZXIgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChwbGF5ZXIuaXNCb3QoKSkgcGxheWVyLnNldENvaW4oQ29uZmlnLmJvdENvaW4pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICBjYy5kZWxheVRpbWUoMC43KSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuZGVhbCwgdGhpcylcbiAgICAgICAgKSk7XG5cbiAgICAgICAgaWYgKENvbmZpZy5zb3VuZEVuYWJsZSkge1xuICAgICAgICAgICAgdGhpcy5zb3VuZFRvZ2dsZS51bmNoZWNrKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNvdW5kVG9nZ2xlLmNoZWNrKCk7XG4gICAgICAgIH1cblxuICAgICAgICBBcGkucHJlbG9hZEludGVyc3RpdGlhbEFkKCk7XG4gICAgfVxuXG4gICAgYWRkU2VhdCA9IChzZWF0OiBQbGF5ZXIpID0+IHtcbiAgICAgICAgbGV0IHByb2ZpbGUgPSB0aGlzLmdldFByb2ZpbGVCb3QoKTtcbiAgICAgICAgc2VhdC5zaG93KCk7XG4gICAgICAgIHNlYXQuc2V0QXZhdGFyKHByb2ZpbGUuYXZhdGFyKTtcbiAgICAgICAgc2VhdC5zZXRVc2VybmFtZShwcm9maWxlLnVzZXJuYW1lKTtcbiAgICAgICAgdGhpcy5wbGF5ZXJzLnB1c2goc2VhdCk7XG4gICAgfVxuXG4gICAgb25EZXN0cm95KCkge1xuICAgICAgICAvLyB0aGlzLmNsZWFyTG9nVGltZSgpXG4gICAgICAgIC8vIGNvbnN0IGR1cmF0aW9uID0gRGF0ZS5ub3coKSAtIHRoaXMuc3RhcnRUaW1lO1xuICAgICAgICAvLyBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfRFVSQVRJT04sIE1hdGguZmxvb3IoZHVyYXRpb24gLyAxMDAwKSk7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlKGR0KSB7XG4gICAgLy8gICAgIGlmICh0aGlzLnBsYXlUaW1lIDwgMzAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDMwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctMzAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgNjAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDYwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctNjAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgOTAgJiYgdGhpcy5wbGF5VGltZSArIGR0ID49IDkwKSB7XG4gICAgLy8gICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBMQVlfVElNRSArICctOTAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgMTgwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSAxODApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy0xODAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgMzYwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSAzNjApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy0zNjAnKTtcbiAgICAvLyAgICAgfSBlbHNlIGlmICh0aGlzLnBsYXlUaW1lIDwgNTAwICYmIHRoaXMucGxheVRpbWUgKyBkdCA+PSA1MDApIHtcbiAgICAvLyAgICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUExBWV9USU1FICsgJy01MDAnKTtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgICB0aGlzLnBsYXlUaW1lICs9IGR0O1xuICAgIC8vIH1cblxuICAgIG9uVG91Y2hTdGFydChldnQ6IGNjLlRvdWNoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlICE9IEdhbWUuUExBWSkgcmV0dXJuO1xuXG4gICAgICAgIGxldCBzZWxlY3RlZCA9IHRoaXMubXlzZWxmLnRvdWNoKGV2dC5nZXRMb2NhdGlvbigpKTtcblxuICAgICAgICBpZiAoIXNlbGVjdGVkKSByZXR1cm47XG5cbiAgICAgICAgbGV0IHByZXBhcmVDYXJkcyA9IHRoaXMubXlzZWxmLnByZXBhcmVDYXJkcztcblxuICAgICAgICBpZiAocHJlcGFyZUNhcmRzLmNvbnRhaW5zKHNlbGVjdGVkKSkge1xuICAgICAgICAgICAgdGhpcy5teXNlbGYudW5zZWxlY3RDYXJkKHNlbGVjdGVkKTtcbiAgICAgICAgICAgIHRoaXMub25DYXJkU2VsZWN0ZWQoc2VsZWN0ZWQsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNlbGVjdENhcmQoc2VsZWN0ZWQpO1xuICAgICAgICAgICAgdGhpcy5vbkNhcmRTZWxlY3RlZChzZWxlY3RlZCwgdHJ1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy50dXJuICE9IHRoaXMubXlzZWxmLnNlYXQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwcmVwYXJlQ2FyZHMua2luZCA9PSBDYXJkR3JvdXAuVW5ncm91cGVkKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZShmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5jb21tb25DYXJkcy5sZW5ndGgoKSA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwcmVwYXJlQ2FyZHMuZ3QodGhpcy5jb21tb25DYXJkcy5wZWVrKCkpKSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWxsb3dGaXJlKGZhbHNlKTtcbiAgICB9XG5cbiAgICBvblBsYXllclRpbWVvdXQoKSB7XG4gICAgICAgIHRoaXMub25TdGFuZENsaWNrZWQoKVxuICAgICAgICAvLyB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgLy8gaWYgKHRoaXMuY29tbW9uQ2FyZHMubGVuZ3RoKCkgPT0gMCkge1xuICAgICAgICAvLyAgICAgbGV0IGNhcmRzID0gSGVscGVyLmZpbmRNaW5DYXJkKHRoaXMubXlzZWxmLmNhcmRzKTtcbiAgICAgICAgLy8gICAgIHRoaXMub25GaXJlKHRoaXMubXlzZWxmLCBuZXcgQ2FyZEdyb3VwKFtjYXJkc10sIENhcmRHcm91cC5TaW5nbGUpKTtcbiAgICAgICAgLy8gICAgIHRoaXMubXlzZWxmLnJlb3JkZXIoKTtcbiAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgLy8gICAgIHJldHVybiB0aGlzLm9uRm9sZCh0aGlzLm15c2VsZik7XG4gICAgICAgIC8vIH1cbiAgICB9XG5cbiAgICBzaG93QWxsUGxheWVyQ2FyZCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcChwbGF5ZXIgPT4ge1xuICAgICAgICAgICAgcGxheWVyLnNob3dBbGxDYXJkKClcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBvblF1aXRDbGlja2VkKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlFVSVRfR0FNRSk7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlID09IEdhbWUuUExBWSB8fCB0aGlzLnN0YXRlID09IEdhbWUuREVBTCkge1xuICAgICAgICAgICAgdGhpcy5wb3B1cC5vcGVuKHRoaXMubm9kZSwgMSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvUXVpdEdhbWUpO1xuICAgICAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdob21lJyk7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkJhY2tDbGlja2VkKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZSA9PSBHYW1lLlBMQVkgfHwgdGhpcy5zdGF0ZSA9PSBHYW1lLkRFQUwpIHtcbiAgICAgICAgICAgIGxldCBjb2luID0gTWF0aC5tYXgoMCwgQXBpLmNvaW4gLSB0aGlzLmJldFZhbHVlICogMTApO1xuICAgICAgICAgICAgQXBpLnVwZGF0ZUNvaW4oY29pbik7XG4gICAgICAgIH1cbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb1F1aXRHYW1lKTtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdob21lJyk7XG4gICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICB9XG5cbiAgICBzaG93SGFuZENhcmRzKCkge1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvU2hvd0NhcmQpO1xuICAgICAgICAvLyB0aGlzLm15c2VsZi5zaG93SGFuZENhcmRzKCk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLlBMQVk7XG5cbiAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLm15c2VsZi5jYXJkcyk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcblxuXG5cbiAgICAgICAgLy8gaWYgKEhlbHBlci5pc0JpZ1dpbih0aGlzLm15c2VsZi5jYXJkcykpIHtcblxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgICAgICBjYy5kZWxheVRpbWUoMC43KSxcbiAgICAgICAgLy8gICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLm9uQmlnV2luLCB0aGlzLCB0aGlzLm15c2VsZilcbiAgICAgICAgLy8gICAgICkpO1xuICAgICAgICAvLyAgICAgcmV0dXJuO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSgwLjcpLFxuICAgICAgICAvLyAgICAgY2MuY2FsbEZ1bmModGhpcy5teXNlbGYuc29ydEhhbmRDYXJkcywgdGhpcy5teXNlbGYpXG4gICAgICAgIC8vICkpO1xuXG4gICAgICAgIHRoaXMucGxheWVycy5tYXAoKHBsYXllciwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGlmIChwbGF5ZXIuaXNCb3QoKSkgcGxheWVyLnNob3dDYXJkQ291bnRlcigpIGVsc2Uge1xuICAgICAgICAgICAgICAgIHBsYXllci5zaG93QWxsQ2FyZCgpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG5cblxuICAgICAgICBsZXQgY2hlY2tCSiA9IHRoaXMucGxheWVycy5maWx0ZXIocGxheWVyID0+IHtcbiAgICAgICAgICAgIHBsYXllci5jaGVja0JsYWNrSmFjaygpXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy8gbGV0IGlzVXNlcldpbiA9IGNoZWNrQkouZmluZChwbGF5ZXIgPT4gcGxheWVyLmlzVXNlcigpKVxuICAgICAgICAvLyBsZXQgaXNVc2VyT3JEZWFsZXJXaW4gPSBjaGVja0JKLmZpbmQocGxheWVyID0+ICFwbGF5ZXIuaXNCb3QoKSB8fCBwbGF5ZXIuaXNDYWkoKSlcbiAgICAgICAgLy8gaWYgKGlzVXNlck9yRGVhbGVyV2luKSByZXR1cm4gdGhpcy5zaG93UmVzdWx0KClcbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSAtIDE7IGkgPiAwOyAtLWkpIHtcbiAgICAgICAgLy8gICAgIHRoaXMucGxheWVyc1tpXS5zaG93Q2FyZENvdW50ZXIoKTtcbiAgICAgICAgLy8gfVxuXG4gICAgICAgIGlmICghdGhpcy5vd25lcikge1xuICAgICAgICAgICAgdGhpcy5vd25lciA9IHRoaXMuZmluZFBsYXllckhhc0ZpcnN0Q2FyZCgpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFR1cm4odGhpcy5vd25lciwgdHJ1ZSk7XG4gICAgICAgIHRoaXMuc29ydC5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgZmluZFBsYXllckhhc0ZpcnN0Q2FyZCgpIHtcbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgIC8vICAgICBpZiAoSGVscGVyLmZpbmRDYXJkKHRoaXMucGxheWVyc1tpXS5jYXJkcywgMywgMSkpXG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgLy8gfVxuICAgICAgICBsZXQgdXNlckluUm91bmQgPSB0aGlzLnBsYXllcnMuZmlsdGVyKHBsYXllciA9PiBwbGF5ZXIuaXNJblJvdW5kKCkgJiYgIXBsYXllci5pc0NhaSgpICYmIHBsYXllcilcbiAgICAgICAgcmV0dXJuIHVzZXJJblJvdW5kWzBdO1xuICAgIH1cblxuICAgIHNob3dEYXNoYm9hcmQobGVhZDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmFsbG93Rm9sZCghbGVhZCk7XG4gICAgICAgIGlmICh0aGlzLmNvbW1vbkNhcmRzLmxlbmd0aCgpID4gMCkge1xuICAgICAgICAgICAgbGV0IGNhcmRzID0gQm90LnN1Z2dlc3QodGhpcy5jb21tb25DYXJkcywgdGhpcy5teXNlbGYuY2FyZHMpO1xuICAgICAgICAgICAgdGhpcy5hbGxvd0ZpcmUoISFjYXJkcyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmFsbG93RmlyZSh0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmZvbGQubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmZpcmUubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cblxuICAgIHNldENvbG9yKGJ0bjogY2MuQnV0dG9uLCBjb2xvcjogY2MuQ29sb3IpIHtcbiAgICAgICAgYnRuLnRhcmdldC5jb2xvciA9IGNvbG9yO1xuICAgICAgICBidG4udGFyZ2V0LmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4ge1xuICAgICAgICAgICAgY2hpbGQuY29sb3IgPSBjb2xvcjtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGlkZURhc2hib2FyZChuZWVkU2hvdyA9IGZhbHNlKSB7XG4gICAgICAgIGxldCBhY3Rpb25Vc2VyID0gY2MuZmluZCgnQ2FudmFzL2FjdGlvblVzZXInKVxuXG4gICAgICAgIGFjdGlvblVzZXIuYWN0aXZlID0gbmVlZFNob3dcbiAgICAgICAgaWYgKG5lZWRTaG93KSB7XG4gICAgICAgICAgICBsZXQgc2hvd0luc3VyciA9IHRoaXMucGxheWVyc1swXS5jYXJkc1swXS5yYW5rID09PSAxXG4gICAgICAgICAgICAvLyBzaG93SW5zdXJyID0gdHJ1ZVxuICAgICAgICAgICAgYWN0aW9uVXNlci5jaGlsZHJlbls0XS5hY3RpdmUgPSBzaG93SW5zdXJyXG5cbiAgICAgICAgICAgIGxldCBzaG93U3BsaXQgPSAhdGhpcy5teXNlbGYuaXNTcGxpdCAmJiB0aGlzLm15c2VsZi5jYXJkc1swXS5yYW5rID09PSB0aGlzLm15c2VsZi5jYXJkc1sxXS5yYW5rXG5cbiAgICAgICAgICAgIHNob3dTcGxpdCA9IHRydWVcbiAgICAgICAgICAgIGlmICghc2hvd1NwbGl0ICYmIHNob3dJbnN1cnIpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25Vc2VyLmNoaWxkcmVuWzRdLnggPSAxOTAuNDRcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFjdGlvblVzZXIuY2hpbGRyZW5bM10uYWN0aXZlID0gc2hvd1NwbGl0XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhbGxvd0ZvbGQoYWxsb3c6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5mb2xkLmludGVyYWN0YWJsZSA9IGFsbG93O1xuICAgICAgICBsZXQgY29sb3IgPSBhbGxvdyA/IGNjLkNvbG9yLldISVRFIDogY2MuQ29sb3IuR1JBWTtcbiAgICAgICAgdGhpcy5zZXRDb2xvcih0aGlzLmZvbGQsIGNvbG9yKTtcbiAgICB9XG5cbiAgICBhbGxvd0ZpcmUoYWxsb3c6IGJvb2xlYW4pIHtcbiAgICAgICAgLy8gdGhpcy5maXJlLmludGVyYWN0YWJsZSA9IGFsbG93O1xuICAgICAgICBsZXQgY29sb3IgPSBhbGxvdyA/IGNjLkNvbG9yLldISVRFIDogY2MuQ29sb3IuR1JBWTtcbiAgICAgICAgdGhpcy5zZXRDb2xvcih0aGlzLmZpcmUsIGNvbG9yKTtcbiAgICB9XG5cbiAgICBvblNvcnRDYXJkQ2xpY2tlZCgpIHtcbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb1NvcnRDYXJkKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc29ydEhhbmRDYXJkcygpO1xuICAgIH1cblxuICAgIG9uRmlyZUNsaWNrZWQoKSB7XG4gICAgICAgIHRoaXMub25IaXQoKVxuICAgICAgICAvLyBsZXQgY2FyZHMgPSB0aGlzLm15c2VsZi5wcmVwYXJlQ2FyZHM7XG4gICAgICAgIC8vIGlmIChjYXJkcy5jb3VudCgpID09IDApIHsgcmV0dXJuIGNvbnNvbGUubG9nKCdlbXB0eScpOyB9XG5cbiAgICAgICAgLy8gaWYgKHRoaXMuY29tbW9uQ2FyZHMubGVuZ3RoKCkgPiAwICYmICFjYXJkcy5ndCh0aGlzLmNvbW1vbkNhcmRzLnBlZWsoKSkpIHtcbiAgICAgICAgLy8gICAgIHJldHVybiBjb25zb2xlLmxvZygnaW52YWxpZCcpO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gaWYgKGNhcmRzLmlzSW52YWxpZCgpKSB7XG4gICAgICAgIC8vICAgICByZXR1cm4gY29uc29sZS5sb2coJ2ludmFsaWQnKTtcbiAgICAgICAgLy8gfVxuXG4gICAgICAgIC8vIHRoaXMuaGlkZURhc2hib2FyZCgpO1xuICAgICAgICAvLyB0aGlzLm9uRmlyZSh0aGlzLm15c2VsZiwgY2FyZHMpO1xuICAgICAgICAvLyAvLyByZXNvcnQgaGFuZCBjYXJkXG4gICAgICAgIC8vIHRoaXMubXlzZWxmLnJlb3JkZXIoKTtcbiAgICB9XG5cblxuICAgIG9uRm9sZENsaWNrZWQoKSB7XG4gICAgICAgIC8vIHRoaXMuaGlkZURhc2hib2FyZCgpO1xuICAgICAgICAvLyB0aGlzLm9uRm9sZCh0aGlzLm15c2VsZik7XG4gICAgfVxuXG4gICAgZGVhbCgpIHtcbiAgICAgICAgQXBpLnByZWxvYWRSZXdhcmRlZFZpZGVvKCk7XG4gICAgICAgIGlmIChBcGkuY29pbiA8PSB0aGlzLmJldFZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLm9wZW4obnVsbCwgMyk7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fT1BFTik7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICB0aGlzLnBsYXkubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5kZWNrLnNodWZmbGUoKTtcbiAgICAgICAgdGhpcy5jb21tb25DYXJkcy5yZXNldCgpO1xuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGJvdCA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgICAgIGlmIChib3QuZ2V0Q29pbigpIDw9IHRoaXMuYmV0VmFsdWUgKiAzKSB7XG4gICAgICAgICAgICAgICAgbGV0IHByb2ZpbGUgPSB0aGlzLmdldFByb2ZpbGVCb3QoKTtcbiAgICAgICAgICAgICAgICBsZXQgcmF0ZSA9IDAuNSArIE1hdGgucmFuZG9tKCkgKiAwLjU7XG4gICAgICAgICAgICAgICAgYm90LnNldENvaW4oTWF0aC5mbG9vcih0aGlzLm15c2VsZi5nZXRDb2luKCkgKiByYXRlKSk7XG4gICAgICAgICAgICAgICAgYm90LnNldEF2YXRhcihwcm9maWxlLmF2YXRhcik7XG4gICAgICAgICAgICAgICAgYm90LnNldFVzZXJuYW1lKHByb2ZpbGUudXNlcm5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY2MuZmluZCgnQ2FudmFzL2FjdGlvblVzZXIvaW5zdXJyJykueCA9IDQwMi4wMzhcblxuICAgICAgICB0aGlzLnNlYXRzWzZdLmhpZGUoKVxuXG4gICAgICAgIHRoaXMucGxheWVycy5tYXAocCA9PiB7XG4gICAgICAgICAgICBwLnJlc2V0KCk7XG4gICAgICAgICAgICBwLnVwZGF0ZVBvaW50KCcwJyk7XG4gICAgICAgICAgICBwLnNldEJvbnVzVHlwZSgwKVxuICAgICAgICB9KVxuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgMjsgaSsrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKTsgaisrKSB7XG4gICAgICAgICAgICAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tqXTtcbiAgICAgICAgICAgICAgICBsZXQgY2FyZCA9IHRoaXMuZGVjay5waWNrKCk7XG4gICAgICAgICAgICAgICAgcGxheWVyLnB1c2goY2FyZCwgMC4zICsgKGkgKiB0aGlzLmdldFRvdGFsUGxheWVyKCkgKyBqKSAqIEdhbWUuREVBTF9TUEVFRCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy8gdGhpcy5kZWNrLmhpZGUoKTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZShHYW1lLkRFQUxfU1BFRUQgKiB0aGlzLnRvdGFsUGxheWVyICogMiksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnNob3dIYW5kQ2FyZHMsIHRoaXMpKVxuICAgICAgICApO1xuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgY2MuZGVsYXlUaW1lKDAuNCksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYygoKSA9PiB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvRGVhbENhcmQpKVxuICAgICAgICApKTtcblxuICAgICAgICBBcGkucHJlbG9hZEludGVyc3RpdGlhbEFkKCk7XG4gICAgfVxuXG4gICAgc2hvd0VmZmVjdChwbGF5ZXI6IFBsYXllciwgZWZmZWN0OiBjYy5Ob2RlKSB7XG4gICAgICAgIGVmZmVjdC5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBlZmZlY3Quc2V0UG9zaXRpb24ocGxheWVyLm5vZGUuZ2V0UG9zaXRpb24oKSk7XG4gICAgfVxuXG4gICAgb25Gb2xkKHBsYXllcjogUGxheWVyKSB7XG4gICAgICAgIHV0aWwucGxheUF1ZGlvKHRoaXMuYXVkaW9Gb2xkKTtcbiAgICAgICAgaWYgKHRoaXMuY29tbW9uQ2FyZHMuaGFzQ29tYmF0KCkpIHtcbiAgICAgICAgICAgIGxldCBjb3VudCA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0Q29tYmF0TGVuZ3RoKCk7XG4gICAgICAgICAgICBsZXQgdmljdGltID0gdGhpcy5jb21tb25DYXJkcy5nZXRDb21iYXRWaWN0aW0oKTtcbiAgICAgICAgICAgIGxldCB3aW5uZXIgPSB0aGlzLmNvbW1vbkNhcmRzLmdldENvbWJhdFdpbm5lcigpO1xuICAgICAgICAgICAgbGV0IGJpZ1BpZyA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0Q29tYmF0KCk7XG4gICAgICAgICAgICBsZXQgc3BvdFdpbiA9IE1hdGgucG93KDIsIGNvdW50IC0gMSkgKiBIZWxwZXIuY2FsY3VsYXRlU3BvdFdpbihiaWdQaWcuY2FyZHMpICogdGhpcy5iZXRWYWx1ZTtcbiAgICAgICAgICAgIGxldCBjb2luID0gTWF0aC5taW4oc3BvdFdpbiwgdmljdGltLmNvaW5WYWwpO1xuICAgICAgICAgICAgd2lubmVyLmFkZENvaW4oY29pbik7XG4gICAgICAgICAgICB2aWN0aW0uc3ViQ29pbihjb2luKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5jb21tb25DYXJkcy5pc0NvbWJhdE9wZW4oKSkge1xuICAgICAgICAgICAgdGhpcy5jb21tb25DYXJkcy5yZXNldENvbWJhdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgcGxheWVyLnNldEluUm91bmQoZmFsc2UpO1xuICAgICAgICBwbGF5ZXIuc2V0QWN0aXZlKGZhbHNlKTtcbiAgICAgICAgbGV0IHBsYXllcnMgPSB0aGlzLmdldEluUm91bmRQbGF5ZXJzKCk7XG4gICAgICAgIGlmIChwbGF5ZXJzLmxlbmd0aCA+PSAyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5uZXh0VHVybigpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubmV4dFJvdW5kKHBsYXllcnNbMF0pO1xuICAgIH1cblxuICAgIG9uRmlyZShwbGF5ZXI6IFBsYXllciwgY2FyZHM6IENhcmRHcm91cCwgaXNEb3duOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgY29uc3QgYXVkaW9DbGlwID0gY2FyZHMuaGlnaGVzdC5yYW5rID09IDE1IHx8IGNhcmRzLmNvdW50KCkgPiAxID8gdGhpcy5hdWRpb0ZpcmUgOiB0aGlzLmF1ZGlvRmlyZVNpbmdsZTtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjE3KSxcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKCgpID0+IHV0aWwucGxheUF1ZGlvKGF1ZGlvQ2xpcCkpXG4gICAgICAgICkpO1xuXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUoZmFsc2UpO1xuICAgICAgICBsZXQgY2FyZENvdW50ID0gY2FyZHMuY291bnQoKTtcbiAgICAgICAgbGV0IHBvcyA9IHRoaXMuY29tbW9uQ2FyZHMuZ2V0UG9zaXRpb24oKTtcblxuICAgICAgICBjYXJkcy5zb3J0KCk7XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IGNhcmRzLmNvdW50KCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKGlzRG93bikgY2FyZHMuYXQoaSkuc2hvdygpO1xuXG4gICAgICAgICAgICBsZXQgY2FyZCA9IGNhcmRzLmF0KGkpO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMuY29tbW9uQ2FyZHMudG90YWxDYXJkcyArIGk7XG4gICAgICAgICAgICBsZXQgbW92ZSA9IGNjLm1vdmVUbygwLjMsIGNjLnYyKHBvcy54ICsgKGkgLSBjYXJkQ291bnQgLyAyKSAqIEdhbWUuQ0FSRF9TUEFDRSwgcG9zLnkpKTtcbiAgICAgICAgICAgIGNhcmQubm9kZS5ydW5BY3Rpb24obW92ZSk7XG4gICAgICAgICAgICBsZXQgc2NhbGUgPSBjYy5zY2FsZVRvKDAuMywgMC42KTtcbiAgICAgICAgICAgIGNhcmQubm9kZS5ydW5BY3Rpb24oc2NhbGUpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY29tbW9uQ2FyZHMucHVzaChjYXJkcyk7XG4gICAgICAgIGlmIChjYXJkcy5oaWdoZXN0LnJhbmsgPT0gMTUpIHtcbiAgICAgICAgICAgIHRoaXMuY29tbW9uQ2FyZHMucHVzaENvbWJhdChwbGF5ZXIsIGNhcmRzKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmNvbW1vbkNhcmRzLmlzQ29tYmF0T3BlbigpKSB7XG4gICAgICAgICAgICB0aGlzLmNvbW1vbkNhcmRzLnB1c2hDb21iYXQocGxheWVyLCBjYXJkcyk7XG4gICAgICAgIH1cbiAgICAgICAgcGxheWVyLnJlbW92ZUNhcmRzKGNhcmRzKTtcbiAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgICAgICBwbGF5ZXIudXBkYXRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjYXJkcy5oaWdoZXN0LnJhbmsgPT0gMTUpIHtcbiAgICAgICAgICAgIHRoaXMubm90aWNlLnNob3dCaWdQaWcoY2FyZHMuY291bnQoKSwgMyk7XG4gICAgICAgIH0gZWxzZSBpZiAoY2FyZHMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIGNhcmRzLmNvdW50KCkgPT0gKDMgKiAyKSkge1xuICAgICAgICAgICAgdGhpcy5ub3RpY2Uuc2hvdyhOb3RpY2UuVEhSRUVfUEFJUiwgMyk7XG4gICAgICAgIH0gZWxzZSBpZiAoY2FyZHMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIGNhcmRzLmNvdW50KCkgPT0gKDQgKiAyKSkge1xuICAgICAgICAgICAgdGhpcy5ub3RpY2Uuc2hvdyhOb3RpY2UuRk9VUl9QQUlSLCAzKTtcbiAgICAgICAgfSBlbHNlIGlmIChjYXJkcy5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiBjYXJkcy5jb3VudCgpID09IDQpIHtcbiAgICAgICAgICAgIHRoaXMubm90aWNlLnNob3coTm90aWNlLkZPVVJfQ0FSRCwgMyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHBsYXllci5jYXJkcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5uZXh0VHVybigpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zdGF0ZSA9IEdhbWUuTEFURTtcbiAgICAgICAgdGhpcy5zb3J0Lm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICBjYy5kZWxheVRpbWUoMiksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnNob3dSZXN1bHQsIHRoaXMsIHBsYXllcilcbiAgICAgICAgKSk7XG4gICAgfVxuXG4gICAgb25CaWdXaW4oc2VuZGVyOiBjYy5Ob2RlLCB3aW5uZXI6IFBsYXllcikge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgICAgICB0aGlzLmJpZ1dpbi5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBsZXQgdG90YWwgPSAwO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgICAgIGlmIChwbGF5ZXIgIT0gd2lubmVyKSB7XG4gICAgICAgICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHBsYXllci5oaWRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgcGxheWVyLnNob3dIYW5kQ2FyZHMoKTtcbiAgICAgICAgICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbGV0IGxvc3QgPSAxMyAqIHRoaXMuYmV0VmFsdWU7XG4gICAgICAgICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIuc3ViQ29pbihsb3N0KTtcbiAgICAgICAgICAgICAgICB0b3RhbCArPSBsb3N0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHdpbm5lci5hZGRDb2luKHRvdGFsKTtcbiAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIHRoaXMub25XaW4odG90YWwpO1xuICAgIH1cblxuICAgIC8vIHNob3dSZXN1bHQoc2VuZGVyOiBjYy5Ob2RlLCB3aW5uZXI6IFBsYXllcikge1xuICAgIC8vICAgICBsZXQgcGxheWVycyA9IHRoaXMucGxheWVycy5maWx0ZXIoKHBsYXllcikgPT4gKHBsYXllciAhPSB3aW5uZXIpKTtcbiAgICAvLyAgICAgcGxheWVycy5zb3J0KChhLCBiKSA9PiB7IHJldHVybiBhLmNhcmRzLmxlbmd0aCAtIGIuY2FyZHMubGVuZ3RoOyB9KTtcblxuICAgIC8vICAgICB0aGlzLnNob3dFZmZlY3Qod2lubmVyLCB0aGlzLnJlc3VsdEVmZmVjdHNbMF0pO1xuICAgIC8vICAgICBsZXQgdG90YWwgPSAwO1xuICAgIC8vICAgICBsZXQgcmFua2luZyA9IDE7XG4gICAgLy8gICAgIGlmICh3aW5uZXIgPT0gdGhpcy5teXNlbGYpIHJhbmtpbmcgPSAxO1xuICAgIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBsYXllcnMubGVuZ3RoOyBpKyspIHtcbiAgICAvLyAgICAgICAgIGxldCBwbGF5ZXIgPSBwbGF5ZXJzW2ldO1xuICAgIC8vICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgLy8gICAgICAgICAgICAgcGxheWVyLmhpZGVDYXJkQ291bnRlcigpO1xuICAgIC8vICAgICAgICAgICAgIHBsYXllci5zaG93SGFuZENhcmRzKCk7XG4gICAgLy8gICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgICAgIGlmIChwbGF5ZXIgPT0gdGhpcy5teXNlbGYpIHJhbmtpbmcgPSBpICsgMjtcbiAgICAvLyAgICAgICAgIHRoaXMuc2hvd0VmZmVjdChwbGF5ZXIsIHRoaXMucmVzdWx0RWZmZWN0c1tpICsgMV0pO1xuICAgIC8vICAgICAgICAgbGV0IGxvc3QgPSBIZWxwZXIuY2FsY3VsYXRlKHBsYXllci5jYXJkcykgKiB0aGlzLmJldFZhbHVlO1xuICAgIC8vICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAvLyAgICAgICAgIHBsYXllci5zdWJDb2luKGxvc3QpO1xuICAgIC8vICAgICAgICAgdG90YWwgKz0gbG9zdDtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgICB3aW5uZXIuYWRkQ29pbih0b3RhbCk7XG4gICAgLy8gICAgIEFwaS51cGRhdGVDb2luKHRoaXMubXlzZWxmLmNvaW5WYWwpO1xuICAgIC8vICAgICB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgIC8vICAgICBpZiAod2lubmVyID09IHRoaXMubXlzZWxmKSB7XG4gICAgLy8gICAgICAgICB0aGlzLm9uV2luKHRvdGFsKTtcbiAgICAvLyAgICAgfSBlbHNlIHtcbiAgICAvLyAgICAgICAgIHRoaXMub25Mb3NlKHJhbmtpbmcpO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuXG4gICAgb25Mb3NlKHJhbmtpbmc6IG51bWJlcikge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLkxPU0UgKyAnXycgKyByYW5raW5nKTtcbiAgICAgICAgdXRpbC5wbGF5QXVkaW8odGhpcy5hdWRpb0xvc2UpO1xuICAgICAgICBjb25zdCBybmQgPSBNYXRoLnJhbmRvbSgpO1xuICAgICAgICBpZiAocmFua2luZyA9PSAyICYmIHJuZCA8PSAwLjMpIHtcbiAgICAgICAgICAgIEFwaS5zaG93SW50ZXJzdGl0aWFsQWQoKTtcbiAgICAgICAgfSBlbHNlIGlmIChyYW5raW5nID09IDMgJiYgcm5kIDw9IDAuMikge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICB9IGVsc2UgaWYgKHJhbmtpbmcgPT0gNCAmJiBybmQgPD0gMC4xKSB7XG4gICAgICAgICAgICBBcGkuc2hvd0ludGVyc3RpdGlhbEFkKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuICAgIH1cblxuICAgIG9uV2luKHdvbjogbnVtYmVyKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuV0lOKTtcbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9TUElOX09QRU4pO1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvV2luKTtcbiAgICAgICAgY29uc3Qgcm5kID0gTWF0aC5yYW5kb20oKTtcbiAgICAgICAgaWYgKHJuZCA8PSAwLjMgJiYgQXBpLmlzSW50ZXJzdGl0aWFsQWRMb2FkZWQoKSkge1xuICAgICAgICAgICAgQXBpLnNob3dJbnRlcnN0aXRpYWxBZCgpO1xuICAgICAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zcGluLnNob3cod29uKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uU3BpbkhpZGUoKSB7XG4gICAgICAgIHRoaXMuZGVsYXlSZXNldCgzKTtcbiAgICB9XG5cbiAgICBvblNwaW5Db21wbGV0ZWQocmVzdWx0LCB3b246IG51bWJlcikge1xuICAgICAgICBsZXQgYm9udXMgPSB0aGlzLnNwaW4uY29tcHV0ZShyZXN1bHQuaWQsIHdvbik7XG4gICAgICAgIGlmIChib251cyA+IDApIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLmFkZENvaW4oYm9udXMpO1xuICAgICAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkZWxheVJlc2V0KGR0OiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLmRlbGF5VGltZShkdCksXG4gICAgICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLnJlc2V0LCB0aGlzKVxuICAgICAgICApKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMucmVzdWx0RWZmZWN0cy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgdGhpcy5yZXN1bHRFZmZlY3RzW2ldLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmdldFRvdGFsUGxheWVyKCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgdGhpcy5wbGF5ZXJzW2ldLnJlc2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuYmlnV2luLmFjdGl2ZSkge1xuICAgICAgICAgICAgdGhpcy5iaWdXaW4uYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLy8gdGhpcy5kZWNrLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMucGxheS5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgb25DYXJkU2VsZWN0ZWQoY2FyZDogQ2FyZCwgc2VsZWN0ZWQ6IGJvb2xlYW4pIHtcblxuICAgIH1cblxuICAgIG5leHRUdXJuKCkge1xuICAgICAgICBsZXQgbmV4dCA9IHRoaXMuZ2V0TmV4dEluUm91bmRQbGF5ZXIoKTtcbiAgICAgICAgdGhpcy5zZXRDdXJyZW50VHVybihuZXh0LCBmYWxzZSk7XG4gICAgfVxuXG4gICAgbmV4dFJvdW5kKGxlYWQ6IFBsYXllcikge1xuICAgICAgICB0aGlzLmNvbW1vbkNhcmRzLm5leHRSb3VuZCgpO1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5nZXRUb3RhbFBsYXllcigpIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIHRoaXMucGxheWVyc1tpXS5zZXRJblJvdW5kKHRydWUpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFR1cm4obGVhZCwgdHJ1ZSk7XG4gICAgICAgIC8vIHV0aWwucGxheUF1ZGlvKHRoaXMuYXVkaW9HYXJiYWdlQ2FyZCk7XG4gICAgfVxuXG4gICAgb25IaXRDbGlja2VkKCkge1xuICAgICAgICBsZXQgcGxheWVyID0gdGhpcy5teXNlbGZcbiAgICAgICAgdGhpcy50YWtlQ2FyZChwbGF5ZXIpXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuaGlkZURhc2hib2FyZCgpXG4gICAgfVxuXG4gICAgb25Eb3VibGVDbGlja2VkKCkge1xuXG4gICAgfVxuXG4gICAgb25TcGxpdENsaWNrZWQoKSB7XG5cbiAgICB9XG5cbiAgICBvblN0YW5kQ2xpY2tlZCgpIHtcbiAgICAgICAgbGV0IHBsYXllciA9IHRoaXMubXlzZWxmXG4gICAgICAgIHBsYXllci5zZXRBY3RpdmUoZmFsc2UpO1xuXG4gICAgICAgIGlmICghcGxheWVyLmluUm91bmQgJiYgcGxheWVyLnN1YlVzZXIpIHtcbiAgICAgICAgICAgIHBsYXllciA9IHBsYXllci5zdWJVc2VyXG4gICAgICAgIH1cblxuICAgICAgICBwbGF5ZXIuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKClcbiAgICAgICAgdGhpcy5uZXh0VHVybigpXG4gICAgfVxuXG4gICAgc2hvd1Jlc3VsdCA9ICgpID0+IHtcbiAgICAgICAgLy8gbGV0IGxlYWRlclBvaW50ID0gdGhpcy5wbGF5ZXJzLnNsaWNlKC0xKVswXS5wb2ludFxuICAgICAgICAvLyBsZXQgdXNlclBvaW50ID0gdGhpcy5wbGF5ZXJzWzBdLnBvaW50XG5cbiAgICAgICAgLy8gbGV0IHJlc3VsdFN0cmluZyA9IFtgxJFp4buDbSBjw6FpOiAke2xlYWRlclBvaW50fWAsIGDEkGnhu4NtIHVzZXI6ICR7dXNlclBvaW50fWBdXG5cbiAgICAgICAgLy8gdGhpcy5wbGF5ZXJzLm1hcChwID0+IHtcbiAgICAgICAgLy8gICAgIGlmIChwLmlzQm90KCkgJiYgIXAuaXNDYWkoKSkge1xuICAgICAgICAvLyAgICAgICAgIHJlc3VsdFN0cmluZy5wdXNoKGDEkGnhu4NtIGJvdCAke3Jlc3VsdFN0cmluZy5sZW5ndGggLSAxfTogJHtwLnBvaW50fWApXG4gICAgICAgIC8vICAgICB9XG4gICAgICAgIC8vIH0pXG5cblxuICAgICAgICBsZXQgZGVhbGVyID0gdGhpcy5wbGF5ZXJzWzBdXG5cbiAgICAgICAgbGV0IGRlYWxlckFkZENvaW4gPSAwXG5cbiAgICAgICAgdGhpcy5wbGF5ZXJzLm1hcCgocGxheWVyLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgaWYgKCFpbmRleCkgcmV0dXJuXG5cbiAgICAgICAgICAgIGxldCBwbGF5ZXJDb2luID0gdGhpcy5jYWxDb2ludEVuZEdhbWUocGxheWVyKVxuICAgICAgICAgICAgaWYgKHBsYXllci5zdWJVc2VyKSB7XG4gICAgICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmNhbENvaW50RW5kR2FtZShwbGF5ZXIuc3ViVXNlcilcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcGxheWVyLmNoYW5nZUNvaW4ocGxheWVyQ29pbilcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ3VzZXIgJyArIGluZGV4ICsgJyAnICsgcGxheWVyQ29pbik7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG5cbiAgICAgICAgICAgIGRlYWxlckFkZENvaW4gLT0gcGxheWVyQ29pblxuICAgICAgICB9KVxuXG4gICAgICAgIGRlYWxlci5jaGFuZ2VDb2luKGRlYWxlckFkZENvaW4pXG5cbiAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICBjb25zb2xlLmxvZygnbmhhIGNhaSAnICsgZGVhbGVyQWRkQ29pbik7XG4gICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcblxuICAgICAgICB0aGlzLnNob3dBbGxQbGF5ZXJDYXJkKClcbiAgICAgICAgdGhpcy5kZWxheVJlc2V0KDMpO1xuXG5cblxuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICAvLyB0aGlzLm93bmVyID0gd2lubmVyO1xuICAgICAgICAvLyB0aGlzLmJpZ1dpbi5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAvLyBsZXQgdG90YWwgPSAwO1xuICAgICAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IHBsYXllciA9IHRoaXMucGxheWVyc1tpXTtcbiAgICAgICAgLy8gICAgIGlmIChwbGF5ZXIgIT0gd2lubmVyKSB7XG4gICAgICAgIC8vICAgICAgICAgaWYgKHBsYXllci5pc0JvdCgpKSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIHBsYXllci5oaWRlQ2FyZENvdW50ZXIoKTtcbiAgICAgICAgLy8gICAgICAgICAgICAgcGxheWVyLnNob3dIYW5kQ2FyZHMoKTtcbiAgICAgICAgLy8gICAgICAgICAgICAgcGxheWVyLnJlb3JkZXIoKTtcbiAgICAgICAgLy8gICAgICAgICB9XG4gICAgICAgIC8vICAgICAgICAgbGV0IGxvc3QgPSAxMyAqIHRoaXMuYmV0VmFsdWU7XG4gICAgICAgIC8vICAgICAgICAgbG9zdCA9IE1hdGgubWluKHBsYXllci5jb2luVmFsLCBsb3N0KTtcbiAgICAgICAgLy8gICAgICAgICBwbGF5ZXIuc3ViQ29pbihsb3N0KTtcbiAgICAgICAgLy8gICAgICAgICB0b3RhbCArPSBsb3N0O1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG4gICAgICAgIC8vIHdpbm5lci5hZGRDb2luKHRvdGFsKTtcbiAgICAgICAgQXBpLnVwZGF0ZUNvaW4odGhpcy5teXNlbGYuY29pblZhbCk7XG4gICAgICAgIC8vIHRoaXMub25XaW4odG90YWwpO1xuXG4gICAgICAgIC8vIHJldHVybiB0aGlzLnRvYXN0LnNob3coYFThu5VuZyBjbW4ga+G6v3QgcuG7k2lcXG4gJHtyZXN1bHRTdHJpbmcuam9pbignXFxuJyl9YClcbiAgICB9XG5cblxuICAgIGNhbENvaW50RW5kR2FtZSA9IChwbGF5ZXI6IFBsYXllcikgPT4ge1xuICAgICAgICBsZXQgZGVhbGVyID0gdGhpcy5wbGF5ZXJzWzBdXG4gICAgICAgIGxldCBwbGF5ZXJDb2luID0gMFxuICAgICAgICBpZiAocGxheWVyLmJvbnVzVHlwZSA9PSAyKSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luIC09IHRoaXMuYmV0VmFsdWUgLyAyXG4gICAgICAgIH0gZWxzZSBpZiAocGxheWVyLnBvaW50ID4gMjEpIHtcbiAgICAgICAgICAgIHBsYXllckNvaW4gLT0gdGhpcy5iZXRWYWx1ZVxuICAgICAgICAgICAgLy8gcGxheWVyLnN1YkNvaW4odGhpcy5iZXRWYWx1ZSlcbiAgICAgICAgfSBlbHNlIGlmIChwbGF5ZXIuY2hlY2tCbGFja0phY2soKSkge1xuICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmJldFZhbHVlICogMS41XG4gICAgICAgIH0gZWxzZSBpZiAocGxheWVyLnBvaW50ID4gZGVhbGVyLnBvaW50KSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luICs9IHRoaXMuYmV0VmFsdWVcbiAgICAgICAgfSBlbHNlIGlmIChwbGF5ZXIucG9pbnQgPT09IGRlYWxlci5wb2ludCkge1xuICAgICAgICAgICAgcmV0dXJuIDBcbiAgICAgICAgfSBlbHNlIGlmIChkZWFsZXIucG9pbnQgPiAyMSkge1xuICAgICAgICAgICAgcGxheWVyQ29pbiArPSB0aGlzLmJldFZhbHVlXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luIC09IHRoaXMuYmV0VmFsdWVcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgvMS8udGVzdChwbGF5ZXIuYm9udXNUeXBlLnRvU3RyaW5nKCkpKSB7XG4gICAgICAgICAgICBwbGF5ZXJDb2luICo9IDJcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGxheWVyQ29pblxuICAgIH1cblxuICAgIGFzeW5jIHNldEN1cnJlbnRUdXJuKHBsYXllcjogUGxheWVyLCBsZWFkOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMucGxheWVycy5tYXAocCA9PiBwLnNldEFjdGl2ZShmYWxzZSkpXG4gICAgICAgIGlmICghcGxheWVyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zaG93UmVzdWx0KClcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnR1cm4gPSBwbGF5ZXIuc2VhdDtcbiAgICAgICAgcGxheWVyLnNldEFjdGl2ZSh0cnVlKTtcblxuICAgICAgICBpZiAocGxheWVyLmlzVXNlcigpKSB7XG4gICAgICAgICAgICByZXR1cm4gR2FtZS5zbGVlcChHYW1lLldBSVRfUkVfU0hPV19VU0VSX0FDVElPTikudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKHRydWUpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGNhbkhpdCA9IHBsYXllci5wb2ludCA+IHRoaXMucGxheWVyc1swXS5wb2ludFxuXG4gICAgICAgIGlmIChwbGF5ZXIucG9pbnQgPCAxOCkge1xuICAgICAgICAgICAgY2FuSGl0ID0gdHJ1ZVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFjYW5IaXQpIHtcbiAgICAgICAgICAgIGNhbkhpdCA9IHRoaXMucGxheWVycy5maW5kKHAgPT4gcGxheWVyLnBvaW50ID4gcC5wb2ludCkgIT0gdW5kZWZpbmVkXG4gICAgICAgIH1cblxuICAgICAgICBpZiAocGxheWVyLnBvaW50ID49IDE4KSBjYW5IaXQgPSBmYWxzZVxuXG4gICAgICAgIGlmIChwbGF5ZXIuY2FyZHM/Lmxlbmd0aCA9PT0gNSkgY2FuSGl0ID0gZmFsc2VcblxuICAgICAgICBhd2FpdCBHYW1lLnNsZWVwKEdhbWUuV0FJVF9CT1QpO1xuICAgICAgICBwbGF5ZXIuc2V0SW5Sb3VuZChmYWxzZSlcbiAgICAgICAgLy8gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGlmIChjYW5IaXQpIHtcbiAgICAgICAgICAgIHRoaXMudGFrZUNhcmQocGxheWVyKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy50b2FzdC5zaG93KHBsYXllci51c2VybmFtZS5zdHJpbmcgKyAnIHN0YW5kJyArIHBsYXllci5wb2ludCArIFwiIMSRaeG7g21cIilcbiAgICAgICAgICAgIHRoaXMubmV4dFR1cm4oKVxuICAgICAgICB9XG4gICAgICAgIC8vIH0sIDI1MDApO1xuXG5cbiAgICAgICAgLy8gaWYgKCFwbGF5ZXIuaXNCb3QoKSkge1xuICAgICAgICAvLyAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKHRydWUpXG4gICAgICAgIC8vIH0gZWxzZSB7XG5cbiAgICAgICAgLy8gaWYoKXtcblxuICAgICAgICAvLyB9XG5cbiAgICAgICAgLy8gfVxuXG5cblxuICAgICAgICAvLyBpZiAodGhpcy50dXJuID4gMCkge1xuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgICAgICBjYy5kZWxheVRpbWUoMS41KSxcbiAgICAgICAgLy8gICAgICAgICBjYy5jYWxsRnVuYyh0aGlzLmV4ZWNCb3QsIHRoaXMsIHBsYXllcilcbiAgICAgICAgLy8gICAgICkpO1xuICAgICAgICAvLyB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgdGhpcy5zaG93RGFzaGJvYXJkKGxlYWQpO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgZ2V0SW5Sb3VuZFBsYXllcnMoKTogQXJyYXk8UGxheWVyPiB7XG4gICAgICAgIGxldCBwbGF5ZXJzOiBBcnJheTxQbGF5ZXI+ID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLmdldFRvdGFsUGxheWVyKCkgLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKHRoaXMucGxheWVyc1tpXS5pc0luUm91bmQoKSlcbiAgICAgICAgICAgICAgICBwbGF5ZXJzLnB1c2godGhpcy5wbGF5ZXJzW2ldKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGxheWVycztcbiAgICB9XG5cbiAgICBnZXROZXh0SW5Sb3VuZFBsYXllcigpOiBQbGF5ZXIge1xuICAgICAgICAvLyB0aW0gdGhhbmcgbmFvIGNvIGdoZSBuZ29pID4gdHVybiB0cmMgdmEgZGFuZyBvIHRyb25nIHZhbiBjaG9pXG4gICAgICAgIGxldCBuZXh0VHVybiA9IHRoaXMucGxheWVycy5maW5kKHBsYXllciA9PiB7XG4gICAgICAgICAgICByZXR1cm4gKHBsYXllci5zZWF0ID09PSB0aGlzLnR1cm4gJiYgcGxheWVyPy5zdWJVc2VyPy5pblJvdW5kKSB8fCAocGxheWVyLnNlYXQgPiB0aGlzLnR1cm4gJiYgcGxheWVyLmlzSW5Sb3VuZCgpKVxuICAgICAgICB9KVxuXG4gICAgICAgIGlmICghbmV4dFR1cm4gJiYgdGhpcy5wbGF5ZXJzWzBdLmluUm91bmQpIHJldHVybiB0aGlzLnBsYXllcnNbMF0gLy8gbmV1IGtob25nIGNvbiBhaSB0aGkgY2h1eWVuIGx1b3QgY2hvIG5oYSBjYWlcblxuICAgICAgICAvLyBsZXQgbmV4dFR1cm4gPSB0aGlzLnBsYXllcnNbdGhpcy50dXJuICsgMV1cbiAgICAgICAgLy8gaWYgKCFuZXh0VHVybiAmJiB0aGlzLnBsYXllcnNbMF0uaXNJblJvdW5kKCkpIG5leHRUdXJuID0gdGhpcy5wbGF5ZXJzWzBdXG4gICAgICAgIC8vIGlmICghbmV4dFR1cm4uaXNJblJvdW5kKCkpIHJldHVyblxuICAgICAgICByZXR1cm4gbmV4dFR1cm5cbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IDE7IGkgPCB0aGlzLmdldFRvdGFsUGxheWVyKCk7IGkrKykge1xuICAgICAgICAvLyAgICAgbGV0IG9mZnNldCA9IHRoaXMudHVybiArIGk7XG4gICAgICAgIC8vICAgICBpZiAob2Zmc2V0ID49IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKSkgb2Zmc2V0IC09IHRoaXMuZ2V0VG90YWxQbGF5ZXIoKTtcbiAgICAgICAgLy8gICAgIGlmICh0aGlzLnBsYXllcnNbb2Zmc2V0XS5pc0luUm91bmQoKSlcbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gdGhpcy5wbGF5ZXJzW29mZnNldF07XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgY2FsY3VsYXRlUG9pbnQ6IChwbGF5ZXI6IFBsYXllcikgPT4ge1xuXG4gICAgfVxuXG4gICAgYXN5bmMgdGFrZUNhcmQocGxheWVyOiBQbGF5ZXIsIG5lZWROZXh0ID0gdHJ1ZSkge1xuICAgICAgICBsZXQgY2FyZCA9IHRoaXMuZGVjay5waWNrKCk7XG5cbiAgICAgICAgdGhpcy50b2FzdC5zaG93KHBsYXllci51c2VybmFtZS5zdHJpbmcgKyAnOiAnICsgcGxheWVyLnBvaW50ICsgXCLEkSBi4buRYyB0aMOqbVwiICsgY2FyZC5yYW5rKVxuICAgICAgICBpZiAoIWNhcmQ/Lm5vZGUpIHtcbiAgICAgICAgICAgIHRoaXMuc3RhdGUgPSBHYW1lLkxBVEU7XG4gICAgICAgICAgICB0aGlzLnNvcnQubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAvLyAgICAgY2MuZGVsYXlUaW1lKDIpLFxuICAgICAgICAgICAgLy8gICAgIGNjLmNhbGxGdW5jKHRoaXMuc2hvd1Jlc3VsdCwgdGhpcywgcGxheWVyKVxuICAgICAgICAgICAgLy8gKSk7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBwbGF5ZXIucHVzaChjYXJkLCAwKTtcblxuICAgICAgICBpZiAoIW5lZWROZXh0KSByZXR1cm5cbiAgICAgICAgLy8gY29uc3QgYXVkaW9DbGlwID0gY2FyZHMuaGlnaGVzdC5yYW5rID09IDE1IHx8IGNhcmRzLmNvdW50KCkgPiAxID8gdGhpcy5hdWRpb0ZpcmUgOiB0aGlzLmF1ZGlvRmlyZVNpbmdsZTtcbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSgwLjE3KSxcbiAgICAgICAgLy8gICAgIGNjLmNhbGxGdW5jKCgpID0+IHV0aWwucGxheUF1ZGlvKGF1ZGlvQ2xpcCkpXG4gICAgICAgIC8vICkpO1xuXG4gICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoMClcblxuICAgICAgICBpZiAocGxheWVyLmlzQm90KCkpIHtcbiAgICAgICAgICAgIC8vIHBsYXllci51cGRhdGVDYXJkQ291bnRlcigpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGxheWVyLmNhcmRzLm1hcChlbCA9PiBlbC5zaG93KCkpXG4gICAgICAgIH1cbiAgICAgICAgcGxheWVyLnNldEFjdGl2ZShmYWxzZSk7XG5cbiAgICAgICAgbGV0IF9wbGF5ZXIgPSBwbGF5ZXJcblxuICAgICAgICBpZiAoIXBsYXllci5pblJvdW5kICYmIHBsYXllci5zdWJVc2VyKSB7XG4gICAgICAgICAgICBfcGxheWVyID0gcGxheWVyLnN1YlVzZXJcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfcGxheWVyLnBvaW50ID49IDIxIHx8IF9wbGF5ZXIuY2FyZHMubGVuZ3RoID09PSA1KSB7XG4gICAgICAgICAgICBfcGxheWVyLnNldEluUm91bmQoZmFsc2UpXG4gICAgICAgICAgICBfcGxheWVyLnBvaW50ID4gMjEgJiYgdGhpcy50b2FzdC5zaG93KF9wbGF5ZXIudXNlcm5hbWUuc3RyaW5nICsgYCBUaHVhOiAke19wbGF5ZXIucG9pbnR9IMSRaeG7g21gKVxuICAgICAgICAgICAgLy8gYm90IHN0YW5kXG4gICAgICAgICAgICBfcGxheWVyLnNldEFjdGl2ZShmYWxzZSk7XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLm5leHRUdXJuKClcbiAgICAgICAgfVxuICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHBsYXllci5jYXJkcy5tYXAoZWwgPT4gZWwubm9kZS5uYW1lKSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgLy8gdGhpcy5jaGVja1dpbigpXG4gICAgICAgIC8vIHRoaXMubmV4dFR1cm4oKVxuXG4gICAgICAgIGlmICghcGxheWVyLmJvbnVzVHlwZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRDdXJyZW50VHVybihwbGF5ZXIsIGZhbHNlKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGxheWVyLnNldEluUm91bmQoZmFsc2UpXG4gICAgICAgICAgICB0aGlzLm5leHRUdXJuKClcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHNldFRpbWVvdXQodGhpcy5uZXh0VHVybiwgMTUwMCk7XG5cbiAgICAgICAgLy8gdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgLy8gICAgIGNjLmRlbGF5VGltZSguMTUpLFxuICAgICAgICAvLyAgICAgY2MuY2FsbEZ1bmModGhpcy5uZXh0VHVybilcbiAgICAgICAgLy8gKSk7XG4gICAgfVxuXG4gICAgY2hlY2tXaW4gPSAoKSA9PiB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5wbGF5ZXJzKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG4gICAgICAgICAgICAvLyB0aGlzLnBsYXllcnMuXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZXhlY0JvdChzZW5kZXI6IGNjLk5vZGUsIGJvdDogUGxheWVyKSB7XG4gICAgICAgIC8vIGFsZXJ0KDEpXG4gICAgICAgIGxldCBjYXJkcyA9IHRoaXMuY29tbW9uQ2FyZHMuaXNFbXB0eSgpXG4gICAgICAgICAgICA/IEJvdC5yYW5kb20oYm90LmNhcmRzKVxuICAgICAgICAgICAgOiBCb3Quc3VnZ2VzdCh0aGlzLmNvbW1vbkNhcmRzLCBib3QuY2FyZHMpO1xuXG4gICAgICAgIGlmICghY2FyZHMpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9uRm9sZChib3QpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5vbkZpcmUodGhpcy5wbGF5ZXJzW3RoaXMudHVybl0sIGNhcmRzLCB0cnVlKTtcbiAgICB9XG5cbiAgICBnZXRUb3RhbFBsYXllcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGxheWVycy5sZW5ndGg7XG4gICAgfVxuXG4gICAgZ2V0UHJvZmlsZUJvdCgpOiBQcm9maWxlIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCAxMDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcm5kID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy5wcm9maWxlQm90cy5sZW5ndGgpO1xuICAgICAgICAgICAgaWYgKCF0aGlzLmlzVXNhZ2UodGhpcy5wcm9maWxlQm90c1tybmRdKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnByb2ZpbGVCb3RzW3JuZF1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5wcm9maWxlQm90c1swXVxuICAgIH1cblxuICAgIGlzVXNhZ2UodXNlcjogUHJvZmlsZSkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGxheWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKHRoaXMucGxheWVyc1tpXS51c2VybmFtZS5zdHJpbmcgPT0gdXNlci51c2VybmFtZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBvblNvdW5kVG9nZ2xlKHNlbmRlcjogY2MuVG9nZ2xlLCBpc09uOiBib29sZWFuKSB7XG4gICAgICAgIENvbmZpZy5zb3VuZEVuYWJsZSA9ICFzZW5kZXIuaXNDaGVja2VkO1xuICAgIH1cblxuICAgIGNsYWltQmFua3J1cHRjeU1vbmV5KGJvbnVzOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgbXNnID0gY2MuanMuZm9ybWF0U3RyKExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KCdNT05FWTEnKSwgdXRpbC5udW1iZXJGb3JtYXQoYm9udXMpKTtcbiAgICAgICAgdGhpcy50b2FzdC5zaG93KG1zZyk7XG4gICAgICAgIEFwaS5jb2luSW5jcmVtZW50KGJvbnVzKTtcbiAgICAgICAgdGhpcy5teXNlbGYuc2V0Q29pbihBcGkuY29pbik7XG4gICAgICAgIHRoaXMucG9wdXAuY2xvc2UobnVsbCwgMyk7XG4gICAgICAgIC8vIHVwZGF0ZSBiZXQgcm9vbVxuICAgICAgICB0aGlzLmJldFZhbHVlID0gTWF0aC5yb3VuZChBcGkuY29pbiAqIDAuMyk7XG4gICAgICAgIHRoaXMuYmV0VGV4dC5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdCh0aGlzLmJldFZhbHVlKTtcbiAgICB9XG5cbiAgICBpbnZpdGVGaXJlbmQoKSB7XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuSU5WSVRFX0ZSSUVORCk7XG4gICAgICAgIEFwaS5pbnZpdGUoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShDb25maWcuYmFua3J1cHRfYm9udXMpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDMpO1xuICAgICAgICAgICAgdGhpcy50b2FzdC5zaG93KCdN4budaSBi4bqhbiBjaMahaSBraMO0bmcgdGjDoG5oIGPDtG5nJyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFkUmV3YXJkKCkge1xuICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fQ0xJQ0spO1xuICAgICAgICBBcGkuc2hvd1Jld2FyZGVkVmlkZW8oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShDb25maWcuYmFua3J1cHRfYm9udXMpO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBvcHVwLmNsb3NlKG51bGwsIDMpO1xuICAgICAgICAgICAgdGhpcy5jbGFpbUJhbmtydXB0Y3lNb25leShBcGkucmFuZG9tQm9udXMoKSk7XG4gICAgICAgICAgICBBcGkubG9nRXZlbnQoRXZlbnRLZXlzLlBPUFVQX0FEUkVXQVJEX0NPSU5fRVJST1IpO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIGFzeW5jIG9uU2V0Qm9udXNDbGlja2VkKGUsIGJvbnVzKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCc9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0nKTtcbiAgICAgICAgY29uc29sZS5sb2coYm9udXMpO1xuICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG5cbiAgICAgICAgaWYgKGJvbnVzID09IDMpIHtcbiAgICAgICAgICAgIHRoaXMubXlzZWxmLnNwbGl0KHRoaXMuc2VhdHNbNl0pXG4gICAgICAgICAgICB0aGlzLnNlYXRzWzZdLnNob3coKVxuICAgICAgICAgICAgdGhpcy5oaWRlRGFzaGJvYXJkKClcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoR2FtZS5ERUFMX1NQRUVEKVxuICAgICAgICAgICAgLy8gdGhpcy5zZXRDdXJyZW50VHVybih0aGlzLm15c2VsZiwgZmFsc2UpXG4gICAgICAgICAgICAvLyB0aGlzLm9uSGl0Q2xpY2tlZCgpXG4gICAgICAgICAgICB0aGlzLnRha2VDYXJkKHRoaXMubXlzZWxmLCBmYWxzZSlcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoNTAwKVxuICAgICAgICAgICAgdGhpcy50YWtlQ2FyZCh0aGlzLm15c2VsZi5zdWJVc2VyLCBmYWxzZSlcbiAgICAgICAgICAgIGF3YWl0IEdhbWUuc2xlZXAoMTAwMClcbiAgICAgICAgICAgIHRoaXMubmV4dFR1cm4oKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5teXNlbGYuc2V0Qm9udXNUeXBlKGJvbnVzKVxuICAgICAgICAgICAgaWYgKGJvbnVzID09IDEpIHsgLy8gZG91YmxlXG4gICAgICAgICAgICAgICAgdGhpcy5vbkhpdENsaWNrZWQoKVxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm15c2VsZi5pblJvdW5kKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlzZWxmLmluUm91bmQgPSBmYWxzZVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlzZWxmLnN1YlVzZXIuaW5Sb3VuZCA9IGZhbHNlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uU3RhbmRDbGlja2VkKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgLy8gb25TZXRCb251cyA9IChib251cykgPT4ge1xuICAgIC8vICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGJvbnVzKTtcbiAgICAvLyAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgIC8vICAgICAvLyB0aGlzLm15c2VsZi5zZXRCb251c1R5cGUoKVxuICAgIC8vIH1cblxuICAgIC8vIGNoZWF0XG4gICAgY2hlYXRfd2luKCkge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgdGhpcy5zaG93UmVzdWx0KG51bGwsIHRoaXMubXlzZWxmKTtcbiAgICB9XG5cbiAgICBjaGVhdF9sb3NlKCkge1xuICAgICAgICB0aGlzLnN0YXRlID0gR2FtZS5MQVRFO1xuICAgICAgICB0aGlzLmhpZGVEYXNoYm9hcmQoKTtcbiAgICAgICAgdGhpcy5zaG93UmVzdWx0KG51bGwsIHRoaXMucGxheWVyc1sxXSk7XG4gICAgfVxuXG4gICAgc3RhdGljIHNsZWVwID0gYXN5bmMgKHdhaXQ6IG51bWJlcikgPT4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKDEpO1xuICAgICAgICAgICAgfSwgd2FpdCk7XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgc3RhdGljIHJlYWRvbmx5IFdBSVQ6IG51bWJlciA9IDA7IC8vIHdhaXQgZm9yIHBsYXllclxuICAgIHN0YXRpYyByZWFkb25seSBERUFMOiBudW1iZXIgPSAxOyAvLyBkZWFsIGNhcmRcbiAgICBzdGF0aWMgcmVhZG9ubHkgUExBWTogbnVtYmVyID0gMjsgLy8gcGxheVxuICAgIHN0YXRpYyByZWFkb25seSBMQVRFOiBudW1iZXIgPSAzOyAvLyBsYXRlXG4gICAgLy8gc3RhdGljIHJlYWRvbmx5IERFQUxfU1BFRUQ6IG51bWJlciA9IDAvLzEuNjY7XG4gICAgLy8gc3RhdGljIHJlYWRvbmx5IENBUkRfU1BBQ0U6IG51bWJlciA9IDQ1O1xuICAgIC8vIHN0YXRpYyByZWFkb25seSBXQUlUX0JPVDogbnVtYmVyID0gMDtcbiAgICAvLyBzdGF0aWMgcmVhZG9ubHkgV0FJVF9SRV9TSE9XX1VTRVJfQUNUSU9OOiBudW1iZXIgPSAwO1xuXG4gICAgc3RhdGljIHJlYWRvbmx5IERFQUxfU1BFRUQ6IG51bWJlciA9IDEuMi8vMS42NjtcbiAgICBzdGF0aWMgcmVhZG9ubHkgQ0FSRF9TUEFDRTogbnVtYmVyID0gNDU7XG4gICAgc3RhdGljIHJlYWRvbmx5IFdBSVRfQk9UOiBudW1iZXIgPSAyMDAwO1xuICAgIHN0YXRpYyByZWFkb25seSBXQUlUX1JFX1NIT1dfVVNFUl9BQ1RJT046IG51bWJlciA9IDEwMDA7XG5cbn1cbiJdfQ==