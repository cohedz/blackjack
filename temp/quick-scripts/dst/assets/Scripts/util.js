
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/util.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5164dU8pUhEeacqN2jObHLU', 'util');
// Scripts/util.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var Config_1 = require("./Config");
var util = /** @class */ (function () {
    function util() {
    }
    util.numberFormat = function (n) {
        var ii = 0;
        while ((n = n / 1000) >= 1) {
            ii++;
        }
        return (Math.round(n * 10 * 1000) / 10) + util.prefix[ii];
    };
    util.playAudio = function (audioClip) {
        if (Config_1.default.soundEnable)
            cc.audioEngine.playEffect(audioClip, false);
    };
    util.prefix = ["", "k", "M", "G", "T", "P", "E", "Z", "Y", "x10^27", "x10^30", "x10^33"]; // should be enough. Number.MAX_VALUE is about 10^308
    util.logTimeInGame = function (time) {
        if ([30, 60, 90, 120, 180, 270, 510].indexOf(time) > -1 || time === 0) {
            Api_1.default.logEvent(time + "s_sstime");
        }
    };
    return util;
}());
exports.default = util;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL3V0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw2QkFBd0I7QUFDeEIsbUNBQThCO0FBRTlCO0lBQUE7SUFtQkEsQ0FBQztJQWhCaUIsaUJBQVksR0FBMUIsVUFBMkIsQ0FBUztRQUNoQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFBRSxFQUFFLEVBQUUsQ0FBQztTQUFFO1FBQ3JDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRWEsY0FBUyxHQUF2QixVQUF3QixTQUF1QjtRQUMzQyxJQUFJLGdCQUFNLENBQUMsV0FBVztZQUNsQixFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQVhlLFdBQU0sR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxxREFBcUQ7SUFhNUksa0JBQWEsR0FBRyxVQUFDLElBQVk7UUFDdkMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxFQUFFO1lBQ25FLGFBQUcsQ0FBQyxRQUFRLENBQUksSUFBSSxhQUFVLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUMsQ0FBQTtJQUNMLFdBQUM7Q0FuQkQsQUFtQkMsSUFBQTtrQkFuQm9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQXBpIGZyb20gXCIuL0FwaVwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgdXRpbCB7XG4gICAgc3RhdGljIHJlYWRvbmx5IHByZWZpeCA9IFtcIlwiLCBcImtcIiwgXCJNXCIsIFwiR1wiLCBcIlRcIiwgXCJQXCIsIFwiRVwiLCBcIlpcIiwgXCJZXCIsIFwieDEwXjI3XCIsIFwieDEwXjMwXCIsIFwieDEwXjMzXCJdOyAvLyBzaG91bGQgYmUgZW5vdWdoLiBOdW1iZXIuTUFYX1ZBTFVFIGlzIGFib3V0IDEwXjMwOFxuXG4gICAgcHVibGljIHN0YXRpYyBudW1iZXJGb3JtYXQobjogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IGlpID0gMDtcbiAgICAgICAgd2hpbGUgKChuID0gbiAvIDEwMDApID49IDEpIHsgaWkrKzsgfVxuICAgICAgICByZXR1cm4gKE1hdGgucm91bmQobiAqIDEwICogMTAwMCkgLyAxMCkgKyB1dGlsLnByZWZpeFtpaV07XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBwbGF5QXVkaW8oYXVkaW9DbGlwOiBjYy5BdWRpb0NsaXApIHtcbiAgICAgICAgaWYgKENvbmZpZy5zb3VuZEVuYWJsZSlcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoYXVkaW9DbGlwLCBmYWxzZSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBsb2dUaW1lSW5HYW1lID0gKHRpbWU6IG51bWJlcikgPT4ge1xuICAgICAgICBpZiAoWzMwLCA2MCwgOTAsIDEyMCwgMTgwLCAyNzAsIDUxMF0uaW5kZXhPZih0aW1lKSA+IC0xIHx8IHRpbWUgPT09IDApIHtcbiAgICAgICAgICAgIEFwaS5sb2dFdmVudChgJHt0aW1lfXNfc3N0aW1lYCk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=