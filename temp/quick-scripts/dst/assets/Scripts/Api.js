
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Api.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cffd1wdHiNPIaFpHSP+5t/F', 'Api');
// Scripts/Api.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var image_1 = require("./image");
var Api = /** @class */ (function () {
    function Api() {
    }
    Api.initAsync = function (callback, leaderboard) {
        if (Api.initialized) {
            Api.loadLeaderboard(leaderboard);
            return callback(false, Api.dailyBonusDay); // daily_bonus_claimed_at, daily_bonus_day
        }
        if (!Api.initialized && window.hasOwnProperty('FBInstant')) {
            Api.initialized = true;
            Api.photo = FBInstant.player.getPhoto();
            Api.username = FBInstant.player.getName();
            Api.playerId = FBInstant.player.getID();
            Api.locale = FBInstant.getLocale();
            Api.getPlayerData(callback);
            Api.loadLeaderboard(leaderboard);
            Api.subscribeBot();
            Api.createShortcut();
        }
        else {
            Api.ticket = 10;
            Api.dailyBonusDay = 0;
            // setTimeout(() => {
            Api.locale = 'vi_VN';
            // Api.locale = 'th_TH';
            // callback(true, Api.dailyBonusDay);
            Api.loadLeaderboard(leaderboard);
            // }, 5000);
        }
    };
    Api.getPlayerData = function (callback) {
        FBInstant.player.getDataAsync(['coin', 'ticket', 'daily_bonus_claimed_at', 'daily_bonus_day']).then(function (data) {
            cc.log('data is loaded');
            Api.coin = data['coin'];
            if (Api.coin == undefined || Api.coin == null) {
                Api.coin = Config_1.default.defaultCoin;
                Api.isDirty = true;
            }
            Api.ticket = data['ticket'] || 0;
            Api.dailyBonusDay = data['daily_bonus_day'] || 0;
            Api.dailyBonusClaimed = !Api.dailyBonusClaimable(data['daily_bonus_claimed_at']);
            callback(!Api.dailyBonusClaimed, Api.dailyBonusDay);
            if (Api.isDirty) {
                Api.flush();
            }
        });
    };
    Api.subscribeBot = function () {
        FBInstant.player.canSubscribeBotAsync().then(function (can_subscribe) {
            cc.log('subscribeBotAsync', can_subscribe);
            Api.canSubscribeBot = can_subscribe.valueOf();
            // Then when you want to ask the player to subscribe
            if (Api.canSubscribeBot) {
                FBInstant.player.subscribeBotAsync().then(
                // Player is subscribed to the bot
                ).catch(function (e) {
                    // Handle subscription failure
                    cc.log('subscribeBotAsync');
                });
            }
        }).catch(function (e) {
            cc.log('subscribeBotAsync', e);
        });
    };
    Api.createShortcut = function () {
        FBInstant.canCreateShortcutAsync()
            .then(function (canCreateShortcut) {
            if (canCreateShortcut) {
                FBInstant.createShortcutAsync()
                    .then(function () {
                    // Shortcut created
                })
                    .catch(function () {
                    // Shortcut not created
                });
            }
        });
    };
    Api.loadLeaderboard = function (leaderboard) {
        fetch(Api.lb_api + "/" + Api.lb_id).then(function (response) { return response.json(); }).then(function (res) {
            var _a;
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            ((_a = res === null || res === void 0 ? void 0 : res.entries) === null || _a === void 0 ? void 0 : _a.length) && res.entries.map(function (e, i) {
                leaderboard.render(i + 1, e.username, e.score, e.photo, e.user_id);
            });
            // for (let i = 0; i < res.entries.length; i++) {
            //     const e = res.entries[i];
            //     leaderboard.render(
            //         i + 1,
            //         e.username,
            //         e.score,
            //         e.photo,
            //         e.user_id,
            //     );
            // }
            leaderboard.onLoadComplete();
        }).catch(function (error) { return console.error(error); });
    };
    Api.preloadRewardedVideo = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (!window.hasOwnProperty('FBInstant')) {
        if (callback)
            callback();
        return;
        // }
        if (Api.preloadedRewardedVideo || !Config_1.default.reward_video) {
            return;
        }
        FBInstant.getRewardedVideoAsync(Config_1.default.reward_video).then(function (rewarded) {
            // Load the Ad asynchronously
            Api.preloadedRewardedVideo = rewarded;
            return Api.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            cc.log('Rewarded video preloaded');
            if (callback)
                callback();
        }).catch(function (e) {
            Api.preloadedRewardedVideo = null;
            console.error(e.message);
            if (callback)
                callback();
        });
    };
    Api.showRewardedVideo = function (success, error) {
        // if (!window.hasOwnProperty('FBInstant')) {
        //     return error('rrr');
        // }
        // if (!Config.reward_video) {
        success();
        // }
        // if (!Api.preloadedRewardedVideo) {
        //     console.error('ad not load yet');
        //     return error('ad not load yet');
        // }
        // Api.preloadedRewardedVideo.showAsync().then(function () {
        //     success();
        //     Api.preloadedRewardedVideo = null;
        //     cc.log('Rewarded video watched successfully');
        // }).catch(function (e) {
        //     error(e.message);
        //     Api.preloadedRewardedVideo = null;
        //     console.error(e.message);
        // });
    };
    Api.preloadInterstitialAd = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (window.hasOwnProperty('FBInstant') && !Api.preloadedInterstitial) {
        //     FBInstant.getInterstitialAdAsync(Config.intertital_ads).then(function(interstitial) {
        //         // Load the Ad asynchronously
        //         Api.preloadedInterstitial = interstitial;
        //         return Api.preloadedInterstitial.loadAsync();
        //     }).then(function() {
        //         cc.log('Interstitial preloaded');
        if (callback)
            callback();
        // }).catch(function(err){
        //     cc.error('Interstitial failed to preload');
        //     cc.error(err);
        // });
        // }
    };
    Api.showInterstitialAd = function () {
        // if (window.hasOwnProperty('FBInstant')) {
        //     if (Api.preloadedInterstitial) {
        //         Api.preloadedInterstitial.showAsync().then(function () {
        //             // Perform post-ad success operation
        //             cc.log('Interstitial ad finished successfully');
        //             Api.preloadedInterstitial = null;
        //         }).catch(function (e) {
        //             cc.error(e.message);
        //             Api.preloadedInterstitial = null;
        //         });
        //     }
        // }
    };
    Api.isInterstitialAdLoaded = function () {
        if (!window.hasOwnProperty('FBInstant')) {
            return false;
        }
        return !!Api.preloadedInterstitial;
    };
    Api.challenge = function (playerId) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FBInstant.context.createAsync(playerId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, FBInstant.updateAsync({
                                action: 'CUSTOM',
                                template: 'play_turn',
                                cta: 'Chơi ngay',
                                image: image_1.default,
                                text: Api.username + ' đang thách đấu bạn. Nhấn chơi ngay!',
                                strategy: 'IMMEDIATE'
                            })];
                }
            });
        });
    };
    Api.shareAsync = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                if (window.hasOwnProperty('FBInstant')) {
                    return [2 /*return*/, FBInstant.shareAsync({
                            intent: 'SHARE',
                            image: image_1.default,
                            text: 'Tiến liên miền nam 2020!',
                        })];
                }
                return [2 /*return*/, Promise.resolve()];
            });
        });
    };
    Api.invite = function (success, error) {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.context
                .chooseAsync()
                .then(function () {
                success(FBInstant.context.getID());
                FBInstant.updateAsync({
                    action: 'CUSTOM',
                    template: 'play_turn',
                    cta: 'Chơi ngay',
                    image: image_1.default,
                    text: Api.username + ' mời bạn cùng chơi game tiến liên miền nam',
                    strategy: 'IMMEDIATE'
                });
            }).catch(error);
        }
        else {
            success('');
        }
    };
    Api.coinIncrement = function (val) {
        Api.updateCoin(Api.coin + val);
    };
    Api.updateCoin = function (coin) {
        Api.coin = coin;
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function () { return cc.log('Score saved'); }).catch(function (err) { return console.error(err); });
    };
    Api.updateTicket = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ ticket: Api.ticket }).then(function () {
                cc.log('ticket is set');
            });
        }
    };
    Api.claimDailyBonus = function (day) {
        Api.dailyBonusClaimed = true;
        if (window.hasOwnProperty('FBInstant')) {
            var claimed_at = new Date();
            FBInstant.player.setDataAsync({
                coin: Api.coin,
                ticket: Api.ticket,
                'daily_bonus_claimed_at': claimed_at.toString(),
                'daily_bonus_day': day
            }).then(function () {
                cc.log('data is set');
            }, function (reason) {
                cc.log('data is not set', reason);
            });
        }
        Api.dailyBonusDay++;
    };
    Api.dailyBonusClaimable = function (claimedAt) {
        if (!claimedAt)
            return true;
        if (typeof claimedAt == 'string') {
            claimedAt = new Date(claimedAt);
        }
        var now = new Date();
        if (now.getFullYear() != claimedAt.getFullYear())
            return true;
        if (now.getMonth() != claimedAt.getMonth())
            return true;
        if (now.getDate() != claimedAt.getDate())
            return true;
        return false;
    };
    Api.flush = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin, ticket: Api.ticket }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function (res) { return res.json(); }).then(function (res) { return cc.log(res); }).catch(function (err) { return console.error(err); });
    };
    Api.logEvent = function (eventName, valueToSum, parameters) {
        if (!eventName)
            return;
        // if (window.hasOwnProperty('FBInstant')) {
        try {
            // window.firebase.analytics().logEvent(eventName, parameters);
            console.log('====================================');
            console.log(eventName, parameters);
            console.log('====================================');
        }
        catch (error) {
            console.log('====================================');
            console.log('analytic error', error);
            console.log('====================================');
        }
        // FBInstant.logEvent(eventName, valueToSum, parameters);
        // }
    };
    Api.setScoreAsync = function (score) {
        // if (!window.hasOwnProperty('FBInstant')) return;
        var data = { score: score, userId: Api.playerId, name: Api.username, photo: Api.photo };
        return fetch(Api.lb_api + "/" + Api.lb_id, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    };
    Api.randomBonus = function () {
        var r = Math.random();
        if (r < 0.05)
            return 400000;
        if (r < 0.2)
            return 300000;
        if (r < 0.5)
            return 200000;
        return 100000;
    };
    Api.initialized = false;
    Api.coin = 10000;
    Api.ticket = 0;
    Api.username = 'DM';
    Api.playerId = 'DM';
    Api.locale = 'vi_VN';
    Api.photo = 'https://i.imgur.com/FUOxs43.jpg';
    Api.isDirty = false;
    Api.preloadedRewardedVideo = null;
    Api.preloadedInterstitial = null;
    Api.canSubscribeBot = false;
    Api.dailyBonusDay = 0;
    Api.dailyBonusClaimed = false;
    Api.lb_api = 'https://leaderboard.adpia.com.vn/instant'; //'https://lb.dozo.vn/api'
    Api.lb_id = 'a330e5054804f7fb73bd1f99'; //'5f714415630b9b9ff8146f15'
    Api.ID_APP = '440201687124598';
    return Api;
}());
exports.default = Api;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1DQUE4QjtBQUM5QixpQ0FBbUM7QUFFbkM7SUFBQTtJQStWQSxDQUFDO0lBN1VpQixhQUFTLEdBQXZCLFVBQXdCLFFBQStDLEVBQUUsV0FBd0I7UUFDN0YsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFO1lBQ2pCLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsT0FBTyxRQUFRLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLDBDQUEwQztTQUN4RjtRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDeEQsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDdkIsR0FBRyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMxQyxHQUFHLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDeEMsR0FBRyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFbkMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM1QixHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNuQixHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDeEI7YUFBTTtZQUNILEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ2hCLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLHFCQUFxQjtZQUNyQixHQUFHLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNyQix3QkFBd0I7WUFDeEIscUNBQXFDO1lBQ3JDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakMsWUFBWTtTQUNmO0lBQ0wsQ0FBQztJQUVjLGlCQUFhLEdBQTVCLFVBQTZCLFFBQStDO1FBQ3hFLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSx3QkFBd0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSTtZQUM5RyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDekIsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEIsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLFNBQVMsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDM0MsR0FBRyxDQUFDLElBQUksR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDOUIsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDdEI7WUFDRCxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakQsR0FBRyxDQUFDLGlCQUFpQixHQUFHLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7WUFDakYsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNwRCxJQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ2IsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFYyxnQkFBWSxHQUEzQjtRQUNJLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxhQUFhO1lBQ2hFLEVBQUUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFDM0MsR0FBRyxDQUFDLGVBQWUsR0FBRyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDOUMsb0RBQW9EO1lBQ3BELElBQUksR0FBRyxDQUFDLGVBQWUsRUFBRTtnQkFDckIsU0FBUyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLElBQUk7Z0JBQ3JDLGtDQUFrQztpQkFDckMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUNmLDhCQUE4QjtvQkFDOUIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNoQyxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRWMsa0JBQWMsR0FBN0I7UUFDSSxTQUFTLENBQUMsc0JBQXNCLEVBQUU7YUFDN0IsSUFBSSxDQUFDLFVBQVUsaUJBQWlCO1lBQzdCLElBQUksaUJBQWlCLEVBQUU7Z0JBQ25CLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRTtxQkFDMUIsSUFBSSxDQUFDO29CQUNGLG1CQUFtQjtnQkFDdkIsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQztvQkFDSCx1QkFBdUI7Z0JBQzNCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFYSxtQkFBZSxHQUE3QixVQUE4QixXQUF3QjtRQUNsRCxLQUFLLENBQUksR0FBRyxDQUFDLE1BQU0sU0FBSSxHQUFHLENBQUMsS0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFmLENBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7O1lBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUVwRCxPQUFBLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxPQUFPLDBDQUFFLE1BQU0sS0FBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN6QyxXQUFXLENBQUMsTUFBTSxDQUNkLENBQUMsR0FBRyxDQUFDLEVBQ0wsQ0FBQyxDQUFDLFFBQVEsRUFDVixDQUFDLENBQUMsS0FBSyxFQUNQLENBQUMsQ0FBQyxLQUFLLEVBQ1AsQ0FBQyxDQUFDLE9BQU8sQ0FDWixDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUE7WUFFRixpREFBaUQ7WUFDakQsZ0NBQWdDO1lBQ2hDLDBCQUEwQjtZQUMxQixpQkFBaUI7WUFDakIsc0JBQXNCO1lBQ3RCLG1CQUFtQjtZQUNuQixtQkFBbUI7WUFDbkIscUJBQXFCO1lBQ3JCLFNBQVM7WUFDVCxJQUFJO1lBQ0osV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRWEsd0JBQW9CLEdBQWxDLFVBQW1DLFFBQTJCO1FBQTNCLHlCQUFBLEVBQUEsZUFBMkI7UUFDMUQsNkNBQTZDO1FBQzdDLElBQUksUUFBUTtZQUFFLFFBQVEsRUFBRSxDQUFDO1FBQ3pCLE9BQU87UUFDUCxJQUFJO1FBQ0osSUFBSSxHQUFHLENBQUMsc0JBQXNCLElBQUksQ0FBQyxnQkFBTSxDQUFDLFlBQVksRUFBRTtZQUNwRCxPQUFPO1NBQ1Y7UUFDRCxTQUFTLENBQUMscUJBQXFCLENBQUMsZ0JBQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRO1lBQ3hFLDZCQUE2QjtZQUM3QixHQUFHLENBQUMsc0JBQXNCLEdBQUcsUUFBUSxDQUFDO1lBQ3RDLE9BQU8sR0FBRyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2xELENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNKLEVBQUUsQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUNuQyxJQUFJLFFBQVE7Z0JBQUUsUUFBUSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNQLEdBQUcsQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7WUFDbEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekIsSUFBSSxRQUFRO2dCQUFFLFFBQVEsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVhLHFCQUFpQixHQUEvQixVQUFnQyxPQUFtQixFQUFFLEtBQTRCO1FBQzdFLDZDQUE2QztRQUM3QywyQkFBMkI7UUFDM0IsSUFBSTtRQUNKLDhCQUE4QjtRQUM5QixPQUFPLEVBQUUsQ0FBQztRQUNWLElBQUk7UUFDSixxQ0FBcUM7UUFDckMsd0NBQXdDO1FBQ3hDLHVDQUF1QztRQUN2QyxJQUFJO1FBQ0osNERBQTREO1FBQzVELGlCQUFpQjtRQUNqQix5Q0FBeUM7UUFDekMscURBQXFEO1FBQ3JELDBCQUEwQjtRQUMxQix3QkFBd0I7UUFDeEIseUNBQXlDO1FBQ3pDLGdDQUFnQztRQUNoQyxNQUFNO0lBQ1YsQ0FBQztJQUVhLHlCQUFxQixHQUFuQyxVQUFvQyxRQUEyQjtRQUEzQix5QkFBQSxFQUFBLGVBQTJCO1FBQzNELDBFQUEwRTtRQUMxRSw0RkFBNEY7UUFDNUYsd0NBQXdDO1FBQ3hDLG9EQUFvRDtRQUNwRCx3REFBd0Q7UUFDeEQsMkJBQTJCO1FBQzNCLDRDQUE0QztRQUM1QyxJQUFJLFFBQVE7WUFBRSxRQUFRLEVBQUUsQ0FBQztRQUN6QiwwQkFBMEI7UUFDMUIsa0RBQWtEO1FBQ2xELHFCQUFxQjtRQUNyQixNQUFNO1FBQ04sSUFBSTtJQUNSLENBQUM7SUFFYSxzQkFBa0IsR0FBaEM7UUFDSSw0Q0FBNEM7UUFDNUMsdUNBQXVDO1FBQ3ZDLG1FQUFtRTtRQUNuRSxtREFBbUQ7UUFDbkQsK0RBQStEO1FBQy9ELGdEQUFnRDtRQUNoRCxrQ0FBa0M7UUFDbEMsbUNBQW1DO1FBQ25DLGdEQUFnRDtRQUNoRCxjQUFjO1FBQ2QsUUFBUTtRQUNSLElBQUk7SUFDUixDQUFDO0lBRWEsMEJBQXNCLEdBQXBDO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDckMsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUM7SUFDdkMsQ0FBQztJQUVtQixhQUFTLEdBQTdCLFVBQThCLFFBQWdCO3VDQUFHLE9BQU87Ozs0QkFDcEQscUJBQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUE3QyxTQUE2QyxDQUFDO3dCQUM5QyxzQkFBTyxTQUFTLENBQUMsV0FBVyxDQUFDO2dDQUN6QixNQUFNLEVBQUUsUUFBUTtnQ0FDaEIsUUFBUSxFQUFFLFdBQVc7Z0NBQ3JCLEdBQUcsRUFBRSxXQUFXO2dDQUNoQixLQUFLLEVBQUUsZUFBWTtnQ0FDbkIsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEdBQUcsc0NBQXNDO2dDQUMzRCxRQUFRLEVBQUUsV0FBVzs2QkFDeEIsQ0FBQyxFQUFDOzs7O0tBQ047SUFFbUIsY0FBVSxHQUE5Qjt1Q0FBa0MsT0FBTzs7Z0JBQ3JDLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtvQkFDcEMsc0JBQU8sU0FBUyxDQUFDLFVBQVUsQ0FBQzs0QkFDeEIsTUFBTSxFQUFFLE9BQU87NEJBQ2YsS0FBSyxFQUFFLGVBQVk7NEJBQ25CLElBQUksRUFBRSwwQkFBMEI7eUJBQ25DLENBQUMsRUFBQztpQkFDTjtnQkFDRCxzQkFBTyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUM7OztLQUM1QjtJQUVhLFVBQU0sR0FBcEIsVUFBcUIsT0FBOEIsRUFBRSxLQUE0QjtRQUM3RSxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxDQUFDLE9BQU87aUJBQ1osV0FBVyxFQUFFO2lCQUNiLElBQUksQ0FBQztnQkFDRixPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNuQyxTQUFTLENBQUMsV0FBVyxDQUFDO29CQUNsQixNQUFNLEVBQUUsUUFBUTtvQkFDaEIsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLEdBQUcsRUFBRSxXQUFXO29CQUNoQixLQUFLLEVBQUUsZUFBWTtvQkFDbkIsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEdBQUcsNENBQTRDO29CQUNqRSxRQUFRLEVBQUUsV0FBVztpQkFDeEIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFYSxpQkFBYSxHQUEzQixVQUE0QixHQUFXO1FBQ25DLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRWEsY0FBVSxHQUF4QixVQUF5QixJQUFZO1FBQ2pDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25ELEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUNuRyxDQUFDO0lBRWEsZ0JBQVksR0FBMUI7UUFDSSxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2RCxFQUFFLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRWEsbUJBQWUsR0FBN0IsVUFBOEIsR0FBVztRQUNyQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxJQUFJLFVBQVUsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1lBQzVCLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUMxQixJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUk7Z0JBQ2QsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNO2dCQUNsQix3QkFBd0IsRUFBRSxVQUFVLENBQUMsUUFBUSxFQUFFO2dCQUMvQyxpQkFBaUIsRUFBRSxHQUFHO2FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ0osRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUMxQixDQUFDLEVBQUUsVUFBVSxNQUFNO2dCQUNmLEVBQUUsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRWMsdUJBQW1CLEdBQWxDLFVBQW1DLFNBQXdCO1FBQ3ZELElBQUksQ0FBQyxTQUFTO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDNUIsSUFBSSxPQUFPLFNBQVMsSUFBSSxRQUFRLEVBQUU7WUFDOUIsU0FBUyxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsSUFBTSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN2QixJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDOUQsSUFBSSxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ3hELElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUN0RCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRWEsU0FBSyxHQUFuQjtRQUNJLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZFLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBVixDQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFYLENBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBRWEsWUFBUSxHQUF0QixVQUF1QixTQUFpQixFQUFFLFVBQW1CLEVBQUUsVUFBbUI7UUFDOUUsSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFNO1FBQ3RCLDRDQUE0QztRQUM1QyxJQUFJO1lBQ0EsK0RBQStEO1lBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7U0FDdkQ7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztTQUN2RDtRQUNELHlEQUF5RDtRQUN6RCxJQUFJO0lBQ1IsQ0FBQztJQUVjLGlCQUFhLEdBQTVCLFVBQTZCLEtBQUs7UUFDOUIsbURBQW1EO1FBRW5ELElBQU0sSUFBSSxHQUFHLEVBQUUsS0FBSyxPQUFBLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUNsRixPQUFPLEtBQUssQ0FBSSxHQUFHLENBQUMsTUFBTSxTQUFJLEdBQUcsQ0FBQyxLQUFPLEVBQ3JDO1lBQ0ksTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUU7WUFDL0MsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1NBQzdCLENBQ0osQ0FBQTtJQUNMLENBQUM7SUFFYSxlQUFXLEdBQXpCO1FBQ0ksSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxHQUFHLElBQUk7WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsR0FBRyxHQUFHO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFDM0IsSUFBSSxDQUFDLEdBQUcsR0FBRztZQUFFLE9BQU8sTUFBTSxDQUFDO1FBQzNCLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUE3Vk0sZUFBVyxHQUFZLEtBQUssQ0FBQztJQUM3QixRQUFJLEdBQVcsS0FBSyxDQUFDO0lBQ3JCLFVBQU0sR0FBVyxDQUFDLENBQUM7SUFDbkIsWUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixZQUFRLEdBQVcsSUFBSSxDQUFDO0lBQ3hCLFVBQU0sR0FBVyxPQUFPLENBQUM7SUFDekIsU0FBSyxHQUFXLGlDQUFpQyxDQUFDO0lBQ2xELFdBQU8sR0FBWSxLQUFLLENBQUM7SUFDekIsMEJBQXNCLEdBQXlCLElBQUksQ0FBQztJQUNwRCx5QkFBcUIsR0FBeUIsSUFBSSxDQUFDO0lBQ25ELG1CQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLGlCQUFhLEdBQVcsQ0FBQyxDQUFDO0lBQzFCLHFCQUFpQixHQUFZLEtBQUssQ0FBQztJQUNuQyxVQUFNLEdBQUcsMENBQTBDLENBQUEsQ0FBQSwwQkFBMEI7SUFDN0UsU0FBSyxHQUFHLDBCQUEwQixDQUFBLENBQUEsNEJBQTRCO0lBQzlELFVBQU0sR0FBRyxpQkFBaUIsQ0FBQTtJQStVckMsVUFBQztDQS9WRCxBQStWQyxJQUFBO2tCQS9Wb0IsR0FBRyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMZWFkZXJib2FyZCBmcm9tIFwiLi9MZWFkZXJib2FyZFwiO1xuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCBpbWFnZV9iYXNlNjQgZnJvbSAnLi9pbWFnZSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFwaSB7XG4gICAgc3RhdGljIGluaXRpYWxpemVkOiBib29sZWFuID0gZmFsc2U7XG4gICAgc3RhdGljIGNvaW46IG51bWJlciA9IDEwMDAwO1xuICAgIHN0YXRpYyB0aWNrZXQ6IG51bWJlciA9IDA7XG4gICAgc3RhdGljIHVzZXJuYW1lOiBzdHJpbmcgPSAnRE0nO1xuICAgIHN0YXRpYyBwbGF5ZXJJZDogc3RyaW5nID0gJ0RNJztcbiAgICBzdGF0aWMgbG9jYWxlOiBzdHJpbmcgPSAndmlfVk4nO1xuICAgIHN0YXRpYyBwaG90bzogc3RyaW5nID0gJ2h0dHBzOi8vaS5pbWd1ci5jb20vRlVPeHM0My5qcGcnO1xuICAgIHN0YXRpYyBpc0RpcnR5OiBib29sZWFuID0gZmFsc2U7XG4gICAgc3RhdGljIHByZWxvYWRlZFJld2FyZGVkVmlkZW86IEZCSW5zdGFudC5BZEluc3RhbmNlID0gbnVsbDtcbiAgICBzdGF0aWMgcHJlbG9hZGVkSW50ZXJzdGl0aWFsOiBGQkluc3RhbnQuQWRJbnN0YW5jZSA9IG51bGw7XG4gICAgc3RhdGljIGNhblN1YnNjcmliZUJvdCA9IGZhbHNlO1xuICAgIHN0YXRpYyBkYWlseUJvbnVzRGF5OiBudW1iZXIgPSAwO1xuICAgIHN0YXRpYyBkYWlseUJvbnVzQ2xhaW1lZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHN0YXRpYyBsYl9hcGkgPSAnaHR0cHM6Ly9sZWFkZXJib2FyZC5hZHBpYS5jb20udm4vaW5zdGFudCcvLydodHRwczovL2xiLmRvem8udm4vYXBpJ1xuICAgIHN0YXRpYyBsYl9pZCA9ICdhMzMwZTUwNTQ4MDRmN2ZiNzNiZDFmOTknLy8nNWY3MTQ0MTU2MzBiOWI5ZmY4MTQ2ZjE1J1xuICAgIHN0YXRpYyBJRF9BUFAgPSAnNDQwMjAxNjg3MTI0NTk4J1xuXG4gICAgcHVibGljIHN0YXRpYyBpbml0QXN5bmMoY2FsbGJhY2s6IChib251czogYm9vbGVhbiwgZGF5OiBudW1iZXIpID0+IHZvaWQsIGxlYWRlcmJvYXJkOiBMZWFkZXJib2FyZCkge1xuICAgICAgICBpZiAoQXBpLmluaXRpYWxpemVkKSB7XG4gICAgICAgICAgICBBcGkubG9hZExlYWRlcmJvYXJkKGxlYWRlcmJvYXJkKTtcbiAgICAgICAgICAgIHJldHVybiBjYWxsYmFjayhmYWxzZSwgQXBpLmRhaWx5Qm9udXNEYXkpOyAvLyBkYWlseV9ib251c19jbGFpbWVkX2F0LCBkYWlseV9ib251c19kYXlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghQXBpLmluaXRpYWxpemVkICYmIHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEFwaS5pbml0aWFsaXplZCA9IHRydWU7XG4gICAgICAgICAgICBBcGkucGhvdG8gPSBGQkluc3RhbnQucGxheWVyLmdldFBob3RvKCk7XG4gICAgICAgICAgICBBcGkudXNlcm5hbWUgPSBGQkluc3RhbnQucGxheWVyLmdldE5hbWUoKTtcbiAgICAgICAgICAgIEFwaS5wbGF5ZXJJZCA9IEZCSW5zdGFudC5wbGF5ZXIuZ2V0SUQoKTtcbiAgICAgICAgICAgIEFwaS5sb2NhbGUgPSBGQkluc3RhbnQuZ2V0TG9jYWxlKCk7XG5cbiAgICAgICAgICAgIEFwaS5nZXRQbGF5ZXJEYXRhKGNhbGxiYWNrKTtcbiAgICAgICAgICAgIEFwaS5sb2FkTGVhZGVyYm9hcmQobGVhZGVyYm9hcmQpO1xuICAgICAgICAgICAgQXBpLnN1YnNjcmliZUJvdCgpO1xuICAgICAgICAgICAgQXBpLmNyZWF0ZVNob3J0Y3V0KCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBBcGkudGlja2V0ID0gMTA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0RheSA9IDA7XG4gICAgICAgICAgICAvLyBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIEFwaS5sb2NhbGUgPSAndmlfVk4nO1xuICAgICAgICAgICAgLy8gQXBpLmxvY2FsZSA9ICd0aF9USCc7XG4gICAgICAgICAgICAvLyBjYWxsYmFjayh0cnVlLCBBcGkuZGFpbHlCb251c0RheSk7XG4gICAgICAgICAgICBBcGkubG9hZExlYWRlcmJvYXJkKGxlYWRlcmJvYXJkKTtcbiAgICAgICAgICAgIC8vIH0sIDUwMDApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgZ2V0UGxheWVyRGF0YShjYWxsYmFjazogKGJvbnVzOiBib29sZWFuLCBkYXk6IG51bWJlcikgPT4gdm9pZCkge1xuICAgICAgICBGQkluc3RhbnQucGxheWVyLmdldERhdGFBc3luYyhbJ2NvaW4nLCAndGlja2V0JywgJ2RhaWx5X2JvbnVzX2NsYWltZWRfYXQnLCAnZGFpbHlfYm9udXNfZGF5J10pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGNjLmxvZygnZGF0YSBpcyBsb2FkZWQnKTtcbiAgICAgICAgICAgIEFwaS5jb2luID0gZGF0YVsnY29pbiddO1xuICAgICAgICAgICAgaWYgKEFwaS5jb2luID09IHVuZGVmaW5lZCB8fCBBcGkuY29pbiA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgQXBpLmNvaW4gPSBDb25maWcuZGVmYXVsdENvaW47XG4gICAgICAgICAgICAgICAgQXBpLmlzRGlydHkgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgQXBpLnRpY2tldCA9IGRhdGFbJ3RpY2tldCddIHx8IDA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0RheSA9IGRhdGFbJ2RhaWx5X2JvbnVzX2RheSddIHx8IDA7XG4gICAgICAgICAgICBBcGkuZGFpbHlCb251c0NsYWltZWQgPSAhQXBpLmRhaWx5Qm9udXNDbGFpbWFibGUoZGF0YVsnZGFpbHlfYm9udXNfY2xhaW1lZF9hdCddKTtcbiAgICAgICAgICAgIGNhbGxiYWNrKCFBcGkuZGFpbHlCb251c0NsYWltZWQsIEFwaS5kYWlseUJvbnVzRGF5KTtcbiAgICAgICAgICAgIGlmIChBcGkuaXNEaXJ0eSkge1xuICAgICAgICAgICAgICAgIEFwaS5mbHVzaCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBzdWJzY3JpYmVCb3QoKSB7XG4gICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuY2FuU3Vic2NyaWJlQm90QXN5bmMoKS50aGVuKGZ1bmN0aW9uIChjYW5fc3Vic2NyaWJlKSB7XG4gICAgICAgICAgICBjYy5sb2coJ3N1YnNjcmliZUJvdEFzeW5jJywgY2FuX3N1YnNjcmliZSk7XG4gICAgICAgICAgICBBcGkuY2FuU3Vic2NyaWJlQm90ID0gY2FuX3N1YnNjcmliZS52YWx1ZU9mKCk7XG4gICAgICAgICAgICAvLyBUaGVuIHdoZW4geW91IHdhbnQgdG8gYXNrIHRoZSBwbGF5ZXIgdG8gc3Vic2NyaWJlXG4gICAgICAgICAgICBpZiAoQXBpLmNhblN1YnNjcmliZUJvdCkge1xuICAgICAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc3Vic2NyaWJlQm90QXN5bmMoKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAvLyBQbGF5ZXIgaXMgc3Vic2NyaWJlZCB0byB0aGUgYm90XG4gICAgICAgICAgICAgICAgKS5jYXRjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBIYW5kbGUgc3Vic2NyaXB0aW9uIGZhaWx1cmVcbiAgICAgICAgICAgICAgICAgICAgY2MubG9nKCdzdWJzY3JpYmVCb3RBc3luYycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KS5jYXRjaCgoZSkgPT4ge1xuICAgICAgICAgICAgY2MubG9nKCdzdWJzY3JpYmVCb3RBc3luYycsIGUpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBjcmVhdGVTaG9ydGN1dCgpIHtcbiAgICAgICAgRkJJbnN0YW50LmNhbkNyZWF0ZVNob3J0Y3V0QXN5bmMoKVxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGNhbkNyZWF0ZVNob3J0Y3V0KSB7XG4gICAgICAgICAgICAgICAgaWYgKGNhbkNyZWF0ZVNob3J0Y3V0KSB7XG4gICAgICAgICAgICAgICAgICAgIEZCSW5zdGFudC5jcmVhdGVTaG9ydGN1dEFzeW5jKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTaG9ydGN1dCBjcmVhdGVkXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTaG9ydGN1dCBub3QgY3JlYXRlZFxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBsb2FkTGVhZGVyYm9hcmQobGVhZGVyYm9hcmQ6IExlYWRlcmJvYXJkKSB7XG4gICAgICAgIGZldGNoKGAke0FwaS5sYl9hcGl9LyR7QXBpLmxiX2lkfWApLnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKS50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuXG4gICAgICAgICAgICByZXM/LmVudHJpZXM/Lmxlbmd0aCAmJiByZXMuZW50cmllcy5tYXAoKGUsIGkpID0+IHtcbiAgICAgICAgICAgICAgICBsZWFkZXJib2FyZC5yZW5kZXIoXG4gICAgICAgICAgICAgICAgICAgIGkgKyAxLFxuICAgICAgICAgICAgICAgICAgICBlLnVzZXJuYW1lLFxuICAgICAgICAgICAgICAgICAgICBlLnNjb3JlLFxuICAgICAgICAgICAgICAgICAgICBlLnBob3RvLFxuICAgICAgICAgICAgICAgICAgICBlLnVzZXJfaWQsXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgIC8vIGZvciAobGV0IGkgPSAwOyBpIDwgcmVzLmVudHJpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIC8vICAgICBjb25zdCBlID0gcmVzLmVudHJpZXNbaV07XG4gICAgICAgICAgICAvLyAgICAgbGVhZGVyYm9hcmQucmVuZGVyKFxuICAgICAgICAgICAgLy8gICAgICAgICBpICsgMSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS51c2VybmFtZSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS5zY29yZSxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS5waG90byxcbiAgICAgICAgICAgIC8vICAgICAgICAgZS51c2VyX2lkLFxuICAgICAgICAgICAgLy8gICAgICk7XG4gICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICBsZWFkZXJib2FyZC5vbkxvYWRDb21wbGV0ZSgpO1xuICAgICAgICB9KS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBwcmVsb2FkUmV3YXJkZWRWaWRlbyhjYWxsYmFjazogKCkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICAgICAgLy8gaWYgKCF3aW5kb3cuaGFzT3duUHJvcGVydHkoJ0ZCSW5zdGFudCcpKSB7XG4gICAgICAgIGlmIChjYWxsYmFjaykgY2FsbGJhY2soKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgICAvLyB9XG4gICAgICAgIGlmIChBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyB8fCAhQ29uZmlnLnJld2FyZF92aWRlbykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIEZCSW5zdGFudC5nZXRSZXdhcmRlZFZpZGVvQXN5bmMoQ29uZmlnLnJld2FyZF92aWRlbykudGhlbihmdW5jdGlvbiAocmV3YXJkZWQpIHtcbiAgICAgICAgICAgIC8vIExvYWQgdGhlIEFkIGFzeW5jaHJvbm91c2x5XG4gICAgICAgICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IHJld2FyZGVkO1xuICAgICAgICAgICAgcmV0dXJuIEFwaS5wcmVsb2FkZWRSZXdhcmRlZFZpZGVvLmxvYWRBc3luYygpO1xuICAgICAgICB9KS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgIGNjLmxvZygnUmV3YXJkZWQgdmlkZW8gcHJlbG9hZGVkJyk7XG4gICAgICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIH0pLmNhdGNoKChlKSA9PiB7XG4gICAgICAgICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IG51bGw7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUubWVzc2FnZSk7XG4gICAgICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2hvd1Jld2FyZGVkVmlkZW8oc3VjY2VzczogKCkgPT4gdm9pZCwgZXJyb3I6IChtc2c6IHN0cmluZykgPT4gdm9pZCkge1xuICAgICAgICAvLyBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgLy8gICAgIHJldHVybiBlcnJvcigncnJyJyk7XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gaWYgKCFDb25maWcucmV3YXJkX3ZpZGVvKSB7XG4gICAgICAgIHN1Y2Nlc3MoKTtcbiAgICAgICAgLy8gfVxuICAgICAgICAvLyBpZiAoIUFwaS5wcmVsb2FkZWRSZXdhcmRlZFZpZGVvKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmVycm9yKCdhZCBub3QgbG9hZCB5ZXQnKTtcbiAgICAgICAgLy8gICAgIHJldHVybiBlcnJvcignYWQgbm90IGxvYWQgeWV0Jyk7XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gQXBpLnByZWxvYWRlZFJld2FyZGVkVmlkZW8uc2hvd0FzeW5jKCkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vICAgICBzdWNjZXNzKCk7XG4gICAgICAgIC8vICAgICBBcGkucHJlbG9hZGVkUmV3YXJkZWRWaWRlbyA9IG51bGw7XG4gICAgICAgIC8vICAgICBjYy5sb2coJ1Jld2FyZGVkIHZpZGVvIHdhdGNoZWQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIC8vIH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIC8vICAgICBlcnJvcihlLm1lc3NhZ2UpO1xuICAgICAgICAvLyAgICAgQXBpLnByZWxvYWRlZFJld2FyZGVkVmlkZW8gPSBudWxsO1xuICAgICAgICAvLyAgICAgY29uc29sZS5lcnJvcihlLm1lc3NhZ2UpO1xuICAgICAgICAvLyB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHByZWxvYWRJbnRlcnN0aXRpYWxBZChjYWxsYmFjazogKCkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICAgICAgLy8gaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykgJiYgIUFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgIEZCSW5zdGFudC5nZXRJbnRlcnN0aXRpYWxBZEFzeW5jKENvbmZpZy5pbnRlcnRpdGFsX2FkcykudGhlbihmdW5jdGlvbihpbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgICAgICAvLyBMb2FkIHRoZSBBZCBhc3luY2hyb25vdXNseVxuICAgICAgICAvLyAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBpbnRlcnN0aXRpYWw7XG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwubG9hZEFzeW5jKCk7XG4gICAgICAgIC8vICAgICB9KS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyAgICAgICAgIGNjLmxvZygnSW50ZXJzdGl0aWFsIHByZWxvYWRlZCcpO1xuICAgICAgICBpZiAoY2FsbGJhY2spIGNhbGxiYWNrKCk7XG4gICAgICAgIC8vIH0pLmNhdGNoKGZ1bmN0aW9uKGVycil7XG4gICAgICAgIC8vICAgICBjYy5lcnJvcignSW50ZXJzdGl0aWFsIGZhaWxlZCB0byBwcmVsb2FkJyk7XG4gICAgICAgIC8vICAgICBjYy5lcnJvcihlcnIpO1xuICAgICAgICAvLyB9KTtcbiAgICAgICAgLy8gfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2hvd0ludGVyc3RpdGlhbEFkKCkge1xuICAgICAgICAvLyBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAvLyAgICAgaWYgKEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwpIHtcbiAgICAgICAgLy8gICAgICAgICBBcGkucHJlbG9hZGVkSW50ZXJzdGl0aWFsLnNob3dBc3luYygpLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAvLyAgICAgICAgICAgICAvLyBQZXJmb3JtIHBvc3QtYWQgc3VjY2VzcyBvcGVyYXRpb25cbiAgICAgICAgLy8gICAgICAgICAgICAgY2MubG9nKCdJbnRlcnN0aXRpYWwgYWQgZmluaXNoZWQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIC8vICAgICAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBudWxsO1xuICAgICAgICAvLyAgICAgICAgIH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIGNjLmVycm9yKGUubWVzc2FnZSk7XG4gICAgICAgIC8vICAgICAgICAgICAgIEFwaS5wcmVsb2FkZWRJbnRlcnN0aXRpYWwgPSBudWxsO1xuICAgICAgICAvLyAgICAgICAgIH0pO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBpc0ludGVyc3RpdGlhbEFkTG9hZGVkKCk6IGJvb2xlYW4ge1xuICAgICAgICBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gISFBcGkucHJlbG9hZGVkSW50ZXJzdGl0aWFsO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgY2hhbGxlbmdlKHBsYXllcklkOiBzdHJpbmcpOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgYXdhaXQgRkJJbnN0YW50LmNvbnRleHQuY3JlYXRlQXN5bmMocGxheWVySWQpO1xuICAgICAgICByZXR1cm4gRkJJbnN0YW50LnVwZGF0ZUFzeW5jKHtcbiAgICAgICAgICAgIGFjdGlvbjogJ0NVU1RPTScsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJ3BsYXlfdHVybicsXG4gICAgICAgICAgICBjdGE6ICdDaMahaSBuZ2F5JyxcbiAgICAgICAgICAgIGltYWdlOiBpbWFnZV9iYXNlNjQsXG4gICAgICAgICAgICB0ZXh0OiBBcGkudXNlcm5hbWUgKyAnIMSRYW5nIHRow6FjaCDEkeG6pXUgYuG6oW4uIE5o4bqlbiBjaMahaSBuZ2F5IScsXG4gICAgICAgICAgICBzdHJhdGVneTogJ0lNTUVESUFURSdcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBzaGFyZUFzeW5jKCk6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgcmV0dXJuIEZCSW5zdGFudC5zaGFyZUFzeW5jKHtcbiAgICAgICAgICAgICAgICBpbnRlbnQ6ICdTSEFSRScsXG4gICAgICAgICAgICAgICAgaW1hZ2U6IGltYWdlX2Jhc2U2NCxcbiAgICAgICAgICAgICAgICB0ZXh0OiAnVGnhur9uIGxpw6puIG1p4buBbiBuYW0gMjAyMCEnLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgaW52aXRlKHN1Y2Nlc3M6IChtc2c6IHN0cmluZykgPT4gdm9pZCwgZXJyb3I6IChyZWFzb246IGFueSkgPT4gdm9pZCkge1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgRkJJbnN0YW50LmNvbnRleHRcbiAgICAgICAgICAgICAgICAuY2hvb3NlQXN5bmMoKVxuICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhGQkluc3RhbnQuY29udGV4dC5nZXRJRCgpKTtcbiAgICAgICAgICAgICAgICAgICAgRkJJbnN0YW50LnVwZGF0ZUFzeW5jKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ0NVU1RPTScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogJ3BsYXlfdHVybicsXG4gICAgICAgICAgICAgICAgICAgICAgICBjdGE6ICdDaMahaSBuZ2F5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlOiBpbWFnZV9iYXNlNjQsXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBBcGkudXNlcm5hbWUgKyAnIG3hu51pIGLhuqFuIGPDuW5nIGNoxqFpIGdhbWUgdGnhur9uIGxpw6puIG1p4buBbiBuYW0nLFxuICAgICAgICAgICAgICAgICAgICAgICAgc3RyYXRlZ3k6ICdJTU1FRElBVEUnXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pLmNhdGNoKGVycm9yKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHN1Y2Nlc3MoJycpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBjb2luSW5jcmVtZW50KHZhbDogbnVtYmVyKSB7XG4gICAgICAgIEFwaS51cGRhdGVDb2luKEFwaS5jb2luICsgdmFsKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHVwZGF0ZUNvaW4oY29pbjogbnVtYmVyKSB7XG4gICAgICAgIEFwaS5jb2luID0gY29pbjtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgY29pbjogQXBpLmNvaW4gfSkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY2MubG9nKCdkYXRhIGlzIHNldCcpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLnNldFNjb3JlQXN5bmMoQXBpLmNvaW4pLnRoZW4oKCkgPT4gY2MubG9nKCdTY29yZSBzYXZlZCcpKS5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHVwZGF0ZVRpY2tldCgpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgdGlja2V0OiBBcGkudGlja2V0IH0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNjLmxvZygndGlja2V0IGlzIHNldCcpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGNsYWltRGFpbHlCb251cyhkYXk6IG51bWJlcikge1xuICAgICAgICBBcGkuZGFpbHlCb251c0NsYWltZWQgPSB0cnVlO1xuICAgICAgICBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICAgICAgbGV0IGNsYWltZWRfYXQgPSBuZXcgRGF0ZSgpO1xuICAgICAgICAgICAgRkJJbnN0YW50LnBsYXllci5zZXREYXRhQXN5bmMoe1xuICAgICAgICAgICAgICAgIGNvaW46IEFwaS5jb2luLFxuICAgICAgICAgICAgICAgIHRpY2tldDogQXBpLnRpY2tldCxcbiAgICAgICAgICAgICAgICAnZGFpbHlfYm9udXNfY2xhaW1lZF9hdCc6IGNsYWltZWRfYXQudG9TdHJpbmcoKSxcbiAgICAgICAgICAgICAgICAnZGFpbHlfYm9udXNfZGF5JzogZGF5XG4gICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjYy5sb2coJ2RhdGEgaXMgc2V0Jyk7XG4gICAgICAgICAgICB9LCBmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgICAgICAgICAgY2MubG9nKCdkYXRhIGlzIG5vdCBzZXQnLCByZWFzb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgQXBpLmRhaWx5Qm9udXNEYXkrKztcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBkYWlseUJvbnVzQ2xhaW1hYmxlKGNsYWltZWRBdDogc3RyaW5nIHwgRGF0ZSkge1xuICAgICAgICBpZiAoIWNsYWltZWRBdCkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmICh0eXBlb2YgY2xhaW1lZEF0ID09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICBjbGFpbWVkQXQgPSBuZXcgRGF0ZShjbGFpbWVkQXQpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IG5vdyA9IG5ldyBEYXRlKCk7XG4gICAgICAgIGlmIChub3cuZ2V0RnVsbFllYXIoKSAhPSBjbGFpbWVkQXQuZ2V0RnVsbFllYXIoKSkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmIChub3cuZ2V0TW9udGgoKSAhPSBjbGFpbWVkQXQuZ2V0TW9udGgoKSkgcmV0dXJuIHRydWU7XG4gICAgICAgIGlmIChub3cuZ2V0RGF0ZSgpICE9IGNsYWltZWRBdC5nZXREYXRlKCkpIHJldHVybiB0cnVlO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBmbHVzaCgpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHtcbiAgICAgICAgICAgIEZCSW5zdGFudC5wbGF5ZXIuc2V0RGF0YUFzeW5jKHsgY29pbjogQXBpLmNvaW4sIHRpY2tldDogQXBpLnRpY2tldCB9KS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjYy5sb2coJ2RhdGEgaXMgc2V0Jyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBBcGkuc2V0U2NvcmVBc3luYyhBcGkuY29pbikudGhlbihyZXMgPT4gcmVzLmpzb24oKSkudGhlbihyZXMgPT4gY2MubG9nKHJlcykpLmNhdGNoKGVyciA9PiBjb25zb2xlLmVycm9yKGVycikpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgbG9nRXZlbnQoZXZlbnROYW1lOiBzdHJpbmcsIHZhbHVlVG9TdW0/OiBudW1iZXIsIHBhcmFtZXRlcnM/OiBPYmplY3QpIHtcbiAgICAgICAgaWYgKCFldmVudE5hbWUpIHJldHVyblxuICAgICAgICAvLyBpZiAod2luZG93Lmhhc093blByb3BlcnR5KCdGQkluc3RhbnQnKSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gd2luZG93LmZpcmViYXNlLmFuYWx5dGljcygpLmxvZ0V2ZW50KGV2ZW50TmFtZSwgcGFyYW1ldGVycyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Jyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhldmVudE5hbWUsIHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2FuYWx5dGljIGVycm9yJywgZXJyb3IpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PScpO1xuICAgICAgICB9XG4gICAgICAgIC8vIEZCSW5zdGFudC5sb2dFdmVudChldmVudE5hbWUsIHZhbHVlVG9TdW0sIHBhcmFtZXRlcnMpO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgc2V0U2NvcmVBc3luYyhzY29yZSkge1xuICAgICAgICAvLyBpZiAoIXdpbmRvdy5oYXNPd25Qcm9wZXJ0eSgnRkJJbnN0YW50JykpIHJldHVybjtcblxuICAgICAgICBjb25zdCBkYXRhID0geyBzY29yZSwgdXNlcklkOiBBcGkucGxheWVySWQsIG5hbWU6IEFwaS51c2VybmFtZSwgcGhvdG86IEFwaS5waG90byB9XG4gICAgICAgIHJldHVybiBmZXRjaChgJHtBcGkubGJfYXBpfS8ke0FwaS5sYl9pZH1gLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyB9LFxuICAgICAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGRhdGEpXG4gICAgICAgICAgICB9XG4gICAgICAgIClcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHJhbmRvbUJvbnVzKCkge1xuICAgICAgICBjb25zdCByID0gTWF0aC5yYW5kb20oKTtcbiAgICAgICAgaWYgKHIgPCAwLjA1KSByZXR1cm4gNDAwMDAwO1xuICAgICAgICBpZiAociA8IDAuMikgcmV0dXJuIDMwMDAwMDtcbiAgICAgICAgaWYgKHIgPCAwLjUpIHJldHVybiAyMDAwMDA7XG4gICAgICAgIHJldHVybiAxMDAwMDA7XG4gICAgfVxufSJdfQ==