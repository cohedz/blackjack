
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/CommonCard.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1351esCO5tMRYPcn390B3zj', 'CommonCard');
// Scripts/CommonCard.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonCard = /** @class */ (function () {
    function CommonCard() {
        this.totalCards = 0;
        this.cards = [];
        this.combat = [];
        this.latest = null;
    }
    CommonCard.prototype.getPosition = function () {
        if (this.cards.length == 0)
            return cc.Vec2.ZERO;
        var sx = Math.random() * 100 - 50;
        var sy = Math.random() * 100 - 50;
        return cc.v2(sx, sy + 50);
    };
    CommonCard.prototype.reset = function () {
        this.totalCards = 0;
        this.cards = [];
        this.latest = null;
        if (this.combat.length > 0) {
            this.combat = [];
        }
    };
    CommonCard.prototype.push = function (cards) {
        this.totalCards += cards.count();
        this.cards.push(cards);
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        this.latest = cards;
    };
    CommonCard.prototype.isCombatOpen = function () {
        return this.combat.length > 0;
    };
    CommonCard.prototype.pushCombat = function (player, cards) {
        this.combat.push({ player: player, cards: cards });
    };
    CommonCard.prototype.hasCombat = function () {
        if (this.combat.length < 2)
            return false;
        var peek = this.combat[this.combat.length - 1];
        if (peek.cards.highest.rank == 15)
            return false;
        return true;
    };
    CommonCard.prototype.getCombat = function () {
        return this.combat[0].cards;
    };
    CommonCard.prototype.getCombatLength = function () {
        return this.combat.length;
    };
    CommonCard.prototype.getCombatWinner = function () {
        return this.combat[this.combat.length - 1].player;
    };
    CommonCard.prototype.getCombatVictim = function () {
        return this.combat[this.combat.length - 2].player;
    };
    CommonCard.prototype.resetCombat = function () {
        this.combat = [];
    };
    CommonCard.prototype.peek = function () {
        return this.cards[this.cards.length - 1];
    };
    CommonCard.prototype.length = function () {
        return this.cards.length;
    };
    CommonCard.prototype.isEmpty = function () {
        return this.cards.length == 0;
    };
    CommonCard.prototype.nextRound = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            for (var j = this.cards[i].count() - 1; j >= 0; --j) {
                var card = this.cards[i].at(j);
                card.hide();
                card.node.setScale(0.5);
            }
        }
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        if (this.combat.length > 0) {
            this.combat = [];
        }
        this.cards = [];
        this.latest = null;
    };
    CommonCard.prototype.overlapCard = function (cards) {
        for (var i = 0; i < cards.count(); i++) {
            cards.at(i).overlap();
        }
    };
    return CommonCard;
}());
exports.default = CommonCard;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NvbW1vbkNhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRQTtJQUFBO1FBQ0ksZUFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixVQUFLLEdBQXFCLEVBQUUsQ0FBQztRQUM3QixXQUFNLEdBQW9CLEVBQUUsQ0FBQztRQUVyQixXQUFNLEdBQWMsSUFBSSxDQUFDO0lBb0dyQyxDQUFDO0lBbEdHLGdDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDdEIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUV4QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUVsQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsMEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztJQUVELHlCQUFJLEdBQUosVUFBSyxLQUFnQjtRQUNqQixJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxpQ0FBWSxHQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELCtCQUFVLEdBQVYsVUFBVyxNQUFjLEVBQUUsS0FBZ0I7UUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLFFBQUEsRUFBRSxLQUFLLE9BQUEsRUFBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUM7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUN6QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUNoRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELG9DQUFlLEdBQWY7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzlCLENBQUM7SUFFRCxvQ0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUN0RCxDQUFDO0lBRUQsb0NBQWUsR0FBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7SUFDdEQsQ0FBQztJQUVELGdDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQseUJBQUksR0FBSjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsMkJBQU0sR0FBTjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDN0IsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQzNCO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNqQztRQUNELElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELGdDQUFXLEdBQVgsVUFBWSxLQUFnQjtRQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDO0lBQ0wsaUJBQUM7QUFBRCxDQXpHQSxBQXlHQyxJQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENhcmRHcm91cCBmcm9tIFwiLi9DYXJkR3JvdXBcIjtcbmltcG9ydCBQbGF5ZXIgZnJvbSBcIi4vUGxheWVyXCI7XG5cbmludGVyZmFjZSBDYXJkUGxheSB7XG4gICAgcGxheWVyOiBQbGF5ZXI7XG4gICAgY2FyZHM6IENhcmRHcm91cDtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29tbW9uQ2FyZCB7XG4gICAgdG90YWxDYXJkczogbnVtYmVyID0gMDtcbiAgICBjYXJkczogQXJyYXk8Q2FyZEdyb3VwPiA9IFtdO1xuICAgIGNvbWJhdDogQXJyYXk8Q2FyZFBsYXk+ID0gW107XG5cbiAgICBwcml2YXRlIGxhdGVzdDogQ2FyZEdyb3VwID0gbnVsbDtcblxuICAgIGdldFBvc2l0aW9uKCk6IGNjLlZlYzIge1xuICAgICAgICBpZiAodGhpcy5jYXJkcy5sZW5ndGggPT0gMClcbiAgICAgICAgICAgIHJldHVybiBjYy5WZWMyLlpFUk87XG5cbiAgICAgICAgbGV0IHN4ID0gTWF0aC5yYW5kb20oKSAqIDEwMCAtIDUwO1xuICAgICAgICBsZXQgc3kgPSBNYXRoLnJhbmRvbSgpICogMTAwIC0gNTA7XG5cbiAgICAgICAgcmV0dXJuIGNjLnYyKHN4LCBzeSArIDUwKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgdGhpcy50b3RhbENhcmRzID0gMDtcbiAgICAgICAgdGhpcy5jYXJkcyA9IFtdO1xuICAgICAgICB0aGlzLmxhdGVzdCA9IG51bGw7XG4gICAgICAgIGlmICh0aGlzLmNvbWJhdC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLmNvbWJhdCA9IFtdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVzaChjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIHRoaXMudG90YWxDYXJkcyArPSBjYXJkcy5jb3VudCgpO1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZHMpO1xuICAgICAgICBpZiAodGhpcy5sYXRlc3QpIHtcbiAgICAgICAgICAgIHRoaXMub3ZlcmxhcENhcmQodGhpcy5sYXRlc3QpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGF0ZXN0ID0gY2FyZHM7XG4gICAgfVxuXG4gICAgaXNDb21iYXRPcGVuKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXQubGVuZ3RoID4gMDtcbiAgICB9XG5cbiAgICBwdXNoQ29tYmF0KHBsYXllcjogUGxheWVyLCBjYXJkczogQ2FyZEdyb3VwKSB7XG4gICAgICAgIHRoaXMuY29tYmF0LnB1c2goe3BsYXllciwgY2FyZHN9KTtcbiAgICB9XG5cbiAgICBoYXNDb21iYXQoKTogYm9vbGVhbiB7XG4gICAgICAgIGlmICh0aGlzLmNvbWJhdC5sZW5ndGggPCAyKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIGxldCBwZWVrID0gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMV07XG4gICAgICAgIGlmIChwZWVrLmNhcmRzLmhpZ2hlc3QucmFuayA9PSAxNSkgcmV0dXJuIGZhbHNlO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBnZXRDb21iYXQoKTogQ2FyZEdyb3VwIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29tYmF0WzBdLmNhcmRzO1xuICAgIH1cblxuICAgIGdldENvbWJhdExlbmd0aCgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXQubGVuZ3RoO1xuICAgIH1cblxuICAgIGdldENvbWJhdFdpbm5lcigpOiBQbGF5ZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMV0ucGxheWVyO1xuICAgIH1cblxuICAgIGdldENvbWJhdFZpY3RpbSgpOiBQbGF5ZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5jb21iYXRbdGhpcy5jb21iYXQubGVuZ3RoIC0gMl0ucGxheWVyO1xuICAgIH1cblxuICAgIHJlc2V0Q29tYmF0KCkge1xuICAgICAgICB0aGlzLmNvbWJhdCA9IFtdO1xuICAgIH1cblxuICAgIHBlZWsoKTogQ2FyZEdyb3VwIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHNbdGhpcy5jYXJkcy5sZW5ndGggLSAxXTtcbiAgICB9XG5cbiAgICBsZW5ndGgoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgIH1cblxuICAgIGlzRW1wdHkoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmNhcmRzLmxlbmd0aCA9PSAwO1xuICAgIH1cblxuICAgIG5leHRSb3VuZCgpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuY2FyZHMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGogPSB0aGlzLmNhcmRzW2ldLmNvdW50KCkgLSAxOyBqID49IDA7IC0taikge1xuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkc1tpXS5hdChqKTtcbiAgICAgICAgICAgICAgICBjYXJkLmhpZGUoKTtcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUuc2V0U2NhbGUoMC41KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5sYXRlc3QpIHtcbiAgICAgICAgICAgIHRoaXMub3ZlcmxhcENhcmQodGhpcy5sYXRlc3QpO1xuICAgICAgICB9XG4gICAgICAgIGlmKHRoaXMuY29tYmF0Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuY29tYmF0ID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jYXJkcyA9IFtdO1xuICAgICAgICB0aGlzLmxhdGVzdCA9IG51bGw7XG4gICAgfVxuXG4gICAgb3ZlcmxhcENhcmQoY2FyZHM6IENhcmRHcm91cCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNhcmRzLmNvdW50KCk7IGkrKykge1xuICAgICAgICAgICAgY2FyZHMuYXQoaSkub3ZlcmxhcCgpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19