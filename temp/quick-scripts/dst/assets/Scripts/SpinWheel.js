
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/SpinWheel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'daacdf5d5lMhrNHqrN6niHA', 'SpinWheel');
// Scripts/SpinWheel.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var Popup_1 = require("./Popup");
var Toast_1 = require("./Toast");
var Modal_1 = require("./popop/Modal");
var EventKeys_1 = require("./EventKeys");
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SpinWheel = /** @class */ (function (_super) {
    __extends(SpinWheel, _super);
    function SpinWheel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.wheel = null;
        _this.ticket = null;
        _this.popup = null;
        _this.toast = null;
        _this.btnRun = null;
        _this.btnAd = null;
        _this.audioSpinWin = null;
        _this.audioSpinLose = null;
        _this.audioSpinRun = null;
        _this.modal = null;
        _this.won = 0;
        _this.isRunning = false;
        return _this;
    }
    SpinWheel_1 = SpinWheel;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    SpinWheel.prototype.start = function () {
    };
    // update (dt) {}
    SpinWheel.prototype.show = function (won) {
        var playNow = Api_1.default.ticket > 0;
        this.popup.open(this.node, 2);
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
        this.node.active = true;
        this.isRunning = false;
        this.btnAd.node.active = !playNow;
        this.btnRun.node.active = playNow;
        this.won = won;
    };
    SpinWheel.prototype.hide = function () {
        this.popup.close(this.node, 2);
    };
    SpinWheel.prototype.play = function () {
        if (this.isRunning || Api_1.default.ticket <= 0)
            return;
        Api_1.default.ticket--;
        this.updateTicket();
        this.spin();
        Api_1.default.logEvent(EventKeys_1.default.SPIN);
    };
    SpinWheel.prototype.spin = function () {
        util_1.default.playAudio(this.audioSpinRun);
        this.isRunning = true;
        var result = this.randomPrize();
        var action = cc.rotateBy(5, 360 * 10 + result.angle - 30 - (this.wheel.node.angle % 360));
        this.wheel.node.runAction(cc.sequence(action.easing(cc.easeCubicActionOut()), cc.callFunc(this.onCompleted, this, result)));
    };
    SpinWheel.prototype.showVideoAd = function () {
        var _this = this;
        if (this.isRunning)
            return;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_CLICK);
        Api_1.default.showRewardedVideo(function () {
            _this.spin();
        }, function (msg) {
            Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_ERROR);
            _this.modal.show(Language_1.default.getInstance().get('NOVIDEO'));
        });
    };
    SpinWheel.prototype.onCompleted = function (sender, data) {
        var msg = Language_1.default.getInstance().get(data.msg);
        this.toast.show(msg);
        this.onSpinCompleted(data, this.won);
        if (data.id == 1) {
            this.node.runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(this.spin, this)));
        }
        else {
            this.node.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(this.hide, this)));
            util_1.default.playAudio(data.id == 0 ? this.audioSpinLose : this.audioSpinWin);
        }
        // if(Api.ticket == 0) {
        //     this.btnAd.node.active = true;
        //     this.btnRun.node.active = false;
        // }
    };
    SpinWheel.prototype.onDisable = function () {
        this.onSpinHide();
    };
    SpinWheel.prototype.compute = function (id, val) {
        switch (id) {
            case 2: return val;
            case 3: return val * 2;
            case 4: return val * 4;
            case 5: return val * 9;
            default: return 0;
        }
    };
    SpinWheel.prototype.onClose = function () {
        if (this.isRunning)
            return;
        Popup_1.default.instance.close(null, 2);
    };
    SpinWheel.prototype.randomPrize = function () {
        var rate = Math.random() * 100;
        var sample = SpinWheel_1.result_rate;
        for (var i = 0; i < sample.length; i++)
            if (sample[i].rate > rate)
                return sample[i];
        return sample[0];
    };
    SpinWheel.prototype.updateTicket = function () {
        Api_1.default.updateTicket();
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
    };
    var SpinWheel_1;
    //Tỷ lệ vòng quay : X10 ( 5% ) , X5 ( 10% ) , X3 ( 20% ) , THÊM LƯỢT ( 15% ) , MẤT LƯỢT ( 15% ) , X2 (35%)
    SpinWheel.result_rate = [
        { id: 0, angle: 4 * 60, rate: 15, msg: "LUCK" },
        { id: 1, angle: 5 * 60, rate: 30, msg: "TURN" },
        { id: 2, angle: 0 * 60, rate: 65, msg: "X2" },
        { id: 3, angle: 1 * 60, rate: 85, msg: "X3" },
        { id: 4, angle: 2 * 60, rate: 90, msg: "X5" },
        { id: 5, angle: 3 * 60, rate: 100, msg: "X10" },
    ];
    __decorate([
        property(cc.Sprite)
    ], SpinWheel.prototype, "wheel", void 0);
    __decorate([
        property(cc.Label)
    ], SpinWheel.prototype, "ticket", void 0);
    __decorate([
        property(Popup_1.default)
    ], SpinWheel.prototype, "popup", void 0);
    __decorate([
        property(Toast_1.default)
    ], SpinWheel.prototype, "toast", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnRun", void 0);
    __decorate([
        property(cc.Button)
    ], SpinWheel.prototype, "btnAd", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinWin", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinLose", void 0);
    __decorate([
        property({ type: cc.AudioClip })
    ], SpinWheel.prototype, "audioSpinRun", void 0);
    __decorate([
        property(Modal_1.default)
    ], SpinWheel.prototype, "modal", void 0);
    SpinWheel = SpinWheel_1 = __decorate([
        ccclass
    ], SpinWheel);
    return SpinWheel;
}(cc.Component));
exports.default = SpinWheel;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1NwaW5XaGVlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLGtGQUFrRjtBQUNsRix5RkFBeUY7QUFDekYsbUJBQW1CO0FBQ25CLDRGQUE0RjtBQUM1RixtR0FBbUc7QUFDbkcsOEJBQThCO0FBQzlCLDRGQUE0RjtBQUM1RixtR0FBbUc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVuRywrQkFBMEI7QUFDMUIsNkJBQXdCO0FBQ3hCLGlDQUE0QjtBQUM1QixpQ0FBNEI7QUFDNUIsdUNBQWtDO0FBQ2xDLHlDQUFvQztBQUNwQyx1Q0FBa0M7QUFFNUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBdUMsNkJBQVk7SUFBbkQ7UUFBQSxxRUFxSkM7UUFuSkcsV0FBSyxHQUFjLElBQUksQ0FBQztRQUV4QixZQUFNLEdBQWEsSUFBSSxDQUFDO1FBRXhCLFdBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsV0FBSyxHQUFVLElBQUksQ0FBQztRQUVwQixZQUFNLEdBQWMsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFHeEIsa0JBQVksR0FBaUIsSUFBSSxDQUFDO1FBRWxDLG1CQUFhLEdBQWlCLElBQUksQ0FBQztRQUVuQyxrQkFBWSxHQUFpQixJQUFJLENBQUM7UUFFbEMsV0FBSyxHQUFVLElBQUksQ0FBQztRQUtaLFNBQUcsR0FBVyxDQUFDLENBQUM7UUFDaEIsZUFBUyxHQUFZLEtBQUssQ0FBQzs7SUEwSHZDLENBQUM7a0JBckpvQixTQUFTO0lBNEIxQix3QkFBd0I7SUFFeEIsZUFBZTtJQUVmLHlCQUFLLEdBQUw7SUFDQSxDQUFDO0lBRUQsaUJBQWlCO0lBRWpCLHdCQUFJLEdBQUosVUFBSyxHQUFXO1FBQ1osSUFBTSxPQUFPLEdBQUcsYUFBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7SUFDbkIsQ0FBQztJQUVELHdCQUFJLEdBQUo7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCx3QkFBSSxHQUFKO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLGFBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQztZQUFFLE9BQU87UUFFOUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVaLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsd0JBQUksR0FBSjtRQUNJLGNBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRWxDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQ2pDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLENBQUMsRUFDdEMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FDOUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtCQUFXLEdBQVg7UUFBQSxpQkFTQztRQVJHLElBQUksSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQzNCLGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2xELGFBQUcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNsQixLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQyxFQUFFLFVBQUMsR0FBRztZQUNILGFBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ2xELEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDM0QsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sK0JBQVcsR0FBbkIsVUFBb0IsTUFBZSxFQUFFLElBQVM7UUFDMUMsSUFBTSxHQUFHLEdBQUcsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFHLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFDakIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUMvQixDQUFDLENBQUE7U0FDTDthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FDM0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFDakIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUMvQixDQUFDLENBQUM7WUFDSCxjQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDekU7UUFDRCx3QkFBd0I7UUFDeEIscUNBQXFDO1FBQ3JDLHVDQUF1QztRQUN2QyxJQUFJO0lBQ1IsQ0FBQztJQUVELDZCQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7SUFDckIsQ0FBQztJQUVELDJCQUFPLEdBQVAsVUFBUSxFQUFVLEVBQUUsR0FBVztRQUMzQixRQUFPLEVBQUUsRUFBRTtZQUNQLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDbkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDckI7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQzNCLGVBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU8sK0JBQVcsR0FBbkI7UUFDSSxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBQ2pDLElBQUksTUFBTSxHQUFHLFdBQVMsQ0FBQyxXQUFXLENBQUM7UUFDbkMsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO1lBQzlCLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJO2dCQUFFLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hELE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFTyxnQ0FBWSxHQUFwQjtRQUNJLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2RCxDQUFDOztJQUVELDBHQUEwRztJQUNsRixxQkFBVyxHQUFHO1FBQ2xDLEVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUM7UUFDNUMsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUMsRUFBRSxFQUFFLElBQUksRUFBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBQztRQUM1QyxFQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFDO1FBQzFDLEVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUM7UUFDMUMsRUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUMsRUFBRSxFQUFFLElBQUksRUFBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBQztRQUMxQyxFQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFDO0tBQzlDLENBQUE7SUFsSkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs0Q0FDSTtJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNLO0lBRXhCO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzs0Q0FDSTtJQUVwQjtRQURDLFFBQVEsQ0FBQyxlQUFLLENBQUM7NENBQ0k7SUFFcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2Q0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzRDQUNJO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUMsQ0FBQzttREFDRztJQUVsQztRQURDLFFBQVEsQ0FBQyxFQUFDLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFDLENBQUM7b0RBQ0k7SUFFbkM7UUFEQyxRQUFRLENBQUMsRUFBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVMsRUFBQyxDQUFDO21EQUNHO0lBRWxDO1FBREMsUUFBUSxDQUFDLGVBQUssQ0FBQzs0Q0FDSTtJQXJCSCxTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBcUo3QjtJQUFELGdCQUFDO0NBckpELEFBcUpDLENBckpzQyxFQUFFLENBQUMsU0FBUyxHQXFKbEQ7a0JBckpvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vICAtIFtFbmdsaXNoXSBodHRwOi8vd3d3LmNvY29zMmQteC5vcmcvZG9jcy9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBbQ2hpbmVzZV0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC96aC9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gIC0gW0VuZ2xpc2hdIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZy9kb2NzL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIFtDaGluZXNlXSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL3poL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vLyAgLSBbRW5nbGlzaF0gaHR0cDovL3d3dy5jb2NvczJkLXgub3JnL2RvY3MvY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IEFwaSBmcm9tIFwiLi9BcGlcIjtcbmltcG9ydCBQb3B1cCBmcm9tIFwiLi9Qb3B1cFwiO1xuaW1wb3J0IFRvYXN0IGZyb20gXCIuL1RvYXN0XCI7XG5pbXBvcnQgTW9kYWwgZnJvbSBcIi4vcG9wb3AvTW9kYWxcIjtcbmltcG9ydCBFdmVudEtleXMgZnJvbSBcIi4vRXZlbnRLZXlzXCI7XG5pbXBvcnQgTGFuZ3VhZ2UgZnJvbSBcIi4vTGFuZ3VhZ2VcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTcGluV2hlZWwgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgd2hlZWw6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHRpY2tldDogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShQb3B1cClcbiAgICBwb3B1cDogUG9wdXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShUb2FzdClcbiAgICB0b2FzdDogVG9hc3QgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgYnRuUnVuOiBjYy5CdXR0b24gPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5CdXR0b24pXG4gICAgYnRuQWQ6IGNjLkJ1dHRvbiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6IGNjLkF1ZGlvQ2xpcH0pXG4gICAgYXVkaW9TcGluV2luOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7dHlwZTogY2MuQXVkaW9DbGlwfSlcbiAgICBhdWRpb1NwaW5Mb3NlOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuICAgIEBwcm9wZXJ0eSh7dHlwZTogY2MuQXVkaW9DbGlwfSlcbiAgICBhdWRpb1NwaW5SdW46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG4gICAgQHByb3BlcnR5KE1vZGFsKVxuICAgIG1vZGFsOiBNb2RhbCA9IG51bGw7XG5cbiAgICBvblNwaW5IaWRlOiAoKSA9PiB2b2lkO1xuICAgIG9uU3BpbkNvbXBsZXRlZDogKHJlc3VsdDoge2lkOiBudW1iZXIsIGFuZ2xlOiBudW1iZXIsIG1zZzogc3RyaW5nfSwgd29uOiBudW1iZXIpID0+IHZvaWQ7XG5cbiAgICBwcml2YXRlIHdvbjogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIGlzUnVubmluZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHNob3cod29uOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3QgcGxheU5vdyA9IEFwaS50aWNrZXQgPiAwO1xuICAgICAgICB0aGlzLnBvcHVwLm9wZW4odGhpcy5ub2RlLCAyKTtcbiAgICAgICAgdGhpcy50aWNrZXQuc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLnRpY2tldCk7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmlzUnVubmluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLmJ0bkFkLm5vZGUuYWN0aXZlID0gIXBsYXlOb3c7XG4gICAgICAgIHRoaXMuYnRuUnVuLm5vZGUuYWN0aXZlID0gcGxheU5vdztcbiAgICAgICAgdGhpcy53b24gPSB3b247XG4gICAgfVxuXG4gICAgaGlkZSgpIHtcbiAgICAgICAgdGhpcy5wb3B1cC5jbG9zZSh0aGlzLm5vZGUsIDIpO1xuICAgIH1cblxuICAgIHBsYXkoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzUnVubmluZyB8fCBBcGkudGlja2V0IDw9IDApIHJldHVybjtcblxuICAgICAgICBBcGkudGlja2V0LS07XG4gICAgICAgIHRoaXMudXBkYXRlVGlja2V0KCk7XG5cbiAgICAgICAgdGhpcy5zcGluKCk7XG5cbiAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5TUElOKTtcbiAgICB9XG5cbiAgICBzcGluKCkge1xuICAgICAgICB1dGlsLnBsYXlBdWRpbyh0aGlzLmF1ZGlvU3BpblJ1bik7XG5cbiAgICAgICAgdGhpcy5pc1J1bm5pbmcgPSB0cnVlO1xuICAgICAgICBsZXQgcmVzdWx0ID0gdGhpcy5yYW5kb21Qcml6ZSgpO1xuICAgICAgICBsZXQgYWN0aW9uID0gY2Mucm90YXRlQnkoNSwgMzYwICogMTAgKyByZXN1bHQuYW5nbGUgLSAzMCAtICh0aGlzLndoZWVsLm5vZGUuYW5nbGUgJSAzNjApKTtcbiAgICAgICAgdGhpcy53aGVlbC5ub2RlLnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShcbiAgICAgICAgICAgIGFjdGlvbi5lYXNpbmcoY2MuZWFzZUN1YmljQWN0aW9uT3V0KCkpLFxuICAgICAgICAgICAgY2MuY2FsbEZ1bmModGhpcy5vbkNvbXBsZXRlZCwgdGhpcywgcmVzdWx0KVxuICAgICAgICApKTtcbiAgICB9XG5cbiAgICBzaG93VmlkZW9BZCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNSdW5uaW5nKSByZXR1cm47XG4gICAgICAgIEFwaS5sb2dFdmVudChFdmVudEtleXMuUE9QVVBfQURSRVdBUkRfU1BJTl9DTElDSyk7XG4gICAgICAgIEFwaS5zaG93UmV3YXJkZWRWaWRlbygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNwaW4oKTtcbiAgICAgICAgfSwgKG1zZykgPT4ge1xuICAgICAgICAgICAgQXBpLmxvZ0V2ZW50KEV2ZW50S2V5cy5QT1BVUF9BRFJFV0FSRF9TUElOX0VSUk9SKTtcbiAgICAgICAgICAgIHRoaXMubW9kYWwuc2hvdyhMYW5ndWFnZS5nZXRJbnN0YW5jZSgpLmdldCgnTk9WSURFTycpKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uQ29tcGxldGVkKHNlbmRlcjogY2MuTm9kZSwgZGF0YTogYW55KSB7XG4gICAgICAgIGNvbnN0IG1zZyA9IExhbmd1YWdlLmdldEluc3RhbmNlKCkuZ2V0KGRhdGEubXNnKTtcbiAgICAgICAgdGhpcy50b2FzdC5zaG93KG1zZyk7XG4gICAgICAgIHRoaXMub25TcGluQ29tcGxldGVkKGRhdGEsIHRoaXMud29uKTtcbiAgICAgICAgaWYoZGF0YS5pZCA9PSAxKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjMpLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuc3BpbiwgdGhpcylcbiAgICAgICAgICAgICkpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxuICAgICAgICAgICAgICAgIGNjLmRlbGF5VGltZSgwLjUpLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgICAgICkpO1xuICAgICAgICAgICAgdXRpbC5wbGF5QXVkaW8oZGF0YS5pZCA9PSAwID8gdGhpcy5hdWRpb1NwaW5Mb3NlIDogdGhpcy5hdWRpb1NwaW5XaW4pO1xuICAgICAgICB9XG4gICAgICAgIC8vIGlmKEFwaS50aWNrZXQgPT0gMCkge1xuICAgICAgICAvLyAgICAgdGhpcy5idG5BZC5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIC8vICAgICB0aGlzLmJ0blJ1bi5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAvLyB9XG4gICAgfVxuXG4gICAgb25EaXNhYmxlKCkge1xuICAgICAgICB0aGlzLm9uU3BpbkhpZGUoKVxuICAgIH1cblxuICAgIGNvbXB1dGUoaWQ6IG51bWJlciwgdmFsOiBudW1iZXIpIHtcbiAgICAgICAgc3dpdGNoKGlkKSB7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiB2YWw7XG4gICAgICAgICAgICBjYXNlIDM6IHJldHVybiB2YWwgKiAyO1xuICAgICAgICAgICAgY2FzZSA0OiByZXR1cm4gdmFsICogNDtcbiAgICAgICAgICAgIGNhc2UgNTogcmV0dXJuIHZhbCAqIDk7XG4gICAgICAgICAgICBkZWZhdWx0OiByZXR1cm4gMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ2xvc2UoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzUnVubmluZykgcmV0dXJuO1xuICAgICAgICBQb3B1cC5pbnN0YW5jZS5jbG9zZShudWxsLCAyKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJhbmRvbVByaXplKCkge1xuICAgICAgICBjb25zdCByYXRlID0gTWF0aC5yYW5kb20oKSAqIDEwMDtcbiAgICAgICAgbGV0IHNhbXBsZSA9IFNwaW5XaGVlbC5yZXN1bHRfcmF0ZTtcbiAgICAgICAgZm9yKGxldCBpPTA7IGk8IHNhbXBsZS5sZW5ndGg7IGkrKylcbiAgICAgICAgICAgIGlmIChzYW1wbGVbaV0ucmF0ZSA+IHJhdGUpIHJldHVybiBzYW1wbGVbaV07XG4gICAgICAgIHJldHVybiBzYW1wbGVbMF07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1cGRhdGVUaWNrZXQoKSB7XG4gICAgICAgIEFwaS51cGRhdGVUaWNrZXQoKTtcbiAgICAgICAgdGhpcy50aWNrZXQuc3RyaW5nID0gdXRpbC5udW1iZXJGb3JtYXQoQXBpLnRpY2tldCk7XG4gICAgfVxuXG4gICAgLy9U4bu3IGzhu4cgdsOybmcgcXVheSA6IFgxMCAoIDUlICkgLCBYNSAoIDEwJSApICwgWDMgKCAyMCUgKSAsIFRIw4pNIEzGr+G7olQgKCAxNSUgKSAsIE3huqRUIEzGr+G7olQgKCAxNSUgKSAsIFgyICgzNSUpXG4gICAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgcmVzdWx0X3JhdGUgPSBbXG4gICAgICAgIHtpZDogMCwgYW5nbGU6IDQqNjAsIHJhdGU6ICAxNSwgbXNnOiBcIkxVQ0tcIn0sXG4gICAgICAgIHtpZDogMSwgYW5nbGU6IDUqNjAsIHJhdGU6ICAzMCwgbXNnOiBcIlRVUk5cIn0sXG4gICAgICAgIHtpZDogMiwgYW5nbGU6IDAqNjAsIHJhdGU6ICA2NSwgbXNnOiBcIlgyXCJ9LFxuICAgICAgICB7aWQ6IDMsIGFuZ2xlOiAxKjYwLCByYXRlOiAgODUsIG1zZzogXCJYM1wifSxcbiAgICAgICAge2lkOiA0LCBhbmdsZTogMio2MCwgcmF0ZTogIDkwLCBtc2c6IFwiWDVcIn0sXG4gICAgICAgIHtpZDogNSwgYW5nbGU6IDMqNjAsIHJhdGU6IDEwMCwgbXNnOiBcIlgxMFwifSxcbiAgICBdXG59XG4iXX0=