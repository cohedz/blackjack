
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Slider2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '60e38Bi/OJDvpmRhqGFGQN+', 'Slider2');
// Scripts/Slider2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var util_1 = require("./util");
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Slider2 = /** @class */ (function (_super) {
    __extends(Slider2, _super);
    function Slider2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maxValue = null;
        _this.minValue = null;
        _this.value = null;
        _this.fill = null;
        _this.slider = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    Slider2.prototype.onLoad = function () {
        var sliderEventHandler = new cc.Component.EventHandler();
        sliderEventHandler.target = this.node;
        sliderEventHandler.component = "Slider2";
        sliderEventHandler.handler = "_onValueChange";
        this.slider = this.getComponent(cc.Slider);
        this.slider.slideEvents.push(sliderEventHandler);
    };
    Slider2.prototype.start = function () {
        this.minValue.string = util_1.default.numberFormat(Config_1.default.minBet);
        this.maxValue.string = util_1.default.numberFormat(Config_1.default.maxBet);
        this._onValueChange(this.slider, null);
    };
    Slider2.prototype._onValueChange = function (sender, params) {
        var val = this.getValue();
        this.onValueChange(val);
        this.value.string = util_1.default.numberFormat(val);
        var size = this.fill.node.getContentSize();
        size.width = this.node.getContentSize().width * this.slider.progress;
        this.fill.node.setContentSize(size);
    };
    Slider2.prototype.getValue = function () {
        var val = Config_1.default.minBet + (Config_1.default.maxBet - Config_1.default.minBet) * this.slider.progress;
        return Math.round(val / 1000) * 1000;
    };
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "maxValue", void 0);
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "minValue", void 0);
    __decorate([
        property(cc.Label)
    ], Slider2.prototype, "value", void 0);
    __decorate([
        property(cc.Sprite)
    ], Slider2.prototype, "fill", void 0);
    Slider2 = __decorate([
        ccclass
    ], Slider2);
    return Slider2;
}(cc.Component));
exports.default = Slider2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL1NsaWRlcjIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBQzlCLCtCQUEwQjtBQUUxQixvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjtBQUU1RSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFxQywyQkFBWTtJQUFqRDtRQUFBLHFFQThDQztRQTVDRyxjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRTFCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFFMUIsV0FBSyxHQUFhLElBQUksQ0FBQztRQUV2QixVQUFJLEdBQWMsSUFBSSxDQUFDO1FBRXZCLFlBQU0sR0FBYyxJQUFJLENBQUM7O1FBbUN6QixpQkFBaUI7SUFDckIsQ0FBQztJQWpDRyx3QkFBd0I7SUFFeEIsd0JBQU0sR0FBTjtRQUNJLElBQUksa0JBQWtCLEdBQUcsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3pELGtCQUFrQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RDLGtCQUFrQixDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7UUFDeEMsa0JBQWtCLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDO1FBRTlDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsZ0NBQWMsR0FBZCxVQUFlLE1BQU0sRUFBRSxNQUFNO1FBQ3pCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGNBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNyRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELDBCQUFRLEdBQVI7UUFDSSxJQUFJLEdBQUcsR0FBRyxnQkFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLGdCQUFNLENBQUMsTUFBTSxHQUFHLGdCQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDakYsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUM7SUFDekMsQ0FBQztJQXpDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZDQUNPO0lBRTFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NkNBQ087SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzswQ0FDSTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lDQUNHO0lBUk4sT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQThDM0I7SUFBRCxjQUFDO0NBOUNELEFBOENDLENBOUNvQyxFQUFFLENBQUMsU0FBUyxHQThDaEQ7a0JBOUNvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcbmltcG9ydCB1dGlsIGZyb20gXCIuL3V0aWxcIjtcblxuLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTbGlkZXIyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbWF4VmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbWluVmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgdmFsdWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICAgIGZpbGw6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBzbGlkZXI6IGNjLlNsaWRlciA9IG51bGw7XG4gICAgb25WYWx1ZUNoYW5nZTogKHY6IG51bWJlcikgPT4gdm9pZDtcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB2YXIgc2xpZGVyRXZlbnRIYW5kbGVyID0gbmV3IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIoKTtcbiAgICAgICAgc2xpZGVyRXZlbnRIYW5kbGVyLnRhcmdldCA9IHRoaXMubm9kZTtcbiAgICAgICAgc2xpZGVyRXZlbnRIYW5kbGVyLmNvbXBvbmVudCA9IFwiU2xpZGVyMlwiXG4gICAgICAgIHNsaWRlckV2ZW50SGFuZGxlci5oYW5kbGVyID0gXCJfb25WYWx1ZUNoYW5nZVwiO1xuXG4gICAgICAgIHRoaXMuc2xpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuU2xpZGVyKTtcbiAgICAgICAgdGhpcy5zbGlkZXIuc2xpZGVFdmVudHMucHVzaChzbGlkZXJFdmVudEhhbmRsZXIpO1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLm1pblZhbHVlLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KENvbmZpZy5taW5CZXQpO1xuICAgICAgICB0aGlzLm1heFZhbHVlLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KENvbmZpZy5tYXhCZXQpO1xuICAgICAgICB0aGlzLl9vblZhbHVlQ2hhbmdlKHRoaXMuc2xpZGVyLCBudWxsKTtcbiAgICB9XG5cbiAgICBfb25WYWx1ZUNoYW5nZShzZW5kZXIsIHBhcmFtcykge1xuICAgICAgICBsZXQgdmFsID0gdGhpcy5nZXRWYWx1ZSgpO1xuICAgICAgICB0aGlzLm9uVmFsdWVDaGFuZ2UodmFsKTtcbiAgICAgICAgdGhpcy52YWx1ZS5zdHJpbmcgPSB1dGlsLm51bWJlckZvcm1hdCh2YWwpO1xuICAgICAgICBsZXQgc2l6ZSA9IHRoaXMuZmlsbC5ub2RlLmdldENvbnRlbnRTaXplKCk7XG4gICAgICAgIHNpemUud2lkdGggPSB0aGlzLm5vZGUuZ2V0Q29udGVudFNpemUoKS53aWR0aCAqIHRoaXMuc2xpZGVyLnByb2dyZXNzO1xuICAgICAgICB0aGlzLmZpbGwubm9kZS5zZXRDb250ZW50U2l6ZShzaXplKTtcbiAgICB9XG5cbiAgICBnZXRWYWx1ZSgpOiBudW1iZXIge1xuICAgICAgICBsZXQgdmFsID0gQ29uZmlnLm1pbkJldCArIChDb25maWcubWF4QmV0IC0gQ29uZmlnLm1pbkJldCkgKiB0aGlzLnNsaWRlci5wcm9ncmVzcztcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodmFsIC8gMTAwMCkgKiAxMDAwO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=