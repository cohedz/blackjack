
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/LBEntry.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '48e8adoRHBEI57tbH867RGK', 'LBEntry');
// Scripts/LBEntry.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LBEntry = /** @class */ (function (_super) {
    __extends(LBEntry, _super);
    function LBEntry() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rank = null;
        _this.medal = null;
        _this.avatar = null;
        _this.playerName = null;
        _this.playerCoin = null;
        _this.spriteMedals = [];
        _this.playerId = null;
        _this.photo = null;
        _this.coin = 0;
        _this.click_key = 'lb_battle';
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    LBEntry.prototype.start = function () {
    };
    LBEntry.prototype.render = function (rank, name, coin, avatar, playerId) {
        var _this = this;
        this.playerId = playerId;
        this.playerName.string = name;
        this.playerCoin.string = util_1.default.numberFormat(coin);
        this.coin = coin;
        cc.assetManager.loadRemote(avatar, function (err, tex) {
            _this.photo = tex;
            _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
        });
        if (this.spriteMedals.length >= rank) {
            this.rank.node.active = false;
            this.medal.spriteFrame = this.spriteMedals[rank - 1];
        }
        else {
            this.rank.string = util_1.default.numberFormat(rank);
            this.medal.node.active = false;
        }
        if (Api_1.default.playerId == playerId) {
            this.click_key = 'lb_share';
        }
    };
    LBEntry.prototype.onClick = function () {
        cc.systemEvent.emit(this.click_key, this.playerId, this.photo, this.playerName.string, this.coin);
    };
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "rank", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "medal", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerCoin", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], LBEntry.prototype, "spriteMedals", void 0);
    LBEntry = __decorate([
        ccclass
    ], LBEntry);
    return LBEntry;
}(cc.Component));
exports.default = LBEntry;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0xCRW50cnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsK0JBQTBCO0FBQzFCLDZCQUF3QjtBQUVsQixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFxQywyQkFBWTtJQUFqRDtRQUFBLHFFQW9EQztRQWxERyxVQUFJLEdBQWEsSUFBSSxDQUFDO1FBRXRCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFFeEIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixrQkFBWSxHQUFxQixFQUFFLENBQUM7UUFFNUIsY0FBUSxHQUFXLElBQUksQ0FBQztRQUN4QixXQUFLLEdBQWlCLElBQUksQ0FBQztRQUMzQixVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBQ2pCLGVBQVMsR0FBVyxXQUFXLENBQUM7O0lBbUM1QyxDQUFDO0lBbENHLHdCQUF3QjtJQUV4QixlQUFlO0lBRWYsdUJBQUssR0FBTDtJQUVBLENBQUM7SUFFRCx3QkFBTSxHQUFOLFVBQU8sSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsTUFBYyxFQUFFLFFBQWdCO1FBQWpGLGlCQXFCQztRQXBCRyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsY0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBZSxNQUFNLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRztZQUN0RCxLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztZQUNqQixLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTtZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxHQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDbEM7UUFFRCxJQUFJLGFBQUcsQ0FBQyxRQUFRLElBQUksUUFBUSxFQUFFO1lBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1NBQy9CO0lBQ0wsQ0FBQztJQUVELHlCQUFPLEdBQVA7UUFDSSxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEcsQ0FBQztJQWpERDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3lDQUNHO0lBRXRCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MENBQ0k7SUFFeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzsyQ0FDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOytDQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7K0NBQ1M7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztpREFDVztJQVpuQixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBb0QzQjtJQUFELGNBQUM7Q0FwREQsQUFvREMsQ0FwRG9DLEVBQUUsQ0FBQyxTQUFTLEdBb0RoRDtrQkFwRG9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IHV0aWwgZnJvbSBcIi4vdXRpbFwiO1xuaW1wb3J0IEFwaSBmcm9tIFwiLi9BcGlcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMQkVudHJ5IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgcmFuazogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgbWVkYWw6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBhdmF0YXI6IGNjLlNwcml0ZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHBsYXllck5hbWU6IGNjLkxhYmVsID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgcGxheWVyQ29pbjogY2MuTGFiZWwgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcbiAgICBzcHJpdGVNZWRhbHM6IGNjLlNwcml0ZUZyYW1lW10gPSBbXTtcblxuICAgIHByaXZhdGUgcGxheWVySWQ6IHN0cmluZyA9IG51bGw7XG4gICAgcHJpdmF0ZSBwaG90bzogY2MuVGV4dHVyZTJEID0gbnVsbDtcbiAgICBwcml2YXRlIGNvaW46IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSBjbGlja19rZXk6IHN0cmluZyA9ICdsYl9iYXR0bGUnO1xuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICByZW5kZXIocmFuazogbnVtYmVyLCBuYW1lOiBzdHJpbmcsIGNvaW46IG51bWJlciwgYXZhdGFyOiBzdHJpbmcsIHBsYXllcklkOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5wbGF5ZXJJZCA9IHBsYXllcklkO1xuICAgICAgICB0aGlzLnBsYXllck5hbWUuc3RyaW5nID0gbmFtZTtcbiAgICAgICAgdGhpcy5wbGF5ZXJDb2luLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KGNvaW4pO1xuICAgICAgICB0aGlzLmNvaW4gPSBjb2luO1xuICAgICAgICBjYy5hc3NldE1hbmFnZXIubG9hZFJlbW90ZTxjYy5UZXh0dXJlMkQ+KGF2YXRhciwgKGVyciwgdGV4KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBob3RvID0gdGV4O1xuICAgICAgICAgICAgdGhpcy5hdmF0YXIuc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUodGV4KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHRoaXMuc3ByaXRlTWVkYWxzLmxlbmd0aCA+PSByYW5rKSB7XG4gICAgICAgICAgICB0aGlzLnJhbmsubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubWVkYWwuc3ByaXRlRnJhbWUgPSB0aGlzLnNwcml0ZU1lZGFsc1tyYW5rLTFdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5yYW5rLnN0cmluZyA9IHV0aWwubnVtYmVyRm9ybWF0KHJhbmspO1xuICAgICAgICAgICAgdGhpcy5tZWRhbC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKEFwaS5wbGF5ZXJJZCA9PSBwbGF5ZXJJZCkge1xuICAgICAgICAgICAgdGhpcy5jbGlja19rZXkgPSAnbGJfc2hhcmUnO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25DbGljaygpIHtcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQuZW1pdCh0aGlzLmNsaWNrX2tleSwgdGhpcy5wbGF5ZXJJZCwgdGhpcy5waG90bywgdGhpcy5wbGF5ZXJOYW1lLnN0cmluZywgdGhpcy5jb2luKTtcbiAgICB9XG59XG4iXX0=