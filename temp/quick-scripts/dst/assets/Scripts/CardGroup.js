
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/CardGroup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bece1QhfllOVa5CfNF0TmNb', 'CardGroup');
// Scripts/CardGroup.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup = /** @class */ (function () {
    function CardGroup(cards, kind) {
        this.cards = cards;
        this.kind = CardGroup.Single;
        this.kind = kind;
        if (kind != 0) {
            this.highest = Helper_1.default.findMaxCard(cards);
        }
    }
    CardGroup.prototype.getPosition = function () {
        if (this.cards.length % 2 != 0) {
            var idx = (this.cards.length - 1) / 2;
            return this.cards[idx].node.getPosition();
        }
        var middle = this.cards.length / 2;
        var c1 = this.cards[middle].node.getPosition();
        var c2 = this.cards[middle - 1].node.getPosition();
        return cc.v2((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
    };
    CardGroup.prototype.gt = function (o) {
        if (this.kind == o.kind && this.count() == o.count())
            return this.highest.gt(o.highest);
        if (o.highest.rank == 15 && o.count() == 1) {
            if (this.kind == CardGroup.MultiPair)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.highest.rank == 15 && o.count() == 2) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }
        if (o.kind == CardGroup.MultiPair && o.count() == 6) {
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
        }
        if (o.kind == CardGroup.House && o.count() == 4) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            else
                return false;
        }
        return false;
    };
    CardGroup.prototype.at = function (i) {
        return this.cards[i];
    };
    CardGroup.prototype.count = function () {
        return this.cards.length;
    };
    CardGroup.prototype.push = function (card) {
        this.cards.push(card);
    };
    CardGroup.prototype.remove = function (card) {
        var index = this.cards.indexOf(card, 0);
        this.cards.splice(index, 1);
    };
    CardGroup.prototype.contains = function (card) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            if (this.cards[i] == card)
                return true;
        }
        return false;
    };
    CardGroup.prototype.calculate = function () {
        if (this.cards.length == 1) {
            this.kind = CardGroup.Single;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isHouse(this.cards)) {
            this.kind = CardGroup.House;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isStraight(this.cards)) {
            this.kind = CardGroup.Straight;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else if (Helper_1.default.isMultiPair(this.cards)) {
            this.kind = CardGroup.MultiPair;
            this.highest = Helper_1.default.findMaxCard(this.cards);
        }
        else {
            this.kind = CardGroup.Ungrouped;
        }
    };
    CardGroup.prototype.sort = function () {
        Helper_1.default.sort(this.cards);
    };
    CardGroup.prototype.isInvalid = function () {
        return this.kind == CardGroup.Ungrouped;
    };
    CardGroup.prototype.dump = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            console.log(this.cards[i].toString());
        }
    };
    CardGroup.Ungrouped = 0;
    CardGroup.Single = 1;
    CardGroup.House = 2;
    CardGroup.Straight = 3;
    CardGroup.Boss = 4;
    CardGroup.MultiPair = 5;
    return CardGroup;
}());
exports.default = CardGroup;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NhcmRHcm91cC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1DQUE4QjtBQUU5QjtJQUtJLG1CQUFZLEtBQWtCLEVBQUUsSUFBWTtRQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLE9BQU8sR0FBRyxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM1QztJQUNMLENBQUM7SUFFRCwrQkFBVyxHQUFYO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzVCLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0M7UUFFRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDbkMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0MsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25ELE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxzQkFBRSxHQUFGLFVBQUcsQ0FBWTtRQUNYLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQ2hELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXRDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxTQUFTO2dCQUNoQyxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDakQsT0FBTyxJQUFJLENBQUM7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ3hDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNyRCxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDakQsT0FBTyxJQUFJLENBQUM7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNqRCxPQUFPLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztnQkFDckQsT0FBTyxJQUFJLENBQUM7U0FDbkI7UUFFRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQzdDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUNyRCxPQUFPLElBQUksQ0FBQzs7Z0JBRVosT0FBTyxLQUFLLENBQUM7U0FDcEI7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsc0JBQUUsR0FBRixVQUFHLENBQVM7UUFDUixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzdCLENBQUM7SUFFRCx3QkFBSSxHQUFKLFVBQUssSUFBVTtRQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQkFBTSxHQUFOLFVBQU8sSUFBVTtRQUNiLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELDRCQUFRLEdBQVIsVUFBUyxJQUFVO1FBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSTtnQkFBRSxPQUFPLElBQUksQ0FBQztTQUMxQztRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU0sSUFBSSxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pEO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDbkM7SUFDTCxDQUFDO0lBRUQsd0JBQUksR0FBSjtRQUNJLGdCQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsU0FBUyxDQUFDO0lBQzVDLENBQUM7SUFFRCx3QkFBSSxHQUFKO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFZSxtQkFBUyxHQUFXLENBQUMsQ0FBQztJQUN0QixnQkFBTSxHQUFXLENBQUMsQ0FBQztJQUNuQixlQUFLLEdBQVcsQ0FBQyxDQUFDO0lBQ2xCLGtCQUFRLEdBQVcsQ0FBQyxDQUFDO0lBQ3JCLGNBQUksR0FBVyxDQUFDLENBQUM7SUFDakIsbUJBQVMsR0FBVyxDQUFDLENBQUM7SUFDMUMsZ0JBQUM7Q0E3SEQsQUE2SEMsSUFBQTtrQkE3SG9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2FyZCBmcm9tIFwiLi9DYXJkXCI7XG5pbXBvcnQgSGVscGVyIGZyb20gXCIuL0hlbHBlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXJkR3JvdXAge1xuICAgIGNhcmRzOiBBcnJheTxDYXJkPjtcbiAgICBoaWdoZXN0OiBDYXJkO1xuICAgIGtpbmQ6IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKGNhcmRzOiBBcnJheTxDYXJkPiwga2luZDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuY2FyZHMgPSBjYXJkcztcbiAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLlNpbmdsZTtcbiAgICAgICAgdGhpcy5raW5kID0ga2luZDtcbiAgICAgICAgaWYgKGtpbmQgIT0gMCkge1xuICAgICAgICAgICAgdGhpcy5oaWdoZXN0ID0gSGVscGVyLmZpbmRNYXhDYXJkKGNhcmRzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldFBvc2l0aW9uKCk6IGNjLlZlYzIge1xuICAgICAgICBpZiAodGhpcy5jYXJkcy5sZW5ndGggJSAyICE9IDApIHtcbiAgICAgICAgICAgIGxldCBpZHggPSAodGhpcy5jYXJkcy5sZW5ndGggLSAxKSAvIDI7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYXJkc1tpZHhdLm5vZGUuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBtaWRkbGUgPSB0aGlzLmNhcmRzLmxlbmd0aCAvIDI7XG4gICAgICAgIGxldCBjMSA9IHRoaXMuY2FyZHNbbWlkZGxlXS5ub2RlLmdldFBvc2l0aW9uKCk7XG4gICAgICAgIGxldCBjMiA9IHRoaXMuY2FyZHNbbWlkZGxlIC0gMV0ubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgICAgICByZXR1cm4gY2MudjIoKGMxLnggKyBjMi54KSAvIDIsIChjMS55ICsgYzIueSkgLyAyKTtcbiAgICB9XG5cbiAgICBndChvOiBDYXJkR3JvdXApOiBib29sZWFuIHtcbiAgICAgICAgaWYgKHRoaXMua2luZCA9PSBvLmtpbmQgJiYgdGhpcy5jb3VudCgpID09IG8uY291bnQoKSlcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmhpZ2hlc3QuZ3Qoby5oaWdoZXN0KTtcblxuICAgICAgICBpZiAoby5oaWdoZXN0LnJhbmsgPT0gMTUgJiYgby5jb3VudCgpID09IDEpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpcilcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIGlmICh0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLkhvdXNlICYmIHRoaXMuY291bnQoKSA9PSA0KVxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG8uaGlnaGVzdC5yYW5rID09IDE1ICYmIG8uY291bnQoKSA9PSAyKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5NdWx0aVBhaXIgJiYgdGhpcy5jb3VudCgpID09IDgpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiB0aGlzLmNvdW50KCkgPT0gNClcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvLmtpbmQgPT0gQ2FyZEdyb3VwLk11bHRpUGFpciAmJiBvLmNvdW50KCkgPT0gNikge1xuICAgICAgICAgICAgaWYgKHRoaXMua2luZCA9PSBDYXJkR3JvdXAuSG91c2UgJiYgdGhpcy5jb3VudCgpID09IDQpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICBpZiAodGhpcy5raW5kID09IENhcmRHcm91cC5NdWx0aVBhaXIgJiYgdGhpcy5jb3VudCgpID09IDgpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoby5raW5kID09IENhcmRHcm91cC5Ib3VzZSAmJiBvLmNvdW50KCkgPT0gNCkge1xuICAgICAgICAgICAgaWYgKHRoaXMua2luZCA9PSBDYXJkR3JvdXAuTXVsdGlQYWlyICYmIHRoaXMuY291bnQoKSA9PSA4KVxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBhdChpOiBudW1iZXIpOiBDYXJkIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHNbaV07XG4gICAgfVxuXG4gICAgY291bnQoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FyZHMubGVuZ3RoO1xuICAgIH1cblxuICAgIHB1c2goY2FyZDogQ2FyZCkge1xuICAgICAgICB0aGlzLmNhcmRzLnB1c2goY2FyZCk7XG4gICAgfVxuXG4gICAgcmVtb3ZlKGNhcmQ6IENhcmQpIHtcbiAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5jYXJkcy5pbmRleE9mKGNhcmQsIDApO1xuICAgICAgICB0aGlzLmNhcmRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgfVxuXG4gICAgY29udGFpbnMoY2FyZDogQ2FyZCk6IGJvb2xlYW4ge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuY2FyZHNbaV0gPT0gY2FyZCkgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGNhbGN1bGF0ZSgpIHtcbiAgICAgICAgaWYgKHRoaXMuY2FyZHMubGVuZ3RoID09IDEpIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5TaW5nbGU7XG4gICAgICAgICAgICB0aGlzLmhpZ2hlc3QgPSBIZWxwZXIuZmluZE1heENhcmQodGhpcy5jYXJkcyk7XG4gICAgICAgIH0gZWxzZSBpZiAoSGVscGVyLmlzSG91c2UodGhpcy5jYXJkcykpIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5Ib3VzZTtcbiAgICAgICAgICAgIHRoaXMuaGlnaGVzdCA9IEhlbHBlci5maW5kTWF4Q2FyZCh0aGlzLmNhcmRzKTtcbiAgICAgICAgfSBlbHNlIGlmIChIZWxwZXIuaXNTdHJhaWdodCh0aGlzLmNhcmRzKSkge1xuICAgICAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLlN0cmFpZ2h0O1xuICAgICAgICAgICAgdGhpcy5oaWdoZXN0ID0gSGVscGVyLmZpbmRNYXhDYXJkKHRoaXMuY2FyZHMpO1xuICAgICAgICB9IGVsc2UgaWYgKEhlbHBlci5pc011bHRpUGFpcih0aGlzLmNhcmRzKSkge1xuICAgICAgICAgICAgdGhpcy5raW5kID0gQ2FyZEdyb3VwLk11bHRpUGFpcjtcbiAgICAgICAgICAgIHRoaXMuaGlnaGVzdCA9IEhlbHBlci5maW5kTWF4Q2FyZCh0aGlzLmNhcmRzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMua2luZCA9IENhcmRHcm91cC5Vbmdyb3VwZWQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzb3J0KCkge1xuICAgICAgICBIZWxwZXIuc29ydCh0aGlzLmNhcmRzKTtcbiAgICB9XG5cbiAgICBpc0ludmFsaWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmtpbmQgPT0gQ2FyZEdyb3VwLlVuZ3JvdXBlZDtcbiAgICB9XG5cbiAgICBkdW1wKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5jYXJkcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5jYXJkc1tpXS50b1N0cmluZygpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHN0YXRpYyByZWFkb25seSBVbmdyb3VwZWQ6IG51bWJlciA9IDA7XG4gICAgc3RhdGljIHJlYWRvbmx5IFNpbmdsZTogbnVtYmVyID0gMTtcbiAgICBzdGF0aWMgcmVhZG9ubHkgSG91c2U6IG51bWJlciA9IDI7XG4gICAgc3RhdGljIHJlYWRvbmx5IFN0cmFpZ2h0OiBudW1iZXIgPSAzO1xuICAgIHN0YXRpYyByZWFkb25seSBCb3NzOiBudW1iZXIgPSA0O1xuICAgIHN0YXRpYyByZWFkb25seSBNdWx0aVBhaXI6IG51bWJlciA9IDU7XG59XG4iXX0=