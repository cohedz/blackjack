
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Config.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a4c56iZ0yNPbYQ1DV9FQmk8', 'Config');
// Scripts/Config.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.language = 'vi';
    Config.defaultCoin = 100000;
    Config.botCoin = 100000;
    Config.bankrupt = 1000;
    Config.bankrupt_bonus = 500000;
    Config.minBet = 3000;
    Config.maxBet = 100000;
    Config.betValue = 3000;
    Config.totalPlayer = 6;
    Config.userphoto = null;
    Config.battle = null;
    Config.intertital_ads = '717961958974985_757265555044625';
    Config.reward_video = '316445729595977_363105338263349';
    Config.gift_reward_video = '316445729595977_363105338263349';
    Config.soundEnable = true;
    Config.dailyBonus = [
        { coin: 100000 },
        { ticket: 3 },
        { coin: 500000 },
        { coin: 1000000 },
        { coin: 5000000 },
        { ticket: 5 },
        { coin: 10000000 }
    ];
    Config.stepOfCountTime = 30000;
    return Config;
}());
exports.default = Config;
// Ngày 1 : 100K ,
// Ngày 2 : 300K , 
// ngày 3 thêm 3 lượt SPIN x tiền , 
// ngày 4 : 1M , 
// ngày 5 thêm 5 lượt Spin x tiền , 
// ngày 6 5M , 
// ngày 7 : 10M

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHRzL0NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7SUEwQkEsQ0FBQztJQXpCaUIsZUFBUSxHQUFXLElBQUksQ0FBQztJQUNmLGtCQUFXLEdBQVcsTUFBTSxDQUFDO0lBQzdCLGNBQU8sR0FBVyxNQUFNLENBQUM7SUFDekIsZUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixxQkFBYyxHQUFXLE1BQU0sQ0FBQztJQUNoQyxhQUFNLEdBQVcsSUFBSSxDQUFDO0lBQ3RCLGFBQU0sR0FBVyxNQUFNLENBQUM7SUFDakMsZUFBUSxHQUFXLElBQUksQ0FBQztJQUN4QixrQkFBVyxHQUFXLENBQUMsQ0FBQztJQUN4QixnQkFBUyxHQUFpQixJQUFJLENBQUM7SUFDL0IsYUFBTSxHQUE0RCxJQUFJLENBQUM7SUFDOUQscUJBQWMsR0FBVyxpQ0FBaUMsQ0FBQztJQUMzRCxtQkFBWSxHQUFXLGlDQUFpQyxDQUFDO0lBQ3pELHdCQUFpQixHQUFXLGlDQUFpQyxDQUFDO0lBQ3ZFLGtCQUFXLEdBQUcsSUFBSSxDQUFDO0lBQ1YsaUJBQVUsR0FBRztRQUNoQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7UUFDaEIsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO1FBQ2IsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO1FBQ2hCLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRTtRQUNqQixFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7UUFDakIsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO1FBQ2IsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFO0tBQ3JCLENBQUM7SUFDWSxzQkFBZSxHQUFHLEtBQUssQ0FBQztJQUMxQyxhQUFDO0NBMUJELEFBMEJDLElBQUE7a0JBMUJvQixNQUFNO0FBNEIzQixrQkFBa0I7QUFDbEIsbUJBQW1CO0FBQ25CLG9DQUFvQztBQUNwQyxpQkFBaUI7QUFDakIsb0NBQW9DO0FBQ3BDLGVBQWU7QUFDZixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29uZmlnIHtcbiAgICBwdWJsaWMgc3RhdGljIGxhbmd1YWdlOiBzdHJpbmcgPSAndmknO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdENvaW46IG51bWJlciA9IDEwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IGJvdENvaW46IG51bWJlciA9IDEwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IGJhbmtydXB0OiBudW1iZXIgPSAxMDAwO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgYmFua3J1cHRfYm9udXM6IG51bWJlciA9IDUwMDAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IG1pbkJldDogbnVtYmVyID0gMzAwMDtcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IG1heEJldDogbnVtYmVyID0gMTAwMDAwO1xuICAgIHB1YmxpYyBzdGF0aWMgYmV0VmFsdWU6IG51bWJlciA9IDMwMDA7XG4gICAgcHVibGljIHN0YXRpYyB0b3RhbFBsYXllcjogbnVtYmVyID0gNjtcbiAgICBwdWJsaWMgc3RhdGljIHVzZXJwaG90bzogY2MuVGV4dHVyZTJEID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGJhdHRsZTogeyBwaG90bzogY2MuVGV4dHVyZTJELCB1c2VybmFtZTogc3RyaW5nLCBjb2luOiBudW1iZXIgfSA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyByZWFkb25seSBpbnRlcnRpdGFsX2Fkczogc3RyaW5nID0gJzcxNzk2MTk1ODk3NDk4NV83NTcyNjU1NTUwNDQ2MjUnO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgcmV3YXJkX3ZpZGVvOiBzdHJpbmcgPSAnMzE2NDQ1NzI5NTk1OTc3XzM2MzEwNTMzODI2MzM0OSc7XG4gICAgcHVibGljIHN0YXRpYyByZWFkb25seSBnaWZ0X3Jld2FyZF92aWRlbzogc3RyaW5nID0gJzMxNjQ0NTcyOTU5NTk3N18zNjMxMDUzMzgyNjMzNDknO1xuICAgIHB1YmxpYyBzdGF0aWMgc291bmRFbmFibGUgPSB0cnVlO1xuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgZGFpbHlCb251cyA9IFtcbiAgICAgICAgeyBjb2luOiAxMDAwMDAgfSxcbiAgICAgICAgeyB0aWNrZXQ6IDMgfSxcbiAgICAgICAgeyBjb2luOiA1MDAwMDAgfSxcbiAgICAgICAgeyBjb2luOiAxMDAwMDAwIH0sXG4gICAgICAgIHsgY29pbjogNTAwMDAwMCB9LFxuICAgICAgICB7IHRpY2tldDogNSB9LFxuICAgICAgICB7IGNvaW46IDEwMDAwMDAwIH1cbiAgICBdO1xuICAgIHB1YmxpYyBzdGF0aWMgc3RlcE9mQ291bnRUaW1lID0gMzAwMDA7XG59XG5cbi8vIE5nw6B5IDEgOiAxMDBLICxcbi8vIE5nw6B5IDIgOiAzMDBLICwgXG4vLyBuZ8OgeSAzIHRow6ptIDMgbMaw4bujdCBTUElOIHggdGnhu4FuICwgXG4vLyBuZ8OgeSA0IDogMU0gLCBcbi8vIG5nw6B5IDUgdGjDqm0gNSBsxrDhu6N0IFNwaW4geCB0aeG7gW4gLCBcbi8vIG5nw6B5IDYgNU0gLCBcbi8vIG5nw6B5IDcgOiAxME0iXX0=