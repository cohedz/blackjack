"use strict";
cc._RF.push(module, 'cd2a0cL565MMb3PI2MSL01H', 'DailyBonusItem');
// Scripts/popop/bonus/DailyBonusItem.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DailyBonusItem = /** @class */ (function (_super) {
    __extends(DailyBonusItem, _super);
    function DailyBonusItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = null;
        _this.claimed = null;
        _this.extra = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    DailyBonusItem.prototype.start = function () {
    };
    // update (dt) {}
    DailyBonusItem.prototype.setClaimable = function (claimable, isClaimed) {
        this.claimed.active = isClaimed;
        this.getComponent(cc.Button).interactable = claimable;
        this.extra.active = claimable;
    };
    __decorate([
        property(cc.Label)
    ], DailyBonusItem.prototype, "value", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonusItem.prototype, "claimed", void 0);
    __decorate([
        property(cc.Node)
    ], DailyBonusItem.prototype, "extra", void 0);
    DailyBonusItem = __decorate([
        ccclass
    ], DailyBonusItem);
    return DailyBonusItem;
}(cc.Component));
exports.default = DailyBonusItem;

cc._RF.pop();