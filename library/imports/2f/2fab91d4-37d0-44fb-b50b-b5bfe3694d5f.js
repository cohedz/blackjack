"use strict";
cc._RF.push(module, '2fab9HUN9BE+7ULtb/jaU1f', 'Bot');
// Scripts/Bot.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CardGroup_1 = require("./CardGroup");
var Helper_1 = require("./Helper");
var Bot = /** @class */ (function () {
    function Bot() {
    }
    Bot.random = function (handCards) {
        var cards = null;
        var firstCard = Helper_1.default.findCard(handCards, 3, 1);
        if (firstCard) {
            var straight = Bot.findAllCardByRank(handCards, 3);
            if (straight.length >= 3) {
                return new CardGroup_1.default(straight, CardGroup_1.default.House);
            }
            return new CardGroup_1.default([firstCard], CardGroup_1.default.Single);
        }
        cards = Bot.findStraightCard(handCards);
        if (cards) {
            return new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        cards = Bot.findHouseCard(handCards);
        if (cards && !(cards[0].rank == 15 && cards.length < handCards.length)) {
            return new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        var card = Helper_1.default.findMinCard(handCards);
        return new CardGroup_1.default([card], CardGroup_1.default.Single);
    };
    Bot.suggest = function (commonCards, handCards) {
        var common = commonCards.peek();
        if (common.highest.rank == 15 && common.count() == 3) {
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 2) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 4 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var pair = Helper_1.default.findHouseCard(handCards, common.highest, 2);
            if (pair) {
                return new CardGroup_1.default(pair, CardGroup_1.default.House);
            }
            return null;
        }
        if (common.highest.rank == 15 && common.count() == 1) {
            var cards = Helper_1.default.findMultiPairCard(handCards, null, 3 * 2);
            if (cards) {
                return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
            }
            var four = Helper_1.default.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup_1.default(four, CardGroup_1.default.House);
            }
            var card = Helper_1.default.findMinCard(handCards, common.highest);
            if (card) {
                return new CardGroup_1.default([card], CardGroup_1.default.Single);
            }
            return null;
        }
        if (common.kind == CardGroup_1.default.Single) {
            var cards = Helper_1.default.findMinCard(handCards, common.highest);
            return cards == null ? null : new CardGroup_1.default([cards], CardGroup_1.default.Single);
        }
        if (common.kind == CardGroup_1.default.House) {
            var cards = Helper_1.default.findHouseCard(handCards, common.highest, common.count());
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        if (common.kind == CardGroup_1.default.Straight) {
            var cards = Helper_1.default.findStraightCard(handCards, common);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        if (common.kind == CardGroup_1.default.MultiPair) {
            var cards = Helper_1.default.findMultiPairCard(handCards, common.highest, common.count() / 2);
            return cards == null ? null : new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
        }
        return null;
    };
    Bot.findStraightCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            if (cardStarted.rank < 13) {
                var straight = Bot._findStraightCard(cards, cardStarted);
                if (straight.length >= 3) {
                    return straight;
                }
            }
        }
        return null;
    };
    Bot._findStraightCard = function (cards, started) {
        var straight = [started];
        for (var c = started.rank + 1; c < 15; c++) {
            var card = Helper_1.default.findCardByRank(cards, c);
            if (card == null) {
                break;
            }
            else {
                straight.push(card);
            }
        }
        return straight;
    };
    Bot.findHouseCard = function (cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
            var cardStarted = cards[i];
            var straight = Bot.findAllCardByRank(cards, cardStarted.rank);
            if (straight.length >= 2) {
                return straight;
            }
        }
        return null;
    };
    Bot.findAllCardByRank = function (cards, rank) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
            }
        }
        return setCards;
    };
    return Bot;
}());
exports.default = Bot;

cc._RF.pop();