"use strict";
cc._RF.push(module, '48e8adoRHBEI57tbH867RGK', 'LBEntry');
// Scripts/LBEntry.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("./util");
var Api_1 = require("./Api");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LBEntry = /** @class */ (function (_super) {
    __extends(LBEntry, _super);
    function LBEntry() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rank = null;
        _this.medal = null;
        _this.avatar = null;
        _this.playerName = null;
        _this.playerCoin = null;
        _this.spriteMedals = [];
        _this.playerId = null;
        _this.photo = null;
        _this.coin = 0;
        _this.click_key = 'lb_battle';
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    LBEntry.prototype.start = function () {
    };
    LBEntry.prototype.render = function (rank, name, coin, avatar, playerId) {
        var _this = this;
        this.playerId = playerId;
        this.playerName.string = name;
        this.playerCoin.string = util_1.default.numberFormat(coin);
        this.coin = coin;
        cc.assetManager.loadRemote(avatar, function (err, tex) {
            _this.photo = tex;
            _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
        });
        if (this.spriteMedals.length >= rank) {
            this.rank.node.active = false;
            this.medal.spriteFrame = this.spriteMedals[rank - 1];
        }
        else {
            this.rank.string = util_1.default.numberFormat(rank);
            this.medal.node.active = false;
        }
        if (Api_1.default.playerId == playerId) {
            this.click_key = 'lb_share';
        }
    };
    LBEntry.prototype.onClick = function () {
        cc.systemEvent.emit(this.click_key, this.playerId, this.photo, this.playerName.string, this.coin);
    };
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "rank", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "medal", void 0);
    __decorate([
        property(cc.Sprite)
    ], LBEntry.prototype, "avatar", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerName", void 0);
    __decorate([
        property(cc.Label)
    ], LBEntry.prototype, "playerCoin", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], LBEntry.prototype, "spriteMedals", void 0);
    LBEntry = __decorate([
        ccclass
    ], LBEntry);
    return LBEntry;
}(cc.Component));
exports.default = LBEntry;

cc._RF.pop();