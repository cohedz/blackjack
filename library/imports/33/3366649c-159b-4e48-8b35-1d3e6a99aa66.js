"use strict";
cc._RF.push(module, '33666ScFZtOSIs1HT5qmapm', 'Text2');
// Scripts/Text2.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Language_1 = require("./Language");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Text2 = /** @class */ (function (_super) {
    __extends(Text2, _super);
    function Text2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.key = '';
        _this.label = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Text2.prototype.start = function () {
        this.label = this.getComponent(cc.Label);
        if (!this.label) {
            this.label = this.getComponent(cc.RichText);
        }
        if (this.label) {
            this.label.string = Language_1.default.getInstance().get(this.key);
        }
        cc.systemEvent.on('LANG_CHAN', this.onLanguageChange, this);
    };
    // update (dt) {}
    Text2.prototype.onLanguageChange = function () {
        console.log('onLanguageChange', this, this.label);
        if (this.label) {
            this.label.string = Language_1.default.getInstance().get(this.key);
        }
    };
    Text2.prototype.onDestroy = function () {
        cc.systemEvent.off('LANG_CHAN');
    };
    __decorate([
        property()
    ], Text2.prototype, "key", void 0);
    Text2 = __decorate([
        ccclass
    ], Text2);
    return Text2;
}(cc.Component));
exports.default = Text2;

cc._RF.pop();