"use strict";
cc._RF.push(module, 'cffd1wdHiNPIaFpHSP+5t/F', 'Api');
// Scripts/Api.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var image_1 = require("./image");
var Api = /** @class */ (function () {
    function Api() {
    }
    Api.initAsync = function (callback, leaderboard) {
        if (Api.initialized) {
            Api.loadLeaderboard(leaderboard);
            return callback(false, Api.dailyBonusDay); // daily_bonus_claimed_at, daily_bonus_day
        }
        if (!Api.initialized && window.hasOwnProperty('FBInstant')) {
            Api.initialized = true;
            Api.photo = FBInstant.player.getPhoto();
            Api.username = FBInstant.player.getName();
            Api.playerId = FBInstant.player.getID();
            Api.locale = FBInstant.getLocale();
            Api.getPlayerData(callback);
            Api.loadLeaderboard(leaderboard);
            Api.subscribeBot();
            Api.createShortcut();
        }
        else {
            Api.ticket = 10;
            Api.dailyBonusDay = 0;
            // setTimeout(() => {
            Api.locale = 'vi_VN';
            // Api.locale = 'th_TH';
            // callback(true, Api.dailyBonusDay);
            Api.loadLeaderboard(leaderboard);
            // }, 5000);
        }
    };
    Api.getPlayerData = function (callback) {
        FBInstant.player.getDataAsync(['coin', 'ticket', 'daily_bonus_claimed_at', 'daily_bonus_day']).then(function (data) {
            cc.log('data is loaded');
            Api.coin = data['coin'];
            if (Api.coin == undefined || Api.coin == null) {
                Api.coin = Config_1.default.defaultCoin;
                Api.isDirty = true;
            }
            Api.ticket = data['ticket'] || 0;
            Api.dailyBonusDay = data['daily_bonus_day'] || 0;
            Api.dailyBonusClaimed = !Api.dailyBonusClaimable(data['daily_bonus_claimed_at']);
            callback(!Api.dailyBonusClaimed, Api.dailyBonusDay);
            if (Api.isDirty) {
                Api.flush();
            }
        });
    };
    Api.subscribeBot = function () {
        FBInstant.player.canSubscribeBotAsync().then(function (can_subscribe) {
            cc.log('subscribeBotAsync', can_subscribe);
            Api.canSubscribeBot = can_subscribe.valueOf();
            // Then when you want to ask the player to subscribe
            if (Api.canSubscribeBot) {
                FBInstant.player.subscribeBotAsync().then(
                // Player is subscribed to the bot
                ).catch(function (e) {
                    // Handle subscription failure
                    cc.log('subscribeBotAsync');
                });
            }
        }).catch(function (e) {
            cc.log('subscribeBotAsync', e);
        });
    };
    Api.createShortcut = function () {
        FBInstant.canCreateShortcutAsync()
            .then(function (canCreateShortcut) {
            if (canCreateShortcut) {
                FBInstant.createShortcutAsync()
                    .then(function () {
                    // Shortcut created
                })
                    .catch(function () {
                    // Shortcut not created
                });
            }
        });
    };
    Api.loadLeaderboard = function (leaderboard) {
        fetch(Api.lb_api + "/" + Api.lb_id).then(function (response) { return response.json(); }).then(function (res) {
            var _a;
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            ((_a = res === null || res === void 0 ? void 0 : res.entries) === null || _a === void 0 ? void 0 : _a.length) && res.entries.map(function (e, i) {
                leaderboard.render(i + 1, e.username, e.score, e.photo, e.user_id);
            });
            // for (let i = 0; i < res.entries.length; i++) {
            //     const e = res.entries[i];
            //     leaderboard.render(
            //         i + 1,
            //         e.username,
            //         e.score,
            //         e.photo,
            //         e.user_id,
            //     );
            // }
            leaderboard.onLoadComplete();
        }).catch(function (error) { return console.error(error); });
    };
    Api.preloadRewardedVideo = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (!window.hasOwnProperty('FBInstant')) {
        if (callback)
            callback();
        return;
        // }
        if (Api.preloadedRewardedVideo || !Config_1.default.reward_video) {
            return;
        }
        FBInstant.getRewardedVideoAsync(Config_1.default.reward_video).then(function (rewarded) {
            // Load the Ad asynchronously
            Api.preloadedRewardedVideo = rewarded;
            return Api.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            cc.log('Rewarded video preloaded');
            if (callback)
                callback();
        }).catch(function (e) {
            Api.preloadedRewardedVideo = null;
            console.error(e.message);
            if (callback)
                callback();
        });
    };
    Api.showRewardedVideo = function (success, error) {
        // if (!window.hasOwnProperty('FBInstant')) {
        //     return error('rrr');
        // }
        // if (!Config.reward_video) {
        success();
        // }
        // if (!Api.preloadedRewardedVideo) {
        //     console.error('ad not load yet');
        //     return error('ad not load yet');
        // }
        // Api.preloadedRewardedVideo.showAsync().then(function () {
        //     success();
        //     Api.preloadedRewardedVideo = null;
        //     cc.log('Rewarded video watched successfully');
        // }).catch(function (e) {
        //     error(e.message);
        //     Api.preloadedRewardedVideo = null;
        //     console.error(e.message);
        // });
    };
    Api.preloadInterstitialAd = function (callback) {
        if (callback === void 0) { callback = null; }
        // if (window.hasOwnProperty('FBInstant') && !Api.preloadedInterstitial) {
        //     FBInstant.getInterstitialAdAsync(Config.intertital_ads).then(function(interstitial) {
        //         // Load the Ad asynchronously
        //         Api.preloadedInterstitial = interstitial;
        //         return Api.preloadedInterstitial.loadAsync();
        //     }).then(function() {
        //         cc.log('Interstitial preloaded');
        if (callback)
            callback();
        // }).catch(function(err){
        //     cc.error('Interstitial failed to preload');
        //     cc.error(err);
        // });
        // }
    };
    Api.showInterstitialAd = function () {
        // if (window.hasOwnProperty('FBInstant')) {
        //     if (Api.preloadedInterstitial) {
        //         Api.preloadedInterstitial.showAsync().then(function () {
        //             // Perform post-ad success operation
        //             cc.log('Interstitial ad finished successfully');
        //             Api.preloadedInterstitial = null;
        //         }).catch(function (e) {
        //             cc.error(e.message);
        //             Api.preloadedInterstitial = null;
        //         });
        //     }
        // }
    };
    Api.isInterstitialAdLoaded = function () {
        if (!window.hasOwnProperty('FBInstant')) {
            return false;
        }
        return !!Api.preloadedInterstitial;
    };
    Api.challenge = function (playerId) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FBInstant.context.createAsync(playerId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, FBInstant.updateAsync({
                                action: 'CUSTOM',
                                template: 'play_turn',
                                cta: 'Chơi ngay',
                                image: image_1.default,
                                text: Api.username + ' đang thách đấu bạn. Nhấn chơi ngay!',
                                strategy: 'IMMEDIATE'
                            })];
                }
            });
        });
    };
    Api.shareAsync = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                if (window.hasOwnProperty('FBInstant')) {
                    return [2 /*return*/, FBInstant.shareAsync({
                            intent: 'SHARE',
                            image: image_1.default,
                            text: 'Tiến liên miền nam 2020!',
                        })];
                }
                return [2 /*return*/, Promise.resolve()];
            });
        });
    };
    Api.invite = function (success, error) {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.context
                .chooseAsync()
                .then(function () {
                success(FBInstant.context.getID());
                FBInstant.updateAsync({
                    action: 'CUSTOM',
                    template: 'play_turn',
                    cta: 'Chơi ngay',
                    image: image_1.default,
                    text: Api.username + ' mời bạn cùng chơi game tiến liên miền nam',
                    strategy: 'IMMEDIATE'
                });
            }).catch(error);
        }
        else {
            success('');
        }
    };
    Api.coinIncrement = function (val) {
        Api.updateCoin(Api.coin + val);
    };
    Api.updateCoin = function (coin) {
        Api.coin = coin;
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function () { return cc.log('Score saved'); }).catch(function (err) { return console.error(err); });
    };
    Api.updateTicket = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ ticket: Api.ticket }).then(function () {
                cc.log('ticket is set');
            });
        }
    };
    Api.claimDailyBonus = function (day) {
        Api.dailyBonusClaimed = true;
        if (window.hasOwnProperty('FBInstant')) {
            var claimed_at = new Date();
            FBInstant.player.setDataAsync({
                coin: Api.coin,
                ticket: Api.ticket,
                'daily_bonus_claimed_at': claimed_at.toString(),
                'daily_bonus_day': day
            }).then(function () {
                cc.log('data is set');
            }, function (reason) {
                cc.log('data is not set', reason);
            });
        }
        Api.dailyBonusDay++;
    };
    Api.dailyBonusClaimable = function (claimedAt) {
        if (!claimedAt)
            return true;
        if (typeof claimedAt == 'string') {
            claimedAt = new Date(claimedAt);
        }
        var now = new Date();
        if (now.getFullYear() != claimedAt.getFullYear())
            return true;
        if (now.getMonth() != claimedAt.getMonth())
            return true;
        if (now.getDate() != claimedAt.getDate())
            return true;
        return false;
    };
    Api.flush = function () {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin, ticket: Api.ticket }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(function (res) { return res.json(); }).then(function (res) { return cc.log(res); }).catch(function (err) { return console.error(err); });
    };
    Api.logEvent = function (eventName, valueToSum, parameters) {
        if (!eventName)
            return;
        // if (window.hasOwnProperty('FBInstant')) {
        try {
            // window.firebase.analytics().logEvent(eventName, parameters);
            console.log('====================================');
            console.log(eventName, parameters);
            console.log('====================================');
        }
        catch (error) {
            console.log('====================================');
            console.log('analytic error', error);
            console.log('====================================');
        }
        // FBInstant.logEvent(eventName, valueToSum, parameters);
        // }
    };
    Api.setScoreAsync = function (score) {
        // if (!window.hasOwnProperty('FBInstant')) return;
        var data = { score: score, userId: Api.playerId, name: Api.username, photo: Api.photo };
        return fetch(Api.lb_api + "/" + Api.lb_id, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    };
    Api.randomBonus = function () {
        var r = Math.random();
        if (r < 0.05)
            return 400000;
        if (r < 0.2)
            return 300000;
        if (r < 0.5)
            return 200000;
        return 100000;
    };
    Api.initialized = false;
    Api.coin = 10000;
    Api.ticket = 0;
    Api.username = 'DM';
    Api.playerId = 'DM';
    Api.locale = 'vi_VN';
    Api.photo = 'https://i.imgur.com/FUOxs43.jpg';
    Api.isDirty = false;
    Api.preloadedRewardedVideo = null;
    Api.preloadedInterstitial = null;
    Api.canSubscribeBot = false;
    Api.dailyBonusDay = 0;
    Api.dailyBonusClaimed = false;
    Api.lb_api = 'https://leaderboard.adpia.com.vn/instant'; //'https://lb.dozo.vn/api'
    Api.lb_id = 'a330e5054804f7fb73bd1f99'; //'5f714415630b9b9ff8146f15'
    Api.ID_APP = '440201687124598';
    return Api;
}());
exports.default = Api;

cc._RF.pop();