"use strict";
cc._RF.push(module, '5e609fdf41BH6QiJ7pgTFU1', 'Card');
// Scripts/Card.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Card = /** @class */ (function (_super) {
    __extends(Card, _super);
    function Card() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.back = null;
        _this.front = null;
        _this.rank = 1; // A: 1, J: 11, Q: 12, K: 13
        _this.suit = 1; // 1: spade, 2: club, 3: diamond, 4: heart
        _this.overlayer = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Card.prototype.onLoad = function () {
        this.front = this.getComponent(cc.Sprite).spriteFrame;
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.overlayer = this.node.children[this.node.children.length - 1];
    };
    Card.prototype.start = function () {
        this.overlayer.active = false;
    };
    Card.prototype.reset = function () {
        this.node.active = true;
        this.node.setScale(1);
        this.overlayer.active = false;
        this.node.setPosition(cc.Vec2.ZERO);
        this.hide();
    };
    Card.prototype.show = function () {
        this.getComponent(cc.Sprite).spriteFrame = this.front;
        this.setChildrenActive(true);
    };
    Card.prototype.hide = function () {
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.setChildrenActive(false);
    };
    Card.prototype.setChildrenActive = function (active) {
        for (var i = 0; i < this.node.children.length - 1; i++) {
            this.node.children[i].active = active;
        }
    };
    Card.prototype.overlap = function () {
        this.overlayer.active = true;
    };
    Card.prototype.setPositionY = function (newPosY) {
        this.node.setPosition(this.node.position.x, newPosY);
    };
    Card.prototype.compare = function (o) {
        if (o == null)
            return 1;
        if (this.rank > o.rank)
            return 1;
        if (this.rank < o.rank)
            return -1;
        if (this.suit > o.suit)
            return 1;
        if (this.suit < o.suit)
            return -1;
        return 0;
    };
    Card.prototype.gt = function (o) {
        return this.compare(o) > 0;
    };
    Card.prototype.lt = function (o) {
        return this.compare(o) < 0;
    };
    // update (dt) {}
    Card.prototype.toString = function () {
        return 'Rank: ' + this.rank + ' Suit: ' + this.suit;
    };
    __decorate([
        property(cc.SpriteFrame)
    ], Card.prototype, "back", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], Card.prototype, "front", void 0);
    Card = __decorate([
        ccclass
    ], Card);
    return Card;
}(cc.Component));
exports.default = Card;

cc._RF.pop();