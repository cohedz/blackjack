"use strict";
cc._RF.push(module, '4cea4WuKU1GD7EAlqpK5ThR', 'Player');
// Scripts/Player.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var CardGroup_1 = require("./CardGroup");
var Timer_1 = require("./Timer");
var TweenMove_1 = require("./tween/TweenMove");
var util_1 = require("./util");
var Config_1 = require("./Config");
var Game_1 = require("./Game");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fold = null;
        _this.timer = null;
        _this.coin = null;
        _this.username = null;
        _this.marker = null;
        _this.avatar = null;
        _this.effect = null;
        _this.pointLbl = null;
        _this.seat = 0;
        _this.cards = [];
        _this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        _this.inRound = true;
        _this.coinVal = 1;
        _this.cardMargin = 0;
        _this.cardRootPosition = null;
        _this.cardDirection = null;
        _this.labelCardCounter = null;
        _this.numOfAce = 0;
        _this.point = 0;
        _this.bonusType = 0; // 0 nothing,  1 double, 2 insurr
        _this.isSplit = 0;
        _this.subUser = null;
        _this.isSubUser = false;
        _this.setBonusType = function (bonusType) {
            try {
                var self = _this;
                var txtBonusUser = 'Canvas/user/bonus';
                if (!_this.inRound) {
                    self = _this.subUser;
                    txtBonusUser = 'Canvas/subUser/bonus';
                }
                self.bonusType = bonusType;
                var bonusLbl = cc.find(txtBonusUser);
                bonusLbl.active = !!bonusType;
                var str = bonusType == 1 ? 'x2' : '/2';
                bonusLbl.getComponent(cc.Label).string = str;
            }
            catch (error) {
            }
        };
        _this.showAllCard = function () {
            _this.cards.map(function (card) {
                card.show();
            });
        };
        _this.changeCoin = function (coin) {
            var color = cc.color(0xf9, 0xd2, 0x1e, 255), addString = '+';
            if (coin < 0) {
                color = cc.color(255, 0, 0, 255), addString = '-';
            }
            _this.effect.getComponent(cc.Label).string = addString + util_1.default.numberFormat(Math.abs(coin));
            _this.effect.node.color = color;
            _this.effect.play();
            _this.setCoin(_this.coinVal + coin);
        };
        _this.split = function (subUser) {
            _this.subUser = subUser;
            _this.subUser.isSubUser = true;
            if (_this.isSplit)
                return;
            _this.isSplit = 1;
            if (_this.cards[1].rank === 1)
                _this.numOfAce--;
            _this.cards.map(function (card, index) {
                if (index === 1) {
                    return _this.subUser.push(card, 0);
                }
                card.node.zIndex = index === 1 ? -1 : index;
                var pos = _this.getCardRootPosition();
                pos.x += index < 2 ? 0 : index * _this.cardMargin;
                var move = cc.sequence(cc.delayTime(Game_1.default.DEAL_SPEED), cc.moveTo(0.2, pos));
                card.node.runAction(move);
            });
            setTimeout(function () {
                _this.cards = _this.cards.filter(function (card, index) { return index !== 1; });
            }, 0);
            _this.updateCardCounter();
            var point = _this.getPoint();
            _this.updatePoint(point.toString());
        };
        _this.showCard2 = function () {
            // if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            //     console.log('====================================');
            //     console.log(123);
            //     console.log('====================================');
            //     this.cards[1].show()
            // }
        };
        _this.getPoint = function () {
            var point = _this.cards.reduce(function (point, current) {
                if (current.rank === 1) {
                    return point;
                }
                else if (current.rank > 10) {
                    return point + 10;
                }
                else {
                    return point + current.rank;
                }
            }, 0);
            if (_this.numOfAce > 0) {
                if (point > 11) {
                    point += _this.numOfAce * 1;
                }
                else {
                    point += _this.numOfAce * 11;
                }
            }
            _this.point = point;
            return point;
            // if (point > 21) {
            //     let txtThua = `$user thua`
            //     if (this.isCai()) {
            //         return alert('cai thua: ' + point + ' diem')
            //     } else if (this.isBot()) {
            //         alert('bot thua: ' + point + ' diem')
            //     } else {
            //         alert('user thua: ' + point + ' diem')
            //     }
            // }
        };
        _this.checkBlackJack = function () {
            var _a, _b;
            var rank1 = (_a = _this.cards[0]) === null || _a === void 0 ? void 0 : _a.rank;
            var rank2 = (_b = _this.cards[1]) === null || _b === void 0 ? void 0 : _b.rank;
            if (rank1 === 1 || rank2 === 1) {
                if (rank1 > 9 || rank2 > 9) {
                    _this.setInRound(false);
                    _this.updatePoint("Black Jack!");
                    return true;
                }
            }
            return false;
        };
        return _this;
    }
    Player_1 = Player;
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Player.prototype.start = function () {
        this.fold.active = false;
        this.timer.hide();
        this.marker.active = false;
        // this.cardMargin = this.seat == 0 ? Player.CARD_MARGIN :
        //     0.5 * Player.CARD_MARGIN;
        this.cardMargin = 0.5 * Player_1.CARD_MARGIN;
        this.cardDirection = this.seat == 1 ? -1 : 1;
        this.labelCardCounter = this.marker.getComponentInChildren(cc.Label);
    };
    // update (dt) {}
    Player.prototype.show = function () {
        this.node.active = true;
    };
    Player.prototype.hide = function () {
        this.node.active = false;
    };
    Player.prototype.isBot = function () {
        return this.seat !== Config_1.default.totalPlayer / 2;
        return this.seat > 0;
    };
    Player.prototype.isUser = function () {
        return this.isSubUser || this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.isCai = function () {
        return this.seat === 0;
        return this.seat === Config_1.default.totalPlayer / 2;
    };
    Player.prototype.setInRound = function (inRound) {
        this.fold.active = !inRound;
        this.inRound = inRound;
    };
    Player.prototype.isInRound = function () {
        return this.inRound;
    };
    Player.prototype.addCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '+' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(0xf9, 0xd2, 0x1e, 255);
        this.effect.play();
        this.setCoin(this.coinVal + val);
    };
    Player.prototype.subCoin = function (val) {
        this.effect.getComponent(cc.Label).string = '-' + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(255, 0, 0, 255);
        this.effect.play();
        this.setCoin(this.coinVal - val);
    };
    Player.prototype.setCoin = function (val) {
        this.coinVal = val;
        this.coin.string = util_1.default.numberFormat(val);
    };
    Player.prototype.getCoin = function () {
        return this.coinVal;
    };
    Player.prototype.setUsername = function (val) {
        this.username.string = val;
    };
    Player.prototype.setAvatar = function (sprite) {
        this.avatar.spriteFrame = sprite;
    };
    Player.prototype.reset = function () {
        this.marker.active = false;
        this.fold.active = false;
        this.cards = [];
        this.isSubUser = false;
        this.inRound = true;
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        this.timer.hide();
        this.numOfAce = 0;
        this.point = 0;
        this.bonusType = 0;
        this.isSplit = 0;
        if (this.subUser) {
            this.subUser.reset();
            this.subUser = null;
        }
    };
    Player.prototype.push = function (card, delay) {
        var _this = this;
        if (!this.inRound && this.subUser)
            return this.subUser.push(card, delay);
        card.node.zIndex = this.cards.length;
        this.cards.push(card);
        if (card.rank === 1)
            this.numOfAce += 1;
        var point = this.getPoint();
        var fun = function () {
            _this.updatePoint(point == 21 && _this.cards.length === 2 ? 'BlackJack' : point.toString());
            if (point > 20)
                _this.inRound = false;
            _this.cards[0].show();
            if (_this.isUser()) {
                card.show();
            }
            else {
                _this.updateCardCounter();
            }
        };
        if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            fun = function () {
                _this.cards[1].show();
                _this.updatePoint(null);
            };
        }
        // this.updatePoint()
        var pos = this.getCardRootPosition();
        var scale = cc.sequence(cc.delayTime(delay), cc.scaleTo(0.2, 0.5));
        card.node.runAction(scale);
        // if (this.isBot()) {
        //     if (this.cards.length < 3) {
        //         pos.x += (this.cards.length - 1) * this.cardMargin;
        //     } else {
        //         pos.x += 2 * this.cardMargin;
        //     }
        // } else {
        pos.x += (this.cards.length - 1) * this.cardMargin;
        // }
        var move = cc.sequence(cc.delayTime(delay), cc.moveTo(0.2, pos), cc.callFunc(fun));
        card.node.runAction(move);
    };
    Player.prototype.touch = function (pos) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var card = this.cards[i];
            if (card.node.getBoundingBoxToWorld().contains(pos)) {
                return card;
            }
        }
        return null;
    };
    Player.prototype.selectCard = function (card) {
        // this.prepareCards.push(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y + 30);
    };
    Player.prototype.unselectCard = function (card) {
        // this.prepareCards.remove(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y);
    };
    Player.prototype.removeCards = function (cards) {
        for (var i = cards.count() - 1; i >= 0; --i) {
            Helper_1.default.removeBy(this.cards, cards.at(i));
        }
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
    };
    Player.prototype.setActive = function (on) {
        if (on) {
            this.timer.show(Player_1.TIME);
        }
        else {
            this.timer.hide();
        }
    };
    Player.prototype.setTimeCallback = function (selector, selectorTarget, data) {
        this.timer.onCompleted(selector, selectorTarget, data);
    };
    Player.prototype.showHandCards = function () {
        for (var i = 0; i < this.cards.length; i++) {
            this.cards[i].show();
        }
    };
    Player.prototype.reorder = function () {
        for (var i = this.cards.length - 1; i >= 0; --i) {
            var pos = this.getCardRootPosition();
            pos.x += i * this.cardMargin * this.cardDirection;
            var card = this.cards[i];
            card.node.setPosition(pos);
            card.node.zIndex = this.seat == 1 ? this.cards.length - i : i;
        }
    };
    Player.prototype.sortHandCards = function () {
        Helper_1.default.sort(this.cards);
        this.reorder();
    };
    Player.prototype.showCardCounter = function () {
        this.marker.active = true;
        this.updateCardCounter();
    };
    Player.prototype.hideCardCounter = function () {
        this.marker.active = false;
    };
    Player.prototype.updateCardCounter = function () {
        try {
            this.labelCardCounter.string = this.cards.length.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.updatePoint = function (label) {
        if (Number(this.pointLbl.string) === NaN)
            return;
        this.point = this.getPoint();
        try {
            this.pointLbl.string = label || this.point.toString();
        }
        catch (error) {
        }
    };
    Player.prototype.getCardRootPosition = function () {
        if (!this.cardRootPosition) {
            this.cardRootPosition = cc.v2(this.marker.position.x * this.node.scaleX + this.node.position.x, this.marker.position.y * this.node.scaleY + this.node.position.y);
        }
        return cc.v2(this.cardRootPosition);
    };
    var Player_1;
    Player.CARD_MARGIN = 60;
    Player.TIME = 30;
    __decorate([
        property(cc.Node)
    ], Player.prototype, "fold", void 0);
    __decorate([
        property(Timer_1.default)
    ], Player.prototype, "timer", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "coin", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "username", void 0);
    __decorate([
        property(cc.Node)
    ], Player.prototype, "marker", void 0);
    __decorate([
        property(cc.Sprite)
    ], Player.prototype, "avatar", void 0);
    __decorate([
        property(TweenMove_1.default)
    ], Player.prototype, "effect", void 0);
    __decorate([
        property(cc.Label)
    ], Player.prototype, "pointLbl", void 0);
    Player = Player_1 = __decorate([
        ccclass
    ], Player);
    return Player;
}(cc.Component));
exports.default = Player;

cc._RF.pop();