"use strict";
cc._RF.push(module, '3b07by6DGRJu75lIR9EK7nZ', 'th_TH');
// lang/th_TH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'เล่นเลย',
    'WIN': 'ที่ 1',
    'LOSE2': 'ครั้งที่ 2',
    'LOSE3': 'วันที่ 3',
    'LOSE4': 'อันดับ 4',
    'PLAYER': 'จำนวนผู้เล่น',
    'BETNO': 'เดิมพัน',
    'DAILY': 'รางวัลรายวัน',
    'D1': 'วันที่ 1',
    'D2': 'วันที่ 2',
    'D3': 'วันที่ 3',
    'D4': 'วันที่ 4',
    'D5': 'วันที่ 5',
    'D6': 'วันที่ 6',
    'D7': 'วันที่ 7',
    '3TURNS': '3 รอบ',
    '5TURNS': '5 รอบ',
    'TURNS': 'รอบ',
    'RECEIVED': 'ได้รับ',
    'LEADER': 'ลีดเดอร์บอร์ด',
    'NOVIDEO': 'ไม่สามารถเล่นวิดีโอได้ในขณะนี้',
    'BET': 'เดิมพัน',
    'NO': 'จำนวน',
    'PASS': 'ผ่าน',
    'HIT': 'ตี',
    'ARRANGE': 'จัด',
    'QUITGAME': 'ออกจากเกม',
    'QUITGAMEP': 'คุณต้องการออกจากเกมหรือไม่? หากคุณออกจากเกมคุณจะเสียสิบเท่าของระดับการเดิมพัน',
    'QUIT': 'เลิก',
    '3PAIRS': '3 คู่ติดต่อกัน',
    '4PAIRS': '4 คู่ติดต่อกัน',
    'FOURKIND': 'สี่ชนิด',
    'FLUSH': 'สเตรทฟลัช',
    '1BEST': 'ดีที่สุด',
    '2BEST': '2 ดีที่สุด',
    '3BEST': '3 ดีที่สุด',
    'INSTRUCT': 'คำแนะนำ',
    'NOMONEY': 'คุณหมดเงินคลิกเพื่อรับเงินเพิ่ม',
    'RECEI2': 'ได้รับ',
    'SPINNOW': 'หมุน',
    'SPIN': 'หมุน',
    '1TURN': 'อีก 1 เทิร์น',
    'LUCKYSPIN': 'หมุนโชคดี',
    'LUCK': 'มีโชคในภายหลัง',
    'TURN': 'อีกหนึ่งเทิร์น',
    'X2': 'X2 เงิน',
    'X3': 'X3 เงิน',
    'X5': 'X5 เงิน',
    'X10': 'X10 เงิน',
    'MISS': 'นางสาว',
    'MONEY1': 'ยินดีด้วย! คุณมีเงิน %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();