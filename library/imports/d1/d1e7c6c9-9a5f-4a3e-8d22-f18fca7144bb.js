"use strict";
cc._RF.push(module, 'd1e7cbJml9KPo0i8Y/KcUS7', 'Timer');
// Scripts/Timer.ts

"use strict";
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Timer = /** @class */ (function (_super) {
    __extends(Timer, _super);
    function Timer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sprite = null;
        _this.timer = 0;
        _this.duration = 0;
        _this.completed = null;
        _this._selectorTarget = null;
        _this.target = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Timer.prototype.onLoad = function () {
        this.sprite = this.getComponent(cc.Sprite);
    };
    Timer.prototype.start = function () {
    };
    Timer.prototype.update = function (dt) {
        this.timer += dt;
        if (this.timer < this.duration) {
            this.sprite.fillRange = this.timer / this.duration;
            return;
        }
        this.node.active = false;
        if (this.completed != null) {
            this.completed.call(this._selectorTarget, this.target);
        }
    };
    Timer.prototype.onCompleted = function (selector, selectorTarget, target) {
        this.completed = selector;
        this.target = target;
        this._selectorTarget = selectorTarget;
    };
    Timer.prototype.show = function (time) {
        this.duration = time;
        this.timer = 0;
        this.node.active = true;
    };
    Timer.prototype.hide = function () {
        this.node.active = false;
    };
    Timer = __decorate([
        ccclass
    ], Timer);
    return Timer;
}(cc.Component));
exports.default = Timer;

cc._RF.pop();