"use strict";
cc._RF.push(module, '95e70d0JFBKabdpj8E+Dcll', 'tl_PH');
// lang/tl_PH.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var th_TH_tut_1 = require("./th_TH_tut");
exports.default = {
    'PLAYNOW': 'Maglaro',
    'WIN': 'Ika-1',
    'LOSE2': 'Ika-2',
    'LOSE3': 'Ika-3',
    'LOSE4': 'Ika-4',
    'PLAYER': 'bilang ng mga manlalaro',
    'BETNO': 'Taya',
    'DAILY': 'Daily reward',
    'D1': 'Araw 1',
    'D2': 'Araw 2',
    'D3': 'Araw 3',
    'D4': 'Araw 4',
    'D5': 'Araw 5',
    'D6': 'Araw 6',
    'D7': 'Araw 7',
    '3TURNS': '3 liko',
    '5TURNS': '5 liko',
    'TURNS': 'liko',
    'RECEIVED': 'Habol',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Hindi maaaring i-play ang video ngayon',
    'BET': 'Taya',
    'NO': 'Numero',
    'PASS': 'Pumasa',
    'HIT': 'Hit',
    'ARRANGE': 'Ayusin',
    'QUITGAME': 'Tumigil sa laro',
    'QUITGAMEP': 'Kung huminto ka sa laro mawawala sa iyo ng sampung beses bilang antas ng pusta',
    'QUIT': 'Umalis na',
    '3PAIRS': '3 magkasunod na pares',
    '4PAIRS': '4 magkasunod na pares',
    'FOURKIND': 'Apat ng isang uri',
    'FLUSH': 'Straight Flush',
    '1BEST': 'pinakamahusay',
    '2BEST': '2 pinakamahusay',
    '3BEST': '3 pinakamahusay',
    'INSTRUCT': 'Panuto',
    'NOMONEY': 'Naubusan ka ng pera, mag-click upang makatanggap ng mas maraming pera',
    'RECEI2': 'Habol',
    'SPINNOW': 'Paikutin',
    'SPIN': 'Paikutin',
    '1TURN': '1 pa naman',
    'LUCKYSPIN': 'masuwerteng paikutin',
    'LUCK': 'Mag swerte ka mamaya',
    'TURN': 'Isa pa',
    'X2': 'X2 pera',
    'X3': 'X3 pera',
    'X5': 'X5 pera',
    'X10': 'X10 pera',
    'MISS': 'Miss',
    'MONEY1': 'Pagbati! Mayroon kang %s',
    'TUT': th_TH_tut_1.default
};

cc._RF.pop();