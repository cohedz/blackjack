window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  Api: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cffd1wdHiNPIaFpHSP+5t/F", "Api");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Config_1 = require("./Config");
    var image_1 = require("./image");
    var Api = function() {
      function Api() {}
      Api.initAsync = function(callback, leaderboard) {
        if (Api.initialized) {
          Api.loadLeaderboard(leaderboard);
          return callback(false, Api.dailyBonusDay);
        }
        if (!Api.initialized && window.hasOwnProperty("FBInstant")) {
          Api.initialized = true;
          Api.photo = FBInstant.player.getPhoto();
          Api.username = FBInstant.player.getName();
          Api.playerId = FBInstant.player.getID();
          Api.locale = FBInstant.getLocale();
          Api.getPlayerData(callback);
          Api.loadLeaderboard(leaderboard);
          Api.subscribeBot();
          Api.createShortcut();
        } else {
          Api.ticket = 10;
          Api.dailyBonusDay = 0;
          Api.locale = "vi_VN";
          Api.loadLeaderboard(leaderboard);
        }
      };
      Api.getPlayerData = function(callback) {
        FBInstant.player.getDataAsync([ "coin", "ticket", "daily_bonus_claimed_at", "daily_bonus_day" ]).then(function(data) {
          cc.log("data is loaded");
          Api.coin = data["coin"];
          if (void 0 == Api.coin || null == Api.coin) {
            Api.coin = Config_1.default.defaultCoin;
            Api.isDirty = true;
          }
          Api.ticket = data["ticket"] || 0;
          Api.dailyBonusDay = data["daily_bonus_day"] || 0;
          Api.dailyBonusClaimed = !Api.dailyBonusClaimable(data["daily_bonus_claimed_at"]);
          callback(!Api.dailyBonusClaimed, Api.dailyBonusDay);
          Api.isDirty && Api.flush();
        });
      };
      Api.subscribeBot = function() {
        FBInstant.player.canSubscribeBotAsync().then(function(can_subscribe) {
          cc.log("subscribeBotAsync", can_subscribe);
          Api.canSubscribeBot = can_subscribe.valueOf();
          Api.canSubscribeBot && FBInstant.player.subscribeBotAsync().then().catch(function(e) {
            cc.log("subscribeBotAsync");
          });
        }).catch(function(e) {
          cc.log("subscribeBotAsync", e);
        });
      };
      Api.createShortcut = function() {
        FBInstant.canCreateShortcutAsync().then(function(canCreateShortcut) {
          canCreateShortcut && FBInstant.createShortcutAsync().then(function() {}).catch(function() {});
        });
      };
      Api.loadLeaderboard = function(leaderboard) {
        fetch(Api.lb_api + "/" + Api.lb_id).then(function(response) {
          return response.json();
        }).then(function(res) {
          for (var i = 0; i < res.entries.length; i++) {
            var e = res.entries[i];
            leaderboard.render(i + 1, e.username, e.score, e.photo, e.user_id);
          }
          leaderboard.onLoadComplete();
        }).catch(function(error) {
          return console.error(error);
        });
      };
      Api.preloadRewardedVideo = function(callback) {
        void 0 === callback && (callback = null);
        callback && callback();
        return;
      };
      Api.showRewardedVideo = function(success, error) {
        success();
      };
      Api.preloadInterstitialAd = function(callback) {
        void 0 === callback && (callback = null);
        callback && callback();
      };
      Api.showInterstitialAd = function() {};
      Api.isInterstitialAdLoaded = function() {
        if (!window.hasOwnProperty("FBInstant")) return false;
        return !!Api.preloadedInterstitial;
      };
      Api.challenge = function(playerId) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, FBInstant.context.createAsync(playerId) ];

             case 1:
              _a.sent();
              return [ 2, FBInstant.updateAsync({
                action: "CUSTOM",
                template: "play_turn",
                cta: "Ch\u01a1i ngay",
                image: image_1.default,
                text: Api.username + " \u0111ang th\xe1ch \u0111\u1ea5u b\u1ea1n. Nh\u1ea5n ch\u01a1i ngay!",
                strategy: "IMMEDIATE"
              }) ];
            }
          });
        });
      };
      Api.shareAsync = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            if (window.hasOwnProperty("FBInstant")) return [ 2, FBInstant.shareAsync({
              intent: "SHARE",
              image: image_1.default,
              text: "Ti\u1ebfn li\xean mi\u1ec1n nam 2020!"
            }) ];
            return [ 2, Promise.resolve() ];
          });
        });
      };
      Api.invite = function(success, error) {
        window.hasOwnProperty("FBInstant") ? FBInstant.context.chooseAsync().then(function() {
          success(FBInstant.context.getID());
          FBInstant.updateAsync({
            action: "CUSTOM",
            template: "play_turn",
            cta: "Ch\u01a1i ngay",
            image: image_1.default,
            text: Api.username + " m\u1eddi b\u1ea1n c\xf9ng ch\u01a1i game ti\u1ebfn li\xean mi\u1ec1n nam",
            strategy: "IMMEDIATE"
          });
        }).catch(error) : success("");
      };
      Api.coinIncrement = function(val) {
        Api.updateCoin(Api.coin + val);
      };
      Api.updateCoin = function(coin) {
        Api.coin = coin;
        window.hasOwnProperty("FBInstant") && FBInstant.player.setDataAsync({
          coin: Api.coin
        }).then(function() {
          cc.log("data is set");
        });
        Api.setScoreAsync(Api.coin).then(function() {
          return cc.log("Score saved");
        }).catch(function(err) {
          return console.error(err);
        });
      };
      Api.updateTicket = function() {
        window.hasOwnProperty("FBInstant") && FBInstant.player.setDataAsync({
          ticket: Api.ticket
        }).then(function() {
          cc.log("ticket is set");
        });
      };
      Api.claimDailyBonus = function(day) {
        Api.dailyBonusClaimed = true;
        if (window.hasOwnProperty("FBInstant")) {
          var claimed_at = new Date();
          FBInstant.player.setDataAsync({
            coin: Api.coin,
            ticket: Api.ticket,
            daily_bonus_claimed_at: claimed_at.toString(),
            daily_bonus_day: day
          }).then(function() {
            cc.log("data is set");
          }, function(reason) {
            cc.log("data is not set", reason);
          });
        }
        Api.dailyBonusDay++;
      };
      Api.dailyBonusClaimable = function(claimedAt) {
        if (!claimedAt) return true;
        "string" == typeof claimedAt && (claimedAt = new Date(claimedAt));
        var now = new Date();
        if (now.getFullYear() != claimedAt.getFullYear()) return true;
        if (now.getMonth() != claimedAt.getMonth()) return true;
        if (now.getDate() != claimedAt.getDate()) return true;
        return false;
      };
      Api.flush = function() {
        window.hasOwnProperty("FBInstant") && FBInstant.player.setDataAsync({
          coin: Api.coin,
          ticket: Api.ticket
        }).then(function() {
          cc.log("data is set");
        });
        Api.setScoreAsync(Api.coin).then(function(res) {
          return res.json();
        }).then(function(res) {
          return cc.log(res);
        }).catch(function(err) {
          return console.error(err);
        });
      };
      Api.logEvent = function(eventName, valueToSum, parameters) {
        try {
          window.firebase.analytics().logEvent(eventName, parameters);
          console.log("====================================");
          console.log(eventName, parameters);
          console.log("====================================");
        } catch (error) {
          console.log("====================================");
          console.log("analytic error", error);
          console.log("====================================");
        }
      };
      Api.setScoreAsync = function(score) {
        var data = {
          score: score,
          userId: Api.playerId,
          name: Api.username,
          photo: Api.photo
        };
        return fetch(Api.lb_api + "/" + Api.lb_id, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
      };
      Api.randomBonus = function() {
        var r = Math.random();
        if (r < .05) return 4e5;
        if (r < .2) return 3e5;
        if (r < .5) return 2e5;
        return 1e5;
      };
      Api.initialized = false;
      Api.coin = 1e4;
      Api.ticket = 0;
      Api.username = "DM";
      Api.playerId = "DM";
      Api.locale = "vi_VN";
      Api.photo = "https://i.imgur.com/FUOxs43.jpg";
      Api.isDirty = false;
      Api.preloadedRewardedVideo = null;
      Api.preloadedInterstitial = null;
      Api.canSubscribeBot = false;
      Api.dailyBonusDay = 0;
      Api.dailyBonusClaimed = false;
      Api.lb_api = "https://lb.dozo.vn/api";
      Api.lb_id = "5f714415630b9b9ff8146f15";
      return Api;
    }();
    exports.default = Api;
    cc._RF.pop();
  }, {
    "./Config": "Config",
    "./image": "image"
  } ],
  AudioPlayer: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a8fc4W4Bk5LCI1G39gq+6eK", "AudioPlayer");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Config_1 = require("./Config");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AudioPlayer = function(_super) {
      __extends(AudioPlayer, _super);
      function AudioPlayer() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.audioClip = null;
        return _this;
      }
      AudioPlayer.prototype.start = function() {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;
        clickEventHandler.component = "AudioPlayer";
        clickEventHandler.handler = "play";
        var button = this.node.getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
      };
      AudioPlayer.prototype.play = function() {
        Config_1.default.soundEnable && cc.audioEngine.play(this.audioClip, false, 1);
      };
      __decorate([ property({
        type: cc.AudioClip
      }) ], AudioPlayer.prototype, "audioClip", void 0);
      AudioPlayer = __decorate([ ccclass ], AudioPlayer);
      return AudioPlayer;
    }(cc.Component);
    exports.default = AudioPlayer;
    cc._RF.pop();
  }, {
    "./Config": "Config"
  } ],
  AutoHide: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1ab56S/omxH+qPXcysDsNwA", "AutoHide");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var AutoHide = function(_super) {
      __extends(AutoHide, _super);
      function AutoHide() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.duration = 3;
        _this.elapsed = 0;
        return _this;
      }
      AutoHide.prototype.start = function() {};
      AutoHide.prototype.update = function(dt) {
        this.elapsed += dt;
        this.elapsed >= this.duration && (this.node.active = false);
      };
      AutoHide.prototype.show = function(sender) {
        this.label.string = "T\xcdnh n\u0103ng s\u1eafp ra m\u1eaft...";
        this.elapsed = 0;
        this.node.active = true;
      };
      AutoHide.prototype.openWithText = function(sender, string) {
        this.label.string = string;
        this.elapsed = 0;
        this.node.active = true;
      };
      __decorate([ property(cc.Label) ], AutoHide.prototype, "label", void 0);
      __decorate([ property ], AutoHide.prototype, "duration", void 0);
      AutoHide = __decorate([ ccclass ], AutoHide);
      return AutoHide;
    }(cc.Component);
    exports.default = AutoHide;
    cc._RF.pop();
  }, {} ],
  Bot: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2fab9HUN9BE+7ULtb/jaU1f", "Bot");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CardGroup_1 = require("./CardGroup");
    var Helper_1 = require("./Helper");
    var Bot = function() {
      function Bot() {}
      Bot.random = function(handCards) {
        var cards = null;
        var firstCard = Helper_1.default.findCard(handCards, 3, 1);
        if (firstCard) {
          var straight = Bot.findAllCardByRank(handCards, 3);
          if (straight.length >= 3) return new CardGroup_1.default(straight, CardGroup_1.default.House);
          return new CardGroup_1.default([ firstCard ], CardGroup_1.default.Single);
        }
        cards = Bot.findStraightCard(handCards);
        if (cards) return new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        cards = Bot.findHouseCard(handCards);
        if (cards && !(15 == cards[0].rank && cards.length < handCards.length)) return new CardGroup_1.default(cards, CardGroup_1.default.House);
        var card = Helper_1.default.findMinCard(handCards);
        return new CardGroup_1.default([ card ], CardGroup_1.default.Single);
      };
      Bot.suggest = function(commonCards, handCards) {
        var common = commonCards.peek();
        if (15 == common.highest.rank && 3 == common.count()) return null;
        if (15 == common.highest.rank && 2 == common.count()) {
          var cards = Helper_1.default.findMultiPairCard(handCards, null, 8);
          if (cards) return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
          var four = Helper_1.default.findHouseCard(handCards, null, 4);
          if (four) return new CardGroup_1.default(four, CardGroup_1.default.House);
          var pair = Helper_1.default.findHouseCard(handCards, common.highest, 2);
          if (pair) return new CardGroup_1.default(pair, CardGroup_1.default.House);
          return null;
        }
        if (15 == common.highest.rank && 1 == common.count()) {
          var cards = Helper_1.default.findMultiPairCard(handCards, null, 6);
          if (cards) return new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
          var four = Helper_1.default.findHouseCard(handCards, null, 4);
          if (four) return new CardGroup_1.default(four, CardGroup_1.default.House);
          var card = Helper_1.default.findMinCard(handCards, common.highest);
          if (card) return new CardGroup_1.default([ card ], CardGroup_1.default.Single);
          return null;
        }
        if (common.kind == CardGroup_1.default.Single) {
          var cards = Helper_1.default.findMinCard(handCards, common.highest);
          return null == cards ? null : new CardGroup_1.default([ cards ], CardGroup_1.default.Single);
        }
        if (common.kind == CardGroup_1.default.House) {
          var cards = Helper_1.default.findHouseCard(handCards, common.highest, common.count());
          return null == cards ? null : new CardGroup_1.default(cards, CardGroup_1.default.House);
        }
        if (common.kind == CardGroup_1.default.Straight) {
          var cards = Helper_1.default.findStraightCard(handCards, common);
          return null == cards ? null : new CardGroup_1.default(cards, CardGroup_1.default.Straight);
        }
        if (common.kind == CardGroup_1.default.MultiPair) {
          var cards = Helper_1.default.findMultiPairCard(handCards, common.highest, common.count() / 2);
          return null == cards ? null : new CardGroup_1.default(cards, CardGroup_1.default.MultiPair);
        }
        return null;
      };
      Bot.findStraightCard = function(cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
          var cardStarted = cards[i];
          if (cardStarted.rank < 13) {
            var straight = Bot._findStraightCard(cards, cardStarted);
            if (straight.length >= 3) return straight;
          }
        }
        return null;
      };
      Bot._findStraightCard = function(cards, started) {
        var straight = [ started ];
        for (var c = started.rank + 1; c < 15; c++) {
          var card = Helper_1.default.findCardByRank(cards, c);
          if (null == card) break;
          straight.push(card);
        }
        return straight;
      };
      Bot.findHouseCard = function(cards) {
        Helper_1.default.sort(cards);
        for (var i = 0; i < cards.length; i++) {
          var cardStarted = cards[i];
          var straight = Bot.findAllCardByRank(cards, cardStarted.rank);
          if (straight.length >= 2) return straight;
        }
        return null;
      };
      Bot.findAllCardByRank = function(cards, rank) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) cards[i].rank == rank && setCards.push(cards[i]);
        return setCards;
      };
      return Bot;
    }();
    exports.default = Bot;
    cc._RF.pop();
  }, {
    "./CardGroup": "CardGroup",
    "./Helper": "Helper"
  } ],
  CardGroup: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bece1QhfllOVa5CfNF0TmNb", "CardGroup");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Helper_1 = require("./Helper");
    var CardGroup = function() {
      function CardGroup(cards, kind) {
        this.cards = cards;
        this.kind = CardGroup.Single;
        this.kind = kind;
        0 != kind && (this.highest = Helper_1.default.findMaxCard(cards));
      }
      CardGroup.prototype.getPosition = function() {
        if (this.cards.length % 2 != 0) {
          var idx = (this.cards.length - 1) / 2;
          return this.cards[idx].node.getPosition();
        }
        var middle = this.cards.length / 2;
        var c1 = this.cards[middle].node.getPosition();
        var c2 = this.cards[middle - 1].node.getPosition();
        return cc.v2((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
      };
      CardGroup.prototype.gt = function(o) {
        if (this.kind == o.kind && this.count() == o.count()) return this.highest.gt(o.highest);
        if (15 == o.highest.rank && 1 == o.count()) {
          if (this.kind == CardGroup.MultiPair) return true;
          if (this.kind == CardGroup.House && 4 == this.count()) return true;
          return false;
        }
        if (15 == o.highest.rank && 2 == o.count()) {
          if (this.kind == CardGroup.MultiPair && 8 == this.count()) return true;
          if (this.kind == CardGroup.House && 4 == this.count()) return true;
          return false;
        }
        if (o.kind == CardGroup.MultiPair && 6 == o.count()) {
          if (this.kind == CardGroup.House && 4 == this.count()) return true;
          if (this.kind == CardGroup.MultiPair && 8 == this.count()) return true;
        }
        if (o.kind == CardGroup.House && 4 == o.count()) return this.kind == CardGroup.MultiPair && 8 == this.count();
        return false;
      };
      CardGroup.prototype.at = function(i) {
        return this.cards[i];
      };
      CardGroup.prototype.count = function() {
        return this.cards.length;
      };
      CardGroup.prototype.push = function(card) {
        this.cards.push(card);
      };
      CardGroup.prototype.remove = function(card) {
        var index = this.cards.indexOf(card, 0);
        this.cards.splice(index, 1);
      };
      CardGroup.prototype.contains = function(card) {
        for (var i = this.cards.length - 1; i >= 0; --i) if (this.cards[i] == card) return true;
        return false;
      };
      CardGroup.prototype.calculate = function() {
        if (1 == this.cards.length) {
          this.kind = CardGroup.Single;
          this.highest = Helper_1.default.findMaxCard(this.cards);
        } else if (Helper_1.default.isHouse(this.cards)) {
          this.kind = CardGroup.House;
          this.highest = Helper_1.default.findMaxCard(this.cards);
        } else if (Helper_1.default.isStraight(this.cards)) {
          this.kind = CardGroup.Straight;
          this.highest = Helper_1.default.findMaxCard(this.cards);
        } else if (Helper_1.default.isMultiPair(this.cards)) {
          this.kind = CardGroup.MultiPair;
          this.highest = Helper_1.default.findMaxCard(this.cards);
        } else this.kind = CardGroup.Ungrouped;
      };
      CardGroup.prototype.sort = function() {
        Helper_1.default.sort(this.cards);
      };
      CardGroup.prototype.isInvalid = function() {
        return this.kind == CardGroup.Ungrouped;
      };
      CardGroup.prototype.dump = function() {
        for (var i = this.cards.length - 1; i >= 0; --i) console.log(this.cards[i].toString());
      };
      CardGroup.Ungrouped = 0;
      CardGroup.Single = 1;
      CardGroup.House = 2;
      CardGroup.Straight = 3;
      CardGroup.Boss = 4;
      CardGroup.MultiPair = 5;
      return CardGroup;
    }();
    exports.default = CardGroup;
    cc._RF.pop();
  }, {
    "./Helper": "Helper"
  } ],
  Card: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5e609fdf41BH6QiJ7pgTFU1", "Card");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Card = function(_super) {
      __extends(Card, _super);
      function Card() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.back = null;
        _this.front = null;
        _this.rank = 1;
        _this.suit = 1;
        _this.overlayer = null;
        return _this;
      }
      Card.prototype.onLoad = function() {
        this.front = this.getComponent(cc.Sprite).spriteFrame;
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.overlayer = this.node.children[this.node.children.length - 1];
      };
      Card.prototype.start = function() {
        this.overlayer.active = false;
      };
      Card.prototype.reset = function() {
        this.node.active = true;
        this.node.setScale(1);
        this.overlayer.active = false;
        this.node.setPosition(cc.Vec2.ZERO);
        this.hide();
      };
      Card.prototype.show = function() {
        this.getComponent(cc.Sprite).spriteFrame = this.front;
        this.setChildrenActive(true);
      };
      Card.prototype.hide = function() {
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.setChildrenActive(false);
      };
      Card.prototype.setChildrenActive = function(active) {
        for (var i = 0; i < this.node.children.length - 1; i++) this.node.children[i].active = active;
      };
      Card.prototype.overlap = function() {
        this.overlayer.active = true;
      };
      Card.prototype.setPositionY = function(newPosY) {
        this.node.setPosition(this.node.position.x, newPosY);
      };
      Card.prototype.compare = function(o) {
        if (null == o) return 1;
        if (this.rank > o.rank) return 1;
        if (this.rank < o.rank) return -1;
        if (this.suit > o.suit) return 1;
        if (this.suit < o.suit) return -1;
        return 0;
      };
      Card.prototype.gt = function(o) {
        return this.compare(o) > 0;
      };
      Card.prototype.lt = function(o) {
        return this.compare(o) < 0;
      };
      Card.prototype.toString = function() {
        return "Rank: " + this.rank + " Suit: " + this.suit;
      };
      __decorate([ property(cc.SpriteFrame) ], Card.prototype, "back", void 0);
      __decorate([ property(cc.SpriteFrame) ], Card.prototype, "front", void 0);
      Card = __decorate([ ccclass ], Card);
      return Card;
    }(cc.Component);
    exports.default = Card;
    cc._RF.pop();
  }, {} ],
  CommonCard: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1351esCO5tMRYPcn390B3zj", "CommonCard");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CommonCard = function() {
      function CommonCard() {
        this.totalCards = 0;
        this.cards = [];
        this.combat = [];
        this.latest = null;
      }
      CommonCard.prototype.getPosition = function() {
        if (0 == this.cards.length) return cc.Vec2.ZERO;
        var sx = 100 * Math.random() - 50;
        var sy = 100 * Math.random() - 50;
        return cc.v2(sx, sy + 50);
      };
      CommonCard.prototype.reset = function() {
        this.totalCards = 0;
        this.cards = [];
        this.latest = null;
        this.combat.length > 0 && (this.combat = []);
      };
      CommonCard.prototype.push = function(cards) {
        this.totalCards += cards.count();
        this.cards.push(cards);
        this.latest && this.overlapCard(this.latest);
        this.latest = cards;
      };
      CommonCard.prototype.isCombatOpen = function() {
        return this.combat.length > 0;
      };
      CommonCard.prototype.pushCombat = function(player, cards) {
        this.combat.push({
          player: player,
          cards: cards
        });
      };
      CommonCard.prototype.hasCombat = function() {
        if (this.combat.length < 2) return false;
        var peek = this.combat[this.combat.length - 1];
        if (15 == peek.cards.highest.rank) return false;
        return true;
      };
      CommonCard.prototype.getCombat = function() {
        return this.combat[0].cards;
      };
      CommonCard.prototype.getCombatLength = function() {
        return this.combat.length;
      };
      CommonCard.prototype.getCombatWinner = function() {
        return this.combat[this.combat.length - 1].player;
      };
      CommonCard.prototype.getCombatVictim = function() {
        return this.combat[this.combat.length - 2].player;
      };
      CommonCard.prototype.resetCombat = function() {
        this.combat = [];
      };
      CommonCard.prototype.peek = function() {
        return this.cards[this.cards.length - 1];
      };
      CommonCard.prototype.length = function() {
        return this.cards.length;
      };
      CommonCard.prototype.isEmpty = function() {
        return 0 == this.cards.length;
      };
      CommonCard.prototype.nextRound = function() {
        for (var i = this.cards.length - 1; i >= 0; --i) for (var j = this.cards[i].count() - 1; j >= 0; --j) {
          var card = this.cards[i].at(j);
          card.hide();
          card.node.setScale(.5);
        }
        this.latest && this.overlapCard(this.latest);
        this.combat.length > 0 && (this.combat = []);
        this.cards = [];
        this.latest = null;
      };
      CommonCard.prototype.overlapCard = function(cards) {
        for (var i = 0; i < cards.count(); i++) cards.at(i).overlap();
      };
      return CommonCard;
    }();
    exports.default = CommonCard;
    cc._RF.pop();
  }, {} ],
  Config: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a4c56iZ0yNPbYQ1DV9FQmk8", "Config");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Config = function() {
      function Config() {}
      Config.language = "vi";
      Config.defaultCoin = 1e5;
      Config.botCoin = 1e5;
      Config.bankrupt = 100;
      Config.bankrupt_bonus = 5e5;
      Config.minBet = 3e3;
      Config.maxBet = 1e5;
      Config.betValue = 3e3;
      Config.totalPlayer = 6;
      Config.userphoto = null;
      Config.battle = null;
      Config.intertital_ads = "717961958974985_757265555044625";
      Config.reward_video = "316445729595977_363105338263349";
      Config.gift_reward_video = "316445729595977_363105338263349";
      Config.soundEnable = true;
      Config.dailyBonus = [ {
        coin: 1e5
      }, {
        ticket: 3
      }, {
        coin: 5e5
      }, {
        coin: 1e6
      }, {
        coin: 5e6
      }, {
        ticket: 5
      }, {
        coin: 1e7
      } ];
      return Config;
    }();
    exports.default = Config;
    cc._RF.pop();
  }, {} ],
  DailyBonusItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cd2a0cL565MMb3PI2MSL01H", "DailyBonusItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DailyBonusItem = function(_super) {
      __extends(DailyBonusItem, _super);
      function DailyBonusItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.value = null;
        _this.claimed = null;
        _this.extra = null;
        return _this;
      }
      DailyBonusItem.prototype.start = function() {};
      DailyBonusItem.prototype.setClaimable = function(claimable, isClaimed) {
        this.claimed.active = isClaimed;
        this.getComponent(cc.Button).interactable = claimable;
        this.extra.active = claimable;
      };
      __decorate([ property(cc.Label) ], DailyBonusItem.prototype, "value", void 0);
      __decorate([ property(cc.Node) ], DailyBonusItem.prototype, "claimed", void 0);
      __decorate([ property(cc.Node) ], DailyBonusItem.prototype, "extra", void 0);
      DailyBonusItem = __decorate([ ccclass ], DailyBonusItem);
      return DailyBonusItem;
    }(cc.Component);
    exports.default = DailyBonusItem;
    cc._RF.pop();
  }, {} ],
  DailyBonus: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2f9e8XOO6BHzap8qYPJCMDb", "DailyBonus");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Popup_1 = require("../../Popup");
    var Config_1 = require("../../Config");
    var DailyBonusItem_1 = require("./DailyBonusItem");
    var Api_1 = require("../../Api");
    var AutoHide_1 = require("../../AutoHide");
    var util_1 = require("../../util");
    var EventKeys_1 = require("../../EventKeys");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DailyBonus = function(_super) {
      __extends(DailyBonus, _super);
      function DailyBonus() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.toast = null;
        _this.days = [];
        _this.effect = null;
        _this.current = 0;
        return _this;
      }
      DailyBonus.prototype.start = function() {};
      DailyBonus.prototype.show = function(day, claimed) {
        void 0 === claimed && (claimed = false);
        claimed || day++;
        day > Config_1.default.dailyBonus.length && (day = 1);
        this.current = day;
        this.popup.open(this.node, 2);
        for (var i = 0; i < this.days.length; i++) {
          var d = i + 1;
          var claimable = d == day && !claimed;
          d < day ? this.days[i].setClaimable(claimable, true) : d == day ? this.days[i].setClaimable(claimable, claimed) : this.days[i].setClaimable(claimable, false);
        }
      };
      DailyBonus.prototype.onClaim = function(sender, day) {
        if (Api_1.default.dailyBonusClaimed) return;
        var date = day ? parseInt(day) : this.current;
        this.onClaimCompl(sender, date, false);
        cc.systemEvent.emit("claim_daily_bonus", date, Config_1.default.dailyBonus[date - 1], false);
        this.popup.close(this.node, 2);
      };
      DailyBonus.prototype.onClaimExtra = function(sender, day) {
        var _this = this;
        if (Api_1.default.dailyBonusClaimed) return;
        var date = day ? parseInt(day) : this.current;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD);
        Api_1.default.showRewardedVideo(function() {
          _this.onClaimCompl(sender, date, false);
          cc.systemEvent.emit("claim_daily_bonus", date, Config_1.default.dailyBonus[date - 1], true);
          _this.popup.close(_this.node, 2);
        }, function() {
          cc.systemEvent.emit("claim_daily_bonus_fail");
          Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_AD_ERROR);
          _this.popup.close(_this.node, 2);
        });
      };
      DailyBonus.prototype.onClaimCompl = function(sender, day, double) {
        var gift = Config_1.default.dailyBonus[day - 1];
        if (!gift.coin) return;
        var e = cc.instantiate(this.effect);
        e.active = true;
        e.position = sender.target.position;
        e.scale = 1;
        e.parent = cc.director.getScene().getChildByName("Canvas");
        var coinBonus = double ? 2 * gift.coin : gift.coin;
        e.getComponent(cc.Label).string = util_1.default.numberFormat(coinBonus);
        e.runAction(cc.sequence(cc.moveBy(1.2, 0, 250), cc.removeSelf()));
      };
      __decorate([ property(Popup_1.default) ], DailyBonus.prototype, "popup", void 0);
      __decorate([ property(AutoHide_1.default) ], DailyBonus.prototype, "toast", void 0);
      __decorate([ property(DailyBonusItem_1.default) ], DailyBonus.prototype, "days", void 0);
      __decorate([ property(cc.Node) ], DailyBonus.prototype, "effect", void 0);
      DailyBonus = __decorate([ ccclass ], DailyBonus);
      return DailyBonus;
    }(cc.Component);
    exports.default = DailyBonus;
    cc._RF.pop();
  }, {
    "../../Api": "Api",
    "../../AutoHide": "AutoHide",
    "../../Config": "Config",
    "../../EventKeys": "EventKeys",
    "../../Popup": "Popup",
    "../../util": "util",
    "./DailyBonusItem": "DailyBonusItem"
  } ],
  Deck: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cce96i6QBJMl7PTDBXefIeo", "Deck");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Card_1 = require("./Card");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Deck = function(_super) {
      __extends(Deck, _super);
      function Deck() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.cards = Array();
        _this.offset = 0;
        return _this;
      }
      Deck.prototype.onLoad = function() {
        for (var i = 0; i < 52; i++) {
          var card = this.node.children[i].getComponent(Card_1.default);
          var cardName = card.node.name;
          card.rank = this.getRankFromName(cardName);
          card.suit = this.getSuitFromName(cardName);
          this.cards.push(card);
        }
      };
      Deck.prototype.shuffle = function() {
        var length = this.cards.length;
        var lastIndex = length - 1;
        var index = -1;
        while (++index < length) {
          var rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
          var value = this.cards[rand];
          this.cards[rand] = this.cards[index];
          this.cards[index] = value;
        }
        for (var i = this.cards.length - 1; i >= 0; --i) {
          var card = this.cards[i];
          card.reset();
        }
        this.offset = 0;
      };
      Deck.prototype.pick = function() {
        var card = this.cards[this.offset];
        ++this.offset;
        return card;
      };
      Deck.prototype.hide = function() {
        for (var i = this.offset; i < this.cards.length; i++) this.cards[i].node.active = false;
      };
      Deck.prototype.getRankFromName = function(cardName) {
        if (3 == cardName.length) return 10;
        switch (cardName[0]) {
         case "J":
          return 11;

         case "Q":
          return 12;

         case "K":
          return 13;

         case "A":
          return 1;

         default:
          return parseInt(cardName[0]);
        }
      };
      Deck.prototype.getSuitFromName = function(cardName) {
        switch (cardName[cardName.length - 1]) {
         case "S":
          return 1;

         case "C":
          return 2;

         case "D":
          return 3;

         case "H":
          return 4;

         default:
          throw new Error(cardName);
        }
      };
      Deck = __decorate([ ccclass ], Deck);
      return Deck;
    }(cc.Component);
    exports.default = Deck;
    cc._RF.pop();
  }, {
    "./Card": "Card"
  } ],
  EventKeys: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fef6cR3M0xAfLpe1NdULNzR", "EventKeys");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var EventKeys = {
      PLAY_NOW: "play_now",
      SHOP_GO: "shop_go",
      VIP_GO: "vip_go",
      DAILY_GIFT_CLICK: "daily_gift_click",
      PLAY_MODE: "play_mode",
      PLAY_ACCEPT: "play_accept",
      QUIT_GAME: "quit_game",
      WIN: "win",
      LOSE: "lose",
      PLAY_DURATION: "play_duration",
      PLAY_TIME: "play_time",
      INVITE_FRIEND: "invite_friend",
      SPIN: "spin",
      PLAY_WITH_FRIEND: "play_with_friend",
      POPUP_ADREWARD_COIN_OPEN: "100k_popup",
      POPUP_ADREWARD_COIN_CLICK: "100k_clickpopup",
      POPUP_ADREWARD_COIN_ERROR: "100k_novideopopup",
      POPUP_ADREWARD_SPIN_OPEN: "spin_popup",
      POPUP_ADREWARD_SPIN_CLICK: "spin_click_popup",
      POPUP_ADREWARD_SPIN_ERROR: "spin_novideo_popup",
      POPUP_DAILY_REWARD_SHOW: "daily_reward_popup",
      POPUP_DAILY_REWARD_AD: "daily_reward_click_video",
      POPUP_DAILY_REWARD_AD_ERROR: "daily_reward_no_video"
    };
    exports.default = EventKeys;
    cc._RF.pop();
  }, {} ],
  Game: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c8251pV8NZGpa2LG0Z5yRed", "Game");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Player_1 = require("./Player");
    var Deck_1 = require("./Deck");
    var Helper_1 = require("./Helper");
    var CardGroup_1 = require("./CardGroup");
    var CommonCard_1 = require("./CommonCard");
    var Toast_1 = require("./Toast");
    var Api_1 = require("./Api");
    var Notice_1 = require("./Notice");
    var Config_1 = require("./Config");
    var util_1 = require("./util");
    var SpinWheel_1 = require("./SpinWheel");
    var Popup_1 = require("./Popup");
    var Bot_1 = require("./Bot");
    var Modal_1 = require("./popop/Modal");
    var EventKeys_1 = require("./EventKeys");
    var Language_1 = require("./Language");
    var suiteAsset = {
      H: "co",
      D: "ro",
      C: "chuon",
      S: "b\xedch"
    };
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Profile = function() {
      function Profile() {
        this.avatar = null;
        this.username = "";
      }
      __decorate([ property(cc.SpriteFrame) ], Profile.prototype, "avatar", void 0);
      __decorate([ property(cc.String) ], Profile.prototype, "username", void 0);
      Profile = __decorate([ ccclass("Profile") ], Profile);
      return Profile;
    }();
    var Game = function(_super) {
      __extends(Game, _super);
      function Game() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.deck = null;
        _this.play = null;
        _this.seats = [];
        _this.fire = null;
        _this.fold = null;
        _this.sort = null;
        _this.resultEffects = [];
        _this.bigWin = null;
        _this.toast = null;
        _this.notice = null;
        _this.betText = null;
        _this.spin = null;
        _this.popup = null;
        _this.profileBots = [];
        _this.soundToggle = null;
        _this.audioFold = null;
        _this.audioShowCard = null;
        _this.audioGarbageCard = null;
        _this.audioFire = null;
        _this.audioLose = null;
        _this.audioWin = null;
        _this.audioSortCard = null;
        _this.audioQuitGame = null;
        _this.audioDealCard = null;
        _this.audioFireSingle = null;
        _this.modal = null;
        _this.players = [];
        _this.commonCards = new CommonCard_1.default();
        _this.state = 0;
        _this.turn = 0;
        _this.leaveGame = false;
        _this.owner = null;
        _this.myself = null;
        _this.betValue = 1e3;
        _this.totalPlayer = 6;
        _this.startTime = 0;
        _this.playTime = 0;
        _this.addSeat = function(seat) {
          var profile = _this.getProfileBot();
          seat.show();
          seat.setAvatar(profile.avatar);
          seat.setUsername(profile.username);
          _this.players.push(seat);
        };
        _this.showAllPlayerCard = function() {
          _this.players.map(function(player) {
            player.showAllCard();
          });
        };
        _this.showResult = function() {
          var dealer = _this.players[0];
          var dealerAddCoin = 0;
          _this.players.map(function(player, index) {
            if (!index) return;
            var playerCoin = _this.calCointEndGame(player);
            player.subUser && (playerCoin += _this.calCointEndGame(player.subUser));
            player.changeCoin(playerCoin);
            console.log("====================================");
            console.log("user " + index + " " + playerCoin);
            console.log("====================================");
            dealerAddCoin -= playerCoin;
          });
          dealer.changeCoin(dealerAddCoin);
          console.log("====================================");
          console.log("nha cai " + dealerAddCoin);
          console.log("====================================");
          _this.showAllPlayerCard();
          _this.delayReset(3);
          _this.state = Game_1.LATE;
          Api_1.default.updateCoin(_this.myself.coinVal);
        };
        _this.calCointEndGame = function(player) {
          var dealer = _this.players[0];
          var playerCoin = 0;
          if (2 == player.bonusType) playerCoin -= _this.betValue / 2; else if (player.point > 21) playerCoin -= _this.betValue; else if (player.checkBlackJack()) playerCoin += 1.5 * _this.betValue; else if (player.point > dealer.point) playerCoin += _this.betValue; else {
            if (player.point === dealer.point) return 0;
            dealer.point > 21 ? playerCoin += _this.betValue : playerCoin -= _this.betValue;
          }
          /1/.test(player.bonusType.toString()) && (playerCoin *= 2);
          return playerCoin;
        };
        _this.checkWin = function() {
          return new Promise(function(resolve) {});
        };
        return _this;
      }
      Game_1 = Game;
      Game.prototype.onLoad = function() {
        var _this = this;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        var As = cc.find("Canvas/cards/AS");
        cc.resources.load("cards", cc.SpriteAtlas, function(err, atlas) {
          var initCardsResouces = new Promise(function(resolve) {
            new Array(13).fill("").map(function(el, rank) {
              [ "S", "C", "D", "H" ].map(function(suite, idSuite) {
                if (!rank && !suite) return;
                var r = rank + 1;
                var newNode = cc.instantiate(As);
                switch (r) {
                 case 1:
                  r = "A";
                  break;

                 case 11:
                  r = "J";
                  break;

                 case 12:
                  r = "Q";
                  break;

                 case 13:
                  r = "K";
                }
                var assetSuiteName = suiteAsset[suite];
                var rankAssetname = r;
                idSuite < 2 && (rankAssetname += "-1");
                newNode.name = r + suite;
                var suiteSprite = atlas.getSpriteFrame(assetSuiteName);
                newNode.children[0].getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(rankAssetname);
                newNode.children[1].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                newNode.children[2].getComponent(cc.Sprite).spriteFrame = suiteSprite;
                newNode.children[3].getComponent(cc.Widget).target = newNode;
                newNode.active = false;
                As.parent.addChild(newNode);
                12 === rank && "H" === suite && setTimeout(function() {
                  resolve(1);
                }, 500);
              });
            });
          });
          Promise.all([ initCardsResouces ]).then(function(rs) {
            _this.initGame();
          }).catch(function(err) {
            console.log("====================================");
            console.log(err);
            console.log("====================================");
          });
        });
      };
      Game.prototype.initGame = function() {
        var _this = this;
        this.startTime = Date.now();
        this.state = Game_1.WAIT;
        this.betValue = Config_1.default.betValue;
        this.totalPlayer = Config_1.default.totalPlayer;
        this.betText.string = Language_1.default.getInstance().get("NO") + ": " + util_1.default.numberFormat(this.betValue);
        this.deck.node.active = false;
        this.sort.node.active = false;
        this.play.node.active = false;
        this.hideDashboard();
        this.notice.hide();
        this.spin.onSpinHide = this.onSpinHide.bind(this);
        this.spin.onSpinCompleted = this.onSpinCompleted.bind(this);
        Config_1.default.battle && (this.totalPlayer = 2);
        var arr = /0|3/;
        4 === this.totalPlayer ? arr = /0|1|3|5/ : 6 === this.totalPlayer && (arr = /0|1|2|3|4|5/);
        this.seats.map(function(seat, i) {
          seat.seat = _this.players.length;
          arr.test(i.toString()) ? _this.addSeat(seat) : seat.hide();
        });
        this.myself = this.players[this.totalPlayer / 2];
        this.myself.setUsername(Api_1.default.username);
        this.myself.setCoin(Api_1.default.coin);
        this.myself.setTimeCallback(this.onPlayerTimeout, this, this.node);
        Config_1.default.userphoto && this.myself.setAvatar(new cc.SpriteFrame(Config_1.default.userphoto));
        if (Config_1.default.battle) {
          var bot = this.players[1];
          bot.setCoin(Config_1.default.battle.coin);
          bot.setAvatar(new cc.SpriteFrame(Config_1.default.battle.photo));
          bot.setUsername(Config_1.default.battle.username);
        } else this.players.map(function(player) {
          player.isBot() && player.setCoin(Config_1.default.botCoin);
        });
        this.node.runAction(cc.sequence(cc.delayTime(.7), cc.callFunc(this.deal, this)));
        Config_1.default.soundEnable ? this.soundToggle.uncheck() : this.soundToggle.check();
        Api_1.default.preloadInterstitialAd();
      };
      Game.prototype.onDestroy = function() {
        var duration = Date.now() - this.startTime;
        Api_1.default.logEvent(EventKeys_1.default.PLAY_DURATION, Math.floor(duration / 1e3));
      };
      Game.prototype.update = function(dt) {
        this.playTime < 30 && this.playTime + dt >= 30 ? Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-30") : this.playTime < 60 && this.playTime + dt >= 60 ? Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-60") : this.playTime < 90 && this.playTime + dt >= 90 ? Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-90") : this.playTime < 180 && this.playTime + dt >= 180 ? Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-180") : this.playTime < 360 && this.playTime + dt >= 360 ? Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-360") : this.playTime < 500 && this.playTime + dt >= 500 && Api_1.default.logEvent(EventKeys_1.default.PLAY_TIME + "-500");
        this.playTime += dt;
      };
      Game.prototype.onTouchStart = function(evt) {
        if (this.state != Game_1.PLAY) return;
        var selected = this.myself.touch(evt.getLocation());
        if (!selected) return;
        var prepareCards = this.myself.prepareCards;
        if (prepareCards.contains(selected)) {
          this.myself.unselectCard(selected);
          this.onCardSelected(selected, false);
        } else {
          this.myself.selectCard(selected);
          this.onCardSelected(selected, true);
        }
        if (this.turn != this.myself.seat) return;
        if (prepareCards.kind == CardGroup_1.default.Ungrouped) {
          this.allowFire(false);
          return;
        }
        if (0 == this.commonCards.length()) {
          this.allowFire(true);
          return;
        }
        if (prepareCards.gt(this.commonCards.peek())) {
          this.allowFire(true);
          return;
        }
        this.allowFire(false);
      };
      Game.prototype.onPlayerTimeout = function() {
        this.onStandClicked();
      };
      Game.prototype.onQuitClicked = function() {
        Api_1.default.logEvent(EventKeys_1.default.QUIT_GAME);
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) this.popup.open(this.node, 1); else {
          util_1.default.playAudio(this.audioQuitGame);
          cc.director.loadScene("home");
          Api_1.default.showInterstitialAd();
        }
      };
      Game.prototype.onBackClicked = function() {
        if (this.state == Game_1.PLAY || this.state == Game_1.DEAL) {
          var coin = Math.max(0, Api_1.default.coin - 10 * this.betValue);
          Api_1.default.updateCoin(coin);
        }
        util_1.default.playAudio(this.audioQuitGame);
        cc.director.loadScene("home");
        Api_1.default.showInterstitialAd();
      };
      Game.prototype.showHandCards = function() {
        util_1.default.playAudio(this.audioShowCard);
        this.state = Game_1.PLAY;
        this.players.map(function(player, index) {
          player.isBot() ? player.showCardCounter() : player.showAllCard();
        });
        var checkBJ = this.players.filter(function(player) {
          player.checkBlackJack();
        });
        this.owner || (this.owner = this.findPlayerHasFirstCard());
        this.setCurrentTurn(this.owner, true);
        this.sort.node.active = true;
      };
      Game.prototype.findPlayerHasFirstCard = function() {
        var userInRound = this.players.filter(function(player) {
          return player.isInRound() && !player.isCai() && player;
        });
        return userInRound[0];
      };
      Game.prototype.showDashboard = function(lead) {
        this.allowFold(!lead);
        if (this.commonCards.length() > 0) {
          var cards = Bot_1.default.suggest(this.commonCards, this.myself.cards);
          this.allowFire(!!cards);
        } else this.allowFire(true);
        this.fold.node.active = true;
        this.fire.node.active = true;
      };
      Game.prototype.setColor = function(btn, color) {
        btn.target.color = color;
        btn.target.children.forEach(function(child) {
          child.color = color;
        });
      };
      Game.prototype.hideDashboard = function(needShow) {
        void 0 === needShow && (needShow = false);
        var actionUser = cc.find("Canvas/actionUser");
        actionUser.active = needShow;
        if (needShow) {
          var showInsurr = 1 === this.players[0].cards[0].rank;
          actionUser.children[4].active = showInsurr;
          var showSplit = !this.myself.isSplit && this.myself.cards[0].rank === this.myself.cards[1].rank;
          showSplit = true;
          !showSplit && showInsurr && (actionUser.children[4].x = 190.44);
          actionUser.children[3].active = showSplit;
        }
      };
      Game.prototype.allowFold = function(allow) {
        this.fold.interactable = allow;
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fold, color);
      };
      Game.prototype.allowFire = function(allow) {
        var color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fire, color);
      };
      Game.prototype.onSortCardClicked = function() {
        util_1.default.playAudio(this.audioSortCard);
        this.myself.sortHandCards();
      };
      Game.prototype.onFireClicked = function() {
        this.onHit();
      };
      Game.prototype.onFoldClicked = function() {};
      Game.prototype.deal = function() {
        var _this = this;
        Api_1.default.preloadRewardedVideo();
        if (Api_1.default.coin <= this.betValue) {
          this.popup.open(null, 3);
          Api_1.default.showInterstitialAd();
          Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
          return;
        }
        this.play.node.active = false;
        this.deck.node.active = true;
        this.deck.shuffle();
        this.commonCards.reset();
        for (var i = 1; i < this.players.length; i++) {
          var bot = this.players[i];
          if (bot.getCoin() <= 3 * this.betValue) {
            var profile = this.getProfileBot();
            var rate = .5 + .5 * Math.random();
            bot.setCoin(Math.floor(this.myself.getCoin() * rate));
            bot.setAvatar(profile.avatar);
            bot.setUsername(profile.username);
          }
        }
        cc.find("Canvas/actionUser/insurr").x = 402.038;
        this.seats[6].hide();
        this.players.map(function(p) {
          p.reset();
          p.updatePoint("0");
          p.setBonusType(0);
        });
        for (var i = 0; i < 2; i++) for (var j = 0; j < this.getTotalPlayer(); j++) {
          var player = this.players[j];
          var card = this.deck.pick();
          player.push(card, .3 + (i * this.getTotalPlayer() + j) * Game_1.DEAL_SPEED);
        }
        this.node.runAction(cc.sequence(cc.delayTime(Game_1.DEAL_SPEED * this.totalPlayer * 2), cc.callFunc(this.showHandCards, this)));
        this.node.runAction(cc.sequence(cc.delayTime(.4), cc.callFunc(function() {
          return util_1.default.playAudio(_this.audioDealCard);
        })));
        Api_1.default.preloadInterstitialAd();
      };
      Game.prototype.showEffect = function(player, effect) {
        effect.active = true;
        effect.setPosition(player.node.getPosition());
      };
      Game.prototype.onFold = function(player) {
        util_1.default.playAudio(this.audioFold);
        if (this.commonCards.hasCombat()) {
          var count = this.commonCards.getCombatLength();
          var victim = this.commonCards.getCombatVictim();
          var winner = this.commonCards.getCombatWinner();
          var bigPig = this.commonCards.getCombat();
          var spotWin = Math.pow(2, count - 1) * Helper_1.default.calculateSpotWin(bigPig.cards) * this.betValue;
          var coin = Math.min(spotWin, victim.coinVal);
          winner.addCoin(coin);
          victim.subCoin(coin);
        }
        this.commonCards.isCombatOpen() && this.commonCards.resetCombat();
        player.setInRound(false);
        player.setActive(false);
        var players = this.getInRoundPlayers();
        if (players.length >= 2) return this.nextTurn();
        this.nextRound(players[0]);
      };
      Game.prototype.onFire = function(player, cards, isDown) {
        void 0 === isDown && (isDown = false);
        var audioClip = 15 == cards.highest.rank || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        this.node.runAction(cc.sequence(cc.delayTime(.17), cc.callFunc(function() {
          return util_1.default.playAudio(audioClip);
        })));
        player.setActive(false);
        var cardCount = cards.count();
        var pos = this.commonCards.getPosition();
        cards.sort();
        for (var i = cards.count() - 1; i >= 0; --i) {
          isDown && cards.at(i).show();
          var card = cards.at(i);
          card.node.zIndex = this.commonCards.totalCards + i;
          var move = cc.moveTo(.3, cc.v2(pos.x + (i - cardCount / 2) * Game_1.CARD_SPACE, pos.y));
          card.node.runAction(move);
          var scale = cc.scaleTo(.3, .6);
          card.node.runAction(scale);
        }
        this.commonCards.push(cards);
        15 == cards.highest.rank ? this.commonCards.pushCombat(player, cards) : this.commonCards.isCombatOpen() && this.commonCards.pushCombat(player, cards);
        player.removeCards(cards);
        player.isBot() && player.updateCardCounter();
        15 == cards.highest.rank ? this.notice.showBigPig(cards.count(), 3) : cards.kind == CardGroup_1.default.MultiPair && 6 == cards.count() ? this.notice.show(Notice_1.default.THREE_PAIR, 3) : cards.kind == CardGroup_1.default.MultiPair && 8 == cards.count() ? this.notice.show(Notice_1.default.FOUR_PAIR, 3) : cards.kind == CardGroup_1.default.House && 4 == cards.count() && this.notice.show(Notice_1.default.FOUR_CARD, 3);
        if (player.cards.length > 0) return this.nextTurn();
        this.state = Game_1.LATE;
        this.sort.node.active = false;
        this.node.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(this.showResult, this, player)));
      };
      Game.prototype.onBigWin = function(sender, winner) {
        this.state = Game_1.LATE;
        this.owner = winner;
        this.bigWin.active = true;
        var total = 0;
        for (var i = 0; i < this.players.length; i++) {
          var player = this.players[i];
          if (player != winner) {
            if (player.isBot()) {
              player.hideCardCounter();
              player.showHandCards();
              player.reorder();
            }
            var lost = 13 * this.betValue;
            lost = Math.min(player.coinVal, lost);
            player.subCoin(lost);
            total += lost;
          }
        }
        winner.addCoin(total);
        Api_1.default.updateCoin(this.myself.coinVal);
        this.onWin(total);
      };
      Game.prototype.onLose = function(ranking) {
        Api_1.default.logEvent(EventKeys_1.default.LOSE + "_" + ranking);
        util_1.default.playAudio(this.audioLose);
        var rnd = Math.random();
        2 == ranking && rnd <= .3 ? Api_1.default.showInterstitialAd() : 3 == ranking && rnd <= .2 ? Api_1.default.showInterstitialAd() : 4 == ranking && rnd <= .1 && Api_1.default.showInterstitialAd();
        this.delayReset(3);
      };
      Game.prototype.onWin = function(won) {
        Api_1.default.logEvent(EventKeys_1.default.WIN);
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_OPEN);
        util_1.default.playAudio(this.audioWin);
        var rnd = Math.random();
        if (rnd <= .3 && Api_1.default.isInterstitialAdLoaded()) {
          Api_1.default.showInterstitialAd();
          this.delayReset(3);
        } else this.spin.show(won);
      };
      Game.prototype.onSpinHide = function() {
        this.delayReset(3);
      };
      Game.prototype.onSpinCompleted = function(result, won) {
        var bonus = this.spin.compute(result.id, won);
        if (bonus > 0) {
          this.myself.addCoin(bonus);
          Api_1.default.updateCoin(this.myself.coinVal);
        }
      };
      Game.prototype.delayReset = function(dt) {
        this.node.runAction(cc.sequence(cc.delayTime(dt), cc.callFunc(this.reset, this)));
      };
      Game.prototype.reset = function() {
        for (var i = this.resultEffects.length - 1; i >= 0; --i) this.resultEffects[i].active = false;
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) this.players[i].reset();
        this.bigWin.active && (this.bigWin.active = false);
        this.play.node.active = true;
      };
      Game.prototype.onCardSelected = function(card, selected) {};
      Game.prototype.nextTurn = function() {
        var next = this.getNextInRoundPlayer();
        this.setCurrentTurn(next, false);
      };
      Game.prototype.nextRound = function(lead) {
        this.commonCards.nextRound();
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) this.players[i].setInRound(true);
        this.setCurrentTurn(lead, true);
      };
      Game.prototype.onHitClicked = function() {
        var player = this.myself;
        this.takeCard(player);
        player.setActive(true);
        this.hideDashboard();
      };
      Game.prototype.onDoubleClicked = function() {};
      Game.prototype.onSplitClicked = function() {};
      Game.prototype.onStandClicked = function() {
        var player = this.myself;
        player.setActive(false);
        !player.inRound && player.subUser && (player = player.subUser);
        player.setInRound(false);
        this.hideDashboard();
        this.nextTurn();
      };
      Game.prototype.setCurrentTurn = function(player, lead) {
        var _a;
        return __awaiter(this, void 0, void 0, function() {
          var canHit;
          var _this = this;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              this.players.map(function(p) {
                return p.setActive(false);
              });
              if (!player) return [ 2, this.showResult() ];
              this.turn = player.seat;
              player.setActive(true);
              if (player.isUser()) return [ 2, Game_1.sleep(Game_1.WAIT_RE_SHOW_USER_ACTION).then(function() {
                _this.hideDashboard(true);
              }) ];
              canHit = player.point > this.players[0].point;
              player.point < 18 && (canHit = true);
              canHit || (canHit = void 0 != this.players.find(function(p) {
                return player.point > p.point;
              }));
              player.point >= 18 && (canHit = false);
              5 === (null === (_a = player.cards) || void 0 === _a ? void 0 : _a.length) && (canHit = false);
              return [ 4, Game_1.sleep(Game_1.WAIT_BOT) ];

             case 1:
              _b.sent();
              player.setInRound(false);
              if (canHit) this.takeCard(player); else {
                this.toast.show(player.username.string + " stand" + player.point + " \u0111i\u1ec3m");
                this.nextTurn();
              }
              return [ 2 ];
            }
          });
        });
      };
      Game.prototype.getInRoundPlayers = function() {
        var players = [];
        for (var i = this.getTotalPlayer() - 1; i >= 0; --i) this.players[i].isInRound() && players.push(this.players[i]);
        return players;
      };
      Game.prototype.getNextInRoundPlayer = function() {
        var _this = this;
        var nextTurn = this.players.find(function(player) {
          var _a;
          return player.seat === _this.turn && (null === (_a = null === player || void 0 === player ? void 0 : player.subUser) || void 0 === _a ? void 0 : _a.inRound) || player.seat > _this.turn && player.isInRound();
        });
        if (!nextTurn && this.players[0].inRound) return this.players[0];
        return nextTurn;
      };
      Game.prototype.takeCard = function(player, needNext) {
        void 0 === needNext && (needNext = true);
        return __awaiter(this, void 0, void 0, function() {
          var card, _player;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              card = this.deck.pick();
              this.toast.show(player.username.string + ": " + player.point + "\u0111 b\u1ed1c th\xeam" + card.rank);
              if (!(null === card || void 0 === card ? void 0 : card.node)) {
                this.state = Game_1.LATE;
                this.sort.node.active = false;
                return [ 2 ];
              }
              player.push(card, 0);
              if (!needNext) return [ 2 ];
              return [ 4, Game_1.sleep(0) ];

             case 1:
              _a.sent();
              player.isBot() || player.cards.map(function(el) {
                return el.show();
              });
              player.setActive(false);
              _player = player;
              !player.inRound && player.subUser && (_player = player.subUser);
              if (_player.point >= 21 || 5 === _player.cards.length) {
                _player.setInRound(false);
                _player.point > 21 && this.toast.show(_player.username.string + " Thua: " + _player.point + " \u0111i\u1ec3m");
                _player.setActive(false);
                return [ 2, this.nextTurn() ];
              }
              if (player.bonusType) {
                player.setInRound(false);
                this.nextTurn();
              } else this.setCurrentTurn(player, false);
              return [ 2 ];
            }
          });
        });
      };
      Game.prototype.execBot = function(sender, bot) {
        var cards = this.commonCards.isEmpty() ? Bot_1.default.random(bot.cards) : Bot_1.default.suggest(this.commonCards, bot.cards);
        if (!cards) return this.onFold(bot);
        this.onFire(this.players[this.turn], cards, true);
      };
      Game.prototype.getTotalPlayer = function() {
        return this.players.length;
      };
      Game.prototype.getProfileBot = function() {
        for (var i = 0; i < 10; i++) {
          var rnd = Math.floor(Math.random() * this.profileBots.length);
          if (!this.isUsage(this.profileBots[rnd])) return this.profileBots[rnd];
        }
        return this.profileBots[0];
      };
      Game.prototype.isUsage = function(user) {
        for (var i = 0; i < this.players.length; i++) if (this.players[i].username.string == user.username) return true;
        return false;
      };
      Game.prototype.onSoundToggle = function(sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
      };
      Game.prototype.claimBankruptcyMoney = function(bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get("MONEY1"), util_1.default.numberFormat(bonus));
        this.toast.show(msg);
        Api_1.default.coinIncrement(bonus);
        this.myself.setCoin(Api_1.default.coin);
        this.popup.close(null, 3);
        this.betValue = Math.round(.3 * Api_1.default.coin);
        this.betText.string = util_1.default.numberFormat(this.betValue);
      };
      Game.prototype.inviteFirend = function() {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function() {
          _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function() {
          _this.popup.close(null, 3);
          _this.toast.show("M\u1eddi b\u1ea1n ch\u01a1i kh\xf4ng th\xe0nh c\xf4ng");
        });
      };
      Game.prototype.adReward = function() {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function() {
          _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function() {
          _this.popup.close(null, 3);
          _this.claimBankruptcyMoney(Api_1.default.randomBonus());
          Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
      };
      Game.prototype.onSetBonusClicked = function(e, bonus) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              console.log("====================================");
              console.log(bonus);
              console.log("====================================");
              if (!(3 == bonus)) return [ 3, 4 ];
              this.myself.split(this.seats[6]);
              this.seats[6].show();
              this.hideDashboard();
              return [ 4, Game_1.sleep(Game_1.DEAL_SPEED) ];

             case 1:
              _a.sent();
              this.takeCard(this.myself, false);
              return [ 4, Game_1.sleep(500) ];

             case 2:
              _a.sent();
              this.takeCard(this.myself.subUser, false);
              return [ 4, Game_1.sleep(1e3) ];

             case 3:
              _a.sent();
              this.nextTurn();
              return [ 3, 5 ];

             case 4:
              this.myself.setBonusType(bonus);
              if (1 == bonus) {
                this.onHitClicked();
                this.myself.inRound ? this.myself.inRound = false : this.myself.subUser.inRound = false;
              } else this.onStandClicked();
              _a.label = 5;

             case 5:
              return [ 2 ];
            }
          });
        });
      };
      Game.prototype.cheat_win = function() {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.myself);
      };
      Game.prototype.cheat_lose = function() {
        this.state = Game_1.LATE;
        this.hideDashboard();
        this.showResult(null, this.players[1]);
      };
      var Game_1;
      Game.sleep = function(wait) {
        return __awaiter(void 0, void 0, void 0, function() {
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve) {
              setTimeout(function() {
                resolve(1);
              }, wait);
            }) ];
          });
        });
      };
      Game.WAIT = 0;
      Game.DEAL = 1;
      Game.PLAY = 2;
      Game.LATE = 3;
      Game.DEAL_SPEED = 1.2;
      Game.CARD_SPACE = 45;
      Game.WAIT_BOT = 2e3;
      Game.WAIT_RE_SHOW_USER_ACTION = 1e3;
      __decorate([ property(Deck_1.default) ], Game.prototype, "deck", void 0);
      __decorate([ property(cc.Button) ], Game.prototype, "play", void 0);
      __decorate([ property(Player_1.default) ], Game.prototype, "seats", void 0);
      __decorate([ property(cc.Button) ], Game.prototype, "fire", void 0);
      __decorate([ property(cc.Button) ], Game.prototype, "fold", void 0);
      __decorate([ property(cc.Button) ], Game.prototype, "sort", void 0);
      __decorate([ property(cc.Node) ], Game.prototype, "resultEffects", void 0);
      __decorate([ property(cc.Node) ], Game.prototype, "bigWin", void 0);
      __decorate([ property(Toast_1.default) ], Game.prototype, "toast", void 0);
      __decorate([ property(Notice_1.default) ], Game.prototype, "notice", void 0);
      __decorate([ property(cc.Label) ], Game.prototype, "betText", void 0);
      __decorate([ property(SpinWheel_1.default) ], Game.prototype, "spin", void 0);
      __decorate([ property(Popup_1.default) ], Game.prototype, "popup", void 0);
      __decorate([ property(Profile) ], Game.prototype, "profileBots", void 0);
      __decorate([ property(cc.Toggle) ], Game.prototype, "soundToggle", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioFold", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioShowCard", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioGarbageCard", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioFire", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioLose", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioWin", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioSortCard", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioQuitGame", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioDealCard", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Game.prototype, "audioFireSingle", void 0);
      __decorate([ property(Modal_1.default) ], Game.prototype, "modal", void 0);
      Game = Game_1 = __decorate([ ccclass ], Game);
      return Game;
    }(cc.Component);
    exports.default = Game;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./Bot": "Bot",
    "./CardGroup": "CardGroup",
    "./CommonCard": "CommonCard",
    "./Config": "Config",
    "./Deck": "Deck",
    "./EventKeys": "EventKeys",
    "./Helper": "Helper",
    "./Language": "Language",
    "./Notice": "Notice",
    "./Player": "Player",
    "./Popup": "Popup",
    "./SpinWheel": "SpinWheel",
    "./Toast": "Toast",
    "./popop/Modal": "Modal",
    "./util": "util"
  } ],
  HandCard: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3f5f4vN8GVIHYlDzS8EcLN6", "HandCard");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Card_1 = require("./Card");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var HandCard = function(_super) {
      __extends(HandCard, _super);
      function HandCard() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.cards = null;
        return _this;
      }
      HandCard.prototype.start = function() {};
      __decorate([ property(Card_1.default) ], HandCard.prototype, "cards", void 0);
      HandCard = __decorate([ ccclass ], HandCard);
      return HandCard;
    }(cc.Component);
    exports.default = HandCard;
    cc._RF.pop();
  }, {
    "./Card": "Card"
  } ],
  Helper: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1879dJgJEVIpqZTqzYVoVpF", "Helper");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Helper = function() {
      function Helper() {}
      Helper.isHouse = function(cards, amount) {
        void 0 === amount && (amount = 0);
        if (0 != amount && cards.length != amount) return false;
        if (0 == amount && (cards.length < 2 || cards.length > 4)) return false;
        var cardMin = Helper.findMinCard(cards);
        var cardMax = Helper.findMaxCard(cards);
        return cardMin.rank == cardMax.rank;
      };
      Helper.isStraight = function(cards, amount) {
        void 0 === amount && (amount = 0);
        if (0 != amount && cards.length != amount) return false;
        if (0 == amount && cards.length < 3) return false;
        var cardMin = Helper.findMinCard(cards);
        for (var i = cards.length - 1; i > 0; --i) {
          if (cardMin.rank + i > 14) return false;
          var card = Helper.findCardByRank(cards, cardMin.rank + i);
          if (!card) return false;
        }
        return true;
      };
      Helper.isMultiPair = function(cards, amount) {
        void 0 === amount && (amount = 0);
        if (cards.length < 6 || cards.length % 2 == 1) return false;
        if (0 != amount && cards.length != 2 * amount) return false;
        for (var i = cards.length - 1; i >= 0; --i) {
          if (15 == cards[i].rank) return false;
          var matched = Helper.findCardByRank(cards, cards[i].rank, cards[i]);
          if (!matched) return false;
        }
        return true;
      };
      Helper.isBigWin = function(cards) {
        var reduce = cards.reduce(function(accumulator, currentValue) {
          accumulator[currentValue.rank] || (accumulator[currentValue.rank] = []);
          accumulator[currentValue.rank].push(currentValue);
          return accumulator;
        }, {});
        if (!!reduce[15] && 4 == reduce[15].length) return 1;
        if (Helper.isDragon(reduce)) return 2;
        if (6 == Helper.pairCount(reduce)) return 3;
        return 0;
      };
      Helper.isDragon = function(reduce) {
        for (var i = 3; i <= 14; i++) if (!reduce[i]) return false;
        return true;
      };
      Helper.pairCount = function(reduce) {
        var count = 0;
        for (var v in reduce) reduce[v].length >= 2 && count++;
        return count;
      };
      Helper.sort = function(cards) {
        cards.sort(function(a, b) {
          return a.gt(b) ? 1 : -1;
        });
      };
      Helper.findCard = function(cards, rank, suit) {
        for (var i = cards.length - 1; i >= 0; --i) {
          var card = cards[i];
          if (card.rank == rank && card.suit == suit) return card;
        }
        return null;
      };
      Helper.findMaxCard = function(cards, filter) {
        void 0 === filter && (filter = null);
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
          var c = cards[i];
          (null == filter || c.lt(filter)) && (null == card ? card = c : c.gt(card) && (card = c));
        }
        return card;
      };
      Helper.findMinCard = function(cards, filter) {
        void 0 === filter && (filter = null);
        var card = null;
        for (var i = cards.length - 1; i >= 0; --i) {
          var c = cards[i];
          (null == filter || c.gt(filter)) && (null == card ? card = c : c.lt(card) && (card = c));
        }
        return card;
      };
      Helper.findAllCardByRange = function(cards, max, min) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) cards[i].rank > min && cards[i].rank <= max && (Helper.findCardByRank(setCards, cards[i].rank) || setCards.push(cards[i]));
        return setCards;
      };
      Helper.findAllCardByRank = function(cards, rank, limit) {
        var setCards = [];
        for (var i = cards.length - 1; i >= 0; --i) if (cards[i].rank == rank) {
          setCards.push(cards[i]);
          if (setCards.length == limit) return setCards;
        }
        return setCards;
      };
      Helper.findCardByRank = function(cards, rank, filter) {
        void 0 === filter && (filter = null);
        for (var i = cards.length - 1; i >= 0; --i) if (cards[i].rank == rank && cards[i] != filter) return cards[i];
        return null;
      };
      Helper.findIndex = function(arr, target) {
        for (var i = arr.length - 1; i >= 0; --i) if (target == arr[i]) return i;
        return -1;
      };
      Helper.removeBy = function(arr, target) {
        var idx = Helper.findIndex(arr, target);
        Helper.removeAt(arr, idx);
      };
      Helper.removeAt = function(arr, idx) {
        arr.splice(idx, 1);
      };
      Helper.highlight = function(cards, handCards) {
        return [];
      };
      Helper.findStraightCard = function(cards, filter) {
        for (var i = cards.length - 1; i >= 0; --i) {
          var cardStarted = cards[i];
          if (cardStarted.rank < 15 && cardStarted.gt(filter.highest)) {
            var straight = Helper.findAllCardByRange(cards, cardStarted.rank, cardStarted.rank - filter.count());
            if (straight.length == filter.count()) return straight;
          }
        }
        return null;
      };
      Helper.findHouseCard = function(cards, filter, limit) {
        for (var i = cards.length - 1; i >= 0; --i) {
          var cardStarted = cards[i];
          if (cardStarted.gt(filter)) {
            var straight = Helper.findAllCardByRank(cards, cardStarted.rank, limit);
            if (straight.length == limit) return straight;
          }
        }
        return null;
      };
      Helper.findMultiPairCard = function(cards, filter, limit) {
        return null;
      };
      Helper.calculate = function(cards) {
        var pigs = Helper.findAllCardByRank(cards, 15, 4);
        var spotWin = Helper.calculateSpotWin(pigs);
        return cards.length + spotWin;
      };
      Helper.calculateSpotWin = function(pigs) {
        var total = 0;
        for (var i = pigs.length - 1; i >= 0; --i) total += pigs[i].suit + 2;
        return total;
      };
      return Helper;
    }();
    exports.default = Helper;
    cc._RF.pop();
  }, {} ],
  Home: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2c5ff9R/FJPip0TZkafs6WJ", "Home");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Api_1 = require("./Api");
    var Slider2_1 = require("./Slider2");
    var Config_1 = require("./Config");
    var Leaderboard_1 = require("./Leaderboard");
    var util_1 = require("./util");
    var Popup_1 = require("./Popup");
    var DailyBonus_1 = require("./popop/bonus/DailyBonus");
    var AutoHide_1 = require("./AutoHide");
    var Modal_1 = require("./popop/Modal");
    var EventKeys_1 = require("./EventKeys");
    var Language_1 = require("./Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Home = function(_super) {
      __extends(Home, _super);
      function Home() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.avatar = null;
        _this.playerName = null;
        _this.coin = null;
        _this.betSetting = null;
        _this.leaderboard = null;
        _this.music = null;
        _this.dailyBonus = null;
        _this.popup = null;
        _this.toast = null;
        _this.modal = null;
        _this.preloadSceneGame = function() {
          cc.director.preloadScene("game", function(completedCount, totalCount, item) {}, function(error) {
            cc.log("onLoaded");
          });
        };
        _this.onChallenge = function(playerId, photo, username, coin) {
          return __awaiter(_this, void 0, void 0, function() {
            var error_1, minCoin;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                if (Api_1.default.coin <= Config_1.default.bankrupt) {
                  Api_1.default.preloadRewardedVideo();
                  this.popup.open(this.node, 4);
                  Api_1.default.showInterstitialAd();
                  return [ 2 ];
                }
                _a.label = 1;

               case 1:
                _a.trys.push([ 1, 3, , 4 ]);
                return [ 4, Api_1.default.challenge(playerId) ];

               case 2:
                _a.sent();
                return [ 3, 4 ];

               case 3:
                error_1 = _a.sent();
                console.log(error_1);
                return [ 3, 4 ];

               case 4:
                Math.random() < .7 && Api_1.default.showInterstitialAd();
                cc.audioEngine.stopMusic();
                Config_1.default.battle = {
                  photo: photo,
                  username: username,
                  coin: coin
                };
                minCoin = Math.min(Math.max(coin, 1e5), Api_1.default.coin);
                Config_1.default.betValue = Math.floor(.3 * minCoin);
                cc.director.loadScene("game");
                Api_1.default.logEvent(EventKeys_1.default.PLAY_WITH_FRIEND);
                return [ 2 ];
              }
            });
          });
        };
        return _this;
      }
      Home.prototype.start = function() {
        var _this = this;
        this.betSetting.onValueChange = this.onBetChange;
        Api_1.default.initAsync(function(bonus, day) {
          Language_1.default.getInstance().load(Api_1.default.locale);
          cc.systemEvent.emit("LANG_CHAN");
          _this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
          _this.playerName.string = Api_1.default.username;
          cc.assetManager.loadRemote(Api_1.default.photo, function(err, tex) {
            _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
            Config_1.default.userphoto = tex;
          });
          if (bonus) {
            Api_1.default.preloadRewardedVideo(function() {
              Api_1.default.logEvent(EventKeys_1.default.POPUP_DAILY_REWARD_SHOW);
              _this.dailyBonus.show(day, Api_1.default.dailyBonusClaimed);
            });
            cc.systemEvent.on("claim_daily_bonus", _this.onClaimDailyBonus, _this);
            cc.systemEvent.on("claim_daily_bonus_fail", _this.onClaimDailyBonusFail, _this);
          }
        }, this.leaderboard);
        cc.systemEvent.on("lb_battle", this.onChallenge, this);
        cc.systemEvent.on("lb_share", this.onShare, this);
        cc.audioEngine.setMusicVolume(.3);
        Config_1.default.soundEnable && cc.audioEngine.playMusic(this.music, true);
        this.scheduleOnce(this.preloadSceneGame, 1);
        Api_1.default.preloadInterstitialAd();
      };
      Home.prototype.onShare = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, Api_1.default.shareAsync() ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      Home.prototype.onClaimDailyBonus = function(day, bonus, extra) {
        if (bonus.coin) {
          Api_1.default.coinIncrement(extra ? 2 * bonus.coin : bonus.coin);
          this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        }
        bonus.ticket && (Api_1.default.ticket += extra ? 2 * bonus.ticket : bonus.ticket);
        Api_1.default.claimDailyBonus(day);
      };
      Home.prototype.onClaimDailyBonusFail = function() {
        this.modal.show(Language_1.default.getInstance().get("NOVIDEO"));
      };
      Home.prototype.openPlayPopupClick = function() {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_NOW);
        if (Api_1.default.coin > Config_1.default.bankrupt) this.popup.open(this.node, 1); else {
          Api_1.default.preloadRewardedVideo();
          Api_1.default.showInterstitialAd();
          this.popup.open(this.node, 4);
          Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_OPEN);
        }
      };
      Home.prototype.onPlayClick = function() {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_ACCEPT);
        Config_1.default.battle = null;
        cc.audioEngine.stopMusic();
        cc.director.loadScene("game");
      };
      Home.prototype.onShopClick = function() {
        Api_1.default.logEvent(EventKeys_1.default.SHOP_GO);
        cc.director.loadScene("shop");
      };
      Home.prototype.onDailyBonusClick = function() {
        Api_1.default.logEvent(EventKeys_1.default.DAILY_GIFT_CLICK);
        this.dailyBonus.show(Api_1.default.dailyBonusDay, Api_1.default.dailyBonusClaimed);
        Api_1.default.showInterstitialAd();
      };
      Home.prototype.onBetChange = function(value) {
        Config_1.default.betValue = value;
      };
      Home.prototype.onBotChange = function(sender, params) {
        Api_1.default.logEvent(EventKeys_1.default.PLAY_MODE + "-" + params);
        sender.isChecked && (Config_1.default.totalPlayer = parseInt(params));
      };
      Home.prototype.onSoundToggle = function(sender, isOn) {
        Config_1.default.soundEnable = !sender.isChecked;
        Config_1.default.soundEnable ? cc.audioEngine.playMusic(this.music, true) : cc.audioEngine.stopMusic();
      };
      Home.prototype.claimBankruptcyMoney = function(bonus) {
        var msg = cc.js.formatStr(Language_1.default.getInstance().get("MONEY1"), util_1.default.numberFormat(bonus));
        this.toast.openWithText(null, msg);
        Api_1.default.coinIncrement(bonus);
        this.coin.string = util_1.default.numberFormat(Api_1.default.coin);
        this.popup.close(null, 4);
      };
      Home.prototype.inviteFirend = function() {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.INVITE_FRIEND);
        Api_1.default.invite(function() {
          _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function() {
          _this.popup.close(null, 2);
          _this.toast.openWithText(null, "M\u1eddi b\u1ea1n ch\u01a1i kh\xf4ng th\xe0nh c\xf4ng");
        });
      };
      Home.prototype.adReward = function() {
        var _this = this;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_CLICK);
        Api_1.default.showRewardedVideo(function() {
          _this.claimBankruptcyMoney(Config_1.default.bankrupt_bonus);
        }, function() {
          _this.claimBankruptcyMoney(Api_1.default.randomBonus());
          Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_COIN_ERROR);
        });
      };
      __decorate([ property(cc.Sprite) ], Home.prototype, "avatar", void 0);
      __decorate([ property(cc.Label) ], Home.prototype, "playerName", void 0);
      __decorate([ property(cc.Label) ], Home.prototype, "coin", void 0);
      __decorate([ property(Slider2_1.default) ], Home.prototype, "betSetting", void 0);
      __decorate([ property(Leaderboard_1.default) ], Home.prototype, "leaderboard", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], Home.prototype, "music", void 0);
      __decorate([ property(DailyBonus_1.default) ], Home.prototype, "dailyBonus", void 0);
      __decorate([ property(Popup_1.default) ], Home.prototype, "popup", void 0);
      __decorate([ property(AutoHide_1.default) ], Home.prototype, "toast", void 0);
      __decorate([ property(Modal_1.default) ], Home.prototype, "modal", void 0);
      Home = __decorate([ ccclass ], Home);
      return Home;
    }(cc.Component);
    exports.default = Home;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./AutoHide": "AutoHide",
    "./Config": "Config",
    "./EventKeys": "EventKeys",
    "./Language": "Language",
    "./Leaderboard": "Leaderboard",
    "./Popup": "Popup",
    "./Slider2": "Slider2",
    "./popop/Modal": "Modal",
    "./popop/bonus/DailyBonus": "DailyBonus",
    "./util": "util"
  } ],
  LBEntry: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "48e8adoRHBEI57tbH867RGK", "LBEntry");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var util_1 = require("./util");
    var Api_1 = require("./Api");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var LBEntry = function(_super) {
      __extends(LBEntry, _super);
      function LBEntry() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.rank = null;
        _this.medal = null;
        _this.avatar = null;
        _this.playerName = null;
        _this.playerCoin = null;
        _this.spriteMedals = [];
        _this.playerId = null;
        _this.photo = null;
        _this.coin = 0;
        _this.click_key = "lb_battle";
        return _this;
      }
      LBEntry.prototype.start = function() {};
      LBEntry.prototype.render = function(rank, name, coin, avatar, playerId) {
        var _this = this;
        this.playerId = playerId;
        this.playerName.string = name;
        this.playerCoin.string = util_1.default.numberFormat(coin);
        this.coin = coin;
        cc.assetManager.loadRemote(avatar, function(err, tex) {
          _this.photo = tex;
          _this.avatar.spriteFrame = new cc.SpriteFrame(tex);
        });
        if (this.spriteMedals.length >= rank) {
          this.rank.node.active = false;
          this.medal.spriteFrame = this.spriteMedals[rank - 1];
        } else {
          this.rank.string = util_1.default.numberFormat(rank);
          this.medal.node.active = false;
        }
        Api_1.default.playerId == playerId && (this.click_key = "lb_share");
      };
      LBEntry.prototype.onClick = function() {
        cc.systemEvent.emit(this.click_key, this.playerId, this.photo, this.playerName.string, this.coin);
      };
      __decorate([ property(cc.Label) ], LBEntry.prototype, "rank", void 0);
      __decorate([ property(cc.Sprite) ], LBEntry.prototype, "medal", void 0);
      __decorate([ property(cc.Sprite) ], LBEntry.prototype, "avatar", void 0);
      __decorate([ property(cc.Label) ], LBEntry.prototype, "playerName", void 0);
      __decorate([ property(cc.Label) ], LBEntry.prototype, "playerCoin", void 0);
      __decorate([ property(cc.SpriteFrame) ], LBEntry.prototype, "spriteMedals", void 0);
      LBEntry = __decorate([ ccclass ], LBEntry);
      return LBEntry;
    }(cc.Component);
    exports.default = LBEntry;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./util": "util"
  } ],
  Language: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3d3c6mK5nNMjYk5tCjZK4el", "Language");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var vi_VN_1 = require("../lang/vi_VN");
    var th_TH_1 = require("../lang/th_TH");
    var en_US_1 = require("../lang/en_US");
    var tl_PH_1 = require("../lang/tl_PH");
    var Language = function() {
      function Language() {
        this.lang = en_US_1.default;
      }
      Language.getInstance = function() {
        Language._instance || (Language._instance = new Language());
        return Language._instance;
      };
      Language.prototype.load = function(lang) {
        this.lang = "vi_VN" == lang ? vi_VN_1.default : "th_TH" == lang ? th_TH_1.default : "tl_PH" == lang ? tl_PH_1.default : en_US_1.default;
      };
      Language.prototype.get = function(key) {
        return this.lang[key];
      };
      Language._instance = null;
      return Language;
    }();
    exports.default = Language;
    cc._RF.pop();
  }, {
    "../lang/en_US": "en_US",
    "../lang/th_TH": "th_TH",
    "../lang/tl_PH": "tl_PH",
    "../lang/vi_VN": "vi_VN"
  } ],
  Leaderboard: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "baa5a1+8bxE/rfm6Qsnw+yf", "Leaderboard");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LBEntry_1 = require("./LBEntry");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Leaderboard = function(_super) {
      __extends(Leaderboard, _super);
      function Leaderboard() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.template = null;
        _this.container = null;
        return _this;
      }
      Leaderboard.prototype.start = function() {};
      Leaderboard.prototype.render = function(rank, name, coin, avatar, playerId) {
        var entry = cc.instantiate(this.template);
        entry.getComponent(LBEntry_1.default).render(rank, name, coin, avatar, playerId);
        this.container.addChild(entry);
        entry.active = true;
      };
      Leaderboard.prototype.onLoadComplete = function() {};
      __decorate([ property(cc.Node) ], Leaderboard.prototype, "template", void 0);
      __decorate([ property(cc.Node) ], Leaderboard.prototype, "container", void 0);
      Leaderboard = __decorate([ ccclass ], Leaderboard);
      return Leaderboard;
    }(cc.Component);
    exports.default = Leaderboard;
    cc._RF.pop();
  }, {
    "./LBEntry": "LBEntry"
  } ],
  Modal: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a10e5sRCWFPIIokEQkdNm2n", "Modal");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Modal = function(_super) {
      __extends(Modal, _super);
      function Modal() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popup = null;
        _this.title = null;
        _this.content = null;
        return _this;
      }
      Modal_1 = Modal;
      Modal.prototype.onLoad = function() {
        Modal_1.instance = this;
      };
      Modal.prototype.start = function() {};
      Modal.prototype.show = function(text) {
        this.content.string = text;
        this.node.active = true;
        this.popup.active = true;
      };
      Modal.prototype.onClose = function() {
        this.node.active = false;
        this.popup.active = false;
      };
      var Modal_1;
      Modal.instance = null;
      __decorate([ property(cc.Node) ], Modal.prototype, "popup", void 0);
      __decorate([ property(cc.Label) ], Modal.prototype, "title", void 0);
      __decorate([ property(cc.Label) ], Modal.prototype, "content", void 0);
      Modal = Modal_1 = __decorate([ ccclass ], Modal);
      return Modal;
    }(cc.Component);
    exports.default = Modal;
    cc._RF.pop();
  }, {} ],
  Notice: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6cfdf+Ec8NOfKCqLqWb3Un0", "Notice");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Language_1 = require("./Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Alert = function(_super) {
      __extends(Alert, _super);
      function Alert() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
      }
      Alert_1 = Alert;
      Alert.prototype.onLoad = function() {
        this.label = this.getComponent(cc.Label);
      };
      Alert.prototype.start = function() {};
      Alert.prototype.showBigPig = function(num, duration) {
        2 == num ? this.show(Alert_1.TWO_PIG, duration) : 3 == num ? this.show(Alert_1.THREE_PIG, duration) : this.show(Alert_1.ONE_PIG, duration);
      };
      Alert.prototype.show = function(type, duration) {
        this.node.active = true;
        this.label.string = Language_1.default.getInstance().get(type);
        var action = cc.sequence(cc.delayTime(duration), cc.callFunc(this.hide, this));
        this.node.runAction(action);
      };
      Alert.prototype.hide = function() {
        this.node.active = false;
      };
      var Alert_1;
      Alert.ONE_PIG = "1BEST";
      Alert.TWO_PIG = "2BEST";
      Alert.THREE_PIG = "3BEST";
      Alert.THREE_PAIR = "3PAIRS";
      Alert.FOUR_CARD = "FOURKIND";
      Alert.FOUR_PAIR = "4PAIRS";
      Alert = Alert_1 = __decorate([ ccclass ], Alert);
      return Alert;
    }(cc.Component);
    exports.default = Alert;
    cc._RF.pop();
  }, {
    "./Language": "Language"
  } ],
  Player: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4cea4WuKU1GD7EAlqpK5ThR", "Player");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Helper_1 = require("./Helper");
    var CardGroup_1 = require("./CardGroup");
    var Timer_1 = require("./Timer");
    var TweenMove_1 = require("./tween/TweenMove");
    var util_1 = require("./util");
    var Config_1 = require("./Config");
    var Game_1 = require("./Game");
    var Api_1 = require("./Api");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Player = function(_super) {
      __extends(Player, _super);
      function Player() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fold = null;
        _this.timer = null;
        _this.coin = null;
        _this.username = null;
        _this.marker = null;
        _this.avatar = null;
        _this.effect = null;
        _this.pointLbl = null;
        _this.seat = 0;
        _this.cards = [];
        _this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        _this.inRound = true;
        _this.coinVal = 1;
        _this.cardMargin = 0;
        _this.cardRootPosition = null;
        _this.cardDirection = null;
        _this.labelCardCounter = null;
        _this.numOfAce = 0;
        _this.point = 0;
        _this.bonusType = 0;
        _this.isSplit = 0;
        _this.subUser = null;
        _this.isSubUser = false;
        _this.setBonusType = function(bonusType) {
          try {
            var self = _this;
            var txtBonusUser = "Canvas/user/bonus";
            if (!_this.inRound) {
              self = _this.subUser;
              txtBonusUser = "Canvas/subUser/bonus";
            }
            self.bonusType = bonusType;
            var bonusLbl = cc.find(txtBonusUser);
            bonusLbl.active = !!bonusType;
            var str = 1 == bonusType ? "x2" : "/2";
            bonusLbl.getComponent(cc.Label).string = str;
          } catch (error) {}
        };
        _this.showAllCard = function() {
          _this.cards.map(function(card) {
            card.show();
          });
        };
        _this.changeCoin = function(coin) {
          var color = cc.color(249, 210, 30, 255), addString = "+";
          coin < 0 && (color = cc.color(255, 0, 0, 255), addString = "-");
          _this.effect.getComponent(cc.Label).string = addString + util_1.default.numberFormat(Math.abs(coin));
          _this.effect.node.color = color;
          _this.effect.play();
          _this.setCoin(_this.coinVal + coin);
        };
        _this.split = function(subUser) {
          _this.subUser = subUser;
          _this.subUser.isSubUser = true;
          if (_this.isSplit) return;
          _this.isSplit = 1;
          1 === _this.cards[1].rank && _this.numOfAce--;
          _this.cards.map(function(card, index) {
            if (1 === index) return _this.subUser.push(card, 0);
            card.node.zIndex = 1 === index ? -1 : index;
            var pos = _this.getCardRootPosition();
            pos.x += index < 2 ? 0 : index * _this.cardMargin;
            var move = cc.sequence(cc.delayTime(Game_1.default.DEAL_SPEED), cc.moveTo(.2, pos));
            card.node.runAction(move);
          });
          setTimeout(function() {
            _this.cards = _this.cards.filter(function(card, index) {
              return 1 !== index;
            });
          }, 0);
          _this.updateCardCounter();
          var point = _this.getPoint();
          _this.updatePoint(point.toString());
        };
        _this.showCard2 = function() {};
        _this.getPoint = function() {
          var point = _this.cards.reduce(function(point, current) {
            return 1 === current.rank ? point : current.rank > 10 ? point + 10 : point + current.rank;
          }, 0);
          _this.numOfAce > 0 && (point += point > 11 ? 1 * _this.numOfAce : 11 * _this.numOfAce);
          _this.point = point;
          return point;
        };
        _this.checkBlackJack = function() {
          var _a, _b;
          var rank1 = null === (_a = _this.cards[0]) || void 0 === _a ? void 0 : _a.rank;
          var rank2 = null === (_b = _this.cards[1]) || void 0 === _b ? void 0 : _b.rank;
          if ((1 === rank1 || 1 === rank2) && (rank1 > 9 || rank2 > 9)) {
            _this.setInRound(false);
            _this.updatePoint("Black Jack!");
            return true;
          }
          return false;
        };
        return _this;
      }
      Player_1 = Player;
      Player.prototype.start = function() {
        this.fold.active = false;
        this.timer.hide();
        this.marker.active = false;
        this.cardMargin = .5 * Player_1.CARD_MARGIN;
        this.cardDirection = 1 == this.seat ? -1 : 1;
        this.labelCardCounter = this.marker.getComponentInChildren(cc.Label);
      };
      Player.prototype.show = function() {
        this.node.active = true;
      };
      Player.prototype.hide = function() {
        this.node.active = false;
      };
      Player.prototype.isBot = function() {
        return this.seat !== Config_1.default.totalPlayer / 2;
      };
      Player.prototype.isUser = function() {
        return this.isSubUser || this.seat === Config_1.default.totalPlayer / 2;
      };
      Player.prototype.isCai = function() {
        return 0 === this.seat;
      };
      Player.prototype.setInRound = function(inRound) {
        this.fold.active = !inRound;
        this.inRound = inRound;
      };
      Player.prototype.isInRound = function() {
        return this.inRound;
      };
      Player.prototype.addCoin = function(val) {
        this.effect.getComponent(cc.Label).string = "+" + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(249, 210, 30, 255);
        this.effect.play();
        this.setCoin(this.coinVal + val);
      };
      Player.prototype.subCoin = function(val) {
        this.effect.getComponent(cc.Label).string = "-" + util_1.default.numberFormat(val);
        this.effect.node.color = cc.color(255, 0, 0, 255);
        this.effect.play();
        this.setCoin(this.coinVal - val);
      };
      Player.prototype.setCoin = function(val) {
        this.coinVal = val;
        this.coin.string = util_1.default.numberFormat(val);
      };
      Player.prototype.getCoin = function() {
        return this.coinVal;
      };
      Player.prototype.setUsername = function(val) {
        this.username.string = val;
      };
      Player.prototype.setAvatar = function(sprite) {
        this.avatar.spriteFrame = sprite;
      };
      Player.prototype.reset = function() {
        this.marker.active = false;
        this.fold.active = false;
        this.cards = [];
        this.isSubUser = false;
        this.inRound = true;
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
        this.timer.hide();
        this.numOfAce = 0;
        this.point = 0;
        this.bonusType = 0;
        this.isSplit = 0;
        if (this.subUser) {
          this.subUser.reset();
          this.subUser = null;
        }
      };
      Player.prototype.push = function(card, delay) {
        var _this = this;
        Api_1.default.logEvent("123", 1, {
          123: 123
        });
        if (!this.inRound && this.subUser) return this.subUser.push(card, delay);
        card.node.zIndex = this.cards.length;
        this.cards.push(card);
        1 === card.rank && (this.numOfAce += 1);
        var point = this.getPoint();
        var fun = function() {
          _this.updatePoint(21 == point && 2 === _this.cards.length ? "BlackJack" : point.toString());
          point > 20 && (_this.inRound = false);
          _this.cards[0].show();
          _this.isUser() ? card.show() : _this.updateCardCounter();
        };
        this.isBot() && !this.isCai() && 2 === this.cards.length && (fun = function() {
          _this.cards[1].show();
          _this.updatePoint(null);
        });
        var pos = this.getCardRootPosition();
        var scale = cc.sequence(cc.delayTime(delay), cc.scaleTo(.2, .5));
        card.node.runAction(scale);
        pos.x += (this.cards.length - 1) * this.cardMargin;
        var move = cc.sequence(cc.delayTime(delay), cc.moveTo(.2, pos), cc.callFunc(fun));
        card.node.runAction(move);
      };
      Player.prototype.touch = function(pos) {
        for (var i = this.cards.length - 1; i >= 0; --i) {
          var card = this.cards[i];
          if (card.node.getBoundingBoxToWorld().contains(pos)) return card;
        }
        return null;
      };
      Player.prototype.selectCard = function(card) {};
      Player.prototype.unselectCard = function(card) {};
      Player.prototype.removeCards = function(cards) {
        for (var i = cards.count() - 1; i >= 0; --i) Helper_1.default.removeBy(this.cards, cards.at(i));
        this.prepareCards = new CardGroup_1.default([], CardGroup_1.default.Ungrouped);
      };
      Player.prototype.setActive = function(on) {
        on ? this.timer.show(Player_1.TIME) : this.timer.hide();
      };
      Player.prototype.setTimeCallback = function(selector, selectorTarget, data) {
        this.timer.onCompleted(selector, selectorTarget, data);
      };
      Player.prototype.showHandCards = function() {
        for (var i = 0; i < this.cards.length; i++) this.cards[i].show();
      };
      Player.prototype.reorder = function() {
        for (var i = this.cards.length - 1; i >= 0; --i) {
          var pos = this.getCardRootPosition();
          pos.x += i * this.cardMargin * this.cardDirection;
          var card = this.cards[i];
          card.node.setPosition(pos);
          card.node.zIndex = 1 == this.seat ? this.cards.length - i : i;
        }
      };
      Player.prototype.sortHandCards = function() {
        Helper_1.default.sort(this.cards);
        this.reorder();
      };
      Player.prototype.showCardCounter = function() {
        this.marker.active = true;
        this.updateCardCounter();
      };
      Player.prototype.hideCardCounter = function() {
        this.marker.active = false;
      };
      Player.prototype.updateCardCounter = function() {
        try {
          this.labelCardCounter.string = this.cards.length.toString();
        } catch (error) {}
      };
      Player.prototype.updatePoint = function(label) {
        if (NaN === Number(this.pointLbl.string)) return;
        this.point = this.getPoint();
        try {
          this.pointLbl.string = label || this.point.toString();
        } catch (error) {}
      };
      Player.prototype.getCardRootPosition = function() {
        this.cardRootPosition || (this.cardRootPosition = cc.v2(this.marker.position.x * this.node.scaleX + this.node.position.x, this.marker.position.y * this.node.scaleY + this.node.position.y));
        return cc.v2(this.cardRootPosition);
      };
      var Player_1;
      Player.CARD_MARGIN = 60;
      Player.TIME = 30;
      __decorate([ property(cc.Node) ], Player.prototype, "fold", void 0);
      __decorate([ property(Timer_1.default) ], Player.prototype, "timer", void 0);
      __decorate([ property(cc.Label) ], Player.prototype, "coin", void 0);
      __decorate([ property(cc.Label) ], Player.prototype, "username", void 0);
      __decorate([ property(cc.Node) ], Player.prototype, "marker", void 0);
      __decorate([ property(cc.Sprite) ], Player.prototype, "avatar", void 0);
      __decorate([ property(TweenMove_1.default) ], Player.prototype, "effect", void 0);
      __decorate([ property(cc.Label) ], Player.prototype, "pointLbl", void 0);
      Player = Player_1 = __decorate([ ccclass ], Player);
      return Player;
    }(cc.Component);
    exports.default = Player;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./CardGroup": "CardGroup",
    "./Config": "Config",
    "./Game": "Game",
    "./Helper": "Helper",
    "./Timer": "Timer",
    "./tween/TweenMove": "TweenMove",
    "./util": "util"
  } ],
  Popup: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "756eeJpotFA972j694PfnLH", "Popup");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Popup = function(_super) {
      __extends(Popup, _super);
      function Popup() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popups = [];
        return _this;
      }
      Popup_1 = Popup;
      Popup.prototype.onLoad = function() {};
      Popup.prototype.start = function() {
        Popup_1.instance = this;
      };
      Popup.prototype.close = function(sender, id) {
        this.node.active = false;
        this.popups.forEach(function(p) {
          p.active = false;
        });
      };
      Popup.prototype.open = function(sender, id) {
        this.node.active = true;
        id && (this.popups[id - 1].active = true);
      };
      var Popup_1;
      Popup.instance = null;
      __decorate([ property(cc.Node) ], Popup.prototype, "popups", void 0);
      Popup = Popup_1 = __decorate([ ccclass ], Popup);
      return Popup;
    }(cc.Component);
    exports.default = Popup;
    cc._RF.pop();
  }, {} ],
  Shop: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "22b59b5sv5O3KRVI8Z7dJWa", "Shop");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Shop = function(_super) {
      __extends(Shop, _super);
      function Shop() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Shop.prototype.start = function() {};
      Shop.prototype.backClick = function() {
        cc.director.loadScene("home");
      };
      Shop.prototype.buyClick = function(value) {};
      Shop = __decorate([ ccclass ], Shop);
      return Shop;
    }(cc.Component);
    exports.default = Shop;
    cc._RF.pop();
  }, {} ],
  Slider2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "60e38Bi/OJDvpmRhqGFGQN+", "Slider2");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Config_1 = require("./Config");
    var util_1 = require("./util");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Slider2 = function(_super) {
      __extends(Slider2, _super);
      function Slider2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.maxValue = null;
        _this.minValue = null;
        _this.value = null;
        _this.fill = null;
        _this.slider = null;
        return _this;
      }
      Slider2.prototype.onLoad = function() {
        var sliderEventHandler = new cc.Component.EventHandler();
        sliderEventHandler.target = this.node;
        sliderEventHandler.component = "Slider2";
        sliderEventHandler.handler = "_onValueChange";
        this.slider = this.getComponent(cc.Slider);
        this.slider.slideEvents.push(sliderEventHandler);
      };
      Slider2.prototype.start = function() {
        this.minValue.string = util_1.default.numberFormat(Config_1.default.minBet);
        this.maxValue.string = util_1.default.numberFormat(Config_1.default.maxBet);
        this._onValueChange(this.slider, null);
      };
      Slider2.prototype._onValueChange = function(sender, params) {
        var val = this.getValue();
        this.onValueChange(val);
        this.value.string = util_1.default.numberFormat(val);
        var size = this.fill.node.getContentSize();
        size.width = this.node.getContentSize().width * this.slider.progress;
        this.fill.node.setContentSize(size);
      };
      Slider2.prototype.getValue = function() {
        var val = Config_1.default.minBet + (Config_1.default.maxBet - Config_1.default.minBet) * this.slider.progress;
        return 1e3 * Math.round(val / 1e3);
      };
      __decorate([ property(cc.Label) ], Slider2.prototype, "maxValue", void 0);
      __decorate([ property(cc.Label) ], Slider2.prototype, "minValue", void 0);
      __decorate([ property(cc.Label) ], Slider2.prototype, "value", void 0);
      __decorate([ property(cc.Sprite) ], Slider2.prototype, "fill", void 0);
      Slider2 = __decorate([ ccclass ], Slider2);
      return Slider2;
    }(cc.Component);
    exports.default = Slider2;
    cc._RF.pop();
  }, {
    "./Config": "Config",
    "./util": "util"
  } ],
  SpinWheel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "daacdf5d5lMhrNHqrN6niHA", "SpinWheel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var util_1 = require("./util");
    var Api_1 = require("./Api");
    var Popup_1 = require("./Popup");
    var Toast_1 = require("./Toast");
    var Modal_1 = require("./popop/Modal");
    var EventKeys_1 = require("./EventKeys");
    var Language_1 = require("./Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SpinWheel = function(_super) {
      __extends(SpinWheel, _super);
      function SpinWheel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.wheel = null;
        _this.ticket = null;
        _this.popup = null;
        _this.toast = null;
        _this.btnRun = null;
        _this.btnAd = null;
        _this.audioSpinWin = null;
        _this.audioSpinLose = null;
        _this.audioSpinRun = null;
        _this.modal = null;
        _this.won = 0;
        _this.isRunning = false;
        return _this;
      }
      SpinWheel_1 = SpinWheel;
      SpinWheel.prototype.start = function() {};
      SpinWheel.prototype.show = function(won) {
        var playNow = Api_1.default.ticket > 0;
        this.popup.open(this.node, 2);
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
        this.node.active = true;
        this.isRunning = false;
        this.btnAd.node.active = !playNow;
        this.btnRun.node.active = playNow;
        this.won = won;
      };
      SpinWheel.prototype.hide = function() {
        this.popup.close(this.node, 2);
      };
      SpinWheel.prototype.play = function() {
        if (this.isRunning || Api_1.default.ticket <= 0) return;
        Api_1.default.ticket--;
        this.updateTicket();
        this.spin();
        Api_1.default.logEvent(EventKeys_1.default.SPIN);
      };
      SpinWheel.prototype.spin = function() {
        util_1.default.playAudio(this.audioSpinRun);
        this.isRunning = true;
        var result = this.randomPrize();
        var action = cc.rotateBy(5, 3600 + result.angle - 30 - this.wheel.node.angle % 360);
        this.wheel.node.runAction(cc.sequence(action.easing(cc.easeCubicActionOut()), cc.callFunc(this.onCompleted, this, result)));
      };
      SpinWheel.prototype.showVideoAd = function() {
        var _this = this;
        if (this.isRunning) return;
        Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_CLICK);
        Api_1.default.showRewardedVideo(function() {
          _this.spin();
        }, function(msg) {
          Api_1.default.logEvent(EventKeys_1.default.POPUP_ADREWARD_SPIN_ERROR);
          _this.modal.show(Language_1.default.getInstance().get("NOVIDEO"));
        });
      };
      SpinWheel.prototype.onCompleted = function(sender, data) {
        var msg = Language_1.default.getInstance().get(data.msg);
        this.toast.show(msg);
        this.onSpinCompleted(data, this.won);
        if (1 == data.id) this.node.runAction(cc.sequence(cc.delayTime(.3), cc.callFunc(this.spin, this))); else {
          this.node.runAction(cc.sequence(cc.delayTime(.5), cc.callFunc(this.hide, this)));
          util_1.default.playAudio(0 == data.id ? this.audioSpinLose : this.audioSpinWin);
        }
      };
      SpinWheel.prototype.onDisable = function() {
        this.onSpinHide();
      };
      SpinWheel.prototype.compute = function(id, val) {
        switch (id) {
         case 2:
          return val;

         case 3:
          return 2 * val;

         case 4:
          return 4 * val;

         case 5:
          return 9 * val;

         default:
          return 0;
        }
      };
      SpinWheel.prototype.onClose = function() {
        if (this.isRunning) return;
        Popup_1.default.instance.close(null, 2);
      };
      SpinWheel.prototype.randomPrize = function() {
        var rate = 100 * Math.random();
        var sample = SpinWheel_1.result_rate;
        for (var i = 0; i < sample.length; i++) if (sample[i].rate > rate) return sample[i];
        return sample[0];
      };
      SpinWheel.prototype.updateTicket = function() {
        Api_1.default.updateTicket();
        this.ticket.string = util_1.default.numberFormat(Api_1.default.ticket);
      };
      var SpinWheel_1;
      SpinWheel.result_rate = [ {
        id: 0,
        angle: 240,
        rate: 15,
        msg: "LUCK"
      }, {
        id: 1,
        angle: 300,
        rate: 30,
        msg: "TURN"
      }, {
        id: 2,
        angle: 0,
        rate: 65,
        msg: "X2"
      }, {
        id: 3,
        angle: 60,
        rate: 85,
        msg: "X3"
      }, {
        id: 4,
        angle: 120,
        rate: 90,
        msg: "X5"
      }, {
        id: 5,
        angle: 180,
        rate: 100,
        msg: "X10"
      } ];
      __decorate([ property(cc.Sprite) ], SpinWheel.prototype, "wheel", void 0);
      __decorate([ property(cc.Label) ], SpinWheel.prototype, "ticket", void 0);
      __decorate([ property(Popup_1.default) ], SpinWheel.prototype, "popup", void 0);
      __decorate([ property(Toast_1.default) ], SpinWheel.prototype, "toast", void 0);
      __decorate([ property(cc.Button) ], SpinWheel.prototype, "btnRun", void 0);
      __decorate([ property(cc.Button) ], SpinWheel.prototype, "btnAd", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], SpinWheel.prototype, "audioSpinWin", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], SpinWheel.prototype, "audioSpinLose", void 0);
      __decorate([ property({
        type: cc.AudioClip
      }) ], SpinWheel.prototype, "audioSpinRun", void 0);
      __decorate([ property(Modal_1.default) ], SpinWheel.prototype, "modal", void 0);
      SpinWheel = SpinWheel_1 = __decorate([ ccclass ], SpinWheel);
      return SpinWheel;
    }(cc.Component);
    exports.default = SpinWheel;
    cc._RF.pop();
  }, {
    "./Api": "Api",
    "./EventKeys": "EventKeys",
    "./Language": "Language",
    "./Popup": "Popup",
    "./Toast": "Toast",
    "./popop/Modal": "Modal",
    "./util": "util"
  } ],
  Text2: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "33666ScFZtOSIs1HT5qmapm", "Text2");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Language_1 = require("./Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Text2 = function(_super) {
      __extends(Text2, _super);
      function Text2() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.key = "";
        _this.label = null;
        return _this;
      }
      Text2.prototype.start = function() {
        this.label = this.getComponent(cc.Label);
        this.label || (this.label = this.getComponent(cc.RichText));
        this.label && (this.label.string = Language_1.default.getInstance().get(this.key));
        cc.systemEvent.on("LANG_CHAN", this.onLanguageChange, this);
      };
      Text2.prototype.onLanguageChange = function() {
        console.log("onLanguageChange", this, this.label);
        this.label && (this.label.string = Language_1.default.getInstance().get(this.key));
      };
      Text2.prototype.onDestroy = function() {
        cc.systemEvent.off("LANG_CHAN");
      };
      __decorate([ property() ], Text2.prototype, "key", void 0);
      Text2 = __decorate([ ccclass ], Text2);
      return Text2;
    }(cc.Component);
    exports.default = Text2;
    cc._RF.pop();
  }, {
    "./Language": "Language"
  } ],
  Timer: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d1e7cbJml9KPo0i8Y/KcUS7", "Timer");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Timer = function(_super) {
      __extends(Timer, _super);
      function Timer() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.sprite = null;
        _this.timer = 0;
        _this.duration = 0;
        _this.completed = null;
        _this._selectorTarget = null;
        _this.target = null;
        return _this;
      }
      Timer.prototype.onLoad = function() {
        this.sprite = this.getComponent(cc.Sprite);
      };
      Timer.prototype.start = function() {};
      Timer.prototype.update = function(dt) {
        this.timer += dt;
        if (this.timer < this.duration) {
          this.sprite.fillRange = this.timer / this.duration;
          return;
        }
        this.node.active = false;
        null != this.completed && this.completed.call(this._selectorTarget, this.target);
      };
      Timer.prototype.onCompleted = function(selector, selectorTarget, target) {
        this.completed = selector;
        this.target = target;
        this._selectorTarget = selectorTarget;
      };
      Timer.prototype.show = function(time) {
        this.duration = time;
        this.timer = 0;
        this.node.active = true;
      };
      Timer.prototype.hide = function() {
        this.node.active = false;
      };
      Timer = __decorate([ ccclass ], Timer);
      return Timer;
    }(cc.Component);
    exports.default = Timer;
    cc._RF.pop();
  }, {} ],
  Toast: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9f74bKQm+BNarPR7bnDfejb", "Toast");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Toast = function(_super) {
      __extends(Toast, _super);
      function Toast() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.label = null;
        return _this;
      }
      Toast.prototype.start = function() {};
      Toast.prototype.show = function(text) {
        this.node.stopAllActions();
        this.label.string = text;
        this.node.active = true;
        var widget = this.getComponent(cc.Widget);
        widget.top = -this.node.height;
        widget.updateAlignment();
        this.node.runAction(cc.sequence(cc.moveBy(.5, 0, -this.node.height), cc.delayTime(.7), cc.moveBy(.5, 0, this.node.height), cc.callFunc(this.hide, this)));
      };
      Toast.prototype.hide = function() {
        this.node.active = false;
      };
      __decorate([ property(cc.Label) ], Toast.prototype, "label", void 0);
      Toast = __decorate([ ccclass ], Toast);
      return Toast;
    }(cc.Component);
    exports.default = Toast;
    cc._RF.pop();
  }, {} ],
  TweenMove: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "21238R/+BlIzaVSvLlZw08P", "TweenMove");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var TweenMove = function(_super) {
      __extends(TweenMove, _super);
      function TweenMove() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.duration = 2;
        _this.delayHide = 1;
        _this.distance = 180;
        _this.from = cc.Vec2.ZERO;
        _this.time = 0;
        return _this;
      }
      TweenMove.prototype.onLoad = function() {
        this.from = this.node.getPosition();
      };
      TweenMove.prototype.start = function() {};
      TweenMove.prototype.update = function(dt) {
        this.time += dt;
        if (this.time < this.duration) {
          var y = this.from.y + this.distance * (this.time / this.duration);
          this.node.setPosition(this.from.x, y);
        }
        this.time >= this.duration + this.delayHide && (this.node.active = false);
      };
      TweenMove.prototype.play = function() {
        this.time = 0;
        this.node.active = true;
      };
      TweenMove = __decorate([ ccclass ], TweenMove);
      return TweenMove;
    }(cc.Component);
    exports.default = TweenMove;
    cc._RF.pop();
  }, {} ],
  en_US_tut: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b99d6dWx5hBq5cWjzQLp+PI", "en_US_tut");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var tutor = 'The game is for four players. A standard 52 card deck is used; there are no Jokers and no wild cards. It is possible for two or three to play. It can also be played by more than four players, using two 52 card packs shuffled together.\n\nThe game is normally dealt and played clockwise, but can be played anticlockwise instead if the players agree in advance to do so.\n\nThe ranking of the cards is: Two (highest), Ace, King, Queen, Jack, Ten, Nine, Eight, Seven, Six, Five, Four, Three (lowest).\n\nWithin each rank there is also an order of suits: Hearts (highest), Diamonds, Clubs, Spades (lowest).\n\nSo the 3 of Spades is the lowest card in the pack, and the 2 of Hearts is the highest. Rank is more important than suit, so for example the 8 beats the 7.\n\nThe Deal\n\nFor the first game, the dealer is chosen at random; subsequently the loser of each game has to deal the next. When there are four players, 13 cards are dealt to each player.\n\nIf there are fewer than four players, 13 cards are still dealt to each player, and there will be some cards left undealt - these are not used in the game. An alternative with three players is, by prior agreement, to deal 17 cards each. When there are only two players, only 13 cards each should be dealt - if all the cards were dealt the players would be able to work out each other\'s hands, which would spoil the game. When there are more than four players, you can agree in advance either to deal 13 cards each from the double deck, or deal as many cards as possible equally to the players.\n\nThe Play\n\nIn the first game only, the player with the 3 of Spades begins play. If no one has the 3 (in the three or two player game) whoever holds the lowest card begins. The player must begin by playing this lowest card, either on its own or as part of a combination.\n\nIn subsequent games, the winner of the previous game plays first, and can start with any combination.\n\nEach player in turn must now either beat the previously played card or combination, by playing a card or combination that beats it, or pass and not play any cards. The played card(s) are placed in a heap face up in the centre of the table. The play goes around the table as many times as necessary until someone plays a card or combination that no one else beats. When this happens, all the played cards are set aside, and the person whose play was unbeaten starts again by playing any legal card or combination face up to the centre of the table.\n\nIf you pass you are locked out of the play until someone makes a play that no one beats. Only when the cards are set aside and a new card or combination is led are you entitled to play again.\n\nExample (with three players): the player to your right plays a single three, you hold an ace but decide to pass, the player to your left plays a nine and the player to right plays a king. You cannot now beat the king with your ace, because you have already passed. If the third player passes too, and your right hand opponent now leads a queen, you can now play your ace if you want to.\n\nThe legal plays in the game are as follows:\nSingle card The lowest single card is the 3 and the highest is the 2.\nPair Two cards of the same rank - such as 7-7 or Q-Q.\nTriple Three cards of the same rank - such as 5-5-5\nFour of a kind Four cards of the same rank - such as 9-9-9-9.\nSequence Three or more cards of consecutive rank (the suits can be mixed) - such as 4-5-6 or J-Q-K-A. Sequences cannot "turn the corner" between two and three - A-2-3 is not a valid sequence because 2 is high and 3 is low.\nDouble Sequence Three or more pairs of consecutive rank - such as 3-3-4-4-5-5 or 6-6-7-7-8-8-9-9.\n\nIn general, a combination can only be beaten by a higher combination of the same type and same number of cards. So if a single card is led, only single cards can be played; if a pair is led only pairs can be played; a three card sequence can only be beaten by a higher three card sequence; and so on. You cannot for example beat a pair with a triple, or a four card sequence with a five card sequence.\n\nTo decide which of two combinations of the same type is higher you just look at the highest card in the combination. For example 7-7 beats 7-7 because the heart beats the diamond. In the same way 8-9-10 beats 8-9-10 because it is the highest cards (the tens) that are compared.\n\nThere are just four exceptions to the rule that a combination can only be beaten by a combination of the same type:\n\nA four of a kind can beat any single two (but not any other single card, such as an ace or king). A four of a kind can be beaten by a higher four of a kind.\n\nA sequence of three pairs (such as 7-7-8-8-9-9) can beat any single two (but not any other single card). A sequence of three pairs can be beaten by a higher sequence of three pairs.\n\nA sequence of four pairs (such as 5-5-6-6-7-7-8-8) can beat a pair of twos (but not any other pair). A sequence of four pairs can be beaten by a higher sequence of four pairs.\n\nA sequence of five pairs (such as 8-8-9-9-10-10-J-J-Q-Q) can beat a set of three twos (but not any other three of a kind). A sequence of five pairs can be beaten by a higher sequence of five pairs.\n\nThese combinations that can beat single twos or sets of twos are sometimes known as bombs or two-bombs, and can be played even by a player who has previously passed.\n\nNote that these exceptions only apply to beating twos, not other cards. For example, if someone plays an ace you cannot beat it with your four of a kind, but if the ace has been beaten by a two, then your four of a kind can be used to beat the two';
    exports.default = tutor;
    cc._RF.pop();
  }, {} ],
  en_US: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0d583jnHB5GtZrm4qYwUeQu", "en_US");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var en_US_tut_1 = require("./en_US_tut");
    exports.default = {
      PLAYNOW: "Play now",
      WIN: "1st",
      LOSE2: "2nd",
      LOSE3: "3rd",
      LOSE4: "4th",
      PLAYER: "Number of players",
      BETNO: "Bet",
      DAILY: "Daily reward",
      D1: "Day 1",
      D2: "Day 2",
      D3: "Day 3",
      D4: "Day 4",
      D5: "Day 5",
      D6: "Day 6",
      D7: "Day 7",
      "3TURNS": "3 turns",
      "5TURNS": "5 turns",
      TURNS: "turns",
      RECEIVED: "Received",
      LEADER: "Leaderboard",
      NOVIDEO: "Video cannot be played now",
      BET: "Bet",
      NO: "No",
      PASS: "Pass",
      HIT: "Hit",
      ARRANGE: "Arrange",
      QUITGAME: "Quit game",
      QUITGAMEP: "Do you want to quit game? If you quit game you'll lose ten times as bet level",
      QUIT: "Quit",
      "3PAIRS": "3 consecutive pairs",
      "4PAIRS": "4 consecutive pairs",
      FOURKIND: "Four of a kind",
      FLUSH: "Straight Flush",
      "1BEST": "best",
      "2BEST": "2 best",
      "3BEST": "3 best",
      INSTRUCT: "Instruction",
      NOMONEY: "You've run out of money, click to receive more money",
      RECEI2: "Claim",
      SPINNOW: "Spin",
      SPIN: "Spin",
      "1TURN": "1 more turn",
      LUCKYSPIN: "Lucky Spin",
      LUCK: "Have a luck later",
      TURN: "One more turn",
      X2: "X2 money",
      X3: "X3 money",
      X5: "X5 money",
      X10: "X10 money",
      MISS: "Miss",
      MONEY1: "Congratulation! You've got %s",
      TUT: en_US_tut_1.default
    };
    cc._RF.pop();
  }, {
    "./en_US_tut": "en_US_tut"
  } ],
  image: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "52ca30fTzNLN67UCDz74xRK", "image");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var image_base64 = "data:image/jpeg;base64,/9j/4QlQaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MCA3OS4xNjA0NTEsIDIwMTcvMDUvMDYtMDE6MDg6MjEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+0ALFBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/bAIQABAMDAwMDBAMDBAYEAwQGBwUEBAUHCAYGBwYGCAoICQkJCQgKCgwMDAwMCgwMDQ0MDBERERERFBQUFBQUFBQUFAEEBQUIBwgPCgoPFA4ODhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/90ABABa/+4ADkFkb2JlAGTAAAAAAf/AABEIAhwC0AMAEQABEQECEQH/xADCAAACAgMBAQEAAAAAAAAAAAACAwABBAYHBQgJAQEBAQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgQAAEDAgUCBAMFBAYFCgUDBQEAAhEDBAUGEiExB0ETIlFhMnGBCBQjQpEVUqGxFiQzYnLBGCWSstEXNENUZHN0dYLhRFNVY6ImNUU2ZYPw8REBAQACAQQAAwYDBwIFBAMAAAECEQMEEiExBSJBBhMyUWFxI0KBFDNScpGhwQexJDTR4fAVJUNiNYKy/9oADAMAAAERAhEAPwD3aVPRwvzD7g5dq9UDGcme6qBqNa1st5KxW4Gi1m57pFrLot0haQdQwECHPbPG/ZA2j6+igzJnSjIH6Q/U/dBVNul+tnBVx9jIOwPut5DDqNdMjhcPq3D6YcYW4zTyNgR2VSBrOGxPCNF0nNLhsjOXpmue1zIjsqzCwKfflRuKaG6+VIU5jNRW450RlhgqoLdpEd0FGmA4kd9ys1uLa6JB7qKaxsSTwQrGaa+jOktO/K0yogjzE7HZAvSBUkd0DRTLgghDggX5fEAd3QTw4qEsO3Zc77bnpTTLi0iXStRTGguIEbjhaZyN0OaZcZB4RlHtLthygEsJ3J2CA2AkGe3CCgS0oI1AzQ7dzTwEAMB0z37oDpk6TPKsAuaZb8lQstl2kd+VKGNYGGD8Kgt3hwdkCNGkkkRPCB58zQ4cjlATfhatMBLN9PpuiqqND90agQ0kaRwEAiGuUovTvPqoC0koFuGmoGlALxpHl+LsgNsafPueyCHy7/mQIeXd0BU/hKBZa48FBNJaN0DBuPkgrymQ7sgQ94J0ws5LBtpgiVG1PB2A9VsLNM6lgE50HSgjhLZ9FtSpndBYaHDdBTmNYCTyeECdMCPqpUU1kuhZFvp9iVYFvaGvC0oS7dBHebhALmwNkCi1xQKeTGkoFljI3UoWdDTxyoqxo5I2RnJTnMBkco5v/9DYaUjlfmH3DHNE7coAcTMHgKoF0ESsVuBpA6pHCYrWax8CCNlpFuqe3KBbmB24G/qgOkCJb2Kgy2gx7oyx9UP0OQZFAwC3tKuPsMeTC3kMcudqAPw91wbjKp+g4W4zRyAYPB5VqQFVsiRwjSqTAN+6Jl6ZTYAhViE3DTqGgwo3EpluoSZPdSFZDC5rtU+X0W45015ZUAKqKkQGjsggDgDPKzW4mglmvvPKimiC0AqxmmyS2JWmSqhOj37IINJcJG4QOa4g+yAnGeUC3Uw4h/ogpwa4ksMEcrnfbc9KpFgeCOZ3K1FZb2zJb9IWmcg6DAJ57oyW4kAkcoCYA5oHrygNwLQACgEgncndBKbS6R39UBgFrTHPdAuajY08IGgyJI3PKsEJ22+SoU4aXEt5UoLzFs91BAQWwR5vVANV7nRvxwgukIB9+UDWloG42C0yFzxufzdkAgCJ7lGojARMDZAktOvUTAnhShhI1ADgKAiYAPugW8te7URwgW8Et1ILplyAnBzgTG4QKeZ8rtz2QUxrgY/L3QR9OXEM2hBWlwadRmEBs06RA3PKCNDS47IE1WAO42WclitRaDCjZbqzmUy4fEFoUyqXNa52xPKyCgPdMIDLZkB2y2FFukH2RVtaY1N4QLrNdUI9kA9jt5h3UqFtJEzysgHOdIMqwU4Fx1HlaVGNa8wRugJ7N9uEE0cIBeWjaNwgxns1EmECIiQQpQp3ExuoomNLmS7hGcglkztujm//0djaREr8w+4ttQTugN0P3aJQ0Q7YQpY1Eo6huPXhJBm0wXxIiFQ14pgbhAlx2gCEDbZzTII37IHgoyxa1OXSDus7a0zKDWtpgkqzwyJ8EbcLVuxjP327rlfbcZFHVABHA5W4zTQ0PHO6tSEg7EOMRwjQqPxST9ESsl7RGzk2zop0EiT7KNB8CKgLTMdkhWQ2eCtxzpzAwCITa6RwgyAps0gcQII3KLIvV5SzsO6ijbJAAC1jGaJxgRMFVkLHtdsRt3KzsFLQ7UrsO1Mc3iCtSAHeyibUdUQDCztrQA0AwDJPKjamjSS0DnuqMukCGxMwqzkZuBJ4PCrJTwDMFQA3yt0jf3QOaQ4QRytaAvLWAy3hQEyo0Alu5PdBcHSgBrtJDef8kDQ4HYCStQU8AjZS0L2LiCYPZZtBtnSRHCCNgtO0OWtBWk/H/BQGzdA5rN4IlaZDVpEGeyAHQBtwjULJfpGn9EAPBLQXcqUG0AjVwVA0AFh3QKIDB6ygTVlrdtwoJQc54JDYhNg26xLSee61IAcyBud/VQUwEcboCd5WSdnd0AA6mmSgJgAaEFOaeWoEukugrNWLYIqAOGyja3sa5sAd1djHqUXyHH4R2UBNA08oLAgTyVsW9ss2PP8ABFKaXDZp2QGCQ4akS0Lw0uPZRJdsaoDMppVBuvYFNA2t2gnfsqpbmOa6BtPdBCXAaSN0E32QU5vdADgIlAoUwSZ7rNCnUxuPTuobADHl7Il8qnSfVGdP/9LZyIHwr8w+4jWtcNwgkmmSG/CjUJqb7tQHbRJBQZrTB42QW9oeJlAhzXwYG4QMpNOx790Snbt8x2CVmAO7lzrpD6bS5mnutsDcNLY9EGMeZCzfbcZFJ5iSNuFqM00Ahuocq1MSXw8gj4u6NDDRIPdQrJ0FwB7d0ZJqMmS3cBBdF5Dt0i1maARIW451fhgDUOVGoAguAgqKjQZIPpsgJoMEu5QPpt2BmJ4W8WaOo1rgW9/VWsscN0gt9VzEbBdpPZWDIYNJkiGrpBHNIGocLNZADO6xXSKADH6+x5SNC0gukd90DQxxBMxC1GaYA4gA+khVkOkAB3Z2wUoXT+MtdyEgd8JjuN1sLc0vB1LIYxrWskCQgPV5UCSPxJHflAxo0HZanoW4iOVihTdnF0TCyGUp3ee+0LUFuYRLmrd9BTXjTDlkGB6IGBrwZnZaZU57mnfzNPqgAQ8nTwjUARt5Tv6IA0zxv6qUWxriSANlBbS7UR2j+KC6jQWgFAio1xaW9ipQNMeGyAd1IDd5xsd10gp7g7busi2M9CgJ2+0SRygWWggkbII0iIHblAUkbASSgWWw6TypVgXuLjP6LLaUg4tcHfRAupr0xOyAQ0AQTugjf3T6rYupqbxwUUoeQyeEU5wa9uodtkYoHMaQCDJ7omLHcHbiNkbKB8M7IGTMEHdAVU8OAlAvVLpeId6II/zbtQCQSECnT9EFOAjUO2yzUKeC4SdlAnTpdM7IBduRCD//09ppvEeZfmH3EJDvhQLcz1O6NFEOaY/J6oG0nMafL35QZrHB20coBcSx4A4QAXuFXSRsUDqZBBA7oUbvOzQOQlYhAOh+6510jPpOAErbCP3BPqgxmAB2/CzfbcZVGlIPuZWozTHCGlvorUjDGxKNMikNTZ9FCnU3BwLXfD3RkhzfCq6WmWlBkUGtc5ItZABcCAtxzqN/cPbZRqCAAEKKBrCHygZ4Qe5pmAOUDhTbqgH5fNbxZqmN0OcCZVrIHOaHAkbLmGaA5wc3lWC++mpsukBNEeUrLJb6ZEkLFdIXBDTPdI0ZTaQASgPxC2R7LUZonVm02tLu+yrIXOElo7cfVSgrdzXktPxDlIGOG2v6LYBYBU/K1rTwqHOAbueECnAE6hwUBtY125K1PQXVbBGlYoUGu1e3dZGVT08RstQU8mfbj9VuhTmaXhvblZBM+Ij0QM1eVaZQkO5QKO1QaUaQgSUFNBAMDupQDHu8QyFAxpY6Z5QKdDdu6BdQntypRdEn8wUgM6TwukC3t0nZZFtMBBRqAbnugFzvLI7oKFMkBw47oGO8rt0C6jgTClWFn4QstmUXiC08kR+qBLzodpKBZDdXKC2/EtqN+4I9ECXHyR3lFExp0xPKMUO7XaUTEmo7S6PVGwVAGwURYBdEIogY8pQU4OjVCATs35oFtl0+iCtBB52QU+NBhZqFOnSIUCagc7b0QLI0j3Qf/9TaaZavzL7wHy10oKMnzDkqLC6heRpKKKhTbMnlErPHlCIheADKzVhAqjVsN1IlZdEBwJPK2xVuYVCMYhpd/eVdGZQaabIduiDdAZt37IMeiCHQeVBm0zBlHOreyQXKriwyJfHcbo2zKDoZ5voiU0FrRxylSMZ7vMudbPoLcYPOzmgcLcDqlMPaY7FQBGlzQoLOoOlvKC2CXaXd9ygfyPNyOFYzVEmI7LTIS2Q30UoOmdLoHCkB1C30XSAYCVmFVQWEHlpXOukTTTeyW8qNKgsa1p33lAxoDzC3izTxTaQB3G60yBzAefiUopjXNeCFA6Q4QeeyCBjfzIJDRp0oLc+Dq+iBJDnSJ2lA5oLAB9UA+pKBTqYPmJj0QOaSYAMwEBlmse4QLJcDHYILZsS7sgNhpn4lplCTO3HZBACXb/RGoVrIJ1eqCtWky3upRZpjZw5PKgg22QC4QY9UCqoOgFSiqRPfhSBpLR8IXSBZElZACW1YKBzw0iAgx3DR8igJoIEt4PKAtXiS2N0C3N0DdSrAOeADIn2WWxtawgObsUC6jS4x+VbYhTqYPmR0gmNEakEJaJd6oEVC3Us0U1waZCKa78Ruy1ilY5BALe6VCtx/aDZYUTYkFphEoizXyVWBAfhaVp0YztRMIDLSAI57oFkAGAgvQ1zDPKBZ8sNQKexkz3KBFSiCg//V2WmJZqHK/MvvDc4PbBG4QACWAeqiwNSpKKOk3V5h2RKytXk4k9kRRGpu23qs1Yxy4sepErNpOLN52IW2KyC5rmyNlCMN1Nniamnf1VdGdTGoAt2A590RVXTy3lAFLS6oZUGQxh1Qdx2Rzq3PIJbGyq4sV+kPkHdG2XRAdCJT/K8RHCVIxqlMa9lzrZ1IAcLcYPaRG7eFuBjXAiBtPKgF4gg9/VQEx0DUeUFkfnQGXa3COVYzTHAaYWmSgXfm+EcKUMpjT5juDwpA4APYT+ZdIAcxoEgbpWSqjRokHdc66QsyxrQ36qNGNa2oQPRAYpgO9FvFmmjTPqVpkD9nSpQVJ2xJEqC3Q3ePkgrUC0l25QGym4t1AbDsgoNJMPECUFFgJMcIGt4hABaTPogWHSId2OyB7HNA9Twgo7AmUCttP97ugNgkboDbSDgYMLTIXOIIAE+rkEL3F7QBA/e9UagatMnzzIKBb/Lp0jlShg1KCgPNugCpqmRuUCnvD2Bo2cOVBbAIiVIGCW87rpAJbp3WQqoS5wdG47oDDwWiRv6oFPM7dkEZzHZAzg7II9use6lWElukrLaQ2UEc4THZbYhG7hHZHSLptIBB4QQ0g6ROw4CDGewA8ys0VGoaQ2D6orIpNDG6XGStYpSarS2pqaZb6JULcHVDLogLBCwGEx3CFG4CJCrFQOfG53WnRGATJO6Bjmtjy8oE+HIj8yAA3n1CBbtyZ39ECXN3nhAt0+s+yD//1tmYdJB7HsvzD7uzXaZBGyGw1A0j5I1GPAJRWXRI0ER3RKdSc1pOoTPCIF/lO3BKlgQQHOg9u6mis2kzaSZ9lpnQ3RpgKLIx2hpdPJVaZzfKABwQjHcB7SCT2KzauwU6ZkuBgqwZJp1YDmO+arOluJY0B3KLIxnaC6Y3RplUgA0RsfVEpzmwwOafmlSMd5IO2651s2mXTEbrcYZTjGy3BYaSBCgmmW7ndQC1wB3/AEQPgVGxMIDY0bD07rWkyVVn8u6rAQdQgBSgqTt9LhsFIMhgkmNpXSAX8QeVm5L2lloI0x9VlrSnwABCiq0HYtMIGkGGk8hbxZotIjUNitMhme3sgZRLJLSpoGR5o5BTQW9gAIUDKb3t2iR6LXaJUcHcrIW1omAUDGthxkrUibDqcXERtwpYsCaYkT2U0thmlrQCPlCukBWa7bTx6JoK3DpP6KBragG0IGNaNJ091plQ2MRt3QXUBDhHB4CLsDIgtJn2Q2U4OBnsCppTGvDiJ2jlNCttyOAmhdQEAEclNDHcxvin+KlgjmtG7fMVmAm1QOQukELxEkKWBetu4hZBANLI/igTpgxKoto7oDaZJQQHzKLCapJdCabA2ZG2yaBuj4o+iqaJLgTDUaEHADSeUEIIBPMoMeoGxvs5ZoW0unnZQ2y2BhG/K1KAcGlh+aqMYsDTpnlZ0FOpgGQoGNLdIkqs6XVaB5mmVo7ggzAIQ7gkODjHCHchBO8wUO5YHqh3FkAOModwKuggfNIdxBa2duVdHc//19oe2ANuF+YfcGWhwBb9UAQN5KNwh+x8qKzaLQW+6JTAAD6ogX0yXSDKBQB1lBl0w6B6ILLX64PCBbaRZU9kGe0tI91XImoSHEOMDsudbhlBmxI3HYrWPpaawuA0niVUVVbrJgyB9EWMbQdUDcorLpN2hyJTWuDm6P1SpGHUeGPIJ+S51tl25luv83otxg2d5PC3AxomSCoBDwDBKgmxOwlQOaDEAQtQGGvbyNitJkt7mtEIwUCBJbv6qUNYzUJHKkDWy1u/xLpAFWmXecFcnQsOcBJQFIJg8oCB7BA0tc4SBwt4s0IcAIK0yvSAdX5UE8OJd+iAmu8w9EFvBILhwswE2YmNl0grw9TSVzoW1pAkcoGMJO55W56ZqVGiJB3UrcA2Ilx+SkapoZPy5CrCnHSN0oWYJlZFgA9kBEOaZaPKtMpwZKAy4k7DYIEAuDjqEe6AXU36pmZ7IsMDeJECN0VTdLZb+iCqrnADTvCABT8xf27qX0La0Ml/YrEFeQ8BdILPmlmncJQjw5JI4XOgmNb3KsA1QI8vKoATpjugNkR7oJwZPCLCKhgl3YI6KY4Rv34QEzzOjsgUWDVHCCQzVJO4QEXyTHCBFZod5p3WahVMSYUGQ1ugiTstQC6ATHBKqkVmPG4UqEOa8CSVkFp8su2QGA7StuanciOUF7/ogqCUAvdpHugCY2PKCnRxCsCnx6Kj/9DbiHHsvzD7hLpaYQCWndG4VoOqUV6NIANDhwESiLNUvCIVDnb+6gjIdUIQZbRAAVFVHaXhALXgv4QZTdARyDWLTBCxW4Ki7stT0tOaQCZ7qouo1pbsYRYxGtLKkjedlFZzJgEjZVKU/wAtSQdkqQhzGPfJ9VzrbMZSLDqB8q3GGQdOkLcEdpB+agB7AfhUFTMNHIUD2kkADtytQOBIAJ4C0mSPArCR2UYKFPSTKUOpmFIDcukCzqaIO4XJ0VpBbPoUELoIBGx4KCtLqTpG7SgyGawNXY7reLNXrZU8hEFaZA9unyygMNMcoDYzue26AA4y6eFmBrS6dMLpAFRvm2O3dc6KawctO/ogIS0b8rc9M0Bl4LRsVK3FNYSPD7qRrI8DQQwneOO6rCVGT80oDR4XO5PCyKLQfi2KAwS0aC7daYC7hATXkCIRS6gkaygWHyiwYdH1RRCJQW4A9kC439u6l9CBwc3T7rMgp7PRbggLmg+p2SjGc+pTOlw2WKDaNbduyQLH4jo9NlQJYWmEFtaZQMdu3T3RYx3UzBlGyQd49EDC0xqagB586Cnt1OBH1RTNiCAgx6jDHKzUAwOYCRuoGAg791qC4BmVQt0wQe/ClCTTIMn6LIrU4nTCBjeFtzDAGx5PCCDUGkeiChUh0HlBbg17T6hBjFpgPQXILfdWBL6ZA1dlR//R2zW6F+ZfZhZaXGTwo3EcAACOEbU3S58O4RWUwQPbsiUe/ZEKJOrdZqwNFn40nhSJXohoAn0W2PqRX3CNioifiQZLQ2EYIqtGqRysVuG0d9j8S1PSU8sI5MqoVWBDNlnJYGmTrbPCRWWyYMLQVXYwkRyUC9DTDUGWGODdIKMiAmQfiCAmGYJQSpsWkc90FaZdP6oMinv5TweUFkOjQziUZqg19MxO3dVlPKXAl26BoDVoGC13lQKdLTA+FYdFES2RwCgjuQ6eVA2A+N5K1AbQ4/IdluM1ZgHhVlZGtsfVEoqXPyRBFzqZ24KIUX6ncIp1NqAXaSSDsVUQMLIcTITudZR1C1zQQnftnKbpRMQXDZa1sl0yrSzuLp4FKm539792V0x4rWc+R7dplS0o3Au7yo59bTGlejHheTLkem/C8JqDek7Udg8chd/uvDjOTVedfZZika9hUFVrfM5jvigei83JwvTjzNcLHtqOa5ha5uxXhyx1XpmW0a1dGA+E5p2Ox5QVUpElpB7LOl2pu+pr+Snc6THZOgajtxsE7i4sim0RDh8ita2xbobaBe4NY1z3fLZa+6tScunrW+Xry4Ae8ilT/eK648Nc8uZ6lHL2G0gPHc6o/wDu8L0Y8TzZcu4KplzCqrCLdzqTztKuWCYZvBxDBrnDXRUGugfhrDsvNlxvbjyPIeGNdAEu7FefWnbO7JeHa/fus2uePtTRAnuUjd9gawySURYmY7d1KLjfbnuoCbMzwtY+wmqHAyDLXcq0KFOTPZYBnyDUEC3eYakC/MeEdIg1jnhFR7RUCBTQGmEDogAqxmqOwPvuqwU90NBSrAD19Vl0LJcTB+FENp09R5WY5grfEAtwQlwHspQk7uBQWDuQrBbgNO6oXwNuUAPkmEH/0tsZ5m78r8y+zFlvljuo3CSSDpduAjZbSC+IRXpMaAADvKJROaRx3RCHkDdyzVg6Ia4FzfiUiVktOoQTBHZbY+oKgCNipg+qDJpU5BJRgqtTAEt2jusVuDpEytT0lZBgQSqgK8RsJCzksLojWd9o4KRWWGEQWnbutBVamA9xG22x90CWs/EA1c8oM1oDSjJjqY16htPKAS2D5dgEFFwBkjdBQeR5okIHU3OLgeB6IHHY7cFGaEFzXEEzPCrITu4SgfTayNwtCtLWvBBgIKqFuggDdYdC99A0mB3QDyASoH0YA91qDIiRq7hbjNVs6AQqyHUWzAn2RKYwtj0KINzpYCUAADsED2aRygCvSD9+/ZNmgtaNEOMEcFXs2mz6FpWunCnSpk/3uy6Y8NZvLp7dtl6lTAqXj9RHFLtC9vHxPNnyvXYKdNgZSaGMHAC9eOGnmy5Nqc6Bt32K6TTnsOrVseEuUjNlp1Cq6m4FnYyApqWMTKyvIzFYsaxmI0Bpp1TFWPVfN5eN9Thz3GvlsN1DheR6ABrvD3O5QUdNPTIMxv6IgQC4mAS7sAtfd7anJplW+E3948eFRIB5e7YBdMeCsZ88e7b5boUmzd1tbtpY3gL0Y8Onmy5np0aVtbsDaNINA7r04YaebLO1bq7iYn6LrJHPdB8+6t0zqrkAQOFhqMhpp3dI2lw2WPENJ7FZ7Y7TJo2JWL8Puqls7mkefUFfP5cdPfjlt5x8z/kvDPbupoBEkbroBPBUCm6t5KlFjW0yO6gsucTBGxVx9gHtLBB3B4WqADtoHfZYBBsiOyBWkhpA4QAHhvPKOkFq1BFAYDDHPqgTp7zugc10t0nurGclloGzt1WCntY7y6dkqxjv1NG2wHZZdA6y5sIgqZLXT2WY5irCSCFuCAuLQCdvRShRZ5xHCAankcrBZe1zQD2VCwQHCO6CVGukQZ9kH//T2t3EtX5l9qQOp7j7I6SLeGxq7qNFU2jVPugz6ZgeqJTGncyiFvptMyVmrA0BDobt7qRKyJEif1W2Pqj2mJ7I2lMcb8oMnXobA3RNEueC078rOlhtBsRvKsSnvYXt9BPKqEV9TIB+H1WclgKckgDg7SkWsymC0EHj1Wk2GqCQN9jsSiltpHxBvsgy2uHfda0yaASE0KZzpI+qyKLQ8QOQggIaPl2QNpy8Ej9FqRm04zpIIj0Kvaz3bKJJA9QpoSO559E0HNAJidoVEdBI24QQtY6YPHZTTWyzE8x7KaNqgOIhZ+q7WwkOiNgtRLkyhLhtx3W4zclahIaBxyqm1t5JHPohsQbO5H1VZW5vlAnhXSbXTBLg0ceqy3o0NPzPqOFdIqnRuLh/h0mlzuNhsumPG55cmnt2OX9MVr8iW/kG69/HxPJnz6+j12hlv5bdgpt9OV6O2R57yXLyIyQT+Y7zyty6Yqgdo7jurbtNJErOlDss3Df1alW12k7Fbx8M5Y7ZVOlTuqNW0qCadQHSPRyxnj3O3Hl2tLq0alKtUt6gAfTdpLV8i42PodyqVlc3DtFFhfvy0bLWPHazc3qUcs1K4BvX+EBvA3K9M6b9Xny6jV1p61ththZj8OmHkfmK9GPHI43ltZHiuOw2aPyt2Xaajjd0Dpcdzt6BXaaBGl3JgqKhYJUXaPP8FNLsIg91dJsdMkAA7wZCLt5maKWptG/9Rpq/PsvN1GHh6uHk3Wn1PLUnsF8qTVfSs8LaQ6Y2HZarMCBzOygoNgoC1AbR9VNCxBEKzwBrMJHt6qhAZus6BSBymgsu2dATQx3N178EdlGtiYCZQ7hVGw3SdirIdzF8N8+yWHcdsWgRuEiW7E1wDfMPqqgXBhOx3SqU6ByNllqVjvLQZ7I0jXNcIB+qzHI7T5V0gUR6FLBW2xJ3CmgLjrkKgRSjngoKIYJA5QpQD5k8KyJt/9TbaUOEFfmX24Gqwt2ajpCdWluknhRUpOBKDNpuBCJTIkTMIgHDT35WasXQbIJCkSnOGxDu66MfUp2vTEy1Rs6nwD2CBrQ1zXSd0CnBoEHlBk0C07DkcolNqGWwD3RCnnWND+3Czk1AUXOpvjTLUhWZpDgSTAK0xPZTmwwid0bDSl1SCdggzPDBbDfi99ltAU6pDodxwgyHCXam8d1lAgOaSQNlBHU5G3fdA1jW6YBj1W450ekgSDIHK1fTMLc4NIPZZaM0AsLkEYHBsnmUDWt1FAtrdDz6FAJaC4lADfI+DwsX239DG/FEbHdajFZTWNP5oWoyotDXSqKY0kk9lYhzW+6WNQb27Qe6z5a8GUKL6jw2i0uI2kDZd8MHHLN7lvgYO987S3/5bP8AivZjxPHlyvVpMt7Znh2tMNHG/P6r2TikeW8lpTnHUARPrur26Te1yPWVAUiNkAg7qwFwqKDZQixSJ4BKNM2hTNJzXu2A33RSLixw+tdPuyyX1N3D3XL7mV1vIgf4Y00WimzuGjdWccjF5AGpvBPyndac6jnEnf8AgiKHCC/dAs7u27IDBBQU5oPyQBo9EFtBCC72gL3Crm3IkgeI36LlzeY7cN1WgagRDx5ivk2ar62OW4EDztgbDlZouowbnt2UCg4gw4QOyCzDj7ICDRpmUC5IJBOyCuJMILBa9sEeZAqoAAdPfhAho8wB+qyGupgQWnfuEFVAIBJ3W56GOXOktClBNHrt6qQXUbEbbFUVoPMbBKFPae/Cy1GM86tiNkdIoAN2aFmOLJg6fotwJAJcQtUU5kSSoKa2BI78IKJf34QKcAfMESoC5zSHbLUR/9Xa2n91fmX24skv39NijpCKlPxPM3nuoo6TDqhyDKDQxwPZEo3HkjvwiFVnHZZqwy18savh7qRKzXeE5sDk8LbH1Yznhp0kI2fT8PQgEu08cFAqo7cIMq2LQTq2KJWU9rRpI47ohVQ7ksHHKzk1EpuD4kJCnwR8lpiewO9uUbLokeJ5tigyiWu52W0Fpa0NQOHdZRRcQ0tCgpjqgbHZAxjJW4503dgkb+q1fTMBVZTc0O4WWgsimIJmUDGgiSeDwgMNLWygqY8p+IoBDt4fwNkC6jRr247LF9t/Q3ZoDlqMU5p1gELUZEPKd9wVRYEmBwrFh1KhUqODKY1OJ2AXfDHbGd0922wLxA194dLf3QvXjwx48uV69BltbM8Kg0ADuV3nHp57yWoZPK6yacb5U3la7m+1C2TKvtNaCWhYokQoLDNwrBkClPyVF+HRYdbjuOyEEys0GGCPdGgOqazB3QDv6q45FAufJlpiwt/xBSXcWGRO60IRAQQcIKLY3QU1BZ4QU3hBEDrUgOc127XDSfqs5eY3hdNDxm3dZ4jc0QPLqL2fInZfM5Zp9PjrH1OLQQN4Xmd6WSS7S4wiKc2e8wgEcx6oKc1ze+yCO80EbRygA1WtEE8ooqbh6IhT/hQKAY5wEwVkOFJzXGPMDxG6aqbMNrVcwvFMkD2K6SXRuMN9Nw8zhpPoVmxR0w18AqQR8nY/CFQM7KUKJAMnhRqMV7ZeXDj0RuKaXPOwWY5HBzoj0XSAfM0zHKoh80gqBRnYII5sCJ5QLDZlvoJRKn5S0crUR//W2tn4b4/KvzL7cSpyUdIWX6RtyFEXRe5z90GcylTJDzyEZo3NaTPopVjFrNc523C5tn2rSGw7hajNZJazbZbY+pNUyQ5v5UbNY8OZuECqm2zfhQJcxxLS07DhBnUd418qJWXtGyIxnA6jBWcmoZSJJgjYd1YVk7OMnhaYntjvcWvBHCNjYATqKBrmwNXK2yZy3yoLpOcNlgXUdsZQEA0sBPKM0bBIK3GREEcLVFucHM0v4WRGBukBoSJTmuc3YcLSI7jUgA7vE7u9VkLrscBM90dIoMc4jdc7PLbIa2TDuIW5dMWbMoMaHODjHotzy560OnTqeJopgvLjDQuuPFtnv09uxwGvVbqvIpN50henDi7bty5OXw9y3trWybFuzd3516sY8eWY3Oc5el57kBzVFRBTWwZQM0uO44QWKT3iI+qB1O3AHndwgJxpMHkG/qgU6q94LS6G8oEaOTqn2UoppgqBp4QRaAuagAt4WQ0CGoFu4QW3hBCkEHK0LUEUAd0F03Oa9XFHiZwtxrtb1rfLVHh1j7t4Xk5493DWtNc4mHcdl8575VVWwJjdc7F2WxzjO3Ckuks2Magr3J2oHHVupva6Lc3S8uiVZjtLloLyx0QQD7iStfc2s/esi1sbquxzadJ1Rp9BC1j09Zy5Y9W1y7iFVs19FFno9enHhebLkZlDLGH03+JXcahHIHH0W5wrebw9Nlth9tHg0W7cTyvVjNPNlls5twxojQ3f8sLra5y6Yt3hVhiLCH0206p2aWrzZ4bevDNpOIYXc4Zcut6g1NG7X9ivHy8Wnrwz2xu2k8rz2NXyx6zXN8jO6xrysVJad+YWlY7g4O1IDplrxpbsswU4aPLC6QU0k7HhSggwAbKAXCSgBzY39ECgdTj8kSoG7rUR/9fbQR+YSvzL7cA8gmG7BHSEnZxBUQ6m0Dzd0GUyXNO26M0bg4MUqwoE6T6rm2bRqwC0jf1WozWQCCN+Ftj6khulx83kPZGz6bdtMb+qBFUFjtM8oA2Y4AdkGZSgkTyolZI4RCajJ3HKzWodQMw1ysKfUp6dhtPdaYntjuaDsDujYmtkQUGRShjYIkLaINLT5d/ZAROmTESsotmmqNxB7qCywN2Rmms0tbutxkzylshavoL8r/KZWRYlrmgDyDlIlZGzhDfLPdaRTWu/s37t/eQAPw/IdysiqrBUaN+EdIjKUagDuO5VkWnUwT5I1OPGncrrjxbcryae5YYDXuA2rXIZT9fzL04cOnnz5496haWVmAKLQ53755Xsww082XJsbzqM+q9FxmnnuVXMAD9FjTKpPZaTtCCe4lGhtYXGY29kDxbuPIgIDaylTHmdMdkE+8ACGDS30QKc4uMoBJKAAN0F8BSgAAOVBcoCc4ei0JyEFEDZZFOJGw4QQtkBBYECUEgFIKiCtCy07D1UFHy87qCBurjZBNO890wKXi9r9/wWvSiXUYqj5hc+aO/Hk0EOJABbx/NfJy9voY3wbuQdQkAbBJGtl0tzBA34UvHtqZ6OZa3NUxTpucTxAW5wsXlkehRy5iNUAvYKQPdy7TgrjednMyxa04NxWdUeeQ3hdseHTlly7Z1vhuGUIFKgHOG+pwleiYR57lWWawaIY0NHsIW+2M99Lc8u+Iz81rwzss7rXhjyAiSs2NbUdt+6zpLPKwYIPdamm5dAxe0p3+HPIEXFIF7H9/dc+WSx6MMtVobmTpcO/wDkvkck1X0MPMAS0EA8+qzrwfUNam1w1g7jdZaYpcD8QlBQbTBlZgZ8Qk7+i6QUGCCe6lCRrBidlBepoHm5QCCHSDuECabdJM+qJRvgbgbrUR//0Nuc9jxsvzL7cJc4E6eIR0hT5dBG/qoaOp6iQeyGma3Vtp47oxkGo524nlZqxiNqPc4sOxlZkaehRI0wW/Va0lE4RvO3otbTRAIc+Fnas5rtLOJK0m2PVGrcHdZ2pIa4uEpsZ9Cm74grEZJcIlKaK1SfYcrPtYNokgjj1WtFO3fIcfkqxPbHDfDfJRsVA1Huc5wgDhBm03NLdwtufco0oMtQ7kdsAXbrLSjGnU0wAoGMPiNB+iM0wDaIn0W4ycwtpjjnZavoLLofxLVkEBBkcHkICDCfM0/MKypo0Alu52XSYxCS2SXEggcFcdt6VqbszbU7cAc7rpMLU7tPVw/Ar29PnHh0TvqdsYXs4+m3N7eXk6nV1ps1lhdjhzJpt11e73L148enly5LkyZBMjn+C7a289iEBvmO5Kuk0m0SrFXpmCeyGxtoucJH8VF2NrKYEVNk2ghUZR2pgD3KbAOrPPLpTYWHmU2JBmU2IT7JsTUO6CGOQqBO4Uop3E+ii6QDYFDRhE9ldmggbJs0hBIBUNBIJKGhgbQhpCJaQhpGjS31SGk+i0aE5pjZDSmgk7hTSLLD22TQmk9t0xmmmRaUyXFjvgqAhyZTuXHw092X743NVrGBtLW7S88QSvn58Hze3sx5NRm0cssb/wA7qyfRvEKzg/Vm9RZ9GdRw7C7XanRDnDlzl2x4Y5XqLfoe1waIpMDfSAu0wkcrnapz6kSSS71P/BdPDHsp2o78Kba0AAe8qKLdGdFgEqLpekq6VCDCrOgFpcqsWBAghTSmU4eHUyNi0hJjtZfLQryk6jcVqZEaXbD2Xx+ear6/F6Y3h6zJXKZeCz5inMc0x2WdxrRDxvCbhoBbI0x9VPCapjQRDYV3DVU5jlvUqqABJB5TU/MKc3cyFLBYYIlvblQLduZ4I5RmhdJHlTcZ8v/R2wFrRAC/MvtwotGuT3R0gdBbJJ2KimUNYhsSJ5+aDM1FrYA5RjJBDgT+YKUjBaCK3mG8rMar02gEDT25W0WXt4PIUoXTINThYi1nFjAB+6V0jnSHs2JbwsNEgAkHuUVlUtdM6ux7LUGQ6oIG26lCtW8dipisZVLQWGOAFtKt/wAII78qMT2xrgEEAHf0VbZVvGzSOyB3wHcCFtxW6ImUEa1pEzKy3EB2LCFFWAGED1RmngaYc74e63GTDpcJG45Wr6AjSRDgsiUyRUA5ngJEprmgSQYPcLppJVtcNI9T27p21uWM6xwe7vWhzB4bPzFwgrvjxPJlyvcs8Ew+0Ot7PGrj8x4BXuw4o8uXK9EvLgGu4GwA2hd+3Xhy3vyh3byroW1sQAjNM0F20cKobToQPNA+qApo09zuR2QA6uSDoEAqBJc75qATzugsyeEEAIO6Ai6BPZBRMoJMKwETIiFRUFSkQgcFRtbGyY7IDAngIBIE7cIL0g8IKDR37coDDZEjhBCwgcIK0mOCkFik47xstIaKfGxQohbuJkN2RlZt4QXpY3hhRotxcdmAtA3QKe2qd4O/6LOvLe/BT6VU8D6LUxYyCaLzENI9VbNMCbQcOxUVZoOIgBSrC3W74IDd1GgC2qfuFBfgv40lBDQd2agrwXfulUU+i8D4VUUKLv3UBig4DcICbThwdHB3Vl0barjuG134nUfRplzHiRC+fy4br3cfJqPKdhl8z/oHfouF4tzTpOTyqrht9pH4Dv0XP+z1072I7CMQJnwHQn9np94A4TiIMm3d+if2en38EMIxSQfu7oT+z0+/izhWJkkCgVqdPXO8uy/2PiR38B2pX+z1PvFnBsSc0/1c6lqcVh94UcFxZp3oOAV+7PvAuwjEzsLcx3Kl4bT77QRguJNMCgYWf7PT7+P/0tt0r8y+3AO5R0gXQ46XcDhRTmMe5ogwQUGQNUAEzHJRzyR4FONPLuVKsJDC6qCVmNVnU2chbQFWnp39VKFsoOnUCsRazC0jS48LpHOqqgEyOPRYaKAAcDGyKzAW8xsVqAS1zZ9DupQDPDD5J3HZTFYaIe0+GdK2lPa3RTBcZUYntjVJD3VDwN4VbZlEh4BHogMNIdutuKywwd0EpPjynsstxdV2wjkqKE6/KAjNZMP0w7vC3GRk6Yb2Wr6FwARPBWRehrXtqTEbhWTyzfTNt8Ou78jw2Q3947bL14YvNllp79nglraBr6n4lfuDwvfhwyvJlzWPRNXbSIa30C6dmnn76EO9N1d6bk2Y0SAVuXa60bSoF5nsqMjwaQI1HccLNShdXDSWtaiEPcSdUx7IF6pO/KBg4UFSPRQG5iBY2KCzygnOyCh3CCwFYJMFUGIg+yixVPS87mG9yo0fTfSpnyjX80DDcsH/AEYQD96b+439EF/e2j8jf0QUbtonyNQQXmw0ho+iCOvHxsG/ogoXjo8wb7QkFG8fyAtIW2+qOnaEKIX9UbSjKnXr/RALr2p6o0EXdSOUAuvKkDdYtYtD99rRsVqUUL2t3K1btU+91HclQQ3NQfmUqwt11UPDio0D7zXG+s7IA+/XE/EEA/fa/wC8gMX1WOVpFi9qu2nhAD72rMgoF/fazuXcIFuvKxBAft3WalAb2rIdOw2nup27SZCN5UcPiKmtN9yjeVSI1cLXcfeVRuq0fEncv3lC68q+qndE7S/vlb98p3Re0P3qtJ8yd6b0tl3V/eTvO5RvKs7OKfiO5X3yr3cVOw7kN1UG8ytSaS+S3XdTutbjHa//09u3LG6l+afZga40xo4PKy3ChHI5XNtkUNRMHhbjFZHhmDoO5VrKnsIp+bdwUbgJcWtj4v8AJVqs6i4hn95EVVI078qUKpFsrEWsljYHm8y6RzoKrQd2o2GkS86H9+EGY0DTEIlU6AeIEIhLmAPFRgn1RYyKBM7tgHdVmmufo2/giT2x6bpeWuEB2yOjKpNDTDUQ1xcz4t1pyXp1O0zyEC4LXaTwNkDdI1tjhAY1AkDhTu0sh9MxuQtTJKKo1rm6iPL+YrVxuTO9G2tldXhDbamSzs5/YLrhxOWfJpsVlgNratFS5P3iqN9J7L3ceGrK8efLuaeqTADGN00wNmhex5gNDiUDW0j+6gcy2jdwAB3lAZdSpERufVAp1w/cN2bKAWvdIdKAtRJJPdAHJhSiFpjblTQEamkSgaN1oTV+VBHCQsgANJQHygoDeUFlxBgKwDUc3QSTClajx6mKtqXjLCgdVY/2pHZo3WZ7W+nqtPZogRv7ldGBtLkFrQvUgE7lBSCwYKlFOMlQQIAdPZADQ5ICWhWpAPKyLPCBbuEAHhBEFgwgjjIQC7hAGqdkCi3dBZ5QE3lBRdugEmZQI33hBQmTKCOEoKcIQCEEdugH8yzoC5qBTjGyCMMoKPKsFt5VEeYVgTUMhUf/1NuduYPC/NPswNSm4iWmR6LLpCWAh5lc2mTS1A8rcYp7HODplWsic4OJlRuFSQ7bkKtVk06sQiLeC/j9FKF0mEv0nywsRazabgBsF0jnQuEA+6NlBwBBjccIMulVlqJV1fPBhEJhzS/93bZFjJokuA2gnYIzS6hfUrBrD8HPuqk9rlwI1D6o6MoCWAs2KBjQX+Unzeq04oWw8EcoLfBPG/dATC3iNx3QGJBiYHMlWYbO7TNtbWtcnRQpOc7978q9GHC8+fI9+2wKhSAdfPD3dqI4BXr4+HTz5cr1Rpa0NpNDKbdg0bL0zB5889rDSRsNyuunn1dm06Tzudj6o6GspU6fxGUFuuGj4RCBTnzvPO6AKh4hAImN0BN22HCBo3CACIOyCxMH1V0JvBlNCMMGFAWkTMboKaTO6yKcdJJ7II1wcJAQTzeqCnSASOVYPGxXEHWtrVqOIDWgypWo0/Id+cVxnFb1xkMc1lM+0LM9rfTo3f5LowY0mEFwVoBuggQRBYG6lEgSVBI5QB7oKnZIIYhaAEDsgmwWRR3QCQgogQUC2kkboLCCn7HZBORugGAgB3KAN0BhALu5QC334QAAQ4gcIIRuZQD2QASSUEIEBBICBf5ldCEymglzDqLjweyxRBtuBCASrBBxKoh35VgQ9pOwVH//1dv8rhqB2X5nb7UgW1GulvHuo6SFbauZ91nSsime54CsZ0bILZHdU7VAOgnt6o1oIadU9kKzKTdUCN/VER7XtmPooAZq16Zlx5U0tPa0xI3WnOmPksiPN3CNk06U1ASfKdwgy2tA2P8ABGLfKF3oNgilOfrcY29VWmQzyNmfkoxVUKbhqqHYlKk9rB1PA7TuUb2yWCJA7LWhZDtncErWox2mQSQTz6LO00EU3HW/bnjusy3bXbNHWtrWuXAUGOc487cL1YcW3HLORslhltrfNfO1N50Dhe7i4JI8fJzWV7dFlOkzw6IDGN2AC9WOMjzZclpopOf3/huulu3K+TRQDRqedvVQWalNkBu5RrYHVnE7bBELc4kTKCwQeeUAlwn5IITq3QFAiZQUI7IGNcewQTcbnhAbY4V2KI80JsDp3lQESewQDMmFNAzTJHsmhQpxsJTQa2k4jYGU0KdZvcNyAPnug8PG8Ptn2lZlWXANJLex2SxdtH6Y+ELnGW02hrGVw1oHsFiezbpXuuiGN4QSD6rS6FA5naYPzRm1qWJdSsi4PfVsNxPHLa2xCg4trW73Q9pHb5rFy01J4bLb16F3b0rq2f4ltXaKlGoOHMcJBTHLZo5o7rdiKPJU0KJSwBPZQQ7hAJ9FdiDZNgdKguIEoAB1Sgh2lAtrYQXCAH7lBY4QUgAiUFHZBBwroC7cJoCPRNChtumhCJEpoARG31TQAt7poUTt8lBQMoBLYMq7AzO6bE2IWQD9thwmgBED5oK4AVEG/KsAuGyo/9bbdfhtLANl+ZfbhIY5zi5o2R0iBrgoVk0IcNDkQ/wwAAEWK1tgsJgjlFLb+G8dwiVneIPKAP0RC6jnRqBQAwuDg9FrJY4x5VHOhqPeBrHJ2KNrazU2e44KoyqJY5knkbFHO+x6B9CqsIdTDHSO6NnhodDJ3CjFSprYPLwlSewnywW/VGmXT3aDwfdaDXEuhrNyFyx2lyHSbUuHhlJpe/gRuP1Xrw47XG5vfsst1HxUxB2kchjf817sOCaeTLm8veoUKFr5bRnh7Qe5Xrw45Hly5ac2g47yQCZdK62acu7Y3eGzYCVFELiBDWwSgB7nRJM+yBQcdyR8kFh0g+qCgSRBQW0yZ/MEF6Z3QWBAQEAI3QUABwgNphAR8wQEG6d0Flp1auyCwAfb5oLawuMQgYLRxMwB8ygYKbae7iNuyCGtTB8rZQA64dEDYeiBJeTu5Zo8rFo+7VZ4LT/Ja+g590qANTG3d/vZ/gCuX8w6b6LqGNQWtNBfUbRY6s8wxo8Qk8eQGUcq/MbO+MDMOcMcxt4Dql/eV6mp0yNJI2/RZyxamT766IY2MwdLsu3hdNWlbC2qbzDqB0f5LEjUroAIA+a7UqjuSVEVypQEGVBUiYQU7lBJCCIIfhhApvl5QW5AAQTUEAHfhBcbQgqCgFADiOEFjhaFRI2QAAZlBRIAMoKkFu3dADiJQUePnwlAngrIEbDdBRIdwgGDugGCEEO4QUYI2QC6ABKASRGysAEn6Kj/19vYWhul3K/MvtwJc4bjgbI6QLXioYUQ9tN1OD+VBka2wC36osKcW7uPdFRlEVBqHKJWVQboEO3KICo3y6uw3QXRb+Y8IrIBaOOFHOqeadTy+irYaMtJ9EGXTADSQjnfa2nyyqsJnU+ewKNipteXPqjgGFGKfXqGGEjY8qVJ7RrRUALePUKxpl21JzzoY01HdhC6zG36M3Kfm2DD8uVqv414fCp+g5X0cenkeLLkbFbWNvatAtaAYf3iP4r1Y8UePPkZTmRvVfzyunpj35L102Hyb+5UuSXFfjveIOw4Vl2zrRBGknflVTAPKgrUTxygp2qDKARsCUEb5+NkBMG7kDG8II06SZ7oKEhAYagvTCBgYSNkB06VV8hA8UWNjxD+iCz93b/eQCLgT+GEEfXc74tvkgW6COUC+NggomUAnhSjzcWE21X/AAlX6DQOlYh2OH/tjly/mHTAJErqDagtaaap1Pxunl7p9mLFHHT4NlWp0XcEVKjdDf4lHKvzxwfA3YllzMeNluqphNO3qA8z94rhp+u66a8OW/L6s+yHjjbnJuL4G52p2G3fjUxMxTrjt7SvP9XfB9FnYAegA/gujVQd0RQ5UoHuoAHxII7lAKAkFOQAeEEPCAB3QAeUF8OhBaCIFHlBRb5kE9VoUO6CvVABE7IKiNkCnII8+VqUQbg/JZAIBOyCIKJEIAnsgp2yAC7iUEcQWhWBbtgqP//Q25zdJX5d9uLc1umVXSERBlqIy6TnOYQUDARogIKc0eHJ+iCrdxD4CDLl4Do7oFMe9wg8TugbbhpLg/hGTn040hvwoEvDmGPVVo2iZ8v5kGS0ODdz9FEq26dJMRuiFSHEtHdN6O3bJoMEeGVdbT0utTe4aGNLyO7VucFy8M/fSPZwnLl3WpivdjwLf90cwvZx9P2vNyc221W9naWLWiiwa/8A5ncr6OGOo+bn5rNZUbTEv3cujKVLrUPJsgxqlSo+N9wstRQl+x3Km1NAgQt4s0LhuHei0gp7oLDoKCj5ilFDYkLIJvlQMQW1BbviQGGl5ACB7KLyPQd5QMFKg3cvk9wgLxabBDGBx91YFOrvn0HoFQtz3OQLcgNrkFoLAO+6AQ6DBKCu5QB+dBgYmJtqvyKlGgdLBH7a/wDGuWZ7HS2crYIN3QWqVwz7VeN/s3pkMODtNXFrulSLfVlM6/5gLccq4z0dyk/GOjnUu88OalRlP7p7vt2F5/iFnD8VTP1B/ZGxr7jnm+wRztFPErItYD3qW51ALz8k+Z34/wAL7UmSYGw4911rERRpR7KwR3KoEd0oWW+ZZF/CgFBNKCxsECj8RQD2ClEU0LHKQQ8LQAO3QUOVoUdkC1zBD4VuBX5lQSAD3PogAb7oAIlBOEFIFuQUTtp9UC4jb0WaIeEFHsgF3CC3mA1WBdQyFR//0dxPmlfl324S57dMTuq6QNJ41Qd4RGYzc7bDuEBaA1pQRrwRpI+SC6TC3zjfeIQZQeY3G/qgx/FAcWhsSgfTaC3YIycHOcIAiEFVA5wnuFWg0RpOr83dBmRraXTuolTSSBv8wiFMp6bgFnnB/IF0w4+5nLPtbBY4DdXkVXjwKPqdivbhwPHyczYrbD7HD2/gM11O73DeV7uPjmN28WfJtkmvDNbzEd138OW6x7K88fVWIlsQ35zyptdsgPJc6TMIg3ExwgETMxztCmU8NPPxfMmA5eom5xjELeypyCDVqAE9o08rjjd1XpW9zRu7eldW1RtW2rtFSjVYZD2O3BXp0zTJlEThBZ+HblBTSZSiyJKyGNAKBhA7IKBMQBuO6COeyWgb1HcAIHsqspiAPN3Pugo1qjidyUAiZkoDLp27eisFKipQXAQCNkDG7oCgIALRMhBIQUQJ4QefiX9hU27FSjQelw/DxoxB++v3WZ7HSae255WwchBXv2VK+Rvti42K2M5dy813ktqL72u08TUcAyflBW45V1X7O+WWWXRu2oV6Yc/GxcXD2kfEy4MMn5Bc+PznTl/DHyr06vKmResmG06xI+44m+zuHHbyveaRB9lnmnlrhvh+hZkeX93ZZl3G9LB9VoTZWAXcqiD2QURvKyKgFADhB2QSUEkoFvHccoAgwmhFdCzsdk0KJMFQLHKC1oA4lBTRK5gjsNluBJBmVQQO26ACgEgNGyBYMlBcBALtkC3cwgGDMoBdIMrIEuHwoKdII3QRw2QCZI33VgWd1R//0twaYBPqvzD7cA6kCNSzt0gGNGrVESrEZQ23HCosHWdIJhAsmJnbSgdbudBjfvCDKLgA2e/ZAuu1hALRBQNpGAAqaHqPbZRkl1R5d6AJtO5kU5dHG6NMjemImdXAVktZtkZ9lguIXxADTSpHl7tl7OPppl9Xlz5+36Npw/A7DDA1xaKtwOXu4lfQ4ummH13t4+Tnuf0Z9StrO/HYDYBeiYyPPbsBcTsePVas3NM68vBzbfOsMJuajXQQ3S0/4lys00zMIBp2Nq106hTBd7ytSbR6TRzvueyoYSfVBbPjEnsYCZ35Vlfnv9p6rfUurGM2txc1X2xp29SjRLj4bWmk2YHEyvLhfK3w+g/sp9TW5oy0/JuJ1wcZwZv9Xc/Y1LbsPmN17p5m3OZbfQ7SBO/PHyWK6aHpLkl2xvyhBAVVC3T9eUoIQBKyCYUBh7ZQDXrCkx7zs0Db6coPKwW9fdsrXp3pPeW2/fYGJ/UK6HrAkyDzPKaDGg+qaaxktGA4xHEgH6q3HUc92y2fRruPZ4yvlfVTxjEGUblg81Dl4kSNl48ue430+58P+E83VcXfJ9dfR62F4pZ4xh9tiVjUFW2umeJTe2Yhd8OTueP4h0mXSdReG/Rmgd11eDhvdjbn4v0RFU5pmEBNJHKAgZQW4wgrndABJlS0YGJgii8Du0/yVsGg9LtQp41/4165z2OjgGF0Fx5S5BYPkA7u4CY+TLw+AvtD4u7MHWDFLemdbbV9LDaLPikhvt7uWrdOPt9x5PwluX8s4HgoGn7nZ0KJI2gtYDx9V58c7jlb+bvyYy4R8N9esNflrrDi1zRZ4NKtXo4lTI9akVHBv1Xqyw7puvPx3t8PujLGJMxzLmFYwxwIvbWjWMb7uYJ/ivJjl9Hor045XeRFt+KFkQiXH2V2K4KbFndNADsmgLt900ABJ7JoWmgJPITQFpkKiO7II4d1KB5lQABugtXYW5NigIU0LJ2WgPZABQD3QU8SIQKAhwQETCmxRE7psKIlyoklADpdspoLjb3TQHflNAxu1NAXHaFdBZb3Qf/T23cNkr8w+3Ait5SFiukVTcXu9lqekZsNa2RuFRbXtBDnCO2yBdRnJ9UDrdmlm3KDIe0aAR8Q7KjH8YEEEbgKUZABGgxsVz2U+B6LWnO1jvb59PDud0xxtrds09TDMMvr0htNmmn3e4QI9l7uPi28mfJptmHYFZ2YFSufvFX0PAXvw4Jp4s+a7emaxI0xFMcNG0Bd5hpxuey5B77ei6RgPBlUNEOarBpvUdr3Zcu3M+Jjdf0aQSueQ9vL9229wewu2birRYCfotYj1WzqnsoHQgEQHA9wVnP0R8Kfa1s2W3Vmjc1BNO6s6FYz7O8OPlAXLCeVyaZYX+J9GepFhito533VgoXVMcCvaXLWuI94kwvVbq6c8Y/RTA8Wscw4VZ43hzxUs7+m2tTe0y3ziSPoVbG9vTa4wPdc57TQgZMLSrLZG6UWGCFkCOUAhp1e6DzMyXL7TCbuuz/o6NR4/wBn/igxskPZUy3h9RvmL6YdvxqJJK1BsTfU8nlA0GE+rWOO8hQIgjeQQfSFrL059PyTHKyvkXr9H/KNd6t/waJ292gRC+Tye3+oPsL03Dl0HdZ9fyjpH2ds2OvMMu8q3jya1i7xbME80jyPoey7cd0/Df8AUL4XOPn/ALRjPFdzHA9xIXtx8v4xzzv7e36VXsq0gLpkoLO/CCCZQEQSEFxsgCRMLNGHiQ1UnRxBldBoXTLTpxlo5++1P4LhPxDovDV1FcsIQKurhljbXF7UMNtqRqmeNLGlx/gEwM35jY1jF1f5svcwUn/1utfVrim/SSS7xCWGN+BCuTjG6N6+dYAA1uO3OgCGRTBAjbaWLOWPiVcsvo03Nmacx5uvxiuZrupd35p+CK9Rmk6QNhwBsuu/lTD2+4vs4Y4Mb6WYW0vDq2HuqWdXff8ADMtn6ELw4e3oydaI3PtyvZGQt2euQnclUV3lILWgtyCjwgAQOUFkgSeyAOSYQU0RIKCnEbICO4lSgBsSFAB2KC0AESgvYBaC3bkwgocQUAkGUAnaEEkEoFuEGUoAlZBE7IBEEytCiAEAEDdAojsgCCgL8vyQBugh90H/1NukAauQvzD7cKeWVNwI9ViukMpDYH8vZaiMtoDmbcKgS4M5QKr1DqkcFA+kX+GNPM/wQZEeWZ8w4QJBZUdB+JKM+lpeBLvhTDHbFZdtY3F47wrZmp54cR5f1XrnHXG2Niw/LVrZAVsQPjVz+XsCvXjw6ePLme2KjGNFOi0MY3YNHYL3YYR5Ms6AAk889l0vhmTc2Zp2UnljLwWW+ia0YXaBp7o2JjoVHmZgs24hhVzbuH9oxzf1C5ZDUelmIVauDVMOrH8TD61S1M8jQ7ZbxHQ2qA9Y9UFgSZWMvSx8XfbRsSM35fvBv49i+h8g15d/mnF7Mk6gZJfnToNlHqJhrPFxHBLM2+IQJc61pPLA73LSP4rpzfjn7M4emyfZF6mtfRr9N8WqkOaDc4SXO4b+amCeTJ4Xea0x52+tADOrsQI/zXD6uv0ENjKqLLilBNOyyB7oJ+coPJzDS+8YbdW//wAyk9v6tQa90suzVyvRt3GX2lR9I+2lxWoN9buB7bIDiCEb4stZwXotZenHPi3lbHyb1v0f8qs1RqohtqajfVu0r5Wft/pv7E8X/wBo3v1v/s8uyurjpp1NNxJFvSrtqPHAdbXUOb8xDlJ4r2dRwY/FPhd+tk/rvb7Btbqld0qV1bkG2rN8agRv5agX0eK7j/M3U8d6bqM+OmjldHl0t3m42URbWoLIjdBUlBYJhAJHmCzkMS//ALF/yK6DQ+mggYyf+3Vf5hcJ+IdBnZdQTIQaP1jxv+j/AExzJiQMFtq+jTPfVXOgR+qYGb5Q+yxle2x/qDVvL+i27scMsqlVzXNDm+LUIptkHvuSrk5R9pf0Xy7+bCbOR/8AYYFnK/LCxxT7UGTsMd01di2HYfRtq+G3NOs+pb02scadQ6HAx6Qu3HNxi3TXPsd4z4lnmHLdVxdUpVGXTG9x4nldt82rzZTWTpx3cfUUmI+e/wBV1joqN5XMQ8KgUFrQByAXcIAPIQR3CAeBKC4/igpzeEFeyUCeVkC7lBEAoIeFoL7oKQTaJQLcgocoBqtLhASgAwhpWRDwgpq0I7hADuECjygEoJ2KAEEcJQf/1dwGnRpX5d9uMdw0gqukOtnNjzfREZIa8bg+X0VFOe30hQA7Q5p9RwgyKLnaQO6B/wALSfzIMNwNN5cQAD3Us3NG9PXwK0bi10G03EUGfGfkvb0vDY83Ny7b7RFOhS8K3phjWr7Pb4fKyySo9z2A+iOa2QYJ5QGQ0eYkNDdy8mAB3TLPGY6rUx5M/lwlqUrihWbNBzX0z8Lmu1ApxXU3Hf8As+Wtck1+4odJ3WZy3O3f0eOYTG2RY9ytNrgDYKUKrfiMc3nbhcrBy7A6n7B6g4jhjjptcUb95o+9Tuu+F8DqjXaqbdKoprYQPY7aFr6NZ3xHyb9tWzJp5UxAD4XXFJx/9IK8mHjMv4XRPsw+BjvRW3wi9ptr23iXNm+m74Sx3mcD9HL3dRd2fs83D6r5Vz3lvGOiPVPThxfSp2ldt9hFxw19s9xcBPqNwV5Z7emvvvIGcrHPmVcOzLYOaWXlIeNSHNOs3Z4PzO69H0cZ7bPEHSs10WRG65itYG0KgmbjzIIA0u1IEX1NtSkRHM/xCDnPTh/3HGsfwOpsGVzWYP8AvIUHUIhxg7LpAcxyVnL3GosO3Vy9M4f3dfJPXolvUi4qN+JtGg4fQBfN/mf6Z+wuPd8Es/zf/wCWz9c8rB+CZdzhatJm1o2d+4ehY11N5+UkJy47j4n2O+LTi5+Xg353O3/lvfQjNn7dysMIuKgdiGFO8Pfl1LsV26W6j8d9v/g16XnnNJ8uV39fbq69+38xnL3RPzKM4zsuxrLe9pv34QSA06hwgiCLOQwr4aqLx6groNF6bD8PF3f9urD9CFwn4hv35V1EZsQEHxN9oXqxmzEsfx7p5WqUqeAWd2xtJjW+d7QwO3PzK1cXLbSOjvUbMeRMeZZ4F4TaeNXFtRu61VkuLfEDSB+q53E2/RFu7Wn1aCZ5kiVvHw678Plr7U/UXMWDXv8AQW0dSbgOMWIfdh7NT51x5fThdJXDKPnjI/UDMnTzEK2LZarClc16Rp1vEbMtBndc8/bXHNR+h2ScWu8dyjg2M35BvL21p16+nZut4kwFz06vf7LUgo8KgUFIKcgWgA/Egj+UEQRvKCz8SAT8SATygB/dAPJbqQRyCkFEQZQA4yUFflKAeyBYQW/hKIeFkAgnZaC0FF2yABuUAnlBSAe6CRKD/9bbQ5ujjf1X5d9uFEF20qukE1kQPREZjXENBLvoqCc0bO5CgQXAO2EygzKQdGqIAQVWqtjcwg17GMXFlbOdq1PJhv1XTim85P1Yzusa6PkyydZ4NbPJ1Va7NdVx5l24X6LHjmL4uee2zDdu+88rptxk2mqIb29FyUQ34EION/aAzBmLAsNw+lhNw62tL1z6VzVYYdxsF+c+I8ueHJbPWn9O+wfQ8PVdTZySX92mdDOpdTDcVOWMbuDUtb139Ur1XToqkRvPqvZ8O6uZzVfc+1v2buPJvhnj9I+nTw3eYEE+vuPYr7WeMnmfV/Fs8bhyZY33FgBYQPdSicccpIOW9RKIwvFsGzHSEfdrjRVj9x5iPlusW6o6bY1mVrdtWn8Dw1zT7FdhkaZPKAmgxAVxTO+Hzb9sezNXJGCXRE+Dflpd6CoyI+sLyXxk6zziyvse3nidN8TtHu8lriDi8khoa1zWmZK9WeW3m43mfazvMg41l2gf2pQdm/Dquu2oW7g91Sk/Z7SRwODC5R3rSPsn9TRgGP1MlYnX0YZi7g6y1ny0rlhgN9tYXo+jjPb7d3kzIMmPX3n29Fmugi4njaOVzAkklUG3+9ugjgGoAfJpu+UhBy9jhhPU9u+lmJ2+47a28KDqbeASZPc+66QOERws5e4sTgk9lcvSYf3dfJPX0lvUO6Prb0Y/2V8vL8T/AE99gLr4Nf8A+3/Z9G3GXaGaen1HA7kDTdYbRZSLvy1TSaWn/ahezLHeL+EdN1ufR/FLnvxx5f6vmzpjj1fIee2W15NO3dVdY4jTdto0nRq+cwfkvHx5ayf3v7TdPh8W+G92Hmyd35/R9fh0t1NgsOnS4d2PEgr6Eyf5e6jgvDktdovLN4bEoxPSauAVFE74NkAt4QQrNGLff2LiOYXQaN02bFLGfQYhWj+C4T8Q3z8q6iEbbcqjU8T6W9PcXva+J4pl20u8QujquLmqwl73bCSZ9lu1jsJtekvTazr07mhliyp16Tg+lUbTMtc06gRv2IlYuR2N0AAAA4GwlY208DHcjZQzLdNvsewi2v7xjPDZVrs1uaznSDPC6SpY8kdIumnmnLFgQ74vwjv/ABVqyabdZ2dnh9rSsrKk2haUGinSosENYxuwACzpTT6BBDwoBdsQgE87IIgFBUDfZAt3KCjwgscIBJMoIgooAIMoKcEAoKgoLPwlBjknVygITCCigoAeiCilEd8KyEtJJ3QW7ZaAOQASEFbRsgE/DPeUFDhAtxjdBWsnhB//19opjWOYX5d93S3MIIAVU6m0AeY7hA9pYRJ4QWTqBHA7IEfC7bdBmU6w0R3QYdyWhrjM7cIOV50xV1GrQYTpaazZ9hK7dP8A3mP7xy5fwX9n0xgpYcNtjTIg0qRA7EFoX6jOPg729RhEQV5bWpdLMdlUMYQRxCDk/wBoTCjf5DfdN3fYXFOqHDkA7FfE+KeMfT+gfYjmuHX4zeu7w+W7ewuXYbUx20nRZ1WsuHt+JmoDS4R7r4nHMuLHuxf3zqOp4/v5w5yWZT3fb6u6M9Q6eb8HGFX1Qft3Dg2i8E/2lMDZwn25X6zpef73CXL2/wA//a34D/YuuyuNtwz8/R1HVt/L3916ub5J48vwWcy7tYzcQAGSVMPmw3WstS+F6SRLVraNR6gYZ+1MBu6LW+drdTI3Opolcssd+RXTrGBi2WbInarTaaNX1D6ZghdMbsba0PDv7vcrV8B49W7jumDNm3DPtaWv3jpLWrNEut723qD2EkFePP8AE7S+NPi3L2Ys5WdpXy7li7u2UL+o11xaWgcTUPH5eJXt7dxy1MXTcmfZm6mZwcL3FKP7Hs6nmFa/JfcHffbt9Vi46O54XVbpfi3RjNVhSt7h9zZVmsu8OxEDSXVmHcSO7TwtTJmPt3ox1AodR8i2GM06gfiNFgtsSbxoq09vNPqt2NbdCcWu3aYHpBkrhfYobLSjG6CSroR3wmAmhy3qHTNhjWBYw0aSy6ZTc7+68wZWR06iQ9jXg+V28/NblDwRtG6XzWoMmWkLWU8OeN/h18l9fjPUSu3b/m1H+Uf5L5Wcnc/0x9hOSY/BbcvE3l/2fT2B3tpbZbwqtd1mUKRsrf43gGfCaF6pyyzT+Ddf03JzdVl91N2X/X93zD1vGAvzg/E8BvaVd180VbxlCIZXZ5Z+vdePLGS7f6C+xHF1XJ0WfD1OHbNWS73fLvvSXNbc0ZLs6z3h19ZAW9y2ZJLdhK9XHlt/Fftd8KvSdTcPpG9t3Xr3p+Kly322fL+a0WqI3RBAyIKAWkgx2QEQVLBi3kmi/wCSto0jpwYZjLfXEKy5z2N8DZELoKPMILIBStbVHuVntNpqA8pIDjOgesBSzTnvyk9+xVlbggV13oqnFTaB43UtFd1BHtmEA8bIKQURBI9EAz/FADvVAI3CCA7KUSE2BTYtu+5VAkoFkkoIdkF8BADnbFAqO6Ci5BDwgqYEoBJQQmRCmgGnSU0BcdpVCyZQAQgqYEIJ2hBUCEAOAVFQAU0P/9DanDQfLwvzD7wHVSXBBkU3BxAdx3QOa1uoj8vKCwGOJAMRsgV4YNSATKB+hzRpj3lB5t8XBpEwD3Qcb6j0ag012klrTq+o3XXhus8b+rnyTeN/Z9CdKMwMx/KlhcPM1abRSqj00iAv0sy2+HlNOggNiRwueUZnlGmTCyDB0kgqjW+oWF/tjJWNWTRJdbPcAf32CQvl/FMO7hfb+AdReLr+P94+buidtZYriuK5RxJoNtjFm5gns9hMEe4Xk+Hcc5eOy+4/tn2wufSzi58fyk/328BzMe6TZ4JYXU6thUkTxVtye/rIXh4+W8XLcX1Ozi+O9D3T8WM/b/1fYOV8yWWbMEt8asHB1Ou0F7BzSf3ZC/WcOc5MX+e/inBeh5bx2PaAiWuHz9vmrfF0+Nj4nn6rZIB9CjTFvKAqUHsInUDP6KDm3Tys3CMxY5gNX4KVYV6DO0VRv/FTjHVGEnb1XTL0CnQIHHdMEct+0daC56OZiIbq8Cm2sB38jhJH6ryZfidI4X9i8WdXMOY7e5oU6tx93p1KLntDnM85kgkGF9PCfK8/JfL7LLg7Yk6dWzh+6DtPruvPyeG8HzX9r7MWAU8t2OWMRsKlbGrtwvcMvW7MtzTdDwHep9F5JfLeU8PlDLebc34NTucNyxfXNt+0XA1qFpqcXvB2MNHJXtwYfpD04xPEcWyPgt1jNCrbYo60osuqVfaoalIRJHofVMorZuCuaja4ILgrQIQWkIOf9U7I18u16rBNW0i4Z7FhC532Nmy9fi+wWyugZFSkwk++kLcHstA7KtQztC3l6csf7uvkzr+C3qHXcRP9VokH03O6+Pyzdf6h/wCnX/8AFT92t2WHdQ82CnRtaF/e2rNLKZbqbRa0cb+i1jxWvq9V1vw3oeS5Wef8uNbdbfZ8zy+yrXt5Ut7V7aLqjaLXa6j3N8waT2lZy4LI/PdR9v8AocObDHCZecpPwa/5ToVmirlvNr8Evz4VnijhReyptorjhOPLTX21+H49Z0d6nD3Jt9XNG5HeYj5iQvpYZbj/ADV97bxWfUQ34WzH0sN7IqaSHT2QCRIJCBjRI90GLeHTTeO8LNGj9O2gNxk9/wBoVlJ7G+N9PRbATJQWdkE549CfoO6DjHVfqzQyZn7JuCGqG29Wv4mLHsyhWHhtJ995hWzwx9XZnOD/ADtHlefLG433Eey4ukWCF13sqiURRQVCAj2QCRKADsgo7k+6AYKAHDsgHgboINlKJIUA8pBbRsVoKJg7oL2QCd0Ak9kAuOyAQdkAEbhBZOwQVBhAJCAZhBTj3QCdxCBSATygjhCCgJ4QUfRAsmDurBNQJVH/0dqO6/MPvE6makGTSaTB/J2QPiD5eEFOY7ttKgVTqubU0xJCDMNRzmzG6ow72nrp7gRG8IOb52wvx7JxaNUA7K43ViZehdA8ymwxm7y1dP0065mgCdp9Avv8GW3xeeWPpuk6QvVnHnwFoIMrko9QG3dAu5pfebavQcJp1ab6RH+MQvPnj37xvrT08V+6yw5J7lfHGULp+WOqds150tt799s/tFN7y1fA+HcnZ1Fxnq7f6J+J5T4j8FmX17Z/2fQfWnp2M44E7EbFjf25YNL6VYDerT5IPyAX0ev6aTLun1fyr7K/Hc/h/LeO/hz1/wDPLivRjP8AcZIxo4Riji3B72p4dem6ZpVZ534Xn6Dnsz1fD+kfaj7P8fW9N/asPNk34v8AxH11TeK1Flam7VTqDXTe3cPYeN197Ky3b/P+WF87mrBjjZHGeUc1zhupSXbkuYGHAc/4Zi42oX02tYcDU7YEqcauqUXeSfTZdMg4yWApgjkn2h875ZwDIGMZbxe4c3FMes30sPtmidZBBJntwvJlPmdI+IsidRsydOr28xLLVRltfXtA2zi/zAAkH/JfQl1HPObr9FummZ35xyNgWYa//Obq2YbkFun8Vkh2x9Vyy8szw8rqb0my91TpYXRx2pVotwys6troHS57HiNDjyB8lzmHld7eplLpjkbJdOlRy9g1vbvaI+9aA+s+eSS7+ZXeeINvgBjdgHCWgDmPdcbfKqAlVRtCCA7laBN+FB42ZbNt5hV1QI+Om5rp/vAhYo1vpZeePlxlnUk1LOpUt3essP8AwVg34RILeCtfVqGRsFvL0442fd18mfaD0/8AKFVDx/8AC0oB9RK+XldZP9N/YDHDP4PZl68/9n09lLQzLODhsU2i0o/C0NJc6mD2Xs484/gfxSXHrc8rvUyeq+rSoUalapAbTBe97pd5WiTsumdllfKw5b1PLJrWvW3xhn2/whme77FcrXJqYe+q25Y+mNOiqXSQ2Y7r5eU16f6r+z/Dn1Pw6cPJLPH7O7dNOrt5nPG2YDf0W0S21D6dYRqe6ntv811wyr+T/av7MYdBxZZY2X+rrwcSZiJ7L6EfyZYO6ApkIKHBQW0xCDGvh+G4rNGkdPPgxj/zCt/kpPY3scn5LYoN3QU7cSOByUHl5jx7Dss4He4/i9TwMPsaRqV3TBIZu1gnkuKo/N7PGbr/ADzmrEcy4iSal7WLqVGdqdGfK0f4RwulnysfV9l/Zy6msztlBuA4jX15hwRrab3uPmq0W7MfHJ25XjrcdoBGo9t9/muuN8LU7rSIghQR3CASSgW4lBEEQCeUAOQUVKBKgpWCwfKVQsiUEQCgFAB3JQSIQUR3QASgInZAskoFkmUBP+FACAEAH4kBOGyACdIlAIO0lAt6sAjlUf/S2jVDi1fmH6Alh85BHdBn040GOEZppcWtBHKoEl53PC55ARSl2tv1VgyQzyzyfRaCKpBBbHKDwMXw8XFB7InY7KUcTu3XWUs02uKUPK2jXl3yJ3X0uiy08XPNvsPAMWpYvhVtiFuQ9tam10r7OXzR8jKPb1tcPdclV+cQimNnnkyIHsNys3KyVqZdstn5PjTqlYHA+qF/XHkBuGXTPdrod/kvy3SyYdR5/N/pD7P8ePU/CMb9e3X+z66s8Ws3YHZ4reV6dK2qUKdV1R7gB5mDn1lfpOa4Yy2fV/A8Oj5+Tlyw7bvC/L4fI3V24y1d5wr3OVK4fQq73haPIK/932X4/l6nLvr/AED9ksOrvDeLnlk19Zp2PoP1KditkcpYvX/1lZj+qVHba2AcD5L9V0ueOfF+r+a/bL4DydLzXPjny/pHcGy0+b6gfvL34Tw/m/T4YyXu9iI1KZYvJ00/iZf1c26q2JqYN+0KAJuLCq24b/8A41iOzbsuX7MTwizv2u1Mr0m1T/igT/Fbg9xvC1fQ5L1v6Njq3aYRSt7xtjeYZWdUqVy3W427xpLABuJK54jEyP8AZk6c5RfTqX1q7HMRogF1S7jQDHZvoF6NueXt2O2tLeyoU7e0pMoWzNqdOk0MpgDsAFi1ZD4mFithPkdCgaDMFASCII4t8qBm/qgxr1gfTcxwlrm6SfmrfQ5z0/qOscx5gwlx0htUXNNv+PYrnB1BpEQPy+X6hdLPMWTV7vyIv7pljY3F5VBfToU3VHMby4MEmFrLLUdOl6XHn5ph/ifGPU3N1rnbNNXGLKi6jbmiyiGVD5vKSeV8vK92T/WH2b+Gcfw/o5xbnnz/AKuvdCuoOOY9jFTAsXrGpYULNrbNrWEBtSkY3d32XbCP5d9tfhXS8e+Tis7/ANPbvVVgq030yJY9pa4HiCIXqs+Wv5B0HJleSZcvvC7m3CLH7Nlu+5rVcYxZ7bbU6rTpWrNLtLnE6dT5HdeXDj0/rfJ/1C6jp+HWNu/6f+jpeVemeUco3NO8wqznE2N8MXNV58UD5L0TB+J+Lfa3qviXFccrf9m5b9+e66vyk3rypFEOEEQQoF3f9kfks0aP094xj/zCt/kpPY3kcLYgEkDie6AYc4jYRJ3JhsM/M5VLXxX9pnq9/SfFv6GYHXJy/hNQm9ewyLm8/Mz3a0cLcYtfO5cOy1WdtnyFnXFch5ks8xYRUIqUXBlaj+WpTO7mn2hefKOkr9FMm5rwrO2XrTMmC1W1rW7YHOpk+enUGzmu+RWJGnvkEeXkRP6rcC+60LdygM8IAdygW5BEFjgoA7lAvuUFxIQA4EcIBBMboLQUOSgEcoKcgWgpBRQUfhKAEFdigh4CBZ5QRADnIB1IBIbs4rIjvbhBT4jflWBO/f6Kibd0FANlB//T2Y/Dq/N6r8w/QAY+HAO790Gcxp0jTu1GaNxJHpCoBxqaCJJXPIHRc5ukO77FWB29N0djutBdcgiRx3QYlRrSwmfmpRyzqNgjbilUr0278/VduDPVcs8dxvXQDNBvcEfgd06bmxcGNBO+kei/RcF7o+PzY6dtYwl0zt6KX24n7CCopoc0eYGDCZ67GsJu3b5a+0nYOoZqsMQaPLd2+gv9Sxx2P0X5Lr8Lx5bj+4/YLrcuTp8+P6Y5a/2aFSxnO+bqNpl+2qXN/b0AKDLWkSGRsBJHAhWXl5Mcfen6zm6Loek5fvMu2Wfo6zk77ONWs2lfZuuRQZp3s7c7tJ41O7r6uHw3Hs3l7fjPi/284+HlmPDJqflb/wCjm+bMv410szqDbvc0UqnjWFyCYfSmdJPy5Xy8eXLh5dfR+26Lq+D4z0ustXKz930hlvrLlLFMHw27vb1tHErt7LevafmFc7A/Jfe4epmT+GfE/sz1PH1OWWMvbv8AT/1dM8w+QIBI/dIkFfQ3uPyueH3edn1eLmKyZfYddWtQAivTcIXKOTTOk18/9lXGCVTNXDazreHc6CSQtwdJa6e6su6HNlw0Hdq1ZoEGhhWZU0kw4OO4Jg+30UtakazmjqRknJJptzHi9CzuHNllu46nkT8RA4+SxFpmXM9ZRzowVMt4tRviz42MMP39j6LTLZafwlp2IMb8oCe7QPVBTX6uyC3AEwAgMb7fRAqvxpJ27q30OZH/AFR1Mo1I8mJ2zmH0L6e4lc4OoUnEtEcnzEe5Xarb8lgq9NtahVoVTppVmOpPhuqWvEFcsptOkzz4ubHk+mN25hhPQTIuHVXVLulWv6pcXaax1U2gmY07cLjjxau39G6v7b80kywy9Tt9/wDs6JhmB4LgtPwMLsaNmxohgpMDTvzuu8xfiuo+MdR1GfdnbZ+70mkhq6aeDkznJZcfGltfBkcrOmMMsebPtyXPO6u3TeHHn2yQQ4CM32EyD7Igp39kBd0Ak+aECrp34ZWaNL6fcYuf/wC4VlJ7G8TtC2KBgIOM/aP6kPyHkz7lh1bTjeO6ra3LTD2UiBrcPSJRztfBb3u1ay4ufMlx3JPM/qukc7S5b2CqbWDyOx5WdOkrvP2ZOpdXKma2ZXv6h/YWOO8NocfLSuohsDtqGxXLKarpH2+Zjb4BsPVIqCFoRx2QWDtugGp6hAv57oLkIBnlBR4QKcgtpKCnIBbwgs8IBQCgFAMcoKA5QUQgDsgEcoI6AgEmRCAIMhBHIFwCUFOAHCBdQEgQs0TtCCnAkqwUeFQpyChyg//U2WfLDtl+YfoFU6YnczPCDMouLfL/AARk10gzGyACdpCxkI0uPI/RWB7jIA7jb3WhTqctIHJQIa0HyaeOZUHh5iw5tzZVG6ZkLUknkk3HKMk4q/JvUCiaxNO0u3eE8dvNwV9noefd0+X1GD69tKwr0mPadnN1Ar6GeOnz8busuAWCDvC4bdrijYILTyRCtwuTPLqcfy/icO+0thPiZfw3FQJNrX8Mn2qCJXwPiWVysxs9P6r/ANOOXLix5MLPOV2Z9mk2NfLd/TFBn7Qo3Xnqx5ywt2/RfV6TLH7uY6nh4ft3n1PF1GHdbjjnv1fyd0BhzdxJMTHl+RC9fJ+Hw/mPNycd1jvd/N81/aRxa/fi9hgVzZtZYU2eJa3TGk1HuPIX5fqJcrrT+9/Yri4eDimWWeTUsi9Hs4Zpq0bz7v8As3DGlr2X1caXugz5W8g+5Xt6TpfO919r459o+HiwuGOGGX62eX2DhtF9nZW9nXrGvUoUm06lb94tESvuSaf526nk7ua5z6irsD2kHeQR+qmU046cmwoOwDqbeWerTb4rS8Wm08a2+ikyNOsU9wDxPZdNaQ+S3gpaLBMRM91iLHhZ2zKzKOUcYzI+NWG2tStQB48aIZ/+RCzVfmTj2P4nmfFrrHMXrvuL27qOq1PFcSBq3AAPATDzVvll5TzVjOTsbtscwi5qUKtCoH1GNcdD6YPmaW8brvMI45XT9MsoZio5syzhOY6OzMQtmVt+BU0jUB9ZWLjpZdvcJJid1hoewEhBQPda0GTIk7JoA5oezYSQVRzPqEDY4rgmMN8otrpjXO9nbFc9DpFpUFSi2o07u3+h4W5dk8XbI1Egg9xCsdvvfkuOp5HJM6fLJk++0Le9+Hj4eCY49n4t3flZENBDYn9VzuWnqxmcz7csZ2/mIFsxMiOwSZbcZMvvL2TxPKVatG2pOr13tp0GCXVnkNb/ABUzunbp+Dk5ebxPNeTZ5py5iFwbWxxO3r15gMbUaCf1XHvr6XV/Buq4f4vbv969qOw57dx+q743b43Pc+PKSz2HUHGO3YrdjtnjMZKgmduFlMpqjcS2EZCRLlYEXW1N36qWDUcgMAp4vvziFb/JYnsboBBhbEJ3QfD/ANrbE6131Jo2NQ/1ewsKbKY7Ne95JcP8XH0W9OLgLhEhVnQQxztm7EnS2e55P8FmZeW+yL92/CeJ9F20zfDLw+6qWN5QvaJIr29SnWpOby19N2oFefP26YXw/T/Abp1/gmHXr/iuLahVM8y+m1xP1JWW2eQrKKOwlUXOyCnGAgW47SgnZAMoKc4+iBZ35QWDCCnGUFMQEQIQB6oBQCgiCcIFuMoB4CAJ3QU7dAOw3QQzsRwgF3KBPiN1RO6CzuCgGdlNCoTQsQAqFuQKdPZUSO/dND//1djd5hC/MP0AqTQ8jcyNkGY0Rv8AmCMr8V5JbGw3JQKe52mQNljIPtnGOFYGloa7UTC0Jq1EaT+qA5a9u4iOSO6lS+mJeUNVBw5lTfhJXD+ouFvo1WYhRH4rHa59C3hdel5O2uHJht9A9KcyHMeVLG7c6bim3wqo92iF+k7tx8jt1XQmEnkR7KdpsyAHSrx8ms9JhhrK5VzDrzc4U/IF9Z3tenTvXFrrWm4w5zgewXwfiuc7vD9t9kfvs+q+T0+bcpZ/zDky2vmYJVFL71pLyRv5D2/VeHpOa+n91+LfC+HruD+N4vHvXqe32bkzGDmLLeHYtVYWVa1BviNcILngbmPmv03Fe6eX+Zfinw7j4+ezG/X82TiOXsFxe6tbvFbKnd3FmD4D6vmDJ9lM+nl8nH8S5eKfd4vWYGtZpY0NaPKANob9F14+PteDqOp5eS+apwLg0nkcgbBXJywxv1E4eVW+XRyTqdQdhuJ4RmGhIfa3Apvd/wDbcVy9K6fYXNO5s6Nwwy2owEevC7z0wyzJAI4PCxQQBkBZajSusmB3WYemOY8KsmF91UtvEAHOmkQ8/wAlmj80IqNP4gio0lr52IIMQR6pj7UTHOe9tNgLqrjpY0CSXcAfVeqVwz9v0KyNmnK3S3pvlnBc44vRscQFq2q61qu/FY6r5w3SPYrnkuLouWs4ZYzjQdXy1idDEWU/7VtF0uZ8wYKzpt7+lsD3/wA00A427qg2TEFBBADgO5QaN1NsRcZbungeegRXb/6N1ij3sp3gxDAcNugZFW3Y6e/CuI9wkE7LQY2QJbue31Ut03hhc7qOX52635fyhiNTCbaicTxCntcaT+Gw77avaN15c8n7/wCC/Yzqut4u7LWr+uv+C8jdcsIzZibMHvbb9nXtbe3IdLHHsCfUqzOOv2l+xXP8O4JyYas3587/AOHOOvmdsVuczVcqWtZ9DDbBrTWbTdp1veJ3hceTkfvfsD8E4ubgnLyTz/8AP0cftL+7srllzaV30bhhlr2mCuXc/q3J0PByZdmU8f0fX/RvOtbN2VWVLwh2IWD/AAa/qQBAP1Xu48n+Y/tn8KnTdRe313eP2b+71HC9L+f8uW5DAZEt4WW6oku5RFE7ytQY10ZpO+R/kpRqnT/+zxb/AMwr/wCSx9RuhPJWgBJ7IPk77W3T/Ea2K4fnzDbepcWb7dtpfMpt1aHUyS1xA7eY7re3PT5etbG7vLina2dF9zcVXilTp0ml7nOdtwN4TZp2DOmSLbIL+meD4vb024rXDrzG2ES4ePcNIY72DdlNfU217rJ07xPI+csRpus3U8Evq5ucLrsaTR8GsNbWSNgWzBWu5Hh5ByVi+dcyWWCYfbVHtq1qf3uuAdFKg10uLncDZc8vbWL9JrO1o2Npb2NAzStKTLem71ZSaGg/wWWzirALjsqLPCAXnZAB3EIJMDdAIMoLI7d0AERugqO6CiNkFAGEElBXqgqEAHYwgg3QRw2QLI3QKe7eEAiZj1QXE7eiAdu/CAidtkC3coE+C3Vq7oL4G6AT8KARxugouQCgGN9+FYJCo//W2J7HBfmH6AFPxA/ynYoPQpTqlyMmVIJDQInugVUIp7cwsZBlu6dwrAw1tT/DLdud1oDUpiZa6D6dlA2iC5hb+ZEvpVSQ3SQump2uUaRm/Cm3VhWBbJI22Xn45qu+M28noTjxwnH73K14dNG6M2wJiHD0X6XhymXp8Xmx1a+mWSQJ3Pcr13w8kvk0BpIBXPHj3ltvK7mnKus3TXEM9U8NrYK4Mv7WpoqOqfB4LvbuQV8TrenvJm/efZP41h8Ptt/4/wCV5G6EZcyy5l7jIGLYq2NWv+yafYL28PRY8eM06fGvthy9dyXHDeMx/OTz+zq9GjSt2tpUA1lGmNLKbBpa0egC9eM16fzzludy7rRggugnjlXvu9NzKSdw57DdLk195sTYPzWpNudzQeVu+6Yt7ah1Cwj9qZdvLaB4pp6qbvRzd1jk8E8sLpji7MUyzamoZr2s0Ks86mbFdMfUZrfKZ1NCzRNU+UfqsxqFXt1ZWVlWusRrU6FnRE1qtYhrA0bmSduBws0fFXVPKvQzMuLYli+Us108OxJ/iV69kaZNvUqQTLHAQN9lMVcGy3jX9HsdsMbZb0r1+H1RWp0KomlVfTPlJH8V6Z6cMzcxY9imZ8Wuccxi7fd4jfPqVSCfIwaoLGA8aRELn9Vxex09z1i+QszWePYXcvp06T2NuaIMU6tLUNepo7wummn6c4ff08UsbfEKMeDe0qdwwegqU9QCaU7usBgIhBB3QeRmO0F5hV1QcJFSm5sfMQsUap0nu6rsvusax/Ew+4q25B5DQ7yyriOhQIBPqtDDx2//AGZgeIYgw6X21vVqNPu1hI/iufJ6ezoumvUc+HHLJvKe/wB3wddXVa6e+7rvL7i5e6rVedy5zzqP8183K7r/AGfxz+y9Nhhj+Xsdpc17W5o3lF2mrb1G1aLgYMsMq4106zp/7R0+WOfmWPazpmNmbMx1sdpsNH7xSpNLD3qU2QSVrO7vh8f4J8NvS4SStfHmKYx97lk5fE9voX7MjqofmJg+D+rkE/CC7UD/ACXXjvl/EP8Aqbx48ePHN+dPoNw7Be+en8Fx9TaNJAhHbKaolGVHhagxrkHwnfL/ACUo1TIBili//mFf/JYnsbmDOy0LgSgXc0aNzTfSrsZVo1PLUpVW6qbm/IoPFw3J2UsIvDiGEYHY2d5ufvFO3Y2qe53hB8SfaNzXSzD1SvPuNYVLbB2ssqNTVM1aZ1VIPpqXS6043e31/kbEsE6jZAwTE8QoUMRp3Fqxt3RrtFVorMGl4IPEELz+dtyNlwrAMCwOmaOCYZa2DD8TLWk2kXD5jst1t6AM957Tx9PooIVYBdwqLPCAHcIBQU7hBQCCygp3CARwUEPAQUEC+6C0EQLdygjeUFu4QLPKBW2pBWmHgoK9UAA7oCPAQA7lBSBb0AnhAPZACCIIVYKVH//X2J7naV+YfeFbtndBkhjpBB2QMeSNyeECiNXmmZWMlh9q0QCeysZplcbFzeVcvST2x9Z8o7Ssxpn0/I+T6LYKrpI1eqg8XELcVmOpn4VRx3H6VfK2ZrPHbMafBrNe4/3Zgr2/DcuzKvJzzcfVuXMXoY1hVviNB+ptdjXbe4X2+S912+PrVezI3jkbJWkaSRuiCaJKv0rN9wRbzHO0fquPH7deT08LNmZLbKuHNxa8b/VPHp0q7vQVHBs/RM85MvL3dB8P5OtznDh+K+fW/T2LO9tb+hSvbCs2tZVgH06jTOx7LpMMcvTydZP7Nn91njq/myQIM+qzlncPDGPBjjO63YtK6ytXDHJh4hQZXtn03iWkOb+oXn5I5X5PTlfTqqcHzTjmW6g00nv+8Ww9nHdbwvg3vy680jgcdlc4CYAJnk7fKSFzxy1Go+RvthZ9xJuJWOQrCs+hh3gC6xAUzDqr3u8gPtAMrPuq+TyA/VtAJ54DSPywu+OOmaOnTfXcKY1F7iGFlNs6SeP1WkOuKFzbV3213SNvds8tSi8Q5hbyPryhBW9GrWrNtrekataoQxjBy5zjAA+ZW8a55P1Mybh1bCsn4FhtyS6vb2NGlWefiD20wd/lMKZVrF7LVzaR8yEB0j2d9ECbxpqUy08QQf5rFHNshvfh2bMwYPUMNrOZdMHqHK4jqTXS0SNt4/VaCMRtKWI4fc4fWEU7um+g4+gqNLZ/iuXLNx6el5vuuXHP8rHwrjeEXWBYpeYLeNLLizqvpAEbuptcQx31C+bnjqv9kfDOr4/iHRY3Gy3UeYHE+XsFqR7+Lkys7b9Fqu1+XyJg7qLx8e73Pqj7OmDPs8nXOJ1mxUxO51Uz60qOwP6yvRw4+X+av+pfX49T1uGE/k8f7uxHkxwvoZen8v5cPniwuWPpne1OWhG8LUCrn+zf/hP8lKNQyF/Z4t/5hX/yWPqNyZytC3coKc7yoI3YiTMEk+7SIKDUrjpnkGvWfXq4Da1a1RxfUqOYC5znGSTsg9rCcHwvArQ4fgtoyytSdXg0hpaCTJMD1KD0Z2PtsgocoIeUFO4QUeyAex+aBbkEDZPmQFxsOEFHlBJhADnIKmUFFAJ4QU0oLQLdygEESdkFlAt5hAuZQUgkwgGN5QWgW9u6CnFyBe/5kF7duEAnlADuUAFBUwrADnKj/9D33vLed1+YfeMtnuPZBlB5BMhAJJqSRwgmmWAAwR6LGSw2jqbDZVjNZJPlgCSrl6SMUtdqAI7rMaZw83HMLYkDQdf0QYlanr4EINFzxgf3ywrNDZIbIPst459lZ5Mdx7/QXML6+FXOAV3TcYa4Fmo+YsJ4+i/RcN7sdvi8uOnbmNPMnffdacIY0+qKawclPon1XIkA7HeD9FnCNZuY9faxb04v2bAPr0Gie41iV8T4ly3HG6/N+7+xuN/+oYdvvWX/AGcJ6Y9XsTyW6lh96513gZOk03GX02+rfRcui66zxk/qHxv7J9N13HcsZJn/AJfL6nyznLAM1WjbvCbxlbUAXMLgHMPoWr9B97hnNv4T1vwHrOgzuOctn03psTXTBjyngkwmNfGnDy8ftVZoLHBu3/FMo3L3OL5qdUy71BwjGmjTQvHG1rEbDfiVzxvk1p2K3fqY2D259l3oaJkwdz3+QK43FqPjL7Y+WLu3zNhObWsJw+/tm2lWqPhp1KROmT2mVmeKr5mcweUVZ32eBudjsZXol2xk7R0TyhWx/LPUHEzQbUOHYZTqW90WyGX1J+vyH1DRuq5bdVZ0jyz9oLKeH56wC7bhWa302UcZpcUqlwwaQ4gcFwElS+msfbZuln2V8OyfjdLH8237MYvbQh9nZ026aGpplrz6lpEq43wmT6RDpEkc/wCamVawAWxwo0F87fxQENwEBVo8MiNyFijl10DhPU+yrny0cQomjPYlvAVxHUmkhu/w9loHJgkzpjeOUs21Jvbj3WvpdWzJSOZsBph2O2rA29oN28ak0bH3cAvFy4P6h9gPtNl0vJePmvy/rXy85kOeHMLH0yWVGOEOa8cghef0/wBHcHVcXUY3PDz4QjygqT278vnilZFtaVb65o2FoJr3NUW9L1Je7TKrx9b1U6bpe+vuvLGC0cu4DhuCUQG07Siym8jYawPP+rpK+jhj4j/HPxTqb1fW5Z383rOEbfyXSvnd8y5rF9kcoCSUUxq1Ai6gU3fI/wAlKNPyCZo4v/5jX/yWPqN0b6rQokSgA7mBwgIQBHfgIIgF2x2QQIBncoLlBR4QVKCkEgHsgAkoLCCiQOyBZMlBNu6CS1BUfogByARyEBoBgFBR24QLc5AskHlBW3ZBNhygB0z7IKHCCIK55QRAt6AAOd0AoKcgUUAElWCKj//R2KozeF+YfeMtmESZHsgyAwlup0T7IApk7gAEIAPiB4gbKWDKptLiD6IDcS2oC7YcKZJoBcHVDB2bx7qRWVSPfutinvgQ4EoAeNQlvCDGxG1FxauaW76Y+alnf7a9xyfB71+S+oltcavDs7x5p152b5tgvtdHzX8L5fU4SPqm1rirSpvB1AtG/qF9TkwmN1HzcfTKEOM8LjWhg7x2Vk3KY+c8ZfVMiZAEmCs8Hzb23lJ3WX1HEvtKYpSt8p2OGtd+Jd3QIH92mNU/qvz/AMSus+1/T/sFw9/Vzk+mMv8AvHyvJDi7ueV8aTT+8SM/DMWxLB67bvDLqpa12GQ6m4gfUcL1cfUZYPPz9Fw801njL/SOs5f+0Rm7CmNt8VpU8So041VHbPOrjjuvp8fX53LWo/A9V9jOk5O7uzykn5afTOXMSu8awSzxW7tjaVryn4n3c8tBEiV96z5O5/BOq4MeHqsuPC7xxvv6tI6uYaa+APvKO9ayc24Y4dtJXnw8158vbbMl4nTxbAbC+a/UalNoqH0cBuvTfSPfMgnvysTI28XNmUcFztgVzgOYbdtzh1w3gxNN/ZzfcLncV2+aK32MyMUa6wzE1mCBxOirTJrhp7AjZdcIzfLvWC5BwHIfTvEctYNTDqP3S4dc3DgGvrVTTPmcueXJZU7XyT9mzqZWyNm5uCXIfVwTGHClcNDS7w3ydLgOF1nmEmn3qxwDWvBimQCDEiHCRASeCzZ7XNOwUvlZNKMFFCQgJrdggInaPUQpocs6msdY3ODY2wn+pXjNZ7aHnSZ/VT0OmWj/ABqDXzLdtPyIkKyjLbtwtxLlZ6QkbEidI49Se6554Sryb49ZS6/ZxDq/0cbi7a2asq0QzFNJN5atgNqhu5IA/N6LzZcUj+w/ZL7V58PH91nqy+Lbvb5srMfS10XtLatN2h7HCCHgwQV4vO3+gZyTk6aXj8+nTeg+Xf25ni3vK9PXZ4Ux10/08QbNBXoww2/nv25+JZdN0HZPdfWknTHIO8FfQnp/mOZ6y7h6yQPVHHHHt5Ln9avWS3jdG1gCJRFh8A+yoTdkeESfRQadkHajipG4OI3E/QhTQ3UEBvuqBI7oLaPzIKcd5QSZQUd0FcIK7+5QWQAPdBSASYKCHiUFSgFBJQFsRui6Kd6ozQA6pnsi4+U2SgiRCkqFughVQjkICLoQTb1QC7iUa0SdyUNAIAEqWogU2inLQo7oB42QQ7BAAcIJ9EEDpQCd0CiNzugrjZBDxKBXJhBRbBVgqAqP/9L3qlWSvzD7xlHUdwPmgygZZ3hAmkXB50/D7oGOeQCT6oMmg9kDVMoLutDY3n0hZyGOwmQ5qkGZTquiCIK2LqAkanO2QI1PYQPyFBlGnrpGPRXBY5L1LwZ9S3fd02xUoxUa8ch7TIXfg5O3J5uXHbsfSnMrsfyvZXFR4dcUAKFwDzqbtK/Rd3dNvkZ49t06MwyVisI4w6Uxvis3x5LrXBYWsZu52xjsCueN7G+efJ4918rfaHxoXeabfBWP1tw2iBVb+7WqGSPoF+W6zk7uR/ffsB0N4ei78vdrjq8j+mwbduVHSN06YZaGas8YXh1Ruq1bU+8XQjYspbwvq9Hx992/JfanrZ0vSZZT3X3D4TGNLKfkaGBjD6BuwEfJfpf5dP8AL+HJ33LK+68TMeHsxLDbm3e3asxzHjtuNlx1ploPR6+qW9riGA1visLhzWh37pMrrLsdXDe3cKUFIEDv6qC/MCPMSOxn/wBlZdBF1atvLW4sbiXULqm6lULTpdoeIO/yXKzdGtZV6ZZGybRbTwLBrelUAh1zUYKlR8GQSXcFdsb4G5Bx+kmT7R6ILaA0bIIZCCwZEoCDoaD2KCyZAhBpXUmwF5lfECG6n0qfitHeWmQsZD08k35xLLWH3ZMvfRYKgP7zRBVg2Np2Wmcrpcn1R6s8ZyYaMcBBMR3BCzZ4eHjx5OLenCOtfStt5Rr5vwCiG3tsNWIWtJu1dvq0DuvHlxv7f9ivtX9zjjwct/8An9a9L7OGEttMr32M1GgV7+48Km71p0xx+q68eOnz/wDqP8Q++58ccfWnZTER3HZel/JZ6V/JFECJ34QVq3j9EE33CBdyJpOHsg03p/qFDFgeBiNx/EhBuskxHCAidoQWNhCATugkGJQSO6ATvugE+o5CCwdXPKCeyADuUFk7IBQDIQRBc7QjULJHwz5v3e6MVzTF+ufTrAcSucJxfEKlriFq7TWovpkEH/3TTG9Mdn2g+ltVlWo3FzpogF0sIkH0WpGpSj9ovpR8P7XcDz8BSrsP+kX0o4/a7v8AYKyncn+kT0p/+rO/2Ch3IftEdKj/APy5/wBgodyf6Q/Sv/6uf9gobUftEdKiI/ax/wBgox3F/wCkL0tk/wCtj/sFDuUftCdLi3bFSf8A0KfVqZLZ9oLpbx+1jMwBoMk+ytmm42nKufstZ3FV+XK7rqlR2qVdJDASYifVYxu1sbKN1tAd4QWTI27IA3giECxIElBczugA7lAJ5QUeEARuSgondWATwqP/0/cADnR39F+XfeZVGm7SYP0VD2S0Q/hABZqBLPKPdAmSXBj/AF5QZ4pt0beiAajRAHJWaBpOAO3/ALKQZD3NaAYk+y2A1Hwy53CAA8lzacfVBmUnlrS0rWA8LMVgy/tKrAJ1SHA/JcN2ZJppfSDGKuXc3XmW7l0W15L6AOw1N7BfpOlzl45N+XyeqxvfX0vRqaoIPlI2Xoymvbx7jJ1R5uQsT2WPMxi+p4Rhl5iZpuquoU3VfDYC5ziwagAB7hcuq8Y+Ht4sZnz4YX1uPhbMd7iOI4zd4tizH072/qPr1fFBa4Fx2EH0C/H5TK5bsr/U3R59Pw4cfFx542dvnV28v0Prwpt9iWUYnsJjsq6x9B/ZiwfXe43jj2y6g2nbUXH1f5nQfo1fpPhWPy+X8Y/6h9VfHHK+kpcSZC+pfxP4nzy8eeMnq2FXLQ6k4DnZZyjtn7unG7F5y51SfRdtbYvTLWjtrHdZwvll2Si8uaHHgj+Wy65ew/b5z3WBZbAkfooIPU8q6An4RCAwTAWgz8s9kBFwQAT6d0BAukjsOEEk6gCgwMatvvNjcUYkPpvaR6yFnIaZ0ouXtwi4wuqfxcPuH0nT2BMhIOitVp293gSRMOS400kaeJ9l0jpnz38gwHtc10EOEPY7gtPZc88Ux3jnjyz28rBMEs8u2JwzDmabM1KldrP3DVdq2+UqYx7fiHXZdTy47er9Z91p4aiIiCb6hCAt53QKuifCdHof5INNyAXeDi4P/wBRr/5IN1bwgiAkAoITsggPlQASgEFAbeUFHlAsEz7ILQRAs8oCQRFCfMCATt8Ubc8EIzXBftEdI25uwp2asv27RmPDWl1djBBuKTeZjkhd+KSuGe3yLlPAf6SZhtsu171mHffHmma1wXBjXj8pjiD6qZzTWNdku/su3uHPZRvs34Xb1HCadOu51NxaeCJ5Xnm7XXL0xj9mszH9NMHn/vT/AMVqefTltP8ARskf/wBbYPB4PjH/AIqS79Ch9mlxJAzthEt+IeKdv4qpbpf+jVVif6Z4PETPinj9VdVbdB/0bXyR/TXCJHP4p/4qTz4i68bV/o1VCT/+ssJM8HxOf4pJu6XXjZOK/ZsvcJwO6zFVzLh9XDLVpc6rSJIcR2DuFMfm8zzGMrMbquV5PyvieccftcDwprqz6tTS+s0TopAwXmeF0zs074P0AyPk3C8iZetsFw6lpdTANeptL6hG7pC8vFvdbybJOnU1d2Adie6AZ5QDqKCO+FAA4QLJIKCIISEAeqATyrBR4VH/1Pa0efU0r8pH3mWzYSDuukDGvdHmE+hREFRx2IhRuKcBqaQqVlsdAHp3VZVVl5JbwoEMY6dzPsgzG05ZA2JCBTtTWFp3hGg0nulroQZzBLo9QjLHuaIdTeCOxCDjmcbarl/GrLMNnLa1pUbUPuyd16ekuuTbz803H0xlfF6OM4RaYhRIc24pipt2JG6/R8uXdHwcsfmex4pZse644eI9Ms0cAKjC0jZw4iZWMsducyuF3Hj4nlHL2O0H0MRw2hUEeYmm0P8AoV58ul75qvr8Hx7m4P7q2ZfppxjOf2drcU332TKxbWcCXWFYyHH0aey+P1HQ9vp/T/g3254scZjz+/1y/wDZwDFMMxLBb6rh2I0KlrdUiQ6m4RMdwTyF8q3LG6f1LofiOHW493FdT9PL6h+zdbNZki4u+HXF3UH+yAv1vQ46j+E/bznyy6x2gO2Xsy9v57le/UoC6fKRsVvP01rXhyDqzavsK+GZiogh9hXa4ubzoJgyvJjNUdRwW8ZfWFC4kGlWY17D7ESvTWa9FsRA4CiDb6oJq1GUEjTugmpA1vw6UAeH7oIRpb9UDGmRKCjH1QKrhxpOETIP6Qg5tkyr+z8+Y9hL/LTutN0werjsUHUWTAkdhv8AJAZMQgIO2QA5BJdKAmuQR3Y+qAplBEEK1Am5MUX/ACP8lKNRyAZpYt/5hX/yWJ7G6jhaFIIgFBEBBAo8lBSCwgL1QA34UEQCgFBEFjugByAQJ8sA6toPERvPzViV8afaO6U/0UxRufMtUn0sEvagqXTKQ3tbgGS6P3SV334cNeWS24PXPpk5gqaOo2VGeV4dp+9WzRJAA3JhYxy7ctulu5pg9LMr9Ncy9OcVtsxXBts4UKlc0x4jzct0CKYDRuSTMrnl/B/qzG0Dpx0Xtss4Jd3l/Vp3pq0G31B9R/j1N/MKlMbifZSY/dzu/Nv3NBdlToi/qicJN1/qP7j4jbbxKn3dtwXCdwdWrT6rfLOzDG/m4dvf8v8AheBgeSekuKf0x+945UpOw+vWZglIV3SWsBLdI5P1W+XLszxn5rfn+b/CDDcrdI62ScDZd4u85lvLlra9UveajG6/NrA4Eeq5T+FvL62ulnmY/TKbYWd+mOWbvPuCZV6b4rWvKd+JxANqlwpAH4p7CJTK3iw7/wCbJMNXP7u+oR1tzhaURY9KMpvc/BcKDaF5UonUbi6Ijc9913wxnT8fZ+fn/VxzwvJn3f0dy6BdLqWSsv08YxGiDmHEmB1Qn/o2RAaPnyvm3zk+jjO2OwO2geggn3Xr+jnaFRE+aAXBu6BaC3fCgUgFzUF8IAegHsghVgE8Kj//1fcGkObAX5SPvMmNtgukBiWtEn6IiqjyRLQjcDReZh4me5QrLDwO0hVlC/02B7KAabT+qDJBdEA8IALiNiEaBQkEg8DhBkh5HmCMpUrEs3bseSg07OeE0sTw2oNMua0gT6Hsky7btnObjM6D5gf91ucr3Tv63hziaYJ5pO2H6L9L02Xfi+Ny4aruBY35gCN/VX6vLlvxoymdxP0WpXbGy+zCJeEuWp4S8M3vH2vvB77Fcrh3zyxlwYTza0vqD05wfPeH1aNekKWKNH9XuwAKgI4Gr0lePPpMfb9f8G+0HN0uWOOFutz6vN6L4DfZbyvc4FijCy7sr2tqP5XMcB5mr19NNR3+0/WcXU9T4dHa704XSecn4fk+XOaHMuj2Wsna1p2f8KOJ4Be0Ni51NxbP90LlYjzuk+KuxDK1G3qGa1ifu9Un4paf+C3KldDGxIHwzsVUMCCaR6wguQ4bncIKQXq8yA0Egn5IIJCAhpO/dBHbsLQeQUHK8WcMI6k4XeuhtO9pOt3H1dEiUHVWu2A1SAgMEILkRsgh4QDJ9UBjSgomfkgsFAex3QCHAyPRagXcQaTh7KUadkJsU8UIO37Ruf5tWJ7G7N4WhEFO+FBSC5CC42JBQLIgTEoA1SdxEIDJEbcoBkoLbwUFIBQC7lAUiEASgEoBd8J99iqlebjeC4fmTCbvBsWpeNY3lM0qoIkgEcj3HZWVLHwncNx7oF1Tkhxp2dYuIB8lxZVDwPXy/wAV0zw3i443d06Nj11hPS7qJhfVvCLVt9lLM1HxvDptDvBqVBNQDtqEwuHPfvda+jpWxYvjdahnCx6z3OUagyU+1bbueQ015eZbVDJ5nuu+X8TGYz6M71TKWOUcsZvxbqXmPJlahlvGaFJlldhjH1KcD4zTnbWsW/e/J/hXj+TK5X6vKy/f4LlHNmN55zllKth2D4+af7Bmm17Rq5Dmzs5y55fxfnn8rPHj2Y3G/U7CsXyzkK+zJm3OuU3YdhOYH+JgjKjGOD6YbGgAE6XOmVrk/i8mOvw68rctcd/ONdwi4temGQcX6ieEKGZ82VKlHL1uRBo2zydxO4gLtnjM+TX8scLb2Sz8Tyvs6dOamb8w1s44yw1cLsXmtSdU3bWrEyeeYK8nPzfeZ7n7PoYSSPstrQwBrRoaPhb2A7QmOBckJgR25XRzipHZFUgBwCAY90Ed8KAEAO5QUUCiSggQFt3VgBxCo//W9octMr8tp95nMqNDfMN1oXUgnfhDQCRoJajQ6EVBpcIPqESslrWtaQ7jse6IW7SAfRS0MtQ0ggnfspKH6dIPutDHcHkEk79kXa6bXEuIQ2yaMaYciIIaXN5b2lBhV7dlc1Ke5Bbp09t1izdX6OaW9w7JXUGyxH4LO8cKFw48AEwCvu9Hy9k1HzOfF9OWtdlxRZUpmWECD695Xus+v5vn45e2QIBkrNjncNnaiNJbvKSNdvjW1ubIk8rp3N/TSwRABHC5ZY9yY/L6UYBJDQCTqMfID/JaxnbHPLDuz77bsQO/ASTV26ZfNd1e4cSs78rtiYhRbXtntIBBBn67KZG3Junlc4JnXGsBf5aNwfGosPvyVOO7R2Km46R85Hy7LtoODisSiwSqLdHxRBCCDiSgGd0DNRQMaZCCudkFGn6EoLDS0KWjmHU2kbV2GYy3mxumF08aXbJsdLsqjbi2p1WmQ5jXD3BEpKH7AqiztCAxBEIFnYwgI7AILIhBQ9UBcoJAV2FVwDSdvGyDUcg/2OK7/wD8jc/zas6G6N9FRZ2QCd2oB1FBYEiUEJgQO6CBxAiUFh2xJ3PZBNnNng+yBTjEoLadkEQCdkAv+JBXqgEmAIQRvmJlADj5VRKfPfjspPBfLhf2l+nTc1ZS/pFhtEftjBWF0t5dbjd7T325C9nDl3Xtv5PJy/w53RxzpLcW/UTIGOdKMSdNzbUziOX3P+Nj27uaCffePdeLpfGdld+f5cZcW89NLjP3U3pziuTal7bWLsFBw59V7S65PhOlrYO3aOFePO8fJ+lTtlw39VZjxbqvmrpRXtzhtqy0w64p2lauHHxa7LNwGpgJ2aSPMt9NJx8ueX+Ksct3hjpmdS3dScyYflTIVzYWllc4xUpVvvzHF7adS0h3B4McwufTfw+LPH/FHXlu88bPTwM7HPGd8+4D0lzEbWpRsalK9uLu2aQ19Kk0eZ2qY2C1nb0/FMcPmuX5/wDDPDhOXLLK/wArQesGNVs/dQrbKOAy7DMMc3DcOpM+EuENqP8AnyvR1UnD0ss85Vx6W93LbX2HkfKllk7K9hl+1YGtt6bfFI5NQgF0x7r5vDx/Lt69a8NiPryvX3JotzisqsIIgW87FAokgTKCy+RCCiYCACZ3QDqJCADyggQQnZULcdk2m3//1/Zo0w9wA/LyvzD7zLhpb7g7IC1NcACihe7wpA3aRt80UVnWgkOHKJWU51NslxRCnEH5LNBUwRuw7qQP1mo2B8QWwl+rUIBjugbRqaHEHkoDIAcDvDkBVHACByeEAUpp6g4bnghdL+BGgdQcJ/aVhVrMH4lEh08EFpkELHT8l7nm5cduodJsyDHcr2rqzpuqA8GqPentK/T90sj5eeOq38+Zpj//AGVHMVNxESgYXHUPRAcE8IKcgJx3CC9zypJ5A1WTTcBO/CZwcZzk12X894Pj1IaaVZ/3esR6P23XLHwOyUKgqMa8egH0C9QeCuetA2kSgvuPRBbiCICBE7oHjcSgNswgjSJj0QEXTwgmrUCO4Uo03qLh7b/LV6wDztbrZ82bqD0Mh4h+0Mr4bck+c0gx3zbt/krBsjhv7qizwgjT2QRwMygs7gICIkbIAJ07FATSgp2+wQKuAfCcPVBp2QCfCxT3xC5I+pCDd2g7FAZ3QCdhCAIQEDAQU87BABKCTsQgJphqCj8Md0AzAQUTHKCyZQC7dxQUOCgEiQgjRBM+iAS0kH2QC0x3hw4QLuaFC7oVra4YH0K7HU6rTuC14g/zTu7fKXHumnwRjFvedF+tTnUHFlraXba1Ij4XWlZ0OA9RBj6Lp1ePZqxw6S925W9Zy/bWSertpe5VxV2FYDncUrl1Ru9Jvj6Wud6AidlOpx3hLF6a6zsrervJ2csPxaz6W2GZwMvYtSrYjUvHsDrkOB1PZz8Ljwsc11hjY1xY7zyl+hFfLfUrNVvXzNc5ho22KZJrVqNi2lTGiqLfZ1SpvsXgLryzWeMjlxZfJlb9Gm5LzFidplTOXWXMbxUx6/a6xw/s0PdsdE8Bb6afe8+r6jXFl2T92B9l/Kv7ezZeZuxFviUsOl1Nztwa1QySPXcrw9Tn39XcPp/7O3Hx9l2+xDJcexneOF31pupEBRAFp5QU50CEFQUAPkIAduIQTSgjuEAcAoABjZAEiSgpxhBUyiUtxRH/0PaoVNJBO08yvzD7zM1U5gCZ5hANSn3BgeiKh0Bnm3KKukaY3hErIewGnqbBRAB4ADXARCzQykGuMhSBmlrSDO3dbAOcNXkJhAdBwO55QZDhIYI39UCXtM6f3dkDqb2vGl2xHErNv0Z+rCxezp17V7Y3e3S4ey1hjJ5LNtJ6W4m7LmdrzL9VxbaXxmgHbAOb2HzlfY6Tk7tx8zqcdafRVJxdPoBH1C+hp4jW9lA8ARPdAYe1ADt+EEPKB7Q0gLUAvBgRwpkOZdWsLdd4DUq0h+PbkV6bhyCwyvP9RsuR8ZGNZbw++G73Umtqf4mbFerH0Npa4OHuOVKIsgnElAEHdBTPKUD2kQgNvCC2hsmRugjoDfKgGYgDvypR52K0PvVlXoES2oxzT9QoNO6U3LqeGXuE1Nn4fd1KIB5DZkKwdGBkk9lQQQVwUBAg90EkeqCwSgjhLZ7oBb7oL7oF3HwGEGm5ADvAxI9vv9x/vBBvDeAgKR6oBcgFBcIBfwEAoLHBQUSYhBUngoIUFP7IJBQASZKAWk8d/RBYO26ojtt00bUe3ugAx9SgomOe3I9k1s3Y+X/te5TZVw7Bs40Wee3f9xu3NHNJ27ZPtuvVhrlw8vJnLx5+GiYtc1c7fZ+w3FwS/G8l3f3etUH9p93cYZvzsS1cOnvdvHPx+W3bqZ26uHn9nrUsGtrHprZdUaWcbh+b6DGCi99UONNtR4a6loneAuPD82dwvuNcvyyZY/V6nVKla5O6dWV/lHMdapiGYX0ziLfGDvvT6wBc8AHYSV6OK92Fzy+jzck1lJj521PrRcf0ayDkvIFA6ajqAxG/A5dUq7gOHzlTpbePjy5L4u7r9Tqfx4zHz+z6A6AZYblvpzhzalPRcXzvvdckQ4tdOkLw8E7t8l9vrdRJMJp1Bpc4z7THoF6dvNVl23zVRN4E7Jo2W8EQTxKgIgxPZAp53b6FBIahtSCnIFu4QKcYKBYaXEmUBwEEIAQpLuUZf//R9mi3xBqX5h95l0aZCAqrTyTsikPgiBuinMZ5PfsiUwVCKQb3B3RAO0mJ5RYZSmnv2RTSfID7oFh26BlLlBkF7iQW/COUZU5++ojb1QGA17muA3QMrMLwdMFoEH5qjlOe7avhOJ2uYLMaatnUFVxb+6CJXq6bLWTzc+O5H0HljFqeNYRaYhSeHNuKbXOH94jdfoMrvF8XPHVe80SP8K5Y+nTfgxvCrCnGeyCN1fCgscFAVMwEDWGZCDxsfsWX2H17Z4BFRpb7x3XPKeRzvpHfOtf2plyqS19lXcaTT+48yP5LrL4HWGeUn0MH6rMga06jCot5jZABdsgjeWoHFsFAQKCy7dBNWyAZ7/RagXWbqpuZ6jb5rNHNsqVDhmfMbw139neBt1T+fDlmex1BpMQOAVoNHCCkFN0oC2QWOEEcYagEGQgiBVwYouQaj09M2eI/+PuP94IN37IBdygmpBEFk7IA8pcA47OQUW6SQOAggQQmEAEy5BY2KCi7dBbnbIAe7ZJCwtzmspufU0toNGp+s6QPfUuk43K1zLN3X3pzk11S3r4gL+9ZsbayHjOB9D2CuteCXblGJfbEpNqOGF5c1U/yurVYcfpGymxeGfbDsqjw3FsvPpU581W3qy8fIRupV26/k7rVkLPBp0cOvxb39WALO8ApvJ9NUqNyugO2Ghp9PKNx9D3ClataJ1dy43NXTzHsLc3W40X3FuO4qUhr2/RdOD25cvp8r/Z7rtxa3zZkG9dFHF8OqVKYPavSBAd+v8lnn/Hj+8OHxjf2IyHfZDo9Pc0ZfzPYVamO2lebSrD3kAv0SANhHO6THXVd37uOF3i2XOmH5KzBnbIOXcnW7qdR3hvvWDVApCOWv27dlz5L/wCE7fz26dPPmax1Truzl1tGCWv4lvbV6GH0QOIpEav5ldufP+DjP0jPTTXJl+77Zw6ypWFjbWNEaaVvSZTa300iF4+B7uTLyyKjm0galVzW0miS5x0ge5K9Uw35efPk1XNc39d+n+U/EoV8QN5eMkmhbQ9sjaJCutMd23M7z7XmGtM2eAVHUjsHVH6SR8ktWMrCPtY5Yuntp4nhNe2DtjUY/XH0hYrTrWVupuSc4t/1RitJ1btRrO0VJ9AFyqz22wx8Tdo2jkEHuElaoHLpMXOxNSNhHJQDViNuUCoJElBUQEEHZBHcqwKcY3VH/9L26IDfLTMr8w+8yC+pTIB7oCcdbTJkorDOpneRKKzbZxIk8olN0gOmNjyiMdwcNR99kWMmiR4fm3RUDXaQPfZBT54Ag+oQMpNGknV554QZIaGNMmUZK1giO3YIDpOLNxuUGQXgw0DnlUazm2wF9Y1qGlpljmkEcypM+3KHbuJ0Kxp7aN7lm7eTXw558LUdyw+nsF+nwu8HxefHVduYZAc13lLdx7ypHnNaYCovVPZBbQdaAnARwgo7DZBGvjkoF12NezfmD+izYOOg/wBHeqFNzvJa4szwvRoeOD81mUdlpuJY09nCf02XbXgGDBlZEc6RzuggIIjugNoQHJI3QXI0z3QEeyCjs6fy+iCCPotQVUgtI4Mgz8lKOYY7/qrqFg1+PLSvg+3eRsPNBE/osT2OpUHtcwOPLiXAew2WgyfThBJCCBrZdugCe38UBiW7EoLJkIABKA5hAm5I8ByDT+nbv6niP/j7j/eCDdg73QWgpBaCHhApzZc0oGGAgoEIKcQQgA+vdBTigpBf5UHk5jzJg+VsJuMaxy5ba4fbtJc93LnDhjR3ld+3Tj94+LupvXrN3UbETgGVm1rHBKjiy1sbaTdXEmAXFu8FXvkLNjy/9nu6p2lLHupON0ctYc86hQqOa+7LSJPlcdyuWWW6smnoO/0XMA1W7v2hjtantUuKby1pBIEkHYbrOwpg+zFmTTbURiGBXJJ03DqhewN7Et4VSvGzR0HxTCsM/pZ08xZmZ8IpDxtVmQ26psnkhu+yLK2no99pDEcEuaOWs8Vn3WEBwpW1+6TXt3cRUncwpfTpt9YVcRwy5weriLLinVwp9B9XxyW6DSLTJJ9+IXTg9sc3p8LdHL+3tuuFr9xOmzuLm4otZ603udt8hK59VdWU4vwZfs305X6k5SxHPuFYNlF1/huYK722t4XNgBx1S0dl25LO7DL84zw4fK2vLOG5yzF1Hy3j2YcqnB6GD4dVt6lxLSH1KbfKT6FcLjvHDH86mGXbk4xkPELP/l6be4y6KT8UuCHOPDy4gSVepmvH5OnDPNr7gxvHMMy5h9fF8XuWW+H241VKpcD2kNb6kq8HH4Z5c/mfGvVDrzmPPt+cCyuallgz3+FTbRJ8ev2Exvuuty7fDNx7rsvA+gVanhrMd6h45Ry9Z1RrFKqQbl077tO65XLZMdPQOG/Znwio2hXur7Eaghj7hjnDW71A4SOkMOUfs9Zkf4WD41d4HeVNqbbmXeYbcOUqtczD0UztlJjsfyrdtxrCqX4gvLB0VGt9S1vJXOzZW5dKvtIXuHVKWX88l1azJDKd6ZbWpEbaak7q9uiV9U2d7ZYna0b+zri4tbhgeyvS3ZB4G3dO/TejnfCtIEHb3QC7cIBQV2QAUASZMlWATvsOVR//0/UtyW1AeAvzD7zKqPe54aBLT3QV+JSG/HuiqbUDvn7orKotgSUQb6h3A7IaLc7UPdBktpFtNpnb+KG0qO0ERwENkOqNmZMopjGwwaCSSZJPKDJIdpiZBRkuN4QPp6mvgwZQMHxEe+yoxb+iKrRq2iSFm4S+fyWZac2pXD8oZ6s8WYSyzuHeDcEcHWZBP1X1uj6nLP5b6fO58JX0paVm1KTHiPM0O9vNuvqZSS+Hy/V0zZgCO6y0tpk7oGAw6UEJJQR3wSPiQBpncoDeA5vz2+iuvC6cl6s2JoMs8coSKlhWY6e0T3Xml8mnScCvad/htvdU3aqdWm1zD8xv/Feq+h6XKygTI9IQWwwZQOHEoJJiEF9oQFJQXJIgoBJggK7FwNJ/VQc16pUjSsrHFmbOsrinUJHZuqD/ADUvgdCsKrK9pSrsMhzGx9QCrKM1vMdlrQoneFkW3ff1QU7seCguZCCxxCASSHABATohBj3O9JwCDUOnUfcsR9fv9x/vBBu4QXKCtRQFKC+QgAmI9kEJndAI53QWYB24QUd0Au7KxKqJ2HPZZyuvRLtj39/a4Zh9bE72q2hZ27HVa73mAxjOSfn2XTDHbnnnY+FOpvUHMfWnOtPA8AbUfhBreDhNiyYc2YNRw/iunNn2+Z+FwmDe7ajlvoRTs8BwejSzB1hxXSwNdpc20fWGwB7RK4T13Z+vo6zKw2p03q0s0NxLr7ij7u3xK3dWw2hTe5zDcDd1ItaZDmtOy5zculuVrFyVlurjeTM54VlbAMPuMIt7m4p2F1iLAy9DNAOkcHYbgnuu+jbz8w4p0lxrKuS2XmB17R1vc07fFn0bctPh0xpcC8DzanBSM2vPpnEunuYMdzb01un2uUcKqUPFwTENVN9xTrDz6KbpJZKVZWN1Nyjl/PGV29X8hUfAaHEZkwSmPNRq96jQO0pPLVunK8NzPni5sqeVcNxG7r2tfytw+g5z9esSABzsOy3f4c3Gtd88s7C8p9ScExG3xTDMBxK2v7Z5fSuW0DIMLy5ZXl9rj8s1G7f0x+0XIJGLGAf/AIc8nvwt3O2Yz/DNNY2YzSf00+0UWOa5mLQ8Fr/6vyDz2VnJZcb/AIbtyvHjvbntfKOfnXdbEq2C37LgVPHqXApODg8nVI955WplebK9zWV+7ngnFc65vxy1p4RjGJ3Ne1puNNtrVJEPbxI9ZWry5cfiJMJn5ruGQcq4X0ty3a51zBZm9zfirQ7CMM063MaTHiFvaFzu+T5qb7fEexe5TwTGsdvcX6i4xXx+wrWTrqi61a4UrRxG9N44Dm+inbYb28vDMYyNeZRwrLtpgHgYoLxtOxxe7oRRfTbUlrnvjeQd1vHZfD0M+5XwK96jYNhmcsOtsNws2heLnCYLXPH5nae0BWxnurycNwPPWVL/ABTG+ld+cayRZVCXUa9Xd4a2XM0GZhZk8+C5BxjLWU+teD3GOZRpMwnPdozVf4TAaK7m/FpH7y1je66qTJ5nRjq1iuQsZ/olmc1P2U6oaT6VWdVB4MSJ7Llzccx9Oszr7Eo3FC6oUq9B4fQrN1MqN3BC3uO1xi9uJVc1E7QoB7Sgh2CBZKAJklWBZmVR/9T1rX8QTC/MPvMioSxs7zKCn1NTd9wigFNkgid0VmEFrQG/VBZADASYlAknQZmQeIRKymjWGkOiOxRDKgb3IO26DEDGufI4Rpk0paDtzwgOCyC07lGQVHvJE9uSgcHFwbo390DPM0y7lUR9QQde87NUZrR884W67wyrpaDVpxUaW8y3cLtwZdlY5MPDpHSzMn7dytbPrO13Ft+BWd31MC/QY5d2O3xc8dVv7GkgOmQqwN3YBBcGZQGN0EmDJ4QWNxsgtkkFLfDUaznbCmYngV3avbLntcR8wFx15Vr3SXFDdZbbZVSfGsKj6DgefKdl234SujDcfQH9UZC5m0hALSfQoMpu4hBI3hAQEmO6Amjt3QQoKIlBWqNuxQajn3DmX2XsQpAT+E5zR7tGr/JShnT6/OJZVw2q8zVbSa14/vM23SDbmnzAroJ3WBbSIhBZ3CCggsFBD6+iAXevqgRc6hScUGpdOSHYbiDgIJv7gj/aCDdgCEFygEbmEFiUBAwN0C3GTsgsHaEEIIBKCoJb7oBGxQW/sUpVes8DuunHjti+Hy59q3qXVoMo9PMIrkVHtFbFnMPLT8NMx6r0zWLnfLWMsW9t0O6f0s13Vr4/UjM7NOCWZaXvtrZ4+KIkLw/iz7L+FrR9ll7JlTo5Vz/iV+09Srm4Nyy+rVv6zSvRWhlMNO4AHMrtyYzLHtvqeh0DEbPHcrZoyJmLF7utm7Er6lVpDC4aTTdWptca9AExtESVw4N3CW+2a8DGcfy409Q8Sx3ErzKmM3E29DAaVbwalRzKY8OoWtBBLyfNHZbyRrw6i5uzblPL+X8l5Ee6lhda3rXdZ1LyVXW5DvKXAfGRJTBK9u+v+olxnvDM75h6aVTh9lZvtK1nT01Q7XuHFoO/6LVWNP6eZmtsC6h1qN0z7hgmdrm5scUy1WYaRtQ8nQ8teADM8rDWXpozfu3SHrPUN3SNWwwW+e5rBuXW72y0j6PCvJ6dMPT6K/0rMhPeGfsy9a+o5rKbH02t8xHAk891nhnimT3MT684Tg1h+1MVy1iNrhuxbduYw0nB3BDmkha0wxsP+0XlXFrGpiFhg95Xs6RipVaKQA/VwV0PLuftUdO6Bq0K1ncyGwaZYwjkyDBXXgx1tjkvp87dOMIs8/8AVoOdRLcMqXNW/qUTyKFNxqD/AN1z5ZLXfDxi6BifVssz/WzDgls/G7yk9+GUMENLxKFO0pmA9sTDiQnd2+HHe14HmLrDY2GP0rfJlStRxypUrBz6e1HxREBpG+yzeRqF431Zv25Zw/JuZ8pPwqi19KliF86iWgNa4anMIGxhMc9rXQbK5ytSzFh+JdN6QzLdVbZ7L+wq1PE0UIEEF3Dh3CWponCbLNNPBszY7lu+p4M59So+6y3cNaW0WtHmO+4JC1xec4zfTT8TyllrLeSrTqpknGPu2YqRFe8aKoPi1XO87NAO2648vjPwzHj55wvDOq2UKfUbLtJtHMmHMDMcsqWxcAJ1wO69Wt4+XSN8+zh1OdjFkco4xV/rVo3+oued3D037r593HbHLb6DDXkiRE8+y64XZkoDfldazFuCigduJQL9UAEwVYJHdUf/1fUo1ix0adl+YfeZYIqsOrbfugosAYR+iKUwnW0HsisyXEEDnsEC6hLmaHHfsEC2FuptMj3lErPOhgEIiwGgk8yNvRBiuAL+dPyRplN1Fsg7eqAi6WyUZAJeZO7UGSzQ3SOED4Y4+oVGPV0gkRxws33GMvcYt/aMuaZYOXNIB7T7q8l/J3utNV6a37stZzvMCuDps8Q/Et2cN8Ru5X3+lzl49b8vh8+Nl2+haLpaS0bE7L06rymgTypPIuQgppKAncIIw9igMODNvVZrUYt8NdJ5IkARp9ZQcmyi85f6g4pgr9ra8/rFBvYzzCYpXX6R2ngcQtIaAT/xTYoMMfVAwbcIKeTOyA5IIPsgjHcoClBY7oBjdB5uLUBcW1alEh7S39RClGkdKLg0bXEcHcfxLK6ew+zSZb+qQdIDjIC2DBMrIs7IClBEEQU47eyCgZAQJvDFF/yQad01dqwy9j/r1z/vBBvfZBXzQUTDtkFAmUB9kCTIKCwdt0EDiTHZAQO8dkA90BEkQW89lL6WPIzJjdrlnAr/AB2+Om3w+k+qQdgS0Ej9TsvV083HHkvl8OdNcJq9VuqN3j+YnF+EWtSrjGKVKnAp0zLKZJ4bwvNzZ3LLWPm/ozL+bpGQuomC5g6vYvnDOFMUMuUbd1nlt9yybSmKT9IawkadTgNl15pjr7uXzPJ596ehZO6W3Fj1Avsaw/wsVxK6rVsNwy4pOZWq0tEU6lGn2JO4hJrlw7b4mP1Td3r6taxm1blXIWS865bxq5xLqK2pToWltVqmrVFN40uo+C4mNOzTAUyyk/Qnn0ycWw7LOSajuoHWeozHeoOJaKltgFONFHaWB49u6dmU82aiXKS6rQcd+0b1FxqqbPAXU8Isi406GH2NEAtbwNwFvLX0auNn0edR6ndccE/r9xeYpToN5qXNIiiAeCSQuNsSN6wXrjkXOIsndWcGa7GcPqNqWuM2LQ1z30jI8TvurjGr6cq6g52tc19SbzOVtb/1F9dlShQfE+Hb6WgOB9QFebXb4dcPT6xy7jHT7PuDUsVbg+Hus2UqYxGi6iwVWOfDS5pAB8pH6Lzcds2l9mXnSPCbvG7fLt7ilzWySymb+0wMuBpF+ofm+IiDMSr3NdraKvTfpnZNFu7CqNrRdDtFMlrSDsNQ9CteU7XK82dOOmd5m+zwU4fRpC+uTRmi4g7UtXr6wvTx2yPLn5rhvTzMmHdK+qF6zGab/wBmU6lzYXDhvUFJ5LRB+S45TLu3p6ZlO3TcsW6x5GyLb1LHpRhDXX1y5xq4tdNDny4yYn0ldu3G+7HHHGyeWo1utPWa5H3tl5dttuWPp0jp/gFm4YfnHSS17GDfaRzGS2wzxh9tjuEHy1mVqTRVg7SNplYuEx9NNzwfDML8/UjoVcNGI0ml+JZbrHd35nBo52hYpoxjsJzlkjFM8YhiNSxzTUuC7ErNlTROgw6iaXKuFuN2zZ4Oxmw6S4Xe5Vxuzqtp2Neq2liWHFxfTDtMnxKfbddce3LLzXLHz5eXmbMuVMh9VLW6ytUo1suYvRDMWs7b/m7mv8vymCunNdenT6b+jR85YXX6WdRrXHcEd/qa+ey8sXM48N5ktkeixyccyny+WeO683w+y8s4zQzDgdpi9s/Uy6Y1zjMgGBP8V4+L5t9vnT1cssm3pzv6Bd97c56RzkUE7b8IKMIFFWAZJMKj/9b1qTWkS7svzL7x2r04QU95LYHKisdr9FT8SUVn27vPqAnZBKo1EuiIQY4cdcgSUSs1lTUAyoIlEG6D8J43QI1N1+ZGmQ13k8vwoDDWuHKMqALPI3ugYwGRq5QZDXaNo53VCK1TURtClWG026qZETIVSuc52p1cLvLPMNoIqWdVr6h/uA+b+C9XTXXJK8fUzeFjv+WsVo4thNpf0XamV6bXj/1Bfore7F8jt09rUuPH42ovK5qCtvyoLmRvyOUANMklA2Aee26AagbUZ8/8lmjjmfGPwLNeC5iZtT8UUax/+287q4XyOtWlZj6YcDqY8SD7nf8AktUZeoxtwsa8gg5aFtQRBbXdjwgJ4iC36oJMkICLo+qCkCLhuum5o7oOZ5WqDC+ouMYeRFK8pC5pj1cw6T/NB1TymHeoQW0e6A/ibqQUgiC5gIBnUCEEZ6eiBF8YoPPsf5INM6XvnDL4f9uuf94IN+bwgpzkAoCQRALkC0FjgoDYgrugtVY4D9rDNj8IyNa4Db1NN1jNwA4etGn5nfzC9nTXt28uV+ZyHBaNfJPQKteWjCcw58vDZWwZ/bfdKXAb383mC8fSfL1FyXlnhvVrmHCKXSrCen78s3VXNmG/dq99gwtwXhlCoKhr6/7w2We3VuX+Lw69252/4fL1sTzpb4jjWEdXrXLVS6yNglpVsrq7exrbptQuhx0HllIiJW5P5P6/6Odv8/8AT/Vp2D4pgOFMzF9oLF7I07a7ruo5Pwmt8LqxEeK2n23Elb6fD7/lsy9RjP8Ah4+PbkuXsvZm615sv8Xxa8NGxa43OMYrWP4NtR50sJ2BhOr6i55fdY+o68XBOSd2Xttlz1OyjkNwy50mwKjfXLT4L8bvWCtVrPH52NM9+FeTCYSSfkTlue5f5XQ6OM9TbC1w/E+olJl/kDHbR37Y1UmUnWXieRux3BHIXm7fLLkmOfZ6zfTvzdZWbTxrLly7xLLEaFZoaaDzLG1W/lc3uu+/DX0IqdI6mXc0ZSwDEL+le45id237/Y27hUFFmsO3I9hC82UdMbqO05ty9idlnXMNHJFrb0rS1tLUXuHt2LmFrvENMD8xAXXH8mMvzbDhPVPDMXOG3OsNrYdULy2p5alFu1E0nj0jddZwyOf3trIzh1DwoYVc4pXjwX0q7Lai13ncaQBbH1XSahu1xujY5lqYthGc8Xrvt6LLptWjaO+NrHAAE/MOC83JnZVweNnHpViub8946zBLmh+0nMbfNtKzxT8Zp2hpPcwu/wB5e1yk1k8nKvQfN78Up18z237HwO2cK17d3DwIZTM6Gz6ryceHddvbyZeHRMG6q4rmLqBRyXk+jasyzQIoUXVLdrwQwaS9wjeT7pycXljiy8PV6jZXyRc3rMCz/Rs8Lxi62sscw8imA7salIcBeq8fyxmXdril3aZx6H5woXTLgvtnOFS2vKP/ADe7oH5bSQuetNOrYp/R66usD604LRP7GfUZTzPhlHenTqTDqhZws5Xumi5dvlslLHsl3Wab/M97hBGTcVs22lHFKtMeCKw5OnsXcSrlx/NLPTGuz5P8X/LVcOrdLbvIGZ7TQz9rtqV3WjXsP3g7/hmn3hdP5v0Na/hvFe49ReitRtUF+N5WfDJEVRRb6rp0/wAmdw/Nz553YSOh/Zgza7EcBusvXNSa9q4OpNd2pnlfO4b9xnlj/i3/ALvocuffhjPy07+SSZPPsu8mppxvsJ5VQLuEAHhAMwSrADgOSYVH/9f2Q3YDsvzL7xjQAdxsgqo6m1w7SopLoeIG5lFZFOq5rQ2PqgJzzG288oAp/FJ2J4j1RKzXNljQ4Q4d0QHiNGwAlAkQ96NMhpADWD9EBOY5jiBwUZJa6p4gA3g8oPQZ5hqPKCtZaSCFQLnNJErNWG0TLSAd+yqV5GZsMp3mHVqLwCHMIIPclb7u3y5ZY93gXRTG6v3O7y7cOi4wypDA47mk709gv0nT3uwfK5sdOyU3OkGJBU9V54ZAVE0ajIMBBYZpJk8oFVGuY6W8eiBzDqbJ54QXoEEfos0aD1OwYYjl27AH41AeNRPeWb7LMuhndP8AE3YplnD7moZqFsVe51NEbrpBuOwbHqtaBgCFkRvKCO5QENKAuyBYQED67oCBCBdQaiADHdByzMbjg3UHAcRdApXE29Rw2nVOxQdUpumI4j+ZQNQXqAEdvRBY4QUguJCACDOyBjRt7oMW+3tqny/yQaX0w/8A2u9I/wCvXP8AvBBv/ZAL2627GCgjATygtBGzvPCAXGUANQEeyAggCd0FlB8Wfa0xOrifUPDcBpEuFja02sYNwKtydPHrwu/LezCPHb8zYc62eM0M99OcoZWw39puyth1viFWwb5GuLw11SdXBBC4X5cO96OSeGz0eqGI2WPY71Ouco3dPLFxbMw1ldoYbinc0jph7f3S7utb3jJ+V25zxbf8U013Hb7qLl/KNhkTEMLtRZ59uq1G0qU3k1LWnd1PEfRc310ldJ+Pv/TTO/k7P12539oHGBUzHg/TbL4/1ZlqjSsaFNnD7t7QNwOTvut5X7ni3PdOP5uW5ZfhTqhiVHpzlHDOkWAvFK+fSZeZquGeV1SvXGsUZHIHELn0HFq3k5Pq6c3Jd6w9OJ2l3XsLmjfWrzSu6Lg6jVp/Gx49B6Bcs8rlnb9Po32yR3jqP9oS1zxkl2TKtpUdUdQti/EZ0l11QA1lwHaF1mLm41h+a8yYfRfY4dil1b2lU6qtGnUc1rg7aAAVjPxHTF9O9MOjtPBsy5Zz1dYhWuXXts68q2twD41KoxgLiZ/L5liTaW6dEybimB4n1OzE9txpxGm8UqVJ2xqtDZ29QJ4WbdVqTbx+pP2ebXMuI1sfyliLsFxK7cBeU4i3qepAHBW7lW5hIvIfRPLFK2Y/MNxcY3iNi5wNSo8ii2oDv5OFwtydJ2xrHXfMuF4JeVrA1WU7hrabaFuw+YAuYRHpAC9ucnbP2ePj85Vzrr0y6squXs04dVqW7qtMUnVqJ0nZrXt3H+Irph23FMprJyPEs9Zwxi2+54pjV1XtCI8KpVc4Ee6mGpXbObxefhGO4tgNapcYPc1LS5e3SK1KS/f5Ljy5+U4sXoZrzbiOcLyhiGLO13dvbsoPqBxOvR3PvK63OdsZk1a6jkPEqXU3KN304zBU8TGLGk64y/dEy6WCRT1Hf6LlbtsjohjZsMcxHp3mARhuNNfa1aTvhZdCWggHvKvHrHKW+mc5uab63Fsfusq4j0vssC++XGXLhv3ioeDb0qmv9SFngy1xZY5fiu9Gd78pl+SVM9ZBbnbAscZgr6eGstjaXj/B/CZcARBMRMq4/wBx238W4W7z72PlLFLE9VcewujYPssDzLRcylb1W6G1A5kamjjlTly1ZyJhO7KxrPR64qZQ6s18EcS2k6vVtiCY8oJDZWOu4/w5z9Dpc+/Kz8n2UXAbJLubdr7CXKoDV7oBcfRBQ5VgEqj/0PX8Rw07CF+ZfeMbUmZ2QBWDXlunkchRQUnBtSCJP8EVlOe0CS39EFHcS3hAuhu8l3bhErPY4PBDjueEQnSGvO+3dADWkVCCdzuEaZBAbpfBQNE1A7feNkTRFMaXbndDTKo1Yq6Xjy9iEQdWq2SI+SDH06jO4QZFAtpSdy7sCqLuT41MgjzEcHhTKbmk05zRvn5S6gWd6ZbY3cW9d3Yl55K+t0fUZT5fo+fz4yvo6zuG1KbXNMtIGk+siV9Pkxnt823trJ1bwsqZTdsQ3+KA51DfkII0wRIkn1QSNyRsgsSSASpoeZi1BtzRq0XiQ9pb9CN1mwc06X16uG4hjGXKr/PaVzWptd/8uodlqDrYf4jAY27Eeq6fQECYhYB6i3c7lBNYeZCAkBtIIQA4aeEFmAEFjTsQgp7WnffbdWDmfVe3LMPs8Ta3/mNyyqXegDhKzaOh4VctubKjcDcVqbHSONLhI/itTzBmFxCgtu4lAQO0IBcYhAbXHSgsBBUwSFYMW+dFtV/wlKNL6XH/AFVex/165/3gs7HQOyoGN+UBgBvCAOTughMNJCAeyCAQgh3+iC5QUIO6CE7gIPhbqYXY39o6pbuMhuJWdL20s8Pb+C69V6xx/Z5uLCZ7t+jptvcZ4d14zzjOTqFvcHB7M2dZl27SHMDA4BgO+/qs5492uL6f7td9y47fyeKOoOeLvprY5KxLL1a3xDNV590wzGCP6q41628f3m9pWcJ5yv6LfOOM/VsdKtm+66z4fgmda9tXtMmYZVxG3r29M0mPqGj5C9pJ8wIiVvju9YfSzbnl7uX1l04f0xpNzj1kuMdxIeNaWda7xm51cfgy4Hftwpy258nbfo1njPu5i8jC8u4z1o6j3TqQqVaGIXlV9e+AltuzzFhcfTYAJz8mWUmLXT4TGPCz/wBPMwdN8Sbh+PeG2rVa+rTbSeCXUg6A4jtqW+PHux8/Q7vNev0y6RY71Me44VWpMsbWppvWucG1GMI1TB3MnYLz3myl1HTsmmqZjwLE8oY/c4TiNCrbV7SrrtxWZpLqbXeVzf0XXLHeO6zLqvqT7PObcx5kwrFcRzJeOvXWrPueHVHAAsa9suGw4gNXDDK703njNbbRaZctcYw6ve4NVZh2a231avYYq0R+I3QwMcf3d10zmmOPJ7uXeq9xaM/ZOd7F9pjlBsOr2zTUpXLmmNbQdgSumOqtafj3VTErFlfDMiYZUrYlfVqhr4hcgsp0Q4zqjeYBW7I5Vqdx0qpY1Rq4lmC8fimZbpge6u4y1taKhIZ/d8oXmx3yWy/Ru4zj8z6svEMAss25BwFmKNBZbCj4sHceGX03fxhMvk8RqYzLzXH+rnSWnkC2w7F8Nruq4ViDn0HU3nUWXLRq/QhdeLG3yxlyammrdOcNt8Rzdh9K4vKNlbU6zX1X3G7H02ndhHuF4uouUye3gxlx26/9oPp9lrLWEWtzlChbfjPNzfljwakPAc3SOzSvp4cMvHuvB328mvo4XlTG6+Xsw2GNWjzTFtXZUd/h7gj0XD1jbHo180jonVa3bgPULD80YX+DZYuy3xGgW8B7o1kfVXDCc3Blcvc/I5Pk5ZjPVdcxHEcx2eesFxjKQpvfnHDWNrtq7UnVmDSXH3Xl7ryTj5b77tePWox29mNxn1efiWY7HLWSsVydj2H6sy2F02u8UqOqjUeXagA6JC9+peXv+urP0Yn4e36POznnSpi2asj4s3C62GG3DA+tUaAx7XnZrIjZcbxzLguF9YmOXZyTX8zXs6NOC9bGXtNppi5q0LkAbD8SJXTky7ul8/S6Z6fH7vqLjPrH2Lb1G16FKqOHsa4fULx8V+SPXnNZWGCIXdgDoCuhXZNC5hABdG5Qf//R9gQafuF+ZfeH4bXNDnHzAbIMattU1A791FMoRMxJRWUa7SPDc3lBCWtkjcHsEAUoJPuiU8tGxBhER9PiTygQ3ck76m8FGmfSqa6QkQR6oBBbTJJkAoElwc/ynkoMgM/EBJgIlXUlsmZ3gIgHOe14BE/JBkMEOD+3cKh5e1zXFgBIjYpEaPn/AAf73YeKyGvpzUae4c3cLfBnrNw5MNx0zpZj39IMr2NzUdNzQZ4VYdxUbtv9F+kl7o+PyzVb1DQZhRlXwGB3QGNoJPzQG/4gRwgBwkoCYNIO6BVw0PpujmIH12Uo5Bfg5d6kWt9Gm3xNhoVj2Lm/CkHYKVQGnAPlmQAugMmTCwDmBA/iggYBuEF6hBHdBKLjuD+Xuga7cIFkgwCgMDYAFBckOj12WoNXzzYNxHL2I0NMl1NxYPcBc8vYx+m2JHEMq2D3GXsYKL55mkdK3j6G4kSoCaNoQWPRBT/5II0oGSOUAOMlagxL/a2qk/un+SlGl9Kt8Jvv/HXH8XBYnsdCnZaFAygtBRI7IKI8seqCRsgpBJQUT2QUJCCuSJ4JA/ig+GccOr7Tb2P3H7apNPuNLY/kuvVe8f6OPT+sv6t6tcKz5ivVnqo/JmJUcPqAOp3La7fENeWABrf3Y7lTL+9jE/uqfY5lzDmzLORcg43hFxgVOteUmMzJt4ZqWry4Poz8LnEQs4fz/s39Mf3VbYff4Bm/rBcYniNTFbmywak2ld1z+IGVBsz/ANMwVri/Hj/lc8vWX+Zybos4WWXuomOgk17PBnUqbxyPHfpIWb/e11z/AAtW6TdQ7rpxmejjAfVdhkH73ZUztXhpDA4H0JXXPHy1xeldVOorupeNUMeu8PZaXrKfgVxTcXMeGklhE8EBazvbHLXl6/SHq87pbcXJo4ey9/aNVn3y4qEh4pM5a0cSOy88x8t7apn3N15nPM+IY5dV6lxb1KtQ2oqnzU6DnEtYfkCvRnZ2aTT6W6FUKGDdPrN1WBcYiatYj1L3sYz+DSvBh+J6M/wtm6aYvTv8MqE1G6aGJ3Vu5o3AcKo/yC78rhxOq4lhFniOAuoutqd7XDS2m94mN52XLDJ0sc0xGwFsalGhbi2uGM0O0jT53NA/yXTLJzsZthRdcOe1pJfTZqBiCXvYA0/qXLrwQ5r4jScnG3ucMZlvxtd1aYlc0bql6W9OoKxd8pC48s3k1x35Wpfabx61p4LgmV2f86q13X9UfmYxzSGfUhe/hymHH5eLPdz0+aRTuabKd21rmMkihXjZzztAPqF4s8pndvoYZds09HGcfxXMd7TqX1d76vh07ceYtb+H5QI4XTk5fk054Y7zlLv8CxbDK1SniFlVovpgGoXNMBrhLTI2g9lxx88drWd1yR1DqA92J9LMl427+3tvEsXvPoN2hXpb/ByhzX+Pj/V0XDDieM5G6e4jhNUUcVsr02tGo7doa7kOXDCa4p+ltXkbfiONYfky8x+yzxWpXWKYtSFzQuzSDg8gFop6fy7r27+bH9ZtxjmOf843mNZYyjQvMGq2Ro12ChdRDXtYdtJG/wCqY/3ef7Jf7zD92D1h8ufsEutXnrWlq+fcAys5f+V/quH/AJv+j62wOoamDWDzy63pk/7IXk4vwx6+X8VZsmV6I5qeZ4WgGrsoJKBbnA7IP//S9Wk4nyr8y+8KoSAQOR2QYxJMSop9NrgA5qKyDBbL+UAAhocR3QXRcdUEQERkuYYkc+iIRVrFwjgt7IpludxI55RWS5keWYQLqa4GrcHhADAyeIcgymS+B3CIFzCHHuJlEHrBZqG5HMIDoua4b8KhzWhgc7gniVcfaVhYlb/e6D6bwNOkgTwV5pbM2tbjWelGKnL+b8Qy1XcRb4g417YcCRyAv1XBnLj7fE6jG7fQDKgO577BdHn2txnfugmp2lA4GQCUAnlAQ7AII5oIg94lSjmXVmwe7C6eM0W/jYbVbW250tKQbrlzEG4nhNrdNg+LSpvLhvuRut/QeuQJkeiyCaD+ZBeraEAoCBgGOUDo8ohAl5MbchBbXugSEDADIdCsGHiVHxrWrT7uDh+oWMhz7pXWNqcYwR2xsrt7mT+7U3W8fQ6c0nuoLaSggdugt24lALSeEBh3ZBZHdagwMQd/Vqo7aXfyKlGmdKXThF9H/Xq/81iex0IExutCHbhBJKoiCpMx2QF2QCFBDyEFd0EQCPy/P/NUfC3UmcE+0fWunAtaMVtKvpLXeHJH6rp1Hm46ceDxMt/q6N+wM+XvXLPltkvFKeFOurUVq9Sq3W2rRqtEfI+6mX97KxJ/DsVa49juccHyXkjHcKuMLtLLFfuV1mNhil41nOnwvQvKzjNd/wCzf0x/djPy+/LNTrbgzr2piH+rKVZtzWdrr6akO3PtwVri/Hj/AJXPL1l/mcs6Vu8Tp51PpNEOfhlF23/e8K+PvLXXP8LjhLSAT8JEqctv0a4/T18uZZxjNd6/DcDomve06Tq4pA+ZzGcho7lcs896awx97Kx3AcYy1dsscetH2d09greA86XhjtgXDsV1viMeN+2+4D0pq4zkG+xt7KrcwVC2thNu3cV7Rk+K9o76QvPjlbk6ZSad+wW2bhuA4LhobDrGzs3uj94jxDP+2FMJe5c7O17GAWGF4S6jdYONVjiR+/uYNx95Y97av8125PLjx+3ZsPrPrWdKoGeEYB0Bc8I71pGc6PhYgyoI1XLmuMdtO3811yjnXjW1zVsqFWswxUY9rpI/JqOkfR2pXhu9uXL5nhxjpbjdtTzDnPF6zw+tUvHNoCeSHlzwPZwbC3qd3kwvhxzOmP3eOdQbvE8yWz61Nt0GVbNx0FlAGGNaD7Fa6mamo1xYS5brv+fMh9NLLpHaYi2lWZTsh97pUaTgarH12wNYG8Bc+m4vzXqNT0+XsAr4RbYvbXGM031MIFQPrMaZeWA7R7rjz4a3p14bPG30n1kzTkTGun9lYYVefcsTuLalcUxoBqVGUthTqHkGCvTxSTi8uXLN8ksc5xWKnQTDnu3fTxRxDT8LRoPB91x4J/Dyn5HJd8+P9W45UpYnddIsHp4dX8C/bi9P7vWH5S8iCuc12Zfs3yfT9Wz1b1uWsx4vQ6q3FLEL28sQ+yujT1CGyAwehXb+bi/yOMadn/NmKYxgeVsJr4M+ws6ddr7W5I8tSmPLE+/K1LJx57/JLN8uGM97ed1ie1+esDtQN6Vjaj13A4Vyxv8AZda+u/6fmuHnqt/TWv6vrnAhGC2G0f1ent/6QvFw+cJY9fL+Os7aPdeiOYQdjK0FOPooK1IFPdBQf//T9I7BrmlfmX3k8Ul3CBXiNDzq78IrJYxwAcDt6IrIedbRtEKDHLo8vogbSaSZBgwgcw1HAhzvkgBzQwy4TPdAbJ/KqyyXOlv95AqpUIaNX5FGka1pcDPKB7g5m7HBUSXncOHG4QDTLhs4QD6IlZDQwfD80Qb2Pe0EK4zdZy9BcDHhnkeqxlj5bx9ObZyoVsGxezzBbSyrZ1QXOb+6Tv8AwXu4OTVjw82O30Jl7E6OK4bbX1E66dxTY4H0cV9+4Pk609jSuV8KnCA27tj0QUDvCAxAEkwguQQSDPqtQeJmjD6eJYPdWThPjUi0j6LlRpvSXEKj8ErYXXP9Zw2s6i4f3Qdl1nodJaZbssi5dqagI8oLHdAImdkDGkgwUEdEbIIDACA21J2QBXEsJ52lByzBi7Cep19aTpoYjQFZo/vjlB1XVs1AzsgoN3CAyIEIFk6TKAm+qCy+QVqDAxHa1rH+67+RUo0rpK7/AFRff+Or/wA1j6jopctCtSC9S0IgiCwgHsfmpROyghMIK1SgobSStY+0r4r+1fhtTCOpWG5gpt0svbanV1f/AHLd0/8ABdaw27H6easT6kZPxvJWIjDLrOGC0qNzdndhFu0F23quVR5uNZjxrDMtjpHmO0uXV8OxuiL/ADPbk+C22fVFVtVrgPK8+q3l/N+sYbHTyZaZWzvnXALG+fiNHMeW3XjX16ni1DUpTqBPcbKYe8f0x0OJdDB96OdsvuMvxHA67ms9atuS8/pKn1b+jjbtTYBHGxXSmPt1roDn7A8j5xp3GMWVN/30toMxGo4t+6tJ3dHcHuvLcd5R6L+GsLrdnfAc+5sqY3gNrVt6jGm3rue/Uyr4RhlVoPEjsvZyz5Xg458zqGQ8Qw6jl/LDGHEq7m2z6TqFNsMZTrPLaul3cP2B9F5sY9GbZ8IdfWVG+tMVeDcWlS500y4O8O3bo8NriO7WiFpmNju7qxwSphtGzIfbaTWowdtFcNd/vKX03HZcLuqd3YUarCJc1moN7bLlxuteBjzG3eKU6LgCyizzu/vO/wDbdemuVa2+0D7G5qNGlviEEckkg8f7RKcHy2pcXy7g1G5y91OucGsLU3rBdi7Zb1HlmvT5uI4IlZ5Mvm2xMXh9acKxahmupme/tGYdQxeq59OjSdq01GAfwW7e75nS3Xhoz8y49Vo17ete1qlG6aKdam5xcwtbwI+Sn3h2beWeYcyBMhgOw7Ays53eGX7LMdGPr1q5pfeXueaILWajIDT2S3eE/SOnG6/mIC06GYBbVBDrrEKlWPUNaR/mp08+Tkv56ef/APNG45cw3E7zpNgGHYXU8DEL3FWm2f6Fp2K4Y+eO3828/WP6Zbe9RxSjhWM4/lzqRWbiuZKtBlLDbrQC1/iCWsHoQV6vWXH+mDE/m/XJrOP4nmbFcfypkbH8MbZCxdTfTeB8TZj/ACXHqJrpbl/ijpxf+bxy/KvFz0/9t9ZBZUBqbb1KFo0fKAf5r3dbn91wYz/FjJ/rHHppvky/zW/7vsi1YKNrRogR4dNjI/wtAXzenw7OPHH8o9nLd52oXbru5hLkAFyBZcgU92yD/9T0Kfh6A2dwvzL7ywROyBNVrdYd39EVmUqoLAAijc90KBL9htuSgZRJeQJiBuga4QNQ2IQC94cBvugbR4VZZH9k+DuPVAiq9oJ7g9lGkY4Pe3Tz6IHuOkgO5HPyVGNWLhU8VhhvEIMhlUOa0k/oiVm0QxzT6kIhrXbBoPCsuqlRzQ8z+pS3Y13NOHC/tK1AtBkHkT2TG6sZyx2zuiWOOqYbcYBdOIucPqENDjuW9l+mw5e58blmnYNW0hXkcMVtOoSsT01V03EODT3VRenSTv3QWI4O6C2s0mfXstQKrsDxpIkHY/JcshyrAIwHqPf4dOm3xWl47G8DxKZ3A9yus9DqtMkwZ0iFkHKAkAP8QRoPJ3QOaCPi57wggmdkEBdJBGyBbiC7yk/JA5kQghMtO6DlWdHOwnOuXsYBApuqut6hHpVECUHU6ZLwCN2mCD7IDY6OUBtO6AnEwgAiWwgjTtCAjGy1BgYqYs6x/uO/kpRpHSR84Vfz/wBer/zWJ7HRZBWhEFrQiAhwgGSgF08qUW1226gskIBkdkFGY2WsfaV86/a8y8MQydhmYWM1VMMujSrQNxSriP02XWsOfZfxDFswdHcu4vgV192zLkzFWWzaxM6LW7d4bS4/uyd1yqN2uq+bcn3GacjY3ZVM3Ynmqz/adreWrAPhZpLXA7NDCdit5fT9awxMBscq4Bmfp7jWGYrVuMTxy1rYJjNtc1TVr0jUo7Ncx5OnzTssT+a/ldDkeQmtyb17fg92Q20q311hVQDYCnc6msn2Ihav4m/o5rnHBquXs04xgtVpbUs7utTIPprJb/Ahbpi8GJ5+q54+3ovpZceBs6QdcTEeo91vnz8OPFj5d76a3F3mLJ2H4HaYq9+IWl3WpfsafCcW1hrGiqIgS3hccLuJye3QrLALzEbzHLuqxllSuMHbVFoyprex7AW1HvM7uJG57rTMaLmOxzn05ucIo4+52IZeu30TY4w2YbRcA/Q/0iYCl9OkfTuC4taMwWwt8PrNddYrSFU1J2awCdvRcuN0q8XvKbLW6pW7g99wBpq/nGo6Nj8gvTXKtffeOouo0Z/D8QPuPWC0RCzne0tcWz9a4jZ9bcGzHhdsH218adrb0GEAkMYQ8kHj4gul4+7HbOOTM6rUMCt8KIz9UYy5osqOsLOhvUNxoc1oL/qDCvFj/C/qzlfmaj0C6Y5bzrXuL2+vC67t2up3GHPaNOlwIa4H2XjnnJ6O7Ucz6iZTsMl5luMBw7EBiP3efEc0QKYdvpdPcTyvbnx9uM/Vzxz7ttdZht2adCu6i77pXf4VOqfgLm7xPqvHx3fHl+m3Xjda6tNdhuV8kZbb5KlO1NzWp+9U7L09NP8Awty/N5r/AH0dKtsv4jd4BkTAMFvhh+JMZUxIVHcgAbGO68vD54cf1ysduTxcp+U2xbjGbW5wLEMKuWU8V6n08QNNr2tBc+pTMNc08hkDhenk/FP0mnGfT9ZszBsZxjN3UZmI5lw39n3GXbNz7qmedTGyHfJW4d+HHxfrXTj9Z5/lGk9MKT829XX4pU87HXTq5I32DjC8/wAXz7vu8Z/LlIvw+byy/a19lFw3j6SutWXfkokfVRSnvjgoB1AhAtzkCnu2KD//1ci3LnEuI3PK/Mvv6Oa0gE90NKHmcJ5PKKewFjtJG3KA3VpEDlQJdUDXAd/dBkUmkCR3QNc4yA4fVBjOjUCPVBlsaWs1BVk9zwWgP5PKBFQAGRuPdRpVIfitcNgUGa6dOo7kqhDtLhojdBVEMAMjjlErKMU2seJ0u/miG05gvHf1U1sF4hBLfy9/Vak0aJu262E9iIKqtBw+/OVOoNveVJZY4jpo1fQP/Kvp9Jlv2+b1PHI+jbaqKlLUCILdQX1eSeHzMfFPaQOOFyjoIjhwVTS3EESOUFDflEFMcLUVHtLmEtMOhZsNOUdR6FTCsWwfMNKGvtbkCq/t4dTZ0/qkv0NOmWFRle0p1NyCA5pPcHhasRlAAn2XO0FAWhGhsEe/KAm7bILOx2QDuTM/NBQgmRz3QEN+DA90EfDQWuPKDmvVi3d+xaeIME1LOvTrAj+6d0G+4Hd077DLO5pk6KtEOB9wFrQ9AAFNAxsmhA4nYpoTgpoDEFNCw4oMDFH/ANTrzxod/JSjS+kwH7IviO97X/msT2OiNaOV00CHMJoUTCCxuJQWDsgFxjhBTjsggAhNCEBNCojdNCiTG3Kej21jqFlyjm3JuN5fdBqXtu7wSeRUY3U0j3kBdMfLGc0+PPs/34o5ixvpzir/AAaOYLWtZtD9gy8oTp54cCNljKaZx8uh5Qus5ZXw+76oY/e1cZu8BrVcu4lgdXy1GWtOoA17HjvMfNLd6/S7YeJmu8ypYZdu83Yvhb8D6lMxuni1nb1g4VjQqVdQ0AbaAw7+6sniz87sah9oG2NhnrB8/YQNNtj9ra4jRezg1KMF2477CVrk8Tu+q7ry+u1tTxHF8Gz7YNH3DNlhTuXOG4bdUWinVbHrIBTjvdPLeLK6Y9BL7qRle5xxtxUw2tSrsFB1Zn4VahB1lvuCvPhllcnXbkeI2TLDGLnD6b/HpW9c0G1BI8UNdDvl6LXPjlUxsx9PrHCOl2UMAwS0ZQxN9lbZqbQ+7XjKgfXsMTa2WAOEQHSsYY2QsmVaX1Cv7/pDjtjlSyum4liN5YtoYxeOcS4ipUd5h9HSvTnJMdxww85adT6j4pb0um1LLePt8d5oUnWt2Rq1fgy3WPyv3WOKTOXa81uFmnN+mmcKjaWHYZiT3219YtdSdSrS13huPlLQeQuU8enbG2+3cbCtQxBtI03BwgNZvtLfJv8AUSt7tXLGH22GMvr6nSeNPiODif7vmj/8YV554mmccJfb5v8AtH3WJ5fzfg1K2rupXdlSNzbVGmCHVIHb2AXt47fu/LxXKzPUY2R8SHVjLGNZGzK41scpNfiWDYm8TVNZm7mF3pHZePDl89n0erk45PMc2wXNubOn7r7DcJqmwu6zx49QfGWsMggniVvLj7PMXHWU8vMxPMdfHswux7FaDa9Su5rrymPKKgZAd9SAryc1ykn5McPHJ3fs+qr7L+QcZyDgOG4XaUrbELyvRuKFkxwNRr3xqJ9oBXHgwxm8L/N/y6y2cdy+rjGfbj+l3VunhVjL7eyqUcPt2jcAUYbx8+V6s5OHj+6np5OnuXLvPL3HVc2WOG3GMYjXp4w/D7nJ2HU6NsKLgx5rObLtu49l4eGXDCYX+W7n7vVy3uts9Wary62L4dg9HAM2ZQw4YzeYcz75j900RUeHthzT7zuvTZ3Td/E81t7pr8Mmg4lmqt/QrM+fbqj92v8AMRFnYj8zaTRBXo6b+8nJ/hdc8u3jyxnvJm/Zby65rb7MNRkgjwqTj++V8Pqsrnz9v8u9vZ0mE4uO5fzWWPpJxif4r6WXt5sPwwh/Ky2Q/hBTTsgFzkCKj4CD/9bLouIHZfmX6Bb3weUASWvDhuEGXTeCZcd/RBKoaBLeVBjPI1iQZQZ1N/kA90Bve4eU8IFENdxz2QPt3OcNJMgdu6rJ5LS4au3IQKrnTvw1RpVN8jyfRBktqTDXd1Qt/kqGEC6T/wAQtIO/dEr0dLmhg2LY+iIsOLTvuPZILDRJc7g9u60BPPPlHARWk58w03WGOumN/rFB3isPcOHEL09Pnp5+fHbpvTfHxmHLFldOeTcMYGVgezm7Qf0X28cu6Pj5Y6reNyZ4HolQxhJ2PCCO244RKsHgIghsd1qLBn4YBUqtP6hYU3Fcu31vH4opmpTPuzcfyWPqUrpvi9XFsr2VWu78ek3wqo9CzaCt7ZbjTMCDypoEDJiUFGQ6EB9/ZAQIIjugWSQYQQHeBz3QNkNEAIBfDxuN/ZBrWdbFt9l++tw2TUpu0zwCAqPO6W37rzKlo1x1VLcGg4f3mFUb2zcwgudigpp3QEUFHmEAk6VR5+LEfcq8/uO/ks0aT0gcTg18exvq/wDNYnsdKGwldBGneUFO5hBbT5UFdpQUTIQUXCEBjZBCUAnhBW/ZUBEeUAbGWkrXpL5fDPXbL15026r08x4Sw0bXEKzcTsnDYCuwzVZ/Ip7c/TpeMYNWz3jmX8VwPGXYXlDP1vTq4oKcOpnErJurQ4HZusjzLDDHzDmbOGI3t3j+PZUtMWw2xFxlQAFr3vuy4sbVDncCeF0g0y9w68zl0Qv8Jvrd1LNnTu6e51u9pbVZavdJbB3IE7eyzld+GpGvZXaOoPR3GsomH4/lWp+18Hb/ANI62dtXY3vsN1mXt8L6eP0/625lyHgWKYPaV6lTxmsbhYedTLZ7HAkweZEhb47239zbnuM4vd43idzjF21jbu7qGrVbSaGU9bjJIA4Xbm5JoZdlmXMFOjTwuheV6tqLqnd06Ooud41M+WPkF58894NY+31Ni+HYH1NsquP4fYurYnQwdttc169JzSL3yhrZI5BG6493d4Xt7fL0Me/ZWe8iYZXo1zSuLaxdb1y3vUsyGVGkHkAg8/NdZO2VizvrZc3dJ8uZrytheImoMLx6wtKfgYpS8k6WjT4nEg8BYwxtdrdOcZBzbf4fjVxlLMFMUsToDXSqNM0304gub35Xa+HHLLbt1low94xLEH+Eyuw/dKZ+KSDH/wCICxw/Pk1le2PkDrZiWJZ2ztotqRuLrD7bRXazfSGFzyZ9A1ezqMpjjpwwx3dt36K5Vr4pkhmMYLd07DFLG6q/fq9Yf/DvYabtJHs6V8fgxt5NvVy5Tscg6s32EYjnW8/YjxXs7dlO3bXaAPFfSEOO3uvsc2U08nBvbRi0SOziQB6gLwT8Uv0eu2Tx+btPSQ1sOw/G8+4tVe5mFWzqdl4hMCqRpY1oO0pnd8ss9HJe3HS+h+HvxTNV/m7EW6mWDal5Xcf33S4QV06n+Jz4SfqY64uO17r8dyJe3VjmK9o3D8Zu8Sc/FqWl5aLZrvK54iIhd89Z3vn8v/Dy4fJLhf5v+Wbi17mHL+L3+D4VhdO3wPOjwbNoEOp0idiB2BC5ZZby+8+np1mGsfu/6tc6z3rG3WBdP8LMW2FU2mu2nvqru5ld5l2dPfzZk7+f9H0r0qwAZZyZYWRaGV6oFapHdzhIXyemx7pbfb1c+Ws5J6bk98klerH0ZzWVJcZWmCnHaCgAuACBT6gQYtR8nY7IP//XyeHgDhfmX39qc07koq6G5g7lEZLWimdR3Pogp7zGw2lRS3jUQQEGTSnYem6BtfZocOe6DGAJMg7hBnM0tLSzmNz2VZVVLnNNUBArUXjzbhRTKQDII4P8EVkuYCCZiOFRj1HEQe/CBtuG1ZnYjhErMoVNVJ7HczEogGPgEH8vdWA3uOzm7xvAVAioH8iCisO/tm3NCpSfBBG4TG6Zzm2v9J8UdgebL7LNydNrdk17Vp2837oX3ekzl8bfJ5sdO903AtBmQe69OU815T2wJ9FlUcZ4RKgO4RFvHBViwQBkJVIv7dlek5h3DhB+q50cw6dVDhOY8fy1UdDW1PvVu07eVxgwrGXVGkRM7LrAWiCsi2iSZQUSZQWCQZJQQEOO+yCg0NeTPKBpQU6dJjlB517Tc+1ex2+oEH6hBzzpTcGyvMbwIne2ujUZP7ryqOqh0GRx6oGDhBQ5QWTugqdp7oKdBb7+io8vGHOFnX/7t38lmjTOj5nAr0/9vr/zWZPI6TJhbBNIAQU4+ZBAQBugEnZBAUEd2AQGgFxgIK1diguQCrPaUEwZ/mmS4uSfaGyA3O+Qq9zZ09WNYITd2BG5cHCHt+olXG/m5Zz8nz70QxmlmbAcX6RYtcG0ubwOvcuXodpqUL6lyxvpJEqMvayzkvPmJdL82WFljunHMPv3XGJ4NXB8UV7U621Gk8TEytxHqZVt6eAPwPqbiOM/tfCs7AYNmuhWcA6nVqN0Ax30ERK54S/eX8nSWacu03vQ3rM6lXBfhra8SR5K+G3Ugj0I0u/gs8v4vDNa31fyczJmc7q1sRrwTEWjEMJqjdrra48wAPctJgrvZLpcf1aH5dMiduZ9ly5u2/V17WRh14/DsQtL9gl1tVZWjvDHSVJjO1nT9C7LO2VL7IdPGadelRp3dqKte2oNDagquZBAAG79tlz6efP5debUwcB6f41cYxXzJgtxbutabfvZtqTm6S2nXpy0kep07r3dR2+NOPT+q2bM2Z809PclW2F445zrdlF1Gi6r+KD4jfw3Mqdw0mS124Vwwc8sr9WmZepOs8bwrGrr+sY1jAqOe9x1AUj5mNZ8l5ea36N4Tft1zM+ZTgeB1Mcxaprrvd4eHYeN3Pruot2aPmSu3T49uPdfDn1F7tTDz+zQOmGTr51XEa2JW7auN4k3W88jwryRDj2IAIXi5eS5Zb+jtxTtx+bxWu9SM3YXkbJVPIOUxUscWvKtRuMky2ozS7YfIhfV4+GYYbvh5eTK3LX0ap0Kybl7PWO1sIx62eadJouKNyx0aC0+bV/iXyeXPLLLWPl9DixxmPmj62dP8Py7nLw8HvaNRl2WMo4bT/taI2jUPderOfd8OWWXi68PHhbnyyT82V1IqNyrkrBenlkf67dht9imn4tVT4WuWui479xlycnyz6ba6u93LMcfNbhgmE3WWen+HZfwwAZnzXVaQHfF4DY5HovP01twy5fy9OnL81nH9fyehVOM4pXzRlivhFvbYmy1oW9PEaAb4VJtIbudtyV3m8fknrJw3v58vFxeXlHF8Ur0bvPmcq4qty3Qda4WeKb3s8o0juVOPG5ZfdY+fr/o1ln24/e3x9P9Wp9K8EvuonUWpjV/L2mqbqu87t0t3hc+s5Jc+3Dzi9PSYXHj7svGT7KY1rabNEBrAAxo7AcLlxy45a+iY+d3L2jie5Xpykl8Jvfkl1WNlkIfUkoFuqQEGO+oe26DGfUdJQf/0M1z2taNvMvzL7cJqay6fyo6RGktfLeCiMrVuNSAnOYfKOSiwqC14n4UVlDcaxwgBxc/5DlAtrX6pHCIzKRdHCIYHkNc0jyoFktMQIaswGxupp0cDhbgZBLBv5hyEqkPd4hDYgjZRT6LS2D6KxGRSYWkk91UGA0tcPzSgFzv3OBygp4Y8Ah0FAut+G7bfUk9LK5zmxlTBMbssxWssda1WuqEfuuO69nRZayeHqMdvofBMSoYphlteUXfh1mtLPk4L7du6+XZp6wJG3YbKIIGTI4UoYQCJHZQWfMyO4QDTLhIdygJ4mmP1/RS+hyTM85f6hYRjTfJb3jnW1yfXVu3+KQdWt6rKjA4fCW6luB+pBNbQghOqCgkTugkQUAumdkBEOQFDoKBFYRTM8oOUYaf2L1Ur0T5bfE6BeP8VNZHXWfA1A3UtCakAl26AmmQUA90HmY2Ysa/+B38ig0vo0ZwC+/8fX/mg6Tq4CAkE/MgpyANSAmmd0EQQPG8oB3MzwEEbuJQEeyATyEAOY2ozSWh0hwqsd8Jag+GuuOQ8Q6WZ+o5ry+HUMJvq7cQsqzNhRug6XUfqd49Ftysbri+JtzZg+FdYst1q1BjTTseoWF2ro1UAdLqhYN9+Z9FHOx5v7B6Y3HUOva2NzVrdMrmydWNwA91pZYnUHkcI2EQCfdblY15YmecPo9UemrcZwuo26zTkd9SxxBzN33Vi0+SsPYAArFdsWvYUP8AlW6TVcFJ8TN2SWmvh2retc4fHnZ6nSd1cfEM53WNR6M5fwHM+drbB8zU2jCatN5rl7xSNIs9z3JXHs3XeXUen1/y1lnLmd69rlm6a+jWpsfXsKbY8EaQBv3kbr1ZYawefHL5ne/s0YlaZryBd4PiVvSq3eDvdQpPAB10HiWO+YOy8OF+Z6eWfK1+rbVMG6oXNJrdFDELKuYAga6TSD/kvRyOfDdNn6tOsryhgOHX0V7a6qUbeDvDqlLQf4ldpyXtceXzk57mbpP1H6dYrZ4vlonH8CsJNGkd69FkSdXqF4Jnlcna+MXiZavcQ6j9QmX+LXFavhuGUxVrU3eQW9Vm4AZ7PEfJevPk1Pu/z8uXTYaytduxVuL5bpXfUTDLplqLa301bWsAKdYETt6EOBA+axwcPfl2fk11Gfl8cZ6zjf57zLc5jxGm2lXrkDw2CAGt2C9nPy93yuOHny83Ccw43gXiOwi8qWlSryaRjVp33Xgx/h3b2XHcda6fitnHM9zn/NJacPwag2vc1XcVHUW+Ru/cldcr/asscPpHnl+53l9Ywsp2V11S6kV8bv5GGisbmvPw07emZA+gCz1HP35Tpp6jWM7MPv77rd8wvGYsRr5/wzGqdhb5fuGYdhdrqA8moNLl3knHj91PTnxbzn3t9sHF6eYaGb35cyxjD8Q/pHSpvxiu3cNc7cgEcCFz5Mvuse3+a+lxx+9y7v5Z7eH1Xx2hbfcenOXHzY4eGi7fS3Na4dyD9V2uX9m4u7/8mX+vn2zhj/aOXt/kx/08eneOieSmZUyvSuLmnpxK9HiVHd2j91fI4sX0ssnTGVOY47L3PPkF9TZEjGqVmt0z3RSqlQF2yBNR/CBLqiDFfUgyg//RyC8O5X5l9uA1H12R0hlMsqgjgtRGQ0D6gIEmdRPEIsG0zyinsdp549EFueIMIApVSSYG4QOt6hcXayR7IyYSYc1Atus+UrMGTSOghnotwF4nmj+KVSXluswJ+SinsDgyWj9VYhjdenU489lUGwxJQEHggtAEnaUCyHU3SRIQMcynULSQQFZ6Y213NWHC/wANr0tIcXtMAieFnhz1klx29Lonjz6+F1cAu3TcYW8sY1x8xY7Yc+i/S4Xcj4vLNZ393YacgaTy3ZacxtESpRUkOKgsOOpAcy4IDcfLp9QpfQ5z1awsXWX6l4wHx7AtrUyOZYZ2P0SDZMp4lTxXA7K8Y6WV6TS4+jtMEfqukHvFu8g7KBdQk7e6BrRAAHCAwdt0FyOUFbHcID7IKB5QKeZYe5Qcoz81uGZqwHFxsG3It6jv7r+VkdXtHl9JjpmQgc33WhDygMwEAF4mEFnhB5GPmLG4/wC7d/JBpXRgxgd8Advv1f8AmEHTD29UBtcDsgJxCBeoIBHKCygiC2wOUEeRCAWkaUBE8IAcUE//AOINW6gZJw3qDle7y5iFMTWDn2lQ7upV2jZ09lpHxVkjMOKdFc/3uXcz0tWEV3G0xy2eJZUtz5RVaDsdt1XOx0HHMSHRmxxfB7KxpYzkDO7XV8Fvj8FCpXaGim9392VnbGjsJxLGMm43glX+jYbh9jhlO3zhWtoNvd4fceVlyANjpB83yWorRMzYdddD+qVnmHBnG4yzih+9YbUH9lXsrgy5hI22aeFMmpXg9Y8pWmA43RzTlpzm5QzVT++4bWYfLTquHnpOI9D2VxW3bnV/iF7iNwbu/rOq3LmtYajyXOhg0jcrv37mmO3V2+lfsfXjqeIZjsAQA+jSe0emnVv+q+XvWb22bxdH6gYbTtM2YNfFwZ4lxUt3uI+OpcUXSB/dA/ivd7eTDw0DqnU1U8Ls3vcLqnU+80i0mR4TgQf4Ltbjpm+cnUcN6n3DcIol7re5c2iI8aoKFcjTvqDtlw4JjcnXl8YuW9IKmH4lmPMV6+iyldXlw97jTcHhrKlQyZHI+GF5up8cvd9J4b4/GL2PtT40MFyZhmWLZ8vxCs01ANvJTG+3oSV9jpp2/wAV8/ku8nzFk3JGMZ5va+H4LBu6DPGDHTDtH5V8iZb5q9vHhrFjYhlTFcJzEcuX1GMSY5ratJkktNQ7K9V+UbuWnVM/VmZMynh3TTCjOLX5Zc4yWcyQCxm38l7enx+447nfrPDzZ43kske/Tt6nTLp3TtLWkauZ8xCazaY1VKdsfi43Gy8HRYXHky5M/rvTXWX72Tjw+jxb6nkrDIb+zburhN7Yhls10hlW/cPMY9ivdwaxyuWfqs9Rl8mOGHuPWoCh0kyRUxm5Pi5txmnosaTzL6NJwgGTvsFy6bG8ud5c/WPr+jfUZTjwnFh/N7/q8/ofkG5zTjb8zYu0vs7eoa9RzhOqo4z353Xi6nlvLl3vZwcc4sex9XMimwUWjS0bADYBdcMXHuUHQF2qIXjSpBjPhwkoEFwb3QJqP7zsgx3VN+UGPUqcxyg//9IqmxX5l93SNJ0kIorVxDiEGZMvJ9AEAEzuoKo+appPCLs+oANggHhsooaJh5hBk0Pj+arJzgNTkCg4ueQe3CzA+i4l0nkLcBPGxd3ClVjVCWP2PuibZlNxNMEpstMIgj3TabNotDiQU2bB6n04TaqLyWSeQmwbiX02kn9FZdeGNF3DGua7UPhbAWJNXbU8NCyjWqWHUzw7U6GXVJ4rN7HQJBX3+mzuUkr5HU4yW19H0CXNaXGSQJK9efh5cPMPJOpSgASHfNQESgjHEmUDCJAPdB42ZaNO4wq4o1RqY5jmke0FY2NJ6O1ahy14LjLKVzVYyezWO2XXDyOlMcRspsW8agJTYJh2j0WdroRWpV0PSC35rSVQ4jsFEECSClAtJIMqbC+RHumxzDrG0DAaNUD8SnWplju8lwWZR0LBXuq4fbPcdzTaTHqQtSD06bieVQZ7FBCZMdoQJjzoGj07KjxMwvIw26PcU3fyV0NL6Kk/sS9Hre1z/ELnvyOoH+S0IzZ/zQEdzCBfqgtvqgsnYlBTTIQCXHVCAXuP8UBN4QWUAu+IBBcoAePTYkQSluh8zfa4y5hJwTDMztoBmMNr/dXVm7a6REw4d47K43bnWqdGXHPHS/NeUMy/1zB8Jom5w4O/taFQAnyP3IGymXjPGfmzfGNrSMiZkzBmO7tsqYhidwMIZZ3FmW0n6Kjrbcim5xBloWeozvHxzKe96WTeWv8A9dtxp025m+ztizcam5qZWvDQwes7+1p0mvIDS7uIW5dyUuMkjw8hk5n6I5ywXGPx7LAQy9wofnoVSN9Lt/Ke4UpI5jkbA7HMGLV7LENZostn1W+G7SdVOCN4K4553CbjvjjMvFfQf2dsNtsD6l4xh9jqFu7DqdU6zqdqdDjuvPhe67re/o6h1YOqvbXBAL7W5tRQHZprVBqI94EL6cm48Wd7cpI53ma2pYhmiiLkahb27XUwNt3GDK45Tw3J8ztWKYdhzcAtmOs6FT+quYHPptLgBTmQY5TpMJcnTqPGLj2QcGw6wx/LN/Z0RRrYj94t7xrdmVGNIeCW+srny+ccp/8AsY/Sfo0b7XNeq7NOFW5d+G22cQPQl3/svp91nSyfp/y8eOMvN2/RqX2fsx4lgmcGW9iaYZdwyoXs1OAI/Key8OHHPvMb+eo9mfJccLr6NrypUGZermL41i1NlW9thWqMhsMLqLTp1DvC11uEx6jHCeq82GVz4rnfbWenzBm7qu+5x0m4qm5c70HkJgRvst/Fc7Mpxz1i9PRecN/mzc3Zvxyw6iYziVvWaKtvqs6NJzQ6k2iRBAafkunU4S44z9JXi6HO4ZZWfnY2HKFU5szfhdtjTW1bbDrYVrai0aWCo6XEkd15/iePfxce/rHo6H5ebky+srnOesUvcx9QbqhilTxKFCv4FCk3ytYwGIaOy9vxK/d9uGPrU/7PP0GPdblfdr7GybhVjg2WrC0w+kKVHw2udHLiR+Y918bjnfbb9H088rM5h9LHsOK9UmnHQDsFapbnGCjO2M5xhGoSSSCCpauiahMQmzTFqFJUrGqEqpt//9k=";
    exports.default = image_base64;
    cc._RF.pop();
  }, {} ],
  th_TH_tut: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c9f3eJaDlJCYblINDTqM1DA", "th_TH_tut");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var tutor = '\u0e40\u0e01\u0e21\u0e19\u0e35\u0e49\u0e21\u0e35\u0e44\u0e27\u0e49\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e35\u0e48\u0e04\u0e19 \u0e43\u0e0a\u0e49\u0e2a\u0e33\u0e23\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e21\u0e32\u0e15\u0e23\u0e10\u0e32\u0e19 52 \u0e43\u0e1a \u0e44\u0e21\u0e48\u0e21\u0e35\u0e42\u0e08\u0e4a\u0e01\u0e40\u0e01\u0e2d\u0e23\u0e4c\u0e41\u0e25\u0e30\u0e44\u0e21\u0e48\u0e21\u0e35\u0e44\u0e27\u0e25\u0e14\u0e4c\u0e01\u0e32\u0e23\u0e4c\u0e14 \u0e40\u0e1b\u0e47\u0e19\u0e44\u0e1b\u0e44\u0e14\u0e49\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e2d\u0e07\u0e2b\u0e23\u0e37\u0e2d\u0e2a\u0e32\u0e21\u0e04\u0e23\u0e31\u0e49\u0e07 \u0e19\u0e2d\u0e01\u0e08\u0e32\u0e01\u0e19\u0e35\u0e49\u0e22\u0e31\u0e07\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e44\u0e14\u0e49\u0e42\u0e14\u0e22\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e21\u0e32\u0e01\u0e01\u0e27\u0e48\u0e32\u0e2a\u0e35\u0e48\u0e04\u0e19\u0e42\u0e14\u0e22\u0e43\u0e0a\u0e49\u0e0b\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e4c\u0e14 52 \u0e43\u0e1a\u0e2a\u0e2d\u0e07\u0e0a\u0e38\u0e14\u0e2a\u0e31\u0e1a\u0e01\u0e31\u0e19\n\n\u0e42\u0e14\u0e22\u0e1b\u0e01\u0e15\u0e34\u0e40\u0e01\u0e21\u0e08\u0e30\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48\u0e41\u0e25\u0e30\u0e40\u0e25\u0e48\u0e19\u0e15\u0e32\u0e21\u0e40\u0e02\u0e47\u0e21\u0e19\u0e32\u0e2c\u0e34\u0e01\u0e32 \u0e41\u0e15\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e17\u0e27\u0e19\u0e40\u0e02\u0e47\u0e21\u0e19\u0e32\u0e2c\u0e34\u0e01\u0e32\u0e41\u0e17\u0e19\u0e44\u0e14\u0e49\u0e2b\u0e32\u0e01\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e15\u0e01\u0e25\u0e07\u0e25\u0e48\u0e27\u0e07\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48\u0e08\u0e30\u0e17\u0e33\u0e40\u0e0a\u0e48\u0e19\u0e19\u0e31\u0e49\u0e19\n\n\u0e01\u0e32\u0e23\u0e08\u0e31\u0e14\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e04\u0e37\u0e2d\u0e2a\u0e2d\u0e07 (\u0e2a\u0e39\u0e07\u0e2a\u0e38\u0e14), \u0e40\u0e2d\u0e0b, \u0e04\u0e34\u0e07, \u0e04\u0e27\u0e35\u0e19, \u0e41\u0e08\u0e47\u0e04, \u0e2a\u0e34\u0e1a, \u0e40\u0e01\u0e49\u0e32, \u0e41\u0e1b\u0e14, \u0e40\u0e08\u0e47\u0e14, \u0e2b\u0e01, \u0e2b\u0e49\u0e32, \u0e2a\u0e35\u0e48, \u0e2a\u0e32\u0e21 (\u0e15\u0e48\u0e33\u0e2a\u0e38\u0e14)\n\n\u0e20\u0e32\u0e22\u0e43\u0e19\u0e41\u0e15\u0e48\u0e25\u0e30\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e22\u0e31\u0e07\u0e21\u0e35\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e0a\u0e38\u0e14: Hearts (\u0e2a\u0e39\u0e07\u0e2a\u0e38\u0e14), Diamonds, Clubs, Spades (\u0e15\u0e48\u0e33\u0e2a\u0e38\u0e14)\n\n\u0e14\u0e31\u0e07\u0e19\u0e31\u0e49\u0e19\u0e44\u0e1e\u0e48 3 \u0e42\u0e1e\u0e14\u0e33\u0e08\u0e36\u0e07\u0e40\u0e1b\u0e47\u0e19\u0e44\u0e1e\u0e48\u0e17\u0e35\u0e48\u0e15\u0e48\u0e33\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e43\u0e19\u0e41\u0e1e\u0e47\u0e04\u0e41\u0e25\u0e30\u0e44\u0e1e\u0e48 2 \u0e43\u0e1a\u0e02\u0e2d\u0e07 Hearts \u0e19\u0e31\u0e49\u0e19\u0e2a\u0e39\u0e07\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14 \u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e21\u0e35\u0e04\u0e27\u0e32\u0e21\u0e2a\u0e33\u0e04\u0e31\u0e0d\u0e21\u0e32\u0e01\u0e01\u0e27\u0e48\u0e32\u0e04\u0e27\u0e32\u0e21\u0e40\u0e2b\u0e21\u0e32\u0e30\u0e2a\u0e21\u0e15\u0e31\u0e27\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e40\u0e0a\u0e48\u0e19 8 \u0e0a\u0e19\u0e30 7\n\n\u0e02\u0e49\u0e2d\u0e15\u0e01\u0e25\u0e07\n\n\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e40\u0e01\u0e21\u0e41\u0e23\u0e01\u0e40\u0e08\u0e49\u0e32\u0e21\u0e37\u0e2d\u0e08\u0e30\u0e16\u0e39\u0e01\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e41\u0e1a\u0e1a\u0e2a\u0e38\u0e48\u0e21 \u0e08\u0e32\u0e01\u0e19\u0e31\u0e49\u0e19\u0e1c\u0e39\u0e49\u0e41\u0e1e\u0e49\u0e02\u0e2d\u0e07\u0e41\u0e15\u0e48\u0e25\u0e30\u0e40\u0e01\u0e21\u0e08\u0e30\u0e15\u0e49\u0e2d\u0e07\u0e08\u0e31\u0e14\u0e01\u0e32\u0e23\u0e15\u0e48\u0e2d\u0e44\u0e1b \u0e40\u0e21\u0e37\u0e48\u0e2d\u0e21\u0e35\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e35\u0e48\u0e04\u0e19\u0e44\u0e1e\u0e48 13 \u0e43\u0e1a\u0e08\u0e30\u0e16\u0e39\u0e01\u0e41\u0e08\u0e01\u0e43\u0e2b\u0e49\u0e01\u0e31\u0e1a\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e41\u0e15\u0e48\u0e25\u0e30\u0e04\u0e19\n\n\u0e2b\u0e32\u0e01\u0e21\u0e35\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e19\u0e49\u0e2d\u0e22\u0e01\u0e27\u0e48\u0e32\u0e2a\u0e35\u0e48\u0e04\u0e19\u0e08\u0e30\u0e22\u0e31\u0e07\u0e04\u0e07\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48 13 \u0e43\u0e1a\u0e43\u0e2b\u0e49\u0e01\u0e31\u0e1a\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e41\u0e15\u0e48\u0e25\u0e30\u0e04\u0e19\u0e41\u0e25\u0e30\u0e08\u0e30\u0e21\u0e35\u0e44\u0e1e\u0e48\u0e40\u0e2b\u0e25\u0e37\u0e2d\u0e2d\u0e22\u0e39\u0e48\u0e0b\u0e36\u0e48\u0e07\u0e44\u0e21\u0e48\u0e44\u0e14\u0e49\u0e43\u0e0a\u0e49\u0e43\u0e19\u0e40\u0e01\u0e21 \u0e2d\u0e35\u0e01\u0e17\u0e32\u0e07\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e32\u0e21\u0e04\u0e19\u0e04\u0e37\u0e2d\u0e15\u0e32\u0e21\u0e02\u0e49\u0e2d\u0e15\u0e01\u0e25\u0e07\u0e01\u0e48\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e08\u0e30\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48 17 \u0e43\u0e1a\u0e15\u0e48\u0e2d\u0e04\u0e19 \u0e40\u0e21\u0e37\u0e48\u0e2d\u0e21\u0e35\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e40\u0e1e\u0e35\u0e22\u0e07\u0e2a\u0e2d\u0e07\u0e04\u0e19\u0e04\u0e27\u0e23\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48\u0e43\u0e2b\u0e49\u0e04\u0e19\u0e25\u0e30 13 \u0e43\u0e1a\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19 - \u0e2b\u0e32\u0e01\u0e44\u0e1e\u0e48\u0e17\u0e31\u0e49\u0e07\u0e2b\u0e21\u0e14\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e41\u0e08\u0e01\u0e41\u0e08\u0e07\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e08\u0e30\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e43\u0e0a\u0e49\u0e21\u0e37\u0e2d\u0e02\u0e2d\u0e07\u0e01\u0e31\u0e19\u0e41\u0e25\u0e30\u0e01\u0e31\u0e19\u0e44\u0e14\u0e49\u0e0b\u0e36\u0e48\u0e07\u0e08\u0e30\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e40\u0e01\u0e21\u0e40\u0e2a\u0e35\u0e22 \u0e40\u0e21\u0e37\u0e48\u0e2d\u0e21\u0e35\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e21\u0e32\u0e01\u0e01\u0e27\u0e48\u0e32\u0e2a\u0e35\u0e48\u0e04\u0e19\u0e04\u0e38\u0e13\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e15\u0e01\u0e25\u0e07\u0e25\u0e48\u0e27\u0e07\u0e2b\u0e19\u0e49\u0e32\u0e44\u0e14\u0e49\u0e27\u0e48\u0e32\u0e08\u0e30\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48 13 \u0e43\u0e1a\u0e08\u0e32\u0e01\u0e44\u0e1e\u0e48\u0e2a\u0e2d\u0e07\u0e2a\u0e33\u0e23\u0e31\u0e1a\u0e2b\u0e23\u0e37\u0e2d\u0e41\u0e08\u0e01\u0e44\u0e1e\u0e48\u0e43\u0e2b\u0e49\u0e21\u0e32\u0e01\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e40\u0e17\u0e48\u0e32\u0e17\u0e35\u0e48\u0e08\u0e30\u0e17\u0e33\u0e44\u0e14\u0e49\u0e43\u0e2b\u0e49\u0e01\u0e31\u0e1a\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\n\n\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\n\n\u0e43\u0e19\u0e40\u0e01\u0e21\u0e41\u0e23\u0e01\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e17\u0e35\u0e48\u0e21\u0e35 3 Spades \u0e08\u0e30\u0e40\u0e23\u0e34\u0e48\u0e21\u0e40\u0e25\u0e48\u0e19 \u0e2b\u0e32\u0e01\u0e44\u0e21\u0e48\u0e21\u0e35\u0e43\u0e04\u0e23\u0e21\u0e35\u0e44\u0e1e\u0e48 3 \u0e43\u0e1a (\u0e43\u0e19\u0e40\u0e01\u0e21\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e32\u0e21\u0e2b\u0e23\u0e37\u0e2d\u0e2a\u0e2d\u0e07\u0e04\u0e19) \u0e43\u0e04\u0e23\u0e01\u0e47\u0e15\u0e32\u0e21\u0e17\u0e35\u0e48\u0e16\u0e37\u0e2d\u0e44\u0e1e\u0e48\u0e15\u0e48\u0e33\u0e2a\u0e38\u0e14\u0e08\u0e30\u0e40\u0e23\u0e34\u0e48\u0e21\u0e15\u0e49\u0e19 \u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e08\u0e30\u0e15\u0e49\u0e2d\u0e07\u0e40\u0e23\u0e34\u0e48\u0e21\u0e15\u0e49\u0e19\u0e14\u0e49\u0e27\u0e22\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e15\u0e48\u0e33\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e19\u0e35\u0e49\u0e44\u0e21\u0e48\u0e27\u0e48\u0e32\u0e08\u0e30\u0e14\u0e49\u0e27\u0e22\u0e15\u0e31\u0e27\u0e40\u0e2d\u0e07\u0e2b\u0e23\u0e37\u0e2d\u0e40\u0e1b\u0e47\u0e19\u0e2a\u0e48\u0e27\u0e19\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e02\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e23\u0e27\u0e21\u0e01\u0e31\u0e19\n\n\u0e43\u0e19\u0e40\u0e01\u0e21\u0e15\u0e48\u0e2d \u0e46 \u0e44\u0e1b\u0e1c\u0e39\u0e49\u0e0a\u0e19\u0e30\u0e02\u0e2d\u0e07\u0e40\u0e01\u0e21\u0e01\u0e48\u0e2d\u0e19\u0e2b\u0e19\u0e49\u0e32\u0e08\u0e30\u0e44\u0e14\u0e49\u0e40\u0e25\u0e48\u0e19\u0e01\u0e48\u0e2d\u0e19\u0e41\u0e25\u0e30\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e23\u0e34\u0e48\u0e21\u0e15\u0e49\u0e19\u0e14\u0e49\u0e27\u0e22\u0e0a\u0e38\u0e14\u0e04\u0e48\u0e32\u0e1c\u0e2a\u0e21\u0e43\u0e14\u0e01\u0e47\u0e44\u0e14\u0e49\n\n\u0e43\u0e19\u0e17\u0e32\u0e07\u0e01\u0e25\u0e31\u0e1a\u0e01\u0e31\u0e19\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e41\u0e15\u0e48\u0e25\u0e30\u0e04\u0e19\u0e08\u0e30\u0e15\u0e49\u0e2d\u0e07\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e1e\u0e48\u0e17\u0e35\u0e48\u0e40\u0e25\u0e48\u0e19\u0e01\u0e48\u0e2d\u0e19\u0e2b\u0e19\u0e49\u0e32\u0e19\u0e35\u0e49\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e1e\u0e48\u0e1c\u0e2a\u0e21\u0e01\u0e31\u0e19\u0e42\u0e14\u0e22\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e1e\u0e48\u0e23\u0e27\u0e21\u0e01\u0e31\u0e19\u0e17\u0e35\u0e48\u0e0a\u0e19\u0e30\u0e2b\u0e23\u0e37\u0e2d\u0e2a\u0e48\u0e07\u0e1c\u0e48\u0e32\u0e19\u0e41\u0e25\u0e30\u0e44\u0e21\u0e48\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e43\u0e14 \u0e46 \u0e01\u0e32\u0e23\u0e4c\u0e14\u0e17\u0e35\u0e48\u0e40\u0e25\u0e48\u0e19\u0e08\u0e30\u0e16\u0e39\u0e01\u0e27\u0e32\u0e07\u0e43\u0e19\u0e01\u0e2d\u0e07\u0e42\u0e14\u0e22\u0e2b\u0e07\u0e32\u0e22\u0e2b\u0e19\u0e49\u0e32\u0e02\u0e36\u0e49\u0e19\u0e15\u0e23\u0e07\u0e01\u0e25\u0e32\u0e07\u0e42\u0e15\u0e4a\u0e30 \u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e08\u0e30\u0e14\u0e33\u0e40\u0e19\u0e34\u0e19\u0e44\u0e1b\u0e23\u0e2d\u0e1a \u0e46 \u0e42\u0e15\u0e4a\u0e30\u0e2b\u0e25\u0e32\u0e22 \u0e46 \u0e04\u0e23\u0e31\u0e49\u0e07\u0e40\u0e17\u0e48\u0e32\u0e17\u0e35\u0e48\u0e08\u0e33\u0e40\u0e1b\u0e47\u0e19\u0e08\u0e19\u0e01\u0e27\u0e48\u0e32\u0e08\u0e30\u0e21\u0e35\u0e04\u0e19\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e2b\u0e23\u0e37\u0e2d\u0e0a\u0e38\u0e14\u0e17\u0e35\u0e48\u0e44\u0e21\u0e48\u0e21\u0e35\u0e43\u0e04\u0e23\u0e0a\u0e19\u0e30 \u0e40\u0e21\u0e37\u0e48\u0e2d\u0e40\u0e01\u0e34\u0e14\u0e40\u0e2b\u0e15\u0e38\u0e01\u0e32\u0e23\u0e13\u0e4c\u0e40\u0e0a\u0e48\u0e19\u0e19\u0e35\u0e49\u0e44\u0e1e\u0e48\u0e17\u0e35\u0e48\u0e40\u0e25\u0e48\u0e19\u0e17\u0e31\u0e49\u0e07\u0e2b\u0e21\u0e14\u0e08\u0e30\u0e16\u0e39\u0e01\u0e27\u0e32\u0e07\u0e17\u0e34\u0e49\u0e07\u0e44\u0e27\u0e49\u0e41\u0e25\u0e30\u0e1c\u0e39\u0e49\u0e17\u0e35\u0e48\u0e40\u0e25\u0e48\u0e19\u0e44\u0e14\u0e49\u0e44\u0e21\u0e48\u0e41\u0e1e\u0e49\u0e43\u0e04\u0e23\u0e08\u0e30\u0e40\u0e23\u0e34\u0e48\u0e21\u0e2d\u0e35\u0e01\u0e04\u0e23\u0e31\u0e49\u0e07\u0e42\u0e14\u0e22\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e15\u0e32\u0e21\u0e01\u0e0e\u0e2b\u0e21\u0e32\u0e22\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e1e\u0e48\u0e1c\u0e2a\u0e21\u0e43\u0e14 \u0e46 \u0e42\u0e14\u0e22\u0e2b\u0e31\u0e19\u0e2b\u0e19\u0e49\u0e32\u0e40\u0e02\u0e49\u0e32\u0e2b\u0e32\u0e01\u0e25\u0e32\u0e07\u0e42\u0e15\u0e4a\u0e30\n\n\u0e2b\u0e32\u0e01\u0e04\u0e38\u0e13\u0e1c\u0e48\u0e32\u0e19\u0e44\u0e1b\u0e04\u0e38\u0e13\u0e08\u0e30\u0e16\u0e39\u0e01\u0e25\u0e47\u0e2d\u0e01\u0e44\u0e21\u0e48\u0e43\u0e2b\u0e49\u0e40\u0e25\u0e48\u0e19\u0e08\u0e19\u0e01\u0e27\u0e48\u0e32\u0e08\u0e30\u0e21\u0e35\u0e04\u0e19\u0e40\u0e25\u0e48\u0e19\u0e42\u0e14\u0e22\u0e17\u0e35\u0e48\u0e44\u0e21\u0e48\u0e21\u0e35\u0e43\u0e04\u0e23\u0e0a\u0e19\u0e30 \u0e04\u0e38\u0e13\u0e21\u0e35\u0e2a\u0e34\u0e17\u0e18\u0e34\u0e4c\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e43\u0e2b\u0e21\u0e48\u0e2d\u0e35\u0e01\u0e04\u0e23\u0e31\u0e49\u0e07\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19\n\n\u0e15\u0e31\u0e27\u0e2d\u0e22\u0e48\u0e32\u0e07 (\u0e21\u0e35\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e32\u0e21\u0e04\u0e19): \u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e17\u0e32\u0e07\u0e02\u0e27\u0e32\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e40\u0e25\u0e48\u0e19\u0e2a\u0e32\u0e21\u0e04\u0e19\u0e40\u0e14\u0e35\u0e22\u0e27\u0e04\u0e38\u0e13\u0e16\u0e37\u0e2d\u0e40\u0e2d\u0e0b \u0e41\u0e15\u0e48\u0e15\u0e31\u0e14\u0e2a\u0e34\u0e19\u0e43\u0e08\u0e17\u0e35\u0e48\u0e08\u0e30\u0e2a\u0e48\u0e07\u0e1c\u0e48\u0e32\u0e19\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e17\u0e32\u0e07\u0e0b\u0e49\u0e32\u0e22\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e40\u0e25\u0e48\u0e19\u0e40\u0e01\u0e49\u0e32\u0e04\u0e19\u0e41\u0e25\u0e30\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e17\u0e32\u0e07\u0e02\u0e27\u0e32\u0e40\u0e25\u0e48\u0e19\u0e40\u0e1b\u0e47\u0e19\u0e23\u0e32\u0e0a\u0e32 \u0e15\u0e2d\u0e19\u0e19\u0e35\u0e49\u0e04\u0e38\u0e13\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e23\u0e32\u0e0a\u0e32\u0e14\u0e49\u0e27\u0e22\u0e40\u0e2d\u0e0b\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e44\u0e14\u0e49\u0e40\u0e1e\u0e23\u0e32\u0e30\u0e04\u0e38\u0e13\u0e1c\u0e48\u0e32\u0e19\u0e44\u0e1b\u0e41\u0e25\u0e49\u0e27 \u0e2b\u0e32\u0e01\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e04\u0e19\u0e17\u0e35\u0e48\u0e2a\u0e32\u0e21\u0e1c\u0e48\u0e32\u0e19\u0e44\u0e1b\u0e14\u0e49\u0e27\u0e22\u0e41\u0e25\u0e30\u0e1d\u0e48\u0e32\u0e22\u0e15\u0e23\u0e07\u0e02\u0e49\u0e32\u0e21\u0e21\u0e37\u0e2d\u0e02\u0e27\u0e32\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e08\u0e30\u0e40\u0e1b\u0e47\u0e19\u0e23\u0e32\u0e0a\u0e34\u0e19\u0e35\u0e15\u0e2d\u0e19\u0e19\u0e35\u0e49\u0e04\u0e38\u0e13\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e40\u0e2d\u0e0b\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e44\u0e14\u0e49\u0e2b\u0e32\u0e01\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23\n\n\u0e01\u0e32\u0e23\u0e40\u0e25\u0e48\u0e19\u0e15\u0e32\u0e21\u0e01\u0e0e\u0e2b\u0e21\u0e32\u0e22\u0e43\u0e19\u0e40\u0e01\u0e21\u0e21\u0e35\u0e14\u0e31\u0e07\u0e19\u0e35\u0e49:\n\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e17\u0e35\u0e48\u0e15\u0e48\u0e33\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e04\u0e37\u0e2d\u0e44\u0e1e\u0e48 3 \u0e43\u0e1a\u0e41\u0e25\u0e30\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e04\u0e37\u0e2d\u0e44\u0e1e\u0e48 2 \u0e43\u0e1a\n\u0e08\u0e31\u0e1a\u0e04\u0e39\u0e48\u0e44\u0e1e\u0e48\u0e2a\u0e2d\u0e07\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e21\u0e35\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19\u0e40\u0e0a\u0e48\u0e19 7-7 \u0e2b\u0e23\u0e37\u0e2d Q-Q\n\u0e44\u0e1e\u0e48\u0e2a\u0e32\u0e21\u0e43\u0e1a\u0e43\u0e19\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19 - \u0e40\u0e0a\u0e48\u0e19 5-5-5\n\u0e44\u0e1e\u0e48\u0e2a\u0e35\u0e48\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e21\u0e35\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19 - \u0e40\u0e0a\u0e48\u0e19 9-9-9-9\n\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e2a\u0e32\u0e21\u0e43\u0e1a\u0e02\u0e36\u0e49\u0e19\u0e44\u0e1b\u0e17\u0e35\u0e48\u0e21\u0e35\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e15\u0e48\u0e2d\u0e40\u0e19\u0e37\u0e48\u0e2d\u0e07\u0e01\u0e31\u0e19 (\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e1c\u0e2a\u0e21\u0e0a\u0e38\u0e14\u0e44\u0e14\u0e49) \u0e40\u0e0a\u0e48\u0e19 4-5-6 \u0e2b\u0e23\u0e37\u0e2d J-Q-K-A \u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16 "\u0e1e\u0e25\u0e34\u0e01\u0e21\u0e38\u0e21" \u0e23\u0e30\u0e2b\u0e27\u0e48\u0e32\u0e07\u0e2a\u0e2d\u0e07\u0e16\u0e36\u0e07\u0e2a\u0e32\u0e21 - A-2-3 \u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e17\u0e35\u0e48\u0e16\u0e39\u0e01\u0e15\u0e49\u0e2d\u0e07\u0e40\u0e19\u0e37\u0e48\u0e2d\u0e07\u0e08\u0e32\u0e01 2 \u0e2a\u0e39\u0e07\u0e41\u0e25\u0e30 3 \u0e15\u0e48\u0e33\n\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e04\u0e39\u0e48 3 \u0e04\u0e39\u0e48\u0e02\u0e36\u0e49\u0e19\u0e44\u0e1b\u0e02\u0e2d\u0e07\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a\u0e17\u0e35\u0e48\u0e15\u0e48\u0e2d\u0e40\u0e19\u0e37\u0e48\u0e2d\u0e07\u0e01\u0e31\u0e19\u0e40\u0e0a\u0e48\u0e19 3-3-4-4-5-5 \u0e2b\u0e23\u0e37\u0e2d 6-6-7-7-8-8-9-9\n\n\u0e42\u0e14\u0e22\u0e17\u0e31\u0e48\u0e27\u0e44\u0e1b\u0e01\u0e32\u0e23\u0e1c\u0e2a\u0e21\u0e08\u0e30\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e14\u0e49\u0e42\u0e14\u0e22\u0e01\u0e32\u0e23\u0e1c\u0e2a\u0e21\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e02\u0e2d\u0e07\u0e44\u0e1e\u0e48\u0e1b\u0e23\u0e30\u0e40\u0e20\u0e17\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19\u0e41\u0e25\u0e30\u0e08\u0e33\u0e19\u0e27\u0e19\u0e44\u0e1e\u0e48\u0e40\u0e17\u0e48\u0e32\u0e01\u0e31\u0e19 \u0e14\u0e31\u0e07\u0e19\u0e31\u0e49\u0e19\u0e2b\u0e32\u0e01\u0e19\u0e33\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e08\u0e30\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e44\u0e14\u0e49\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19 \u0e16\u0e49\u0e32\u0e04\u0e39\u0e48\u0e19\u0e33\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19\u0e17\u0e35\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e44\u0e14\u0e49; \u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e2a\u0e32\u0e21\u0e43\u0e1a\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e14\u0e49\u0e42\u0e14\u0e22\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e2a\u0e32\u0e21\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19 \u0e41\u0e25\u0e30\u0e2d\u0e37\u0e48\u0e19 \u0e46 \u0e15\u0e31\u0e27\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e40\u0e0a\u0e48\u0e19\u0e04\u0e38\u0e13\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e04\u0e39\u0e48\u0e14\u0e49\u0e27\u0e22\u0e44\u0e1e\u0e48\u0e2a\u0e32\u0e21\u0e43\u0e1a\u0e2b\u0e23\u0e37\u0e2d\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e2a\u0e35\u0e48\u0e43\u0e1a\u0e17\u0e35\u0e48\u0e21\u0e35\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e44\u0e1e\u0e48\u0e2b\u0e49\u0e32\u0e43\u0e1a\n\n\u0e43\u0e19\u0e01\u0e32\u0e23\u0e15\u0e31\u0e14\u0e2a\u0e34\u0e19\u0e43\u0e08\u0e27\u0e48\u0e32\u0e0a\u0e38\u0e14\u0e04\u0e48\u0e32\u0e1c\u0e2a\u0e21\u0e1b\u0e23\u0e30\u0e40\u0e20\u0e17\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19\u0e43\u0e14\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e04\u0e38\u0e13\u0e40\u0e1e\u0e35\u0e22\u0e07\u0e41\u0e04\u0e48\u0e14\u0e39\u0e44\u0e1e\u0e48\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14\u0e43\u0e19\u0e0a\u0e38\u0e14\u0e04\u0e48\u0e32\u0e1c\u0e2a\u0e21 \u0e15\u0e31\u0e27\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e40\u0e0a\u0e48\u0e19 7-7 \u0e40\u0e15\u0e49\u0e19 7-7 \u0e40\u0e1e\u0e23\u0e32\u0e30\u0e2b\u0e31\u0e27\u0e43\u0e08\u0e40\u0e15\u0e49\u0e19\u0e40\u0e1e\u0e0a\u0e23 \u0e43\u0e19\u0e17\u0e33\u0e19\u0e2d\u0e07\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19 8-9-10 \u0e40\u0e15\u0e49\u0e19 8-9-10 \u0e40\u0e1e\u0e23\u0e32\u0e30\u0e40\u0e1b\u0e47\u0e19\u0e44\u0e1e\u0e48\u0e2a\u0e39\u0e07\u0e2a\u0e38\u0e14 (\u0e2b\u0e25\u0e31\u0e01\u0e2a\u0e34\u0e1a) \u0e17\u0e35\u0e48\u0e19\u0e33\u0e21\u0e32\u0e40\u0e1b\u0e23\u0e35\u0e22\u0e1a\u0e40\u0e17\u0e35\u0e22\u0e1a\u0e01\u0e31\u0e19\n\n\u0e21\u0e35\u0e02\u0e49\u0e2d\u0e22\u0e01\u0e40\u0e27\u0e49\u0e19\u0e40\u0e1e\u0e35\u0e22\u0e07\u0e2a\u0e35\u0e48\u0e02\u0e49\u0e2d\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e01\u0e0e\u0e17\u0e35\u0e48\u0e27\u0e48\u0e32\u0e0a\u0e38\u0e14\u0e04\u0e48\u0e32\u0e1c\u0e2a\u0e21\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e14\u0e49\u0e42\u0e14\u0e22\u0e01\u0e32\u0e23\u0e23\u0e27\u0e21\u0e01\u0e31\u0e19\u0e02\u0e2d\u0e07\u0e1b\u0e23\u0e30\u0e40\u0e20\u0e17\u0e40\u0e14\u0e35\u0e22\u0e27\u0e01\u0e31\u0e19\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19:\n\n\u0e44\u0e1e\u0e48\u0e2a\u0e35\u0e48\u0e43\u0e1a\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e1e\u0e48\u0e2a\u0e2d\u0e07\u0e43\u0e1a\u0e43\u0e14\u0e01\u0e47\u0e44\u0e14\u0e49 (\u0e41\u0e15\u0e48\u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e2d\u0e37\u0e48\u0e19 \u0e46 \u0e40\u0e0a\u0e48\u0e19\u0e40\u0e2d\u0e0b\u0e2b\u0e23\u0e37\u0e2d\u0e23\u0e32\u0e0a\u0e32) \u0e2a\u0e35\u0e48\u0e0a\u0e19\u0e34\u0e14\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e14\u0e49\u0e14\u0e49\u0e27\u0e22\u0e2a\u0e35\u0e48\u0e0a\u0e19\u0e34\u0e14\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\n\n\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2a\u0e32\u0e21\u0e04\u0e39\u0e48 (\u0e40\u0e0a\u0e48\u0e19 7-7-8-8-9-9) \u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e44\u0e1e\u0e48\u0e2a\u0e2d\u0e07\u0e43\u0e1a\u0e43\u0e14\u0e01\u0e47\u0e44\u0e14\u0e49 (\u0e41\u0e15\u0e48\u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e44\u0e1e\u0e48\u0e43\u0e1a\u0e40\u0e14\u0e35\u0e22\u0e27\u0e2d\u0e37\u0e48\u0e19 \u0e46 ) \u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2a\u0e32\u0e21\u0e04\u0e39\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e42\u0e14\u0e22\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e02\u0e2d\u0e07\u0e2a\u0e32\u0e21\u0e04\u0e39\u0e48\n\n\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2a\u0e35\u0e48\u0e04\u0e39\u0e48 (\u0e40\u0e0a\u0e48\u0e19 5-5-6-6-7-7-8-8) \u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e04\u0e39\u0e48\u0e02\u0e2d\u0e07\u0e2a\u0e2d\u0e07\u0e04\u0e39\u0e48\u0e44\u0e14\u0e49 (\u0e41\u0e15\u0e48\u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e04\u0e39\u0e48\u0e2d\u0e37\u0e48\u0e19 \u0e46 ) \u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2a\u0e35\u0e48\u0e04\u0e39\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e42\u0e14\u0e22\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e02\u0e2d\u0e07\u0e2a\u0e35\u0e48\u0e04\u0e39\u0e48\n\n\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2b\u0e49\u0e32\u0e04\u0e39\u0e48 (\u0e40\u0e0a\u0e48\u0e19 8-8-9-9-10-10-J-J-Q-Q) \u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e40\u0e0b\u0e15\u0e02\u0e2d\u0e07\u0e2a\u0e32\u0e21\u0e04\u0e39\u0e48\u0e44\u0e14\u0e49 (\u0e41\u0e15\u0e48\u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e2d\u0e35\u0e01\u0e2a\u0e32\u0e21\u0e04\u0e39\u0e48) \u0e25\u0e33\u0e14\u0e31\u0e1a\u0e02\u0e2d\u0e07\u0e2b\u0e49\u0e32\u0e04\u0e39\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e42\u0e14\u0e22\u0e25\u0e33\u0e14\u0e31\u0e1a\u0e17\u0e35\u0e48\u0e2a\u0e39\u0e07\u0e01\u0e27\u0e48\u0e32\u0e02\u0e2d\u0e07\u0e2b\u0e49\u0e32\u0e04\u0e39\u0e48\n\n\u0e0a\u0e38\u0e14\u0e04\u0e48\u0e32\u0e1c\u0e2a\u0e21\u0e40\u0e2b\u0e25\u0e48\u0e32\u0e19\u0e35\u0e49\u0e17\u0e35\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e04\u0e39\u0e48\u0e40\u0e14\u0e35\u0e48\u0e22\u0e27\u0e2b\u0e23\u0e37\u0e2d\u0e0a\u0e38\u0e14\u0e2a\u0e2d\u0e07\u0e0a\u0e38\u0e14\u0e1a\u0e32\u0e07\u0e04\u0e23\u0e31\u0e49\u0e07\u0e40\u0e23\u0e35\u0e22\u0e01\u0e27\u0e48\u0e32\u0e23\u0e30\u0e40\u0e1a\u0e34\u0e14\u0e2b\u0e23\u0e37\u0e2d\u0e23\u0e30\u0e40\u0e1a\u0e34\u0e14\u0e2a\u0e2d\u0e07\u0e25\u0e39\u0e01\u0e41\u0e25\u0e30\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e44\u0e14\u0e49\u0e41\u0e21\u0e49\u0e01\u0e23\u0e30\u0e17\u0e31\u0e48\u0e07\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19\u0e17\u0e35\u0e48\u0e1c\u0e48\u0e32\u0e19\u0e21\u0e32\u0e01\u0e48\u0e2d\u0e19\u0e2b\u0e19\u0e49\u0e32\u0e19\u0e35\u0e49\n\n\u0e42\u0e1b\u0e23\u0e14\u0e17\u0e23\u0e32\u0e1a\u0e27\u0e48\u0e32\u0e02\u0e49\u0e2d\u0e22\u0e01\u0e40\u0e27\u0e49\u0e19\u0e40\u0e2b\u0e25\u0e48\u0e32\u0e19\u0e35\u0e49\u0e43\u0e0a\u0e49\u0e01\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e15\u0e35\u0e2a\u0e2d\u0e07\u0e43\u0e1a\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19\u0e44\u0e21\u0e48\u0e43\u0e0a\u0e48\u0e44\u0e1e\u0e48\u0e2d\u0e37\u0e48\u0e19 \u0e46 \u0e15\u0e31\u0e27\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e40\u0e0a\u0e48\u0e19\u0e16\u0e49\u0e32\u0e21\u0e35\u0e04\u0e19\u0e40\u0e25\u0e48\u0e19\u0e40\u0e2d\u0e0b\u0e04\u0e38\u0e13\u0e08\u0e30\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e21\u0e31\u0e19\u0e14\u0e49\u0e27\u0e22\u0e40\u0e2d\u0e0b\u0e17\u0e31\u0e49\u0e07\u0e2a\u0e35\u0e48\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e44\u0e14\u0e49 \u0e41\u0e15\u0e48\u0e16\u0e49\u0e32\u0e40\u0e2d\u0e0b\u0e16\u0e39\u0e01\u0e2a\u0e2d\u0e07\u0e04\u0e19\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e04\u0e19\u0e17\u0e31\u0e49\u0e07\u0e2a\u0e35\u0e48\u0e02\u0e2d\u0e07\u0e04\u0e38\u0e13\u0e01\u0e47\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e43\u0e0a\u0e49\u0e40\u0e1e\u0e37\u0e48\u0e2d\u0e40\u0e2d\u0e32\u0e0a\u0e19\u0e30\u0e17\u0e31\u0e49\u0e07\u0e2a\u0e2d\u0e07\u0e44\u0e14\u0e49';
    exports.default = tutor;
    cc._RF.pop();
  }, {} ],
  th_TH: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3b07by6DGRJu75lIR9EK7nZ", "th_TH");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var th_TH_tut_1 = require("./th_TH_tut");
    exports.default = {
      PLAYNOW: "\u0e40\u0e25\u0e48\u0e19\u0e40\u0e25\u0e22",
      WIN: "\u0e17\u0e35\u0e48 1",
      LOSE2: "\u0e04\u0e23\u0e31\u0e49\u0e07\u0e17\u0e35\u0e48 2",
      LOSE3: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 3",
      LOSE4: "\u0e2d\u0e31\u0e19\u0e14\u0e31\u0e1a 4",
      PLAYER: "\u0e08\u0e33\u0e19\u0e27\u0e19\u0e1c\u0e39\u0e49\u0e40\u0e25\u0e48\u0e19",
      BETNO: "\u0e40\u0e14\u0e34\u0e21\u0e1e\u0e31\u0e19",
      DAILY: "\u0e23\u0e32\u0e07\u0e27\u0e31\u0e25\u0e23\u0e32\u0e22\u0e27\u0e31\u0e19",
      D1: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 1",
      D2: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 2",
      D3: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 3",
      D4: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 4",
      D5: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 5",
      D6: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 6",
      D7: "\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48 7",
      "3TURNS": "3 \u0e23\u0e2d\u0e1a",
      "5TURNS": "5 \u0e23\u0e2d\u0e1a",
      TURNS: "\u0e23\u0e2d\u0e1a",
      RECEIVED: "\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a",
      LEADER: "\u0e25\u0e35\u0e14\u0e40\u0e14\u0e2d\u0e23\u0e4c\u0e1a\u0e2d\u0e23\u0e4c\u0e14",
      NOVIDEO: "\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e40\u0e25\u0e48\u0e19\u0e27\u0e34\u0e14\u0e35\u0e42\u0e2d\u0e44\u0e14\u0e49\u0e43\u0e19\u0e02\u0e13\u0e30\u0e19\u0e35\u0e49",
      BET: "\u0e40\u0e14\u0e34\u0e21\u0e1e\u0e31\u0e19",
      NO: "\u0e08\u0e33\u0e19\u0e27\u0e19",
      PASS: "\u0e1c\u0e48\u0e32\u0e19",
      HIT: "\u0e15\u0e35",
      ARRANGE: "\u0e08\u0e31\u0e14",
      QUITGAME: "\u0e2d\u0e2d\u0e01\u0e08\u0e32\u0e01\u0e40\u0e01\u0e21",
      QUITGAMEP: "\u0e04\u0e38\u0e13\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e2d\u0e2d\u0e01\u0e08\u0e32\u0e01\u0e40\u0e01\u0e21\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e21\u0e48? \u0e2b\u0e32\u0e01\u0e04\u0e38\u0e13\u0e2d\u0e2d\u0e01\u0e08\u0e32\u0e01\u0e40\u0e01\u0e21\u0e04\u0e38\u0e13\u0e08\u0e30\u0e40\u0e2a\u0e35\u0e22\u0e2a\u0e34\u0e1a\u0e40\u0e17\u0e48\u0e32\u0e02\u0e2d\u0e07\u0e23\u0e30\u0e14\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e40\u0e14\u0e34\u0e21\u0e1e\u0e31\u0e19",
      QUIT: "\u0e40\u0e25\u0e34\u0e01",
      "3PAIRS": "3 \u0e04\u0e39\u0e48\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d\u0e01\u0e31\u0e19",
      "4PAIRS": "4 \u0e04\u0e39\u0e48\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d\u0e01\u0e31\u0e19",
      FOURKIND: "\u0e2a\u0e35\u0e48\u0e0a\u0e19\u0e34\u0e14",
      FLUSH: "\u0e2a\u0e40\u0e15\u0e23\u0e17\u0e1f\u0e25\u0e31\u0e0a",
      "1BEST": "\u0e14\u0e35\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14",
      "2BEST": "2 \u0e14\u0e35\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14",
      "3BEST": "3 \u0e14\u0e35\u0e17\u0e35\u0e48\u0e2a\u0e38\u0e14",
      INSTRUCT: "\u0e04\u0e33\u0e41\u0e19\u0e30\u0e19\u0e33",
      NOMONEY: "\u0e04\u0e38\u0e13\u0e2b\u0e21\u0e14\u0e40\u0e07\u0e34\u0e19\u0e04\u0e25\u0e34\u0e01\u0e40\u0e1e\u0e37\u0e48\u0e2d\u0e23\u0e31\u0e1a\u0e40\u0e07\u0e34\u0e19\u0e40\u0e1e\u0e34\u0e48\u0e21",
      RECEI2: "\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a",
      SPINNOW: "\u0e2b\u0e21\u0e38\u0e19",
      SPIN: "\u0e2b\u0e21\u0e38\u0e19",
      "1TURN": "\u0e2d\u0e35\u0e01 1 \u0e40\u0e17\u0e34\u0e23\u0e4c\u0e19",
      LUCKYSPIN: "\u0e2b\u0e21\u0e38\u0e19\u0e42\u0e0a\u0e04\u0e14\u0e35",
      LUCK: "\u0e21\u0e35\u0e42\u0e0a\u0e04\u0e43\u0e19\u0e20\u0e32\u0e22\u0e2b\u0e25\u0e31\u0e07",
      TURN: "\u0e2d\u0e35\u0e01\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e40\u0e17\u0e34\u0e23\u0e4c\u0e19",
      X2: "X2 \u0e40\u0e07\u0e34\u0e19",
      X3: "X3 \u0e40\u0e07\u0e34\u0e19",
      X5: "X5 \u0e40\u0e07\u0e34\u0e19",
      X10: "X10 \u0e40\u0e07\u0e34\u0e19",
      MISS: "\u0e19\u0e32\u0e07\u0e2a\u0e32\u0e27",
      MONEY1: "\u0e22\u0e34\u0e19\u0e14\u0e35\u0e14\u0e49\u0e27\u0e22! \u0e04\u0e38\u0e13\u0e21\u0e35\u0e40\u0e07\u0e34\u0e19 %s",
      TUT: th_TH_tut_1.default
    };
    cc._RF.pop();
  }, {
    "./th_TH_tut": "th_TH_tut"
  } ],
  tl_PH_tut: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "659f1eXcpxLYqcIXEkkIEA6", "tl_PH_tut");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var tutor = "Para sa unang laro, ang dealer ay napili nang sapalaran; pagkatapos ay ang natalo ng bawat laro ay kailangang harapin ang susunod. Kapag mayroong apat na manlalaro, 13 cards ang ibabahagi sa bawat manlalaro.\n\nKung may mas kaunti sa apat na manlalaro, 13 cards pa rin ang nakikitungo sa bawat manlalaro, at magkakaroon ng ilang mga kard na natitira na hindi na ginagamit - hindi ito ginagamit sa laro. Ang isang kahalili sa tatlong mga manlalaro ay, sa pamamagitan ng naunang kasunduan, upang makitungo sa bawat 17 card. Kapag mayroong dalawang manlalaro lamang, 13 card lamang ang bawat isa ay dapat na harapin - kung ang lahat ng mga kard ay naaksyunan ang mga manlalaro ay maaaring mag-ehersisyo ang mga kamay ng bawat isa, na makakasira sa laro. Kapag mayroong higit sa apat na manlalaro, maaari kang sumang-ayon nang maaga alinman sa pakikitungo sa 13 mga kard bawat isa mula sa dobleng deck, o pakikitungo sa maraming mga card hangga't maaari pantay sa mga manlalaro.\n\nAng laro\n\nSa unang laro lamang, ang manlalaro na may 3 ng Spades ay nagsisimulang maglaro. Kung walang sinuman ang mayroong 3 (sa tatlo o dalawang laro ng manlalaro) sinumang may hawak ng pinakamababang card ay nagsisimula. Dapat magsimula ang manlalaro sa pamamagitan ng paglalaro ng pinakamababang card na ito, alinman sa sarili o bilang bahagi ng isang kumbinasyon.\n\nSa kasunod na mga laro, ang nagwagi ng nakaraang laro ay naglalaro muna, at maaaring magsimula sa anumang kumbinasyon.\n\nAng bawat manlalaro naman ay dapat na talunin ang dati nang nilalaro na card o kombinasyon, sa pamamagitan ng paglalaro ng isang card o kombinasyon na pumapalo dito, o pumasa at hindi naglalaro ng anumang mga card. Ang (mga) tinugtog na kard ay inilalagay sa isang magbunton ng mukha sa gitna ng mesa. Ang paglalaro ay paikot-ikot sa talahanayan nang maraming beses hangga't kinakailangan hanggang sa ang isang tao ay maglaro ng isang kard o kumbinasyon na walang ibang natalo. Kapag nangyari ito, ang lahat ng mga pinatugtog na kard ay itinabi, at ang taong ang walang patalo na laro ay nagsisimula muli sa pamamagitan ng paglalaro ng anumang ligal na kard o kombinasyon na nakaharap sa gitna ng mesa.\n\nKung pumasa ka ay naka-lock sa labas ng laro hanggang sa may gumawa ng isang dula na walang nakakatalo. Kapag ang mga kard ay itinabi at ang isang bagong kard o kombinasyon ay pinangungunahan may karapatan kang maglaro muli.\n\nHalimbawa (na may tatlong mga manlalaro): ang manlalaro sa kanan ay gumaganap ng solong tatlo, may hawak kang alas ngunit nagpasya na pumasa, ang manlalaro sa kaliwa ay gumaganap ng siyam at ang manlalaro sa kanan ay gumaganap ng isang hari. Hindi mo matalo ngayon ang hari sa iyong ace, dahil nakapasa ka na. Kung ang pangatlong manlalaro ay pumasa din, at ang iyong kalaban sa kanang kamay ngayon ay humantong sa isang reyna, maaari mo na ngayong i-play ang iyong ace kung nais mo.\n\nAng mga ligal na dula sa laro ay ang mga sumusunod:\nSingle card Ang pinakamababang solong card ay ang 3 at ang pinakamataas ay ang 2.\nIpares ang Dalawang kard ng parehong ranggo - tulad ng 7-7 o Q-Q.\nTatlong Tatlong kard ng magkaparehong ranggo - tulad ng 5-5-5\nApat ng isang uri Apat na kard ng parehong ranggo - tulad ng 9-9-9-9.\nPagkakasunud-sunod Tatlo o higit pang mga kard ng magkakasunod na ranggo (ang mga suit ay maaaring ihalo) - tulad ng 4-5-6 o J-Q-K-A. Ang mga pagkakasunud-sunod ay hindi maaaring \"i-on ang sulok\" sa pagitan ng dalawa at tatlo - Ang A-2-3 ay hindi isang wastong pagkakasunud-sunod dahil ang 2 ay mataas at 3 ay mababa.\nDouble Sequence Tatlo o higit pang mga pares ng magkakasunod na ranggo - tulad ng 3-3-4-4-5-5 o 6-6-7-7-8-8-9-9.\n\nSa pangkalahatan, ang isang kumbinasyon ay maaari lamang matalo ng isang mas mataas na kumbinasyon ng parehong uri at parehong bilang ng mga kard. Kaya't kung ang isang solong card ay hahantong, iisang card lamang ang maaaring i-play; kung ang isang pares ay pinangunahan pares lamang ang maaaring i-play; ang isang pagkakasunud-sunod ng tatlong card ay maaari lamang matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong card; at iba pa. Hindi mo maaaring halimbawa talunin ang isang pares gamit ang isang triple, o isang pagkakasunud-sunod ng apat na card na may isang pagkakasunud-sunod ng limang card.\n\nUpang magpasya kung alin sa dalawang mga kumbinasyon ng parehong uri ang mas mataas ay titingnan mo lamang ang pinakamataas na card na pinagsama. Halimbawa 7-7 beats 7-7 dahil pinapalo ng puso ang brilyante. Sa parehong paraan 8-9-10 beats 8-9-10 sapagkat ito ang pinakamataas na card (ang sampu) na inihambing.\n\nMayroong apat na mga pagbubukod sa panuntunan na ang isang kumbinasyon ay maaari lamang matalo ng isang kumbinasyon ng parehong uri:\n\nAng isang apat na uri ay maaaring matalo ang anumang solong dalawa (ngunit hindi sa anumang iba pang solong card, tulad ng isang alas o hari). Ang isang apat na uri ay maaaring matalo ng mas mataas na apat na uri.\n\nAng isang pagkakasunud-sunod ng tatlong pares (tulad ng 7-7-8-8-9-9) ay maaaring matalo ang anumang solong dalawa (ngunit hindi anumang iba pang solong card). Ang isang pagkakasunud-sunod ng tatlong mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong mga pares.\n\nAng isang pagkakasunud-sunod ng apat na pares (tulad ng 5-5-6-6-7-7-8-8) ay maaaring matalo ang isang pares ng dalawa (ngunit hindi sa anumang iba pang pares). Ang isang pagkakasunud-sunod ng apat na mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng apat na mga pares.\n\nAng isang pagkakasunud-sunod ng limang pares (tulad ng 8-8-9-9-10-10-J-J-Q-Q) ay maaaring matalo ang isang hanay ng tatlong dalawa (ngunit hindi sa anumang iba pang tatlong uri). Ang isang pagkakasunud-sunod ng limang mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng limang mga pares.\n\nAng mga kumbinasyong ito na maaaring matalo ang mga nag-iisang twos o mga hanay ng dalawa ay paminsan-minsang kilala bilang mga bomba o dalawang-bomba, at maaaring i-play kahit ng isang manlalaro na dati nang lumipas.\n\nTandaan na nalalapat lamang ang mga pagbubukod na ito sa pagkatalo sa dalawa, hindi sa iba pang mga kard. Halimbawa, kung ang isang tao ay naglalaro ng alas ay hindi mo ito matatalo sa iyong apat na uri, ngunit kung ang alas ay pinalo ng dalawa, kung gayon ang iyong apat na uri ay maaaring magamit upang talunin ang dalawa";
    exports.default = tutor;
    cc._RF.pop();
  }, {} ],
  tl_PH: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "95e70d0JFBKabdpj8E+Dcll", "tl_PH");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var th_TH_tut_1 = require("./th_TH_tut");
    exports.default = {
      PLAYNOW: "Maglaro",
      WIN: "Ika-1",
      LOSE2: "Ika-2",
      LOSE3: "Ika-3",
      LOSE4: "Ika-4",
      PLAYER: "bilang ng mga manlalaro",
      BETNO: "Taya",
      DAILY: "Daily reward",
      D1: "Araw 1",
      D2: "Araw 2",
      D3: "Araw 3",
      D4: "Araw 4",
      D5: "Araw 5",
      D6: "Araw 6",
      D7: "Araw 7",
      "3TURNS": "3 liko",
      "5TURNS": "5 liko",
      TURNS: "liko",
      RECEIVED: "Habol",
      LEADER: "Leaderboard",
      NOVIDEO: "Hindi maaaring i-play ang video ngayon",
      BET: "Taya",
      NO: "Numero",
      PASS: "Pumasa",
      HIT: "Hit",
      ARRANGE: "Ayusin",
      QUITGAME: "Tumigil sa laro",
      QUITGAMEP: "Kung huminto ka sa laro mawawala sa iyo ng sampung beses bilang antas ng pusta",
      QUIT: "Umalis na",
      "3PAIRS": "3 magkasunod na pares",
      "4PAIRS": "4 magkasunod na pares",
      FOURKIND: "Apat ng isang uri",
      FLUSH: "Straight Flush",
      "1BEST": "pinakamahusay",
      "2BEST": "2 pinakamahusay",
      "3BEST": "3 pinakamahusay",
      INSTRUCT: "Panuto",
      NOMONEY: "Naubusan ka ng pera, mag-click upang makatanggap ng mas maraming pera",
      RECEI2: "Habol",
      SPINNOW: "Paikutin",
      SPIN: "Paikutin",
      "1TURN": "1 pa naman",
      LUCKYSPIN: "masuwerteng paikutin",
      LUCK: "Mag swerte ka mamaya",
      TURN: "Isa pa",
      X2: "X2 pera",
      X3: "X3 pera",
      X5: "X5 pera",
      X10: "X10 pera",
      MISS: "Miss",
      MONEY1: "Pagbati! Mayroon kang %s",
      TUT: th_TH_tut_1.default
    };
    cc._RF.pop();
  }, {
    "./th_TH_tut": "th_TH_tut"
  } ],
  use_reversed_rotateBy: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dd02fL4CqJITr6/oDI+73WZ", "use_reversed_rotateBy");
    "use strict";
    cc.RotateBy._reverse = true;
    cc._RF.pop();
  }, {} ],
  util: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5164dU8pUhEeacqN2jObHLU", "util");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Config_1 = require("./Config");
    var util = function() {
      function util() {}
      util.numberFormat = function(n) {
        var ii = 0;
        while ((n /= 1e3) >= 1) ii++;
        return Math.round(10 * n * 1e3) / 10 + util.prefix[ii];
      };
      util.playAudio = function(audioClip) {
        Config_1.default.soundEnable && cc.audioEngine.playEffect(audioClip, false);
      };
      util.prefix = [ "", "k", "M", "G", "T", "P", "E", "Z", "Y", "x10^27", "x10^30", "x10^33" ];
      return util;
    }();
    exports.default = util;
    cc._RF.pop();
  }, {
    "./Config": "Config"
  } ],
  vi_VN_tut: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "77101c4onlIip2iwXQKzaFb", "vi_VN_tut");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var tutor = 'Ti\u1ebfn L\xean Mi\u1ec1n Nam s\u1eed d\u1ee5ng b\u1ed9 b\xe0i T\xe2y 52 l\xe1.\n\nGi\xe1 tr\u1ecb qu\xe2n b\xe0i s\u1ebd ph\u1ee5 thu\u1ed9c v\xe0o s\u1ed1: 2(heo) > A(x\xec) > K(gi\xe0) > Q(\u0111\u1ea7m) > J(b\u1ed3i) > 10 > 9 > .... > 3.\n\nN\u1ebfu hai qu\xe2n b\xe0i c\xf3 c\xf9ng s\u1ed1 th\xec s\u1ebd so s\xe1nh theo ch\u1ea5t : \u2665 > \u2666 > \u2663 > \u2660.\nV\xed d\u1ee5 : 10 \u2665 > 10 \u2666\n\nChia b\xe0i: M\u1ed7i ng\u01b0\u1eddi \u0111\u01b0\u1ee3c chia 13 l\xe1 b\xe0i.\n\nC\xe1ch x\u1ebfp b\xe0i: \nB\xe0i r\xe1c 1 l\xe1 b\xe0i l\u1ebb: b\xe0i l\u1ebb c\xf3 th\u1ec3 b\u1ecb \u0111\xe8 b\u1edfi 1 l\xe1 b\xe0i l\u1ebb c\xf3 gi\xe1 tr\u1ecb l\u1edbn h\u01a1n\nVd : 4 \u2666 > 3 \u2660\n\nB\u1ed9 \u0111\xf4i 2 l\xe1 b\xe0i gi\u1ed1ng nhau: 1 \u0111\xf4i c\xf3 th\u1ec3 \u0111\xe8 \u0111\u01b0\u1ee3c 1 \u0111\xf4i b\xe9 h\u01a1n \nVd : J \u2665 + J \u2660 > J \u2666 + J \u2663\n\nT\u01b0\u01a1ng t\u1ef1 v\u1edbi b\u1ed9 3 l\xe1 b\xe0i gi\u1ed1ng nhau ( s\xe1m c\xf4 )\n\nS\u1ea3nh : c\xf3 \xedt nh\u1ea5t 3 l\xe1 b\xe0i c\xf3 gi\xe1 tr\u1ecb t\u0103ng d\u1ea7n: 3 + 4 + 5 + 6 + 7 + 8 ......\n1 S\u1ea3nh c\xf3 th\u1ec3 b\u1ecb \u0111\xe8 b\u1edfi 1 s\u1ea3nh c\xf3 c\xf9ng s\u1ed1 l\xe1, nh\u01b0ng gi\xe1 tr\u1ecb c\u1ee7a s\u1ed1 \u0111\xf3 l\u1edbn h\u01a1n\nVd : S\u1ea3nh 3 4 5 < 4 5 6 , ho\u1eb7c 7 8 9 < J Q K \n\nN\u1ebfu 2 s\u1ea3nh c\xf3 c\xf9ng s\u1ed1 l\xe1 v\xe0 gi\xe1 tr\u1ecb , x\xe9t ch\u1ea5t c\u1ee7a qu\xe2n l\u1edbn nh\u1ea5t c\u1ee7a s\u1ea3nh \u0111\xf3 \u0111\u1ec3 t\xecm ra s\u1ea3nh l\u1edbn h\u01a1n \nVd: J Q K(\u2663) < J Q K(\u2666)\n\n3 \u0111\xf4i th\xf4ng: 3 \u0111\xf4i b\xe0i c\xf3 gi\xe1 tr\u1ecb t\u0103ng d\u1ea7n 4 4 + 5 5 + 6 6 \n\nT\u1ee9 qu\xfd: 4 l\xe1 b\xe0i c\xf3 gi\xe1 tr\u1ecb gi\u1ed1ng nhau 5 5 5 5, J J J J, Q Q Q Q \n              4 \u0111\xf4i th\xf4ng: 4 \u0111\xf4i b\xe0i c\xf3 gi\xe1 tr\u1ecb t\u0103ng d\u1ea7n 4 4 + 5 5 + 6 6 + 7 7 \n              5 \u0111\xf4i th\xf4ng: 5 \u0111\xf4i b\xe0i c\xf3 gi\xe1 tr\u1ecb t\u0103ng d\u1ea7n 4 4 + 5 5 + 6 6 + 7 7 + 8 8\n             6 \u0111\xf4i th\xf4ng: 6 \u0111\xf4i b\xe0i c\xf3 gi\xe1 tr\u1ecb t\u0103ng d\u1ea7n 4 4 + 5 5 + 6 6 + 7 7 + 8 8 + 9 9\n\n\u0110\u1ed9 m\u1ea1nh c\u1ee7a h\xe0ng \u0111\u01b0\u1ee3c t\xednh nh\u01b0 sau: 6 \u0111\xf4i th\xf4ng > 5 \u0111\xf4i th\xf4ng > 4 \u0111\xf4i th\xf4ng > 4 \u0111\xf4i th\xf4ng nh\u1ecf h\u01a1n > t\u1ee9 qu\xfd > 3 \u0111\xf4i th\xf4ng\n\n\u0110\xe1nh b\xe0i:\n\nQuy\u1ec1n \u0111\xe1nh tr\u01b0\u1edbc: V\xe1n \u0111\u1ea7u ti\xean, quy\u1ec1n \u0111\xe1nh tr\u01b0\u1edbc s\u1ebd thu\u1ed9c v\u1ec1 ng\u01b0\u1eddi s\u1edf h\u1eefu 3 \u2660.\n\nT\u1eeb v\xe1n ti\u1ebfp theo ng\u01b0\u1eddi nh\u1ea5t \u1edf v\xe1n tr\u01b0\u1edbc \u0111\xf3 s\u1ebd \u0111\u01b0\u1ee3c quy\u1ec1n \u0111\xe1nh tr\u01b0\u1edbc .\n\nV\xf2ng \u0111\xe1nh: Theo ng\u01b0\u1ee3c chi\u1ec1u kim \u0111\u1ed3ng h\u1ed3, m\u1ed7i ng\u01b0\u1eddi \u0111\u01b0\u1ee3c ra 1 l\xe1 b\xe0i ho\u1eb7c 1 b\u1ed9 nhi\u1ec1u l\xe1. Ng\u01b0\u1eddi ra sau ph\u1ea3i \u0111\xe1nh b\xe0i c\xf3 c\xf9ng lo\u1ea1i v\xe0 cao h\u01a1n ng\u01b0\u1eddi \u0111\xe1nh tr\u01b0\u1edbc, tr\u1eeb tr\u01b0\u1eddng h\u1ee3p ch\u1eb7t b\xe0i. Lo\u1ea1i l\xe0 c\xf9ng b\xe0i l\u1ebb, \u0111\xf4i, b\u1ed9 ba,s\u1ea3nh.\n\nH\u1ebft v\xf2ng \u0111\xe1nh: trong v\xf2ng \u0111\xe1nh, n\u1ebfu c\xf3  1 ng\u01b0\u1eddi b\u1ecf l\u01b0\u1ee3t th\xec coi nh\u01b0 b\u1ecf c\u1ea3 v\xf2ng. N\u1ebfu kh\xf4ng c\xf2n ai ch\u1eb7n \u0111\u01b0\u1ee3c ti\u1ebfp th\xec ng\u01b0\u1eddi \u0111\xe1nh cu\u1ed1i c\xf9ng \u0111\u01b0\u1ee3c ra b\xe0i b\u1eaft \u0111\u1ea7u v\xf2ng m\u1edbi .\n\nK\u1ebft th\xfac: \n\nKhi ng\u01b0\u1eddi \u0111\u1ea7u ti\xean \u0111\xe1nh h\u1ebft b\xe0i, ng\u01b0\u1eddi \u0111\xf3 s\u1ebd \u0111\u01b0\u1ee3c t\xednh Nh\u1ea5t\n\nNh\u1eefng ng\u01b0\u1eddi c\xf2n l\u1ea1i ti\u1ebfp t\u1ee5c \u0111\xe1nh b\xe0i \u0111\u1ec3 ch\u1ecdn ra th\u1ee9 t\u1ef1 v\u1ec1 Nh\xec , Ba , B\xe9t\n\nCh\u1eb7t 2 (Heo) :\n3 \u0111\xf4i th\xf4ng ch\u1eb7t \u0111\u01b0\u1ee3c 2(heo) v\xe0 3 \u0111\xf4i th\xf4ng nh\u1ecf h\u01a1n ( theo v\xf2ng ch\u01a1i ) \n\nT\u1ee9 qu\xfd ch\u1eb7t \u0111\u01b0\u1ee3c heo, \u0111\xf4i heo, 3 \u0111\xf4i th\xf4ng v\xe0 t\u1ee9 qu\xfd nh\u1ecf h\u01a1n ( theo v\xf2ng ch\u01a1i ) \n\n4 \u0111\xf4i th\xf4ng ch\u1eb7t \u0111\u01b0\u1ee3c heo, \u0111\xf4i heo, 3 \u0111\xf4i th\xf4ng , t\u1ee9 qu\xfd v\xe0 4 \u0111\xf4i th\xf4ng nh\u1ecf h\u01a1n ( kh\xf4ng c\u1ea7n theo v\xf2ng ch\u01a1i ) \n\n" Ch\u1eb7t ch\u1ed3ng " cu\u1ed1i c\xf9ng l\xe0 t\u1ed5ng k\u1ebft t\u1ea5t c\u1ea3 c\xe1c h\xe0nh vi "ch\u1eb7t" tr\u01b0\u1edbc \u0111\xf3 . Ng\u01b0\u1eddi b\u1ecb ch\u1eb7t sau c\xf9ng s\u1ebd ch\u1ecbu thi\u1ec7t h\u1ea1i ti\u1ec1n ch\u1eb7t .\n\nT\u1edbi tr\u1eafng: ki\u1ec3u th\u1eafng ngay l\u1eadp t\u1ee9c kh\xf4ng c\u1ea7n \u0111\xe1nh khi ng\u01b0\u1eddi ch\u01a1i th\u1ecfa m\xe3n b\u1ed9 b\xe0i sau :\n\nT\u1ea1i v\xe1n \u0111\u1ea7u: T\u1ee9 qu\xfd 3 ho\u1eb7c 3 \u0111\xf4i th\xf4ng c\xf3 3 \u2660\n\nC\xe1c v\xe1n sau: T\u1ee9 qu\xfd 2 , 5 \u0111\xf4i th\xf4ng , 6 \u0111\xf4i th\xf4ng , 6 \u0111\xf4i b\u1ea5t k\u1ef3 , S\u1ea3nh R\u1ed3ng ( s\u1ea3nh n\u1ed1i t\u1eeb 3 -> A ) , 12 / 13 l\xe1 b\xe0i c\xf9ng m\xe0u ( \u2663\u2660 ho\u1eb7c \u2665\u2666 ) \n\nTh\u1ed1i b\xe0i: \n\nNg\u01b0\u1eddi ch\u01a1i b\u1ecb t\xednh Th\u1ed1i khi ng\u01b0\u1eddi \u0111\xf3 v\u1ec1 B\xe9t v\xe0 tr\xean tay h\u1ecd c\xf2n c\xe1c l\xe1 b\xe0i sau\nTh\u1ed1i c\xf3 2(heo)\nTh\u1ed1i 3 \u0111\xf4i th\xf4ng\nTh\u1ed1i 4 \u0111\xf4i th\xf4ng\nTh\u1ed1i t\u1ee9 qu\xfd\n\nNg\u01b0\u1eddi ch\u01a1i b\u1ecb Ch\xe1y b\xe0i khi trong v\xe1n ch\u01a1i c\xf3 ng\u01b0\u1eddi v\u1ec1 Nh\u1ea5t h\u1ebft b\xe0i , nh\u01b0ng ng\u01b0\u1eddi ch\u01a1i \u0111\xf3 v\u1eabn ch\u01b0a \u0111\xe1nh l\xe1 b\xe0i n\xe0o (c\xf2n \u0111\u1ee7 13 l\xe1).';
    exports.default = tutor;
    cc._RF.pop();
  }, {} ],
  vi_VN: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "92027qqs+hEwbWfts3Mohj3", "vi_VN");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var vi_VN_tut_1 = require("./vi_VN_tut");
    exports.default = {
      PLAYNOW: "Ch\u01a1i ngay",
      WIN: "Nh\u1ea5t",
      LOSE2: "Nh\xec",
      LOSE3: "Ba",
      LOSE4: "B\xe9t",
      PLAYER: "S\u1ed1 ng\u01b0\u1eddi ch\u01a1i",
      BETNO: "M\u1ee9c c\u01b0\u1ee3c",
      DAILY: "Th\u01b0\u1edfng h\xe0ng ng\xe0y",
      D1: "Ng\xe0y 1",
      D2: "Ng\xe0y 2",
      D3: "Ng\xe0y 3",
      D4: "Ng\xe0y 4",
      D5: "Ng\xe0y 5",
      D6: "Ng\xe0y 6",
      D7: "Ng\xe0y 7",
      "3TURNS": "3 l\u01b0\u1ee3t",
      "5TURNS": "5 l\u01b0\u1ee3t",
      TURNS: "5 l\u01b0\u1ee3t",
      RECEIVED: "\u0110\xe3 nh\u1eadn",
      LEADER: "B\u1ea3ng x\u1ebfp h\u1ea1ng",
      NOVIDEO: "Video hi\u1ec7n kh\xf4ng kh\u1ea3 d\u1ee5ng",
      BET: "C\u01b0\u1ee3c",
      NO: "B\xe0n",
      PASS: "B\u1ecf l\u01b0\u1ee3t",
      HIT: "\u0110\xe1nh",
      ARRANGE: "X\u1ebfp b\xe0i",
      QUITGAME: "Tho\xe1t game",
      QUITGAMEP: "B\u1ea1n c\xf3 ch\u1eafc mu\u1ed1n tho\xe1t game? N\u1ebfu tho\xe1t game b\u1ea1n s\u1ebd b\u1ecb tr\u1eeb 10 l\u1ea7n m\u1ee9c c\u01b0\u1ee3c",
      QUIT: "Tho\xe1t",
      "3PAIRS": "3 \u0111\xf4i th\xf4ng",
      "4PAIRS": "4 \u0111\xf4i th\xf4ng",
      FOURKIND: "T\u1ee9 qu\xfd",
      FLUSH: "T\u1edbi tr\u1eafng",
      "1BEST": "Heo",
      "2BEST": "\u0110\xf4i heo",
      "3BEST": "3 heo",
      INSTRUCT: "H\u01b0\u1edbng d\u1eabn",
      NOMONEY: "B\u1ea1n \u0111\xe3 h\u1ebft ti\u1ec1n, nh\u1eadn t\xe0i tr\u1ee3 t\u1eeb nh\xe0 c\xe1i",
      RECEI2: "Nh\u1eadn",
      SPINNOW: "Quay ngay",
      SPIN: "Quay ",
      "1TURN": "Th\xeam l\u01b0\u1ee3t",
      LUCKYSPIN: "V\xf2ng quay may m\u1eafn",
      LUCK: "Ch\xfac b\u1ea1n may m\u1eafn l\u1ea7n sau",
      TURN: "B\u1ea1n \u0111\u01b0\u1ee3c c\u1ed9ng th\xeam l\u01b0\u1ee3t",
      X2: "Nh\xe2n \u0111\xf4i ti\u1ec1n th\u01b0\u1edfng",
      X3: "Nh\xe2n ba ti\u1ec1n th\u01b0\u1edfng",
      X5: "Nh\xe2n n\u0103m ti\u1ec1n th\u01b0\u1edfng",
      X10: "Nh\xe2n m\u01b0\u1eddi ti\u1ec1n th\u01b0\u1edfng",
      MISS: "M\u1ea5t l\u01b0\u1ee3t",
      MONEY1: "Ch\xfac m\u1eebng b\u1ea1n \u0111\xe3 nh\u1eadn \u0111\u01b0\u1ee3c %s t\u1eeb nh\xe0 c\xe1i",
      TUT: vi_VN_tut_1.default
    };
    cc._RF.pop();
  }, {
    "./vi_VN_tut": "vi_VN_tut"
  } ]
}, {}, [ "Api", "AudioPlayer", "AutoHide", "Bot", "Card", "CardGroup", "CommonCard", "Config", "Deck", "EventKeys", "Game", "HandCard", "Helper", "Home", "LBEntry", "Language", "Leaderboard", "Notice", "Player", "Popup", "Shop", "Slider2", "SpinWheel", "Text2", "Timer", "Toast", "image", "Modal", "DailyBonus", "DailyBonusItem", "TweenMove", "util", "en_US", "en_US_tut", "th_TH", "th_TH_tut", "tl_PH", "tl_PH_tut", "vi_VN", "vi_VN_tut", "use_reversed_rotateBy" ]);