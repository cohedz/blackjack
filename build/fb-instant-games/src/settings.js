window._CCSettings = {
    platform: "fb-instant-games",
    groupList: [
        "default"
    ],
    collisionMatrix: [
        [
            true
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/Scenes/home.fire",
    orientation: "landscape",
    debug: true,
    jsList: []
};
