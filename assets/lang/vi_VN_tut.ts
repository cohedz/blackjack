const tutor = `Tiến Lên Miền Nam sử dụng bộ bài Tây 52 lá.

Giá trị quân bài sẽ phụ thuộc vào số: 2(heo) > A(xì) > K(già) > Q(đầm) > J(bồi) > 10 > 9 > .... > 3.

Nếu hai quân bài có cùng số thì sẽ so sánh theo chất : ♥ > ♦ > ♣ > ♠.
Ví dụ : 10 ♥ > 10 ♦

Chia bài: Mỗi người được chia 13 lá bài.

Cách xếp bài: 
Bài rác 1 lá bài lẻ: bài lẻ có thể bị đè bởi 1 lá bài lẻ có giá trị lớn hơn
Vd : 4 ♦ > 3 ♠

Bộ đôi 2 lá bài giống nhau: 1 đôi có thể đè được 1 đôi bé hơn 
Vd : J ♥ + J ♠ > J ♦ + J ♣

Tương tự với bộ 3 lá bài giống nhau ( sám cô )

Sảnh : có ít nhất 3 lá bài có giá trị tăng dần: 3 + 4 + 5 + 6 + 7 + 8 ......
1 Sảnh có thể bị đè bởi 1 sảnh có cùng số lá, nhưng giá trị của số đó lớn hơn
Vd : Sảnh 3 4 5 < 4 5 6 , hoặc 7 8 9 < J Q K 

Nếu 2 sảnh có cùng số lá và giá trị , xét chất của quân lớn nhất của sảnh đó để tìm ra sảnh lớn hơn 
Vd: J Q K(♣) < J Q K(♦)

3 đôi thông: 3 đôi bài có giá trị tăng dần 4 4 + 5 5 + 6 6 

Tứ quý: 4 lá bài có giá trị giống nhau 5 5 5 5, J J J J, Q Q Q Q 
              4 đôi thông: 4 đôi bài có giá trị tăng dần 4 4 + 5 5 + 6 6 + 7 7 
              5 đôi thông: 5 đôi bài có giá trị tăng dần 4 4 + 5 5 + 6 6 + 7 7 + 8 8
             6 đôi thông: 6 đôi bài có giá trị tăng dần 4 4 + 5 5 + 6 6 + 7 7 + 8 8 + 9 9

Độ mạnh của hàng được tính như sau: 6 đôi thông > 5 đôi thông > 4 đôi thông > 4 đôi thông nhỏ hơn > tứ quý > 3 đôi thông

Đánh bài:

Quyền đánh trước: Ván đầu tiên, quyền đánh trước sẽ thuộc về người sở hữu 3 ♠.

Từ ván tiếp theo người nhất ở ván trước đó sẽ được quyền đánh trước .

Vòng đánh: Theo ngược chiều kim đồng hồ, mỗi người được ra 1 lá bài hoặc 1 bộ nhiều lá. Người ra sau phải đánh bài có cùng loại và cao hơn người đánh trước, trừ trường hợp chặt bài. Loại là cùng bài lẻ, đôi, bộ ba,sảnh.

Hết vòng đánh: trong vòng đánh, nếu có  1 người bỏ lượt thì coi như bỏ cả vòng. Nếu không còn ai chặn được tiếp thì người đánh cuối cùng được ra bài bắt đầu vòng mới .

Kết thúc: 

Khi người đầu tiên đánh hết bài, người đó sẽ được tính Nhất

Những người còn lại tiếp tục đánh bài để chọn ra thứ tự về Nhì , Ba , Bét

Chặt 2 (Heo) :
3 đôi thông chặt được 2(heo) và 3 đôi thông nhỏ hơn ( theo vòng chơi ) 

Tứ quý chặt được heo, đôi heo, 3 đôi thông và tứ quý nhỏ hơn ( theo vòng chơi ) 

4 đôi thông chặt được heo, đôi heo, 3 đôi thông , tứ quý và 4 đôi thông nhỏ hơn ( không cần theo vòng chơi ) 

" Chặt chồng " cuối cùng là tổng kết tất cả các hành vi "chặt" trước đó . Người bị chặt sau cùng sẽ chịu thiệt hại tiền chặt .

Tới trắng: kiểu thắng ngay lập tức không cần đánh khi người chơi thỏa mãn bộ bài sau :

Tại ván đầu: Tứ quý 3 hoặc 3 đôi thông có 3 ♠

Các ván sau: Tứ quý 2 , 5 đôi thông , 6 đôi thông , 6 đôi bất kỳ , Sảnh Rồng ( sảnh nối từ 3 -> A ) , 12 / 13 lá bài cùng màu ( ♣♠ hoặc ♥♦ ) 

Thối bài: 

Người chơi bị tính Thối khi người đó về Bét và trên tay họ còn các lá bài sau
Thối có 2(heo)
Thối 3 đôi thông
Thối 4 đôi thông
Thối tứ quý

Người chơi bị Cháy bài khi trong ván chơi có người về Nhất hết bài , nhưng người chơi đó vẫn chưa đánh lá bài nào (còn đủ 13 lá).`

export default tutor;
