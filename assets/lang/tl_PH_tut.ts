const tutor = `Para sa unang laro, ang dealer ay napili nang sapalaran; pagkatapos ay ang natalo ng bawat laro ay kailangang harapin ang susunod. Kapag mayroong apat na manlalaro, 13 cards ang ibabahagi sa bawat manlalaro.

Kung may mas kaunti sa apat na manlalaro, 13 cards pa rin ang nakikitungo sa bawat manlalaro, at magkakaroon ng ilang mga kard na natitira na hindi na ginagamit - hindi ito ginagamit sa laro. Ang isang kahalili sa tatlong mga manlalaro ay, sa pamamagitan ng naunang kasunduan, upang makitungo sa bawat 17 card. Kapag mayroong dalawang manlalaro lamang, 13 card lamang ang bawat isa ay dapat na harapin - kung ang lahat ng mga kard ay naaksyunan ang mga manlalaro ay maaaring mag-ehersisyo ang mga kamay ng bawat isa, na makakasira sa laro. Kapag mayroong higit sa apat na manlalaro, maaari kang sumang-ayon nang maaga alinman sa pakikitungo sa 13 mga kard bawat isa mula sa dobleng deck, o pakikitungo sa maraming mga card hangga't maaari pantay sa mga manlalaro.

Ang laro

Sa unang laro lamang, ang manlalaro na may 3 ng Spades ay nagsisimulang maglaro. Kung walang sinuman ang mayroong 3 (sa tatlo o dalawang laro ng manlalaro) sinumang may hawak ng pinakamababang card ay nagsisimula. Dapat magsimula ang manlalaro sa pamamagitan ng paglalaro ng pinakamababang card na ito, alinman sa sarili o bilang bahagi ng isang kumbinasyon.

Sa kasunod na mga laro, ang nagwagi ng nakaraang laro ay naglalaro muna, at maaaring magsimula sa anumang kumbinasyon.

Ang bawat manlalaro naman ay dapat na talunin ang dati nang nilalaro na card o kombinasyon, sa pamamagitan ng paglalaro ng isang card o kombinasyon na pumapalo dito, o pumasa at hindi naglalaro ng anumang mga card. Ang (mga) tinugtog na kard ay inilalagay sa isang magbunton ng mukha sa gitna ng mesa. Ang paglalaro ay paikot-ikot sa talahanayan nang maraming beses hangga't kinakailangan hanggang sa ang isang tao ay maglaro ng isang kard o kumbinasyon na walang ibang natalo. Kapag nangyari ito, ang lahat ng mga pinatugtog na kard ay itinabi, at ang taong ang walang patalo na laro ay nagsisimula muli sa pamamagitan ng paglalaro ng anumang ligal na kard o kombinasyon na nakaharap sa gitna ng mesa.

Kung pumasa ka ay naka-lock sa labas ng laro hanggang sa may gumawa ng isang dula na walang nakakatalo. Kapag ang mga kard ay itinabi at ang isang bagong kard o kombinasyon ay pinangungunahan may karapatan kang maglaro muli.

Halimbawa (na may tatlong mga manlalaro): ang manlalaro sa kanan ay gumaganap ng solong tatlo, may hawak kang alas ngunit nagpasya na pumasa, ang manlalaro sa kaliwa ay gumaganap ng siyam at ang manlalaro sa kanan ay gumaganap ng isang hari. Hindi mo matalo ngayon ang hari sa iyong ace, dahil nakapasa ka na. Kung ang pangatlong manlalaro ay pumasa din, at ang iyong kalaban sa kanang kamay ngayon ay humantong sa isang reyna, maaari mo na ngayong i-play ang iyong ace kung nais mo.

Ang mga ligal na dula sa laro ay ang mga sumusunod:
Single card Ang pinakamababang solong card ay ang 3 at ang pinakamataas ay ang 2.
Ipares ang Dalawang kard ng parehong ranggo - tulad ng 7-7 o Q-Q.
Tatlong Tatlong kard ng magkaparehong ranggo - tulad ng 5-5-5
Apat ng isang uri Apat na kard ng parehong ranggo - tulad ng 9-9-9-9.
Pagkakasunud-sunod Tatlo o higit pang mga kard ng magkakasunod na ranggo (ang mga suit ay maaaring ihalo) - tulad ng 4-5-6 o J-Q-K-A. Ang mga pagkakasunud-sunod ay hindi maaaring "i-on ang sulok" sa pagitan ng dalawa at tatlo - Ang A-2-3 ay hindi isang wastong pagkakasunud-sunod dahil ang 2 ay mataas at 3 ay mababa.
Double Sequence Tatlo o higit pang mga pares ng magkakasunod na ranggo - tulad ng 3-3-4-4-5-5 o 6-6-7-7-8-8-9-9.

Sa pangkalahatan, ang isang kumbinasyon ay maaari lamang matalo ng isang mas mataas na kumbinasyon ng parehong uri at parehong bilang ng mga kard. Kaya't kung ang isang solong card ay hahantong, iisang card lamang ang maaaring i-play; kung ang isang pares ay pinangunahan pares lamang ang maaaring i-play; ang isang pagkakasunud-sunod ng tatlong card ay maaari lamang matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong card; at iba pa. Hindi mo maaaring halimbawa talunin ang isang pares gamit ang isang triple, o isang pagkakasunud-sunod ng apat na card na may isang pagkakasunud-sunod ng limang card.

Upang magpasya kung alin sa dalawang mga kumbinasyon ng parehong uri ang mas mataas ay titingnan mo lamang ang pinakamataas na card na pinagsama. Halimbawa 7-7 beats 7-7 dahil pinapalo ng puso ang brilyante. Sa parehong paraan 8-9-10 beats 8-9-10 sapagkat ito ang pinakamataas na card (ang sampu) na inihambing.

Mayroong apat na mga pagbubukod sa panuntunan na ang isang kumbinasyon ay maaari lamang matalo ng isang kumbinasyon ng parehong uri:

Ang isang apat na uri ay maaaring matalo ang anumang solong dalawa (ngunit hindi sa anumang iba pang solong card, tulad ng isang alas o hari). Ang isang apat na uri ay maaaring matalo ng mas mataas na apat na uri.

Ang isang pagkakasunud-sunod ng tatlong pares (tulad ng 7-7-8-8-9-9) ay maaaring matalo ang anumang solong dalawa (ngunit hindi anumang iba pang solong card). Ang isang pagkakasunud-sunod ng tatlong mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng tatlong mga pares.

Ang isang pagkakasunud-sunod ng apat na pares (tulad ng 5-5-6-6-7-7-8-8) ay maaaring matalo ang isang pares ng dalawa (ngunit hindi sa anumang iba pang pares). Ang isang pagkakasunud-sunod ng apat na mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng apat na mga pares.

Ang isang pagkakasunud-sunod ng limang pares (tulad ng 8-8-9-9-10-10-J-J-Q-Q) ay maaaring matalo ang isang hanay ng tatlong dalawa (ngunit hindi sa anumang iba pang tatlong uri). Ang isang pagkakasunud-sunod ng limang mga pares ay maaaring matalo ng isang mas mataas na pagkakasunud-sunod ng limang mga pares.

Ang mga kumbinasyong ito na maaaring matalo ang mga nag-iisang twos o mga hanay ng dalawa ay paminsan-minsang kilala bilang mga bomba o dalawang-bomba, at maaaring i-play kahit ng isang manlalaro na dati nang lumipas.

Tandaan na nalalapat lamang ang mga pagbubukod na ito sa pagkatalo sa dalawa, hindi sa iba pang mga kard. Halimbawa, kung ang isang tao ay naglalaro ng alas ay hindi mo ito matatalo sa iyong apat na uri, ngunit kung ang alas ay pinalo ng dalawa, kung gayon ang iyong apat na uri ay maaaring magamit upang talunin ang dalawa`

export default tutor;
