import tutor from './en_US_tut';

export default {
    'PLAYNOW': 'Play now',
    'WIN': '1st',
    'LOSE2': '2nd',
    'LOSE3': '3rd',
    'LOSE4': '4th',
    'PLAYER': 'Number of players',
    'BETNO': 'Bet',
    'DAILY': 'Daily reward',
    'D1': 'Day 1',
    'D2': 'Day 2',
    'D3': 'Day 3',
    'D4': 'Day 4',
    'D5': 'Day 5',
    'D6': 'Day 6',
    'D7': 'Day 7',
    '3TURNS': '3 turns',
    '5TURNS': '5 turns',
    'TURNS': 'turns',
    'RECEIVED': 'Received',
    'LEADER': 'Leaderboard',
    'NOVIDEO': 'Video cannot be played now',
    'BET': 'Bet',
    'NO': 'No',
    'PASS': 'Pass',
    'HIT': 'Hit',
    'ARRANGE': 'Arrange',
    'QUITGAME': 'Quit game',
    'QUITGAMEP': 'Do you want to quit game? If you quit game you\'ll lose ten times as bet level',
    'QUIT': 'Quit',
    '3PAIRS': '3 consecutive pairs',
    '4PAIRS': '4 consecutive pairs',
    'FOURKIND': 'Four of a kind',
    'FLUSH': 'Straight Flush',
    '1BEST': 'best',
    '2BEST': '2 best',
    '3BEST': '3 best',
    'INSTRUCT': 'Instruction',
    'NOMONEY': 'You\'ve run out of money, click to receive more money',
    'RECEI2': 'Claim',
    'SPINNOW': 'Spin',
    'SPIN': 'Spin',
    '1TURN': '1 more turn',
    'LUCKYSPIN': 'Lucky Spin',
    'LUCK': 'Have a luck later',
    'TURN': 'One more turn',
    'X2': 'X2 money',
    'X3': 'X3 money',
    'X5': 'X5 money',
    'X10': 'X10 money',
    'MISS': 'Miss',
    'MONEY1': 'Congratulation! You\'ve got %s',
    'TUT': tutor
}