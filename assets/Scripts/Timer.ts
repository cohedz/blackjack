// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Timer extends cc.Component {
    sprite: cc.Sprite = null;
    timer: number = 0;
    duration: number = 0;
    completed: Function = null;
    _selectorTarget: any = null;
    target: any = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.sprite = this.getComponent(cc.Sprite);
    }

    start() {

    }

    update(dt) {
        this.timer += dt;
        if (this.timer < this.duration) {
            this.sprite.fillRange = this.timer / this.duration;
            return;
        }

        this.node.active = false;
        if (this.completed != null) {
            this.completed.call(this._selectorTarget, this.target);
        }
    }

    onCompleted(selector: Function, selectorTarget: any, target: any) {
        this.completed = selector;
        this.target = target;
        this._selectorTarget= selectorTarget;
    }

    show(time: number) {
        this.duration = time;
        this.timer = 0;
        this.node.active = true;
    }

    hide() {
        this.node.active = false;
    }
}
