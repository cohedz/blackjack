import Card from "./Card";
import CardGroup from "./CardGroup";

export default class Helper {

    static isHouse(cards: Card[], amount: number = 0): boolean {
        if (amount != 0 && cards.length != amount) return false;

        if (amount == 0 && (cards.length < 2 || cards.length > 4)) return false;

        let cardMin = Helper.findMinCard(cards);
        let cardMax = Helper.findMaxCard(cards);

        return cardMin.rank == cardMax.rank;
    }

    static isStraight(cards: Card[], amount: number = 0): boolean {
        if (amount != 0 && cards.length != amount) return false;

        if (amount == 0 && cards.length < 3) return false;

        let cardMin = Helper.findMinCard(cards);

        for (let i = cards.length - 1; i > 0; --i) {
            if (cardMin.rank + i > 14) return false;

            let card = Helper.findCardByRank(cards, cardMin.rank + i);
            if (!card) return false;
        }

        return true;
    }

    static isMultiPair(cards: Card[], amount: number = 0): boolean {
        if (cards.length < 6 || cards.length % 2 == 1) return false;

        if (amount != 0 && cards.length != 2 * amount) return false;

        for (let i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == 15) return false;
            let matched = Helper.findCardByRank(cards, cards[i].rank, cards[i]);
            if (!matched) return false;
        }

        return true;
    }

    static isBigWin(cards: Card[]) {
        let reduce = cards.reduce(function (accumulator: object, currentValue: Card) {
            if (!accumulator[currentValue.rank]) {
                accumulator[currentValue.rank] = [];
            }
            accumulator[currentValue.rank].push(currentValue);
            return accumulator;
        }, {});

        if (!!reduce[15] && reduce[15].length == 4) return 1;
        if (Helper.isDragon(reduce)) return 2;
        if (Helper.pairCount(reduce) == 6) return 3;

        return 0;
    }

    static isDragon(reduce: object): boolean {
        for (let i = 3; i <= 14; i++) {
            if (!reduce[i]) {
                return false;
            }
        }
        return true;
    }

    static pairCount(reduce: object): number {
        let count: number = 0;
        for (let v in reduce) {
            if (reduce[v].length >= 2) count++;
        }
        return count;
    }

    static sort(cards: Card[]) {
        cards.sort(function (a, b) {
            return a.gt(b) ? 1 : -1;
        });
    };

    // card utlis
    static findCard(cards: Card[], rank: number, suit: number): Card {
        for (let i = cards.length - 1; i >= 0; --i) {
            let card = cards[i];
            if (card.rank == rank && card.suit == suit)  return card;
        }
        return null;
    }

    static findMaxCard(cards: Card[], filter: Card = null): Card {
        let card: Card = null;
        for (let i = cards.length - 1; i >= 0; --i) {
            let c = cards[i];
            if (filter == null || c.lt(filter)) {
                if (card == null)
                    card = c;
                else if (c.gt(card))
                    card = c;
            }
        }
        return card;
    }

    static findMinCard(cards: Card[], filter: Card = null): Card {
        let card: Card = null;
        for (let i = cards.length - 1; i >= 0; --i) {
            let c = cards[i];
            if (filter == null || c.gt(filter)) {
                if (card == null)
                    card = c;
                else if (c.lt(card))
                    card = c;
            }

        }
        return card;
    }

    static findAllCardByRange(cards: Card[], max: number, min: number) {
        let setCards = [];
        for (let i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank > min && cards[i].rank <= max) {
                if (!Helper.findCardByRank(setCards, cards[i].rank)) {
                    setCards.push(cards[i]);
                }
            }
        }
        return setCards;
    }

    static findAllCardByRank(cards: Card[], rank: number, limit: number): Card[] {
        let setCards = [];
        for (let i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
                if (setCards.length == limit)
                    return setCards;
            }
        }
        return setCards;
    }

    static findCardByRank(cards: Card[], rank: number, filter: Card = null): Card {
        for (let i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank && cards[i] != filter) return cards[i];
        }
        return null;
    }

    // array utlis
    static findIndex(arr: Array<object>, target): number {
        for (let i = arr.length - 1; i >= 0; --i) {
            if (target == arr[i]) return i;
        }
        return -1;
    }

    static removeBy(arr: Array<object>, target) {
        let idx = Helper.findIndex(arr, target);
        Helper.removeAt(arr, idx);
    }

    static removeAt(arr: Array<object>, idx: number) {
        arr.splice(idx, 1);
    }

    static highlight(cards: Card[], handCards: Card[]) {
        return [];
    };

    static findStraightCard(cards: Card[], filter: CardGroup): Array<Card> {
        for (let i = cards.length - 1; i >= 0; --i) {
            let cardStarted = cards[i];
            if (cardStarted.rank < 15 && cardStarted.gt(filter.highest)) {
                let straight = Helper.findAllCardByRange(cards, cardStarted.rank, cardStarted.rank - filter.count());
                if (straight.length == filter.count()) {
                    return straight;
                }
            }
        }
        return null;
    }

    static findHouseCard(cards: Card[], filter: Card, limit: number): Array<Card> {
        for (let i = cards.length - 1; i >= 0; --i) {
            let cardStarted = cards[i];
            if (cardStarted.gt(filter)) {
                let straight = Helper.findAllCardByRank(cards, cardStarted.rank, limit);
                if (straight.length == limit) {
                    return straight;
                }
            }
        }
        return null;
    }

    static findMultiPairCard(cards: Card[], filter: Card, limit: number): Array<Card> {
        // let reduce = cards.reduce(function(accumulator: object, currentValue: Card) {
        //     if (!accumulator[currentValue.rank]) {
        //         accumulator[currentValue.rank] = [];
        //     }
        //     accumulator[currentValue.rank].push(currentValue);
        //     return accumulator;
        // }, {});

        // for (let i = 0; i < cards.length; i++) {
        //     let card = cards[i];
        //     if (card.gt(filter)) {
        //         let multiPairs = [];
        //         for(let j = 0; j < limit; j++) {
        //             let arr: Array<Card> = reduce[card.rank - j];
        //             if (arr.length == 1) break;
        //             multiPairs.push();
        //         }
        //     }
        // }
        return null;
    }

    // caculate
    static calculate(cards: Card[]): number {
        let pigs = Helper.findAllCardByRank(cards, 15, 4);
        let spotWin = Helper.calculateSpotWin(pigs);
        return cards.length + spotWin;
    }

    static calculateSpotWin(pigs: Card[]): number {
        let total = 0;
        for (let i = pigs.length - 1; i >= 0; --i) {
            total += pigs[i].suit + 2;
        }
        return total;
    }
}
