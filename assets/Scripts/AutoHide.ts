// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class AutoHide extends cc.Component {
    @property(cc.Label)
    label: cc.Label = null;
    @property
    duration: number = 3;
    elapsed: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    update (dt) {
        this.elapsed += dt;
        if (this.elapsed >= this.duration) {
            this.node.active = false;
        }
    }

    show(sender: cc.Node) {
        this.label.string = 'TÍnh năng sắp ra mắt...';
        this.elapsed = 0;
        this.node.active = true;
    }

    openWithText(sender: cc.Node, string: string) {
        this.label.string = string;
        this.elapsed = 0;
        this.node.active = true;
    }
}
