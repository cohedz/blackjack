// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import util from "./util";
import Api from "./Api";
import Popup from "./Popup";
import Toast from "./Toast";
import Modal from "./popop/Modal";
import EventKeys from "./EventKeys";
import Language from "./Language";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SpinWheel extends cc.Component {
    @property(cc.Sprite)
    wheel: cc.Sprite = null;
    @property(cc.Label)
    ticket: cc.Label = null;
    @property(Popup)
    popup: Popup = null;
    @property(Toast)
    toast: Toast = null;
    @property(cc.Button)
    btnRun: cc.Button = null;
    @property(cc.Button)
    btnAd: cc.Button = null;

    @property({type: cc.AudioClip})
    audioSpinWin: cc.AudioClip = null;
    @property({type: cc.AudioClip})
    audioSpinLose: cc.AudioClip = null;
    @property({type: cc.AudioClip})
    audioSpinRun: cc.AudioClip = null;
    @property(Modal)
    modal: Modal = null;

    onSpinHide: () => void;
    onSpinCompleted: (result: {id: number, angle: number, msg: string}, won: number) => void;

    private won: number = 0;
    private isRunning: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
    }

    // update (dt) {}

    show(won: number) {
        const playNow = Api.ticket > 0;
        this.popup.open(this.node, 2);
        this.ticket.string = util.numberFormat(Api.ticket);
        this.node.active = true;
        this.isRunning = false;
        this.btnAd.node.active = !playNow;
        this.btnRun.node.active = playNow;
        this.won = won;
    }

    hide() {
        this.popup.close(this.node, 2);
    }

    play() {
        if (this.isRunning || Api.ticket <= 0) return;

        Api.ticket--;
        this.updateTicket();

        this.spin();

        Api.logEvent(EventKeys.SPIN);
    }

    spin() {
        util.playAudio(this.audioSpinRun);

        this.isRunning = true;
        let result = this.randomPrize();
        let action = cc.rotateBy(5, 360 * 10 + result.angle - 30 - (this.wheel.node.angle % 360));
        this.wheel.node.runAction(cc.sequence(
            action.easing(cc.easeCubicActionOut()),
            cc.callFunc(this.onCompleted, this, result)
        ));
    }

    showVideoAd() {
        if (this.isRunning) return;
        Api.logEvent(EventKeys.POPUP_ADREWARD_SPIN_CLICK);
        Api.showRewardedVideo(() => {
            this.spin();
        }, (msg) => {
            Api.logEvent(EventKeys.POPUP_ADREWARD_SPIN_ERROR);
            this.modal.show(Language.getInstance().get('NOVIDEO'));
        })
    }

    private onCompleted(sender: cc.Node, data: any) {
        const msg = Language.getInstance().get(data.msg);
        this.toast.show(msg);
        this.onSpinCompleted(data, this.won);
        if(data.id == 1) {
            this.node.runAction(cc.sequence(
                cc.delayTime(0.3),
                cc.callFunc(this.spin, this)
            ))
        } else {
            this.node.runAction(cc.sequence(
                cc.delayTime(0.5),
                cc.callFunc(this.hide, this)
            ));
            util.playAudio(data.id == 0 ? this.audioSpinLose : this.audioSpinWin);
        }
        // if(Api.ticket == 0) {
        //     this.btnAd.node.active = true;
        //     this.btnRun.node.active = false;
        // }
    }

    onDisable() {
        this.onSpinHide()
    }

    compute(id: number, val: number) {
        switch(id) {
            case 2: return val;
            case 3: return val * 2;
            case 4: return val * 4;
            case 5: return val * 9;
            default: return 0;
        }
    }

    onClose() {
        if (this.isRunning) return;
        Popup.instance.close(null, 2);
    }

    private randomPrize() {
        const rate = Math.random() * 100;
        let sample = SpinWheel.result_rate;
        for(let i=0; i< sample.length; i++)
            if (sample[i].rate > rate) return sample[i];
        return sample[0];
    }

    private updateTicket() {
        Api.updateTicket();
        this.ticket.string = util.numberFormat(Api.ticket);
    }

    //Tỷ lệ vòng quay : X10 ( 5% ) , X5 ( 10% ) , X3 ( 20% ) , THÊM LƯỢT ( 15% ) , MẤT LƯỢT ( 15% ) , X2 (35%)
    private static readonly result_rate = [
        {id: 0, angle: 4*60, rate:  15, msg: "LUCK"},
        {id: 1, angle: 5*60, rate:  30, msg: "TURN"},
        {id: 2, angle: 0*60, rate:  65, msg: "X2"},
        {id: 3, angle: 1*60, rate:  85, msg: "X3"},
        {id: 4, angle: 2*60, rate:  90, msg: "X5"},
        {id: 5, angle: 3*60, rate: 100, msg: "X10"},
    ]
}
