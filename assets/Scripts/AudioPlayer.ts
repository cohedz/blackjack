// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Config from "./Config";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AudioPlayer extends cc.Component {
    @property({type: cc.AudioClip})
    audioClip: cc.AudioClip = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "AudioPlayer";// This is the code file name
        clickEventHandler.handler = "play";

        var button = this.node.getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }

    // update (dt) {}

    play() {
        if(Config.soundEnable)
            cc.audioEngine.play(this.audioClip, false, 1);
    }


}
