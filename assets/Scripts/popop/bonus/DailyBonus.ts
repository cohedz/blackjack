// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Popup from "../../Popup";
import Config from "../../Config";
import DailyBonusItem from "./DailyBonusItem";
import Api from "../../Api";
import AutoHide from "../../AutoHide";
import util from "../../util";
import EventKeys from "../../EventKeys";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DailyBonus extends cc.Component {
    @property(Popup)
    popup: Popup = null;
    @property(AutoHide)
    toast: AutoHide = null;
    @property(DailyBonusItem)
    days: DailyBonusItem[] = [];
    @property(cc.Node)
    effect: cc.Node = null;
    current: number = 0;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    // update (dt) {}

    show(day: number, claimed: boolean = false) {
        if (!claimed) {day++;}
        if (day > Config.dailyBonus.length) {
            day = 1;
        }
        this.current = day;
        this.popup.open(this.node, 2);
        for (let i = 0; i < this.days.length; i++) {
            const d = i+1;
            const claimable = (d == day) && !claimed;
            if (d < day) {
                this.days[i].setClaimable(claimable, true);
            } else if (d == day) {
                this.days[i].setClaimable(claimable, claimed);
            } else {
                this.days[i].setClaimable(claimable, false);
            }
        }
    }

    onClaim(sender: cc.Node, day: string) {
        if (Api.dailyBonusClaimed) return;
        const date = day ? parseInt(day) : this.current;
        this.onClaimCompl(sender, date, false);
        cc.systemEvent.emit('claim_daily_bonus', date, Config.dailyBonus[date - 1], false);
        this.popup.close(this.node, 2);
    }

    onClaimExtra(sender: cc.Node, day: string) {
        if (Api.dailyBonusClaimed) return;
        const date = day ? parseInt(day) : this.current;
        Api.logEvent(EventKeys.POPUP_DAILY_REWARD_AD);
        Api.showRewardedVideo(() => {
            this.onClaimCompl(sender, date, false);
            cc.systemEvent.emit('claim_daily_bonus', date, Config.dailyBonus[date - 1], true);
            this.popup.close(this.node, 2);
        }, () => {
            cc.systemEvent.emit('claim_daily_bonus_fail');
            Api.logEvent(EventKeys.POPUP_DAILY_REWARD_AD_ERROR);
            this.popup.close(this.node, 2);
        });
    }

    onClaimCompl(sender, day: number, double: boolean) {
        const gift = Config.dailyBonus[day - 1];
        if (!gift.coin) {
            return
        }
        const e = cc.instantiate(this.effect);
        e.active = true;
        e.position = sender.target.position;
        e.scale = 1;
        e.parent = cc.director.getScene().getChildByName("Canvas");
        const coinBonus = double ? gift.coin * 2 : gift.coin;
        e.getComponent(cc.Label).string = util.numberFormat(coinBonus);
        e.runAction(cc.sequence(
            cc.moveBy(1.2, 0, 250),
            cc.removeSelf()
        ))
    }
}
