// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Modal extends cc.Component {
    @property(cc.Node)
    popup: cc.Node = null;

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Label)
    content: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        Modal.instance = this;
    }

    start() {
    }

    // update (dt) {}

    show(text: string) {
        this.content.string = text;
        this.node.active = true;
        this.popup.active = true;
    }

    onClose() {
        this.node.active = false;
        this.popup.active = false;
    }

    public static instance: Modal = null;
}
