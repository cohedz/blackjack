import Card from "./Card";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Deck extends cc.Component {
    cards: Array<Card> = Array();
    offset: number = 0;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        for (let i = 0; i < 52; i++) {
            let card = this.node.children[i].getComponent(Card);
            let cardName = card.node.name;
            card.rank = this.getRankFromName(cardName);
            card.suit = this.getSuitFromName(cardName);
            this.cards.push(card);
        }
    }

    // start() { }

    // update (dt) {}

    shuffle(): void {
        const length = this.cards.length
        const lastIndex = length - 1
        let index = -1
        while (++index < length) {
            const rand = index + Math.floor(Math.random() * (lastIndex - index + 1))
            const value = this.cards[rand]
            this.cards[rand] = this.cards[index]
            this.cards[index] = value
        }
        for (let i = this.cards.length - 1; i >= 0; --i) {
            let card = this.cards[i];
            card.reset();
        }
        this.offset = 0;
        // this.cheat();
    }

    // cheat() {
    //     let pos = 0;
    //     for (let i = this.cards.length - 1; i >= 0; --i) {
    //         let card = this.cards[i];
    //         if (card.rank == 9) {
    //             let tmp = this.cards[pos];
    //             this.cards[pos] = card;
    //             this.cards[i] = tmp;
    //             pos += 4;
    //         }
    //     }
    // }

    pick(): Card {
        let card = this.cards[this.offset];
        ++this.offset;
        return card;
    }

    hide() {
        for (let i = this.offset; i < this.cards.length; i++) {
            this.cards[i].node.active = false;
        }
    }

    private getRankFromName(cardName: string): number {
        if (cardName.length == 3)
            return 10;
        switch (cardName[0]) {
            case 'J': return 11;
            case 'Q': return 12;
            case 'K': return 13;
            case 'A': return 1;
            // case '2': return 15;
            default: return parseInt(cardName[0]);
        }
    }

    private getSuitFromName(cardName: string) {
        switch (cardName[cardName.length - 1]) {
            case 'S': return 1;
            case 'C': return 2;
            case 'D': return 3;
            case 'H': return 4;
            default: throw new Error(cardName);
        }
    }
}
