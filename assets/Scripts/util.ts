import Api from "./Api";
import Config from "./Config";

export default class util {
    static readonly prefix = ["", "k", "M", "G", "T", "P", "E", "Z", "Y", "x10^27", "x10^30", "x10^33"]; // should be enough. Number.MAX_VALUE is about 10^308

    public static numberFormat(n: number): string {
        let ii = 0;
        while ((n = n / 1000) >= 1) { ii++; }
        return (Math.round(n * 10 * 1000) / 10) + util.prefix[ii];
    }

    public static playAudio(audioClip: cc.AudioClip) {
        if (Config.soundEnable)
            cc.audioEngine.playEffect(audioClip, false);
    }

    public static logTimeInGame = (time: number) => {
        if ([30, 60, 90, 120, 180, 270, 510].indexOf(time) > -1 || time === 0) {
            Api.logEvent(`${time}s_sstime`);
        }
    }
}
