// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class TweenMove extends cc.Component {
    private duration: number = 2;
    private delayHide: number = 1;
    private distance: number = 180;
    private from: cc.Vec2 = cc.Vec2.ZERO;
    private time: number = 0;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.from = this.node.getPosition();
    }

    start () {

    }

    update (dt) {
        this.time += dt;
        if (this.time < this.duration) {
            let y = this.from.y + this.distance * (this.time / this.duration);
            this.node.setPosition(this.from.x, y);
        }

        if (this.time >= this.duration + this.delayHide) {
            this.node.active = false;
        }
    }

    play() {
        this.time = 0;
        this.node.active = true;
    }
}
