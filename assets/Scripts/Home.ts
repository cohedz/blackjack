import Api from "./Api";
import Slider2 from "./Slider2";
import Config from "./Config";
import Leaderboard from "./Leaderboard";
import util from "./util";
import Popup from "./Popup";
import DailyBonus from "./popop/bonus/DailyBonus";
import AutoHide from "./AutoHide";
import Modal from "./popop/Modal";
import EventKeys from "./EventKeys";
import Language from "./Language";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Home extends cc.Component {
    @property(cc.Sprite)
    avatar: cc.Sprite = null;
    @property(cc.Label)
    playerName: cc.Label = null;
    @property(cc.Label)
    coin: cc.Label = null;
    @property(Slider2)
    betSetting: Slider2 = null;
    @property(Leaderboard)
    leaderboard: Leaderboard = null;
    @property({ type: cc.AudioClip })
    music: cc.AudioClip = null;
    @property(DailyBonus)
    dailyBonus: DailyBonus = null;
    @property(Popup)
    popup: Popup = null;
    @property(AutoHide)
    toast: AutoHide = null;
    @property(Modal)
    modal: Modal = null;

    playTime: number = 0;
    countTime: number = 0;
    itvCountTime = null;

    initCountTime() {
        this.itvCountTime = setInterval(() => {
            this.countTime += Config.stepOfCountTime / 1000
            util.logTimeInGame(this.countTime)
        }, Config.stepOfCountTime)
    }

    clearLogTime() {
        this.itvCountTime && clearInterval(this.itvCountTime);
        this.itvCountTime = null;
        this.countTime = 0
    }

    // init logic
    start() {

        util.logTimeInGame(0);
        this.initCountTime()

        this.betSetting.onValueChange = this.onBetChange;
        Api.initAsync((bonus: boolean, day: number) => {
            Language.getInstance().load(Api.locale);
            cc.systemEvent.emit('LANG_CHAN');
            this.coin.string = util.numberFormat(Api.coin);
            this.playerName.string = Api.username;
            cc.assetManager.loadRemote<cc.Texture2D>(Api.photo, (err, tex) => {
                this.avatar.spriteFrame = new cc.SpriteFrame(tex);
                Config.userphoto = tex;
            });
            if (bonus) {
                Api.preloadRewardedVideo(() => {
                    Api.logEvent(EventKeys.POPUP_DAILY_REWARD_SHOW);
                    this.dailyBonus.show(day, Api.dailyBonusClaimed);
                });
                cc.systemEvent.on('claim_daily_bonus', this.onClaimDailyBonus, this);
                cc.systemEvent.on('claim_daily_bonus_fail', this.onClaimDailyBonusFail, this);
            }
        }, this.leaderboard);

        cc.systemEvent.on('lb_battle', this.onChallenge, this);
        cc.systemEvent.on('lb_share', this.onShare, this);

        cc.audioEngine.setMusicVolume(0.3);
        if (Config.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        }

        // this.scheduleOnce(this.preloadSceneGame, 1);
        setTimeout(() => {
            this.preloadSceneGame()
        }, 1);
        Api.preloadInterstitialAd();

    }

    preloadSceneGame = () => {
        cc.director.preloadScene('game',
            (completedCount, totalCount, item) => {
                // console.log('====================================');
                // console.log('preload', completedCount, totalCount, item);
                // console.log('====================================');
            },
            (error) => {
                console.log('====================================');
                console.log('loaded game');
                console.log('====================================');
            }
        );
    }

    async onShare() {
        await Api.shareAsync();
    }

    onChallenge = async (playerId: string, photo: cc.Texture2D, username: string, coin: number) => {
        if (Api.coin <= Config.bankrupt) {
            Api.preloadRewardedVideo();
            this.popup.open(this.node, 4);
            Api.showInterstitialAd();
            return;
        }

        try {
            await Api.challenge(playerId);
        } catch (error) { console.log(error) }

        if (Math.random() < 0.7) {
            Api.showInterstitialAd();
        }

        cc.audioEngine.stopMusic();
        Config.battle = { photo, username, coin };
        let minCoin = Math.min(Math.max(coin, 100000), Api.coin);
        Config.betValue = Math.floor(minCoin * 0.3);
        cc.director.loadScene('game');
        Api.logEvent(EventKeys.PLAY_WITH_FRIEND);
    }

    onClaimDailyBonus(day: number, bonus: { coin: number, ticket: number }, extra: boolean) {
        if (bonus.coin) {
            Api.coinIncrement(extra ? 2 * bonus.coin : bonus.coin);
            this.coin.string = util.numberFormat(Api.coin);
        }
        if (bonus.ticket) {
            Api.ticket += extra ? 2 * bonus.ticket : bonus.ticket;
        }
        Api.claimDailyBonus(day);
    }

    onClaimDailyBonusFail() {
        this.modal.show(Language.getInstance().get('NOVIDEO'));
    }

    openPlayPopupClick() {
        Api.logEvent(EventKeys.PLAY_NOW);
        if (Api.coin > Config.bankrupt) {
            this.popup.open(this.node, 1);
        } else {
            Api.preloadRewardedVideo();
            Api.showInterstitialAd();
            this.popup.open(this.node, 4);
            Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_OPEN);
        }
    }

    onPlayClick() {
        Api.logEvent(EventKeys.PLAY_ACCEPT);
        Config.battle = null;
        cc.audioEngine.stopMusic();
        cc.director.loadScene('game');
    }

    onShopClick() {
        Api.logEvent(EventKeys.SHOP_GO);
        cc.director.loadScene('shop');
    }

    onDailyBonusClick() {
        Api.logEvent(EventKeys.DAILY_GIFT_CLICK);
        this.dailyBonus.show(Api.dailyBonusDay, Api.dailyBonusClaimed);
        Api.showInterstitialAd();
    }

    onBetChange(value: number) {
        Config.betValue = value;
    }

    onBotChange(sender: cc.Toggle, params: string) {
        Api.logEvent(EventKeys.PLAY_MODE + '-' + params);
        if (sender.isChecked) {
            Config.totalPlayer = parseInt(params);
        }
    }

    onSoundToggle(sender: cc.Toggle, isOn: boolean) {
        Config.soundEnable = !sender.isChecked;

        if (Config.soundEnable) {
            cc.audioEngine.playMusic(this.music, true);
        } else {
            cc.audioEngine.stopMusic();
        }
    }

    claimBankruptcyMoney(bonus: number) {
        const msg = cc.js.formatStr(Language.getInstance().get('MONEY1'), util.numberFormat(bonus));
        this.toast.openWithText(null, msg);
        Api.coinIncrement(bonus);
        this.coin.string = util.numberFormat(Api.coin);
        this.popup.close(null, 4);
    }

    inviteFirend() {
        Api.logEvent(EventKeys.INVITE_FRIEND);
        Api.invite(() => {
            this.claimBankruptcyMoney(Config.bankrupt_bonus);
        }, () => {
            this.popup.close(null, 2);
            this.toast.openWithText(null, 'Mời bạn chơi không thành công');
        });
    }

    adReward() {
        Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_CLICK);
        Api.showRewardedVideo(() => {
            this.claimBankruptcyMoney(Config.bankrupt_bonus);
        }, () => {
            this.claimBankruptcyMoney(Api.randomBonus());
            Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_ERROR);
        })
    }
}
