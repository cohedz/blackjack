import helper from "./Helper";
import Card from "./Card";
import CardGroup from "./CardGroup";
import Timer from "./Timer";
import TweenMove from "./tween/TweenMove";
import util from "./util";
import Config from "./Config";
import Game from "./Game";
import Api from "./Api";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Player extends cc.Component {
    @property(cc.Node)
    fold: cc.Node = null;
    @property(Timer)
    timer: Timer = null;
    @property(cc.Label)
    coin: cc.Label = null;
    @property(cc.Label)
    username: cc.Label = null;
    @property(cc.Node)
    marker: cc.Node = null;
    @property(cc.Sprite)
    avatar: cc.Sprite = null;
    @property(TweenMove)
    effect: TweenMove = null;
    @property(cc.Label)
    pointLbl: cc.Label = null;

    seat: number = 0;
    cards: Array<Card> = [];
    prepareCards: CardGroup = new CardGroup([], CardGroup.Ungrouped);
    inRound: boolean = true;
    coinVal: number = 1;
    cardMargin: number = 0;
    cardRootPosition: cc.Vec2 = null;
    cardDirection: number = null;
    labelCardCounter: cc.Label = null;
    numOfAce: number = 0
    point: number = 0

    bonusType: number = 0 // 0 nothing,  1 double, 2 insurr
    isSplit: number = 0

    subUser: Player = null
    isSubUser: boolean = false
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.fold.active = false;
        this.timer.hide();
        this.marker.active = false;
        // this.cardMargin = this.seat == 0 ? Player.CARD_MARGIN :
        //     0.5 * Player.CARD_MARGIN;
        this.cardMargin = 0.5 * Player.CARD_MARGIN;
        this.cardDirection = this.seat == 1 ? -1 : 1;
        this.labelCardCounter = this.marker.getComponentInChildren(cc.Label);
    }

    setBonusType = (bonusType: number) => {

        try {
            let self = this
            let txtBonusUser = 'Canvas/user/bonus'
            if (!this.inRound) {
                self = this.subUser
                txtBonusUser = 'Canvas/subUser/bonus'
            }
            self.bonusType = bonusType

            let bonusLbl = cc.find(txtBonusUser);
            bonusLbl.active = !!bonusType
            let str = bonusType == 1 ? 'x2' : '/2'
            bonusLbl.getComponent(cc.Label).string = str
        } catch (error) {

        }
    }

    // update (dt) {}

    show() {
        this.node.active = true;
    }

    showAllCard = () => {
        this.cards.map(card => {
            card.show()
        })
    }

    hide() {
        this.node.active = false;
    }

    isBot(): boolean {
        return this.seat !== Config.totalPlayer / 2;
        return this.seat > 0;
    }

    isUser(): boolean {
        return this.isSubUser || this.seat === Config.totalPlayer / 2;
    }

    isCai(): boolean {
        return this.seat === 0;
        return this.seat === Config.totalPlayer / 2;
    }

    setInRound(inRound: boolean) {
        this.fold.active = !inRound;
        this.inRound = inRound;
    }

    isInRound() {
        return this.inRound;
    }

    changeCoin = (coin: number) => {
        let color = cc.color(0xf9, 0xd2, 0x1e, 255), addString = '+';
        if (coin < 0) {
            color = cc.color(255, 0, 0, 255), addString = '-'
        }

        this.effect.getComponent(cc.Label).string = addString + util.numberFormat(Math.abs(coin));
        this.effect.node.color = color
        this.effect.play();
        this.setCoin(this.coinVal + coin);
    }

    addCoin(val: number) {
        this.effect.getComponent(cc.Label).string = '+' + util.numberFormat(val);
        this.effect.node.color = cc.color(0xf9, 0xd2, 0x1e, 255);
        this.effect.play();
        this.setCoin(this.coinVal + val);
    }

    subCoin(val: number) {
        this.effect.getComponent(cc.Label).string = '-' + util.numberFormat(val);
        this.effect.node.color = cc.color(255, 0, 0, 255);
        this.effect.play();
        this.setCoin(this.coinVal - val);
    }

    setCoin(val: number) {
        this.coinVal = val;
        this.coin.string = util.numberFormat(val);
    }

    getCoin(): number {
        return this.coinVal;
    }

    setUsername(val: string) {
        this.username.string = val;
    }

    setAvatar(sprite: cc.SpriteFrame) {
        this.avatar.spriteFrame = sprite;
    }

    reset() {
        this.marker.active = false;
        this.fold.active = false;
        this.cards = [];
        this.isSubUser = false
        this.inRound = true;
        this.prepareCards = new CardGroup([], CardGroup.Ungrouped);
        this.timer.hide();
        this.numOfAce = 0;
        this.point = 0;
        this.bonusType = 0
        this.isSplit = 0
        if (this.subUser) {
            this.subUser.reset()
            this.subUser = null
        }
    }

    split = (subUser: Player) => {

        this.subUser = subUser
        this.subUser.isSubUser = true

        if (this.isSplit) return
        this.isSplit = 1
        if (this.cards[1].rank === 1) this.numOfAce--
        this.cards.map((card, index) => {
            if (index === 1) {
                return this.subUser.push(card, 0)
            }
            card.node.zIndex = index === 1 ? -1 : index
            let pos = this.getCardRootPosition();
            pos.x += index < 2 ? 0 : index * this.cardMargin;
            let move = cc.sequence(cc.delayTime(Game.DEAL_SPEED), cc.moveTo(0.2, pos));
            card.node.runAction(move);
        })
        setTimeout(() => {
            this.cards = this.cards.filter((card, index) => index !== 1)
        }, 0);

        this.updateCardCounter()
        let point = this.getPoint()
        this.updatePoint(point.toString())

    }

    push(card: Card, delay: number) {
        if (!this.inRound && this.subUser) return this.subUser.push(card, delay)

        card.node.zIndex = this.cards.length;
        this.cards.push(card);
        if (card.rank === 1) this.numOfAce += 1

        let point = this.getPoint()

        let fun = () => {
            this.updatePoint(point == 21 && this.cards.length === 2 ? 'BlackJack' : point.toString())
            if (point > 20) this.inRound = false
            this.cards[0].show()
            if (this.isUser()) { card.show() }
            else {
                this.updateCardCounter()
            }
        }

        if (this.isBot() && !this.isCai() && this.cards.length === 2) {
            fun = () => {
                this.cards[1].show()
                this.updatePoint(null)
            }
        }


        // this.updatePoint()
        let pos = this.getCardRootPosition();


        let scale = cc.sequence(cc.delayTime(delay), cc.scaleTo(0.2, 0.5));
        card.node.runAction(scale);
        // if (this.isBot()) {

        //     if (this.cards.length < 3) {
        //         pos.x += (this.cards.length - 1) * this.cardMargin;
        //     } else {
        //         pos.x += 2 * this.cardMargin;
        //     }


        // } else {
        pos.x += (this.cards.length - 1) * this.cardMargin;
        // }

        let move = cc.sequence(cc.delayTime(delay), cc.moveTo(0.2, pos), cc.callFunc(fun));
        card.node.runAction(move);
    }

    showCard2 = () => {
        // if (this.isBot() && !this.isCai() && this.cards.length === 2) {
        //     console.log('====================================');
        //     console.log(123);
        //     console.log('====================================');
        //     this.cards[1].show()
        // }
    }

    touch(pos: cc.Vec2): Card {
        for (let i = this.cards.length - 1; i >= 0; --i) {
            let card = this.cards[i];
            if (card.node.getBoundingBoxToWorld().contains(pos)) {
                return card;
            }
        }
        return null;
    }

    selectCard(card: Card) {
        // this.prepareCards.push(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y + 30);
    }

    unselectCard(card: Card) {
        // this.prepareCards.remove(card);
        // this.prepareCards.calculate();
        // card.setPositionY(this.marker.position.y + this.node.position.y);
    }

    removeCards(cards: CardGroup) {
        for (let i = cards.count() - 1; i >= 0; --i) {
            helper.removeBy(this.cards, cards.at(i));
        }

        this.prepareCards = new CardGroup([], CardGroup.Ungrouped);
    }

    setActive(on: boolean) {
        if (on) {
            this.timer.show(Player.TIME);
        } else {
            this.timer.hide();
        }
    }

    setTimeCallback(selector: Function, selectorTarget: any, data: any) {
        this.timer.onCompleted(selector, selectorTarget, data);
    }

    showHandCards() {
        for (let i = 0; i < this.cards.length; i++) {
            this.cards[i].show();
        }
    }

    reorder() {
        for (let i = this.cards.length - 1; i >= 0; --i) {
            let pos = this.getCardRootPosition();
            pos.x += i * this.cardMargin * this.cardDirection;
            let card = this.cards[i];
            card.node.setPosition(pos);
            card.node.zIndex = this.seat == 1 ? this.cards.length - i : i;
        }
    }

    sortHandCards() {
        helper.sort(this.cards);
        this.reorder();
    }

    showCardCounter() {
        this.marker.active = true;
        this.updateCardCounter();
    }

    hideCardCounter() {
        this.marker.active = false;
    }

    updateCardCounter() {
        try {
            this.labelCardCounter.string = this.cards.length.toString();
        } catch (error) {

        }
    }

    updatePoint(label: string) {
        if (Number(this.pointLbl.string) === NaN) return
        this.point = this.getPoint()
        try {
            this.pointLbl.string = label || this.point.toString()
        } catch (error) {

        }
    }

    getPoint = () => {
        var point = this.cards.reduce((point, current) => {
            if (current.rank === 1) {
                return point
            } else if (current.rank > 10) {
                return point + 10
            } else {
                return point + current.rank
            }
        }, 0);

        if (this.numOfAce > 0) {
            if (point > 11) {
                point += this.numOfAce * 1
            } else {
                point += this.numOfAce * 11
            }
        }
        this.point = point
        return point
        // if (point > 21) {
        //     let txtThua = `$user thua`
        //     if (this.isCai()) {
        //         return alert('cai thua: ' + point + ' diem')
        //     } else if (this.isBot()) {
        //         alert('bot thua: ' + point + ' diem')
        //     } else {
        //         alert('user thua: ' + point + ' diem')
        //     }
        // }
    }

    checkBlackJack = () => {
        let rank1 = this.cards[0]?.rank
        let rank2 = this.cards[1]?.rank

        if (rank1 === 1 || rank2 === 1) {
            if (rank1 > 9 || rank2 > 9) {
                this.setInRound(false)
                this.updatePoint("Black Jack!")
                return true
            }
        }
        return false
    }

    private getCardRootPosition() {
        if (!this.cardRootPosition) {
            this.cardRootPosition = cc.v2(this.marker.position.x * this.node.scaleX + this.node.position.x,
                this.marker.position.y * this.node.scaleY + this.node.position.y);
        }
        return cc.v2(this.cardRootPosition);
    }

    static readonly CARD_MARGIN: number = 60;
    static readonly TIME: number = 30;
}
