// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Language from "./Language";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Text2 extends cc.Component {
    @property()
    key: string = '';
    label: cc.Label | cc.RichText = null;

    // LIFE-CYCLE CALLBACKS:

    start() {
        this.label = this.getComponent(cc.Label);
        if (!this.label) {
            this.label = this.getComponent(cc.RichText);
        }
        if (this.label) {
            this.label.string = Language.getInstance().get(this.key);
        }

        cc.systemEvent.on('LANG_CHAN', this.onLanguageChange, this);
    }

    // update (dt) {}

    onLanguageChange() {
        console.log('onLanguageChange', this, this.label)
        if (this.label) {
            this.label.string = Language.getInstance().get(this.key);
        }
    }

    onDestroy() {
        cc.systemEvent.off('LANG_CHAN');
    }
}
