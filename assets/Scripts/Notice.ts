// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Language from "./Language";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Alert extends cc.Component {
    private label: cc.Label = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.label = this.getComponent(cc.Label);
    }

    start () { }

    showBigPig(num: number, duration: number) {
        if(num == 2) {
            this.show(Alert.TWO_PIG, duration);
        } else if(num == 3) {
            this.show(Alert.THREE_PIG, duration);
        } else {
            this.show(Alert.ONE_PIG, duration);
        }
    }

    show(type: string, duration: number) {
        this.node.active = true;
        this.label.string = Language.getInstance().get(type);
        let action = cc.sequence(
            cc.delayTime(duration),
            cc.callFunc(this.hide, this)
        );
        this.node.runAction(action);
    }

    hide() {
        this.node.active = false;
    }

    // update (dt) {}

    static readonly ONE_PIG: string = '1BEST';
    static readonly TWO_PIG: string = '2BEST';
    static readonly THREE_PIG: string = '3BEST';
    static readonly THREE_PAIR: string = '3PAIRS';
    static readonly FOUR_CARD: string = 'FOURKIND';
    static readonly FOUR_PAIR: string = '4PAIRS';
}
