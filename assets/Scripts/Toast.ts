// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Toast extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
    }

    // update (dt) {}

    show(text: string) {
        this.node.stopAllActions();
        this.label.string = text;
        this.node.active = true;
        let widget = this.getComponent(cc.Widget);
        widget.top = -this.node.height;
        widget.updateAlignment();
        this.node.runAction(cc.sequence(
            cc.moveBy(0.5, 0, -this.node.height),
            cc.delayTime(0.7),
            cc.moveBy(0.5, 0, this.node.height),
            cc.callFunc(this.hide, this)
        ));
    }

    hide() {
        this.node.active = false;
    }
}
