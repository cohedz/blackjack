import vi from '../lang/vi_VN';
import th from '../lang/th_TH';
import en from '../lang/en_US';
import ph from '../lang/tl_PH';

export default class Language {
    private static _instance: Language = null;

    private lang: any = en;

    public static getInstance() {
        if (!Language._instance) {
            Language._instance = new Language();
        }
        return Language._instance;
    }

    public load(lang: string) {
        if (lang == 'vi_VN') {
            this.lang = vi;
        } else if (lang == 'th_TH') {
            this.lang = th;
        } else if (lang == 'tl_PH') {
            this.lang = ph;
        } else {
            this.lang = en;
        }
    }

    public get(key: string) {
        return this.lang[key];
    }

}
