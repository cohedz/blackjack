import Player from "./Player";
import Deck from "./Deck";
import Card from "./Card";
import Helper from "./Helper";
import CardGroup from "./CardGroup";
import CommonCard from "./CommonCard";
import Toast from "./Toast";
import Api from "./Api";
import Notice from "./Notice";
import Config from "./Config";
import util from "./util";
import SpinWheel from "./SpinWheel";
import Popup from "./Popup";
import Bot from "./Bot";
import Modal from "./popop/Modal";
import EventKeys from "./EventKeys";
import Language from "./Language";

let suiteAsset = {
    H: 'co',
    D: "ro",
    C: "chuon",
    S: "bích"
}

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass('Profile')
class Profile {
    @property(cc.SpriteFrame)
    avatar: cc.SpriteFrame = null;
    @property(cc.String)
    username: string = '';
}

@ccclass
export default class Game extends cc.Component {
    @property(Deck)
    deck: Deck = null;
    @property(cc.Button)
    play: cc.Button = null;
    @property(Player)
    seats: Player[] = [];
    @property(cc.Button)
    fire: cc.Button = null;
    @property(cc.Button)
    fold: cc.Button = null;
    @property(cc.Button)
    sort: cc.Button = null;
    @property(cc.Node)
    resultEffects: cc.Node[] = [];
    @property(cc.Node)
    bigWin: cc.Node = null;
    @property(Toast)
    toast: Toast = null;
    @property(Notice)
    notice: Notice = null;
    @property(cc.Label)
    betText: cc.Label = null;
    @property(SpinWheel)
    spin: SpinWheel = null;
    @property(Popup)
    popup: Popup = null;
    @property(Profile)
    profileBots: Profile[] = [];

    @property(cc.Toggle)
    soundToggle: cc.Toggle = null;

    @property({ type: cc.AudioClip })
    audioFold: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioShowCard: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioGarbageCard: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioFire: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioLose: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioWin: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioSortCard: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioQuitGame: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioDealCard: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    audioFireSingle: cc.AudioClip = null;
    @property(Modal)
    modal: Modal = null;

    players: Player[] = [];
    commonCards: CommonCard = new CommonCard();
    state: number = 0;
    turn: number = 0;
    leaveGame: boolean = false;
    owner: Player = null;
    myself: Player = null;
    betValue: number = 1000;
    totalPlayer: number = 6;
    startTime: number = 0;
    playTime: number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        let As = cc.find('Canvas/cards/AS');

        cc.resources.load('cards', cc.SpriteAtlas, (err, atlas) => {
            let initCardsResouces = new Promise(resolve => {
                new Array(13).fill('').map((el, rank) => {
                    ['S', 'C', 'D', 'H'].map((suite, idSuite) => {
                        if (!rank && !suite) return
                        let r = rank + 1
                        const newNode = cc.instantiate(As)


                        switch (r) {
                            case 1:
                                r = 'A'
                                break;
                            case 11:
                                r = 'J'
                                break;
                            case 12:
                                r = 'Q'
                                break;
                            case 13:
                                r = 'K'
                                break;
                        }

                        let assetSuiteName = suiteAsset[suite]
                        let rankAssetname = r
                        if (idSuite < 2) rankAssetname += '-1'

                        // console.log('====================================');
                        // console.log(rankAssetname, assetSuiteName,);
                        // console.log('====================================');
                        newNode.name = r + suite
                        let suiteSprite = atlas.getSpriteFrame(assetSuiteName);
                        newNode.children[0].getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(rankAssetname);
                        newNode.children[1].getComponent(cc.Sprite).spriteFrame = suiteSprite
                        newNode.children[2].getComponent(cc.Sprite).spriteFrame = suiteSprite
                        newNode.children[3].getComponent(cc.Widget).target = newNode
                        newNode.active = false
                        // console.log('====================================');
                        // console.log(newNode);
                        // console.log('====================================');
                        As.parent.addChild(newNode);
                        if (rank === 12 && suite === 'H') {
                            setTimeout(() => {
                                resolve(1)
                            }, 500);
                        }
                    })
                })
            })
            Promise.all([initCardsResouces]).then(rs => {
                this.initGame()
            }).catch(err => {
                console.log('====================================');
                console.log(err);
                console.log('====================================');
            });
        });
    }

    // start(){}

    initGame() {
        this.startTime = Date.now();
        this.state = Game.WAIT;
        this.betValue = Config.betValue;
        this.totalPlayer = Config.totalPlayer;
        this.betText.string = Language.getInstance().get('NO') + ': ' + util.numberFormat(this.betValue);
        this.deck.node.active = false;
        this.sort.node.active = false;
        this.play.node.active = false;
        this.hideDashboard();
        this.notice.hide();

        this.spin.onSpinHide = this.onSpinHide.bind(this);
        this.spin.onSpinCompleted = this.onSpinCompleted.bind(this);

        if (Config.battle) {
            this.totalPlayer = 2;
        }

        let arr = /0|3/

        if (this.totalPlayer === 4) arr = /0|1|3|5/; else if (this.totalPlayer === 6) arr = /0|1|2|3|4|5/
        this.seats.map((seat, i) => {
            seat.seat = this.players.length;
            if (arr.test(i.toString()))
                this.addSeat(seat)
            else {
                seat.hide();
            }
        })

        // for (let i = 0; i < this.totalPlayer; i++) {
        //     let seat = this.seats[i];
        //     seat.seat = i;
        //     if (i < this.totalPlayer) {
        //         let profile = this.getProfileBot();
        //         seat.show();
        //         seat.setAvatar(profile.avatar);
        //         seat.setUsername(profile.username);
        //         this.players.push(seat);
        //     } else {
        //         seat.hide();
        //     }
        // }



        this.myself = this.players[this.totalPlayer / 2];
        this.myself.setUsername(Api.username);
        this.myself.setCoin(Api.coin);
        this.myself.setTimeCallback(this.onPlayerTimeout, this, this.node);
        if (Config.userphoto) {
            this.myself.setAvatar(new cc.SpriteFrame(Config.userphoto));
        }

        if (Config.battle) {
            let bot = this.players[1];
            bot.setCoin(Config.battle.coin);
            bot.setAvatar(new cc.SpriteFrame(Config.battle.photo));
            bot.setUsername(Config.battle.username);
        } else {
            this.players.map(player => {
                if (player.isBot()) player.setCoin(Config.botCoin);
            })
        }

        this.node.runAction(cc.sequence(
            cc.delayTime(0.7),
            cc.callFunc(this.deal, this)
        ));

        if (Config.soundEnable) {
            this.soundToggle.uncheck();
        } else {
            this.soundToggle.check();
        }

        Api.preloadInterstitialAd();
    }

    addSeat = (seat: Player) => {
        let profile = this.getProfileBot();
        seat.show();
        seat.setAvatar(profile.avatar);
        seat.setUsername(profile.username);
        this.players.push(seat);
    }

    onDestroy() {
        // this.clearLogTime()
        // const duration = Date.now() - this.startTime;
        // Api.logEvent(EventKeys.PLAY_DURATION, Math.floor(duration / 1000));
    }

    // update(dt) {
    //     if (this.playTime < 30 && this.playTime + dt >= 30) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-30');
    //     } else if (this.playTime < 60 && this.playTime + dt >= 60) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-60');
    //     } else if (this.playTime < 90 && this.playTime + dt >= 90) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-90');
    //     } else if (this.playTime < 180 && this.playTime + dt >= 180) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-180');
    //     } else if (this.playTime < 360 && this.playTime + dt >= 360) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-360');
    //     } else if (this.playTime < 500 && this.playTime + dt >= 500) {
    //         Api.logEvent(EventKeys.PLAY_TIME + '-500');
    //     }
    //     this.playTime += dt;
    // }

    onTouchStart(evt: cc.Touch) {
        if (this.state != Game.PLAY) return;

        let selected = this.myself.touch(evt.getLocation());

        if (!selected) return;

        let prepareCards = this.myself.prepareCards;

        if (prepareCards.contains(selected)) {
            this.myself.unselectCard(selected);
            this.onCardSelected(selected, false);
        } else {
            this.myself.selectCard(selected);
            this.onCardSelected(selected, true);
        }

        if (this.turn != this.myself.seat) {
            return;
        }

        if (prepareCards.kind == CardGroup.Ungrouped) {
            this.allowFire(false);
            return;
        }

        if (this.commonCards.length() == 0) {
            this.allowFire(true);
            return;
        }

        if (prepareCards.gt(this.commonCards.peek())) {
            this.allowFire(true);
            return;
        }

        this.allowFire(false);
    }

    onPlayerTimeout() {
        this.onStandClicked()
        // this.hideDashboard();
        // if (this.commonCards.length() == 0) {
        //     let cards = Helper.findMinCard(this.myself.cards);
        //     this.onFire(this.myself, new CardGroup([cards], CardGroup.Single));
        //     this.myself.reorder();
        // } else {
        //     return this.onFold(this.myself);
        // }
    }

    showAllPlayerCard = () => {
        this.players.map(player => {
            player.showAllCard()
        })
    }

    onQuitClicked() {
        Api.logEvent(EventKeys.QUIT_GAME);
        if (this.state == Game.PLAY || this.state == Game.DEAL) {
            this.popup.open(this.node, 1);
        } else {
            util.playAudio(this.audioQuitGame);
            cc.director.loadScene('home');
            Api.showInterstitialAd();
        }
    }

    onBackClicked() {
        if (this.state == Game.PLAY || this.state == Game.DEAL) {
            let coin = Math.max(0, Api.coin - this.betValue * 10);
            Api.updateCoin(coin);
        }
        util.playAudio(this.audioQuitGame);
        cc.director.loadScene('home');
        Api.showInterstitialAd();
    }

    showHandCards() {
        util.playAudio(this.audioShowCard);
        // this.myself.showHandCards();
        this.state = Game.PLAY;

        // console.log('====================================');
        // console.log(this.myself.cards);
        // console.log('====================================');



        // if (Helper.isBigWin(this.myself.cards)) {

        //     this.node.runAction(cc.sequence(
        //         cc.delayTime(0.7),
        //         cc.callFunc(this.onBigWin, this, this.myself)
        //     ));
        //     return;
        // }

        // this.node.runAction(cc.sequence(
        //     cc.delayTime(0.7),
        //     cc.callFunc(this.myself.sortHandCards, this.myself)
        // ));

        this.players.map((player, index) => {
            if (player.isBot()) player.showCardCounter() else {
                player.showAllCard()
            }
        })


        let checkBJ = this.players.filter(player => {
            player.checkBlackJack()
        })

        // let isUserWin = checkBJ.find(player => player.isUser())
        // let isUserOrDealerWin = checkBJ.find(player => !player.isBot() || player.isCai())
        // if (isUserOrDealerWin) return this.showResult()
        // for (let i = this.getTotalPlayer() - 1; i > 0; --i) {
        //     this.players[i].showCardCounter();
        // }

        if (!this.owner) {
            this.owner = this.findPlayerHasFirstCard();
        }
        this.setCurrentTurn(this.owner, true);
        this.sort.node.active = true;
    }

    findPlayerHasFirstCard() {
        // for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
        //     if (Helper.findCard(this.players[i].cards, 3, 1))
        //         return this.players[i];
        // }
        let userInRound = this.players.filter(player => player.isInRound() && !player.isCai() && player)
        return userInRound[0];
    }

    showDashboard(lead: boolean) {
        this.allowFold(!lead);
        if (this.commonCards.length() > 0) {
            let cards = Bot.suggest(this.commonCards, this.myself.cards);
            this.allowFire(!!cards);
        } else {
            this.allowFire(true);
        }
        this.fold.node.active = true;
        this.fire.node.active = true;
    }

    setColor(btn: cc.Button, color: cc.Color) {
        btn.target.color = color;
        btn.target.children.forEach(child => {
            child.color = color;
        });
    }

    hideDashboard(needShow = false) {
        let actionUser = cc.find('Canvas/actionUser')

        actionUser.active = needShow
        if (needShow) {
            let showInsurr = this.players[0].cards[0].rank === 1
            // showInsurr = true
            actionUser.children[4].active = showInsurr

            let showSplit = !this.myself.isSplit && this.myself.cards[0].rank === this.myself.cards[1].rank

            showSplit = true
            if (!showSplit && showInsurr) {
                actionUser.children[4].x = 190.44
            }
            actionUser.children[3].active = showSplit
        }
    }

    allowFold(allow: boolean) {
        this.fold.interactable = allow;
        let color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fold, color);
    }

    allowFire(allow: boolean) {
        // this.fire.interactable = allow;
        let color = allow ? cc.Color.WHITE : cc.Color.GRAY;
        this.setColor(this.fire, color);
    }

    onSortCardClicked() {
        util.playAudio(this.audioSortCard);
        this.myself.sortHandCards();
    }

    onFireClicked() {
        this.onHit()
        // let cards = this.myself.prepareCards;
        // if (cards.count() == 0) { return console.log('empty'); }

        // if (this.commonCards.length() > 0 && !cards.gt(this.commonCards.peek())) {
        //     return console.log('invalid');
        // }

        // if (cards.isInvalid()) {
        //     return console.log('invalid');
        // }

        // this.hideDashboard();
        // this.onFire(this.myself, cards);
        // // resort hand card
        // this.myself.reorder();
    }


    onFoldClicked() {
        // this.hideDashboard();
        // this.onFold(this.myself);
    }

    deal() {
        Api.preloadRewardedVideo();
        if (Api.coin <= this.betValue) {
            this.popup.open(null, 3);
            Api.showInterstitialAd();
            Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_OPEN);
            return
        }
        this.play.node.active = false;
        this.deck.node.active = true;
        this.deck.shuffle();
        this.commonCards.reset();
        for (let i = 1; i < this.players.length; i++) {
            let bot = this.players[i];
            if (bot.getCoin() <= this.betValue * 3) {
                let profile = this.getProfileBot();
                let rate = 0.5 + Math.random() * 0.5;
                bot.setCoin(Math.floor(this.myself.getCoin() * rate));
                bot.setAvatar(profile.avatar);
                bot.setUsername(profile.username);
            }
        }

        cc.find('Canvas/actionUser/insurr').x = 402.038

        this.seats[6].hide()

        this.players.map(p => {
            p.reset();
            p.updatePoint('0');
            p.setBonusType(0)
        })

        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < this.getTotalPlayer(); j++) {
                let player = this.players[j];
                let card = this.deck.pick();
                player.push(card, 0.3 + (i * this.getTotalPlayer() + j) * Game.DEAL_SPEED);
            }
        }
        // this.deck.hide();
        this.node.runAction(cc.sequence(
            cc.delayTime(Game.DEAL_SPEED * this.totalPlayer * 2),
            cc.callFunc(this.showHandCards, this))
        );
        this.node.runAction(cc.sequence(
            cc.delayTime(0.4),
            cc.callFunc(() => util.playAudio(this.audioDealCard))
        ));

        Api.preloadInterstitialAd();
    }

    showEffect(player: Player, effect: cc.Node) {
        effect.active = true;
        effect.setPosition(player.node.getPosition());
    }

    onFold(player: Player) {
        util.playAudio(this.audioFold);
        if (this.commonCards.hasCombat()) {
            let count = this.commonCards.getCombatLength();
            let victim = this.commonCards.getCombatVictim();
            let winner = this.commonCards.getCombatWinner();
            let bigPig = this.commonCards.getCombat();
            let spotWin = Math.pow(2, count - 1) * Helper.calculateSpotWin(bigPig.cards) * this.betValue;
            let coin = Math.min(spotWin, victim.coinVal);
            winner.addCoin(coin);
            victim.subCoin(coin);
        }
        if (this.commonCards.isCombatOpen()) {
            this.commonCards.resetCombat();
        }

        player.setInRound(false);
        player.setActive(false);
        let players = this.getInRoundPlayers();
        if (players.length >= 2) {
            return this.nextTurn();
        }
        this.nextRound(players[0]);
    }

    onFire(player: Player, cards: CardGroup, isDown: boolean = false) {
        const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        this.node.runAction(cc.sequence(
            cc.delayTime(0.17),
            cc.callFunc(() => util.playAudio(audioClip))
        ));

        player.setActive(false);
        let cardCount = cards.count();
        let pos = this.commonCards.getPosition();

        cards.sort();

        for (let i = cards.count() - 1; i >= 0; --i) {
            if (isDown) cards.at(i).show();

            let card = cards.at(i);
            card.node.zIndex = this.commonCards.totalCards + i;
            let move = cc.moveTo(0.3, cc.v2(pos.x + (i - cardCount / 2) * Game.CARD_SPACE, pos.y));
            card.node.runAction(move);
            let scale = cc.scaleTo(0.3, 0.6);
            card.node.runAction(scale);
        }
        this.commonCards.push(cards);
        if (cards.highest.rank == 15) {
            this.commonCards.pushCombat(player, cards);
        } else if (this.commonCards.isCombatOpen()) {
            this.commonCards.pushCombat(player, cards);
        }
        player.removeCards(cards);
        if (player.isBot()) {
            player.updateCardCounter();
        }

        if (cards.highest.rank == 15) {
            this.notice.showBigPig(cards.count(), 3);
        } else if (cards.kind == CardGroup.MultiPair && cards.count() == (3 * 2)) {
            this.notice.show(Notice.THREE_PAIR, 3);
        } else if (cards.kind == CardGroup.MultiPair && cards.count() == (4 * 2)) {
            this.notice.show(Notice.FOUR_PAIR, 3);
        } else if (cards.kind == CardGroup.House && cards.count() == 4) {
            this.notice.show(Notice.FOUR_CARD, 3);
        }
        if (player.cards.length > 0) {
            return this.nextTurn();
        }

        this.state = Game.LATE;
        this.sort.node.active = false;
        this.node.runAction(cc.sequence(
            cc.delayTime(2),
            cc.callFunc(this.showResult, this, player)
        ));
    }

    onBigWin(sender: cc.Node, winner: Player) {
        this.state = Game.LATE;
        this.owner = winner;
        this.bigWin.active = true;
        let total = 0;
        for (let i = 0; i < this.players.length; i++) {
            let player = this.players[i];
            if (player != winner) {
                if (player.isBot()) {
                    player.hideCardCounter();
                    player.showHandCards();
                    player.reorder();
                }
                let lost = 13 * this.betValue;
                lost = Math.min(player.coinVal, lost);
                player.subCoin(lost);
                total += lost;
            }
        }
        winner.addCoin(total);
        Api.updateCoin(this.myself.coinVal);
        this.onWin(total);
    }

    // showResult(sender: cc.Node, winner: Player) {
    //     let players = this.players.filter((player) => (player != winner));
    //     players.sort((a, b) => { return a.cards.length - b.cards.length; });

    //     this.showEffect(winner, this.resultEffects[0]);
    //     let total = 0;
    //     let ranking = 1;
    //     if (winner == this.myself) ranking = 1;
    //     for (let i = 0; i < players.length; i++) {
    //         let player = players[i];
    //         if (player.isBot()) {
    //             player.hideCardCounter();
    //             player.showHandCards();
    //             player.reorder();
    //         }
    //         if (player == this.myself) ranking = i + 2;
    //         this.showEffect(player, this.resultEffects[i + 1]);
    //         let lost = Helper.calculate(player.cards) * this.betValue;
    //         lost = Math.min(player.coinVal, lost);
    //         player.subCoin(lost);
    //         total += lost;
    //     }
    //     winner.addCoin(total);
    //     Api.updateCoin(this.myself.coinVal);
    //     this.owner = winner;
    //     if (winner == this.myself) {
    //         this.onWin(total);
    //     } else {
    //         this.onLose(ranking);
    //     }
    // }

    onLose(ranking: number) {
        Api.logEvent(EventKeys.LOSE + '_' + ranking);
        util.playAudio(this.audioLose);
        const rnd = Math.random();
        if (ranking == 2 && rnd <= 0.3) {
            Api.showInterstitialAd();
        } else if (ranking == 3 && rnd <= 0.2) {
            Api.showInterstitialAd();
        } else if (ranking == 4 && rnd <= 0.1) {
            Api.showInterstitialAd();
        }
        this.delayReset(3);
    }

    onWin(won: number) {
        Api.logEvent(EventKeys.WIN);
        Api.logEvent(EventKeys.POPUP_ADREWARD_SPIN_OPEN);
        util.playAudio(this.audioWin);
        const rnd = Math.random();
        if (rnd <= 0.3 && Api.isInterstitialAdLoaded()) {
            Api.showInterstitialAd();
            this.delayReset(3);
        } else {
            this.spin.show(won);
        }
    }

    onSpinHide() {
        this.delayReset(3);
    }

    onSpinCompleted(result, won: number) {
        let bonus = this.spin.compute(result.id, won);
        if (bonus > 0) {
            this.myself.addCoin(bonus);
            Api.updateCoin(this.myself.coinVal);
        }
    }

    delayReset(dt: number) {
        this.node.runAction(cc.sequence(
            cc.delayTime(dt),
            cc.callFunc(this.reset, this)
        ));
    }

    reset() {
        for (let i = this.resultEffects.length - 1; i >= 0; --i) {
            this.resultEffects[i].active = false;
        }
        for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].reset();
        }
        if (this.bigWin.active) {
            this.bigWin.active = false;
        }
        // this.deck.node.active = false;
        this.play.node.active = true;
    }

    onCardSelected(card: Card, selected: boolean) {

    }

    nextTurn() {
        let next = this.getNextInRoundPlayer();
        this.setCurrentTurn(next, false);
    }

    nextRound(lead: Player) {
        this.commonCards.nextRound();
        for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
            this.players[i].setInRound(true);
        }
        this.setCurrentTurn(lead, true);
        // util.playAudio(this.audioGarbageCard);
    }

    onHitClicked() {
        let player = this.myself
        this.takeCard(player)
        player.setActive(true);
        this.hideDashboard()
    }

    onDoubleClicked() {

    }

    onSplitClicked() {

    }

    onStandClicked() {
        let player = this.myself
        player.setActive(false);

        if (!player.inRound && player.subUser) {
            player = player.subUser
        }

        player.setInRound(false)
        this.hideDashboard()
        this.nextTurn()
    }

    showResult = () => {
        // let leaderPoint = this.players.slice(-1)[0].point
        // let userPoint = this.players[0].point

        // let resultString = [`điểm cái: ${leaderPoint}`, `Điểm user: ${userPoint}`]

        // this.players.map(p => {
        //     if (p.isBot() && !p.isCai()) {
        //         resultString.push(`Điểm bot ${resultString.length - 1}: ${p.point}`)
        //     }
        // })


        let dealer = this.players[0]

        let dealerAddCoin = 0

        this.players.map((player, index) => {
            if (!index) return

            let playerCoin = this.calCointEndGame(player)
            if (player.subUser) {
                playerCoin += this.calCointEndGame(player.subUser)
            }

            player.changeCoin(playerCoin)

            console.log('====================================');
            console.log('user ' + index + ' ' + playerCoin);
            console.log('====================================');

            dealerAddCoin -= playerCoin
        })

        dealer.changeCoin(dealerAddCoin)

        console.log('====================================');
        console.log('nha cai ' + dealerAddCoin);
        console.log('====================================');

        this.showAllPlayerCard()
        this.delayReset(3);



        this.state = Game.LATE;
        // this.owner = winner;
        // this.bigWin.active = true;
        // let total = 0;
        // for (let i = 0; i < this.players.length; i++) {
        //     let player = this.players[i];
        //     if (player != winner) {
        //         if (player.isBot()) {
        //             player.hideCardCounter();
        //             player.showHandCards();
        //             player.reorder();
        //         }
        //         let lost = 13 * this.betValue;
        //         lost = Math.min(player.coinVal, lost);
        //         player.subCoin(lost);
        //         total += lost;
        //     }
        // }
        // winner.addCoin(total);
        Api.updateCoin(this.myself.coinVal);
        // this.onWin(total);

        // return this.toast.show(`Tổng cmn kết rồi\n ${resultString.join('\n')}`)
    }


    calCointEndGame = (player: Player) => {
        let dealer = this.players[0]
        let playerCoin = 0
        if (player.bonusType == 2) {
            playerCoin -= this.betValue / 2
        } else if (player.point > 21) {
            playerCoin -= this.betValue
            // player.subCoin(this.betValue)
        } else if (player.checkBlackJack()) {
            playerCoin += this.betValue * 1.5
        } else if (player.point > dealer.point) {
            playerCoin += this.betValue
        } else if (player.point === dealer.point) {
            return 0
        } else if (dealer.point > 21) {
            playerCoin += this.betValue
        }
        else {
            playerCoin -= this.betValue
        }

        if (/1/.test(player.bonusType.toString())) {
            playerCoin *= 2
        }
        return playerCoin
    }

    async setCurrentTurn(player: Player, lead: boolean) {
        this.players.map(p => p.setActive(false))
        if (!player) {
            return this.showResult()
        }
        this.turn = player.seat;
        player.setActive(true);

        if (player.isUser()) {
            return Game.sleep(Game.WAIT_RE_SHOW_USER_ACTION).then(() => {
                this.hideDashboard(true)
            })
        }

        let canHit = player.point > this.players[0].point

        if (player.point < 18) {
            canHit = true
        }

        if (!canHit) {
            canHit = this.players.find(p => player.point > p.point) != undefined
        }

        if (player.point >= 18) canHit = false

        if (player.cards?.length === 5) canHit = false

        await Game.sleep(Game.WAIT_BOT);
        player.setInRound(false)
        // setTimeout(() => {
        if (canHit) {
            this.takeCard(player)
        } else {
            this.toast.show(player.username.string + ' stand' + player.point + " điểm")
            this.nextTurn()
        }
        // }, 2500);


        // if (!player.isBot()) {
        //     this.hideDashboard(true)
        // } else {

        // if(){

        // }

        // }



        // if (this.turn > 0) {
        //     this.node.runAction(cc.sequence(
        //         cc.delayTime(1.5),
        //         cc.callFunc(this.execBot, this, player)
        //     ));
        // } else {
        //     this.showDashboard(lead);
        // }
    }

    getInRoundPlayers(): Array<Player> {
        let players: Array<Player> = [];
        for (let i = this.getTotalPlayer() - 1; i >= 0; --i) {
            if (this.players[i].isInRound())
                players.push(this.players[i]);
        }
        return players;
    }

    getNextInRoundPlayer(): Player {
        // tim thang nao co ghe ngoi > turn trc va dang o trong van choi
        let nextTurn = this.players.find(player => {
            return (player.seat === this.turn && player?.subUser?.inRound) || (player.seat > this.turn && player.isInRound())
        })

        if (!nextTurn && this.players[0].inRound) return this.players[0] // neu khong con ai thi chuyen luot cho nha cai

        // let nextTurn = this.players[this.turn + 1]
        // if (!nextTurn && this.players[0].isInRound()) nextTurn = this.players[0]
        // if (!nextTurn.isInRound()) return
        return nextTurn
        // for (let i = 1; i < this.getTotalPlayer(); i++) {
        //     let offset = this.turn + i;
        //     if (offset >= this.getTotalPlayer()) offset -= this.getTotalPlayer();
        //     if (this.players[offset].isInRound())
        //         return this.players[offset];
        // }
        // return null;
    }

    calculatePoint: (player: Player) => {

    }

    async takeCard(player: Player, needNext = true) {
        let card = this.deck.pick();

        this.toast.show(player.username.string + ': ' + player.point + "đ bốc thêm" + card.rank)
        if (!card?.node) {
            this.state = Game.LATE;
            this.sort.node.active = false;
            // this.node.runAction(cc.sequence(
            //     cc.delayTime(2),
            //     cc.callFunc(this.showResult, this, player)
            // ));
            return
        }
        player.push(card, 0);

        if (!needNext) return
        // const audioClip = cards.highest.rank == 15 || cards.count() > 1 ? this.audioFire : this.audioFireSingle;
        // this.node.runAction(cc.sequence(
        //     cc.delayTime(0.17),
        //     cc.callFunc(() => util.playAudio(audioClip))
        // ));

        await Game.sleep(0)

        if (player.isBot()) {
            // player.updateCardCounter();
        } else {
            player.cards.map(el => el.show())
        }
        player.setActive(false);

        let _player = player

        if (!player.inRound && player.subUser) {
            _player = player.subUser
        }

        if (_player.point >= 21 || _player.cards.length === 5) {
            _player.setInRound(false)
            _player.point > 21 && this.toast.show(_player.username.string + ` Thua: ${_player.point} điểm`)
            // bot stand
            _player.setActive(false);

            return this.nextTurn()
        }
        // console.log('====================================');
        // console.log(player.cards.map(el => el.node.name));
        // console.log('====================================');
        // this.checkWin()
        // this.nextTurn()

        if (!player.bonusType) {
            this.setCurrentTurn(player, false)
        } else {
            player.setInRound(false)
            this.nextTurn()
        }

        // setTimeout(this.nextTurn, 1500);

        // this.node.runAction(cc.sequence(
        //     cc.delayTime(.15),
        //     cc.callFunc(this.nextTurn)
        // ));
    }

    checkWin = () => {
        // console.log('====================================');
        // console.log(this.players);
        // console.log('====================================');
        return new Promise(resolve => {
            // this.players.
        })
    }

    execBot(sender: cc.Node, bot: Player) {
        // alert(1)
        let cards = this.commonCards.isEmpty()
            ? Bot.random(bot.cards)
            : Bot.suggest(this.commonCards, bot.cards);

        if (!cards) {
            return this.onFold(bot);
        }

        this.onFire(this.players[this.turn], cards, true);
    }

    getTotalPlayer() {
        return this.players.length;
    }

    getProfileBot(): Profile {
        for (let i = 0; i < 10; i++) {
            let rnd = Math.floor(Math.random() * this.profileBots.length);
            if (!this.isUsage(this.profileBots[rnd])) {
                return this.profileBots[rnd]
            }
        }
        return this.profileBots[0]
    }

    isUsage(user: Profile) {
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].username.string == user.username) {
                return true;
            }
        }
        return false;
    }

    onSoundToggle(sender: cc.Toggle, isOn: boolean) {
        Config.soundEnable = !sender.isChecked;
    }

    claimBankruptcyMoney(bonus: number) {
        const msg = cc.js.formatStr(Language.getInstance().get('MONEY1'), util.numberFormat(bonus));
        this.toast.show(msg);
        Api.coinIncrement(bonus);
        this.myself.setCoin(Api.coin);
        this.popup.close(null, 3);
        // update bet room
        this.betValue = Math.round(Api.coin * 0.3);
        this.betText.string = util.numberFormat(this.betValue);
    }

    inviteFirend() {
        Api.logEvent(EventKeys.INVITE_FRIEND);
        Api.invite(() => {
            this.claimBankruptcyMoney(Config.bankrupt_bonus);
        }, () => {
            this.popup.close(null, 3);
            this.toast.show('Mời bạn chơi không thành công');
        });
    }

    adReward() {
        Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_CLICK);
        Api.showRewardedVideo(() => {
            this.claimBankruptcyMoney(Config.bankrupt_bonus);
        }, () => {
            this.popup.close(null, 3);
            this.claimBankruptcyMoney(Api.randomBonus());
            Api.logEvent(EventKeys.POPUP_ADREWARD_COIN_ERROR);
        })
    }

    async onSetBonusClicked(e, bonus) {
        console.log('====================================');
        console.log(bonus);
        console.log('====================================');

        if (bonus == 3) {
            this.myself.split(this.seats[6])
            this.seats[6].show()
            this.hideDashboard()
            await Game.sleep(Game.DEAL_SPEED)
            // this.setCurrentTurn(this.myself, false)
            // this.onHitClicked()
            this.takeCard(this.myself, false)
            await Game.sleep(500)
            this.takeCard(this.myself.subUser, false)
            await Game.sleep(1000)
            this.nextTurn()
        } else {
            this.myself.setBonusType(bonus)
            if (bonus == 1) { // double
                this.onHitClicked()
                if (this.myself.inRound) {
                    this.myself.inRound = false
                } else {
                    this.myself.subUser.inRound = false
                }
            } else {
                this.onStandClicked()
            }
        }

    }

    // onSetBonus = (bonus) => {
    //     console.log('====================================');
    //     console.log(bonus);
    //     console.log('====================================');
    //     // this.myself.setBonusType()
    // }

    // cheat
    cheat_win() {
        this.state = Game.LATE;
        this.hideDashboard();
        this.showResult(null, this.myself);
    }

    cheat_lose() {
        this.state = Game.LATE;
        this.hideDashboard();
        this.showResult(null, this.players[1]);
    }

    static sleep = async (wait: number) => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(1);
            }, wait);
        })
    }

    static readonly WAIT: number = 0; // wait for player
    static readonly DEAL: number = 1; // deal card
    static readonly PLAY: number = 2; // play
    static readonly LATE: number = 3; // late
    // static readonly DEAL_SPEED: number = 0//1.66;
    // static readonly CARD_SPACE: number = 45;
    // static readonly WAIT_BOT: number = 0;
    // static readonly WAIT_RE_SHOW_USER_ACTION: number = 0;

    static readonly DEAL_SPEED: number = 1.2//1.66;
    static readonly CARD_SPACE: number = 45;
    static readonly WAIT_BOT: number = 2000;
    static readonly WAIT_RE_SHOW_USER_ACTION: number = 1000;

}
