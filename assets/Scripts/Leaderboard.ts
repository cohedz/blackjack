// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LBEntry from "./LBEntry";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Leaderboard extends cc.Component {
    @property(cc.Node)
    template: cc.Node = null;

    @property(cc.Node)
    container: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {}

    // update (dt) {}

    render(rank: number, name: string, coin: number, avatar: string, playerId: string) {
        let entry = cc.instantiate(this.template);
        entry.getComponent(LBEntry).render(rank, name, coin, avatar, playerId);
        this.container.addChild(entry);
        entry.active = true;
    }

    onLoadComplete() {}

}
