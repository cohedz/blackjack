import Config from "./Config";
import util from "./util";

// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Slider2 extends cc.Component {
    @property(cc.Label)
    maxValue: cc.Label = null;
    @property(cc.Label)
    minValue: cc.Label = null;
    @property(cc.Label)
    value: cc.Label = null;
    @property(cc.Sprite)
    fill: cc.Sprite = null;

    slider: cc.Slider = null;
    onValueChange: (v: number) => void;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        var sliderEventHandler = new cc.Component.EventHandler();
        sliderEventHandler.target = this.node;
        sliderEventHandler.component = "Slider2"
        sliderEventHandler.handler = "_onValueChange";

        this.slider = this.getComponent(cc.Slider);
        this.slider.slideEvents.push(sliderEventHandler);
    }

    start() {
        this.minValue.string = util.numberFormat(Config.minBet);
        this.maxValue.string = util.numberFormat(Config.maxBet);
        this._onValueChange(this.slider, null);
    }

    _onValueChange(sender, params) {
        let val = this.getValue();
        this.onValueChange(val);
        this.value.string = util.numberFormat(val);
        let size = this.fill.node.getContentSize();
        size.width = this.node.getContentSize().width * this.slider.progress;
        this.fill.node.setContentSize(size);
    }

    getValue(): number {
        let val = Config.minBet + (Config.maxBet - Config.minBet) * this.slider.progress;
        return Math.round(val / 1000) * 1000;
    }

    // update (dt) {}
}
