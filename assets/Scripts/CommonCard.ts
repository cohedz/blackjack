import CardGroup from "./CardGroup";
import Player from "./Player";

interface CardPlay {
    player: Player;
    cards: CardGroup;
}

export default class CommonCard {
    totalCards: number = 0;
    cards: Array<CardGroup> = [];
    combat: Array<CardPlay> = [];

    private latest: CardGroup = null;

    getPosition(): cc.Vec2 {
        if (this.cards.length == 0)
            return cc.Vec2.ZERO;

        let sx = Math.random() * 100 - 50;
        let sy = Math.random() * 100 - 50;

        return cc.v2(sx, sy + 50);
    }

    reset() {
        this.totalCards = 0;
        this.cards = [];
        this.latest = null;
        if (this.combat.length > 0) {
            this.combat = [];
        }
    }

    push(cards: CardGroup) {
        this.totalCards += cards.count();
        this.cards.push(cards);
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        this.latest = cards;
    }

    isCombatOpen() {
        return this.combat.length > 0;
    }

    pushCombat(player: Player, cards: CardGroup) {
        this.combat.push({player, cards});
    }

    hasCombat(): boolean {
        if (this.combat.length < 2) return false;
        let peek = this.combat[this.combat.length - 1];
        if (peek.cards.highest.rank == 15) return false;
        return true;
    }

    getCombat(): CardGroup {
        return this.combat[0].cards;
    }

    getCombatLength(): number {
        return this.combat.length;
    }

    getCombatWinner(): Player {
        return this.combat[this.combat.length - 1].player;
    }

    getCombatVictim(): Player {
        return this.combat[this.combat.length - 2].player;
    }

    resetCombat() {
        this.combat = [];
    }

    peek(): CardGroup {
        return this.cards[this.cards.length - 1];
    }

    length(): number {
        return this.cards.length;
    }

    isEmpty(): boolean {
        return this.cards.length == 0;
    }

    nextRound() {
        for (let i = this.cards.length - 1; i >= 0; --i) {
            for (let j = this.cards[i].count() - 1; j >= 0; --j) {
                let card = this.cards[i].at(j);
                card.hide();
                card.node.setScale(0.5);
            }
        }
        if (this.latest) {
            this.overlapCard(this.latest);
        }
        if(this.combat.length > 0) {
            this.combat = [];
        }
        this.cards = [];
        this.latest = null;
    }

    overlapCard(cards: CardGroup) {
        for (let i = 0; i < cards.count(); i++) {
            cards.at(i).overlap();
        }
    }
}
