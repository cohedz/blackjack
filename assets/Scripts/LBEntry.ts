// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import util from "./util";
import Api from "./Api";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LBEntry extends cc.Component {
    @property(cc.Label)
    rank: cc.Label = null;
    @property(cc.Sprite)
    medal: cc.Sprite = null;
    @property(cc.Sprite)
    avatar: cc.Sprite = null;
    @property(cc.Label)
    playerName: cc.Label = null;
    @property(cc.Label)
    playerCoin: cc.Label = null;
    @property(cc.SpriteFrame)
    spriteMedals: cc.SpriteFrame[] = [];

    private playerId: string = null;
    private photo: cc.Texture2D = null;
    private coin: number = 0;
    private click_key: string = 'lb_battle';
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    render(rank: number, name: string, coin: number, avatar: string, playerId: string) {
        this.playerId = playerId;
        this.playerName.string = name;
        this.playerCoin.string = util.numberFormat(coin);
        this.coin = coin;
        cc.assetManager.loadRemote<cc.Texture2D>(avatar, (err, tex) => {
            this.photo = tex;
            this.avatar.spriteFrame = new cc.SpriteFrame(tex);
        });

        if (this.spriteMedals.length >= rank) {
            this.rank.node.active = false;
            this.medal.spriteFrame = this.spriteMedals[rank-1];
        } else {
            this.rank.string = util.numberFormat(rank);
            this.medal.node.active = false;
        }

        if (Api.playerId == playerId) {
            this.click_key = 'lb_share';
        }
    }

    onClick() {
        cc.systemEvent.emit(this.click_key, this.playerId, this.photo, this.playerName.string, this.coin);
    }
}
