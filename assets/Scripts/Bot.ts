import Card from "./Card";
import CardGroup from "./CardGroup";
import Helper from "./Helper";
import CommonCard from "./CommonCard";

export default class Bot {
    static random(handCards: Array<Card>): CardGroup {
        let cards: Card[] = null;
        let firstCard = Helper.findCard(handCards, 3, 1);
        if (firstCard) {
            let straight = Bot.findAllCardByRank(handCards, 3);
            if (straight.length >= 3) {
                return new CardGroup(straight, CardGroup.House);
            }
            return new CardGroup([firstCard], CardGroup.Single);
        }

        cards = Bot.findStraightCard(handCards);
        if (cards) {
            return new CardGroup(cards, CardGroup.Straight);
        }

        cards = Bot.findHouseCard(handCards);
        if (cards && !(cards[0].rank == 15 && cards.length < handCards.length)) {
            return new CardGroup(cards, CardGroup.House);
        }

        let card = Helper.findMinCard(handCards);
        return new CardGroup([card], CardGroup.Single);
    }

    static suggest(commonCards: CommonCard, handCards: Array<Card>): CardGroup {
        let common = commonCards.peek();

        if (common.highest.rank == 15 && common.count() == 3) {
            return null;
        }

        if (common.highest.rank == 15 && common.count() == 2) {
            let cards = Helper.findMultiPairCard(handCards, null, 4 * 2);
            if (cards) {
                return new CardGroup(cards, CardGroup.MultiPair);
            }
            let four = Helper.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup(four, CardGroup.House);
            }
            let pair = Helper.findHouseCard(handCards, common.highest, 2);
            if (pair) {
                return new CardGroup(pair, CardGroup.House);
            }
            return null;
        }

        if (common.highest.rank == 15 && common.count() == 1) {
            let cards = Helper.findMultiPairCard(handCards, null, 3 * 2);
            if (cards) {
                return new CardGroup(cards, CardGroup.MultiPair);
            }
            let four = Helper.findHouseCard(handCards, null, 4);
            if (four) {
                return new CardGroup(four, CardGroup.House);
            }
            let card = Helper.findMinCard(handCards, common.highest);
            if (card) {
                return new CardGroup([card], CardGroup.Single);
            }
            return null;
        }

        if (common.kind == CardGroup.Single) {
            let cards = Helper.findMinCard(handCards, common.highest);
            return cards == null ? null : new CardGroup([cards], CardGroup.Single);
        }

        if (common.kind == CardGroup.House) {
            let cards = Helper.findHouseCard(handCards, common.highest, common.count());
            return cards == null ? null : new CardGroup(cards, CardGroup.House);
        }

        if (common.kind == CardGroup.Straight) {
            let cards = Helper.findStraightCard(handCards, common);
            return cards == null ? null : new CardGroup(cards, CardGroup.Straight);
        }

        if (common.kind == CardGroup.MultiPair) {
            let cards = Helper.findMultiPairCard(handCards, common.highest, common.count() / 2);
            return cards == null ? null : new CardGroup(cards, CardGroup.MultiPair);
        }

        return null;
    }

    static findStraightCard(cards: Card[]): Array<Card> {
        Helper.sort(cards);
        for (let i = 0; i < cards.length; i++) {
            let cardStarted = cards[i];
            if (cardStarted.rank < 13) {
                let straight = Bot._findStraightCard(cards, cardStarted);
                if (straight.length >= 3) {
                    return straight;
                }
            }
        }
        return null;
    }

    static _findStraightCard(cards: Card[], started: Card): Array<Card> {
        let straight = [started];
        for (let c = started.rank + 1; c < 15; c++) {
            let card = Helper.findCardByRank(cards, c);
            if (card == null) { break; }
            else { straight.push(card); }
        }
        return straight;
    }

    static findHouseCard(cards: Card[]): Array<Card> {
        Helper.sort(cards);
        for (let i = 0; i < cards.length; i++) {
            let cardStarted = cards[i];
            let straight = Bot.findAllCardByRank(cards, cardStarted.rank);
            if (straight.length >= 2) {
                return straight;
            }
        }
        return null;
    }

    static findAllCardByRank(cards: Card[], rank: number): Card[] {
        let setCards = [];
        for (let i = cards.length - 1; i >= 0; --i) {
            if (cards[i].rank == rank) {
                setCards.push(cards[i]);
            }
        }
        return setCards;
    }
}
