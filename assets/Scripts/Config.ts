export default class Config {
    public static language: string = 'vi';
    public static readonly defaultCoin: number = 100000;
    public static readonly botCoin: number = 100000;
    public static readonly bankrupt: number = 100;
    public static readonly bankrupt_bonus: number = 500000;
    public static readonly minBet: number = 3000;
    public static readonly maxBet: number = 100000;
    public static betValue: number = 3000;
    public static totalPlayer: number = 6;
    public static userphoto: cc.Texture2D = null;
    public static battle: { photo: cc.Texture2D, username: string, coin: number } = null;
    public static readonly intertital_ads: string = '717961958974985_757265555044625';
    public static readonly reward_video: string = '316445729595977_363105338263349';
    public static readonly gift_reward_video: string = '316445729595977_363105338263349';
    public static soundEnable = true;
    public static readonly dailyBonus = [
        { coin: 100000 },
        { ticket: 3 },
        { coin: 500000 },
        { coin: 1000000 },
        { coin: 5000000 },
        { ticket: 5 },
        { coin: 10000000 }
    ];
    public static stepOfCountTime = 30000;
}

// Ngày 1 : 100K ,
// Ngày 2 : 300K , 
// ngày 3 thêm 3 lượt SPIN x tiền , 
// ngày 4 : 1M , 
// ngày 5 thêm 5 lượt Spin x tiền , 
// ngày 6 5M , 
// ngày 7 : 10M