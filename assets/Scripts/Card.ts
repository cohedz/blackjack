// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Card extends cc.Component {
    @property(cc.SpriteFrame)
    back: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    front: cc.SpriteFrame = null;
    rank: number = 1; // A: 1, J: 11, Q: 12, K: 13
    suit: number = 1; // 1: spade, 2: club, 3: diamond, 4: heart
    overlayer: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.front = this.getComponent(cc.Sprite).spriteFrame;
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.overlayer = this.node.children[this.node.children.length - 1];
    }

    start() {
        this.overlayer.active = false;
    }

    reset() {
        this.node.active = true;
        this.node.setScale(1);
        this.overlayer.active = false;
        this.node.setPosition(cc.Vec2.ZERO);
        this.hide();
    }

    show() {
        this.getComponent(cc.Sprite).spriteFrame = this.front;
        this.setChildrenActive(true);
    }

    hide() {
        this.getComponent(cc.Sprite).spriteFrame = this.back;
        this.setChildrenActive(false);
    }

    private setChildrenActive(active: boolean) {
        for (let i = 0; i < this.node.children.length - 1; i++) {
            this.node.children[i].active = active;
        }
    }

    overlap() {
        this.overlayer.active = true;
    }

    setPositionY(newPosY: number): void {
        this.node.setPosition(this.node.position.x, newPosY);
    }

    compare(o: Card) {
        if (o == null) return 1;
        if (this.rank > o.rank) return 1;
        if (this.rank < o.rank) return -1;
        if (this.suit > o.suit) return 1;
        if (this.suit < o.suit) return -1;
        return 0;
    }

    gt(o: Card) {
        return this.compare(o) > 0;
    }

    lt(o: Card) {
        return this.compare(o) < 0;
    }

    // update (dt) {}

    toString(): string {
        return 'Rank: ' + this.rank + ' Suit: ' + this.suit;
    }
}
