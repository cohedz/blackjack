// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Popup extends cc.Component {
    @property(cc.Node)
    popups: cc.Node[] = [];

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    }

    start () {
        Popup.instance = this;
    }

    // update (dt) {}

    close(sender: cc.Node, id: number) {
        this.node.active = false;
        this.popups.forEach(p => {
            p.active = false;
        })
    }

    open(sender: cc.Node, id: number) {
        this.node.active = true;
        if(id) {
            this.popups[id - 1].active = true;
        }
    }

    public static instance: Popup = null;
}
