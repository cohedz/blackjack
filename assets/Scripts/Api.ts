import Leaderboard from "./Leaderboard";
import Config from "./Config";
import image_base64 from './image';

export default class Api {
    static initialized: boolean = false;
    static coin: number = 10000;
    static ticket: number = 0;
    static username: string = 'DM';
    static playerId: string = 'DM';
    static locale: string = 'vi_VN';
    static photo: string = 'https://i.imgur.com/FUOxs43.jpg';
    static isDirty: boolean = false;
    static preloadedRewardedVideo: FBInstant.AdInstance = null;
    static preloadedInterstitial: FBInstant.AdInstance = null;
    static canSubscribeBot = false;
    static dailyBonusDay: number = 0;
    static dailyBonusClaimed: boolean = false;
    static lb_api = 'https://leaderboard.adpia.com.vn/instant'//'https://lb.dozo.vn/api'
    static lb_id = 'a330e5054804f7fb73bd1f99'//'5f714415630b9b9ff8146f15'
    static ID_APP = '440201687124598'

    public static initAsync(callback: (bonus: boolean, day: number) => void, leaderboard: Leaderboard) {
        if (Api.initialized) {
            Api.loadLeaderboard(leaderboard);
            return callback(false, Api.dailyBonusDay); // daily_bonus_claimed_at, daily_bonus_day
        }

        if (!Api.initialized && window.hasOwnProperty('FBInstant')) {
            Api.initialized = true;
            Api.photo = FBInstant.player.getPhoto();
            Api.username = FBInstant.player.getName();
            Api.playerId = FBInstant.player.getID();
            Api.locale = FBInstant.getLocale();

            Api.getPlayerData(callback);
            Api.loadLeaderboard(leaderboard);
            Api.subscribeBot();
            Api.createShortcut();
        } else {
            Api.ticket = 10;
            Api.dailyBonusDay = 0;
            // setTimeout(() => {
            Api.locale = 'vi_VN';
            // Api.locale = 'th_TH';
            // callback(true, Api.dailyBonusDay);
            Api.loadLeaderboard(leaderboard);
            // }, 5000);
        }
    }

    private static getPlayerData(callback: (bonus: boolean, day: number) => void) {
        FBInstant.player.getDataAsync(['coin', 'ticket', 'daily_bonus_claimed_at', 'daily_bonus_day']).then(function (data) {
            cc.log('data is loaded');
            Api.coin = data['coin'];
            if (Api.coin == undefined || Api.coin == null) {
                Api.coin = Config.defaultCoin;
                Api.isDirty = true;
            }
            Api.ticket = data['ticket'] || 0;
            Api.dailyBonusDay = data['daily_bonus_day'] || 0;
            Api.dailyBonusClaimed = !Api.dailyBonusClaimable(data['daily_bonus_claimed_at']);
            callback(!Api.dailyBonusClaimed, Api.dailyBonusDay);
            if (Api.isDirty) {
                Api.flush();
            }
        });
    }

    private static subscribeBot() {
        FBInstant.player.canSubscribeBotAsync().then(function (can_subscribe) {
            cc.log('subscribeBotAsync', can_subscribe);
            Api.canSubscribeBot = can_subscribe.valueOf();
            // Then when you want to ask the player to subscribe
            if (Api.canSubscribeBot) {
                FBInstant.player.subscribeBotAsync().then(
                    // Player is subscribed to the bot
                ).catch(function (e) {
                    // Handle subscription failure
                    cc.log('subscribeBotAsync');
                });
            }
        }).catch((e) => {
            cc.log('subscribeBotAsync', e);
        });
    }

    private static createShortcut() {
        FBInstant.canCreateShortcutAsync()
            .then(function (canCreateShortcut) {
                if (canCreateShortcut) {
                    FBInstant.createShortcutAsync()
                        .then(function () {
                            // Shortcut created
                        })
                        .catch(function () {
                            // Shortcut not created
                        });
                }
            });
    }

    public static loadLeaderboard(leaderboard: Leaderboard) {
        fetch(`${Api.lb_api}/${Api.lb_id}`).then(response => response.json()).then(res => {
            if (res?.entries?.length) {
                const lastIndex = res?.entries?.length - 1
                res.entries.map((e, i) => {
                    leaderboard.render(
                        i + 1,
                        e.username,
                        e.score,
                        e.photo,
                        e.user_id,
                    );
                    if (i === lastIndex) leaderboard.onLoadComplete();
                })
            }

            // for (let i = 0; i < res.entries.length; i++) {
            //     const e = res.entries[i];
            //     leaderboard.render(
            //         i + 1,
            //         e.username,
            //         e.score,
            //         e.photo,
            //         e.user_id,
            //     );
            // }
        }).catch(error => console.error(error));
    }

    public static preloadRewardedVideo(callback: () => void = null) {
        // if (!window.hasOwnProperty('FBInstant')) {
        if (callback) callback();
        return;
        // }
        if (Api.preloadedRewardedVideo || !Config.reward_video) {
            return;
        }
        FBInstant.getRewardedVideoAsync(Config.reward_video).then(function (rewarded) {
            // Load the Ad asynchronously
            Api.preloadedRewardedVideo = rewarded;
            return Api.preloadedRewardedVideo.loadAsync();
        }).then(() => {
            cc.log('Rewarded video preloaded');
            if (callback) callback();
        }).catch((e) => {
            Api.preloadedRewardedVideo = null;
            console.error(e.message);
            if (callback) callback();
        });
    }

    public static showRewardedVideo(success: () => void, error: (msg: string) => void) {
        // if (!window.hasOwnProperty('FBInstant')) {
        //     return error('rrr');
        // }
        // if (!Config.reward_video) {
        success();
        // }
        // if (!Api.preloadedRewardedVideo) {
        //     console.error('ad not load yet');
        //     return error('ad not load yet');
        // }
        // Api.preloadedRewardedVideo.showAsync().then(function () {
        //     success();
        //     Api.preloadedRewardedVideo = null;
        //     cc.log('Rewarded video watched successfully');
        // }).catch(function (e) {
        //     error(e.message);
        //     Api.preloadedRewardedVideo = null;
        //     console.error(e.message);
        // });
    }

    public static preloadInterstitialAd(callback: () => void = null) {
        // if (window.hasOwnProperty('FBInstant') && !Api.preloadedInterstitial) {
        //     FBInstant.getInterstitialAdAsync(Config.intertital_ads).then(function(interstitial) {
        //         // Load the Ad asynchronously
        //         Api.preloadedInterstitial = interstitial;
        //         return Api.preloadedInterstitial.loadAsync();
        //     }).then(function() {
        //         cc.log('Interstitial preloaded');
        if (callback) callback();
        // }).catch(function(err){
        //     cc.error('Interstitial failed to preload');
        //     cc.error(err);
        // });
        // }
    }

    public static showInterstitialAd() {
        // if (window.hasOwnProperty('FBInstant')) {
        //     if (Api.preloadedInterstitial) {
        //         Api.preloadedInterstitial.showAsync().then(function () {
        //             // Perform post-ad success operation
        //             cc.log('Interstitial ad finished successfully');
        //             Api.preloadedInterstitial = null;
        //         }).catch(function (e) {
        //             cc.error(e.message);
        //             Api.preloadedInterstitial = null;
        //         });
        //     }
        // }
    }

    public static isInterstitialAdLoaded(): boolean {
        if (!window.hasOwnProperty('FBInstant')) {
            return false;
        }
        return !!Api.preloadedInterstitial;
    }

    public static async challenge(playerId: string): Promise<void> {
        await FBInstant.context.createAsync(playerId);
        return FBInstant.updateAsync({
            action: 'CUSTOM',
            template: 'play_turn',
            cta: 'Chơi ngay',
            image: image_base64,
            text: Api.username + ' đang thách đấu bạn. Nhấn chơi ngay!',
            strategy: 'IMMEDIATE'
        });
    }

    public static async shareAsync(): Promise<void> {
        if (window.hasOwnProperty('FBInstant')) {
            return FBInstant.shareAsync({
                intent: 'SHARE',
                image: image_base64,
                text: 'Tiến liên miền nam 2020!',
            });
        }
        return Promise.resolve();
    }

    public static invite(success: (msg: string) => void, error: (reason: any) => void) {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.context
                .chooseAsync()
                .then(function () {
                    success(FBInstant.context.getID());
                    FBInstant.updateAsync({
                        action: 'CUSTOM',
                        template: 'play_turn',
                        cta: 'Chơi ngay',
                        image: image_base64,
                        text: Api.username + ' mời bạn cùng chơi game tiến liên miền nam',
                        strategy: 'IMMEDIATE'
                    });
                }).catch(error);
        } else {
            success('');
        }
    }

    public static coinIncrement(val: number) {
        Api.updateCoin(Api.coin + val);
    }

    public static updateCoin(coin: number) {
        Api.coin = coin;
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(() => cc.log('Score saved')).catch(err => console.error(err));
    }

    public static updateTicket() {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ ticket: Api.ticket }).then(function () {
                cc.log('ticket is set');
            });
        }
    }

    public static claimDailyBonus(day: number) {
        Api.dailyBonusClaimed = true;
        if (window.hasOwnProperty('FBInstant')) {
            let claimed_at = new Date();
            FBInstant.player.setDataAsync({
                coin: Api.coin,
                ticket: Api.ticket,
                'daily_bonus_claimed_at': claimed_at.toString(),
                'daily_bonus_day': day
            }).then(function () {
                cc.log('data is set');
            }, function (reason) {
                cc.log('data is not set', reason);
            });
        }
        Api.dailyBonusDay++;
    }

    private static dailyBonusClaimable(claimedAt: string | Date) {
        if (!claimedAt) return true;
        if (typeof claimedAt == 'string') {
            claimedAt = new Date(claimedAt);
        }
        const now = new Date();
        if (now.getFullYear() != claimedAt.getFullYear()) return true;
        if (now.getMonth() != claimedAt.getMonth()) return true;
        if (now.getDate() != claimedAt.getDate()) return true;
        return false;
    }

    public static flush() {
        if (window.hasOwnProperty('FBInstant')) {
            FBInstant.player.setDataAsync({ coin: Api.coin, ticket: Api.ticket }).then(function () {
                cc.log('data is set');
            });
        }
        Api.setScoreAsync(Api.coin).then(res => res.json()).then(res => cc.log(res)).catch(err => console.error(err));
    }

    public static logEvent(eventName: string, valueToSum?: number, parameters?: Object) {
        if (!eventName) return
        // if (window.hasOwnProperty('FBInstant')) {
        try {
            // window.firebase.analytics().logEvent(eventName, parameters);
            console.log('====================================');
            console.log(eventName, parameters);
            console.log('====================================');
        } catch (error) {
            console.log('====================================');
            console.log('analytic error', error);
            console.log('====================================');
        }
        // FBInstant.logEvent(eventName, valueToSum, parameters);
        // }
    }

    private static setScoreAsync(score) {
        // if (!window.hasOwnProperty('FBInstant')) return;

        const data = { score, userId: Api.playerId, name: Api.username, photo: Api.photo }
        return fetch(`${Api.lb_api}/${Api.lb_id}`,
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            }
        )
    }

    public static randomBonus() {
        const r = Math.random();
        if (r < 0.05) return 400000;
        if (r < 0.2) return 300000;
        if (r < 0.5) return 200000;
        return 100000;
    }
}