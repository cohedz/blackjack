import Card from "./Card";
import Helper from "./Helper";

export default class CardGroup {
    cards: Array<Card>;
    highest: Card;
    kind: number;

    constructor(cards: Array<Card>, kind: number) {
        this.cards = cards;
        this.kind = CardGroup.Single;
        this.kind = kind;
        if (kind != 0) {
            this.highest = Helper.findMaxCard(cards);
        }
    }

    getPosition(): cc.Vec2 {
        if (this.cards.length % 2 != 0) {
            let idx = (this.cards.length - 1) / 2;
            return this.cards[idx].node.getPosition();
        }

        let middle = this.cards.length / 2;
        let c1 = this.cards[middle].node.getPosition();
        let c2 = this.cards[middle - 1].node.getPosition();
        return cc.v2((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
    }

    gt(o: CardGroup): boolean {
        if (this.kind == o.kind && this.count() == o.count())
            return this.highest.gt(o.highest);

        if (o.highest.rank == 15 && o.count() == 1) {
            if (this.kind == CardGroup.MultiPair)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }

        if (o.highest.rank == 15 && o.count() == 2) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            return false;
        }

        if (o.kind == CardGroup.MultiPair && o.count() == 6) {
            if (this.kind == CardGroup.House && this.count() == 4)
                return true;
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
        }

        if (o.kind == CardGroup.House && o.count() == 4) {
            if (this.kind == CardGroup.MultiPair && this.count() == 8)
                return true;
            else
                return false;
        }

        return false;
    }

    at(i: number): Card {
        return this.cards[i];
    }

    count(): number {
        return this.cards.length;
    }

    push(card: Card) {
        this.cards.push(card);
    }

    remove(card: Card) {
        let index = this.cards.indexOf(card, 0);
        this.cards.splice(index, 1);
    }

    contains(card: Card): boolean {
        for (let i = this.cards.length - 1; i >= 0; --i) {
            if (this.cards[i] == card) return true;
        }
        return false;
    }

    calculate() {
        if (this.cards.length == 1) {
            this.kind = CardGroup.Single;
            this.highest = Helper.findMaxCard(this.cards);
        } else if (Helper.isHouse(this.cards)) {
            this.kind = CardGroup.House;
            this.highest = Helper.findMaxCard(this.cards);
        } else if (Helper.isStraight(this.cards)) {
            this.kind = CardGroup.Straight;
            this.highest = Helper.findMaxCard(this.cards);
        } else if (Helper.isMultiPair(this.cards)) {
            this.kind = CardGroup.MultiPair;
            this.highest = Helper.findMaxCard(this.cards);
        } else {
            this.kind = CardGroup.Ungrouped;
        }
    }

    sort() {
        Helper.sort(this.cards);
    }

    isInvalid(): boolean {
        return this.kind == CardGroup.Ungrouped;
    }

    dump() {
        for (let i = this.cards.length - 1; i >= 0; --i) {
            console.log(this.cards[i].toString());
        }
    }

    static readonly Ungrouped: number = 0;
    static readonly Single: number = 1;
    static readonly House: number = 2;
    static readonly Straight: number = 3;
    static readonly Boss: number = 4;
    static readonly MultiPair: number = 5;
}
